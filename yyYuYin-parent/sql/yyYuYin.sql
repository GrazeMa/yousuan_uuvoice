/*
Navicat MySQL Data Transfer

Source Server         : localDate
Source Server Version : 50721
Source Host           : localhost:3306
Source Database       : lyouyou

Target Server Type    : MYSQL
Target Server Version : 50721
File Encoding         : 65001

Date: 2018-09-30 15:57:49
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for app_advertising
-- ----------------------------
DROP TABLE IF EXISTS `app_advertising`;
CREATE TABLE `app_advertising` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `imgUrl` varchar(255) DEFAULT NULL COMMENT 'Banner图',
  `imgTip` varchar(20) DEFAULT NULL COMMENT '图片尺寸',
  `urlType` tinyint(1) DEFAULT NULL COMMENT '跳转类型',
  `urlHtml` varchar(255) DEFAULT NULL COMMENT '跳转路径',
  `content` text COMMENT '页面内容',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注说明',
  `createTime` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='广告设置';

-- ----------------------------
-- Records of app_advertising
-- ----------------------------

-- ----------------------------
-- Table structure for app_banner_info
-- ----------------------------
DROP TABLE IF EXISTS `app_banner_info`;
CREATE TABLE `app_banner_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `title` varchar(20) DEFAULT NULL COMMENT '标题',
  `imgUrl` varchar(255) DEFAULT NULL COMMENT 'Banner图',
  `orderby` int(11) DEFAULT NULL COMMENT '排序',
  `urlType` tinyint(2) DEFAULT NULL COMMENT '跳转类型',
  `urlHtml` varchar(255) DEFAULT NULL COMMENT '跳转路径',
  `content` text COMMENT '网页内容',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注说明',
  `isSue` tinyint(2) DEFAULT '0' COMMENT '是否发布',
  `isDelete` tinyint(2) DEFAULT '0' COMMENT '是否删除',
  `createTime` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Banner信息';

-- ----------------------------
-- Records of app_banner_info
-- ----------------------------

-- ----------------------------
-- Table structure for app_message_info
-- ----------------------------
DROP TABLE IF EXISTS `app_message_info`;
CREATE TABLE `app_message_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `userId` int(11) DEFAULT NULL COMMENT '用户ID',
  `title` varchar(30) DEFAULT NULL COMMENT '标题',
  `content` varchar(255) DEFAULT NULL COMMENT '消息内容',
  `createTime` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统消息';

-- ----------------------------
-- Records of app_message_info
-- ----------------------------

-- ----------------------------
-- Table structure for app_notice_info
-- ----------------------------
DROP TABLE IF EXISTS `app_notice_info`;
CREATE TABLE `app_notice_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `title` varchar(30) DEFAULT NULL COMMENT '标题',
  `coverPlan` varchar(100) DEFAULT NULL COMMENT '封面图',
  `content` text COMMENT '消息内容',
  `createTime` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统公告';

-- ----------------------------
-- Records of app_notice_info
-- ----------------------------

-- ----------------------------
-- Table structure for app_set_info
-- ----------------------------
DROP TABLE IF EXISTS `app_set_info`;
CREATE TABLE `app_set_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `paramName` varchar(50) DEFAULT NULL COMMENT '参数名',
  `keyStr` varchar(50) DEFAULT NULL COMMENT '键值',
  `valueStr` varchar(100) DEFAULT NULL COMMENT '参数值',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注说明',
  `key1` varchar(200) DEFAULT NULL COMMENT '扩展字段',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统设置';

-- ----------------------------
-- Records of app_set_info
-- ----------------------------

-- ----------------------------
-- Table structure for app_sms_code
-- ----------------------------
DROP TABLE IF EXISTS `app_sms_code`;
CREATE TABLE `app_sms_code` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `phone` varchar(20) DEFAULT NULL COMMENT '手机号',
  `code` varchar(6) DEFAULT NULL COMMENT '验证码',
  `type` tinyint(1) DEFAULT NULL COMMENT '短信类型',
  `isValid` tinyint(1) DEFAULT '1' COMMENT '是否有效',
  `createTime` datetime DEFAULT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='短信验证码';

-- ----------------------------
-- Records of app_sms_code
-- ----------------------------

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `num` int(11) DEFAULT NULL COMMENT '排序',
  `pid` int(11) DEFAULT NULL COMMENT '父部门id',
  `pids` varchar(255) DEFAULT NULL COMMENT '父级ids',
  `simplename` varchar(45) DEFAULT NULL COMMENT '简称',
  `fullname` varchar(255) DEFAULT NULL COMMENT '全称',
  `tips` varchar(255) DEFAULT NULL COMMENT '提示',
  `version` int(11) DEFAULT NULL COMMENT '版本（乐观锁保留字段）',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8 COMMENT='部门表';

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO `sys_dept` VALUES ('35', '1', '0', '[0],', '测试部', '测试开发部', '测试开发人员', null);
INSERT INTO `sys_dept` VALUES ('38', '222', '35', '[0],[35],', '111', '2222', '2222', null);

-- ----------------------------
-- Table structure for sys_dict
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict`;
CREATE TABLE `sys_dict` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `num` int(11) DEFAULT NULL COMMENT '排序',
  `pid` int(11) DEFAULT NULL COMMENT '父级字典',
  `name` varchar(255) DEFAULT NULL COMMENT '名称',
  `tips` varchar(255) DEFAULT NULL COMMENT '提示',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8 COMMENT='字典表';

-- ----------------------------
-- Records of sys_dict
-- ----------------------------
INSERT INTO `sys_dict` VALUES ('29', '0', '0', '性别', null);
INSERT INTO `sys_dict` VALUES ('30', '1', '29', '男', null);
INSERT INTO `sys_dict` VALUES ('31', '2', '29', '女', null);
INSERT INTO `sys_dict` VALUES ('35', '0', '0', '账号状态', null);
INSERT INTO `sys_dict` VALUES ('36', '1', '35', '启用', null);
INSERT INTO `sys_dict` VALUES ('37', '2', '35', '冻结', null);
INSERT INTO `sys_dict` VALUES ('38', '3', '35', '已删除', null);
INSERT INTO `sys_dict` VALUES ('39', '0', '0', '这是一个字典测试', null);
INSERT INTO `sys_dict` VALUES ('40', '1', '39', '测试1', null);
INSERT INTO `sys_dict` VALUES ('41', '2', '39', '测试2', null);
INSERT INTO `sys_dict` VALUES ('42', '0', '0', '测试', null);
INSERT INTO `sys_dict` VALUES ('43', '1', '42', '测试1', null);
INSERT INTO `sys_dict` VALUES ('44', '2', '42', '测试2', null);

-- ----------------------------
-- Table structure for sys_expense
-- ----------------------------
DROP TABLE IF EXISTS `sys_expense`;
CREATE TABLE `sys_expense` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `money` decimal(20,2) DEFAULT NULL COMMENT '报销金额',
  `desc` varchar(255) DEFAULT '' COMMENT '描述',
  `createtime` datetime DEFAULT CURRENT_TIMESTAMP,
  `state` int(11) DEFAULT NULL COMMENT '状态: 1.待提交  2:待审核   3.审核通过 4:驳回',
  `userid` int(11) DEFAULT NULL COMMENT '用户id',
  `processId` varchar(255) DEFAULT NULL COMMENT '流程定义id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='报销表';

-- ----------------------------
-- Records of sys_expense
-- ----------------------------
INSERT INTO `sys_expense` VALUES ('1', '1.00', '2', '2018-06-12 16:02:12', '1', '1', '2501');

-- ----------------------------
-- Table structure for sys_login_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_login_log`;
CREATE TABLE `sys_login_log` (
  `id` int(65) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `logname` varchar(255) DEFAULT NULL COMMENT '日志名称',
  `userid` int(65) DEFAULT NULL COMMENT '管理员id',
  `createtime` datetime DEFAULT NULL COMMENT '创建时间',
  `succeed` varchar(255) DEFAULT NULL COMMENT '是否执行成功',
  `message` text COMMENT '具体消息',
  `ip` varchar(255) DEFAULT NULL COMMENT '登录ip',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='登录记录';

-- ----------------------------
-- Records of sys_login_log
-- ----------------------------

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `code` varchar(255) DEFAULT NULL COMMENT '菜单编号',
  `pcode` varchar(255) DEFAULT NULL COMMENT '菜单父编号',
  `pcodes` varchar(255) DEFAULT NULL COMMENT '当前菜单的所有父菜单编号',
  `name` varchar(255) DEFAULT NULL COMMENT '菜单名称',
  `icon` varchar(255) DEFAULT NULL COMMENT '菜单图标',
  `url` varchar(255) DEFAULT NULL COMMENT 'url地址',
  `num` int(65) DEFAULT NULL COMMENT '菜单排序号',
  `levels` int(65) DEFAULT NULL COMMENT '菜单层级',
  `ismenu` int(11) DEFAULT NULL COMMENT '是否是菜单（1：是  0：不是）',
  `tips` varchar(255) DEFAULT NULL COMMENT '备注',
  `status` int(65) DEFAULT NULL COMMENT '菜单状态 :  1:启用   0:不启用',
  `isopen` int(11) DEFAULT NULL COMMENT '是否打开:    1:打开   0:不打开',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=214 DEFAULT CHARSET=utf8 COMMENT='菜单表';

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES ('105', 'system', '0', '[0],', '系统管理', 'fa-user', '#', '4', '1', '1', null, '1', '1');
INSERT INTO `sys_menu` VALUES ('106', 'mgr', 'system', '[0],[system],', '管理员管理', 'fa-list', '/mgr', '1', '2', '1', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('107', 'mgr_add', 'mgr', '[0],[system],[mgr],', '添加用户', null, '/mgr/add', '1', '3', '0', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('108', 'mgr_edit', 'mgr', '[0],[system],[mgr],', '修改用户', null, '/mgr/edit', '2', '3', '0', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('109', 'mgr_delete', 'mgr', '[0],[system],[mgr],', '删除用户', null, '/mgr/delete', '3', '3', '0', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('110', 'mgr_reset', 'mgr', '[0],[system],[mgr],', '重置密码', null, '/mgr/reset', '4', '3', '0', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('111', 'mgr_freeze', 'mgr', '[0],[system],[mgr],', '冻结用户', null, '/mgr/freeze', '5', '3', '0', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('112', 'mgr_unfreeze', 'mgr', '[0],[system],[mgr],', '解除冻结用户', null, '/mgr/unfreeze', '6', '3', '0', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('113', 'mgr_setRole', 'mgr', '[0],[system],[mgr],', '分配角色', null, '/mgr/setRole', '7', '3', '0', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('114', 'role', 'system', '[0],[system],', '角色管理', null, '/role', '2', '2', '1', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('115', 'role_add', 'role', '[0],[system],[role],', '添加角色', null, '/role/add', '1', '3', '0', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('116', 'role_edit', 'role', '[0],[system],[role],', '修改角色', null, '/role/edit', '2', '3', '0', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('117', 'role_remove', 'role', '[0],[system],[role],', '删除角色', null, '/role/remove', '3', '3', '0', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('118', 'role_setAuthority', 'role', '[0],[system],[role],', '配置权限', null, '/role/setAuthority', '4', '3', '0', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('119', 'menu', 'system', '[0],[system],', '菜单管理', null, '/menu', '4', '2', '1', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('120', 'menu_add', 'menu', '[0],[system],[menu],', '添加菜单', null, '/menu/add', '1', '3', '0', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('121', 'menu_edit', 'menu', '[0],[system],[menu],', '修改菜单', null, '/menu/edit', '2', '3', '0', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('122', 'menu_remove', 'menu', '[0],[system],[menu],', '删除菜单', null, '/menu/remove', '3', '3', '0', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('128', 'log', 'system', '[0],[system],', '业务日志', null, '/log', '6', '2', '1', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('130', 'druid', 'system', '[0],[system],', '监控管理', null, '/druid', '7', '2', '1', null, '1', null);
INSERT INTO `sys_menu` VALUES ('131', 'dept', 'system', '[0],[system],', '部门管理', null, '/dept', '3', '2', '1', null, '1', null);
INSERT INTO `sys_menu` VALUES ('132', 'dict', 'system', '[0],[system],', '字典管理', null, '/dict', '4', '2', '1', null, '1', null);
INSERT INTO `sys_menu` VALUES ('133', 'loginLog', 'system', '[0],[system],', '登录日志', null, '/loginLog', '6', '2', '1', null, '1', null);
INSERT INTO `sys_menu` VALUES ('134', 'log_clean', 'log', '[0],[system],[log],', '清空日志', null, '/log/delLog', '3', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('135', 'dept_add', 'dept', '[0],[system],[dept],', '添加部门', null, '/dept/add', '1', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('136', 'dept_update', 'dept', '[0],[system],[dept],', '修改部门', null, '/dept/update', '1', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('137', 'dept_delete', 'dept', '[0],[system],[dept],', '删除部门', null, '/dept/delete', '1', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('138', 'dict_add', 'dict', '[0],[system],[dict],', '添加字典', null, '/dict/add', '1', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('139', 'dict_update', 'dict', '[0],[system],[dict],', '修改字典', null, '/dict/update', '1', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('140', 'dict_delete', 'dict', '[0],[system],[dict],', '删除字典', null, '/dict/delete', '1', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('141', 'notice', 'system', '[0],[system],', '通知管理', null, '/notice', '9', '2', '1', null, '1', null);
INSERT INTO `sys_menu` VALUES ('142', 'notice_add', 'notice', '[0],[system],[notice],', '添加通知', null, '/notice/add', '1', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('143', 'notice_update', 'notice', '[0],[system],[notice],', '修改通知', null, '/notice/update', '2', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('144', 'notice_delete', 'notice', '[0],[system],[notice],', '删除通知', null, '/notice/delete', '3', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('145', 'hello', '0', '[0],', '通知', 'fa-rocket', '/notice/hello', '1', '1', '1', null, '1', null);
INSERT INTO `sys_menu` VALUES ('148', 'code', '0', '[0],', '代码生成', 'fa-code', '/code', '3', '1', '1', null, '1', null);
INSERT INTO `sys_menu` VALUES ('149', 'api_mgr', '0', '[0],', '接口文档', 'fa-leaf', '/swagger-ui.html', '2', '1', '1', null, '1', null);
INSERT INTO `sys_menu` VALUES ('150', 'to_menu_edit', 'menu', '[0],[system],[menu],', '菜单编辑跳转', 'fa-list', '/menu/menu_edit', '4', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('151', 'menu_list', 'menu', '[0],[system],[menu],', '菜单列表', 'fa-list', '/menu/list', '5', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('152', 'to_dept_update', 'dept', '[0],[system],[dept],', '修改部门跳转', 'fa-list', '/dept/dept_update', '4', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('153', 'dept_list', 'dept', '[0],[system],[dept],', '部门列表', 'fa-list', '/dept/list', '5', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('154', 'dept_detail', 'dept', '[0],[system],[dept],', '部门详情', 'fa-list', '/dept/detail', '6', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('155', 'to_dict_edit', 'dict', '[0],[system],[dict],', '修改菜单跳转', 'fa-list', '/dict/dict_edit', '4', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('156', 'dict_list', 'dict', '[0],[system],[dict],', '字典列表', 'fa-list', '/dict/list', '5', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('157', 'dict_detail', 'dict', '[0],[system],[dict],', '字典详情', 'fa-list', '/dict/detail', '6', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('158', 'log_list', 'log', '[0],[system],[log],', '日志列表', 'fa-list', '/log/list', '2', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('159', 'log_detail', 'log', '[0],[system],[log],', '日志详情', 'fa-list', '/log/detail', '3', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('160', 'del_login_log', 'loginLog', '[0],[system],[loginLog],', '清空登录日志', 'fa-list', '/loginLog/delLoginLog', '1', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('161', 'login_log_list', 'loginLog', '[0],[system],[loginLog],', '登录日志列表', 'fa-list', '/loginLog/list', '2', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('162', 'to_role_edit', 'role', '[0],[system],[role],', '修改角色跳转', 'fa-list', '/role/role_edit', '5', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('163', 'to_role_assign', 'role', '[0],[system],[role],', '角色分配跳转', 'fa-list', '/role/role_assign', '6', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('164', 'role_list', 'role', '[0],[system],[role],', '角色列表', 'fa-list', '/role/list', '7', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('165', 'to_assign_role', 'mgr', '[0],[system],[mgr],', '分配角色跳转', 'fa-list', '/mgr/role_assign', '8', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('166', 'to_user_edit', 'mgr', '[0],[system],[mgr],', '编辑用户跳转', 'fa-list', '/mgr/user_edit', '9', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('167', 'mgr_list', 'mgr', '[0],[system],[mgr],', '用户列表', 'fa-list', '/mgr/list', '10', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('168', 'expense', '0', '[0],', '报销管理', 'fa-clone', '#', '5', '1', '1', null, '1', null);
INSERT INTO `sys_menu` VALUES ('169', 'expense_fill', 'expense', '[0],[expense],', '报销申请', 'fa-list', '/expense', '1', '2', '1', null, '1', null);
INSERT INTO `sys_menu` VALUES ('170', 'expense_progress', 'expense', '[0],[expense],', '报销审批', 'fa-list', '/process', '2', '2', '1', null, '1', null);
INSERT INTO `sys_menu` VALUES ('171', 'setMgt', '0', '[0],', 'APP设置', 'fa-bars', '#', '200', '1', '1', null, '1', null);
INSERT INTO `sys_menu` VALUES ('172', 'setInfo_add', 'setInfo', '[0],[setMgt],[setInfo],', '添加设置', 'fa-list', '/setInfo/add', '202', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('173', 'setInfo_update', 'setInfo', '[0],[setMgt],[setInfo],', '编辑设置', 'fa-list', '/setInfo/update', '203', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('174', 'userInfo', 'userMgt', '[0],[userMgt],', '用户列表', 'fa-list', '/userInfo', '101', '2', '1', null, '1', null);
INSERT INTO `sys_menu` VALUES ('175', 'setInfo', 'setMgt', '[0],[setMgt],', 'APP设置', 'fa-list', '/setInfo', '201', '2', '1', null, '1', null);
INSERT INTO `sys_menu` VALUES ('176', 'userMgt', '0', '[0],', '用户管理', 'fa-bars', '#', '100', '1', '1', null, '1', null);
INSERT INTO `sys_menu` VALUES ('177', '/orderInfo', '0', '[0],', '订单管理', 'fa-bars', '#', '150', '1', '1', null, '1', null);
INSERT INTO `sys_menu` VALUES ('178', 'orderInfo_list', '/orderInfo', '[0],[/orderInfo],', '订单列表', 'fa-list', '/orderInfo', '151', '2', '1', null, '1', null);
INSERT INTO `sys_menu` VALUES ('179', 'noticeInfo', 'noticeInfoMgt', '[0],[noticeInfoMgt],', '公告列表', 'fa-list', '/noticeInfo', '181', '2', '1', null, '1', null);
INSERT INTO `sys_menu` VALUES ('180', 'noticeInfo_add', 'noticeInfo', '[0],[noticeInfo],', '发布公共', 'fa-list', '/noticeInfo/add', '182', '2', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('181', 'noticeInfo_update', 'noticeInfo', '[0],[noticeInfo],', '编辑公告', 'fa-list', '/noticeInfo/update', '183', '2', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('182', 'noticeInfo_delete', 'noticeInfo', '[0],[noticeInfo],', '删除公告', 'fa-list', '/noticeInfo/delete', '184', '2', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('183', 'noticeInfoMgt', '0', '[0],', '公告管理', 'fa-bars', '#', '180', '1', '1', null, '1', null);
INSERT INTO `sys_menu` VALUES ('184', 'userInfo_add', 'userInfo', '[0],[userMgt],[userInfo],', '添加用户', 'fa-list', '/userInfo/add', '100', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('185', 'userInfo_freeze', 'userInfo', '[0],[userMgt],[userInfo],', '冻结用户', 'fa-list', '/userInfo/freeze', '100', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('186', 'productInfoMgt', '0', '[0],', '产品管理', 'fa-bars', '#', '110', '1', '1', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('187', 'productInfo', 'productInfoMgt', '[0],[productInfoMgt],', '产品列表', 'fa-list', '/productInfo', '111', '2', '1', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('189', 'productInfo_add', 'productInfoMgt', '[0],[productInfoMgt],[productInfo],', '添加产品', 'fa-list', '/productInfo/add', '112', '3', '0', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('190', 'productInfo_update', 'productInfoMgt', '[0],[productInfoMgt],[productInfo],', '编辑产品', 'fa-list', '/productInfo/update', '113', '3', '0', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('191', 'productInfo_delete', 'productInfoMgt', '[0],[productInfoMgt],[productInfo],', '删除产品', 'fa-list', '/productInfo/delete', '114', '3', '0', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('192', 'productInfo_detail', 'productInfoMgt', '[0],[productInfoMgt],[productInfo],', '产品详情', 'fa-list', '/productInfo/detail', '115', '3', '0', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('193', 'bannerInfoMgt', '0', '[0],', 'Banner管理', 'fa-bars', '#', '170', '1', '1', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('194', 'bannerInfo', 'bannerInfoMgt', '[0],[bannerInfo],', 'Banner列表', 'fa-list', '/bannerInfo', '171', '2', '1', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('195', 'bannerInfo_add', 'bannerInfoMgt', '[0],[bannerInfo],[bannerInfo],', '添加Banner', 'fa-list', '/bannerInfo/add', '172', '3', '0', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('196', 'bannerInfo_update', 'bannerInfoMgt', '[0],[bannerInfo],[bannerInfo],', '编辑Banner', 'fa-list', '/bannerInfo/update', '173', '3', '0', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('197', 'bannerInfo_delete', 'bannerInfoMgt', '[0],[bannerInfo],[bannerInfo],', '删除Banner', 'fa-list', '/bannerInfo/delete', '174', '3', '0', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('198', 'bannerInfo_detail', 'bannerInfoMgt', '[0],[bannerInfo],[bannerInfo],', 'Banner详情', 'fa-list', '/bannerInfo/detail', '175', '3', '0', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('201', 'serviceProviderMgt', '0', '[0],', '服务商管理', 'fa-bars', '#', '160', '1', '1', null, '1', null);
INSERT INTO `sys_menu` VALUES ('202', 'serviceProvider_list', 'serviceProviderMgt', '[0],[serviceProviderMgt],', '服务商列表', '', '/serviceProvider', '161', '2', '1', null, '1', null);
INSERT INTO `sys_menu` VALUES ('203', 'serviceProvider_add', 'serviceProvider_list', '[0],[serviceProviderMgt],[/serviceProvider_list],', '服务商添加', '', '/serviceProvider/add', '162', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('204', 'serviceProvider_update', 'serviceProvider_list', '[0],[serviceProviderMgt],[/serviceProvider_list],', '编辑服务商', '', '/serviceProvider/update', '163', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('205', 'serviceProvider_delete', 'serviceProvider_list', '[0],[serviceProviderMgt],[/serviceProvider_list],', '删除服务商', '', '/serviceProvider/delete', '164', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('206', 'advertisingMgt', '0', '[0],', '广告设置', 'fa-bars', '#', '190', '1', '1', null, '1', null);
INSERT INTO `sys_menu` VALUES ('207', 'advertising_update', 'advertisingMgt', '[0],[advertisingMgt],', '广告设置', '', '/advertising/advertising_update/1', '191', '2', '1', null, '1', null);
INSERT INTO `sys_menu` VALUES ('208', 'userInfo_update', 'userInfo', '[0],[userMgt],[userInfo],', '编辑用户', '', '/userInfo/update', '100', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('209', 'setHtml', '0', '[0],', '相关介绍', 'fa-bars', '#', '210', '1', '1', null, '1', null);
INSERT INTO `sys_menu` VALUES ('210', 'setInfo_html', 'setHtml', '[0],[setHtml],', '相关介绍页', '', '/setInfo/html', '211', '2', '1', null, '1', null);

-- ----------------------------
-- Table structure for sys_notice
-- ----------------------------
DROP TABLE IF EXISTS `sys_notice`;
CREATE TABLE `sys_notice` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `title` varchar(255) DEFAULT NULL COMMENT '标题',
  `type` int(11) DEFAULT NULL COMMENT '类型',
  `content` text COMMENT '内容',
  `createtime` datetime DEFAULT NULL COMMENT '创建时间',
  `creater` int(11) DEFAULT NULL COMMENT '创建人',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='通知表';

-- ----------------------------
-- Records of sys_notice
-- ----------------------------
INSERT INTO `sys_notice` VALUES ('6', '世界', '10', '<h1>欢迎使用易盈宝管理系统..</h1><p><br></p>', '2017-01-11 08:53:20', '1');

-- ----------------------------
-- Table structure for sys_operation_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_operation_log`;
CREATE TABLE `sys_operation_log` (
  `id` int(65) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `logtype` varchar(255) DEFAULT NULL COMMENT '日志类型',
  `logname` varchar(255) DEFAULT NULL COMMENT '日志名称',
  `userid` int(65) DEFAULT NULL COMMENT '用户id',
  `classname` varchar(255) DEFAULT NULL COMMENT '类名称',
  `method` text COMMENT '方法名称',
  `createtime` datetime DEFAULT NULL COMMENT '创建时间',
  `succeed` varchar(255) DEFAULT NULL COMMENT '是否成功',
  `message` text COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='操作日志';

-- ----------------------------
-- Records of sys_operation_log
-- ----------------------------

-- ----------------------------
-- Table structure for sys_relation
-- ----------------------------
DROP TABLE IF EXISTS `sys_relation`;
CREATE TABLE `sys_relation` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `menuid` bigint(11) DEFAULT NULL COMMENT '菜单id',
  `roleid` int(11) DEFAULT NULL COMMENT '角色id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6392 DEFAULT CHARSET=utf8 COMMENT='角色和菜单关联表';

-- ----------------------------
-- Records of sys_relation
-- ----------------------------
INSERT INTO `sys_relation` VALUES ('6048', '105', '6');
INSERT INTO `sys_relation` VALUES ('6049', '106', '6');
INSERT INTO `sys_relation` VALUES ('6050', '167', '6');
INSERT INTO `sys_relation` VALUES ('6051', '131', '6');
INSERT INTO `sys_relation` VALUES ('6052', '153', '6');
INSERT INTO `sys_relation` VALUES ('6053', '171', '6');
INSERT INTO `sys_relation` VALUES ('6054', '175', '6');
INSERT INTO `sys_relation` VALUES ('6055', '172', '6');
INSERT INTO `sys_relation` VALUES ('6056', '173', '6');
INSERT INTO `sys_relation` VALUES ('6057', '176', '6');
INSERT INTO `sys_relation` VALUES ('6058', '174', '6');
INSERT INTO `sys_relation` VALUES ('6059', '185', '6');
INSERT INTO `sys_relation` VALUES ('6060', '177', '6');
INSERT INTO `sys_relation` VALUES ('6061', '178', '6');
INSERT INTO `sys_relation` VALUES ('6064', '183', '6');
INSERT INTO `sys_relation` VALUES ('6065', '179', '6');
INSERT INTO `sys_relation` VALUES ('6066', '180', '6');
INSERT INTO `sys_relation` VALUES ('6067', '181', '6');
INSERT INTO `sys_relation` VALUES ('6068', '182', '6');
INSERT INTO `sys_relation` VALUES ('6069', '186', '6');
INSERT INTO `sys_relation` VALUES ('6070', '187', '6');
INSERT INTO `sys_relation` VALUES ('6071', '189', '6');
INSERT INTO `sys_relation` VALUES ('6072', '190', '6');
INSERT INTO `sys_relation` VALUES ('6073', '191', '6');
INSERT INTO `sys_relation` VALUES ('6074', '192', '6');
INSERT INTO `sys_relation` VALUES ('6075', '193', '6');
INSERT INTO `sys_relation` VALUES ('6076', '194', '6');
INSERT INTO `sys_relation` VALUES ('6077', '195', '6');
INSERT INTO `sys_relation` VALUES ('6078', '196', '6');
INSERT INTO `sys_relation` VALUES ('6079', '197', '6');
INSERT INTO `sys_relation` VALUES ('6080', '198', '6');
INSERT INTO `sys_relation` VALUES ('6081', '201', '6');
INSERT INTO `sys_relation` VALUES ('6082', '202', '6');
INSERT INTO `sys_relation` VALUES ('6083', '204', '6');
INSERT INTO `sys_relation` VALUES ('6084', '206', '6');
INSERT INTO `sys_relation` VALUES ('6085', '207', '6');
INSERT INTO `sys_relation` VALUES ('6086', '209', '6');
INSERT INTO `sys_relation` VALUES ('6087', '210', '6');
INSERT INTO `sys_relation` VALUES ('6316', '105', '1');
INSERT INTO `sys_relation` VALUES ('6317', '106', '1');
INSERT INTO `sys_relation` VALUES ('6318', '107', '1');
INSERT INTO `sys_relation` VALUES ('6319', '108', '1');
INSERT INTO `sys_relation` VALUES ('6320', '109', '1');
INSERT INTO `sys_relation` VALUES ('6321', '110', '1');
INSERT INTO `sys_relation` VALUES ('6322', '111', '1');
INSERT INTO `sys_relation` VALUES ('6323', '112', '1');
INSERT INTO `sys_relation` VALUES ('6324', '113', '1');
INSERT INTO `sys_relation` VALUES ('6325', '165', '1');
INSERT INTO `sys_relation` VALUES ('6326', '166', '1');
INSERT INTO `sys_relation` VALUES ('6327', '167', '1');
INSERT INTO `sys_relation` VALUES ('6328', '114', '1');
INSERT INTO `sys_relation` VALUES ('6329', '115', '1');
INSERT INTO `sys_relation` VALUES ('6330', '116', '1');
INSERT INTO `sys_relation` VALUES ('6331', '117', '1');
INSERT INTO `sys_relation` VALUES ('6332', '118', '1');
INSERT INTO `sys_relation` VALUES ('6333', '162', '1');
INSERT INTO `sys_relation` VALUES ('6334', '163', '1');
INSERT INTO `sys_relation` VALUES ('6335', '164', '1');
INSERT INTO `sys_relation` VALUES ('6336', '119', '1');
INSERT INTO `sys_relation` VALUES ('6337', '120', '1');
INSERT INTO `sys_relation` VALUES ('6338', '121', '1');
INSERT INTO `sys_relation` VALUES ('6339', '122', '1');
INSERT INTO `sys_relation` VALUES ('6340', '150', '1');
INSERT INTO `sys_relation` VALUES ('6341', '151', '1');
INSERT INTO `sys_relation` VALUES ('6342', '131', '1');
INSERT INTO `sys_relation` VALUES ('6343', '135', '1');
INSERT INTO `sys_relation` VALUES ('6344', '136', '1');
INSERT INTO `sys_relation` VALUES ('6345', '137', '1');
INSERT INTO `sys_relation` VALUES ('6346', '152', '1');
INSERT INTO `sys_relation` VALUES ('6347', '153', '1');
INSERT INTO `sys_relation` VALUES ('6348', '154', '1');
INSERT INTO `sys_relation` VALUES ('6349', '148', '1');
INSERT INTO `sys_relation` VALUES ('6350', '171', '1');
INSERT INTO `sys_relation` VALUES ('6351', '175', '1');
INSERT INTO `sys_relation` VALUES ('6352', '172', '1');
INSERT INTO `sys_relation` VALUES ('6353', '173', '1');
INSERT INTO `sys_relation` VALUES ('6354', '176', '1');
INSERT INTO `sys_relation` VALUES ('6355', '174', '1');
INSERT INTO `sys_relation` VALUES ('6356', '184', '1');
INSERT INTO `sys_relation` VALUES ('6357', '185', '1');
INSERT INTO `sys_relation` VALUES ('6358', '208', '1');
INSERT INTO `sys_relation` VALUES ('6359', '177', '1');
INSERT INTO `sys_relation` VALUES ('6360', '178', '1');
INSERT INTO `sys_relation` VALUES ('6363', '183', '1');
INSERT INTO `sys_relation` VALUES ('6364', '179', '1');
INSERT INTO `sys_relation` VALUES ('6365', '180', '1');
INSERT INTO `sys_relation` VALUES ('6366', '181', '1');
INSERT INTO `sys_relation` VALUES ('6367', '182', '1');
INSERT INTO `sys_relation` VALUES ('6368', '186', '1');
INSERT INTO `sys_relation` VALUES ('6369', '187', '1');
INSERT INTO `sys_relation` VALUES ('6370', '189', '1');
INSERT INTO `sys_relation` VALUES ('6371', '190', '1');
INSERT INTO `sys_relation` VALUES ('6372', '191', '1');
INSERT INTO `sys_relation` VALUES ('6373', '192', '1');
INSERT INTO `sys_relation` VALUES ('6374', '193', '1');
INSERT INTO `sys_relation` VALUES ('6375', '194', '1');
INSERT INTO `sys_relation` VALUES ('6376', '195', '1');
INSERT INTO `sys_relation` VALUES ('6377', '196', '1');
INSERT INTO `sys_relation` VALUES ('6378', '197', '1');
INSERT INTO `sys_relation` VALUES ('6379', '198', '1');
INSERT INTO `sys_relation` VALUES ('6380', '201', '1');
INSERT INTO `sys_relation` VALUES ('6381', '202', '1');
INSERT INTO `sys_relation` VALUES ('6382', '203', '1');
INSERT INTO `sys_relation` VALUES ('6383', '204', '1');
INSERT INTO `sys_relation` VALUES ('6384', '205', '1');
INSERT INTO `sys_relation` VALUES ('6385', '206', '1');
INSERT INTO `sys_relation` VALUES ('6386', '207', '1');
INSERT INTO `sys_relation` VALUES ('6387', '209', '1');
INSERT INTO `sys_relation` VALUES ('6388', '210', '1');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `num` int(11) DEFAULT NULL COMMENT '序号',
  `pid` int(11) DEFAULT NULL COMMENT '父角色id',
  `name` varchar(255) DEFAULT NULL COMMENT '角色名称',
  `deptid` int(11) DEFAULT NULL COMMENT '部门名称',
  `tips` varchar(255) DEFAULT NULL COMMENT '提示',
  `version` int(11) DEFAULT NULL COMMENT '保留字段(暂时没用）',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='角色表';

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES ('1', '1', '0', '超级管理员', '24', 'administrator', '1');
INSERT INTO `sys_role` VALUES ('6', '1', '1', '管理员', '35', '测试开发', null);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `avatar` varchar(255) DEFAULT NULL COMMENT '头像',
  `account` varchar(45) DEFAULT NULL COMMENT '账号',
  `password` varchar(45) DEFAULT NULL COMMENT '密码',
  `salt` varchar(45) DEFAULT NULL COMMENT 'md5密码盐',
  `name` varchar(45) DEFAULT NULL COMMENT '名字',
  `birthday` datetime DEFAULT NULL COMMENT '生日',
  `sex` int(11) DEFAULT NULL COMMENT '性别（1：男 2：女）',
  `email` varchar(45) DEFAULT NULL COMMENT '电子邮件',
  `phone` varchar(45) DEFAULT NULL COMMENT '电话',
  `roleid` varchar(255) DEFAULT NULL COMMENT '角色id',
  `deptid` int(11) DEFAULT NULL COMMENT '部门id',
  `status` int(11) DEFAULT NULL COMMENT '状态(1：启用  2：冻结  3：删除）',
  `createtime` datetime DEFAULT NULL COMMENT '创建时间',
  `version` int(11) DEFAULT NULL COMMENT '保留字段',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8 COMMENT='管理员表';

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('1', null, 'admin', 'ed5ca684e4648271a6a0150e2945e3eb', '8pgby', '系统管理', '2017-05-05 00:00:00', '1', 'test@qq.com', '18800000000', '1', '0', '1', '2016-01-29 08:49:53', '25');
INSERT INTO `sys_user` VALUES ('44', null, 'test1', '45abb7879f6a8268f1ef600e6038ac73', 'ssts3', 'test1', '2017-05-01 00:00:00', '1', 'abc@123.com', '', '5', '26', '3', '2017-05-16 20:33:37', null);
INSERT INTO `sys_user` VALUES ('45', null, 'boss', '71887a5ad666a18f709e1d4e693d5a35', '1f7bf', '老板', '2017-12-04 00:00:00', '1', '', '', '1', '24', '3', '2017-12-04 22:24:02', null);
INSERT INTO `sys_user` VALUES ('46', null, 'manager', 'b53cac62e7175637d4beb3b16b2f7915', 'j3cs9', '经理', '2017-12-04 00:00:00', '1', '', '', '1', '24', '3', '2017-12-04 22:24:24', null);
INSERT INTO `sys_user` VALUES ('47', null, 'test', '2a825d00101e6dd018d9ccc52244d216', 'qiopf', '张三', '2018-05-10 00:00:00', '1', 'test@qq.com', '18800000000', '6', '35', '1', '2018-05-02 16:04:09', null);
