@/*
    上传文件参数的说明:
    id : 文件的id
@*/
<div class="form-group">
    <div class="col-sm-4">
        <div id="${id}PreId">
            <div>
                @if(isEmpty(fileImg)){
                    <img width="100px" height="100px" src="${ctxPath}/static/img/NoPIC.png">
                @}else{
                 <div id="demoCanvas" style="height: 100px;width: 100px" loops="0" clearsAfterStop="true"></div>
                @}
            </div>
        </div>
    </div>
    <div class="col-sm-2">
        <div class="head-scu-btn upload-btn" id="${id}BtnId">
            <i class="fa fa-upload"></i>&nbsp;上传
        </div>
    </div>
    <input type="hidden" id="${id}" value="${fileImg!}"/>
</div>
@if(isNotEmpty(underline) && underline == 'true'){
    <div class="hr-line-dashed"></div>
@}