/**
 * 初始化Android版本控制详情对话框
 */
var AppAndroidVersionsInfoDlg = {
    appAndroidVersionsInfoData : {},
    validateFields: {
        versions: {
            validators: {
                notEmpty: {
                    message: '版本号不能为空'
                }
            }
        },
        url: {
            validators: {
                notEmpty: {
                    message: '版本链接不能为空'
                }
            }
        }
    }
};
/**
 * 验证数据是否为空
 */
AppAndroidVersionsInfoDlg.validate = function () {
    $('#horizontalForm').data("bootstrapValidator").resetForm();
    $('#horizontalForm').bootstrapValidator('validate');
    return $("#horizontalForm").data('bootstrapValidator').isValid();
};
/**
 * 清除数据
 */
AppAndroidVersionsInfoDlg.clearData = function() {
    this.appAndroidVersionsInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
AppAndroidVersionsInfoDlg.set = function(key, val) {
    this.appAndroidVersionsInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
AppAndroidVersionsInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
AppAndroidVersionsInfoDlg.close = function() {
    parent.layer.close(window.parent.AppAndroidVersions.layerIndex);
}

/**
 * 收集数据
 */
AppAndroidVersionsInfoDlg.collectData = function() {
    this.appAndroidVersionsInfoData['state'] = $(":radio[name='state']:checked").val();
    this
    .set('id')
    .set('createDate')
    .set('versions')
    .set('mark')
    .set('url');
}

/**
 * 提交添加
 */
AppAndroidVersionsInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();
    if(!this.validate()){
        return;
    }


    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/appAndroidVersions/add", function(data){
        Feng.success("添加成功!");
        window.parent.AppAndroidVersions.table.refresh();
        AppAndroidVersionsInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.appAndroidVersionsInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
AppAndroidVersionsInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();
    if(!this.validate()){
        return;
    }
    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/appAndroidVersions/update", function(data){
        Feng.success("修改成功!");
        window.parent.AppAndroidVersions.table.refresh();
        AppAndroidVersionsInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.appAndroidVersionsInfoData);
    ajax.start();
}

$(function() {
    Feng.initValidator("horizontalForm", AppAndroidVersionsInfoDlg.validateFields);
});
