/**
 * 入会与退会记录初始化。
 */
var AppGuildHistory = {
    id: "appGuildHistoryTable",	// 表格id
    seItem: null,			// 选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
AppGuildHistory.initColumn = function () {
    return [
        {field: 'selectItem', radio: false},
        	{title: 'ID', field: 'id', visible: false, align: 'center', valign: 'middle'},
        	{title: '优优ID', field: 'usercoding', visible: true, align: 'center', valign: 'middle'},
            {title: '昵称', field: 'nickname', visible: true, align: 'center', valign: 'middle'},
//            {title: '操作人昵称', field: 'nickname', visible: true, align: 'center', valign: 'middle'},
//            {title: '操作人编号', field: '正常', visible: true, align: 'center', valign: 'middle'},
//            {title: '事件类型', field: '正常', visible: true, align: 'center', valign: 'middle'},
            {title: '事件时间', field: 'createDate', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
AppGuildHistory.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        AppGuildHistory.seItem = selected;
        return true;
    }
};

/**
 * 查询入会与退会记录列表
 */
AppGuildHistory.search = function () {
    var queryData = {};
    queryData['usercoding'] = $("#usercoding").val();
    queryData['nickname'] = $("#nickname").val();
    AppGuildHistory.table.refresh({query: queryData});
};

/**
 * 重置
 */
AppGuildHistory.resetSearch = function () {
    $("#usercoding").val("");
    $("#nickname").val("");
    AppGuildHistory.search();
};

$(function () {
    var defaultColunms = AppGuildHistory.initColumn();
    var table = new BSTable(AppGuildHistory.id, "/appGuildHistory/list", defaultColunms);
    table.setPaginationType("server");
    AppGuildHistory.table = table.init();
});
