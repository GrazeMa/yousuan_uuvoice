/**
 * 房间黑名单管理初始化
 */
var AppBlock = {
    id: "AppBlockTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
AppBlock.initColumn = function () {
    return [
        {field: 'selectItem', radio: true,visible:false},
            {title: '', field: 'id', visible: false, align: 'center', valign: 'middle'},
            {title: '设置时间', field: 'createTime', visible: true, align: 'center', valign: 'middle'},
            {title: '优优ID', field: 'eid', visible: true, align: 'center', valign: 'middle'},
            {title: '昵称', field: 'name', visible: true, align: 'center', valign: 'middle'},
            {title: '性别', field: 'sex', visible: true, align: 'center', valign: 'middle',
                formatter: function (value, row) {
                    if(value==1){
                        return "男";
                    }else{
                        return "女"
                    }
                }
            },
            {title: '设置人', field: 'state', visible: true, align: 'center', valign: 'middle',
                formatter: function (value, row) {
                    //1房主，2是管理员
                    if(value==1){
                        return "房主："+row.fgName;
                    }
                }
            }
    ];
};

$(function () {
    var defaultColunms = AppBlock.initColumn();
    var rid=$("#rid").val();
    var table = new BSTable(AppBlock.id, "/appBlock/list?rid="+rid, defaultColunms);
    table.setPaginationType("client");
    AppBlock.table = table.init();
});
