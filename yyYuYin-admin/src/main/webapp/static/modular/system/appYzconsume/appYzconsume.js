/**
 * 优钻获取管理初始化
 */
var AppYzconsume = {
    id: "AppYzconsumeTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
AppYzconsume.initColumn = function () {
    return [
        {field: 'selectItem', radio: true,visible:false},
        {title: '', field: 'id', visible: false, align: 'center', valign: 'middle'},
        {title: '获取时间', field: 'createTime', visible: true, align: 'center', valign: 'middle'},
        {title: '获取用户', field: 'name', visible: true, align: 'center', valign: 'middle'},
        {title: '优优ID', field: 'eid', visible: true, align: 'center', valign: 'middle'},
        {title: '获取类型', field: 'state', visible: true, align: 'center', valign: 'middle',
            formatter: function (value, row) {
                //1是赠送礼物，2是购买花环，3购买座驾，4 发红包
                if(value==1){
                    return "收到礼物"+"-"+row.lwName
                }else if(value==2){
                    return "购买花环"+"-"+row.lwName
                }else if(value==3){
                    return "购买座驾"+"-"+row.lwName
                }else {
                    return "发红包"
                }
            }
        },
        {title: '获取数量', field: 'consumeNum', visible: true, align: 'center', valign: 'middle'},
        {title: '获取优钻数', field: 'consumeYzNum', visible: true, align: 'center', valign: 'middle'}

    ];
};

//导出
AppYzconsume.export = function () {
    var beginTime= $("#beginTime").val();
    var endTime = $("#endTime").val();
    var eid = $("#eid").val();
    var name = $("#name").val();
    window.location.href=Feng.ctxPath + "/export/appYzconsume?beginTime="+beginTime+"&endTime="+endTime+"&eid="+eid+"&name="+name;

};

/**
 * 查询优币充值列表
 */
AppYzconsume.search = function () {
    var queryData = {};
    queryData['beginTime'] = $("#beginTime").val();
    queryData['endTime'] = $("#endTime").val();
    queryData['eid'] = $("#eid").val();
    queryData['name'] = $("#name").val();
    AppYzconsume.table.refresh({query: queryData});
};
/**
 * 重置
 */
AppYzconsume.resetSearch = function () {
    $("#beginTime").val("");
    $("#endTime").val("");
    $("#eid").val("");
    $("#name").val("");
    AppYzconsume.search();
};

$(function () {
    var defaultColunms = AppYzconsume.initColumn();
    var table = new BSTable(AppYzconsume.id, "/appYzconsume/list", defaultColunms);
    table.setPaginationType("client");
    AppYzconsume.table = table.init();
});
