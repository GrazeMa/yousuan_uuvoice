/**
 * 初始化操作日志详情对话框
 */
var AppRecordInfoDlg = {
    appRecordInfoData : {}
};

/**
 * 清除数据
 */
AppRecordInfoDlg.clearData = function() {
    this.appRecordInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
AppRecordInfoDlg.set = function(key, val) {
    this.appRecordInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
AppRecordInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
AppRecordInfoDlg.close = function() {
    parent.layer.close(window.parent.AppRecord.layerIndex);
}

/**
 * 收集数据
 */
AppRecordInfoDlg.collectData = function() {
    this
    .set('id')
    .set('createTime')
    .set('name')
    .set('hand')
    .set('isDelete');
}

/**
 * 提交添加
 */
AppRecordInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/appRecord/add", function(data){
        Feng.success("添加成功!");
        window.parent.AppRecord.table.refresh();
        AppRecordInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.appRecordInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
AppRecordInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/appRecord/update", function(data){
        Feng.success("修改成功!");
        window.parent.AppRecord.table.refresh();
        AppRecordInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.appRecordInfoData);
    ajax.start();
}

$(function() {

});
