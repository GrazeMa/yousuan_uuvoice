/**
 * 操作日志管理初始化
 */
var AppRecord = {
    id: "AppRecordTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
AppRecord.initColumn = function () {
    return [
        {field: 'selectItem', radio: true,visible:false},
            {title: '', field: 'id', visible: false, align: 'center', valign: 'middle'},
            {title: '操作时间', field: 'createTime', visible: true, align: 'center', valign: 'middle'},
            {title: '操作人', field: 'name', visible: true, align: 'center', valign: 'middle'},
            {title: '操作记录', field: 'hand', visible: true, align: 'center', valign: 'middle'},
    ];
};

/**
 * 检查是否选中
 */
AppRecord.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        AppRecord.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加操作日志
 */
AppRecord.openAddAppRecord = function () {
    var index = layer.open({
        type: 2,
        title: '添加操作日志',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/appRecord/appRecord_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看操作日志详情
 */
AppRecord.openAppRecordDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '操作日志详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/appRecord/appRecord_update/' + AppRecord.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除操作日志
 */
AppRecord.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/appRecord/delete", function (data) {
            Feng.success("删除成功!");
            AppRecord.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("appRecordId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询操作日志列表
 */
AppRecord.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    AppRecord.table.refresh({query: queryData});
};

$(function () {
    var pid=$("#pid").val();
    var type=$("#type").val();
    var defaultColunms = AppRecord.initColumn();
    var table = new BSTable(AppRecord.id, "/appRecord/list?pid="+pid+"&type="+type, defaultColunms);
    table.setPaginationType("client");
    AppRecord.table = table.init();
});
