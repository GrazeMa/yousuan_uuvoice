/**
 * 提现管理初始化
 */
var AppWithdraw = {
    id: "AppWithdrawTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
AppWithdraw.initColumn = function () {
    return [
        {field: 'selectItem', radio: true,visible:false},
        {title: '', field: 'id', visible: false, align: 'center', valign: 'middle'},
        {title: '申请时间', field: 'createTime', visible: true, align: 'center', valign: 'middle'},
        {title: '申请用户', field: 'name', visible: true, align: 'center', valign: 'middle'},
        {title: '优优ID', field: 'eid', visible: true, align: 'center', valign: 'middle'},
        {title: '提现来源', field: 'state', visible: true, align: 'center', valign: 'middle',
            formatter: function (value, row) {
                //1钻石 2分成
                if(value==1){
                    return "钻石"
                }else if(value==2){
                    return "分成"
                }
            }
        },
        {title: '提现金额', field: 'money', visible: true, align: 'center', valign: 'middle'},
        {title: '历史提现总金额', field: 'historyMoney_', visible: true, align: 'center', valign: 'middle'},
        {title: '提现帐号', field: 'witName', visible: true, align: 'center', valign: 'middle',
            formatter: function (value, row) {
                return "<p>"+value+"</p><p>"+row.witPhone+"</p>"
            }
        },
        {title: '交易流水号', field: 'serialNum', visible: true, align: 'center', valign: 'middle'},
        {title: '状态', field: 'status', visible: true, align: 'center', valign: 'middle',
            formatter: function (value, row) {
                //1 是待审核， 2已提现， 3已拒绝
                if (value == 1) {
                    return "待审核";
                } else if (value == 2) {
                    return "已提现";
                }else {
                    return "已拒绝";
                }
            }
        },
        {title: '操作', field: 'opt', visible: true, align: 'left', valign: 'middle',
            formatter: function (value, row) {
                var btn=[];
                if(row.status==1){//1 是待审核， 2已提现， 3已拒绝
                    btn[0]=['<a href="javascript:void(0);" onclick="AppWithdraw.openAppWithdrawDetail('+row.id+')">立即处理</a>'];
                    return btn;
                }else{
                    return "不可操作";
                }


            }
        }
    ];
};

/**
 * 立即处理
 */
AppWithdraw.openAppWithdrawDetail = function (id) {
    var index = layer.open({
        type: 2,
        title: '立即处理',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/appWithdraw/appWithdraw_update/' + id
    });
    this.layerIndex = index;
};


//导出
AppWithdraw.export = function () {
    var beginTime= $("#beginTime").val();
    var endTime = $("#endTime").val();
    var eid = $("#eid").val();
    var name = $("#name").val();
    window.location.href=Feng.ctxPath + "/export/appWithdraw?beginTime="+beginTime+"&endTime="+endTime+"&eid="+eid+"&name="+name;

};

/**
 * 查询优币充值列表
 */
AppWithdraw.search = function () {
    var queryData = {};
    queryData['beginTime'] = $("#beginTime").val();
    queryData['endTime'] = $("#endTime").val();
    queryData['eid'] = $("#eid").val();
    queryData['name'] = $("#name").val();
    AppWithdraw.table.refresh({query: queryData});
};
/**
 * 重置
 */
AppWithdraw.resetSearch = function () {
    $("#beginTime").val("");
    $("#endTime").val("");
    $("#eid").val("");
    $("#name").val("");
    AppWithdraw.search();
};


$(function () {
    var defaultColunms = AppWithdraw.initColumn();
    var table = new BSTable(AppWithdraw.id, "/appWithdraw/list", defaultColunms);
    table.setPaginationType("client");
    AppWithdraw.table = table.init();
});
