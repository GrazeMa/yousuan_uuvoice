/**
 * 初始化实名认证管理详情对话框
 */
var AppWithdrawInfoDlg = {
    appWithdrawInfoData : {}
};

/**
 * 清除数据
 */
AppWithdrawInfoDlg.clearData = function() {
    this.appWithdrawInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
AppWithdrawInfoDlg.set = function(key, val) {
    this.appWithdrawInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
AppWithdrawInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
AppWithdrawInfoDlg.close = function() {
    parent.layer.close(window.parent.AppWithdraw.layerIndex);
}

/**
 * 收集数据
 */
AppWithdrawInfoDlg.collectData = function() {
    this
    .set('id')
    .set('createTime')
    .set('eid')
    .set('name')
    .set('xm')
    .set('card')
    .set('mark')
    .set('isDelete');
}

/**
 * 提交修改
 */
AppWithdrawInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/appWithdraw/update", function(data){
        Feng.success("操作成功!");
        window.parent.AppWithdraw.table.refresh();
        AppWithdrawInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    var status=$(":radio[name='status']:checked").val();
    ajax.set("id",$("#id").val());
    ajax.set("serialNum",$("#serialNum").val());
    ajax.set("money",$("#money").text());
    ajax.set("status",status);
    ajax.start();
}

$(function() {
    /*分配协议号  设置分配时间*/
    $("input:radio[name='status']").on('click', function(event){
        if($(this).val()==2){
            $("#serialNumDiv").show();
        }else{
            $("#serialNumDiv").hide();
        }
    });
});
