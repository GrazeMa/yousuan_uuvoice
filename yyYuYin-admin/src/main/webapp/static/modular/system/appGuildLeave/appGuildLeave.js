/**
 * 退会申请初始化。
 */
var AppGuildLeave = {
    id: "appGuildLeaveTable",	// 表格id
    seItem: null,			// 选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
AppGuildLeave.initColumn = function () {
    return [
        {field: 'selectItem', radio: false},
        	{title: 'ID', field: 'id', visible: false, align: 'center', valign: 'middle'},
        	{title: '优优ID', field: 'usercoding', visible: true, align: 'center', valign: 'middle'},
            {title: '昵称', field: 'nickname', visible: true, align: 'center', valign: 'middle'},
            {title: '性别', field: 'sex', visible: true, align: 'center', valign: 'middle',
            	formatter: function (value, row) {
            		// 性别：(1：男；2：女)。
            		if(value == 1) {
            			return "男";
            		} else if (value == 2) {
            			return "女";
            		}
            	}
            },
//            {title: '操作人昵称', field: 'nickname', visible: true, align: 'center', valign: 'middle'},
//            {title: '操作人编号', field: '正常', visible: true, align: 'center', valign: 'middle'},
//            {title: '事件类型', field: '正常', visible: true, align: 'center', valign: 'middle'},
            {title: '事件时间', field: 'createDate', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
AppGuildLeave.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        AppGuildLeave.seItem = selected;
        return true;
    }
};

/**
 * 查询退会申请列表
 */
AppGuildLeave.search = function () {
    var queryData = {};
    queryData['usercoding'] = $("#usercoding").val();
    queryData['nickname'] = $("#nickname").val();
    AppGuildLeave.table.refresh({query: queryData});
};

/**
 * 重置
 */
AppGuildLeave.resetSearch = function () {
    $("#usercoding").val("");
    $("#nickname").val("");
    AppGuildLeave.search();
};

$(function () {
    var defaultColunms = AppGuildLeave.initColumn();
    var table = new BSTable(AppGuildLeave.id, "/appGuildLeave/list", defaultColunms);
    table.setPaginationType("server");
    AppGuildLeave.table = table.init();
});
