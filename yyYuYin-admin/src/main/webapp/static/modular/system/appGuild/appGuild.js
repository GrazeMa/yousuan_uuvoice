/**
 * 公会管理初始化。
 */
var AppGuild = {
    id: "appGuildTable",	// 表格id
    seItem: null,			// 选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
AppGuild.initColumn = function () {
    return [
        {field: 'selectItem', radio: false},
        	{title: 'ID', field: 'id', visible: false, align: 'center', valign: 'middle'},
        	{title: '分组信息', field: 'groupName', visible: true, align: 'center', valign: 'middle'},
        	{title: '优优ID', field: 'usercoding', visible: true, align: 'center', valign: 'middle'},
        	{title: '手机号码', field: 'phone', visible: true, align: 'center', valign: 'middle'},
            {title: '昵称', field: 'nickname', visible: true, align: 'center', valign: 'middle'},
            {title: '性别', field: 'sex', visible: true, align: 'center', valign: 'middle',
            	formatter: function (value, row) {
            		// 性别：(1：男；2：女)。
            		if(value == 1) {
            			return "男";
            		} else if (value == 2) {
            			return "女";
            		}
            	}
            },
//            {title: '身份', field: 'nickname', visible: true, align: 'center', valign: 'middle'},
//            {title: '状态', field: '正常', visible: true, align: 'center', valign: 'middle'},
//            {title: '订单抽成', field: '0%', visible: true, align: 'center', valign: 'middle'},
//            {title: '礼物抽成', field: '0%', visible: true, align: 'center', valign: 'middle'},
//            {title: '注册时间', field: 'createDate', visible: true, align: 'center', valign: 'middle'},
            {title: '入会时间', field: 'createDate', visible: true, align: 'center', valign: 'middle'},
            {title: '操作', field: 'Operation', visible: true, align: 'center', valign: 'middle',
            	formatter: function (value, row) {
            		var btn = [];
                    btn += '<p style="line-height: 10px;"><a href="javascript:void(0);" onclick="AppGuild.modifyStatus(' + row.id + ')">踢出公会</a></p>';
                    btn += '<p style="line-height: 10px;"><a href="javascript:void(0);" onclick="AppGuild.setUpGroup(' + row.id + ')">设置分组</a></p>';
                    return btn;
                }
            }

//        {title: '操作', field: 'opt', visible: true, align: 'left', valign: 'middle',
//            formatter: function (value, row) {
//                var btn=[];
//                if(row.status==1){//1  未处理, 2 未处理
//                    btn[0]=['<a href="javascript:void(0);" onclick="AppFeedback.audit('+row.id+')">立即处理</a>'];
//                }else{
//                    btn[0]=['<a href="javascript:void(0);" onclick="AppFeedback.delete_('+row.id+')">删除</a>'];
//                }
//                return btn;
//
//            }
//        }

    ];
};

/**
 * 检查是否选中
 */
AppGuild.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        AppGuild.seItem = selected;
        return true;
    }
};

/**
 * 批量添加。
 */
AppGuild.batchAdd = function () {
//    alert("ba");
};

/**
 * 踢出公会。
 * @param id
 * @private
 */
AppGuild.modifyStatus = function (id) {
	var confirm = layer.confirm("确定要将此成员踢出公会？", {
		btn : [ '确定', '取消' ]
	}, function() {
		layer.close(confirm);
		var ajax = new $ax(Feng.ctxPath + "/appGuild/delete",
				function(data) {
					Feng.success("删除成功!");
					AppGuild.table.refresh();
				}, function(data) {
					Feng.error("删除失败!" + data.responseJSON.message + "!");
				});
		ajax.set("appGuildId", id);
		ajax.start();
	});
};

/**
 * 设置分组。
 */
AppGuild.setUpGroup = function (id) {
	var index = layer.open({
        type: 2,
        title: '设置分组',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/appGuild/appGuild_update/' + id
    });
    this.layerIndex = index;
};

/**
 * 查询反馈管理列表
 */
AppGuild.search = function () {
    var queryData = {};
    queryData['usercoding'] = $("#usercoding").val();
    queryData['phone'] = $("#phone").val();
    queryData['nickname'] = $("#nickname").val();
    AppGuild.table.refresh({query: queryData});
};

/**
 * 重置
 */
AppGuild.resetSearch = function () {
    $("#usercoding").val("");
    $("#phone").val("");
    $("#nickname").val("");
    AppGuild.search();
};

$(function () {
    var defaultColunms = AppGuild.initColumn();
    var table = new BSTable(AppGuild.id, "/appGuild/list", defaultColunms);
    table.setPaginationType("server");
    AppGuild.table = table.init();
});
