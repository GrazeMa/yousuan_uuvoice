/**
 * 初始化公会管理详情对话框
 */
var AppGuildInfoDlg = {
    appGuildInfoData : {}
};

/**
 * 清除数据
 */
AppGuildInfoDlg.clearData = function() {
    this.appGuildInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
AppGuildInfoDlg.set = function(key, val) {
    this.appGuildInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
AppGuildInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 下拉控制
 * @param e
 */
AppGuildInfoDlg.selectClick=function(e){
    var type=$(e).val();
}

/**
 * 关闭此对话框
 */
AppGuildInfoDlg.close = function() {
	parent.layer.close(window.parent.AppGuild.layerIndex);
}

/**
 * 收集数据
 */
AppGuildInfoDlg.collectData = function() {
    this
    .set('id')
    .set('gid')
//    .set('content')
//    .set('isDelete')
//    .set('createTime')
//    .set('hand')
//    .set('role')
//    .set('status');
}

/**
 * 设置分组。
 */
AppGuildInfoDlg.editSubmit = function(id) {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/appGuild/update", function(data){
        Feng.success("修改成功!");
        window.parent.AppGuild.table.refresh();
        AppGuildInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.appGuildInfoData);
//    ajax.set("status",2);
    ajax.start();
}

$(function() {
	
	// 提交信息
	var ajax = new $ax(Feng.ctxPath + "/appGuild/chooseGuild", function(data) {
		var options = '';
		options += '<option value="' + '0' + '">' + '未分组' + '</option>';
		for (var i = 0; i < data.length; i++) {
			options += '<option value="' + data[i].id + '">' + data[i].groupName + '</option>';
		}
		$("#gid").html(options);
	}, function(data) {
		Feng.error("失败!" + data.responseJSON.message + "!");
	});
	ajax.start();
	
});
