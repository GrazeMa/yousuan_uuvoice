/**
 * 实名认证管理管理初始化
 */
var AppAudit = {
    id: "AppAuditTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
AppAudit.initColumn = function () {
    return [
        {field: 'selectItem', radio: false},
            {title: '', field: 'id', visible: false, align: 'center', valign: 'middle'},
            {title: '提交时间', field: 'createTime', visible: true, align: 'center', valign: 'middle'},
            {title: '优优ID', field: 'eid', visible: true, align: 'center', valign: 'middle'},
            {title: '昵称', field: 'name', visible: true, align: 'center', valign: 'middle'},
            {title: '姓名', field: 'xm', visible: true, align: 'center', valign: 'middle'},
            {title: '身份证号码', field: 'card', visible: true, align: 'center', valign: 'middle'},
            {title: '状态', field: 'state', visible: true, align: 'center', valign: 'middle',
                formatter: function (value, row) {
                    //1 待审核，2 审核通过， 3 已拒绝
                    if(value==1){
                        return "待审核";
                    }else if(value==2){
                        return "已通过";
                    }else if(value==3){
                        return "已拒绝";
                    }
                }

            },
            {title: '审核备注', field: 'mark', visible: true, align: 'center', valign: 'middle',
                formatter: function (value, row) {
                    var v=value;
                    if(value!=null && value!=''){
                        if(value.length>20){
                            v=value.substring(0,20)+"......";
                        }
                        return '<span title="'+value+'">'+v+'</span>';
                    }
                }
            },
            {title: '操作', field: 'opt', visible: true, align: 'left', valign: 'middle',
                formatter: function (value, row) {
                    var btn=[];
                    if(row.state==1){//1 待审核，2 审核通过， 3 已拒绝
                        btn[0]=['<a href="javascript:void(0);" onclick="AppAudit.audit('+row.id+')">立即处理</a>'];
                        return btn;
                    }else{
                        return "不可操作";
                    }


                }
            },
            {title: '照片地址', field: 'photoUrl', visible: true, align: 'center', valign: 'middle',
            	formatter: function (value, row) {
            		return '<a href="' + row.photoUrl + '" target="_blank">' + row.photoUrl + '</a>'
                }
            }
    ];
};

/**
 * 检查是否选中
 */
AppAudit.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        AppAudit.seItem = selected;
        return true;
    }
};

/**
 * 立即审核
 */
AppAudit.audit = function (id) {
    var index = layer.open({
        type: 2,
        title: '立即审核',
        area: ['80%', '80%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/appAudit/appAudit_update/' + id
    });
    this.layerIndex = index;
};

/**
 * 删除实名认证管理
 */
AppAudit.delete = function () {
    if (this.check()) {
        var confirm = layer.confirm('确定要删除该数据？', { btn: ['确定','取消'] }, function(){
            layer.close(confirm);
            var ids="";
            $.each(AppAudit.seItem, function(i,val){
                ids += val.id+",";
            });
            ids = ids.substring(0, ids.length - 1);
            var ajax = new $ax(Feng.ctxPath + "/appAudit/delete", function (data) {
                Feng.success("删除成功!");
                AppAudit.table.refresh();
            }, function (data) {
                Feng.error("删除失败!" + data.responseJSON.message + "!");
            });
            ajax.set("appAuditId",ids);
            ajax.start();
        })
    }
};

/**
 * 查询实名认证管理列表
 */
AppAudit.search = function () {
    var queryData = {};
    queryData['beginTime'] = $("#beginTime").val();
    queryData['endTime'] = $("#endTime").val();
    queryData['eid'] = $("#eid").val();
    queryData['state'] = $("#state").val();
    AppAudit.table.refresh({query: queryData});
};
/**
 * 重置
 */
AppAudit.resetSearch = function () {
    $("#beginTime").val("");
    $("#endTime").val("");
    $("#eid").val("");
    $("#state").val("");
    AppAudit.search();
};
$(function () {
    var defaultColunms = AppAudit.initColumn();
    var table = new BSTable(AppAudit.id, "/appAudit/list", defaultColunms);
    table.setPaginationType("server");
    AppAudit.table = table.init();
});
