/**
 * 初始化实名认证管理详情对话框
 */
var AppAuditInfoDlg = {
    appAuditInfoData : {}
};

/**
 * 清除数据
 */
AppAuditInfoDlg.clearData = function() {
    this.appAuditInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
AppAuditInfoDlg.set = function(key, val) {
    this.appAuditInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
AppAuditInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
AppAuditInfoDlg.close = function() {
    parent.layer.close(window.parent.AppAudit.layerIndex);
}

/**
 * 收集数据
 */
AppAuditInfoDlg.collectData = function() {
    var state=$(":radio[name='state']:checked").val();
    this.appAuditInfoData['state'] = state;
    this
    .set('id')
    .set('createTime')
    .set('eid')
    .set('name')
    .set('xm')
    .set('card')
    .set('mark')
    .set('isDelete')
    .set('photoUrl');
}

/**
 * 提交添加
 */
AppAuditInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/appAudit/add", function(data){
        Feng.success("添加成功!");
        window.parent.AppAudit.table.refresh();
        AppAuditInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.appAuditInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
AppAuditInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/appAudit/update", function(data){
        Feng.success("修改成功!");
        window.parent.AppAudit.table.refresh();
        AppAuditInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.appAuditInfoData);
    ajax.start();
}

$(function() {

});
