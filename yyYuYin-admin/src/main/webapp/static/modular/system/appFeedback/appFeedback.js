/**
 * 反馈管理管理初始化
 */
var AppFeedback = {
    id: "AppFeedbackTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
AppFeedback.initColumn = function () {
    return [
        {field: 'selectItem', radio: false},
            {title: '反馈时间', field: 'createTime', visible: true, align: 'center', valign: 'middle'},
            {title: '反馈人', field: 'nickname', visible: true, align: 'center', valign: 'middle'},
            {title: '优优ID', field: 'usercoding', visible: true, align: 'center', valign: 'middle'},
            {title: '联系方式', field: 'phone', visible: true, align: 'center', valign: 'middle'},
            {title: '反馈内容', field: 'content', visible: true, align: 'center', valign: 'middle',
                formatter: function (value, row) {
                    var v=value;
                    if(value!=null && value!=''){
                        if(value.length>20){
                            v=value.substring(0,20)+"......";
                        }
                        return '<span title="'+value+'">'+v+'</span>';
                    }
                }
            },
            {title: '反馈状态', field: 'status', visible: true, align: 'center', valign: 'middle',
                formatter: function (value, row) {
                    // 1：未处理；2：已处理。
                    if(value==1){
                        return "未处理";
                    }else if(value==2){
                        return "已处理";
                    }
                }
            },
            {title: '处理结果', field: 'hand', visible: true, align: 'center', valign: 'middle',
                formatter: function (value, row) {
                    var v=value;
                    if(value!=null && value!=''){
                        if(value.length>20){
                            v=value.substring(0,20)+"......";
                        }
                        return '<span title="'+value+'">'+v+'</span>';
                    }
                }
            },
        {title: '操作', field: 'opt', visible: true, align: 'left', valign: 'middle',
            formatter: function (value, row) {
                var btn=[];
                if(row.status==1){//1  未处理, 2 未处理
                    btn[0]=['<a href="javascript:void(0);" onclick="AppFeedback.audit('+row.id+')">立即处理</a>'];
                }else{
                    btn[0]=['<a href="javascript:void(0);" onclick="AppFeedback.delete_('+row.id+')">删除</a>'];
                }
                return btn;

            }
        }

    ];
};

/**
 * 检查是否选中
 */
AppFeedback.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        AppFeedback.seItem = selected;
        return true;
    }
};

/**
 * 立即审核
 */
AppFeedback.audit = function (id) {
    var index = layer.open({
        type: 2,
        title: '立即审核',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/appFeedback/appFeedback_update/' + id
    });
    this.layerIndex = index;
};


/**
 * 删除反馈管理 单个
 */
AppFeedback.delete_ = function (id) {
    var confirm = layer.confirm('确定要删除该数据？', { btn: ['确定','取消'] }, function() {
        layer.close(confirm);
        var ajax = new $ax(Feng.ctxPath + "/appFeedback/delete", function (data) {
            Feng.success("删除成功!");
            AppFeedback.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("appFeedbackId", id);
        ajax.start();
    })
};


/**
 * 删除实名认证管理
 */
AppFeedback.delete = function () {
    if (this.check()) {
        var confirm = layer.confirm('确定要删除该数据？', { btn: ['确定','取消'] }, function(){
            layer.close(confirm);
            var ids="";
            $.each(AppFeedback.seItem, function(i,val){
                ids += val.id+",";
            });
            ids = ids.substring(0, ids.length - 1);
            var ajax = new $ax(Feng.ctxPath + "/appFeedback/delete", function (data) {
                Feng.success("删除成功!");
                AppFeedback.table.refresh();
            }, function (data) {
                Feng.error("删除失败!" + data.responseJSON.message + "!");
            });
            ajax.set("appFeedbackId",ids);
            ajax.start();
        })
    }
};

/**
 * 查询反馈管理列表
 */
AppFeedback.search = function () {
    var queryData = {};
    queryData['beginTime'] = $("#beginTime").val();
    queryData['endTime'] = $("#endTime").val();
    queryData['usercoding'] = $("#usercoding").val();
    queryData['status'] = $("#status").val();
    queryData['content'] = $("#content").val();
    AppFeedback.table.refresh({query: queryData});
};

/**
 * 重置
 */
AppFeedback.resetSearch = function () {
    $("#beginTime").val("");
    $("#endTime").val("");
    $("#usercoding").val("");
    $("#status").val("");
    $("#content").val("");
    AppFeedback.search();
};

$(function () {
    var defaultColunms = AppFeedback.initColumn();
    var table = new BSTable(AppFeedback.id, "/appFeedback/list", defaultColunms);
    table.setPaginationType("server");
    AppFeedback.table = table.init();
});
