/**
 * 初始化反馈管理详情对话框
 */
var AppFeedbackInfoDlg = {
    appFeedbackInfoData : {}
};

/**
 * 清除数据
 */
AppFeedbackInfoDlg.clearData = function() {
    this.appFeedbackInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
AppFeedbackInfoDlg.set = function(key, val) {
    this.appFeedbackInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
AppFeedbackInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
AppFeedbackInfoDlg.close = function() {
    parent.layer.close(window.parent.AppFeedback.layerIndex);
}

/**
 * 收集数据
 */
AppFeedbackInfoDlg.collectData = function() {
    this
    .set('id')
    .set('uid')
    .set('content')
    .set('isDelete')
    .set('createTime')
    .set('hand')
    .set('role')
    .set('status');
}

/**
 * 提交添加
 */
AppFeedbackInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/appFeedback/add", function(data){
        Feng.success("添加成功!");
        window.parent.AppFeedback.table.refresh();
        AppFeedbackInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.appFeedbackInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
AppFeedbackInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/appFeedback/update", function(data){
        Feng.success("修改成功!");
        window.parent.AppFeedback.table.refresh();
        AppFeedbackInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.appFeedbackInfoData);
    ajax.set("status",2);
    ajax.start();
}

$(function() {

});
