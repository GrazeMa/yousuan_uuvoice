/**
 * 用户邀请明细管理初始化
 */
var AppInvite = {
    id: "AppInviteTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
AppInvite.initColumn = function () {
    return [
        {field: 'selectItem', radio: true,visible:false},
            {title: '邀请时间', field: 'createTime', visible: true, align: 'center', valign: 'middle'},
            {title: '类型', field: 'buid', visible: true, align: 'center', valign: 'middle',
                formatter:function (value,row) {
                    return "邀请奖励"
                }
            },
            {title: '邀请奖励金额', field: 'money', visible: true, align: 'center', valign: 'middle',
                formatter:function (value,row) {
                    return "+"+value+"元"
                }
            }
    ];
};

/**
 * 检查是否选中
 */
AppInvite.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        AppInvite.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加用户邀请明细
 */
AppInvite.openAddAppInvite = function () {
    var index = layer.open({
        type: 2,
        title: '添加用户邀请明细',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/appInvite/appInvite_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看用户邀请明细详情
 */
AppInvite.openAppInviteDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '用户邀请明细详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/appInvite/appInvite_update/' + AppInvite.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除用户邀请明细
 */
AppInvite.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/appInvite/delete", function (data) {
            Feng.success("删除成功!");
            AppInvite.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("appInviteId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询用户邀请明细列表
 */
AppInvite.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    AppInvite.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = AppInvite.initColumn();
    var uid=$("#uid").val();
    var table = new BSTable(AppInvite.id, "/appInvite/list?uid="+uid, defaultColunms);
    table.setPaginationType("client");
    AppInvite.table = table.init();
});
