/**
 * 房间管理管理初始化
 */
var AppRoom = {
    id: "AppRoomTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
AppRoom.initColumn = function () {
    return [
        {field: 'selectItem', radio: false,visible:false},
        {title: '名次', field: 'ranks', visible: true, align: 'center', valign: 'middle'},
        {title: '优优ID', field: 'usercoding', visible: true, align: 'center', valign: 'middle'},
        {title: '昵称', field: 'nickname', visible: true, align: 'center', valign: 'middle'},
        {title: '性别', field: 'sex', visible: true, align: 'center', valign: 'middle',
            formatter: function (value, row) {
                if(value==1){
                    return "男"
                }else{
                    return "女"
                }
            }
        },
        {title: '财富等级', field: 'level_', visible: true, align: 'center', valign: 'middle'},
        {title: '贡献魅力值', field: 'value_', visible: true, align: 'center', valign: 'middle'},
    ];
};
$(function () {
    var defaultColunms = AppRoom.initColumn();
    var rid = $("#rid").val();
    var id=$("#id").val();
    var uid=$("#uid").val();
    var table = new BSTable(AppRoom.id, "/appRoom/auctionRecord/list?rid="+rid+"&id="+id+"&uid="+uid, defaultColunms);
    table.setPaginationType("client");
    AppRoom.table = table.init();
});
