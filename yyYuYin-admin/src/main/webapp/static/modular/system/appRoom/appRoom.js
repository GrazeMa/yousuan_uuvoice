/**
 * 房间管理管理初始化
 */
var AppRoom = {
    id: "AppRoomTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
AppRoom.initColumn = function () {
    return [
        {field: 'selectItem', radio: false,width:'2%'},
        {title: '房间名称', field: 'roomName', visible: true, align: 'center', valign: 'middle',width:'2%'},
        {title: '房间id', field: 'rid', visible: true, align: 'center', valign: 'middle',width:'2%'},
        {title: '房主昵称', field: 'name', visible: true, align: 'center', valign: 'middle',width:'2%'},
        {title: '在线人数', field: 'lineNum', visible: true, align: 'center', valign: 'middle',width:'2%'},
        {title: '房间密码', field: 'password', visible: true, align: 'center', valign: 'middle',width:'2%'},
        {title: '房间标签', field: 'roomLabel', visible: true, align: 'center', valign: 'middle',width:'2%'},
        {title: '房间话题', field: 'roomTopic', visible: true, align: 'center', valign: 'middle',width:'6%',
            formatter: function (value, row) {
                var v=value;
                if(value!=null && value!=''){
                    if(value.length>20){
                        v=value.substring(0,20)+"......";
                    }
                    return '<span title="'+value+'">'+v+'</span>';
                }
            }
        },
        {title: '房间提示', field: 'roomHint', visible: true, align: 'center', valign: 'middle',width:'6%',
            formatter: function (value, row) {
                var v=value;
                if(value!=null && value!=''){
                    if(value.length>20){
                        v=value.substring(0,20)+"......";
                    }
                    return '<span title="'+value+'">'+v+'</span>';
                }
            }
        },
        {title: '协议号数', field: 'protocolNum', visible: true, align: 'center', valign: 'middle',width:'2%'},
        {title: '是否为牌照房间', field: 'state', visible: true, align: 'center', valign: 'middle',width:'2%',
            formatter: function (value, row) {
            //是否牌子房间 1否，2是
                if(value==1){
                    return "否"
                }else{
                    return "是"
                }
            }
        },
        {title: '是否有效', field: 'status', visible: true, align: 'center', valign: 'middle',width:'2%',
            formatter: function (value, row) {
                //是否有效 1有效，2无效
                if(value==1){
                    return "有效"
                }else{
                    return "无效"
                }
            }
        },
        {title: '操作', field: 'opt', visible: true, align: 'left', valign: 'middle',width:'6%',
            formatter: function (value, row) {
                var btn="";
                btn+='<p style="line-height: 10px;"><a href="javascript:void(0);" onclick="AppRoom.openAppRoomDetail('+row.id+')">详情</a>&nbsp;&nbsp;';
                if(row.isRecommend == 1 ){//是否推荐（1否，2是）
                    btn+='<a href="javascript:void(0);" onclick="AppRoom.isRecommendOrAgreement('+row.id+',1)">设为推荐</a></p>';
                }else{
                    btn+='<a href="javascript:void(0);" onclick="AppRoom.unRecommend('+row.id+',1)">取消推荐</a></p>';
                }
                btn+='<p style="line-height: 10px;"><a href="javascript:void(0);" onclick="AppRoom.isRecommendOrAgreement('+row.id+',2)">分配协议号</a><p>';
                btn+='<p style="line-height: 10px;"><a href="javascript:void(0);" onclick="AppRoom.treasureAndCharm('+row.rid+')">查看贡献榜</a><p>';
                btn+='<p style="line-height: 10px;"><a href="javascript:void(0);" onclick="AppRoom.auction('+row.rid+')">最近竞拍排名</a><p>';
                btn+='<p style="line-height: 10px;"><a href="javascript:void(0);" onclick="AppRoom.block('+row.rid+')">房间黑名单</a><p>';
                if(row.state == 1 ){//是否牌子房间 1否，2是
                    btn+='<p style="line-height: 10px;"><a href="javascript:void(0);" onclick="AppRoom.isState_('+row.id+',2)">设为牌照厅</a></p>';
                }else{
                    btn+='<p style="line-height: 10px;"><a href="javascript:void(0);" onclick="AppRoom.isState_('+row.id+',1)">取消牌照厅</a></p>';
                }
                btn+='<p style="line-height: 10px;"><a href="javascript:void(0);" onclick="AppRoom.pkRecord('+row.rid+')">PK记录</a>&nbsp;&nbsp;';
                if(row.status == 1 ){//是否有效 1有效，2无效
                    btn+='<a href="javascript:void(0);" onclick="AppRoom.isFreeze_('+row.id+',2)">禁用</a></p>';
                }else{
                    btn+='<a href="javascript:void(0);" onclick="AppRoom.isFreeze_('+row.id+',1)">解禁</a></p>';
                }
                var btn1='<button type="button" class="btn btn-primary button-margin button" onclick="AppRoom.toggle(this)" id="'+row.id+'">操作' +
                    '</button><div style="display: none">'+btn+'</div>';
                return btn1;

            }
        },
        {title: '记录', field: 'opt', visible: true, align: 'left', valign: 'middle',width:'2%',
            formatter: function (value, row) {
                var btn=[];
                btn.push('<a href="javascript:void(0);" onclick="AppRoom.recordById('+row.id+')">操作记录</a>')
                return btn;
            }
        }

    ];
};
/*切换*/
AppRoom.toggle=function(e){
    $(".button").each(function (k,v) {
        $(this).show();
        $(this).next().hide();
    })
    $(e).toggle();
    $(e).next().toggle();

}
/**
 * 检查是否选中
 */
AppRoom.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        AppRoom.seItem = selected;
        return true;
    }
};

/**
 * 点击添加房间管理
 */
AppRoom.openAddAppRoom = function () {
    var index = layer.open({
        type: 2,
        title: '添加房间管理',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/appRoom/appRoom_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看房间管理详情
 */
AppRoom.openAppRoomDetail = function (id) {
    var index = layer.open({
        type: 2,
        title: '房间管理详情',
        area: ['90%', '90%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/appRoom/appRoom_update/' + id
    });
    this.layerIndex = index;
};
/**
 * 1 设为推荐 2 分配协议号
 * @param id
 */
AppRoom.isRecommendOrAgreement=function(id,type){
    var title="设为推荐";
    if(type==2){
        title="分配协议号";
    }
    var index = layer.open({
        type: 2,
        title: title,
        area: ['80%', '80%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/appRoom/appRoom_recommendOrAgreement?id='+ id+'&type='+type
    });
    this.layerIndex = index;
}
/**
 * 取消推荐
 * @param id
 */
AppRoom.unRecommend=function(id){
    var confirm = layer.confirm('确定要取消该推荐？', { btn: ['确定','取消'] }, function() {
        layer.close(confirm);
        var ajax = new $ax(Feng.ctxPath + "/appRoom/unRecommend", function (data) {
            Feng.success("取消推荐成功!");
            AppRoom.table.refresh();
        }, function (data) {
            Feng.error("取消推荐失败!" + data.responseJSON.message + "!");
        });
        ajax.set("id",id);
        ajax.start();
    });
}
/**
 *查看贡献榜
 */
AppRoom.treasureAndCharm=function(id){
    var index = layer.open({
        type: 2,
        title: '查看贡献榜',
        area: ['90%', '90%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/appRoom/appTreasureAndCharm_list?rid=' + id
    });
    this.layerIndex = index;
}
/**
 *最近竞拍排名
 */
AppRoom.auction=function(id){
    var index = layer.open({
        type: 2,
        title: '最近竞拍排名',
        area: ['90%', '90%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/appRoom/appAuction_list?rid=' + id
    });
    this.layerIndex = index;
}
/**
 *房间黑名单
 */
AppRoom.block=function(rid){
    var index = layer.open({
        type: 2,
        title: '房间黑名单',
        area: ['90%', '90%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/appBlock?rid=' + rid
    });
    this.layerIndex = index;
}
/**
 * pk记录
 * @param id
 */
AppRoom.pkRecord=function(rid){
    var index = layer.open({
        type: 2,
        title: 'pk记录',
        area: ['90%', '90%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/appPk?rid=' + rid
    });
    this.layerIndex = index;
}
/**
 * 删除房间管理
 */
AppRoom.delete = function () {
    if (this.check()) {
        var confirm = layer.confirm('确定要删除该数据？', { btn: ['确定','取消'] }, function(){
            layer.close(confirm);
            var ids="";
            $.each(AppRoom.seItem, function(i,val){
                ids += val.id+",";
            });
            ids = ids.substring(0, ids.length - 1);
            var ajax = new $ax(Feng.ctxPath + "/appRoom/delete", function (data) {
                Feng.success("删除成功!");
                AppRoom.table.refresh();
            }, function (data) {
                Feng.error("删除失败!" + data.responseJSON.message + "!");
            });
            ajax.set("appRoomId",ids);
            ajax.start();
        })
    }
};
/**
 * 禁用和解禁
 */
AppRoom.isFreeze_ = function (id,state) {
    var stateName = "禁用";
    if (state == 1) {
        stateName = "解禁";
    }
    var confirm = layer.confirm("确定要"+stateName+"该房间？", {btn: ['确定', '取消']}, function () {
        layer.close(confirm);
        var ajax = new $ax(Feng.ctxPath + "/appRoom/update", function (data) {
            Feng.success(""+stateName+"成功!");
            AppRoom.table.refresh();
        }, function (data) {
            Feng.error(""+stateName+"失败!" + data.responseJSON.message + "!");
        });
        ajax.set("id",id);
        ajax.set("status",state);
        ajax.set("type_",1);
        ajax.start();
    })
};
/**
 * 设定和取消牌照厅
 */
AppRoom.isState_ = function (id,state) {
    var stateName = "设置";
    var stateName_="确定要设定该房间为牌照厅？";
    if (state == 1) {
        stateName = "取消";
        stateName_="取消该房间的牌照？";
    }
    var confirm = layer.confirm(stateName_, {btn: ['确定', '取消']}, function () {
        layer.close(confirm);
        var ajax = new $ax(Feng.ctxPath + "/appRoom/update", function (data) {
            Feng.success(""+stateName+"成功!");
            AppRoom.table.refresh();
        }, function (data) {
            Feng.error(""+stateName+"失败!" + data.responseJSON.message + "!");
        });
        ajax.set("id",id);
        ajax.set("state",state);
        ajax.set("type_",2);
        ajax.start();
    })
};
/**
 * 操作日志
 * @param id
 */
AppRoom.recordById=function(id){
    var index = layer.open({
        type: 2,
        title: "查看操作记录",
        area: ['90%', '90%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/appRecord/appUser_recordById?id=' + id+"&type=2"
    });
    this.layerIndex = index;
}
/**
 * 查询房间管理列表
 */
AppRoom.search = function () {
    var queryData = {};
    queryData['roomName'] = $("#roomName").val();
    queryData['name'] = $("#name").val();
    queryData['state'] = $("#state").val();
    queryData['status'] = $("#status").val();
    queryData['rid'] = $("#rid").val();
    AppRoom.table.refresh({query: queryData});
};
/**
 * 重置
 */
AppRoom.resetSearch = function () {
    $("#roomName").val("");
    $("#name").val("");
    $("#state").val("");
    $("#status").val("");
    $("#rid").val("");
    AppRoom.search();
};

$(function () {
    var defaultColunms = AppRoom.initColumn();
    var table = new BSTable(AppRoom.id, "/appRoom/list", defaultColunms);
    table.setPaginationType("server");
    AppRoom.table = table.init();
});
