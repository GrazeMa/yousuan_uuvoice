/**
 * 初始化房间管理详情对话框
 */
var AppRoomInfoDlg = {
    appRoomInfoData : {},
    validateFields: {
        protocolNum: {
            validators: {
                regexp: {
                    regexp: /^[1-9]\d*$/,
                    message: '数字格式不正确'
                }
            }
        },
        days_: {
            validators: {
                regexp: {
                    regexp: /^[1-9]\d*$/,
                    message: '数字格式不正确'
                }
            }
        },
        sequence: {
            validators: {
                regexp: {
                    regexp: /^[1-9]\d*$/,
                    message: '数字格式不正确'
                }
            }
        },
    },
};
/**
 * 验证数据是否为空
 */
AppRoomInfoDlg.validate = function () {
    $('#horizontalForm').data("bootstrapValidator").resetForm();
    $('#horizontalForm').bootstrapValidator('validate');
    return $("#horizontalForm").data('bootstrapValidator').isValid();
};
/**
 * 清除数据
 */
AppRoomInfoDlg.clearData = function() {
    this.appRoomInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
AppRoomInfoDlg.set = function(key, val) {
    this.appRoomInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
AppRoomInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
AppRoomInfoDlg.close = function() {
    parent.layer.close(window.parent.AppRoom.layerIndex);
}

/**
 * 收集数据
 */
AppRoomInfoDlg.collectData = function() {
    this
    .set('id')
    .set('createTime')
    .set('roomName')
    .set('uid')
    .set('rid')
    .set('name')
    .set('lineNum')
    .set('password')
    .set('roomLabel')
    .set('roomCount')
    .set('roomTopic')
    .set('roomHint')
    .set('protocolNum')
    .set('state')
    .set('status')
    .set('sequence')
    .set('type')
    .set('isDelete')
    .set('bjImg')
    .set('isState')
    .set('isPk')
    .set('isJp')
    .set('isGp');
}

/**
 * 提交添加
 */
AppRoomInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/appRoom/add", function(data){
        Feng.success("添加成功!");
        window.parent.AppRoom.table.refresh();
        AppRoomInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.appRoomInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
AppRoomInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/appRoom/update", function(data){
        Feng.success("修改成功!");
        window.parent.AppRoom.table.refresh();
        AppRoomInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.appRoomInfoData);
    ajax.start();
}
/**
 * 设为推荐和分配协议号保存数据
 * @private
 */
AppRoomInfoDlg.editSubmit_=function() {

    this.clearData();
    this.collectData();
    if(!this.validate()){
        return;
    }
    var type_=$("#type_").val();
    var sequence=$("#sequence").val();
    var days=$(":radio[name='days']:checked").val();
    if(type_==1){//设为推荐
        if(sequence==null || sequence==''){
            Feng.info("请输入推荐排序");
            return;
        }
    }else if(type_==2){//分配协议号
        var protocolNum=$("#protocolNum").val();
        if(protocolNum==null || protocolNum==''){
            Feng.info("分配数量不能为空");
            return;
        }
        if(days==0){//有限时间
            days=$("#days_").val();
            if(days==null || days==''){
                Feng.info("请设置分配时间天数");
                return;
            }
        }
    }

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/appRoom/updateOpen", function(data){
        if(data=='cont0'){
            Feng.info("没有可分配的协议号")
        }else{
            Feng.success("操作成功!");
            window.parent.AppRoom.table.refresh();
            AppRoomInfoDlg.close();
        }
    },function(data){
        Feng.error("操作失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.appRoomInfoData);
    ajax.set("type_",type_);
    ajax.set("days",days);
    ajax.start();
}
$(function() {
    Feng.initValidator("horizontalForm", AppRoomInfoDlg.validateFields);
    /*分配协议号  设置分配时间*/
    $("input:radio[name='days']").on('click', function(event){
        if($(this).val()==0){
            $("#dayDiv").show();
        }else{
            $("#dayDiv").hide();
        }
    });
});
