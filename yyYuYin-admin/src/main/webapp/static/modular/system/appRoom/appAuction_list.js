/**
 * 房间47.103.55.172初始化
 */
var AppRoom = {
    id: "AppRoomTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
AppRoom.initColumn = function () {
    return [
        {field: 'selectItem', radio: false,visible:false},
        {title: '名次', field: 'ranks', visible: true, align: 'center', valign: 'middle'},
        {title: '优优ID', field: 'usercoding', visible: true, align: 'center', valign: 'middle'},
        {title: '昵称', field: 'nickname', visible: true, align: 'center', valign: 'middle'},
        {title: '性别', field: 'sex', visible: true, align: 'center', valign: 'middle',
            formatter: function (value, row) {
                if(value==1){
                    return "男"
                }else{
                    return "女"
                }
            }
        },
        {title: '财富等级', field: 'level_', visible: true, align: 'center', valign: 'middle'},
        {title: '竞拍魅力值', field: 'value_', visible: true, align: 'center', valign: 'middle'},
        {title: '操作', field: 'opt', visible: true, align: 'left', valign: 'middle',
            formatter: function (value, row) {
                var btn=[];
                btn.push('<a href="javascript:void(0);" onclick="AppRoom.recordById('+row.rid+','+row.usercoding+')">查看贡献记录</a>')
                return btn;
            }
        }

    ];
};
/**
 * 查看贡献记录
 * @param id
 */
AppRoom.recordById=function(rid,uid){
    var id=$(".titleItemCk").attr("name");
    var url=Feng.ctxPath + '/appRoom/appRoom_auctionRecordById?rid='+rid+"&uid="+uid;
    if(id!=undefined){
        url=Feng.ctxPath + '/appRoom/appRoom_auctionRecordById?id=' + id+'&rid='+rid+"&uid="+uid;
    }
    var index = layer.open({
        type: 2,
        title: "查看贡献记录",
        area: ['90%', '90%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content:url
    });
    this.layerIndex = index;
}
/*页面tab切换*/
AppRoom.titleClick5=function(typeName,id,count){
     $('#AppRoomTable').bootstrapTable('destroy');
    //重置
    //this.resetSearch();
    for(var i=1;i<=count;i++){
        $("#titleDivU"+i+"").removeClass("titleItemCk");
    }
    $("#"+typeName).addClass("titleItemCk");
    var defaultColunms = AppRoom.initColumn();
    var rid = $("#rid").val();
    var id=$(".titleItemCk").attr("name");
    var table = new BSTable(AppRoom.id, "/appRoom/auction/list?rid="+rid+"&id="+id, defaultColunms);
    table.setPaginationType("client");
    AppRoom.table = table.init();
}
/**
 * 查询订单管理列表
 */
AppRoom.search = function (id) {
    var queryData = {};
    queryData['id'] = id;
    AppRoom.table.refresh({query: queryData});
};
$(function () {
    var defaultColunms = AppRoom.initColumn();
    var rid = $("#rid").val();
    var id=$(".titleItemCk").attr("name");
    var url="/appRoom/auction/list?rid="+rid;
    if(id!=undefined){
        url="/appRoom/auction/list?rid="+rid+"&id="+id;
    }
    var table = new BSTable(AppRoom.id,url , defaultColunms);
    table.setPaginationType("client");
    AppRoom.table = table.init();
});
