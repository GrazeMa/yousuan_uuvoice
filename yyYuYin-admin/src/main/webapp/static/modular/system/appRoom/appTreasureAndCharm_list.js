/**
 * 房间管理管理初始化
 */
var type_=1;//默认财富榜
var day_=3;//默认总榜
var AppRoom = {
    id: "AppRoomTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
AppRoom.initColumn = function () {
    if(type_==1){
        return [
            {field: 'selectItem', radio: false,visible:false},
            {title: '名次', field: 'ranks', visible: true, align: 'center', valign: 'middle'},
            {title: '优优ID', field: 'usercoding', visible: true, align: 'center', valign: 'middle'},
            {title: '昵称', field: 'nickname', visible: true, align: 'center', valign: 'middle'},
            {title: '性别', field: 'sex', visible: true, align: 'center', valign: 'middle',
                formatter: function (value, row) {
                    if(value==1){
                        return "男"
                    }else{
                        return "女"
                    }
                }
            },
            {title: '财富等级', field: 'level_', visible: true, align: 'center', valign: 'middle'},
            {title: '财富值', field: 'value_', visible: true, align: 'center', valign: 'middle'},
            {title: '距离财富值(相对上一名)', field: 'num', visible: true, align: 'center', valign: 'middle'}
        ];
    }else{
        return [
            {field: 'selectItem', radio: false,visible:false},
            {title: '名次', field: 'ranks', visible: true, align: 'center', valign: 'middle'},
            {title: '优优ID', field: 'usercoding', visible: true, align: 'center', valign: 'middle'},
            {title: '昵称', field: 'nickname', visible: true, align: 'center', valign: 'middle'},
            {title: '性别', field: 'sex', visible: true, align: 'center', valign: 'middle',
                formatter: function (value, row) {
                    if(value==1){
                        return "男"
                    }else{
                        return "女"
                    }
                }
            },
            {title: '魅力等级', field: 'level_', visible: true, align: 'center', valign: 'middle'},
            {title: '魅力值', field: 'value_', visible: true, align: 'center', valign: 'middle'},
            {title: '距离魅力值(相对上一名)', field: 'num', visible: true, align: 'center', valign: 'middle'}
        ];
    }

};

/**
 * 查询活动订单列表
 */
AppRoom.search = function () {
    var queryData = {};
    queryData['usercoding'] = $("#usercoding").val();
    queryData['nickname'] = $("#nickname").val();
    queryData['type'] = type_;
    queryData['day'] = day_;
    AppRoom.table.refresh({query: queryData});
};
//导出
AppRoom.export = function () {
    var usercoding= $("#usercoding").val();
    var nickname = $("#nickname").val();
    var type = type_;
    var day = day_;
    var rid=$("#rid").val();
    window.location.href=Feng.ctxPath + "/export/appRoom?usercoding="+usercoding+"&nickname="+nickname+"&type="+type+"&day="+day+"&rid="+rid;

};
/**
 * 重置
 */
AppRoom.resetSearch = function(){
    $("#usercoding").val("");
    $("#nickname").val("");
}
/*第一级页面tab切换*/
AppRoom.titleClick1=function(typeName,type){
    $('#AppRoomTable').bootstrapTable('destroy');
    type_=type;
    for(var i=1;i<=2;i++){
        $("#titleDivD"+i+"").removeClass("titleItemCk");
    }
    $("#"+typeName).addClass("titleItemCk");
    var rid=$("#rid").val();
    var defaultColunms = AppRoom.initColumn();
    var table = new BSTable(AppRoom.id, "/appRoom/treasureAndCharm/list?rid="+rid+"&type="+type_, defaultColunms);
    table.setPaginationType("client");
    AppRoom.table = table.init();
    AppRoom.titleClick2('titleDivU3',3);

}
/*第二级页面tab切换*/
AppRoom.titleClick2=function(typeName,day){
    day_=day;
    //$('#AppRoomTable').bootstrapTable('destroy');
    for(var i=1;i<=3;i++){
        $("#titleDivU"+i+"").removeClass("titleItemCk");
    }
    $("#"+typeName).addClass("titleItemCk");
    AppRoom.search();
}

$(function () {
    var rid=$("#rid").val();
    var defaultColunms = AppRoom.initColumn();
    var table = new BSTable(AppRoom.id, "/appRoom/treasureAndCharm/list?rid="+rid, defaultColunms);
    table.setPaginationType("client");
    AppRoom.table = table.init();
});
