/**
 * 广播交友管理初始化
 */
var AppBroad = {
    id: "AppBroadTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
AppBroad.initColumn = function () {
    return [
        {field: 'selectItem', radio: false},
            {title: '', field: 'id', visible: false, align: 'center', valign: 'middle'},
            {title: '发布时间', field: 'createTime', visible: true, align: 'center', valign: 'middle',width:'28%'},
            {title: '优优ID', field: 'eid', visible: true, align: 'center', valign: 'middle'},
            {title: '昵称', field: 'name', visible: true, align: 'center', valign: 'middle'},
            {title: '发布内容', field: 'content', visible: true, align: 'center', valign: 'middle' ,width:'45%',
                formatter: function (value, row) {
                    var v=value;
                    if(value!=null && value!=''){
                        if(value.length>20){
                            v=value.substring(0,20)+"......";
                        }
                        return '<span title="'+value+'">'+v+'</span>';
                    }
                }
            },
        {title: '操作', field: 'opt', visible: true, align: 'left', valign: 'middle',
            formatter: function (value, row) {
                var btn=[];
                if(row.state == 1 ){//是否冻结 1否,2是
                    btn.push('<a href="javascript:void(0);" onclick="AppBroad.isFreeze_('+row.uid+',2)">禁用</a>')
                }else{
                    btn.push('<a href="javascript:void(0);" onclick="AppBroad.isFreeze_('+row.uid+',1)">解禁</a>')
                }

                return btn;
            }
        }
    ];
};

/**
 * 禁用和解禁
 */
AppBroad.isFreeze_ = function (id,state) {
    var stateName = "禁用";
    if (state == 1) {
        stateName = "解禁";
    }
    var confirm = layer.confirm("确定要"+stateName+"该用户？", {btn: ['确定', '取消']}, function () {
        layer.close(confirm);
        var ajax = new $ax(Feng.ctxPath + "/appUser/updateFreeze", function (data) {
            Feng.success(""+stateName+"成功!");
            AppBroad.table.refresh();
        }, function (data) {
            Feng.error(""+stateName+"失败!" + data.responseJSON.message + "!");
        });
        ajax.set("ids",id);
        ajax.set("state",state);
        ajax.start();
    })
};
/**
 * 检查是否选中
 */
AppBroad.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        AppBroad.seItem = selected;
        return true;
    }
};


/**
 * 删除广播交友
 */
AppBroad.delete = function () {
    if (this.check()) {
        var confirm = layer.confirm('确定要删除该数据？', { btn: ['确定','取消'] }, function(){
            layer.close(confirm);
            var ids="";
            $.each(AppBroad.seItem, function(i,val){
                ids += val.id+",";
            });
            ids = ids.substring(0, ids.length - 1);
            var ajax = new $ax(Feng.ctxPath + "/appBroad/delete", function (data) {
                Feng.success("删除成功!");
                AppBroad.table.refresh();
            }, function (data) {
                Feng.error("删除失败!" + data.responseJSON.message + "!");
            });
            ajax.set("appBroadId",ids);
            ajax.start();
        })
    }
};

/**
 * 查询广播交友列表
 */
AppBroad.search = function () {
    var queryData = {};
    queryData['name'] = $("#name").val();
    queryData['content'] = $("#content").val();
    queryData['eid'] = $("#eid").val();
    queryData['beginTime'] = $("#beginTime").val();
    queryData['endTime'] = $("#endTime").val();
    AppBroad.table.refresh({query: queryData});
};

/**
 * 重置
 */
AppBroad.resetSearch = function () {
    $("#beginTime").val("");
    $("#endTime").val("");
    $("#name").val("");
    $("#content").val("");
    $("#eid").val("");
    AppBroad.search();
};
$(function () {
    var defaultColunms = AppBroad.initColumn();
    var table = new BSTable(AppBroad.id, "/appBroad/list", defaultColunms);
    table.setPaginationType("server");
    AppBroad.table = table.init();
});
