/**
 * 房间流水管理初始化
 */
var AppRoomTurnover = {
    id: "AppRoomTurnoverTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
AppRoomTurnover.initColumn = function () {
    return [
        {field: 'selectItem', radio: true,visible:false},
        {title: '', field: 'id', visible: false, align: 'center', valign: 'middle'},
        {title: '消费时间', field: 'createTime', visible: true, align: 'center', valign: 'middle'},
        {title: '消费用户', field: 'name', visible: true, align: 'center', valign: 'middle'},
        {title: '优优ID', field: 'eid', visible: true, align: 'center', valign: 'middle'},
        {title: '房间ID', field: 'rid', visible: true, align: 'center', valign: 'middle'},
        {title: '消费类型', field: 'state', visible: true, align: 'center', valign: 'middle',
            formatter: function (value, row) {
                //1是赠送礼物，2是购买花环，3购买座驾，4 发红包
                if(value==1){
                    return "赠送礼物"+"-"+row.lwName
                }else if(value==2){
                    return "购买花环"+"-"+row.lwName
                }else if(value==3){
                    return "购买座驾"+"-"+row.lwName
                }else {
                    return "发红包"
                }
            }
        },
        {title: '消费数量', field: 'consumeNum', visible: true, align: 'center', valign: 'middle'},
        {title: '消费优币数', field: 'consumeYbNum', visible: true, align: 'center', valign: 'middle',
        	footerFormatter: function (items) {
        		var count = 0;
        	    for (var i in items) {
        	    	// count += parseInt(items[i][this.consumeYbNum]);
        	        count += items[i].consumeYbNum;
        	    }
        	    return "总计: " + count;
        	}
        }
    ];
};

/**
 * 导出。
 */
AppRoomTurnover.export = function () {
    var beginTime= $("#beginTime").val();
    var endTime = $("#endTime").val();
    var rid = $("#rid").val();
    var name = $("#name").val();
    window.location.href=Feng.ctxPath + "/export/appYbconsume?beginTime="+beginTime+"&endTime="+endTime+"&rid="+rid+"&name="+name;
};

/**
 * 查询优币充值列表
 */
AppRoomTurnover.search = function () {
    var queryData = {};
    queryData['beginTime'] = $("#beginTime").val();
    queryData['endTime'] = $("#endTime").val();
    queryData['eid'] = $("#eid").val();
    queryData['rid'] = $("#rid").val();
    queryData['name'] = $("#name").val();
    queryData['price'] = $("#price").val();
    AppRoomTurnover.table.refresh({query: queryData});
};
/**
 * 重置
 */
AppRoomTurnover.resetSearch = function () {
    $("#beginTime").val("");
    $("#endTime").val("");
    $("#eid").val("");
    $("#rid").val("");
    $("#name").val("");
    $("#price").val("");
    AppRoomTurnover.search();
};

$(function () {
    var defaultColunms = AppRoomTurnover.initColumn();
    var table = new BSTable(AppRoomTurnover.id, "/appRoomTurnover/list", defaultColunms);
    table.setPaginationType("client");
    AppRoomTurnover.table = table.init();
});
