/**
 * 用户分成管理初始化
 */
var AppDivide = {
    id: "AppDivideTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
AppDivide.initColumn = function () {
    return [
        {field: 'selectItem', radio: true,visible:false},
            {title: '', field: 'id', visible: false, align: 'center', valign: 'middle'},
            {title: '充值时间', field: 'createTime', visible: true, align: 'center', valign: 'middle'},
            {title: '被邀请用户', field: 'buName', visible: true, align: 'center', valign: 'middle'},
            {title: '用户充值金额', field: 'czMoney', visible: true, align: 'center', valign: 'middle',
                formatter:function (value,row) {
                    return value+"元"
                }
            },
            {title: '充值奖励金额', field: 'money', visible: true, align: 'center', valign: 'middle',
                formatter:function (value,row) {
                    return "+"+value+"元"
                }
            }

    ];
};

/**
 * 检查是否选中
 */
AppDivide.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        AppDivide.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加用户分成
 */
AppDivide.openAddAppDivide = function () {
    var index = layer.open({
        type: 2,
        title: '添加用户分成',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/appDivide/appDivide_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看用户分成详情
 */
AppDivide.openAppDivideDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '用户分成详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/appDivide/appDivide_update/' + AppDivide.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除用户分成
 */
AppDivide.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/appDivide/delete", function (data) {
            Feng.success("删除成功!");
            AppDivide.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("appDivideId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询用户分成列表
 */
AppDivide.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    AppDivide.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = AppDivide.initColumn();
    var uid=$("#uid").val();
    var table = new BSTable(AppDivide.id, "/appDivide/list?uid="+uid, defaultColunms);
    table.setPaginationType("client");
    AppDivide.table = table.init();
});
