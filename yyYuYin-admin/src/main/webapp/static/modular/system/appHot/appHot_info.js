/**
 * 初始化热搜词详情对话框
 */
var AppHotInfoDlg = {
    appHotInfoData : {}
};

/**
 * 清除数据
 */
AppHotInfoDlg.clearData = function() {
    this.appHotInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
AppHotInfoDlg.set = function(key, val) {
    this.appHotInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
AppHotInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
AppHotInfoDlg.close = function() {
    parent.layer.close(window.parent.AppHot.layerIndex);
}

/**
 * 收集数据
 */
AppHotInfoDlg.collectData = function() {
    this
    .set('id')
    .set('createTime')
    .set('name')
    .set('state')
    .set('status')
    .set('num')
    .set('isDelete');
}

/**
 * 提交添加
 */
AppHotInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();
    if($("#name").val()==null || $("#name").val()==''){
        Feng.info("请输入热搜词");
        return ;
    }
    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/appHot/add", function(data){
        if(data==1){
            Feng.info("该热搜词已存在")
        }else {
            Feng.success("添加成功!");
            window.parent.AppHot.table.refresh();
            AppHotInfoDlg.close();
        }
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.appHotInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
AppHotInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();
    if($("#name").val()==null || $("#name").val()==''){
        Feng.info("请输入热搜词");
        return ;
    }
    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/appHot/update", function(data){
        if(data==1){
            Feng.info("该热搜词已存在")
        }else{
            Feng.success("修改成功!");
            window.parent.AppHot.table.refresh();
            AppHotInfoDlg.close();
        }

    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.appHotInfoData);
    ajax.start();
}

$(function() {

});
