/**
 * 热搜词管理初始化
 */
var AppHot = {
    id: "AppHotTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
AppHot.initColumn = function () {
    return [
        {field: 'selectItem', radio: false},
            {title: '', field: 'id', visible: false, align: 'center', valign: 'middle'},
            {title: '添加时间', field: 'createTime', visible: true, align: 'center', valign: 'middle'},
            {title: '热搜词', field: 'name', visible: true, align: 'center', valign: 'middle'},
            {title: '来源', field: 'state', visible: true, align: 'center', valign: 'middle',
                formatter: function (value, row) {
                    //1平台，2 用户
                    if(value==1){
                        return "平台添加";
                    }else {
                        return "用户搜索";
                    }
                }

            },
            {title: '是否是热搜', field: 'status', visible: true, align: 'center', valign: 'middle',
                formatter: function (value, row) {
                    // 1否，2是
                    if(value==1){
                        return "否"
                    }else {
                        return "是"
                    }
                }
            },
            {title: '搜索次数', field: 'num', visible: true, align: 'center', valign: 'middle'},
        {title: '操作', field: 'opt', visible: true, align: 'left', valign: 'middle',
            formatter: function (value, row) {
                var btn=[];

                if(row.status==1){
                    btn+='<p style="line-height: 10px;"><a href="javascript:void(0);" onclick="AppHot.isStatus('+row.id+',2)">设为热搜</a></p>';
                }else{
                    if(row.state==1){//平台
                        btn+='<p style="line-height: 10px;"><a href="javascript:void(0);" onclick="AppHot.openAppHotDetail('+row.id+')">编辑</a></p>';
                    }
                    btn+='<p style="line-height: 10px;"><a href="javascript:void(0);" onclick="AppHot.delete_('+row.id+')">删除</a></p>';
                }
                return btn;

            }
        }

    ];
};

/**
 * 检查是否选中
 */
AppHot.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        AppHot.seItem = selected;
        return true;
    }
};

/**
 * 点击添加热搜词
 */
AppHot.openAddAppHot = function () {
    var index = layer.open({
        type: 2,
        title: '添加热搜词',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/appHot/appHot_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看热搜词详情
 */
AppHot.openAppHotDetail = function (id) {
    var index = layer.open({
        type: 2,
        title: '热搜词编辑',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/appHot/appHot_update/' + id
    });
    this.layerIndex = index;
};

/**
 * 删除热搜词
 */
AppHot.delete_ = function (id) {
    var confirm = layer.confirm('确定要删除该数据？', { btn: ['确定','取消'] }, function(){
        layer.close(confirm);
        var ajax = new $ax(Feng.ctxPath + "/appHot/delete", function (data) {
            Feng.success("删除成功!");
            AppHot.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("appHotId",id);
        ajax.start();
    })
};
/**
 * 删除礼物管理
 */
AppHot.delete = function () {
    if (this.check()) {
        var confirm = layer.confirm('确定要删除该数据？', { btn: ['确定','取消'] }, function(){
            layer.close(confirm);
            var ids="";
            $.each(AppHot.seItem, function(i,val){
                ids += val.id+",";
            });
            ids = ids.substring(0, ids.length - 1);
            var ajax = new $ax(Feng.ctxPath + "/appHot/delete", function (data) {
                Feng.success("删除成功!");
                AppHot.table.refresh();
            }, function (data) {
                Feng.error("删除失败!" + data.responseJSON.message + "!");
            });
            ajax.set("appHotId",ids);
            ajax.start();
        })
    }
};
/**
 * 设为热搜
 * @param id
 * @param state
 */
AppHot.isStatus=function(id,state){
    var ajax = new $ax(Feng.ctxPath + "/appHot/update_", function (data) {
        Feng.success("设置成功!");
        AppHot.table.refresh();
    }, function (data) {
        Feng.error("设置失败!" + data.responseJSON.message + "!");
    });
    ajax.set("id",id);
    ajax.set("status",state);
    ajax.start();
}

/**
 * 查询热搜词列表
 */
AppHot.search = function () {
    var queryData = {};
    queryData['state'] = $("#state").val();
    queryData['name'] = $("#name").val();
    queryData['status'] = $("#status").val();
    queryData['beginTime'] = $("#beginTime").val();
    queryData['endTime'] = $("#endTime").val();
    AppHot.table.refresh({query: queryData});
};
/**
 * 重置
 */
AppHot.resetSearch = function () {
    $("#beginTime").val("");
    $("#endTime").val("");
    $("#name").val("");
    $("#state").val("");
    $("#status").val("");
    AppHot.search();
};


$(function () {
    var defaultColunms = AppHot.initColumn();
    var table = new BSTable(AppHot.id, "/appHot/list", defaultColunms);
    table.setPaginationType("server");
    AppHot.table = table.init();
});
