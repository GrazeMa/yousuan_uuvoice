/**
 * 靓号管理初始化
 */
var AppHall = {
    id: "AppHallTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
AppHall.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
            {title: '', field: 'id', visible: false, align: 'center', valign: 'middle'},
            {title: '靓号', field: 'liang', visible: true, align: 'center', valign: 'middle'},
            {title: '优优ID', field: 'uecding', visible: true, align: 'center', valign: 'middle'},
            {title: '操作', field: 'opt', visible: true, align: 'left', valign: 'middle',
                formatter: function (value, row) {
                    var btn=[];

                    if(row.uecding==null || row.uecding==''){
                        btn+='<p style="line-height: 10px;"><a href="javascript:void(0);" onclick="AppHall.binding('+row.id+')">绑定</a></p>';
                        btn +='<p style="line-height: 10px;"><a href="javascript:void(0);" onclick="AppHall.delete('+row.id+')">删除</a></p>';
                    }else{
                        btn+='<p style="line-height: 10px;"><a href="javascript:void(0);" onclick="AppHall.unbind('+row.id+')">解绑</a></p>';
                    }
                    return btn;

                }
            }

    ];
};

/**
 * 检查是否选中
 */
AppHall.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        AppHall.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加靓号
 */
AppHall.openAddAppHall = function () {
    var index = layer.open({
        type: 2,
        title: '添加靓号',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/appHall/appHall_add'
    });
    this.layerIndex = index;
};

/**
 * 绑定靓号
 */
AppHall.binding = function (id) {
    var index = layer.open({
        type: 2,
        title: '绑定靓号',
        area: ['90%', '90%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/appHall/appHall_update/' + id
    });
    this.layerIndex = index;
};

/**
 * 删除靓号
 */
AppHall.delete = function (id) {
    var confirm = layer.confirm('确定要删除该数据？', { btn: ['确定','取消'] }, function(){
        layer.close(confirm);
        var ajax = new $ax(Feng.ctxPath + "/appHall/delete", function (data) {
            Feng.success("删除成功!");
            AppHall.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("appHallId",id);
        ajax.start();
    });
};
/**
 * 靓号解绑
 */
AppHall.unbind = function (id) {
    var confirm = layer.confirm('确定要解绑该靓号？', { btn: ['确定','取消'] }, function(){
        layer.close(confirm);
        var ajax = new $ax(Feng.ctxPath + "/appHall/unbind", function (data) {
            Feng.success("解绑成功!");
            AppHall.table.refresh();
        }, function (data) {
            Feng.error("解绑失败!" + data.responseJSON.message + "!");
        });
        ajax.set("appHallId",id);
        ajax.start();
    });
};

/**
 * 查询靓号列表
 */
AppHall.search = function () {
    var queryData = {};
    queryData['liang'] = $("#liang").val();
    queryData['uecding'] = $("#uecding").val();
    AppHall.table.refresh({query: queryData});
};


/**
 * 重置
 */
AppHall.resetSearch = function () {
    $("#liang").val("");
    $("#uecding").val("");
    AppHall.search();
};

$(function () {
    var defaultColunms = AppHall.initColumn();
    var table = new BSTable(AppHall.id, "/appHall/list", defaultColunms);
    table.setPaginationType("server");
    AppHall.table = table.init();
});
