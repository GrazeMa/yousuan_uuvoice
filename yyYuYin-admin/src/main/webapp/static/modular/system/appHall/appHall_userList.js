/**
 * 用户管理管理初始化
 */
var AppUser = {
    id: "AppUserTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
AppUser.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
        {title: '注册时间', field: 'createDate', visible: true, align: 'center', valign: 'middle'},
        {title: '昵称', field: 'nickname', visible: true, align: 'center', valign: 'middle'},
        {title: '手机号', field: 'phone', visible: true, align: 'center', valign: 'middle'},
        {title: '优优ID', field: 'usercoding', visible: true, align: 'center', valign: 'middle'},
        {title: '性别', field: 'sexName', visible: true, align: 'center', valign: 'middle'},
        {title: '财富等级', field: 'treasureGrade', visible: true, align: 'center', valign: 'middle',
            formatter: function (value, row) {
                return "LV"+value;
            }
        },
        {title: '魅力等级', field: 'charmGrade', visible: true, align: 'center', valign: 'middle',
            formatter: function (value, row) {
                return "*"+value;
            }
        },
        {title: '优币数', field: 'gold', visible: true, align: 'center', valign: 'middle'},
        {title: '优钻数', field: 'ynum', visible: true, align: 'center', valign: 'middle'},
        {title: '状态', field: 'stateName', visible: true, align: 'center', valign: 'middle'},
    ];
};

/**
 * 检查是否选中
 */
AppUser.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        AppUser.seItem = selected[0];
        return true;
    }
};

/**
 * 确定选择
 */
AppUser.confirm = function () {
    if (this.check()) {
        $(window.parent.AppHallInfoDlg.uecding()).val(this.seItem.usercoding);
        $(window.parent.AppHallInfoDlg.nickName()).val(this.seItem.nickname);
        parent.layer.close(window.parent.AppHallInfoDlg.layerIndex);
    }
};


/**
 * 查询用户管理列表
 */
AppUser.search = function () {
    var queryData = {};
    queryData['usercoding'] = $("#usercoding").val();
    queryData['nickname'] = $("#nickname").val();
    queryData['sex'] = $("#sex").val();
    AppUser.table.refresh({query: queryData});
};
/**
 * 重置
 */
AppUser.resetSearch = function () {
    $("#usercoding").val("");
    $("#nickname").val("");
    $("#sex").val("");
    AppUser.search();
};
$(function () {
    var defaultColunms = AppUser.initColumn();
    var table = new BSTable(AppUser.id, "/appHall/userList", defaultColunms);
    table.setPaginationType("server");
    AppUser.table = table.init();
});
