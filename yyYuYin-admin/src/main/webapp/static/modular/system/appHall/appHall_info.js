/**
 * 初始化靓号详情对话框
 */
var AppHallInfoDlg = {
    appHallInfoData : {},
    layerIndex: -1,
};

/**
 * 清除数据
 */
AppHallInfoDlg.clearData = function() {
    this.appHallInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
AppHallInfoDlg.set = function(key, val) {
    this.appHallInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
AppHallInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
AppHallInfoDlg.close = function() {
    parent.layer.close(window.parent.AppHall.layerIndex);
}

/**
 * 收集数据
 */
AppHallInfoDlg.collectData = function() {
    this
    .set('id')
    .set('liang')
    .set('uecding');
}

/**
 * 提交添加
 */
AppHallInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/appHall/add", function(data){
        if(data=="1"){
            Feng.info("该靓号已经存在,请重新输入");
            return;
        }
        Feng.success("添加成功!");
        window.parent.AppHall.table.refresh();
        AppHallInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.appHallInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
AppHallInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();
    var uecding=$("#uecding").val();
    if(uecding == null || uecding == ''){
        Feng.info("请选择用户");
        return;
    }

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/appHall/update", function(data){
        Feng.success("修改成功!");
        window.parent.AppHall.table.refresh();
        AppHallInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.appHallInfoData);
    ajax.start();
}

/*选择用户*/
AppHallInfoDlg.appUserList = function(){
    var index = layer.open({
        type: 2,
        title: '选择用户',
        area: ['90%', '90%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/appHall/appHall_userList'
    });
    this.layerIndex = index;
}
/*优优号*/
AppHallInfoDlg.uecding = function () {
    return  $("#uecding");
}
/*用户昵称*/
AppHallInfoDlg.nickName = function () {
    return  $("#nickName");
}

$(function() {

});
