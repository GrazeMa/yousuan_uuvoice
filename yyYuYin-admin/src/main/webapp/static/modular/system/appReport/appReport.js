var type_=1;//默认举报用户
/**
 * 举报管理管理初始化
 */
var AppReport = {
    id: "AppReportTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
AppReport.initColumn = function () {

    if(type_==1 || type_==2){
            return [
                {field: 'selectItem', radio: false},
                    {title: '举报时间', field: 'createTime', visible: true, align: 'center', valign: 'middle'},
                    {title: '举报人', field: 'id', visible: true, align: 'center', valign: 'middle',
                        formatter: function (value, row) {
                            return "<p onclick='AppReport.userDetail("+row.uid+")'><a href='#'>"+row.nickname_+"</a></p><p>"+row.usercoding_+"</p>"
                        }
                    },
                    /*{title: '举报类型', field: 'role', visible: true, align: 'center', valign: 'middle',
                        formatter: function (value, row) {
                            //1 是用户，2 是房间
                            if(value==1){
                                return "用户"
                            }else{
                                return "房间"
                            }
                        }
                    },*/
                    {title: '被举报对象', field: 'uid', visible: true, align: 'center', valign: 'middle',
                        formatter: function (uidname, row) {
                            if(row.role==1){//用户
                                return "<p onclick='AppReport.userDetail("+row.buid+")'><a href='#'>"+row.bname+"</a></p><p>"+row.bcoding+"</p>"
                            }else{//房间
                                return "<p onclick='AppReport.buserDetail("+row.buid+")'><a href='#'>"+row.bname+"</a></p><p>"+row.bcoding+"</p>"
                            }
                        }
                    },
                    {title: '举报原因', field: 'content', visible: true, align: 'center', valign: 'middle',
                        formatter: function (value, row) {
                            var v=value;
                            if(value!=null && value!=''){
                                if(value.length>20){
                                    v=value.substring(0,20)+"......";
                                }
                                return '<span title="'+value+'">'+v+'</span>';
                            }
                        }
                    },
                    {title: '举报状态', field: 'status', visible: true, align: 'center', valign: 'middle',
                        formatter: function (value, row) {
                            //（1=未处理  2=已处理）
                            if(value==1){
                                return "未处理"
                            }else {
                                return "已处理"
                            }
                        }

                    },
                    {title: '处理结果', field: 'hand', visible: true, align: 'center', valign: 'middle',
                        formatter: function (value, row) {
                            var dayName="";
                            //-1不冻结，1：1天，2：2天，7：7天，0永久
                            if(row.lockDays==-1){
                                dayName="不冻结";
                            }else if(row.lockDays==0){
                                dayName="永久冻结";
                            }else if(row.lockDays>0){
                                dayName="冻结"+row.lockDays+"天";
                            }
                            var valSpan="";
                            var v=value;
                            if(value!=null && value!=''){
                                if(value.length>20){
                                    v=value.substring(0,20)+"......";
                                }
                                valSpan= '<span title="'+value+'">'+v+'</span>';
                            }
                            return '<p>'+dayName+'</p>'+valSpan;
                        }
                    },
                    {title: '操作', field: 'opt', visible: true, align: 'left', valign: 'middle',
                        formatter: function (value, row) {
                            var btn=[];
                            if(row.status==1){//1  未处理, 2 未处理
                                btn[0]=['<a href="javascript:void(0);" onclick="AppReport.audit('+row.id+')">立即处理</a>'];
                            }else{
                                btn[0]=['<a href="javascript:void(0);" onclick="AppReport.delete_('+row.id+')">删除</a>'];
                            }
                            return btn;

                        }
                    }

            ];
    }else if(type_==3){
        /*音乐举报数据*/
        return [
            {field: 'selectItem', radio: false},
            {title: '举报时间', field: 'createTime', visible: true, align: 'center', valign: 'middle'},
            {title: '举报人', field: 'id', visible: true, align: 'center', valign: 'middle',
                formatter: function (value, row) {
                    return "<p onclick='AppReport.userDetail("+row.uid+")'><a href='#'>"+row.nickname_+"</a></p><p>"+row.usercoding_+"</p>"
                }
            },
            /*{title: '举报类型', field: 'role', visible: true, align: 'center', valign: 'middle',
                formatter: function (value, row) {
                    //1 是用户，2 是房间
                    if(value==1){
                        return "用户"
                    }else if(value==2){
                        return "房间"
                    }else if(value==3){
                        return "音乐"
                    }
                }
            },*/
            {title: '被举报音乐', field: 'uid', visible: true, align: 'center', valign: 'middle',
                formatter: function (v, row) {
                    return "<p>"+row.musicName+"</p><p>"+row.gsNane+"</p>"
                }
            },
            {title: '音乐内容', field: 'url', visible: true, align: 'center', valign: 'middle',
                formatter: function (value, row) {
                    return "<i class='fa fa-play-circle' onclick='AppReport.play(this)' name='"+value+"'></i>"
                }
            },
            {title: '举报原因', field: 'content', visible: true, align: 'center', valign: 'middle',
                formatter: function (value, row) {
                    var v=value;
                    if(value!=null && value!=''){
                        if(value.length>20){
                            v=value.substring(0,20)+"......";
                        }
                        return '<span title="'+value+'">'+v+'</span>';
                    }
                }
            },
            {title: '歌曲归属', field: 'musicUid', visible: true, align: 'center', valign: 'middle',
                formatter: function (value, row) {
                    if(value==0){
                        return "平台上传"
                    }else{
                        return "<p>"+row.bcoding+"</p><p>"+row.bname+"</p>"
                    }
                }
            },
            {title: '举报状态', field: 'status', visible: true, align: 'center', valign: 'middle',
                formatter: function (value, row) {
                    //（1=未处理  2=已处理）
                    if(value==1){
                        return "未处理"
                    }else {
                        return "已处理"
                    }
                }

            },
            {title: '处理结果', field: 'hand', visible: true, align: 'center', valign: 'middle',
                formatter: function (value, row) {
                    var dayName="";
                    //-1不冻结，1：1天，2：2天，7：7天，0永久
                    if(row.lockDays==-1){
                        dayName="不冻结";
                    }else if(row.lockDays==0){
                        dayName="永久冻结";
                    }else if(row.lockDays>0){
                        dayName="冻结"+row.lockDays+"天";
                    }
                    var valSpan="";
                    var v=value;
                    if(value!=null && value!=''){
                        if(value.length>20){
                            v=value.substring(0,20)+"......";
                        }
                        valSpan= '<span title="'+value+'">'+v+'</span>';
                    }
                    return '<p>'+dayName+'</p>'+valSpan;
                }
            },
            {title: '操作', field: 'opt', visible: true, align: 'left', valign: 'middle',
                formatter: function (value, row) {
                    var btn=[];
                    if(row.status==1){//1  未处理, 2 未处理
                        btn[0]=['<a href="javascript:void(0);" onclick="AppReport.audit('+row.id+')">立即处理</a>'];
                    }else{
                        btn[0]=['<a href="javascript:void(0);" onclick="AppReport.delete_('+row.id+')">删除</a>'];
                    }
                    return btn;

                }
            }

        ];
    }
};
/**
 * 播放音乐
 */
AppReport.play=function(e){
    var url=$(e).attr("name");
    /**
     * 音频试听（可选）
     */
    layer.open({
        type: 1,
        title: false,
        closeBtn: 0,
        area: 'auto',
        skin: 'layui-layer-nobg', //没有背景色
        shadeClose: true,
        content: '<div><audio width="100px" height="100px" autoplay="autoplay" controls  src="'+url+'">当前浏览器不支持播放该音频</audio></div>'
    });
}
/**
 * 检查是否选中
 */
AppReport.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        AppReport.seItem = selected;
        return true;
    }
};
/*查看用户资料*/
AppReport.userDetail=function(id){
    var index = layer.open({
        type: 2,
        title: '用户管理详情',
        area: ['90%', '90%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/appUser/appUser_detail/' + id
    });
    this.layerIndex = index;
}
/*查看被举报资料*/
AppReport.buserDetail=function(id){
    var index = layer.open({
        type: 2,
        title: '房间管理详情',
        area: ['90%', '90%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/appRoom/appRoom_update/' + id
    });
    this.layerIndex = index;
}
/**
 * 立即审核
 */
AppReport.audit = function (id) {
    var index = layer.open({
        type: 2,
        title: '立即审核',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/appReport/appReport_update/' + id
    });
    this.layerIndex = index;
};

/**
 * 删除举报管理
 */
AppReport.delete_ = function (id) {
    var confirm = layer.confirm('确定要删除该数据？', { btn: ['确定','取消'] }, function() {
        layer.close(confirm);
        var ajax = new $ax(Feng.ctxPath + "/appReport/delete", function (data) {
            Feng.success("删除成功!");
            AppReport.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("appReportId", id);
        ajax.start();
    })
};
/**
 * 删除举报管理
 */
AppReport.delete = function () {
    if (this.check()) {
        var confirm = layer.confirm('确定要删除该数据？', { btn: ['确定','取消'] }, function(){
            layer.close(confirm);
            var ids="";
            $.each(AppReport.seItem, function(i,val){
                ids += val.id+",";
            });
            ids = ids.substring(0, ids.length - 1);
            var ajax = new $ax(Feng.ctxPath + "/appReport/delete", function (data) {
                Feng.success("删除成功!");
                AppReport.table.refresh();
            }, function (data) {
                Feng.error("删除失败!" + data.responseJSON.message + "!");
            });
            ajax.set("appReportId",ids);
            ajax.start();
        })
    }
};
/*页面tab切换*/
AppReport.titleClick2=function(typeName,type){
    $('#AppReportTable').bootstrapTable('destroy');
    type_=type;
    if(type==3){//音乐
        $("#musicType").show();
    }else {
        $("#musicType").hide();
    }
    for(var i=1;i<=3;i++){
        $("#titleDivU"+i+"").removeClass("titleItemCk");
    }
    $("#"+typeName).addClass("titleItemCk");
    AppReport.resetSearch();
    var defaultColunms = AppReport.initColumn();
    var table = new BSTable(AppReport.id, "/appReport/list?role="+type_, defaultColunms);
    table.setPaginationType("server");
    AppReport.table = table.init();
}
/**
 * 查询举报管理列表
 */
AppReport.search = function () {
    var queryData = {};
    queryData['role'] = type_;
    queryData['status'] = $("#status").val();
    queryData['content'] = $("#content").val();
    queryData['type'] = $("#type").val();
    AppReport.table.refresh({query: queryData});
};
/**
 * 重置
 */
AppReport.resetSearch = function () {
    $("#status").val("");
    $("#content").val("");
    $("#type").val("");
    AppReport.search();
};
$(function () {
    var defaultColunms = AppReport.initColumn();
    var table = new BSTable(AppReport.id, "/appReport/list", defaultColunms);
    table.setPaginationType("server");
    AppReport.table = table.init();
});
