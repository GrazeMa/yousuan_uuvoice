/**
 * 初始化举报管理详情对话框
 */
var AppReportInfoDlg = {
    appReportInfoData : {}
};

/**
 * 清除数据
 */
AppReportInfoDlg.clearData = function() {
    this.appReportInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
AppReportInfoDlg.set = function(key, val) {
    this.appReportInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
AppReportInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
AppReportInfoDlg.close = function() {
    parent.layer.close(window.parent.AppReport.layerIndex);
}

/**
 * 收集数据
 */
AppReportInfoDlg.collectData = function() {
    this
    .set('id')
    .set('uidname')
    .set('uid')
    .set('buidname')
    .set('buid')
    .set('content')
    .set('status')
    .set('isDelete')
    .set('createTime')
    .set('hand')
    .set('role')
    .set('lockDays')
    .set('usercoding');
}

/**
 * 提交添加
 */
AppReportInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/appReport/add", function(data){
        Feng.success("添加成功!");
        window.parent.AppReport.table.refresh();
        AppReportInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.appReportInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
AppReportInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/appReport/update", function(data){
        Feng.success("修改成功!");
        window.parent.AppReport.table.refresh();
        AppReportInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    if($("#role").val()==3){//音乐
        ajax.set("isXj",$("#isXj").val());
    }
    ajax.set(this.appReportInfoData);
    ajax.set("status",2);
    ajax.start();
}

$(function() {

});
