var day_=3;//默认总榜
/**
 * 房间财富榜管理初始化
 */
var AppTreasure = {
    id: "AppTreasureTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
AppTreasure.initColumn = function () {
    return [
        {field: 'selectItem', radio: false,visible:false},
        {title: '名次', field: 'ranks', visible: true, align: 'center', valign: 'middle'},
        {title: '优优ID', field: 'usercoding', visible: true, align: 'center', valign: 'middle'},
        {title: '昵称', field: 'nickname', visible: true, align: 'center', valign: 'middle'},
        {title: '性别', field: 'sex', visible: true, align: 'center', valign: 'middle',
            formatter: function (value, row) {
                if(value==1){
                    return "男"
                }else{
                    return "女"
                }
            }
        },
        {title: '财富等级', field: 'level_', visible: true, align: 'center', valign: 'middle'},
        {title: '财富值', field: 'value_', visible: true, align: 'center', valign: 'middle'},
        {title: '距离财富值(相对上一名)', field: 'num', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 查询活动订单列表
 */
AppTreasure.search = function () {
    var queryData = {};
    queryData['usercoding'] = $("#usercoding").val();
    queryData['nickname'] = $("#nickname").val();
    queryData['day'] = day_;
    AppTreasure.table.refresh({query: queryData});
};
//导出
AppTreasure.export = function () {
    var usercoding= $("#usercoding").val();
    var nickname = $("#nickname").val();
    var day = day_;
    window.location.href=Feng.ctxPath + "/export/appRoom?usercoding="+usercoding+"&nickname="+nickname+"&type="+1+"&day="+day;

};
/*页面tab切换*/
AppTreasure.titleClick2=function(typeName,day){
    day_=day;
    //$('#AppRoomTable').bootstrapTable('destroy');
    for(var i=1;i<=3;i++){
        $("#titleDivU"+i+"").removeClass("titleItemCk");
    }
    $("#"+typeName).addClass("titleItemCk");
    AppTreasure.search();
}

$(function () {
    var defaultColunms = AppTreasure.initColumn();
    var table = new BSTable(AppTreasure.id, "/appTreasure/list", defaultColunms);
    table.setPaginationType("client");
    AppTreasure.table = table.init();
});
