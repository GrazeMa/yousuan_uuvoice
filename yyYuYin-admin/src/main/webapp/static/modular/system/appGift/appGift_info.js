/**
 * 初始化礼物管理详情对话框
 */
var AppGiftInfoDlg = {
    appGiftInfoData : {},
    validateFields: {
        name: {
            validators: {
                notEmpty: {
                    message: '礼物名称不能为空'
                }
            }
        },
        gold: {
            validators: {
                notEmpty: {
                    message: '所需优币不能为空'
                }
                ,regexp: {
                    regexp: /^[1-9]\d*$/,
                    message: '数字格式不正确'
                }
            }
        },
        yz: {
            validators: {
                notEmpty: {
                    message: '可兑换优钻不能为空'
                }
                ,regexp: {
                    regexp: /^[1-9]\d*$/,
                    message: '数字格式不正确'
                }
            }
        },
        sequence: {
            validators: {
                notEmpty: {
                    message: '排序不能为空'
                }
                ,regexp: {
                    regexp: /^[1-9]\d*$/,
                    message: '数字格式不正确'
                }
            }
        },
        day: {
            validators: {
                regexp: {
                    regexp: /^[0-9]\d*$/,
                    message: '数字格式不正确'
                }
            }
        },
    }
};
/**
 * 验证数据是否为空
 */
AppGiftInfoDlg.validate = function () {
    $('#horizontalForm').data("bootstrapValidator").resetForm();
    $('#horizontalForm').bootstrapValidator('validate');
    return $("#horizontalForm").data('bootstrapValidator').isValid();
};

/**
 * 清除数据
 */
AppGiftInfoDlg.clearData = function() {
    this.appGiftInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
AppGiftInfoDlg.set = function(key, val) {
    this.appGiftInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
AppGiftInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
AppGiftInfoDlg.close = function() {
    parent.layer.close(window.parent.AppGift.layerIndex);
}

/**
 * 收集数据
 */
AppGiftInfoDlg.collectData = function() {
    this.appGiftInfoData['status'] = $(":radio[name='status']:checked").val();
    if($("#day_").is(':checked')) {
        this.appGiftInfoData['day']=$("#day_").val();
    }else{
        this.appGiftInfoData['day']=$("#days").val();
    }
    this
    .set('id')
    .set('name')
    .set('img')
    .set('gold')
    .set('yz')
    .set('volume')
    .set('sequence')
    .set('isState')
    .set('isDelete')
    .set('imgFm')
    .set('createTime')
    .set('state');
}

/**
 * 提交添加
 */
AppGiftInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();
    if(!this.validate()){
        return;
    }
    var imgFm=$("#imgFm").val();
    if(imgFm==null || imgFm==""){
        layer.msg("请上传礼物图片");
        return;
    }
    var img=$("#img").val();
    if(img==null || img==""){
        layer.msg("请上传礼物动态图");
        return;
    }
    var day;
    if($("#day_").is(':checked')) {
        day=$("#day_").val();
    }else{
        day=$("#days").val();
    }
    if(day==null || day==''){
        layer.msg("请设置售卖天数");
        return;
    }
    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/appGift/add", function(data){
        Feng.success("添加成功!");
        window.parent.AppGift.table.refresh();
        AppGiftInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.appGiftInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
AppGiftInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();
    this.clearData();
    this.collectData();
    if(!this.validate()){
        return;
    }
    var imgFm=$("#imgFm").val();
    if(imgFm==null || imgFm==""){
        layer.msg("请上传礼物图片");
        return;
    }
    var img=$("#img").val();
    if(img==null || img==""){
        layer.msg("请上传礼物动态图");
        return;
    }
    var day;
    if($("#day_").is(':checked')) {
        day=$("#day_").val();
    }else{
        day=$("#days").val();
    }
    if(day==null || day==''){
        layer.msg("请设置售卖天数");
        return;
    }
    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/appGift/update", function(data){
        Feng.success("修改成功!");
        window.parent.AppGift.table.refresh();
        AppGiftInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.appGiftInfoData);
    ajax.start();
}

$(function() {
    Feng.initValidator("horizontalForm", AppGiftInfoDlg.validateFields);
    // 初始化图片上传
    var imageUp = new $WebUploadImage("imgFm");
    imageUp.setUploadBarId("progressBar");
    imageUp.init();
    // 初始化图片上传
    var imageUp_ = new $WebUploadImage("img");
    imageUp_.setUploadBarId("progressBar");
    imageUp_.init();
});
/*svga动图展示*/
/*
var player = new SVGA.Player('#demoCanvas');
var parser = new SVGA.Parser('#demoCanvas'); // Must Provide same selector eg:#demoCanvas IF support IE6+
parser.load($("#img").val(), function(videoItem) {
    player.setVideoItem(videoItem);
    player.startAnimation();
})*/
