/**
 * 入会申请初始化。
 */
var AppGuildJoin = {
    id: "appGuildJoinTable",	// 表格id
    seItem: null,			// 选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
AppGuildJoin.initColumn = function () {
    return [
        {field: 'selectItem', radio: false},
        	{title: 'ID', field: 'id', visible: false, align: 'center', valign: 'middle'},
        	{title: '优优ID', field: 'usercoding', visible: true, align: 'center', valign: 'middle'},
            {title: '昵称', field: 'nickname', visible: true, align: 'center', valign: 'middle'},
            {title: '性别', field: 'sex', visible: true, align: 'center', valign: 'middle',
            	formatter: function (value, row) {
            		// 性别：(1：男；2：女)。
            		if(value == 1) {
            			return "男";
            		} else if (value == 2) {
            			return "女";
            		}
            	}
            },
//            {title: '操作人昵称', field: 'nickname', visible: true, align: 'center', valign: 'middle'},
//            {title: '操作人编号', field: '正常', visible: true, align: 'center', valign: 'middle'},
//            {title: '事件类型', field: '正常', visible: true, align: 'center', valign: 'middle'},
            {title: '事件时间', field: 'createDate', visible: true, align: 'center', valign: 'middle'},
            {title: '操作', field: 'Operation', visible: true, align: 'center', valign: 'middle',
            	formatter: function (value, row) {
            		var btn = [];
                    btn += '<p style="line-height: 10px;"><a href="javascript:void(0);" onclick="AppGuildJoin.agreed(' + row.id + ')">同意申请</a></p>';
                    btn += '<p style="line-height: 10px;"><a href="javascript:void(0);" onclick="AppGuildJoin.refused(' + row.id + ')">拒绝申请</a></p>';
                    return btn;
                }
            }
    ];
};

/**
 * 检查是否选中
 */
AppGuildJoin.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        AppGuildJoin.seItem = selected;
        return true;
    }
};

/**
 * 同意入会申请。
 */
AppGuildJoin.agreed = function (id) {
	var confirm = layer.confirm("确定同意此入会申请？", {
		btn : [ '确定', '取消' ]
	}, function() {
		layer.close(confirm);
		var ajax = new $ax(Feng.ctxPath + "/appGuildJoin/agreed",
				function(data) {
					Feng.success("操作成功!");
					AppGuildJoin.table.refresh();
				}, function(data) {
					Feng.error("操作失败!" + data.responseJSON.message + "!");
				});
		ajax.set("appGuildId", id);
		ajax.start();
	});
};

/**
 * 拒绝入会申请。
 */
AppGuildJoin.refused = function (id) {
	var confirm = layer.confirm("确定拒绝此入会申请？", {
		btn : [ '确定', '取消' ]
	}, function() {
		layer.close(confirm);
		var ajax = new $ax(Feng.ctxPath + "/appGuildJoin/refused",
				function(data) {
					Feng.success("操作成功!");
					AppGuildJoin.table.refresh();
				}, function(data) {
					Feng.error("操作失败!" + data.responseJSON.message + "!");
				});
		ajax.set("appGuildId", id);
		ajax.start();
	});
};

/**
 * 查询入会申请列表
 */
AppGuildJoin.search = function () {
    var queryData = {};
    queryData['usercoding'] = $("#usercoding").val();
    queryData['nickname'] = $("#nickname").val();
    AppGuildJoin.table.refresh({query: queryData});
};

/**
 * 重置
 */
AppGuildJoin.resetSearch = function () {
    $("#usercoding").val("");
    $("#nickname").val("");
    AppGuildJoin.search();
};

$(function () {
    var defaultColunms = AppGuildJoin.initColumn();
    var table = new BSTable(AppGuildJoin.id, "/appGuildJoin/list", defaultColunms);
    table.setPaginationType("server");
    AppGuildJoin.table = table.init();
});
