/**
 * 推荐位置管理管理初始化
 */
var AppRecommend = {
    id: "AppRecommendTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
AppRecommend.initColumn = function () {
    return [
        {field: 'selectItem', radio: true,visible:false},
        {title: '推荐类型', field: 'state', visible: true, align: 'center', valign: 'middle',
            formatter: function (value, row) {
                //1钻石 2分成
                if(value==1){
                    return "默认推荐"
                }else {
                    return "用户申请"
                }
            }
        },
        {title: '展示开始时间', field: 'sTime', visible: true, align: 'center', valign: 'middle'},
        {title: '展示结束时间', field: 'eTime', visible: true, align: 'center', valign: 'middle'},
        {title: '展示房间ID', field: 'rid', visible: true, align: 'center', valign: 'middle'},
        {title: '房主昵称', field: 'nickName', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 查询推荐申请记录列表
 */
AppRecommend.search = function () {
    var queryData = {};
    queryData['beginTime'] = $("#beginTime").val();
    queryData['endTime'] = $("#endTime").val();
    queryData['rid'] = $("#rid").val();
    queryData['nickName'] = $("#nickName").val();
    AppRecommend.table.refresh({query: queryData});
};
/**
 * 重置
 */
AppRecommend.resetSearch = function () {
    $("#beginTime").val("");
    $("#endTime").val("");
    $("#rid").val("");
    $("#nickName").val("");
    AppRecommend.search();
};

$(function () {
    var sequence=$("#id").val();
    var defaultColunms = AppRecommend.initColumn();
    var table = new BSTable(AppRecommend.id, "/appRecommend/appRecommendHistory?sequence="+sequence, defaultColunms);
    table.setPaginationType("server");
    AppRecommend.table = table.init();

});
