/**
 * 初始化推荐位置管理详情对话框
 */
var AppRecommendInfoDlg = {
    appRecommendInfoData : {}
};

/**
 * 清除数据
 */
AppRecommendInfoDlg.clearData = function() {
    this.appRecommendInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
AppRecommendInfoDlg.set = function(key, val) {
    this.appRecommendInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
AppRecommendInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
AppRecommendInfoDlg.close = function() {
    parent.layer.close(window.parent.AppRecommend.layerIndex);
}

/**
 * 收集数据
 */
AppRecommendInfoDlg.collectData = function() {
    this
    .set('id')
    .set('uid')
    .set('sTime')
    .set('eTime')
    .set('createTime');
}

/**
 * 提交添加
 */
AppRecommendInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();
    if($("#name").text()==null || $("#name").text()==''){
        Feng.info("没有查到对应的房间信息");
        return ;
    }
    if($("#rid").val()==null || $("#rid").val()=='' ){
        Feng.info("请输入房间id");
        return ;
    }
    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/appRecommend/add", function(data){
        if(data==1){
            Feng.info("该房间已在推荐位上");
        }else {
            Feng.success("添加成功!");
            AppRecommendInfoDlg.close();
            parent.location.reload();
        }
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set("id",$("#id").val());
    ajax.set("rid",$("#rid").val());
    ajax.start();

}

/**
 * 提交修改
 */
AppRecommendInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/appRecommend/update", function(data){
        Feng.success("修改成功!");
        AppRecommendInfoDlg.close();
        parent.location.reload();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.appRecommendInfoData);
    ajax.start();
}
/**
 * 输入房间号查出房间姓名和房子昵称
 */
AppRecommendInfoDlg.changeRid=function(e){
    var ajax = new $ax(Feng.ctxPath + "/appRecommend/changeRid", function(data){
        if(data!=null){
            $("#name").text(data.roomName);
            $("#nickName").text(data.name);
        }
    },function(data){
        Feng.error("获取失败!" + data.responseJSON.message + "!");
    });
    ajax.set("rid",$(e).val());
    ajax.start();
}

$(function() {

});
