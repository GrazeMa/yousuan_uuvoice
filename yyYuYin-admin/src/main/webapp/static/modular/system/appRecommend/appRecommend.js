/**
 * 推荐位置管理管理初始化
 */
var AppRecommend = {
    id: "AppRecommendTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
AppRecommend.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
            {title: '', field: 'id', visible: true, align: 'center', valign: 'middle'},
            {title: '房主id', field: 'uid', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'sTime', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'eTime', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'createTime', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
AppRecommend.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        AppRecommend.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加推荐位置管理
 */
AppRecommend.openAddAppRecommend = function (id) {
    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/appRecommend/isUpdate", function(data){
        if(data==1){
            Feng.info("该推荐位还没有到期，请稍后设置");
        }else if(data==2){
            Feng.info("该推荐位已有用户预约，请稍后设置");
        }else {
            var index = layer.open({
                type: 2,
                title: '添加推荐位置管理',
                area: ['800px', '420px'], //宽高
                fix: false, //不固定
                maxmin: true,
                content: Feng.ctxPath + '/appRecommend/appRecommend_add?id='+id
            });
            this.layerIndex = index;
        }
    },function(data){

    });
    ajax.set("id",id);
    ajax.start();


};

/**
 * 打开查看推荐位置管理详情
 */
AppRecommend.openAppRecommendDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '推荐位置管理详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/appRecommend/appRecommend_update/' + AppRecommend.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除推荐位置管理
 */
AppRecommend.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/appRecommend/delete", function (data) {
            Feng.success("删除成功!");
            AppRecommend.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("appRecommendId",this.seItem.id);
        ajax.start();
    }
};

AppRecommend.state = function (id) {

        var ajax = new $ax(Feng.ctxPath + "/appRecommend/update", function (data) {
            Feng.success("开放成功!");
            window.location.reload();
        }, function (data) {
            Feng.error("开放成功!" + data.responseJSON.message + "!");
        });
        ajax.set("id",id);
        ajax.set("isDefault",1);
        ajax.start();

};
/**
 * 打开查看房间管理详情
 */
AppRecommend.openAppRoomDetail = function (id) {
    var index = layer.open({
        type: 2,
        title: '房间管理详情',
        area: ['90%', '90%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/appRoom/appRoom_update/' + id
    });
    this.layerIndex = index;
};
/**
 * 查看栏位历史推荐
 */
AppRecommend.appRecommendHistory=function(id){
    var index = layer.open({
        type: 2,
        title: '查看栏位历史推荐',
        area: ['90%', '90%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/appRecommend/appRecommend_history?id=' + id
    });
    this.layerIndex = index;
}
/**
 * 查看申请记录
 */
AppRecommend.appRecommendClaimer=function(){
    var index = layer.open({
        type: 2,
        title: '查看申请记录',
        area: ['90%', '90%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/appClaimer'
    });
    this.layerIndex = index;
}
/**
 * 查询推荐位置管理列表
 */
AppRecommend.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    AppRecommend.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = AppRecommend.initColumn();
    var table = new BSTable(AppRecommend.id, "/appRecommend/list", defaultColunms);
    table.setPaginationType("client");
    AppRecommend.table = table.init();

    //单选按钮点击事件
    $('input[type=radio][name=speed]').change(function() {
        var ajax = new $ax(Feng.ctxPath + "/appRecommend/updateSpeed", function (data) {
            Feng.success("设置成功!");
        }, function (data) {
            Feng.error("设置成功!" + data.responseJSON.message + "!");
        });
        ajax.set("id",4);
        ajax.set("x",$(this).val());
        ajax.start();
    });
});
