/**
 * 优币兑换管理初始化
 */
var AppYbexchange = {
    id: "AppYbexchangeTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
AppYbexchange.initColumn = function () {
    return [
        {field: 'selectItem', radio: true,visible:false},
        {title: '', field: 'id', visible: false, align: 'center', valign: 'middle'},
        {title: '兑换时间', field: 'createTime', visible: true, align: 'center', valign: 'middle'},
        {title: '兑换用户', field: 'name', visible: true, align: 'center', valign: 'middle'},
        {title: '优优ID', field: 'eid', visible: true, align: 'center', valign: 'middle'},
        {title: '兑换钻石', field: 'dhDiamond', visible: true, align: 'center', valign: 'middle'},
        {title: '获得优币', field: 'hdGold', visible: true, align: 'center', valign: 'middle'},
        {title: '其他奖励', field: 'qtGold', visible: true, align: 'center', valign: 'middle'}

    ];
};
//导出
AppYbexchange.export = function () {
    var beginTime= $("#beginTime").val();
    var endTime = $("#endTime").val();
    var eid = $("#eid").val();
    var name = $("#name").val();
    window.location.href=Feng.ctxPath + "/export/appYbexchange?beginTime="+beginTime+"&endTime="+endTime+"&eid="+eid+"&name="+name;

};
/**
 * 查询优币充值列表
 */
AppYbexchange.search = function () {
    var queryData = {};
    queryData['beginTime'] = $("#beginTime").val();
    queryData['endTime'] = $("#endTime").val();
    queryData['eid'] = $("#eid").val();
    queryData['name'] = $("#name").val();
    AppYbexchange.table.refresh({query: queryData});
};
/**
 * 重置
 */
AppYbexchange.resetSearch = function () {
    $("#beginTime").val("");
    $("#endTime").val("");
    $("#eid").val("");
    $("#name").val("");
    AppYbexchange.search();
};
$(function () {
    var defaultColunms = AppYbexchange.initColumn();
    var table = new BSTable(AppYbexchange.id, "/appYbexchange/list", defaultColunms);
    table.setPaginationType("client");
    AppYbexchange.table = table.init();
});
