/**
 * 道具管理管理初始化
 */
var AppScene = {
    id: "AppSceneTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
AppScene.initColumn = function () {
    return [
        {field: 'selectItem', radio: false},
            {title: '上架时间', field: 'addTime', visible: true, align: 'center', valign: 'middle'},
            {title: '道具名称', field: 'name', visible: true, align: 'center', valign: 'middle'},
            {title: '道具图片', field: 'imgFm', visible: true, align: 'center', valign: 'middle',
                formatter: function (value, row) {
                    if (value == null || value == '') {
                        return '<a class = "view"  href="javascript:void(0)"><img  style="width: 50px;height:50px;" src="' + Feng.ctxPath + '/static/img/NoPIC.png" /></a>';
                    } else {
                        return '<a class = "view"  href="javascript:void(0)"><img  style="width: 50px;height:50px;" src="'  + value + '" /></a>';
                    }
                },
                events: 'operateEvents'
            },
            {title: '道具动态图', field: 'img', visible: true, align: 'center', valign: 'middle',
                formatter: function (value, row) {
                    if(value.indexOf(".svga")!=-1){
                        return "暂不支持svga格式的图片展示";
                    }else {
                        if (value == null || value == '') {
                            return '<a class = "view"  href="javascript:void(0)"><img  style="width: 50px;height:50px;" src="' + Feng.ctxPath + '/static/img/NoPIC.png" /></a>';
                        } else {
                            return '<a class = "view"  href="javascript:void(0)"><img  style="width: 50px;height:50px;" src="' + value + '" /></a>';
                        }
                    }
                },
                events: 'operateEvents'
                /*formatter: function (value, row) {
                    var btn=[];
                    if (value == null || value == '') {
                        return '<a class = "view"  href="javascript:void(0)"><img  style="width: 50px;height:50px;" src="' + Feng.ctxPath + '/static/img/NoPIC.png" /></a>';
                    } else {
                        if(value.indexOf(".svga") != -1){//svga图片
                            btn+='<a id="imgDiv'+row.id+'" src="'+value+'" onclick="AppScene.showSvga('+row.id+')">查看</a>';
                        }else{
                            return '<a class = "view"  href="javascript:void(0)">查看</a>';
                        }
                    }
                    return btn;
                },
                events: 'operateEvents'*/
            },
            {title: '需要优币', field: 'gold', visible: true, align: 'center', valign: 'middle'},
            {title: '类型', field: 'state', visible: true, align: 'center', valign: 'middle',
                formatter: function (value, row) {
                    //1座驾，2 是头环
                    if(value==1){
                        return "座驾"
                    }else if(value==2){
                        return "头环";
                    }
                }
            },
            {title: '剩余售卖天数', field: 'day', visible: true, align: 'center', valign: 'middle',
                formatter: function (value, row) {
                    if(value==-1){
                        return "不限制"
                    }else if(value>=0){
                        return value+"天"
                    }
                }
            },
            {title: '排序', field: 'sequence', visible: true, align: 'center', valign: 'middle'},
            {title: '销量', field: 'volume', visible: true, align: 'center', valign: 'middle'},
            {title: '状态', field: 'isState', visible: true, align: 'center', valign: 'middle',
                formatter: function (value, row) {
                    //1 销售 ,2 下架
                    if(value==1){
                        return "销售中"
                    }else if(value==2){
                        return "已下架";
                    }
                }
            },
        {title: '操作', field: 'opt', visible: true, align: 'left', valign: 'middle',
            formatter: function (value, row) {
                var btn=[];
                if(row.isState==1){
                    btn+='<p style="line-height: 10px;"><a href="javascript:void(0);" onclick="AppScene.isXiajia('+row.id+',2)">下架道具</a></p>';
                }else{
                    btn+='<p style="line-height: 10px;"><a href="javascript:void(0);" onclick="AppScene.openAppSceneDetail('+row.id+')">编辑</a></p>';
                    btn+='<p style="line-height: 10px;"><a href="javascript:void(0);" onclick="AppScene.isXiajia('+row.id+',1)">上架道具</a></p>';
                }
                return btn;

            }
        }

    ];
};
/*查看svga动画图*/
AppScene.showSvga=function(id){
    var imgUrl=$("#imgDiv"+id+"").attr("src");
    var content="";
    if(imgUrl != "") {
        imgUrl = imgUrl;// 设置图片路径
        content ='<div id="demoCanvas'+id+'" ></div>';
    } else {
        imgUrl = Feng.ctxPath + '/static/img/NoPIC.png';// 默认无图
        content='<img src="' + imgUrl + '" height="100%" width="100%" />';
    }
    layer.open({
        type: 1,
        title: false,
        closeBtn: 0,
        area: ['90%', '80%'], //宽高
        skin: 'layui-layer-nobg', //没有背景色
        shadeClose: true,
        content: content
    });

    /*svga动图展示*/
    var player = new SVGA.Player("#demoCanvas"+id+"");
    var parser = new SVGA.Parser("#demoCanvas"+id+""); // Must Provide same selector eg:#demoCanvas IF support IE6+
    parser.load(imgUrl, function(videoItem) {
        player.setVideoItem(videoItem);
        player.startAnimation();
    });
}
/**
 * 检查是否选中
 */
AppScene.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        AppScene.seItem = selected;
        return true;
    }
};

/**
 * 点击添加道具管理
 */
AppScene.openAddAppScene = function () {
    var index = layer.open({
        type: 2,
        title: '添加道具管理',
        area: ['90%', '90%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/appScene/appScene_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看道具管理详情
 */
AppScene.openAppSceneDetail = function (id) {
    var index = layer.open({
        type: 2,
        title: '编辑道具管理',
        area: ['90%', '90%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/appScene/appScene_update/' + id
    });
    this.layerIndex = index;
};

/**
 * 删除道具管理
 */
AppScene.delete = function () {
    if (this.check()) {
        var confirm = layer.confirm('确定要删除该数据？', { btn: ['确定','取消'] }, function(){
            layer.close(confirm);
            var ids="";
            $.each(AppScene.seItem, function(i,val){
                ids += val.id+",";
            });
            ids = ids.substring(0, ids.length - 1);
            var ajax = new $ax(Feng.ctxPath + "/appScene/delete", function (data) {
                Feng.success("删除成功!");
                AppScene.table.refresh();
            }, function (data) {
                Feng.error("删除失败!" + data.responseJSON.message + "!");
            });
            ajax.set("appSceneId",ids);
            ajax.start();
        })
    }
};
/**
 * 上架和下架
 */
AppScene.isXiajia = function (id,state) {
    var stateName = "下架";
    if (state == 1) {
        stateName = "上架";
    }
    var confirm = layer.confirm("确定要"+stateName+"该道具？", {btn: ['确定', '取消']}, function () {
        layer.close(confirm);
        var ajax = new $ax(Feng.ctxPath + "/appScene/updateXiajia", function (data) {
            Feng.success(""+stateName+"成功!");
            AppScene.table.refresh();
        }, function (data) {
            Feng.error(""+stateName+"失败!" + data.responseJSON.message + "!");
        });
        ajax.set("ids",id);
        ajax.set("isState",state);
        ajax.start();
    })
};
/**
 * 查询道具管理列表
 */
AppScene.search = function () {
    var queryData = {};
    queryData['beginTime'] = $("#beginTime").val();
    queryData['endTime'] = $("#endTime").val();
    queryData['name'] = $("#name").val();
    queryData['state'] = $("#state").val();
    queryData['isState'] = $("#isState").val();
    AppScene.table.refresh({query: queryData});
};
/**
 * 重置
 */
AppScene.resetSearch = function () {
    $("#beginTime").val("");
    $("#endTime").val("");
    $("#name").val("");
    $("#state").val("");
    $("#isState").val("");
    AppScene.search();
};
$(function () {
    var defaultColunms = AppScene.initColumn();
    var table = new BSTable(AppScene.id, "/appScene/list", defaultColunms);
    table.setPaginationType("server");
    AppScene.table = table.init();
});
