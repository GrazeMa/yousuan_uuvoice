/**
 * 初始化道具管理详情对话框
 */
var AppSceneInfoDlg = {
    appSceneInfoData : {},
    validateFields: {
        name: {
            validators: {
                notEmpty: {
                    message: '道具名称不能为空'
                }
            }
        },
        gold: {
            validators: {
                notEmpty: {
                    message: '所需优币不能为空'
                }
                ,regexp: {
                    regexp: /^[1-9]\d*$/,
                    message: '数字格式不正确'
                }
            }
        },
        sequence: {
            validators: {
                notEmpty: {
                    message: '排序不能为空'
                }
                ,regexp: {
                    regexp: /^[1-9]\d*$/,
                    message: '数字格式不正确'
                }
            }
        },
        day: {
            validators: {
                regexp: {
                    regexp: /^[0-9]\d*$/,
                    message: '数字格式不正确'
                }
            }
        },
    }
};
/**
 * 验证数据是否为空
 */
AppSceneInfoDlg.validate = function () {
    $('#horizontalForm').data("bootstrapValidator").resetForm();
    $('#horizontalForm').bootstrapValidator('validate');
    return $("#horizontalForm").data('bootstrapValidator').isValid();
};
/**
 * 清除数据
 */
AppSceneInfoDlg.clearData = function() {
    this.appSceneInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
AppSceneInfoDlg.set = function(key, val) {
    this.appSceneInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
AppSceneInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
AppSceneInfoDlg.close = function() {
    parent.layer.close(window.parent.AppScene.layerIndex);
}

/**
 * 收集数据
 */
AppSceneInfoDlg.collectData = function() {
    if($("#day_").is(':checked')) {
        this.appSceneInfoData['day']=$("#day_").val();
    }else{
        this.appSceneInfoData['day']=$("#days").val();
    }
    this
    .set('id')
    .set('name')
    .set('img')
    .set('gold')
    .set('imgFm')
    .set('createTime')
    .set('state')
    .set('sequence')
    .set('isState')
    .set('isDelete')
    .set('volume');
}

/**
 * 提交添加
 */
AppSceneInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();
    if(!this.validate()){
        return;
    }
    var imgFm=$("#imgFm").val();
    if(imgFm==null || imgFm==""){
        layer.msg("请上传道具图片");
        return;
    }
    var img=$("#img").val();
    if(img==null || img==""){
        layer.msg("请上传道具动态图");
        return;
    }
    var day;
    if($("#day_").is(':checked')) {
        day=$("#day_").val();
    }else{
        day=$("#days").val();
    }
    if(day==null || day==''){
        layer.msg("请设置售卖天数");
        return;
    }

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/appScene/add", function(data){
        Feng.success("添加成功!");
        window.parent.AppScene.table.refresh();
        AppSceneInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.appSceneInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
AppSceneInfoDlg.editSubmit = function() {
    this.clearData();
    this.collectData();
    if(!this.validate()){
        return;
    }
    var imgFm=$("#imgFm").val();
    if(imgFm==null || imgFm==""){
        layer.msg("请上传道具图片");
        return;
    }
    var img=$("#img").val();
    if(img==null || img==""){
        layer.msg("请上传道具动态图");
        return;
    }
    var day;
    if($("#day_").is(':checked')) {
        day=$("#day_").val();
    }else{
        day=$("#days").val();
    }
    if(day==null || day==''){
        layer.msg("请设置售卖天数");
        return;
    }

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/appScene/update", function(data){
        Feng.success("修改成功!");
        window.parent.AppScene.table.refresh();
        AppSceneInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.appSceneInfoData);
    ajax.start();
}

$(function() {
    Feng.initValidator("horizontalForm", AppSceneInfoDlg.validateFields);
    // 初始化图片上传
    var imageUp = new $WebUploadImage("imgFm");
    imageUp.setUploadBarId("progressBar");
    imageUp.init();
    // 初始化图片上传
    var imageUp_ = new $WebUploadImage("img");
    imageUp_.setUploadBarId("progressBar");
    imageUp_.init();
});
/*svga动图展示*/
/*
var player = new SVGA.Player('#demoCanvas');
var parser = new SVGA.Parser('#demoCanvas'); // Must Provide same selector eg:#demoCanvas IF support IE6+
parser.load($("#img").val(), function(videoItem) {
    player.setVideoItem(videoItem);
    player.startAnimation();
})*/
