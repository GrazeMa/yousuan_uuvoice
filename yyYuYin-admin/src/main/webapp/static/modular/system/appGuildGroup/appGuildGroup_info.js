/**
 * 初始化公会管理详情对话框。
 */
var AppGuildGroupInfoDlg = {
	appGuildGroupData : {}
};

/**
 * 清除数据
 */
AppGuildGroupInfoDlg.clearData = function() {
	this.appGuildGroupData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
AppGuildGroupInfoDlg.set = function(key, val) {
	this.appGuildGroupData[key] = (typeof val == "undefined") ? $("#" + key)
			.val() : val;
	return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
AppGuildGroupInfoDlg.get = function(key) {
	return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
AppGuildGroupInfoDlg.close = function() {
	parent.layer.close(window.parent.AppGuildGroup.layerIndex);
}

/**
 * 收集数据
 */
AppGuildGroupInfoDlg.collectData = function() {
	this.set('id').set('groupId').set('groupName').set('status').set('createDate').set('remarks');
}

/**
 * 提交添加
 */
AppGuildGroupInfoDlg.addSubmit = function() {

	this.clearData();
	this.collectData();

	//提交信息
	var ajax = new $ax(Feng.ctxPath + "/appGuildGroup/add", function(data) {
		Feng.success("添加成功!");
		window.parent.AppGuildGroup.table.refresh();
		AppGuildGroupInfoDlg.close();
	}, function(data) {
		Feng.error("添加失败!" + data.responseJSON.message + "!");
	});
	ajax.set(this.appGuildGroupData);
	ajax.start();
	
}

/**
 * 提交修改
 */
AppGuildGroupInfoDlg.editSubmit = function() {

	this.clearData();
	this.collectData();

	//提交信息
	var ajax = new $ax(Feng.ctxPath + "/appGuildGroup/update", function(data) {
		Feng.success("修改成功!");
		window.parent.AppGuildGroup.table.refresh();
		AppGuildGroupInfoDlg.close();
	}, function(data) {
		Feng.error("修改失败!" + data.responseJSON.message + "!");
	});
	ajax.set(this.appGuildGroupData);
	ajax.start();
}

$(function() {

});
