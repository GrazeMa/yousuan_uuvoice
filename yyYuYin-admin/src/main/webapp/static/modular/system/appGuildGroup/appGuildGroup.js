/**
 * 公会管理初始化。
 */
var AppGuildGroup = {
    id: "appGuildGroupTable",	// 表格id
    seItem: null,			// 选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列。
 */
AppGuildGroup.initColumn = function () {
    return [
        {field: 'selectItem', radio: false},
        	{title: 'ID', field: 'id', visible: false, align: 'center', valign: 'middle'},
            {title: '公会ID', field: 'groupId', visible: false, align: 'center', valign: 'middle'},
            {title: '分组名称', field: 'groupName', visible: true, align: 'center', valign: 'middle'},
            {title: '备注', field: 'remarks', visible: true, align: 'center', valign: 'middle'},
//            {title: '状态', field: 'status', visible: true, align: 'center', valign: 'middle',
//            	formatter: function (value, row) {
//                    // 状态（0：无效；1：有效。）
//                    if(value == 0) {
//                        return "无效";
//                    } else if(value == 1) {
//                        return "有效";
//                    }
//                }
//            },
            {title: '创建时间', field: 'createDate', visible: true, align: 'center', valign: 'middle'},
            {title: '操作', field: 'Operation', visible: true, align: 'center', valign: 'middle',
            	formatter: function (value, row) {
            		var btn = [];
                    btn += '<p style="line-height: 10px;"><a href="javascript:void(0);" onclick="AppGuildGroup.openAppGuildGroupDetail(' + row.id + ')">编辑</a></p>';
                    btn += '<p style="line-height: 10px;"><a href="javascript:void(0);" onclick="AppGuildGroup.delete_(' + row.id + ')">删除</a></p>';
                    return btn;
                }
            }
    ];
};

/**
 * 检查是否选中。
 */
AppGuildGroup.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        AppGuildGroup.seItem = selected;
        return true;
    }
};

/**
 * 删除单个分组。
 * @param id
 * @private
 */
AppGuildGroup.delete_ = function(id) {
	var confirm = layer.confirm("确定要删除此分组？", {
		btn : [ '确定', '取消' ]
	}, function() {
		layer.close(confirm);
		var ajax = new $ax(Feng.ctxPath + "/appGuildGroup/delete",
				function(data) {
					Feng.success("删除成功!");
					AppGuildGroup.table.refresh();
				}, function(data) {
					Feng.error("删除失败!" + data.responseJSON.message + "!");
				});
		ajax.set("appGuildGroupId", id);
		ajax.start();
	});
}

/**
 * 批量删除选中分组。
 */
AppGuildGroup.delete = function () {
    if (this.check()) {
        var ids = "";
        $.each(AppGuildGroup.seItem, function(i, val){
            ids += val.id + ",";
        });
        ids = ids.substring(0, ids.length - 1);
        var confirm = layer.confirm("确定要删除这些分组？", {btn: ['确定', '取消']}, function () {
            layer.close(confirm);
            var ajax = new $ax(Feng.ctxPath + "/appGuildGroup/delete", function (data) {
                Feng.success("删除成功!");
                AppGuildGroup.table.refresh();
            }, function (data) {
                Feng.error("删除失败!" + data.responseJSON.message + "!");
            });
            ajax.set("appGuildGroupId", ids);
            ajax.start();
        })
    }
};

/**
 * 点击添加公会分组。
 */
AppGuildGroup.openAddAppGuildGroup = function () {
    var index = layer.open({
        type: 2,
        title: '添加房间背景图',
        area: ['80%', '80%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/appGuildGroup/appGuildGroup_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看分组详情。
 */
AppGuildGroup.openAppGuildGroupDetail = function (id) {
    var index = layer.open({
        type: 2,
        title: '分组详情',
        area: ['80%', '80%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/appGuildGroup/appGuildGroup_update/' + id
    });
    this.layerIndex = index;
};

/**
 * 查询公会组管理列表。
 */
AppGuildGroup.search = function () {
    var queryData = {};
    queryData['groupId'] = $("#groupId").val();
    queryData['groupName'] = $("#groupName").val();
    AppGuildGroup.table.refresh({query: queryData});
};

/**
 * 重置。
 */
AppGuildGroup.resetSearch = function () {
    $("#groupName").val("");
    AppGuildGroup.search();
};

$(function () {
    var defaultColunms = AppGuildGroup.initColumn();
    var table = new BSTable(AppGuildGroup.id, "/appGuildGroup/list", defaultColunms);
    table.setPaginationType("server");
    AppGuildGroup.table = table.init();
    AppGuildGroup.search();
//    setTimeout(function(){ AppGuildGroup.search(); }, 1000);
});
