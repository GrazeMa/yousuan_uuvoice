/**
 * 房间流水管理初始化
 */
var AppYbconsume = {
    id: "AppYbconsumeTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
AppYbconsume.initColumn = function () {
    return [
        {field: 'selectItem', radio: true,visible:false},
        {title: '', field: 'id', visible: false, align: 'center', valign: 'middle'},
        {title: '房间名称', field: 'roomName', visible: true, align: 'center', valign: 'middle'},
        {title: '房间ID', field: 'rid', visible: true, align: 'center', valign: 'middle'},
        {title: '房主昵称', field: 'name', visible: true, align: 'center', valign: 'middle'},
        {title: '是否为牌照房间', field: 'state', visible: true, align: 'center', valign: 'middle',
            formatter: function (value, row) {
                //是否牌子房间 1否，2是
                if(value==1){
                    return "否"
                }else if(value==2){
                    return "是"
                }
            }
        },
        {title: '本周使用优币', field: 'consumeNum1', visible: true, align: 'center', valign: 'middle',sortable: true},
        {title: '上周使用优币', field: 'consumeNum2', visible: true, align: 'center', valign: 'middle',sortable: true},
        {title: '本月使用优币', field: 'consumeNum3', visible: true, align: 'center', valign: 'middle',sortable: true},
        {title: '上月使用优币', field: 'consumeNum4', visible: true, align: 'center', valign: 'middle',sortable: true},
        {title: '历史使用优币', field: 'consumeNum5', visible: true, align: 'center', valign: 'middle',sortable: true}

    ];
};

//导出
AppYbconsume.export = function () {
    var roomName= $("#roomName").val();
    var rid = $("#rid").val();
    var name = $("#name").val();
    var state = $("#state").val();
    window.location.href=Feng.ctxPath + "/export/appYbconsumeRoom?roomName="+roomName+"&rid="+rid+"&name="+name+"&state="+state;
};

/**
 * 查询优币充值列表
 */
AppYbconsume.search = function () {
    var queryData = {};
    queryData['roomName'] = $("#roomName").val();
    queryData['rid'] = $("#rid").val();
    queryData['name'] = $("#name").val();
    queryData['state'] = $("#state").val();
    AppYbconsume.table.refresh({query: queryData});
};
/**
 * 重置
 */
AppYbconsume.resetSearch = function () {
    $("#roomName").val("");
    $("#rid").val("");
    $("#name").val("");
    $("#state").val("");
    AppYbconsume.search();
};
$(function () {
    var defaultColunms = AppYbconsume.initColumn();
    var table = new BSTable(AppYbconsume.id, "/appYbconsume/room/list", defaultColunms);
    table.setPaginationType("client");
    AppYbconsume.table = table.init();
});
