/**
 * 初始化用户管理详情对话框
 */
var AppUserInfoDlg = {
    appUserInfoData : {},
    validateFields: {
        password: {
            validators: {
                notEmpty: {
                    message: '重置密码不能为空'
                }
            }
        },
        gold: {
            validators: {
               regexp: {
                    regexp: /^[1-9]\d*$/,
                    message: '数字格式不正确'
                }
            }
        },
        ynum: {
            validators: {
               regexp: {
                    regexp: /^[1-9]\d*$/,
                    message: '数字格式不正确'
                }
            }
        },
        recommendCount: {
            validators: {
                regexp: {
                    regexp: /^[1-9]\d*$/,
                    message: '数字格式不正确'
                }
            }
        },
    }
};
/**
 * 验证数据是否为空
 */
AppUserInfoDlg.validate = function () {
    $('#horizontalForm').data("bootstrapValidator").resetForm();
    $('#horizontalForm').bootstrapValidator('validate');
    return $("#horizontalForm").data('bootstrapValidator').isValid();
};
/**
 * 清除数据
 */
AppUserInfoDlg.clearData = function() {
    this.appUserInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
AppUserInfoDlg.set = function(key, val) {
    this.appUserInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
AppUserInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
AppUserInfoDlg.close = function() {
    parent.layer.close(window.parent.AppUser.layerIndex);
}

/**
 * 收集数据
 */
AppUserInfoDlg.collectData = function() {
    this
    .set('id')
    .set('createDate')
    .set('usercoding')
    .set('sex')
    .set('imgTx')
    .set('charmGrade')
    .set('treasureGrade')
    .set('gift')
    .set('gold')
    .set('ynum')
    .set('dateOfBirth')
    .set('age')
    .set('password')
    .set('phone')
    .set('state')
    .set('nickname')
    .set('jd')
    .set('wd')
    .set('gzSid')
    .set('wxSid')
    .set('qqSid')
    .set('status')
    .set('historyRecharge')
    .set('historyDeposit')
    .set('individuation')
    .set('voice')
    .set('roomName')
    .set('constellation')
    .set('fansNum')
    .set('attentionNum')
    .set('goldNum')
    .set('recommendCount')
    .set('yuml');
}

/**
 * 提交添加
 */
AppUserInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/appUser/add", function(data){
        Feng.success("添加成功!");
        window.parent.AppUser.table.refresh();
        AppUserInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.appUserInfoData);
    ajax.start();
}

/**
 * 提交修改 重置密码 修改优币和优钻
 */
AppUserInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();
    if(!this.validate()){
        return;
    }
    var type_=$("#type_").val();
    if(type_==2){
        if(($("#gold").val()==null || $("#gold").val()=='') && ($("#ynum").val()==null || $("#ynum").val()=='') && ($("#recommendCount").val()==null || $("#recommendCount").val()=='')){
            Feng.info("至少要填一项");
            return;
        }
    }
    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/appUser/update", function(data){
        Feng.success("修改成功!");
        window.parent.AppUser.table.refresh();
        AppUserInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.appUserInfoData);
    ajax.set("type_",type_);
    ajax.start();
}
/**
 * 获取音频时长
 */
var size=0;
var ref;
AppUserInfoDlg.getVoice=function(){
    $("#voice")[0].play();
    var myVid=$("#voice")[0].duration;
    size=Math.ceil(myVid);
    if(size>0){
        ref = window.setInterval("AppUserInfoDlg.size_()",1000);
    }
}
AppUserInfoDlg.size_=function(){
    size--;
    if(size>=0){
        $("#voiceSize").text(size+"\"");
    }else{
        clearInterval(ref);
    }
}
//查看图片
AppUserInfoDlg.ckPicture=function(e){
    var imgUrl=$(e).attr("src");
    layer.open({
        type: 1,
        title: false,
        closeBtn: 0,
        area: 'auto',
        skin: 'layui-layer-nobg', //没有背景色
        shadeClose: true,
        content: '<img src="' + imgUrl + '" height="100%" width="100%" />'
    });
}

$(function() {
    Feng.initValidator("horizontalForm", AppUserInfoDlg.validateFields);
});
