/**
 * 用户管理管理初始化
 */
var AppUser = {
    id: "AppUserTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
AppUser.initColumn = function () {
    return [
        {field: 'selectItem', radio: false},
        {title: '注册时间', field: 'createDate', visible: true, align: 'center', valign: 'middle'},
        {title: '昵称', field: 'nickname', visible: true, align: 'center', valign: 'middle'},
        {title: '手机号', field: 'phone', visible: true, align: 'center', valign: 'middle'},
        {title: '优优ID', field: 'usercoding', visible: true, align: 'center', valign: 'middle'},
        {title: '性别', field: 'sexName', visible: true, align: 'center', valign: 'middle'},
        {title: '财富等级', field: 'treasureGrade', visible: true, align: 'center', valign: 'middle',
            formatter: function (value, row) {
                return "LV"+value;
            }
        },
        {title: '魅力等级', field: 'charmGrade', visible: true, align: 'center', valign: 'middle',
            formatter: function (value, row) {
                return "*"+value;
            }
        },
        {title: '优币数', field: 'gold', visible: true, align: 'center', valign: 'middle'},
        {title: '优钻数', field: 'ynum', visible: true, align: 'center', valign: 'middle'},
        {title: '推荐次数', field: 'recommendCount', visible: true, align: 'center', valign: 'middle'},
        {title: '历史充值', field: 'ybrecharge', visible: true, align: 'center', valign: 'middle'},
        {title: '历史提现', field: 'historyDeposit', visible: true, align: 'center', valign: 'middle'},
        {title: '是否开通房间', field: 'statusName', visible: true, align: 'center', valign: 'middle'},
        {title: '状态', field: 'stateName', visible: true, align: 'center', valign: 'middle'},
        {title: '操作', field: 'opt', visible: true, align: 'left', valign: 'middle',width:'10%',
            formatter: function (value, row) {
                var btn="";
                btn+='<p style="line-height: 10px;"><a href="javascript:void(0);" onclick="AppUser.openAppUserDetail('+row.id+')">详情</a>&nbsp;&nbsp;';
                if(row.state == 1 ){//是否冻结 1否,2是
                    btn+='<a href="javascript:void(0);" onclick="AppUser.isFreeze_('+row.id+',2)">禁用</a></p>';
                }else{
                    btn+='<a href="javascript:void(0);" onclick="AppUser.isFreeze_('+row.id+',1)">解禁</a></p>';
                }
                btn+='<p style="line-height: 10px;"><a href="javascript:void(0);" onclick="AppUser.inviteOrFengDetail('+row.id+',1)">邀请明细</a><p>';
                btn+='<p style="line-height: 10px;"><a href="javascript:void(0);" onclick="AppUser.inviteOrFengDetail('+row.id+',2)">分成明细</a><p>';
                btn+='<p style="line-height: 10px;"><a href="javascript:void(0);" onclick="AppUser.resetPwdOrupdate('+row.id+',1)">重置密码</a><p>';
                btn+='<p style="line-height: 10px;"><a href="javascript:void(0);" onclick="AppUser.resetPwdOrupdate('+row.id+',2)">修改优币/优钻/推荐次数</a><p>';

                var btn1='<button type="button" class="btn btn-primary button-margin button" onclick="AppUser.toggle(this)" id="'+row.id+'">操作' +
                    '</button><div style="display: none">'+btn+'</div>';
                return btn1;

            }
        },
        {title: '记录', field: 'opt', visible: true, align: 'left', valign: 'middle',
            formatter: function (value, row) {
                var btn=[];
                btn.push('<a href="javascript:void(0);" onclick="AppUser.recordById('+row.id+')">操作记录</a>')
                return btn;
            }
        }

    ];
};
/*切换*/
AppUser.toggle=function(e){
    $(".button").each(function (k,v) {
        $(this).show();
        $(this).next().hide();
    })
    $(e).toggle();
    $(e).next().toggle();

}
/**
 * 操作日志
 * @param id
 */
AppUser.recordById=function(id){
    var index = layer.open({
        type: 2,
        title: "查看操作记录",
        area: ['90%', '90%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/appRecord/appUser_recordById?id=' + id+"&type=1"
    });
    this.layerIndex = index;
}
/**
 *  1重置密码 2修改优币/优钻/推荐次数
 */
AppUser.resetPwdOrupdate=function(id,type){
    var typeNmae="重置密码";
    if(type==2){
        typeNmae="修改优币/优钻/推荐次数"
    }
    var index = layer.open({
        type: 2,
        title: typeNmae,
        area: ['90%', '90%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/appUser/appUser_resetPwdOrupdate?id=' + id+'&type_='+type
    });
    this.layerIndex = index;
}
/**
 * 1邀请明细 2分成明细
 */
AppUser.inviteOrFengDetail=function(id,type){
    var typeNmae="邀请明细";
    var content=Feng.ctxPath + '/appInvite?uid=' + id;
    if(type==2){
        typeNmae="分成明细"
        content=Feng.ctxPath + '/appDivide?uid=' + id;
    }
    var index = layer.open({
        type: 2,
        title: typeNmae,
        area: ['90%', '90%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content:content
    });
    this.layerIndex = index;
}
/**
 * 打开查看用户管理详情
 */
AppUser.openAppUserDetail = function (id) {
    var index = layer.open({
        type: 2,
        title: '用户管理详情',
        area: ['90%', '90%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/appUser/appUser_detail/' + id
    });
    this.layerIndex = index;
};
/**
 * 检查是否选中
 */
AppUser.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        AppUser.seItem = selected;
        return true;
    }
};

/**
 * 禁用和解禁
 */
AppUser.isFreeze = function (state) {
    if (this.check()) {
        var stateName = "禁用";
        if (state == 1) {
            stateName = "解禁";
        }
        var ids="";
        $.each(this.seItem, function(i,val){
            ids += val.id+",";
        });
        ids = ids.substring(0, ids.length - 1);
        var confirm = layer.confirm("确定要"+stateName+"这些用户？", {btn: ['确定', '取消']}, function () {
            layer.close(confirm);
            var ajax = new $ax(Feng.ctxPath + "/appUser/updateFreeze", function (data) {
                Feng.success(""+stateName+"成功!");
                AppUser.table.refresh();
            }, function (data) {
                Feng.error(""+stateName+"失败!" + data.responseJSON.message + "!");
            });
            ajax.set("ids",ids);
            ajax.set("state",state);
            ajax.start();
        })
    }
};
/**
 * 禁用和解禁
 */
AppUser.isFreeze_ = function (id,state) {
    var stateName = "禁用";
    if (state == 1) {
        stateName = "解禁";
    }
    var confirm = layer.confirm("确定要"+stateName+"该用户？", {btn: ['确定', '取消']}, function () {
        layer.close(confirm);
        var ajax = new $ax(Feng.ctxPath + "/appUser/updateFreeze", function (data) {
            Feng.success(""+stateName+"成功!");
            AppUser.table.refresh();
        }, function (data) {
            Feng.error(""+stateName+"失败!" + data.responseJSON.message + "!");
        });
        ajax.set("ids",id);
        ajax.set("state",state);
        ajax.start();
    })
};
/**
 * 查询用户管理列表
 */
AppUser.search = function () {
    var queryData = {};
    queryData['usercoding'] = $("#usercoding").val();
    queryData['nickname'] = $("#nickname").val();
    queryData['sex'] = $("#sex").val();
    queryData['status'] = $("#status").val();
    queryData['state'] = $("#state").val();
    AppUser.table.refresh({query: queryData});
};
/**
 * 重置
 */
AppUser.resetSearch = function () {
    $("#usercoding").val("");
    $("#nickname").val("");
    $("#sex").val("");
    $("#status").val("");
    $("#state").val("");
    AppUser.search();
};
$(function () {
    var defaultColunms = AppUser.initColumn();
    var table = new BSTable(AppUser.id, "/appUser/list", defaultColunms);
    table.setPaginationType("server");
    AppUser.table = table.init();
});
