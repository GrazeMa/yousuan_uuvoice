/**
 * 初始化房间背景图详情对话框
 */
var AppBackdropInfoDlg = {
    appBackdropInfoData : {},
    validateFields: {
        name: {
            validators: {
                notEmpty: {
                    message: '背景图名称不能为空'
                }
            }
        }, sequence: {
            validators: {
                notEmpty: {
                    message: '排序不能为空'
                }
                , regexp: {
                    regexp: /^[1-9]\d*$/,
                    message: '数字格式不正确'
                }
            }
        }
    }
};
/**
 * 验证数据是否为空
 */
AppBackdropInfoDlg.validate = function () {
    $('#horizontalForm').data("bootstrapValidator").resetForm();
    $('#horizontalForm').bootstrapValidator('validate');
    return $("#horizontalForm").data('bootstrapValidator').isValid();
};
/**
 * 清除数据
 */
AppBackdropInfoDlg.clearData = function() {
    this.appBackdropInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
AppBackdropInfoDlg.set = function(key, val) {
    this.appBackdropInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
AppBackdropInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
AppBackdropInfoDlg.close = function() {
    parent.layer.close(window.parent.AppBackdrop.layerIndex);
}

/**
 * 收集数据
 */
AppBackdropInfoDlg.collectData = function() {
    this
    .set('id')
    .set('img')
    .set('createTime')
    .set('name')
    .set('sequence')
    .set('num')
    .set('state')
    .set('isDelete');
}

/**
 * 提交添加
 */
AppBackdropInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();
    if(!this.validate()){
        return;
    }
    var img=$("#img").val();
    if(img==null || img==""){
        layer.msg("请上传背景图");
        return;
    }
    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/appBackdrop/add", function(data){
        Feng.success("添加成功!");
        window.parent.AppBackdrop.table.refresh();
        AppBackdropInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.appBackdropInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
AppBackdropInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();
    if(!this.validate()){
        return;
    }
    var img=$("#img").val();
    if(img==null || img==""){
        layer.msg("请上传背景图");
        return;
    }
    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/appBackdrop/update", function(data){
        Feng.success("修改成功!");
        window.parent.AppBackdrop.table.refresh();
        AppBackdropInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.appBackdropInfoData);
    ajax.start();
}

$(function() {
    Feng.initValidator("horizontalForm", AppBackdropInfoDlg.validateFields);
    // 初始化图片上传
    var imageUp = new $WebUploadImage("img");
    imageUp.setUploadBarId("progressBar");
    imageUp.init();
});
