/**
 * 房间背景图管理初始化
 */
var AppBackdrop = {
    id: "AppBackdropTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
AppBackdrop.initColumn = function () {
    return [
        {field: 'selectItem', radio: false},
            {title: '', field: 'id', visible: false, align: 'center', valign: 'middle'},
            {title: '上传时间', field: 'createTime', visible: true, align: 'center', valign: 'middle'},
            {title: '背景图名称', field: 'name', visible: true, align: 'center', valign: 'middle'},
            {title: '背景图', field: 'img', visible: true, align: 'center', valign: 'middle',
                formatter: function (value, row) {
                    if (value == null || value == '') {
                        return '<a class = "view"  href="javascript:void(0)"><img  style="width: 50px;height:50px;" src="' + Feng.ctxPath + '/static/img/NoPIC.png" /></a>';
                    } else {
                        return '<a class = "view"  href="javascript:void(0)"><img  style="width: 50px;height:50px;" src="'  + value + '" /></a>';
                    }
                },
                events: 'operateEvents'
            },
            {title: '顺序', field: 'sequence', visible: true, align: 'center', valign: 'middle'},
            {title: '历史使用次数', field: 'num', visible: true, align: 'center', valign: 'middle'},
            {title: '是否禁用', field: 'state', visible: true, align: 'center', valign: 'middle',
                formatter: function (value, row) {
                    //1否，2是
                    if(value==1){
                        return "正常";
                    }else{
                        return "禁用"
                    }
                }
            },
            {title: '操作', field: 'opt', visible: true, align: 'left', valign: 'middle',
                formatter: function (value, row) {
                    var btn=[];
                    btn+='<p style="line-height: 10px;"><a href="javascript:void(0);" onclick="AppBackdrop.openAppBackdropDetail('+row.id+')">编辑</a></p>';
                    if(row.state==1){//正常
                        btn+='<p style="line-height: 10px;"><a href="javascript:void(0);" onclick="AppBackdrop.isFreeze_('+row.id+',2)">禁用</a></p>';
                    }else{
                        btn+='<p style="line-height: 10px;"><a href="javascript:void(0);" onclick="AppBackdrop.isFreeze_('+row.id+',1)">解禁</a></p>';
                        btn+='<p style="line-height: 10px;"><a href="javascript:void(0);" onclick="AppBackdrop.delete_('+row.id+')">删除</a></p>';
                    }
                    return btn;

                }
            }
    ];
};

/**
 * 检查是否选中
 */
AppBackdrop.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        AppBackdrop.seItem = selected;
        return true;
    }
};

/**
 * 点击添加房间背景图
 */
AppBackdrop.openAddAppBackdrop = function () {
    var index = layer.open({
        type: 2,
        title: '添加房间背景图',
        area: ['80%', '80%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/appBackdrop/appBackdrop_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看房间背景图详情
 */
AppBackdrop.openAppBackdropDetail = function (id) {
    var index = layer.open({
        type: 2,
        title: '房间背景图编辑',
        area: ['80%', '80%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/appBackdrop/appBackdrop_update/' + id
    });
    this.layerIndex = index;
};
/**
 * 单个删除
 * @param id
 * @private
 */
AppBackdrop.delete_ = function (id) {
    var confirm = layer.confirm("确定要删除该房间背景图？", {btn: ['确定', '取消']}, function () {
        layer.close(confirm);
        var ajax = new $ax(Feng.ctxPath + "/appBackdrop/delete", function (data) {
            Feng.success("删除成功!");
            AppBackdrop.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("appBackdropId", id);
        ajax.start();
    });
}
/**
 * 删除房间背景图
 */
AppBackdrop.delete = function () {
    if (this.check()) {
        var ids="";
        $.each(AppBackdrop.seItem, function(i,val){
            ids += val.id+",";
        });
        ids = ids.substring(0, ids.length - 1);
        var confirm = layer.confirm("确定要删除这些房间背景图？", {btn: ['确定', '取消']}, function () {
            layer.close(confirm);
            var ajax = new $ax(Feng.ctxPath + "/appBackdrop/delete", function (data) {
                Feng.success("删除成功!");
                AppBackdrop.table.refresh();
            }, function (data) {
                Feng.error("删除失败!" + data.responseJSON.message + "!");
            });
            ajax.set("appBackdropId", ids);
            ajax.start();
        })
    }
};
/**
 * 禁用和解禁
 */
AppBackdrop.isFreeze = function (state) {
    if (this.check()) {
        var stateName = "禁用";
        if (state == 1) {
            stateName = "解禁";
        }
        var ids="";
        $.each(AppBackdrop.seItem, function(i,val){
            ids += val.id+",";
        });
        ids = ids.substring(0, ids.length - 1);
        var confirm = layer.confirm("确定要"+stateName+"这些房间背景图？", {btn: ['确定', '取消']}, function () {
            layer.close(confirm);
            var ajax = new $ax(Feng.ctxPath + "/appBackdrop/updateFreeze", function (data) {
                Feng.success(""+stateName+"成功!");
                AppBackdrop.table.refresh();
            }, function (data) {
                Feng.error(""+stateName+"失败!" + data.responseJSON.message + "!");
            });
            ajax.set("ids",ids);
            ajax.set("state",state);
            ajax.start();
        })
    }
};
/**
 * 禁用和解禁
 */
AppBackdrop.isFreeze_ = function (id,state) {
    var stateName = "禁用";
    if (state == 1) {
        stateName = "解禁";
    }
    var confirm = layer.confirm("确定要"+stateName+"该房间背景图？", {btn: ['确定', '取消']}, function () {
        layer.close(confirm);
        var ajax = new $ax(Feng.ctxPath + "/appBackdrop/updateFreeze", function (data) {
            Feng.success(""+stateName+"成功!");
            AppBackdrop.table.refresh();
        }, function (data) {
            Feng.error(""+stateName+"失败!" + data.responseJSON.message + "!");
        });
        ajax.set("ids",id);
        ajax.set("state",state);
        ajax.start();
    })
};
/**
 * 查询房间背景图列表
 */
AppBackdrop.search = function () {
    var queryData = {};
    queryData['name'] = $("#name").val();
    queryData['state'] = $("#state").val();
    queryData['beginTime'] = $("#beginTime").val();
    queryData['endTime'] = $("#endTime").val();
    AppBackdrop.table.refresh({query: queryData});
};
/**
 * 重置
 */
AppBackdrop.resetSearch = function () {
    $("#beginTime").val("");
    $("#endTime").val("");
    $("#name").val("");
    $("#state").val("");
    AppBackdrop.search();
};
$(function () {
    var defaultColunms = AppBackdrop.initColumn();
    var table = new BSTable(AppBackdrop.id, "/appBackdrop/list", defaultColunms);
    table.setPaginationType("server");
    AppBackdrop.table = table.init();
});
