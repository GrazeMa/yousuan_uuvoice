/**
 * 结算记录初始化
 */
var AppSettleTurnover = {
    id: "AppSettleTurnoverTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
AppSettleTurnover.initColumn = function () {
    return [
        {field: 'selectItem', radio: true, visible: false},
        {title: '', field: 'id', visible: false, align: 'center', valign: 'middle'},
        {title: '公会ID', field: 'gid', visible: true, align: 'center', valign: 'middle'},
        {title: '结算金额', field: 'amountOfMoney', visible: true, align: 'center', valign: 'middle'},
        {title: '实际结算金额', field: 'actualSettlementAmount', visible: true, align: 'center', valign: 'middle'},
        {title: '开始日期', field: 'beginDate', visible: true, align: 'center', valign: 'middle'},
        {title: '结束日期', field: 'endDate', visible: true, align: 'center', valign: 'middle'},
        {title: '当前状态', field: 'status', visible: true, align: 'center', valign: 'middle',
        	formatter: function (value, row) {
                // 状态（0：等待审核；1：审核成功，等待结算；2：审核成功，结算成功；3：审核成功，异常结算；4：已审核，取消结算。）
                if(value == 0) {
                    return "等待审核";
                } else if (value == 1){
                    return "审核成功，等待结算";
                } else if (value == 2){
                    return "审核成功，结算成功";
                } else if (value == 3){
                    return "审核成功，异常结算";
                } else if (value == 4){
                    return "已审核，取消结算";
                }
            }
        },
        {title: '备注', field: 'remarks', visible: true, align: 'center', valign: 'middle'},
        {title: '提交时间', field: 'createTime', visible: true, align: 'center', valign: 'middle'},
        {title: '审核时间', field: 'updateTime', visible: true, align: 'center', valign: 'middle'}     
    ];
};

/**
 * 查询结算记录列表
 */
AppSettleTurnover.search = function () {
    var queryData = {};
    queryData['beginDate'] = $("#beginDate").val();
    queryData['endDate'] = $("#endDate").val();
    queryData['guildId'] = $("#guildId").val();
    AppSettleTurnover.table.refresh({query: queryData});
};
/**
 * 重置
 */
AppSettleTurnover.resetSearch = function () {
    $("#beginDate").val("");
    $("#endDate").val("");
    AppSettleTurnover.search();
};

/**
 * 点击添加申请结算。
 */
AppSettleTurnover.openAddSettlePage = function() {
	var index = layer.open({
		type : 2,
		title : '申请结算',
		area : [ '800px', '420px' ], // 宽高。
		fix : false, // 不固定。
		maxmin : true,
		content : Feng.ctxPath + '/appSettleTurnover/appSettleTurnove_add/'
	});
	this.layerIndex = index;
}

$(function () {
    var defaultColunms = AppSettleTurnover.initColumn();
    var table = new BSTable(AppSettleTurnover.id, "/appSettleTurnover/list", defaultColunms);
    table.setPaginationType("server");
    AppSettleTurnover.table = table.init();
    AppSettleTurnover.search();
//    setTimeout(function(){ AppSettleTurnover.search(); }, 2000); 
});
