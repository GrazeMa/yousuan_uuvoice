/**
 * 初始化结算记录详情对话框
 */
var AppSettleTurnoverInfoDlg = {
    appSettleTurnoverInfoData : {}
};

/**
 * 清除数据
 */
AppSettleTurnoverInfoDlg.clearData = function() {
    this.appSettleTurnoverInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
AppSettleTurnoverInfoDlg.set = function(key, val) {
    this.appSettleTurnoverInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
AppSettleTurnoverInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
AppSettleTurnoverInfoDlg.close = function() {
    parent.layer.close(window.parent.AppSettleTurnover.layerIndex);
}

/**
 * 收集数据
 */
AppSettleTurnoverInfoDlg.collectData = function() {
    this
    .set('id').set('gid').set('beginDate').set('endDate').set('amountOfMoney')
}

/**
 * 提交添加
 */
AppSettleTurnoverInfoDlg.addSubmit = function() {
	
    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/appSettleTurnover/add", function(data){
        Feng.success("添加成功!");
        window.parent.AppSettleTurnover.table.refresh();
        AppSettleTurnoverInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.appSettleTurnoverInfoData);
    ajax.start();
	
}

$(function() {
	$("#gid").val($("#loginName").val());
	
//	var date = new Date();
//    var month = date.getMonth() + 1;
//    var strDate = date.getDate();
//    if (month >= 1 && month <= 9) {
//        month = "0" + month;
//    }
//    if (strDate >= 0 && strDate <= 9) {
//        strDate = "0" + strDate;
//    }
//    var currentDate = date.getFullYear() + "-" + month + "-" + strDate;
	
	
	/**
	 * 获取本周、本季度、本月、上月的开始日期、结束日期
	 */
	var now = new Date(); //当前日期
	var nowDayOfWeek = now.getDay(); //今天本周的第几天
	var nowDay = now.getDate(); //当前日
	var nowMonth = now.getMonth(); //当前月
	var nowYear = now.getYear(); //当前年
	nowYear += (nowYear < 2000) ? 1900 : 0; //
	var lastMonthDate = new Date(); //上月日期
	lastMonthDate.setDate(1);
	lastMonthDate.setMonth(lastMonthDate.getMonth() - 1);
	var lastYear = lastMonthDate.getYear();
	var lastMonth = lastMonthDate.getMonth();
	// 格式化日期：yyyy-MM-dd
	function formatDate(date) {
	    var myyear = date.getFullYear();
	    var mymonth = date.getMonth() + 1;
	    var myweekday = date.getDate();
	    if (mymonth < 10) {
	        mymonth = "0" + mymonth;
	    }
	    if (myweekday < 10) {
	        myweekday = "0" + myweekday;
	    }
	    return (myyear + "-" + mymonth + "-" + myweekday);
	}
	
	// 获得上周的开始日期。
	function getLastWeekStartDate() {
	    var weekStartDate = new Date(nowYear, nowMonth, nowDay - nowDayOfWeek - 7);
	    return formatDate(weekStartDate);
	}

	// 获得上周的结束日期。
	function getLastWeekEndDate() {
	    var weekEndDate = new Date(nowYear, nowMonth, nowDay - nowDayOfWeek - 1);
	    return formatDate(weekEndDate);
	}
	
    
//  $("#beginDate").val(getLastWeekStartDate());
//	$("#endDate").val(getLastWeekEndDate());
    $("#beginDate").val("2020-06-08");
	$("#endDate").val("2020-06-14");
	
	$("#type").bind("change", function() {
	    var ajax = new $ax(Feng.ctxPath + "/appSettleTurnover/amountOfMoney", function(data){
	        $("#amountOfMoney").val(data.toFixed(2));
	    },function(data){
	        Feng.error("添加失败!" + data.responseJSON.message + "!");
	    });
	    ajax.set("type", $("#type").val());
	    ajax.start();
	});
	
});
