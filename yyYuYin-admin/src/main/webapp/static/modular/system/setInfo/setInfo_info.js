/**
 * 初始化系统设置详情对话框
 */
var SetInfoInfoDlg = {
    setInfoInfoData : {}
};

SetInfoInfoDlg.validate = function (type) {
    var ok=true;
    var req=/^[1-9]\d*$/;//正整数
    var req_=/(^[1-9]([0-9]+)?(\.[0-9]{1,2})?$)|(^(0){1}$)|(^[0-9]\.[0-9]([0-9])?$)/;//小数和整数
    if(type==1){//优币奖励
        $("input[id^='award']").each(function () {
            if(!req.test($(this).val())){
                ok=false;
            }
        })

    }else if(type==2){//礼物，宝箱设置
        $("input[id^='gift']").each(function () {
            if(!req.test($(this).val())){
                ok=false;
            }
        })
        $("input[id^='box'][id$='_z']").each(function () {
            if($(this).val()!=null && $(this).val()!=''){
                if(!req_.test($(this).val())){
                    ok=false;
                }
                if($(this).val()>100){
                    ok=false;
                }
            }
        })

    }else if(type==3) {//分享赠送
        $("input[id^='share']").each(function () {
            if($(this).val()!=null && $(this).val()!=''){
                if(!req_.test($(this).val())){
                    ok=false;
                }
                if($(this).attr("id")=="share3_y"){
                    if($(this).val()>100){
                        ok=false;
                    }
                }
            }
        })

    }else if(type==4) {//速配
        var val=$("#speed1").val();
        if(!req.test(val)){
            ok=false;
        }

    }else if(type==5) {//等级
        $("input[id^='cgrade']").each(function () {
            if($(this).val()!=null && $(this).val()!='') {
                if (!req_.test($(this).val())) {
                    ok = false;
                }
            }
        })
        $("input[id^='grade']").each(function () {
            if($(this).val()!=null && $(this).val()!='') {
                if (!req.test($(this).val())) {
                    ok = false;
                }
            }
        })
    }else if(type==6) {//提现
        $("input[id^='withdraw'][id$='_x']").each(function () {
            if($(this).val()!=null && $(this).val()!='') {
                if (!req_.test($(this).val())) {
                    ok = false;
                }
            }
        })
        $("input[id^='withdraw'][id$='_y']").each(function () {
            if($(this).val()!=null && $(this).val()!='') {
                if (!req.test($(this).val())) {
                    ok = false;
                }
            }
        })

    }else if(type==7) {//充值
        $("input[id^='recharge'][id$='_x']").each(function () {
            if($(this).val()!=null && $(this).val()!='') {
                if (!req.test($(this).val())) {
                    ok = false;
                }
            }
        })
        $("input[id^='recharge'][id$='_y']").each(function () {
            if($(this).val()!=null && $(this).val()!='') {
                if (!req_.test($(this).val())) {
                    ok = false;
                }
            }
        })
    } else if(type==8) {//分成设置
        $("input[id^='liang']").each(function () {
            if($(this).val()!=null && $(this).val()!='') {
                if (!req_.test($(this).val())) {
                    ok = false;
                }
                if($(this).val()>=100){
                    ok = false;
                }
            }
        })
    }

    if(!ok){
        Feng.info("请检测所填项是否填写完整、数值是否填写正确");
    }
    return ok;
};

/**
 * 清除数据
 */
SetInfoInfoDlg.clearData = function() {
    this.setInfoInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
SetInfoInfoDlg.set = function(key, val) {
    this.setInfoInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
SetInfoInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
SetInfoInfoDlg.close = function() {
    parent.layer.close(window.parent.SetInfo.layerIndex);
}

/**
 * 收集数据
 */
SetInfoInfoDlg.collectData = function(type) {
    var id;
    var content="";
    var content1="";
    //默认所有的数据填写正确
    var state=0;
    if(type==1){//优币奖励
        content='{"id":'+1+',"x":'+$("#award1").val() +'};' +
            '{"id":'+2+',"x":'+$("#award2").val() +'};' +
            '{"id":'+3+',"x":'+$("#award3").val() +'};'+
            '{"id":'+4+',"x":'+$("#award4").val() +'};' +
            '{"id":'+5+',"x":'+$("#award5").val() +'};' +
            '{"id":'+6+',"x":'+$("#award6").val() +'};' +
            '{"id":'+7+',"x":'+$("#award7").val() +'}';
        content1= '{"id":'+1+',"x":'+$("#award8_x").val() +',"y":'+$("#award8_y").val()+'};' +
            '{"id":'+2+',"x":'+$("#award9_x").val() +',"y":'+$("#award9_y").val()+'};' +
            '{"id":'+3+',"x":'+$("#award10_x").val() +',"y":'+$("#award10_y").val()+'}';
    }else  if(type==2) {//礼物和宝箱设置
        content='{"id":'+1+',"x":'+$("#gift1").val() +'};' +
            '{"id":'+2+',"x":'+$("#gift2").val() +'};' +
            '{"id":'+3+',"x":'+$("#gift3").val() +'};'+
            '{"id":'+4+',"x":'+$("#gift4").val() +'};'+
            '{"id":'+5+',"x":'+$("#gift5").val() +'};'+
            '{"id":'+6+',"x":'+$("#gift6").val() +'};'+
            '{"id":'+7+',"x":'+$("#gift7").val() +'}';
        content1= '{"id":'+1+',"x":'+$("#box2_x").val() +',"y":'+$("#box2_y").val()+',"z":'+$("#box2_z").val()+',"h":"'+$("#box2_y :selected").text()+'"};' +
            '{"id":'+2+',"x":'+$("#box3_x").val() +',"y":'+$("#box3_y").val()+',"z":'+$("#box3_z").val()+',"h":"'+$("#box3_y :selected").text()+'"};' +
            '{"id":'+3+',"x":'+$("#box4_x").val() +',"y":'+$("#box4_y").val()+',"z":'+$("#box4_z").val()+',"h":"'+$("#box4_y :selected").text()+'"};' +
            '{"id":'+4+',"x":'+$("#box5_x").val() +',"y":'+$("#box5_y").val()+',"z":'+$("#box5_z").val()+',"h":"'+$("#box5_y :selected").text()+'"};' +
            '{"id":'+5+',"x":'+$("#box6_x").val() +',"y":'+$("#box6_y").val()+',"z":'+$("#box6_z").val()+',"h":"'+$("#box6_y :selected").text()+'"};' +
            '{"id":'+7+',"x":'+$("#box8_x").val() +',"y":'+$("#box8_y").val()+',"z":'+$("#box8_z").val()+',"h":"'+$("#box8_y :selected").text()+'"};' +
            '{"id":'+8+',"x":'+$("#box9_x").val() +',"y":'+$("#box9_y").val()+',"z":'+$("#box9_z").val()+',"h":"'+$("#box9_y :selected").text()+'"};' +
            '{"id":'+9+',"x":'+$("#box10_x").val() +',"y":'+$("#box10_y").val()+',"z":'+$("#box10_z").val()+',"h":"'+$("#box10_y :selected").text()+'"};' +
            '{"id":'+10+',"x":'+$("#box11_x").val() +',"y":'+$("#box11_y").val()+',"z":'+$("#box11_z").val()+',"h":"'+$("#box11_y :selected").text()+'"};' +
            '{"id":'+11+',"x":'+$("#box12_x").val() +',"y":'+$("#box12_y").val()+',"z":'+$("#box12_z").val()+',"h":"'+$("#box12_y :selected").text()+'"};' +
            '{"id":'+6+',"x":'+(-1)+',"y":'+(-1)+',"z":'+$("#box7_z").val()+',"h":""}';

    }else if(type==3){//分享赠送
        content='{"id":'+1+',"x":'+$("#share1").val() +',"y":'+(-1)+'};' +
            '{"id":'+2+',"x":'+$("#share2_x").val() +',"y":'+$("#share2_y").val()+'};' +
            '{"id":'+3+',"x":'+$("#share3_x").val() +',"y":'+$("#share3_y").val()+'};'+
            '{"id":'+4+',"x":'+$("#share4").val() +',"y":'+(-1)+'}';
    }else if(type==4){//速配设置
        content='{"id":'+1+',"x":'+$("#speed1").val() +'};' +
            '{"id":'+2+',"x":"'+$(":radio[name='speed2']:checked").val() +'"};' +
            '{"id":'+3+',"x":"'+$("#speed3").val() +'"}';
    }else if(type==5){//等级设置
        for(var i=1;i<=10;i++){
            content+='{"id":'+i+',"x":'+$("#cgrade"+i+"_x").val() +',"y":'+$("#cgrade"+i+"_y").val()+',"z":'+$("#cgrade"+i+"_z").val()+',"b":'+$("#cgrade"+i+"_b").val()+',"p":'+$("#cgrade"+i+"_p").val()+'};' ;
            content1+='{"id":'+i+',"x":'+$("#grade"+i+"_x").val() +',"y":'+$("#grade"+i+"_y").val()+',"z":'+$("#grade"+i+"_z").val()+',"b":'+$("#grade"+i+"_b").val()+',"p":'+$("#grade"+i+"_p").val()+'};' ;
        }
        content=content.substring(0,content.length-1);
        content1=content1.substring(0,content1.length-1);

    }else if(type==6){//提现设置
        for(var i=1;i<=6;i++){
            content+='{"id":'+i+',"x":'+$("#withdraw"+i+"_x").val() +',"y":'+$("#withdraw"+i+"_y").val()+'};' ;
        }
        content=content.substring(0,content.length-1);
    }else if(type==7){//充值设置
        for(var i=1;i<=8;i++){
            content+='{"id":'+(i+1)+',"x":'+$("#recharge"+i+"_x").val() +',"y":'+$("#recharge"+i+"_y").val()+'};' ;
        }
        content+='{"id":'+1+',"x":'+$("#recharge0_x").val() +',"y":'+(-1)+'}';
    }else if(type==8){//分成设置
        for(var i=1;i<=12;i++){
            content+='{"id":'+(i)+',"liang":'+$("#liang"+i).val() +'};' ;
        }
    }

    this.setInfoInfoData['content'] = content;
    this.setInfoInfoData['content1'] = content1;
    this.setInfoInfoData['type'] = type;
}

/**
 * 提交添加
 */
SetInfoInfoDlg.editSubmit = function(type) {

    this.clearData();
    this.collectData(type);
    if (!this.validate(type)) {
        return;
    }
    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/setInfo/update", function(data){
        Feng.success("修改成功!");
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.setInfoInfoData);
    ajax.start();
}
/**
 * 宝箱奖品填充数据
 */
SetInfoInfoDlg.selectBox=function(e){
    var val=$(e).val();
    if(val==3){
        var ajax = new $ax(Feng.ctxPath + "/appGift/getGiftName", function(data){
            var content="";
            $.each(data,function (k,v) {
                content+='<option value="'+v.id+'">'+v.name+'</option>';
            })
            $(e).next().empty().append(content);
        },function(data){
            Feng.error("获取失败!" + data.responseJSON.message + "!");
        });
        ajax.start();
    }else{
        var ajax = new $ax(Feng.ctxPath + "/appScene/getSceneName", function(data){
            var content='';
            $.each(data,function (k,v) {
                content+='<option value="'+v.id+'">'+v.name+'</option>';
            })
            $(e).next().empty().append(content);
        },function(data){
            Feng.error("获取失败!" + data.responseJSON.message + "!");
        });
        ajax.set("state",val);
        ajax.start();
    }

}
/**
 * 点击获取数据
 * @param typeName
 * @param type
 */
SetInfoInfoDlg.titleClick=function(typeName,type){
    //重置
    //this.resetSearch();
    for(var i=1;i<=8;i++){
        $("#titleDivU"+i+"").removeClass("titleItemCk");
        $("#titleDivU"+i+"Form").hide();
    }
    $("#"+typeName).addClass("titleItemCk");
    $("#"+typeName+"Form").show();
    $.ajax({
        type: "post",
        url: Feng.ctxPath + "/setInfo/getContentByType",
        async: false,
        data: {"type": type},
        success: function (data) {
            if(type==1){//优币奖励
                $.each(data, function (k, v) {
                    $("#award"+(k+1)+"").val(v.x);
                    if(k>5){
                        $("#award"+(k+1)+"_x").val(v.x);
                        $("#award"+(k+1)+"_y").val(v.y);
                    }
                })
            }else if(type==2){//礼物和宝箱设置
                $.each(data, function (k, v) {
                    if(k<7){
                        $("#gift"+(k+1)+"").val(v.x);
                    }else{
                        $("#box"+(k-5)+"_x").val(v.x);
                        $("#box"+(k-5)+"_z").val(v.z);
                        if(v.x!=null){
                            $("#box"+(k-5)+"_x").trigger("change");
                        }
                        $("#box"+(k-5)+"_y").val(v.y);
                    }
                })
            }else if(type==3){//分享赠送
                $.each(data, function (k, v) {

                    if(k==1||k==2){
                        $("#share"+(k+1)+"_x").val(v.x);
                        $("#share"+(k+1)+"_y").val(v.y);
                    }else{
                        $("#share"+(k+1)+"").val(v.x);
                    }
                })
            }else if(type==4){//速配设置
                $.each(data, function (k, v) {
                   if(k==1){
                       $("input:radio[name='speed2'][value='"+v.x+"']").attr('checked','true');
                   }else{
                       $("#speed"+(k+1)+"").val(v.x);
                   }
                })
            }else if(type==5){//等级设置
                $.each(data, function (k, v) {
                    if(k>=10){
                        $("#grade"+(k-9)+"_x").val(v.x);
                        $("#grade"+(k-9)+"_y").val(v.y);
                        $("#grade"+(k-9)+"_z").val(v.z);
                        $("#grade"+(k-9)+"_p").val(v.p);
                        $("#grade"+(k-9)+"_b").val(v.b);
                    }else{
                        $("#cgrade"+(k+1)+"_x").val(v.x);
                        $("#cgrade"+(k+1)+"_y").val(v.y);
                        $("#cgrade"+(k+1)+"_z").val(v.z);
                        $("#cgrade"+(k+1)+"_p").val(v.p);
                        $("#cgrade"+(k+1)+"_b").val(v.b);

                    }
                })
            }else if(type==6){//提现设置
                $.each(data, function (k, v) {
                    $("#withdraw"+(k+1)+"_x").val(v.x);
                    $("#withdraw"+(k+1)+"_y").val(v.y);
                })
            }else if(type==7){//充值设置
                $.each(data, function (k, v) {
                    if(k==0){
                        $("#recharge0_x").val(v.x);
                    }else {
                        $("#recharge"+(k)+"_x").val(v.x);
                        $("#recharge"+(k)+"_y").val(v.y);
                    }
                })
            } else if(type==8){//分成设置
                $.each(data, function (k, v) {
                    $("#liang"+(v.id)).val((v.liang*100).toFixed(0));
                })
            }
        }
    });
}
$(function() {
    SetInfoInfoDlg.titleClick('titleDivU1',1);
});
