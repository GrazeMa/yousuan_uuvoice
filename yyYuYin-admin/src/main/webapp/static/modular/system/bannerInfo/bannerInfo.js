/**
 * Banner信息管理初始化
 */
var BannerInfo = {
    id: "BannerInfoTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
BannerInfo.initColumn = function () {
    return [
        {field: 'selectItem', radio: false},
        {title: '创建时间', field: 'createTime', visible: true, align: 'center', valign: 'middle'},
        {title: 'banner标题', field: 'title', visible: true, align: 'center', valign: 'middle'},
        {title: 'banner排序', field: 'orderby', visible: true, align: 'center', valign: 'middle'},
        {title: '类型', field: 'type', visible: true, align: 'center', valign: 'middle',
            formatter: function (value, row) {
                //1首页， 2 男神， 3 女神
                if(value==1){
                    return "首页"
                }else if(value==2){
                    return "男神"
                }else if(value==3){
                    return "女神"
                }
            }
        },
        {title: 'banner图', field: 'imgUrl', visible: true, align: 'center', valign: 'middle',
            formatter: function (value, row) {
                if (row.imgUrl == null || row.imgUrl == '') {
                    return '<a class = "view"  href="javascript:void(0)"><img style="width: 50px;height:50px;" src="' + Feng.ctxPath + '/static/img/NoPIC.png" /></a>';
                } else {
                    return '<a class = "view"  href="javascript:void(0)"><img style="width: 90px;height:50px;" src="'+ row.imgUrl + '" /></a>';
                }
            },
            events: 'operateEvents'},

        {title: '跳转类型', field: 'urlType', visible: true, align: 'center', valign: 'middle',
            formatter: function (value, row) {
                //1是不跳转，2外部，3内部，4个人充值
                if(value==1){
                    return "不跳转"
                }else if(value==2){
                    return "外部跳转"
                }else if(value==3){
                    return "内部跳转"
                }else {
                    return "房主房间"
                }
            }
        },
        {title: '点击次数', field: 'clicks', visible: true, align: 'center', valign: 'middle'},
        {title: '状态', field: 'isSue', visible: true, align: 'center', valign: 'middle',
            formatter: function (value, row) {
                if(value==1){
                    return "已发布"
                }else {
                    return "未发布"
                }
            }
        },
        {title: '操作', field: 'opt', visible: true, align: 'left', valign: 'middle', width: '8%',
            formatter: function (value, row) {
                var btn=[];
                btn[0] = ['<a href="javascript:void(0);" onclick="BannerInfo.openBannerInfoDetail('+row.id+')">编辑</a>'];
                /*btn[1] = ['<a href="javascript:void(0);" onclick="BannerInfo.delete('+row.id+')">删除</a>'];*/
                if(row.isSue == 1){
                    btn[1] = ['<a href="javascript:void(0);" onclick="BannerInfo.setIsSue('+row.id+', 0)">取消发布</a>'];
                } else {
                    btn[1] = ['<a href="javascript:void(0);" onclick="BannerInfo.setIsSue('+row.id+', 1)">发布</a>'];
                }
                return btn;
            }
        }
    ];
};
/**
 * 图片弹出预览框（可选）
 */
window.operateEvents = {
    'click .view': function (e, value, row) {
        // 设置图片路径
        var imgUrl = row.imgUrl;
        if(imgUrl != "") {
            imgUrl =  imgUrl;// 设置图片路径
        } else {
            imgUrl = Feng.ctxPath + '/static/img/NoPIC.png';// 默认无图
        }
        layer.open({
            type: 1,
            title: false,
            closeBtn: 0,
            area: 'auto',
            skin: 'layui-layer-nobg', //没有背景色
            shadeClose: true,
            content: '<img src="' + imgUrl + '" height="100%" width="100%" />'
        });
    },
};

/**
 * 检查是否选中
 */
BannerInfo.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        BannerInfo.seItem = selected;
        return true;
    }
};

/**
 * 点击添加Banner信息
 */
BannerInfo.openAddBannerInfo = function () {
    var index = layer.open({
        type: 2,
        title: '添加Banner信息',
        area: ['95%', '95%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/bannerInfo/bannerInfo_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看Banner信息详情
 */
BannerInfo.openBannerInfoDetail = function (id) {
    var index = layer.open({
        type: 2,
        title: 'Banner信息详情',
        area: ['95%', '95%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/bannerInfo/bannerInfo_update/' + id
    });
    this.layerIndex = index;
};

/**
 * 删除Banner信息
 */
BannerInfo.delete = function () {
    if (this.check()) {
        var ids = "";
        $.each(this.seItem, function (i, val) {
            ids += val.id + ",";
        });
        ids = ids.substring(0, ids.length - 1);
        var operation = function () {
            var ajax = new $ax(Feng.ctxPath + "/bannerInfo/deletes", function (data) {
                Feng.success("删除成功!");
                BannerInfo.table.refresh();
            }, function (data) {
                Feng.error("删除失败!" + data.responseJSON.message + "!");
            });
            ajax.set("bannerInfoIds", ids);
            ajax.start();
        };
        Feng.confirm("是否删除这些banner?", operation);
    }
};

/**
 * 发布/取消发布
 */
BannerInfo.setIsSue = function (id, isSue) {
    var title="确定要发布该banner？";
    if(isSue==0){
        title="确定要取消发布？";
    }
    var confirm = layer.confirm(title, { btn: ['确定','取消'] }, function() {
        layer.close(confirm);
        var ajax = new $ax(Feng.ctxPath + "/bannerInfo/update", function (data) {
            if(data=="8"){
                Feng.info("banner最多只能发布8张");
            }else {
                Feng.success("操作成功!");
                BannerInfo.table.refresh();
            }
        }, function (data) {
            Feng.error("操作失败!" + data.responseJSON.message + "!");
        });
        ajax.set("id",id);
        ajax.set("isSue",isSue);
        ajax.start();
    });

}

/**
 * 查询Banner信息列表
 */
BannerInfo.search = function () {
    var queryData = {};
    queryData['title'] = $("#title").val();
    queryData['beginTime'] = $("#beginTime").val();
    queryData['endTime'] = $("#endTime").val();
    queryData['isSue'] = $("#isSue").val();
    BannerInfo.table.refresh({query: queryData});
};
/**
 *重置
 */
BannerInfo.resetSearch = function () {
    $("#title").val("");
    $("#beginTime").val("");
    $("#endTime").val("");
    $("#isSue").val("");
    BannerInfo.search();
};
$(function () {
    var defaultColunms = BannerInfo.initColumn();
    var table = new BSTable(BannerInfo.id, "/bannerInfo/list", defaultColunms);
    table.setPaginationType("server");
    BannerInfo.table = table.init();
});
