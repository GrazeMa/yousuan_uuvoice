/**
 * 初始化Banner信息详情对话框
 */
var BannerInfoInfoDlg = {
    bannerInfoInfoData : {},
    editor: null,
    validateFields: {
        title: {
            validators: {
                notEmpty: {
                    message: '标题不能为空'
                }
            }
        }
    }
};

/**
 * 清除数据
 */
BannerInfoInfoDlg.clearData = function() {
    this.bannerInfoInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
BannerInfoInfoDlg.set = function(key, val) {
    this.bannerInfoInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
BannerInfoInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
BannerInfoInfoDlg.close = function() {
    parent.layer.close(window.parent.BannerInfo.layerIndex);
}

/**
 * 收集数据
 */
BannerInfoInfoDlg.collectData = function() {
    this.bannerInfoInfoData['content'] = BannerInfoInfoDlg.editor.getContent();
    this
        .set('id')
        .set('type')
        .set('title')
        .set('imgUrl')
        .set('orderby')
        .set('urlType')
        .set('urlHtml')
        .set('remark')
        .set('type')
        .set('isSue')
        .set('isDelete')
        .set('clicks')
        .set('createTime');
}

/**
 * 提交添加
 */
BannerInfoInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    if(!this.validate()){
        return ;
    }
    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/bannerInfo/add", function(data){
        Feng.success("添加成功!");
        window.parent.BannerInfo.table.refresh();
        BannerInfoInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.bannerInfoInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
BannerInfoInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();
    if(!this.validate()){
        return ;
    }
    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/bannerInfo/update", function(data){
        Feng.success("修改成功!");
        window.parent.BannerInfo.table.refresh();
        BannerInfoInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.bannerInfoInfoData);
    ajax.start();
}

BannerInfoInfoDlg.validate = function () {
    if($("#title").val()==null || $("#title").val()==''){
        layer.msg("请输入标题");
        return false;
    };
    if($("#imgUrl").val()==null || $("#imgUrl").val()==''){
        layer.msg("请上传图片");
        return false;
    };
    if($("#orderby").val()==null || $("#orderby").val()==''){
        layer.msg("请填写排序");
        return false;
    }else{
        if(isNaN($("#orderby").val())){
            layer.msg("请输入正确的数字");
            return false;
        }
    }
    if($("#urlType").val()==3){//内部
        var content=BannerInfoInfoDlg.editor.getContent();
        if(content==null || content==''){
            layer.msg("请填写内容");
            return false;
        }
    }
    if($("#urlType").val()==2){//外部
        if($("#urlHtml").val()==null || $("#urlHtml").val() ==''){
            layer.msg("请填写跳转链接");
            return false;
        }
    }
    return true;
};
/**
 * 下拉控制
 * @param e
 */
BannerInfoInfoDlg.selectClick=function(e){
    var type=$(e).val();
    //跳转类型(1不跳，2外部，3内部，4房主房间)
    if(type==1){//1 不跳转
        $("#contentDiv").hide();
        $("#urlHtmlDiv").hide();
    }else if(type==2){// 2 外部跳转
        $("#urlHtmlDiv").show();
        $("#urlHtmlDiv").find("label").text("链接地址");
        $("#contentDiv").hide();
    }else if(type==3){//3：内部跳转
        $("#contentDiv").show();
        $("#urlHtmlDiv").hide();
    }else{// 4 房主房间
        $("#urlHtmlDiv").show();
        $("#urlHtmlDiv").find("label").text("房主房间ID");
        $("#contentDiv").hide();
    }
}
$(function() {
    Feng.initValidator("noticeInfoInfoForm", BannerInfoInfoDlg.validateFields);

    // 初始化编辑器
    BannerInfoInfoDlg.editor = UM.getEditor('editor');

    // 初始化图片上传
    var imageUp = new $WebUploadImage("imgUrl");
    imageUp.setUploadBarId("progressBar");
    imageUp.init();
    //跳转类型(1不跳，2外部，3内部，4房主房间)
    var type=$("#urlType_").val();
    if(type==1){//1 不跳转
        $("#contentDiv").hide();
        $("#urlHtmlDiv").hide();
    }else if(type==2){// 2 外部跳转
        $("#urlHtmlDiv").show();
        $("#urlHtmlDiv").find("label").text("链接地址");
        $("#contentDiv").hide();
    }else if(type==3){//3：内部跳转
        $("#contentDiv").show();
        $("#urlHtmlDiv").hide();
    }else{// 4 房主房间
        $("#urlHtmlDiv").show();
        $("#urlHtmlDiv").find("label").text("房主房间ID");
        $("#contentDiv").hide();
    }
});
