/**
 * 推荐申请记录管理初始化
 */
var AppClaimer = {
    id: "AppClaimerTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
AppClaimer.initColumn = function () {
    return [
        {field: 'selectItem', radio: false,visible:true},
            {title: '申请时间', field: 'createTime', visible: true, align: 'center', valign: 'middle'},
            {title: '申请房间ID', field: 'rid', visible: true, align: 'center', valign: 'middle'},
            {title: '申请人昵称', field: 'nickName', visible: true, align: 'center', valign: 'middle'},
            {title: '推荐开始时间', field: 'sTime', visible: true, align: 'center', valign: 'middle'},
            {title: '推荐结束时间', field: 'eTime', visible: true, align: 'center', valign: 'middle'},
            /*{title: '排号', field: 'sequence', visible: true, align: 'center', valign: 'middle'},*/
            {title: '操作', field: 'opt', visible: true, align: 'left', valign: 'middle',
            formatter: function (value, row) {
                var btn=[];
                btn+='<p><a href="javascript:void(0);" onclick="AppClaimer.delete_('+row.id+')">删除此推荐</a></p>';
                return btn;

            }
        }
    ];
};


/**
 * 删除推荐申请记录
 */
AppClaimer.delete_ = function (id) {
    var confirm = layer.confirm("确定要删除该推荐记录？", {btn: ['确定', '取消']}, function () {
        layer.close(confirm);
        var ajax = new $ax(Feng.ctxPath + "/appClaimer/delete", function (data) {
            Feng.success("删除成功!");
            AppClaimer.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("appClaimerId", id);
        ajax.start();
    })
};
/**
 * 检查是否选中
 */
AppClaimer.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        AppClaimer.seItem = selected;
        return true;
    }
};
/**
 * 删除礼物管理
 */
AppClaimer.delete = function () {
    if (this.check()) {
        var confirm = layer.confirm('确定要删除该数据？', { btn: ['确定','取消'] }, function(){
            layer.close(confirm);
            var ids="";
            $.each(AppClaimer.seItem, function(i,val){
                ids += val.id+",";
            });
            ids = ids.substring(0, ids.length - 1);
            var ajax = new $ax(Feng.ctxPath + "/appClaimer/delete", function (data) {
                Feng.success("删除成功!");
                AppClaimer.table.refresh();
            }, function (data) {
                Feng.error("删除失败!" + data.responseJSON.message + "!");
            });
            ajax.set("appClaimerId",ids);
            ajax.start();
        })
    }
};
/**
 * 查询推荐申请记录列表
 */
AppClaimer.search = function () {
    var queryData = {};
    queryData['beginTime'] = $("#beginTime").val();
    queryData['endTime'] = $("#endTime").val();
    queryData['rid'] = $("#rid").val();
    queryData['nickName'] = $("#nickName").val();
    queryData['sequence'] = $("#sequence").val();
    AppClaimer.table.refresh({query: queryData});
};
/**
 * 重置
 */
AppClaimer.resetSearch = function () {
    $("#beginTime").val("");
    $("#endTime").val("");
    $("#rid").val("");
    $("#nickName").val("");
    $("#sequence").val("");
    AppClaimer.search();
};
$(function () {
    var defaultColunms = AppClaimer.initColumn();
    var table = new BSTable(AppClaimer.id, "/appClaimer/list?", defaultColunms);
    table.setPaginationType("server");
    AppClaimer.table = table.init();
});
