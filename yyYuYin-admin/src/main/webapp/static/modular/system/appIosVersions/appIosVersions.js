/**
 * ios版本控制管理初始化
 */
var AppIosVersions = {
    id: "AppIosVersionsTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
AppIosVersions.initColumn = function () {
    return [
        {field: 'selectItem', radio: true,visible:false},
            {title: '', field: 'id', visible: false, align: 'center', valign: 'middle'},
            {title: '发布时间', field: 'createDate', visible: true, align: 'center', valign: 'middle'},
            {title: '版本号', field: 'versions', visible: true, align: 'center', valign: 'middle'},
            {title: '版本描述', field: 'mark', visible: true, align: 'center', valign: 'middle'},
            {title: '是否强制更新', field: 'state', visible: true, align: 'center', valign: 'middle',
                formatter: function (value, row) {
                    //1否，2 是
                    if(value==1){
                        return "否";
                    }else if(value==2){
                        return "是";
                    }
                }
            },
            {title: '版本链接', field: 'url', visible: true, align: 'center', valign: 'middle'},
            {title: '操作', field: 'opt', visible: true, align: 'left', valign: 'middle',
                formatter: function (value, row) {
                    var btn=[];
                    btn+='<a href="javascript:void(0);" onclick="AppIosVersions.openAppIosVersionsDetail('+row.id+')">编辑</a>&nbsp;&nbsp;';
                    return btn;

                }
            }
    ];
};

/**
 * 检查是否选中
 */
AppIosVersions.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        AppIosVersions.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加ios版本控制
 */
AppIosVersions.openAddAppIosVersions = function () {
    var index = layer.open({
        type: 2,
        title: '添加ios版本控制',
        area: ['80%', '80%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/appIosVersions/appIosVersions_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看ios版本控制详情
 */
AppIosVersions.openAppIosVersionsDetail = function (id) {
    var index = layer.open({
        type: 2,
        title: 'ios版本控制详情',
        area: ['80%', '80%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/appIosVersions/appIosVersions_update/' + id
    });
    this.layerIndex = index;
};

/**
 * 删除ios版本控制
 */
AppIosVersions.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/appIosVersions/delete", function (data) {
            Feng.success("删除成功!");
            AppIosVersions.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("appIosVersionsId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询ios版本控制列表
 */
AppIosVersions.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    AppIosVersions.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = AppIosVersions.initColumn();
    var table = new BSTable(AppIosVersions.id, "/appIosVersions/list", defaultColunms);
    table.setPaginationType("server");
    AppIosVersions.table = table.init();
});
