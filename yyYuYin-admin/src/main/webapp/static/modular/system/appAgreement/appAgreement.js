/**
 * 协议号管理管理初始化
 */
var AppAgreement = {
    id: "AppAgreementTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
AppAgreement.initColumn = function () {
    return [
        {field: 'selectItem', radio: false},
            {title: '导入时间', field: 'createTime', visible: true, align: 'center', valign: 'middle'},
            {title: '优优ID', field: 'usercoding', visible: true, align: 'center', valign: 'middle'},
            {title: '协议序列号', field: 'index', visible: true, align: 'center', valign: 'middle'},
            {title: '昵称', field: 'nickname', visible: true, align: 'center', valign: 'middle'},
            {title: '出生日期', field: 'dateOfBirth', visible: true, align: 'center', valign: 'middle',
                formatter: function (value, row) {
                    return value.substring(0,value.length-8);
                }
            },
            {title: '性别', field: 'sex', visible: true, align: 'center', valign: 'middle',
                formatter: function (value, row) {
                    if(value==1){
                        return "男";
                    }else {
                        return "女";
                    }
                }
            },
            {title: '是否分配', field: 'state', visible: true, align: 'center', valign: 'middle',
                formatter: function (value, row) {
                    if(value==1){
                        return "否";
                    }else {
                        return "是";
                    }
                }
            },
            {title: '当前所在房间', field: 'eid', visible: true, align: 'center', valign: 'middle'},
            {title: '分配时间', field: 'addTime', visible: true, align: 'center', valign: 'middle'},
            {title: '分配可用天数', field: 'days', visible: true, align: 'center', valign: 'middle',
                formatter: function (value, row) {
                    if(value==null || value==''){
                        return "";
                    }else {
                        if(value==-1){
                            return "不限";
                        }else if(value>0) {
                            return value+"天";
                        }
                    }
                }
            },
            {title: '个性签名', field: 'individuation', visible: true, align: 'center', valign: 'middle',
                formatter: function (value, row) {
                    var v=value;
                    if(value!=null && value!=''){
                        if(value.length>20){
                            v=value.substring(0,20)+"......";
                        }
                        return '<span title="'+value+'">'+v+'</span>';
                    }
                }
            },
            {title: '操作', field: 'opt', visible: true, align: 'left', valign: 'middle',
                formatter: function (value, row) {
                    var btn=[];
                    btn+='<p style="line-height: 10px;"><a href="javascript:void(0);" onclick="AppAgreement.openAppAgreementDetail('+row.id+')">编辑</a>&nbsp;&nbsp;';
                    if(row.state==1){
                        btn+='<a href="javascript:void(0);" onclick="AppAgreement.delete_('+row.id+')">删除</a></p>';
                    }
                    if(row.state==2){//1未分配，2已分配
                        btn+='<p style="line-height: 10px;"><a href="javascript:void(0);" onclick="AppAgreement.income_('+row.id+')">召回协议号</a></p>';
                    }
                    return btn;

                }
            }
    ];
};

/**
 * 检查是否选中
 */
AppAgreement.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        AppAgreement.seItem = selected;
        return true;
    }
};

/**
 * 点击添加协议号管理
 */
AppAgreement.openAddAppAgreement = function () {
    var index = layer.open({
        type: 2,
        title: '添加协议号管理',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/appAgreement/appAgreement_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看协议号管理详情
 */
AppAgreement.openAppAgreementDetail = function (id) {
    var index = layer.open({
        type: 2,
        title: '编辑协议号管理',
        area: ['80%', '80%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/appAgreement/appAgreement_update/' + id
    });
    this.layerIndex = index;
};

/**
 * 单个删除
 * @private
 */
AppAgreement.delete_ = function (id) {
    var confirm = layer.confirm('确定要删除该数据？', { btn: ['确定','取消'] }, function() {
        layer.close(confirm);
        var ajax = new $ax(Feng.ctxPath + "/appAgreement/delete", function (data) {
            Feng.success("删除成功!");
            AppAgreement.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("appAgreementId",id);
        ajax.start();
    });
}
/**
 * 批量删除
 */
AppAgreement.delete = function () {
    if (this.check()) {
        var confirm = layer.confirm('确定要删除该数据？', { btn: ['确定','取消'] }, function(){
            layer.close(confirm);
            var ids="";
            $.each(AppAgreement.seItem, function(i,val){
                ids += val.id+",";
            });
            ids = ids.substring(0, ids.length - 1);
            var ajax = new $ax(Feng.ctxPath + "/appAgreement/delete", function (data) {
                Feng.success("删除成功!");
                AppAgreement.table.refresh();
            }, function (data) {
                Feng.error("删除失败!" + data.responseJSON.message + "!");
            });
            ajax.set("appAgreementId",ids);
            ajax.start();
        })
    }
};
/**
 * 单个召回协议号
 * @private
 */
AppAgreement.income_ = function (id) {
    var confirm = layer.confirm('确定要召回此协议号吗？', { btn: ['确定','取消'] }, function() {
        layer.close(confirm);
        var ajax = new $ax(Feng.ctxPath + "/appAgreement/income", function (data) {
            Feng.success("召回成功!");
            AppAgreement.table.refresh();
        }, function (data) {
            Feng.error("召回失败!" + data.responseJSON.message + "!");
        });
        ajax.set("appAgreementId",id);
        ajax.start();
    });
}
/**
 * 批量召回协议号
 */
AppAgreement.income = function () {
    if (AppAgreement.check()) {
        var ids="";
        var ok=true;
        $.each(AppAgreement.seItem, function(i,val){
            if(val.state==1){
                ok=false;
            }
            ids += val.id+",";
        });
        if(!ok){
            Feng.info("请选择已分配的协议号");
            return ;
        }
        ids = ids.substring(0, ids.length - 1);
        var confirm = layer.confirm('确定要召回这些协议号吗？', { btn: ['确定','取消'] }, function(){
            layer.close(confirm);
            var ajax = new $ax(Feng.ctxPath + "/appAgreement/income", function (data) {
                Feng.success("召回成功!");
                AppAgreement.table.refresh();
            }, function (data) {
                Feng.error("召回失败!" + data.responseJSON.message + "!");
            });
            ajax.set("appAgreementId",ids);
            ajax.start();
        })
    }
};

/**
 * 查询协议号管理列表
 */
AppAgreement.search = function () {
    var queryData = {};
    queryData['beginTime'] = $("#beginTime").val();
    queryData['endTime'] = $("#endTime").val();
    queryData['eid'] = $("#eid").val();
    queryData['state'] = $("#state").val();
    queryData['usercoding'] = $("#usercoding").val();
    AppAgreement.table.refresh({query: queryData});
};
/**
 * 重置
 */
AppAgreement.resetSearch = function () {
    $("#beginTime").val("");
    $("#endTime").val("");
    $("#eid").val("");
    $("#state").val("");
    $("#usercoding").val("");
    AppAgreement.search();
};
$(function () {
    var defaultColunms = AppAgreement.initColumn();
    var table = new BSTable(AppAgreement.id, "/appAgreement/list", defaultColunms);
    table.setPaginationType("server");
    AppAgreement.table = table.init();
});

/**
 * ============================excel导入===================================
 */

var agreement = function(){

    this.init = function(){

        //模拟上传excel  
        $("#uploadEventBtn").unbind("click").bind("click",function(){
            $("#uploadEventFile").click();
        });
    };
}
//点击上传按钮  
AppAgreement.uploadBtn = function(){
    var uploadEventFile = $("#uploadEventFile").val();
    if(uploadEventFile == ''){
        Feng.info("请选择excel,再上传");
    }else if(uploadEventFile.lastIndexOf(".xls")<0){//可判断以.xls和.xlsx结尾的excel  
        Feng.info("只能上传Excel文件");
    }else{
        var url =  Feng.ctxPath + '/appAgreement/import/';
        var file = document.querySelector('input[name=file]').files[0];
        var reader = new FileReader();
        if (file) {
            var formData = new FormData();
            formData.append("myfile", file);
            this.sendAjaxRequest(url, 'POST', formData);
        }
    }
};

AppAgreement.sendAjaxRequest = function(url,type,data){
    $.ajax({
        url : url,
        type : type,
        data : data,
        success : function(result) {
            Feng.success("导入成功!");
            AppAgreement.table.refresh();
        },
        error : function() {
            Feng.error( "excel上传失败");
        },
        cache : false,
        contentType : false,
        processData : false
    });
};
//模板导出
AppAgreement.export = function () {
    window.location.href=Feng.ctxPath + "/export/appAgreement";

};

var agreement;
$(function(){
    agreement = new agreement();
    agreement.init();
});
