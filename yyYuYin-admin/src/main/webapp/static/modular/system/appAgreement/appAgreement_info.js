/**
 * 初始化协议号管理详情对话框
 */
var AppAgreementInfoDlg = {
    appAgreementInfoData : {}
};

/**
 * 清除数据
 */
AppAgreementInfoDlg.clearData = function() {
    this.appAgreementInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
AppAgreementInfoDlg.set = function(key, val) {
    this.appAgreementInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
AppAgreementInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
AppAgreementInfoDlg.close = function() {
    parent.layer.close(window.parent.AppAgreement.layerIndex);
}

/**
 * 收集数据
 */
AppAgreementInfoDlg.collectData = function() {
    this
    .set('id')
    .set('createTime')
    .set('usercoding')
    .set('individuation')
    .set('sex')
    .set('index')
    .set('dateOfBirth')
    .set('nickname')
    .set('eid')
    .set('state')
    .set('img')
    .set('isDelete');
}

/**
 * 提交添加
 */
AppAgreementInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/appAgreement/add", function(data){
        Feng.success("添加成功!");
        window.parent.AppAgreement.table.refresh();
        AppAgreementInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.appAgreementInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
AppAgreementInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/appAgreement/update", function(data){
        Feng.success("修改成功!");
        window.parent.AppAgreement.table.refresh();
        AppAgreementInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.appAgreementInfoData);
    ajax.start();
}

$(function() {
    // 初始化图片上传
    var imageUp = new $WebUploadImage("img");
    imageUp.setUploadBarId("progressBar");
    imageUp.init();

    //$("#dateOfBirth").val($("#dateOfBirth_").val());
});
