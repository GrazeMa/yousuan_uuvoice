/**
 * 音乐管理管理初始化
 */
var AppMusic = {
    id: "AppMusicTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
AppMusic.initColumn = function () {
    return [
        {field: 'selectItem', radio: false},
            {title: '', field: 'id', visible: false, align: 'center', valign: 'middle'},
            {title: '上传时间', field: 'createDate', visible: true, align: 'center', valign: 'middle'},
            {title: '音乐名称', field: 'musicName', visible: true, align: 'center', valign: 'middle'},
            {title: '歌手', field: 'gsNane', visible: true, align: 'center', valign: 'middle'},
            {title: '试听音乐', field: 'url', visible: true, align: 'center', valign: 'middle',
                formatter: function (value, row) {
                    return "<i class='fa fa-play-circle' onclick='AppMusic.play(this)' name='"+value+"'></i>"
                }
            },
            {title: '上传者ID', field: 'uid', visible: true, align: 'center', valign: 'middle'},
            {title: '上传者昵称', field: 'nickName', visible: true, align: 'center', valign: 'middle'},
            {title: '下载数', field: 'downloads', visible: true, align: 'center', valign: 'middle'},
            {title: '是否上架', field: 'status', visible: true, align: 'center', valign: 'middle',
                formatter: function (value, row) {
                    //是否上架，1上架，2下架
                    if(value==1){
                        return "上架";
                    }else if(value==2){
                        return "下架";
                    }
                }
            },
        {title: '操作', field: 'opt', visible: true, align: 'left', valign: 'middle',
            formatter: function (value, row) {
                var btn=[];
                if(row.status==1){//1上架，2下架
                    btn[0]=['<a href="javascript:void(0);" onclick="AppMusic.isXiajia('+row.id+',2)">下架歌曲</a>'];
                }else{
                    btn[0]=['<a href="javascript:void(0);" onclick="AppMusic.isXiajia('+row.id+',1)">上架歌曲</a>'];
                    btn[1]=['<a href="javascript:void(0);" onclick="AppMusic.delete_('+row.id+')">删除歌曲</a>'];
                }
                return btn;

            }
        }


    ];
};

/**
 * 检查是否选中
 */
AppMusic.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        AppMusic.seItem = selected;
        return true;
    }
};
/**
 * 播放音乐
 */
AppMusic.play=function(e){
        var url=$(e).attr("name");
        /**
         * 音频试听（可选）
         */
        layer.open({
            type: 1,
            title: false,
            closeBtn: 0,
            area: 'auto',
            skin: 'layui-layer-nobg', //没有背景色
            shadeClose: true,
            content: '<div><audio width="100px" height="100px" autoplay="autoplay" controls  src="'+url+'">当前浏览器不支持播放该音频</audio></div>'
        });
}

/**
 * 点击添加音乐管理
 */
AppMusic.openAddAppMusic = function () {
    var index = layer.open({
        type: 2,
        title: '添加音乐管理',
        area: ['80%', '80%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/appMusic/appMusic_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看音乐管理详情
 */
AppMusic.openAppMusicDetail = function (id) {
    var index = layer.open({
        type: 2,
        title: '音乐管理详情',
        area: ['80%', '80%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/appMusic/appMusic_update/' + id
    });
    this.layerIndex = index;
};
/*单独删除*/
AppMusic.delete_=function(id){
    var confirm = layer.confirm('确定要删除该数据？', { btn: ['确定','取消'] }, function(){
        layer.close(confirm);
        var ajax = new $ax(Feng.ctxPath + "/appMusic/delete", function (data) {
            Feng.success("删除成功!");
            AppMusic.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("appMusicId",id);
        ajax.start();
    });
}
/**
 * 删除音乐管理
 */
AppMusic.delete = function () {
    if (this.check()) {
        var confirm = layer.confirm('确定要删除这些数据？', { btn: ['确定','取消'] }, function(){
            layer.close(confirm);
            var ids="";
            $.each(AppMusic.seItem, function(i,val){
                ids += val.id+",";
            });
            ids = ids.substring(0, ids.length - 1);
            var ajax = new $ax(Feng.ctxPath + "/appMusic/delete", function (data) {
                Feng.success("删除成功!");
                AppMusic.table.refresh();
            }, function (data) {
                Feng.error("删除失败!" + data.responseJSON.message + "!");
            });
            ajax.set("appMusicId",ids);
            ajax.start();
        })
    }
};
/**
 * 上架和下架
 */
AppMusic.isXiajia = function (id,state) {
    var stateName = "下架";
    if (state == 1) {
        stateName = "上架";
    }
    var confirm = layer.confirm("确定要"+stateName+"该音乐？", {btn: ['确定', '取消']}, function () {
        layer.close(confirm);
        var ajax = new $ax(Feng.ctxPath + "/appMusic/updateXiajia", function (data) {
            Feng.success(""+stateName+"成功!");
            AppMusic.table.refresh();
        }, function (data) {
            Feng.error(""+stateName+"失败!" + data.responseJSON.message + "!");
        });
        ajax.set("ids",id);
        ajax.set("status",state);
        ajax.start();
    })
};
/**
 * 查询音乐管理列表
 */
AppMusic.search = function () {
    var queryData = {};
    queryData['beginTime'] = $("#beginTime").val();
    queryData['endTime'] = $("#endTime").val();
    queryData['uid'] = $("#uid").val();
    queryData['status'] = $("#status").val();
    queryData['musicName'] = $("#musicName").val();
    queryData['gsNane'] = $("#gsNane").val();
    AppMusic.table.refresh({query: queryData});
};
/**
 * 重置
 */
AppMusic.resetSearch = function () {
    $("#beginTime").val("");
    $("#endTime").val("");
    $("#uid").val("");
    $("#status").val("");
    $("#musicName").val("");
    $("#gsNane").val("");
    AppMusic.search();
};

$(function () {
    var defaultColunms = AppMusic.initColumn();
    var table = new BSTable(AppMusic.id, "/appMusic/list?state=2", defaultColunms);
    table.setPaginationType("server");
    AppMusic.table = table.init();
});
