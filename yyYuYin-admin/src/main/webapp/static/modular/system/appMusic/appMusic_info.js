/**
 * 初始化音乐管理详情对话框
 */
var AppMusicInfoDlg = {
    appMusicInfoData : {},
    validateFields: {
        musicName: {
            validators: {
                notEmpty: {
                    message: '音乐名称不能为空'
                }
            }
        },
        gsNane: {
                validators: {
                    notEmpty: {
                        message: '歌手名称不能为空'
                    }
                }
            }
    }
};

/**
 * 清除数据
 */
AppMusicInfoDlg.clearData = function() {
    this.appMusicInfoData = {};
}
/**
 * 验证数据是否为空
 */
AppMusicInfoDlg.validate = function () {
    $('#horizontalForm').data("bootstrapValidator").resetForm();
    $('#horizontalForm').bootstrapValidator('validate');
    return $("#horizontalForm").data('bootstrapValidator').isValid();
};

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
AppMusicInfoDlg.set = function(key, val) {
    this.appMusicInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
AppMusicInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
AppMusicInfoDlg.close = function() {
    parent.layer.close(window.parent.AppMusic.layerIndex);
}

/**
 * 收集数据
 */
AppMusicInfoDlg.collectData = function() {
    var state=$(":radio[name='state']:checked").val();
    this.appMusicInfoData['state'] = state;
    this
    .set('id')
    .set('createDate')
    .set('musicName')
    .set('gsNane')
    .set('uid')
    .set('isDelete')
    .set('uuid')
    .set('nickName')
    .set('remark')
    .set('downloads');
}

/**
 * 提交添加
 */
AppMusicInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();
    if(!this.validate()){
        return ;
    }
    var url=$("#url").val();
    if(url==null || url==""){
        layer.msg("请上传音乐");
        return;
    }
    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/appMusic/add", function(data){
        Feng.success("添加成功!");
        window.parent.AppMusic.table.refresh();
        AppMusicInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    var myVid=$("#urlPreId").find("audio")[0].duration;
    size=Math.ceil(myVid);
    ajax.set(this.appMusicInfoData);
    ajax.set("url",url);
    ajax.set("times",size);
    ajax.start();
}

/**
 * 提交修改
 */
AppMusicInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/appMusic/update", function(data){
        Feng.success("操作成功!");
        window.parent.AppMusic.table.refresh();
        AppMusicInfoDlg.close();
    },function(data){
        Feng.error("操作失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.appMusicInfoData);
    ajax.start();
}

$(function() {
    Feng.initValidator("horizontalForm", AppMusicInfoDlg.validateFields);
    // 初始化图片上传
    var imageUp = new $WebUploadMusic("url");
    imageUp.setUploadBarId("progressBar");
    imageUp.init();
});
