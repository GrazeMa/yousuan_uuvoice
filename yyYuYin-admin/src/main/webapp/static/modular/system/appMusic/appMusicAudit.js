/**
 * 音乐管理管理初始化
 */
var AppMusic = {
    id: "AppMusicTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
AppMusic.initColumn = function () {
    return [
        {field: 'selectItem', radio: true,visible:false},
            {title: '', field: 'id', visible: false, align: 'center', valign: 'middle'},
            {title: '上传时间', field: 'createDate', visible: true, align: 'center', valign: 'middle'},
            {title: '音乐名称', field: 'musicName', visible: true, align: 'center', valign: 'middle'},
            {title: '歌手', field: 'gsNane', visible: true, align: 'center', valign: 'middle'},
            {title: '试听音乐', field: 'url', visible: true, align: 'center', valign: 'middle',
                formatter: function (value, row) {
                    return "<i class='fa fa-play-circle' onclick='AppMusic.play(this)' name='"+value+"'></i>"
                }
            },
            {title: '上传者ID', field: 'uid', visible: true, align: 'center', valign: 'middle'},
            {title: '上传者昵称', field: 'nickName', visible: true, align: 'center', valign: 'middle'},
            {title: '备注', field: 'remark', visible: true, align: 'center', valign: 'middle',
                formatter: function (value, row) {
                    var v=value;
                    if(value!=null && value!=''){
                        if(value.length>20){
                            v=value.substring(0,20)+"......";
                        }
                        return '<span title="'+value+'">'+v+'</span>';
                    }
                }
            },
            {title: '状态', field: 'state', visible: true, align: 'center', valign: 'middle',
                formatter: function (value, row) {
                    //1 未审核， 2 审核通过 ，3已拒绝
                    if(value==1){
                        return "未审核";
                    }else if(value==2){
                        return "已通过";
                    }else if(value==3){
                        return "已拒绝";
                    }
                }
            },
        {title: '操作', field: 'opt', visible: true, align: 'left', valign: 'middle',
            formatter: function (value, row) {
                var btn=[];
                if(row.state==1){//1 待审核，2 审核通过， 3 已拒绝
                    btn[0]=['<a href="javascript:void(0);" onclick="AppMusic.audit('+row.id+')">立即审核</a>'];
                    return btn;
                }else{
                    return "不可操作";
                }
            }
        }


    ];
};
/**
 * 播放音乐
 */
AppMusic.play=function(e){
    var url=$(e).attr("name");
    /**
     * 音频试听（可选）
     */
    layer.open({
        type: 1,
        title: false,
        closeBtn: 0,
        area: 'auto',
        skin: 'layui-layer-nobg', //没有背景色
        shadeClose: true,
        content: '<div><audio width="100px" height="100px" autoplay="autoplay" controls  src="'+url+'">当前浏览器不支持播放该音频</audio></div>'
    });
}
/**
 * 检查是否选中
 */
AppMusic.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        AppMusic.seItem = selected[0];
        return true;
    }
};

/**
 * 打开查看音乐管理详情
 */
AppMusic.audit = function (id) {
    var index = layer.open({
        type: 2,
        title: '立即审核',
        area: ['80%', '80%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/appMusic/appMusic_update/' +id
    });
    this.layerIndex = index;
};
/**
 * 查询音乐管理列表
 */
AppMusic.search = function () {
    var queryData = {};
    queryData['beginTime'] = $("#beginTime").val();
    queryData['endTime'] = $("#endTime").val();
    queryData['uid'] = $("#uid").val();
    queryData['state'] = $("#state").val();
    queryData['musicName'] = $("#musicName").val();
    queryData['gsNane'] = $("#gsNane").val();
    AppMusic.table.refresh({query: queryData});
};
/**
 * 重置
 */
AppMusic.resetSearch = function () {
    $("#beginTime").val("");
    $("#endTime").val("");
    $("#uid").val("");
    $("#state").val("");
    $("#musicName").val("");
    $("#gsNane").val("");
    AppMusic.search();
};


$(function () {
    var defaultColunms = AppMusic.initColumn();
    var table = new BSTable(AppMusic.id, "/appMusic/list", defaultColunms);
    table.setPaginationType("server");
    AppMusic.table = table.init();
});
