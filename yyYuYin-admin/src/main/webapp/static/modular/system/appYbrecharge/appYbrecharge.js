/**
 * 优币充值管理初始化
 */
var AppYbrecharge = {
    id: "AppYbrechargeTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
AppYbrecharge.initColumn = function () {
    return [
        {field: 'selectItem', radio: true,visible:false},
            {title: '', field: 'id', visible: false, align: 'center', valign: 'middle'},
            {title: '充值时间', field: 'createTime', visible: true, align: 'center', valign: 'middle'},
            {title: '充值用户', field: 'name', visible: true, align: 'center', valign: 'middle'},
            {title: '优优ID', field: 'eid', visible: true, align: 'center', valign: 'middle'},
            {title: '充值优币', field: 'gmGold', visible: true, align: 'center', valign: 'middle'},
            {title: '赠送优币', field: 'zsGold', visible: true, align: 'center', valign: 'middle'},
            {title: '实得优币', field: 'sdGold', visible: true, align: 'center', valign: 'middle'},
            {title: '支付金额', field: 'play', visible: true, align: 'center', valign: 'middle',
            	footerFormatter: function (items) {
            		var count = 0;
            	    for (var i in items) {
            	        count += items[i].play;
            	    }
            	    return "总计: " + count.toFixed(2);
            	}
            },
            {title: '支付方式', field: 'playState', visible: true, align: 'center', valign: 'middle',
                formatter: function (value, row) {
                    //1 支付宝 , 2 微信, 3苹果内购,4公众号，5手动添加
                    if(value==1){
                        return "支付宝"
                    }else if(value==2){
                        return "微信"
                    }else if(value==3){
                        return "苹果内购"
                    }else if(value==4){
                        return "公众号"
                    }else{
                        return "手动添加"
                    }
                }
            },
            {title: '操作', field: 'opt', visible: true, align: 'left', valign: 'middle',
                formatter: function (value, row) {
                    var btn=[];
                    if(row.playState==5  || row.playState==4){//只有手动添加可以编辑
                        btn[0]=['<a href="javascript:void(0);" onclick="AppYbrecharge.openAppYbrechargeDetail ('+row.id+')">编辑</a>'];
                        return btn;
                    }else{
                        return "不可操作";
                    }


                }
            }

    ];
};

/**
 * 检查是否选中
 */
AppYbrecharge.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        AppYbrecharge.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加优币充值
 */
AppYbrecharge.openAddAppYbrecharge = function () {
    var index = layer.open({
        type: 2,
        title: '添加优币充值',
        area: ['80%', '80%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/appYbrecharge/appYbrecharge_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看优币充值详情
 */
AppYbrecharge.openAppYbrechargeDetail = function (id) {
    var index = layer.open({
        type: 2,
        title: '优币充值详情',
        area: ['80%', '80%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/appYbrecharge/appYbrecharge_update/' + id
    });
    this.layerIndex = index;
};

//导出
AppYbrecharge.export = function () {
    var beginTime= $("#beginTime").val();
    var endTime = $("#endTime").val();
    var eid = $("#eid").val();
    var name = $("#name").val();
    window.location.href=Feng.ctxPath + "/export/appYbrecharge?beginTime="+beginTime+"&endTime="+endTime+"&eid="+eid+"&name="+name;

};
/**
 * 查询优币充值列表
 */
AppYbrecharge.search = function () {
    var queryData = {};
    queryData['beginTime'] = $("#beginTime").val();
    queryData['endTime'] = $("#endTime").val();
    queryData['eid'] = $("#eid").val();
    queryData['name'] = $("#name").val();
    AppYbrecharge.table.refresh({query: queryData});
};
/**
 * 重置
 */
AppYbrecharge.resetSearch = function () {
    $("#beginTime").val("");
    $("#endTime").val("");
    $("#eid").val("");
    $("#name").val("");
    AppYbrecharge.search();
};
$(function () {
    var defaultColunms = AppYbrecharge.initColumn();
    var table = new BSTable(AppYbrecharge.id, "/appYbrecharge/list", defaultColunms);
    table.setPaginationType("client");
    AppYbrecharge.table = table.init();
});
