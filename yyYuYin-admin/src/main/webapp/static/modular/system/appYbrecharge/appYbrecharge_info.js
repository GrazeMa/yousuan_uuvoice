/**
 * 初始化优币充值详情对话框
 */
var AppYbrechargeInfoDlg = {
    appYbrechargeInfoData : {},
    validateFields: {
        eid: {
            validators: {
                notEmpty: {
                    message: '充值用户优优ID不能为空'
                }
            }
        },
        gmGold: {
            validators: {
                notEmpty: {
                    message: '充值优币不能为空'
                }
            }
            ,regexp: {
                regexp: /^[1-9]\d*$/,
                message: '数字格式不正确'
            }
        },
        zsGold: {
            validators: {
                notEmpty: {
                    message: '赠送优币不能为空'
                }
                ,regexp: {
                    regexp: /^[1-9]\d*$/,
                    message: '数字格式不正确'
                }
            }
        },
        play: {
            validators: {
                notEmpty: {
                    message: '支付金额不能为空'
                }
                ,regexp: {
                    regexp: /(^[1-9]([0-9]+)?(\.[0-9]{1,2})?$)|(^(0){1}$)|(^[0-9]\.[0-9]([0-9])?$)/ ,
                    message: '数字格式不正确'
                }
            }
        }
    }
};
/**
 * 验证数据是否为空
 */
AppYbrechargeInfoDlg.validate = function () {
    $('#horizontalForm').data("bootstrapValidator").resetForm();
    $('#horizontalForm').bootstrapValidator('validate');
    return $("#horizontalForm").data('bootstrapValidator').isValid();
};

/**
 * 清除数据
 */
AppYbrechargeInfoDlg.clearData = function() {
    this.appYbrechargeInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
AppYbrechargeInfoDlg.set = function(key, val) {
    this.appYbrechargeInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
AppYbrechargeInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
AppYbrechargeInfoDlg.close = function() {
    parent.layer.close(window.parent.AppYbrecharge.layerIndex);
}

/**
 * 收集数据
 */
AppYbrechargeInfoDlg.collectData = function() {
    this
    .set('id')
    .set('createTime')
    .set('uid')
    .set('name')
    .set('eid')
    .set('gmGold')
    .set('zsGold')
    .set('sdGold')
    .set('play')
    .set('playState')
    .set('isDelete');
}

/**
 * 提交添加
 */
AppYbrechargeInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();
    if(!this.validate()){
        return ;
    }
    var eid_=$("#eid_").val();
    if(eid_!=0){
        Feng.info("请输入正确的优优ID");
        return;
    }
    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/appYbrecharge/add", function(data){
        Feng.success("添加成功!");
        window.parent.AppYbrecharge.table.refresh();
        AppYbrechargeInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.appYbrechargeInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
AppYbrechargeInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();
    if(!this.validate()){
        return ;
    }
    var eid_=$("#eid_").val();
    if(eid_!=0){
        Feng.info("请输入正确的优优ID");
        return;
    }
    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/appYbrecharge/update", function(data){
        Feng.success("修改成功!");
        window.parent.AppYbrecharge.table.refresh();
        AppYbrechargeInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.appYbrechargeInfoData);
    ajax.start();
}
/**
 * 检查输入的用户是否存
 */
AppYbrechargeInfoDlg.checkEid=function(e){
    var ajax = new $ax(Feng.ctxPath + "/appYbrecharge/getUserByEid", function(data){
        if(data==0){
            Feng.info("请输入正确的优优ID");
            $("#eid_").val(data);
        }
    },function(data){
        Feng.error("获取失败!" + data.responseJSON.message + "!");
    });
    ajax.set("eid",$(e).val());
    ajax.start();
}
$(function() {
    Feng.initValidator("horizontalForm", AppYbrechargeInfoDlg.validateFields);
});
