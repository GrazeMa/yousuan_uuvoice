package com.stylefeng.guns.modular.system.service.impl;

import com.stylefeng.guns.modular.system.model.AppDivide;
import com.stylefeng.guns.modular.system.dao.AppDivideMapper;
import com.stylefeng.guns.modular.system.service.IAppDivideService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 充值分成金额 服务实现类
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-06
 */
@Service
public class AppDivideServiceImpl extends ServiceImpl<AppDivideMapper, AppDivide> implements IAppDivideService {
    @Override
    public List<Map<String, Object>> getAppDivideList(AppDivide appDivide) {
        return this.baseMapper.getAppDivideList(appDivide);
    }

    @Override
    public List<Map<String, Object>> getAppDivideSum(AppDivide appDivide) {
        return this.baseMapper.getAppDivideSum(appDivide);
    }
}
