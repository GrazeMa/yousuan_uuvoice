package com.stylefeng.guns.modular.system.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 举报
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-13
 */
@TableName("app_report")
public class AppReport extends Model<AppReport> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    private String uidname;
    /**
     * 举报用户id
     */
    private Integer uid;
    private String buidname;
    /**
     * 被举报用户id
     */
    private Integer buid;
    /**
     * 举报原因
     */
    private String content;
    /**
     * '举报状态（1=未处理  2=已处理）'
     */
    private Integer status;
    /**
     * '是否被删除（1=否  2=是）'
     */
    private Integer isDelete;
    /**
     * '举报时间'
     */
    private Date createTime;
    /**
     * 处理结果
     */
    private String hand;
    /**
     * 1 是用户，2 是房间
     */
    private Integer role;
    /**
     * 用户优优号和房间号
     */
    private String usercoding;
    /**
     *  冻结天数 ： -1不冻结，1：1天，2：2天，7：7天，0永久
     */
    private Integer lockDays;

    @TableField(exist = false)
    private Integer type;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUidname() {
        return uidname;
    }

    public void setUidname(String uidname) {
        this.uidname = uidname;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public String getBuidname() {
        return buidname;
    }

    public void setBuidname(String buidname) {
        this.buidname = buidname;
    }

    public Integer getBuid() {
        return buid;
    }

    public void setBuid(Integer buid) {
        this.buid = buid;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getHand() {
        return hand;
    }

    public void setHand(String hand) {
        this.hand = hand;
    }

    public Integer getRole() {
        return role;
    }

    public void setRole(Integer role) {
        this.role = role;
    }

    public String getUsercoding() {
        return usercoding;
    }

    public void setUsercoding(String usercoding) {
        this.usercoding = usercoding;
    }

    public Integer getLockDays() {
        return lockDays;
    }

    public void setLockDays(Integer lockDays) {
        this.lockDays = lockDays;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "AppReport{" +
        "id=" + id +
        ", uidname=" + uidname +
        ", uid=" + uid +
        ", buidname=" + buidname +
        ", buid=" + buid +
        ", content=" + content +
        ", status=" + status +
        ", isDelete=" + isDelete +
        ", createTime=" + createTime +
        ", hand=" + hand +
        ", role=" + role +
        ", usercoding=" + usercoding +
        "}";
    }
}
