package com.stylefeng.guns.modular.system.dao;

import com.stylefeng.guns.modular.system.model.AppPk;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 房间pk
 Mapper 接口
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-08
 */
public interface AppPkMapper extends BaseMapper<AppPk> {
    /**
     * 根据房间编号获取pk信息
     * @param appPk
     * @return
     */
    List<Map<String,Object>> getAppPkList(AppPk appPk);
}
