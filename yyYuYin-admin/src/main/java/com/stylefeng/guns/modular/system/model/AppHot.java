package com.stylefeng.guns.modular.system.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 热搜词
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-12
 */
@TableName("app_hot")
public class AppHot extends Model<AppHot> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    private Date createTime;
    /**
     * 热搜词
     */
    private String name;
    /**
     * 1平台，2 用户
     */
    private Integer state;
    /**
     * 是否是热搜 1否，2是
     */
    private Integer status;
    /**
     * 搜索次数
     */
    private Integer num;
    /**
     * 1 否,2 是
     */
    private Integer isDelete;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "AppHot{" +
        "id=" + id +
        ", createTime=" + createTime +
        ", name=" + name +
        ", state=" + state +
        ", status=" + status +
        ", num=" + num +
        ", isDelete=" + isDelete +
        "}";
    }
}
