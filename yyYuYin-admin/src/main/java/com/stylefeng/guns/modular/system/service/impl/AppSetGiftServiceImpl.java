package com.stylefeng.guns.modular.system.service.impl;

import com.stylefeng.guns.modular.system.model.AppSetGift;
import com.stylefeng.guns.modular.system.dao.AppSetGiftMapper;
import com.stylefeng.guns.modular.system.service.IAppSetGiftService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 礼物设置 服务实现类
 * </p>
 *
 * @author stylefeng123
 * @since 2019-02-28
 */
@Service
public class AppSetGiftServiceImpl extends ServiceImpl<AppSetGiftMapper, AppSetGift> implements IAppSetGiftService {
    @Override
    public List<Map<String, Object>> getAppSetGiftAndBox(Map<String, Object> map) {
        return this.baseMapper.getAppSetGiftAndBox(map);
    }
}
