package com.stylefeng.guns.modular.system.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.common.constant.factory.PageFactory;
import com.stylefeng.guns.core.util.SinataUtil;
import com.stylefeng.guns.modular.system.model.AppBlock;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;
import com.stylefeng.guns.modular.system.model.AppBroad;
import com.stylefeng.guns.modular.system.service.IAppBroadService;

import java.util.List;
import java.util.Map;

/**
 * 广播交友控制器
 * @Date 2019-03-12 19:21:19
 */
@Controller
@RequestMapping("/appBroad")
public class AppBroadController extends BaseController {

    private String PREFIX = "/system/appBroad/";

    @Autowired
    private IAppBroadService appBroadService;

    /**
     * 跳转到广播交友首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "appBroad.html";
    }

    /**
     * 跳转到添加广播交友
     */
    @RequestMapping("/appBroad_add")
    public String appBroadAdd() {
        return PREFIX + "appBroad_add.html";
    }

    /**
     * 跳转到修改广播交友
     */
    @RequestMapping("/appBroad_update/{appBroadId}")
    public String appBroadUpdate(@PathVariable Integer appBroadId, Model model) {
        AppBroad appBroad = appBroadService.selectById(appBroadId);
        model.addAttribute("item",appBroad);
        LogObjectHolder.me().set(appBroad);
        return PREFIX + "appBroad_edit.html";
    }

    /**
     * 获取广播交友列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(AppBroad appBroad) {
        Page<Map<String,Object>> page = new PageFactory<Map<String,Object>>().defaultPage();
        List<Map<String, Object>> result = appBroadService.getAppBroad(appBroad,page );
        page.setRecords(result);
        return super.packForBT(page);
    }

    /**
     * 新增广播交友
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(AppBroad appBroad) {
        appBroadService.insert(appBroad);
        return SUCCESS_TIP;
    }

    /**
     * 删除广播交友
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(String appBroadId) {
        EntityWrapper<AppBroad> entityWrapper=new EntityWrapper<>();
        entityWrapper.in("id",appBroadId);
        appBroadService.delete(entityWrapper);
        return SUCCESS_TIP;
    }

    /**
     * 修改广播交友
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(AppBroad appBroad) {
        appBroadService.updateById(appBroad);
        return SUCCESS_TIP;
    }

    /**
     * 广播交友详情
     */
    @RequestMapping(value = "/detail/{appBroadId}")
    @ResponseBody
    public Object detail(@PathVariable("appBroadId") Integer appBroadId) {
        return appBroadService.selectById(appBroadId);
    }
}
