package com.stylefeng.guns.modular.system.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.modular.system.model.AppBackdrop;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;
import com.stylefeng.guns.modular.system.model.AppBlock;
import com.stylefeng.guns.modular.system.service.IAppBlockService;

/**
 * 房间黑名单控制器
 * @Date 2019-03-08 16:31:09
 */
@Controller
@RequestMapping("/appBlock")
public class AppBlockController extends BaseController {

    private String PREFIX = "/system/appBlock/";

    @Autowired
    private IAppBlockService appBlockService;

    /**
     * 跳转到房间黑名单首页
     */
    @RequestMapping("")
    public String index(Integer rid,Model model) {
        model.addAttribute("rid",rid);
        return PREFIX + "appBlock.html";
    }

    /**
     * 跳转到添加房间黑名单
     */
    @RequestMapping("/appBlock_add")
    public String appBlockAdd() {
        return PREFIX + "appBlock_add.html";
    }

    /**
     * 跳转到修改房间黑名单
     */
    @RequestMapping("/appBlock_update/{appBlockId}")
    public String appBlockUpdate(@PathVariable Integer appBlockId, Model model) {
        AppBlock appBlock = appBlockService.selectById(appBlockId);
        model.addAttribute("item",appBlock);
        LogObjectHolder.me().set(appBlock);
        return PREFIX + "appBlock_edit.html";
    }

    /**
     * 获取房间黑名单列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(AppBlock appBlock) {
        return appBlockService.selectList(new EntityWrapper<AppBlock>().eq("rid",appBlock.getRid()));
    }

    /**
     * 新增房间黑名单
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(AppBlock appBlock) {
        appBlockService.insert(appBlock);
        return SUCCESS_TIP;
    }

    /**
     * 删除房间黑名单
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer appBlockId) {
        appBlockService.deleteById(appBlockId);
        return SUCCESS_TIP;
    }

    /**
     * 修改房间黑名单
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(AppBlock appBlock) {
        appBlockService.updateById(appBlock);
        return SUCCESS_TIP;
    }

    /**
     * 房间黑名单详情
     */
    @RequestMapping(value = "/detail/{appBlockId}")
    @ResponseBody
    public Object detail(@PathVariable("appBlockId") Integer appBlockId) {
        return appBlockService.selectById(appBlockId);
    }
}
