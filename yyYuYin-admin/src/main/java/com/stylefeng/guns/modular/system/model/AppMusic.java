package com.stylefeng.guns.modular.system.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 音乐审核
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-12
 */
@TableName("app_music")
public class AppMusic extends Model<AppMusic> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    private Date createDate;
    /**
     * 名称
     */
    private String musicName;
    private String gsNane;
    /**
     * 音乐路径
     */
    private String url;
    /**
     * 用户id
     */
    private Integer uid;
    /**
     * 1 未审核， 2 审核通过 ，3已拒绝
     */
    private Integer state;
    /**
     * 1 否,2 是
     */
    private Integer isDelete;
    /**
     * 用户uuid
     */
    private Integer uuid;
    /**
     * 用户昵称
     */
    private String nickName;
    /**
     * 下载数
     */
    private Integer downloads;
    /**
     * 是否上架，1否，2是
     */
    private Integer status;
    /**
     * 审核备注
     */
    private String  remark;
    /**
     * 音乐时长
     */
    private Integer times;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getMusicName() {
        return musicName;
    }

    public void setMusicName(String musicName) {
        this.musicName = musicName;
    }

    public String getGsNane() {
        return gsNane;
    }

    public void setGsNane(String gsNane) {
        this.gsNane = gsNane;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    public Integer getUuid() {
        return uuid;
    }

    public void setUuid(Integer uuid) {
        this.uuid = uuid;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public Integer getDownloads() {
        return downloads;
    }

    public void setDownloads(Integer downloads) {
        this.downloads = downloads;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    public Integer getTimes() {
        return times;
    }

    public void setTimes(Integer times) {
        this.times = times;
    }

    @Override
    public String toString() {
        return "AppMusic{" +
        "id=" + id +
        ", createDate=" + createDate +
        ", musicName=" + musicName +
        ", gsNane=" + gsNane +
        ", url=" + url +
        ", uid=" + uid +
        ", state=" + state +
        ", isDelete=" + isDelete +
        ", uuid=" + uuid +
        ", nickName=" + nickName +
        ", downloads=" + downloads +
        "}";
    }
}
