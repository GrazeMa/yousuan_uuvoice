package com.stylefeng.guns.modular.system.service.impl;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.system.model.AppClaimer;
import com.stylefeng.guns.modular.system.dao.AppClaimerMapper;
import com.stylefeng.guns.modular.system.service.IAppClaimerService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 推荐位申请记录 服务实现类
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-12
 */
@Service
public class AppClaimerServiceImpl extends ServiceImpl<AppClaimerMapper, AppClaimer> implements IAppClaimerService {
    @Override
    public List<Map<String, Object>> getAppClaimer(AppClaimer appClaimer,Page<Map<String,Object>> page) {
        return this.baseMapper.getAppClaimer(appClaimer,page);
    }

    @Override
    public AppClaimer getAppClaimerByTime(Map<String,Object> map) {
        return this.baseMapper.getAppClaimerByTime(map);
    }

    @Override
    public Integer getAppClaimerByTimeCount(Map<String, Object> map) {
        return this.baseMapper.getAppClaimerByTimeCount(map);
    }
}
