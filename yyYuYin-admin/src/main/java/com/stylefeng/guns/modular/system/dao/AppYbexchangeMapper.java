package com.stylefeng.guns.modular.system.dao;

import com.stylefeng.guns.modular.system.model.AppYbexchange;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 优币兑换记录 Mapper 接口
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-14
 */
public interface AppYbexchangeMapper extends BaseMapper<AppYbexchange> {

}
