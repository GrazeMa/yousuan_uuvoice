package com.stylefeng.guns.modular.system.service.impl;

import com.stylefeng.guns.modular.system.model.AppRecord;
import com.stylefeng.guns.modular.system.dao.AppRecordMapper;
import com.stylefeng.guns.modular.system.service.IAppRecordService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 操作记录 服务实现类
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-01
 */
@Service
public class AppRecordServiceImpl extends ServiceImpl<AppRecordMapper, AppRecord> implements IAppRecordService {

}
