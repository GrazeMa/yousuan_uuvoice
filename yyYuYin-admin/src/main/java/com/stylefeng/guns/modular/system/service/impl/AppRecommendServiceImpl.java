package com.stylefeng.guns.modular.system.service.impl;

import com.stylefeng.guns.modular.system.model.AppRecommend;
import com.stylefeng.guns.modular.system.dao.AppRecommendMapper;
import com.stylefeng.guns.modular.system.service.IAppRecommendService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 推荐位置管理 服务实现类
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-12
 */
@Service
public class AppRecommendServiceImpl extends ServiceImpl<AppRecommendMapper, AppRecommend> implements IAppRecommendService {
    @Override
    public List<Map<String, Object>> getAppRecommend(Map map) {
        return this.baseMapper.getAppRecommend(map);
    }
}
