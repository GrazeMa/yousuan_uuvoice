package com.stylefeng.guns.modular.system.dao;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.system.model.AppGift;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 礼物管理 Mapper 接口
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-01
 */
public interface AppGiftMapper extends BaseMapper<AppGift> {
    /**
     * 获取礼物名称，下拉列表获取数据
     * @param map
     * @return
     */
    List<Map<String,Object>> getAppGiftName(Map<String,Object> map);

    /**
     *获取礼物列表数据
     * @param appGift
     * @return
     */
    List<Map<String,Object>> getAppGiftList(AppGift appGift,Page<Map<String,Object>> page);

    /**
     * 获取定时任务数据（道具和礼物）
     * @return
     */
    List<Map<String,Object>> getSceneAndGift();
}
