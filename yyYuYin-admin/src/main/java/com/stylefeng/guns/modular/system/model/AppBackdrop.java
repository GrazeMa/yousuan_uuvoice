package com.stylefeng.guns.modular.system.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 背景图
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-07
 */
@TableName("app_backdrop")
public class AppBackdrop extends Model<AppBackdrop> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 背景图
     */
    private String img;
    /**
     * 上传时间
     */
    private Date createTime;
    /**
     * 背景图名称
     */
    private String name;
    /**
     * 顺序
     */
    private Integer sequence;
    /**
     * 历史使用次数
     */
    private Integer num;
    /**
     * 是否禁用  1否，2是
     */
    private Integer state;
    /**
     * '是否被删除（1=否  2=是）'
     */
    private Integer isDelete;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getSequence() {
        return sequence;
    }

    public void setSequence(Integer sequence) {
        this.sequence = sequence;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "AppBackdrop{" +
        "id=" + id +
        ", img=" + img +
        ", createTime=" + createTime +
        ", name=" + name +
        ", sequence=" + sequence +
        ", num=" + num +
        ", state=" + state +
        ", isDelete=" + isDelete +
        "}";
    }
}
