package com.stylefeng.guns.modular.system.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 靓号绑定
 * </p>
 *
 * @author stylefeng123
 * @since 2019-07-09
 */
@TableName("app_hall")
public class AppHall extends Model<AppHall> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 靓号
     */
    private String liang;
    /**
     * 用户的唯一标识id
     */
    private String uecding;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLiang() {
        return liang;
    }

    public void setLiang(String liang) {
        this.liang = liang;
    }

    public String getUecding() {
        return uecding;
    }

    public void setUecding(String uecding) {
        this.uecding = uecding;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "AppHall{" +
        "id=" + id +
        ", liang=" + liang +
        ", uecding=" + uecding +
        "}";
    }
}
