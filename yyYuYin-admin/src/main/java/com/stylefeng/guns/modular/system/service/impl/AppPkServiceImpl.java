package com.stylefeng.guns.modular.system.service.impl;

import com.stylefeng.guns.modular.system.model.AppPk;
import com.stylefeng.guns.modular.system.dao.AppPkMapper;
import com.stylefeng.guns.modular.system.service.IAppPkService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 房间pk
 服务实现类
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-08
 */
@Service
public class AppPkServiceImpl extends ServiceImpl<AppPkMapper, AppPk> implements IAppPkService {
    @Override
    public List<Map<String, Object>> getAppPkList(AppPk appPk) {
        return this.baseMapper.getAppPkList(appPk);
    }
}
