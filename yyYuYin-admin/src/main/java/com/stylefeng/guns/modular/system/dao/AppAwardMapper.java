package com.stylefeng.guns.modular.system.dao;

import com.stylefeng.guns.modular.system.model.AppAward;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 优钻兑换优币奖励 Mapper 接口
 * </p>
 *
 * @author stylefeng123
 * @since 2019-02-28
 */
public interface AppAwardMapper extends BaseMapper<AppAward> {

}
