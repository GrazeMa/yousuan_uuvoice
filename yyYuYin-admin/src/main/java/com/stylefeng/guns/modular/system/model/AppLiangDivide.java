package com.stylefeng.guns.modular.system.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 设置的一些关于分成的参数
 * </p>
 *
 * @author stylefeng123
 * @since 2019-07-09
 */
@TableName("app_liang_divide")
public class AppLiangDivide extends Model<AppLiangDivide> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 厅主分成
     */
    private Double liang;
    private String mark;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getLiang() {
        return liang;
    }

    public void setLiang(Double liang) {
        this.liang = liang;
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "AppLiangDivide{" +
        "id=" + id +
        ", liang=" + liang +
        ", mark=" + mark +
        "}";
    }
}
