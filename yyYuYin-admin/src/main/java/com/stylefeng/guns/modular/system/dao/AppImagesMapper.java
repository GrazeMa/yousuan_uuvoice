package com.stylefeng.guns.modular.system.dao;

import com.stylefeng.guns.modular.system.model.AppImages;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 图片 Mapper 接口
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-02
 */
public interface AppImagesMapper extends BaseMapper<AppImages> {

}
