package com.stylefeng.guns.modular.system.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.stylefeng.guns.modular.system.model.AppLiangDivide;

/**
 * <p>
 * 广告设置 Mapper 接口
 * </p>
 *
 * @author stylefeng123
 * @since 2018-06-28
 */
public interface AppLiangDivideMapper extends BaseMapper<AppLiangDivide> {

}
