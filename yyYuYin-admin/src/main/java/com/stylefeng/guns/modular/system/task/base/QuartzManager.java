package com.stylefeng.guns.modular.system.task.base;

import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;

import java.util.Date;
import java.util.Map;

public class QuartzManager {
	private static SchedulerFactory factory = new StdSchedulerFactory();
	private static final String TRIGGER_NAME_PREFIX = "TRIGGER_PREFIX_";
	private static final String JOB_NAME_PREFIX = "JOB_PREFIX_";
	
	/**
	 * 添加定时任务：具体某个时间点执行一次的任务，如：在某个2015-06-01 12:00发送一条消息
	 * 
	 * @param class1
	 * @param jobName
	 *            具体的任务名+ID标识唯一
	 * @param jobType
	 * @param date
	 * @param jp
	 */
	public synchronized static void addJob(Class<? extends Job> jobClass, String jobName, TimeJobType jobType, Date date,
                                           Map<String,  ? extends Object> jp) {
		//logger.debug("ADD JOB {},jobName={},jobTyep={},jobDate={},",jobClass.getName(),jobName,jobType,date);
		try {
			Scheduler sched = factory.getScheduler();
			JobDetail job = JobBuilder.newJob(jobClass).withIdentity(JOB_NAME_PREFIX + jobName, jobType.getType())
					.setJobData(new JobDataMap(jp)).build();

			SimpleTrigger trigger = (SimpleTrigger) TriggerBuilder.newTrigger()
					.withIdentity(TRIGGER_NAME_PREFIX + jobName, jobType.getType()).startAt(date).build();
			removeJob(jobName, jobType);
			
			sched.scheduleJob(job, trigger);

			if (!sched.isShutdown()) {
				sched.start();
			}
		} catch (Exception e) {
			//logger.error("ADD JOB exception {},jobName={},jobTyep={},jobDate={},",jobClass.getName(),jobName,jobType,date);
		}
	}
	
	   /**
	    * 修改一个任务的触发时间(使用默认的任务组名，触发器名，触发器组名) 
	    * @param jobName 
	    * @param time 
	    */  
	    public synchronized static void modifyJobTime(String jobName, TimeJobType jobType, Date time) {
	    	 //logger.error("Update JOB exception,jobName={},jobTyep={},jobDate={}," ,jobName,jobType,time);
	        try {  
	        	JobKey jobKey = new JobKey(JOB_NAME_PREFIX + jobName, jobType.getType());
				TriggerKey key = new TriggerKey(TRIGGER_NAME_PREFIX + jobName, jobType.getType());
				
				
	            Scheduler sched = factory.getScheduler();  
	            SimpleTrigger trigger = (SimpleTrigger) sched.getTrigger(key);  
	            if(trigger == null) {  
	                return;  
	            }  
	            Date oldTime = trigger.getStartTime();
	          
	            if (oldTime.getTime() != time.getTime()) {  
	                JobDetail jobDetail = sched.getJobDetail(jobKey);  
	                Class<? extends Job> objJobClass = jobDetail.getJobClass();  
	                removeJob(jobName,jobType);  
	                Map<String, Object> jp = jobDetail.getJobDataMap();
	                addJob(objJobClass, jobName, jobType, time, jp); 
	            }  
	        } catch (Exception e) {  
	           // logger.error("Update JOB exception,jobName={},jobTyep={},jobDate={}," ,jobName,jobType,time);
	        }  
	    }

	/**
	 * 移除一个任务
	 * 
	 * @param jobName
	 * @param jobGroupName
	 * @param triggerName
	 * @param triggerGroupName
	 */
	public synchronized static void removeJob(String jobName, TimeJobType jobType) {
		try {
			JobKey jobKey = new JobKey(JOB_NAME_PREFIX + jobName, jobType.getType());
			TriggerKey key = new TriggerKey(TRIGGER_NAME_PREFIX + jobName, jobType.getType());
			Scheduler sched = factory.getScheduler();

			JobDetail detail = sched.getJobDetail(jobKey);
			if (detail != null) {
				sched.pauseJob(jobKey);
				sched.pauseTrigger(key);// 停止触发器
				sched.unscheduleJob(key);// 移除触发器
				sched.deleteJob(jobKey);// 删除任务
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	/**
	 * 启动所有定时任务
	 */
	public synchronized static void startJobs() {
		try {
			Scheduler sched = factory.getScheduler();
			sched.start();
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	/**
	 * 关闭所有定时任务
	 */
	public synchronized static void shutdownJobs() {
		try {
			Scheduler sched = factory.getScheduler();
			if (!sched.isShutdown()) {
				sched.shutdown();
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

}
