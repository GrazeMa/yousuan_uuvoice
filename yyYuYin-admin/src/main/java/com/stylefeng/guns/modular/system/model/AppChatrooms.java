package com.stylefeng.guns.modular.system.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 加入聊天室的
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-07
 */
@TableName("app_chatrooms")
public class AppChatrooms extends Model<AppChatrooms> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    private Integer uid;
    /**
     * 房间ID
     */
    private String pid;
    /**
     * 类型 1是房主，2 是管理员，3用户 4 协议用户
     */
    private Integer type;
    /**
     * 是否上麦 1否，2 是
     */
    private Integer state;
    /**
     * 上麦顺序
     */
    private Integer sequence;
    /**
     * 是否在房间 1在，2 不再
     */
    private Integer status;

    private Date createTime;
    /**
     * 是否是协议号，1否，2是
     */
    private Integer isAgreement;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Integer getSequence() {
        return sequence;
    }

    public void setSequence(Integer sequence) {
        this.sequence = sequence;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getIsAgreement() {
        return isAgreement;
    }

    public void setIsAgreement(Integer isAgreement) {
        this.isAgreement = isAgreement;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "AppChatrooms{" +
        "id=" + id +
        ", uid=" + uid +
        ", pid=" + pid +
        ", type=" + type +
        ", state=" + state +
        ", sequence=" + sequence +
        ", status=" + status +
        "}";
    }
}
