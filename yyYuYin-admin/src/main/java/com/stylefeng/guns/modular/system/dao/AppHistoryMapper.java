package com.stylefeng.guns.modular.system.dao;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.system.model.AppHistory;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 推荐栏位历史 Mapper 接口
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-12
 */
public interface AppHistoryMapper extends BaseMapper<AppHistory> {
    /**
     * 获取推荐记录数据
     * @param appHistory
     * @return
     */
    List<Map<String,Object>> getAppHistory(AppHistory appHistory,Page<Map<String,Object>> page);
}
