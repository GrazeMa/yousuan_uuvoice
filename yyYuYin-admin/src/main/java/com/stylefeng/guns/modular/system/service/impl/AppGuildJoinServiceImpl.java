package com.stylefeng.guns.modular.system.service.impl;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.system.model.AppGuild;
import com.stylefeng.guns.modular.system.dao.AppGuildJoinMapper;
import com.stylefeng.guns.modular.system.service.IAppGuildJoinService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
/**
 * @Description 入会申请实现类。
 * @author Grazer_Ma
 * @Date 2020-05-14 22:25:35
 */
@Service
public class AppGuildJoinServiceImpl extends ServiceImpl<AppGuildJoinMapper, AppGuild> implements IAppGuildJoinService {
    @Override
    public List<Map<String, Object>> getAppGuildJoin(AppGuild appGuild, Page<Map<String,Object>> page) {
        return this.baseMapper.getAppGuildJoin(appGuild, page);
    }
}
