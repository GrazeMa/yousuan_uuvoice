package com.stylefeng.guns.modular.system.dao;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.system.model.AppHall;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.stylefeng.guns.modular.system.model.AppUser;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 靓号绑定 Mapper 接口
 * </p>
 *
 * @author stylefeng123
 * @since 2019-07-09
 */
public interface AppHallMapper extends BaseMapper<AppHall> {
    /**
     * 靓号绑定用户（排除已经绑定了的用户）
     * @param appUser
     * @param page
     * @return
     */
    List<Map<String,Object>> getAppUserListExHall(AppUser appUser, Page page);
}
