package com.stylefeng.guns.modular.system.dao;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.system.model.AppBroad;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 广播交友 Mapper 接口
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-12
 */
public interface AppBroadMapper extends BaseMapper<AppBroad> {
    /**
     *  广播交友
     * @param appBroad
     * @param page
     * @return
     */
    List<Map<String,Object>> getAppBroad(AppBroad appBroad, Page page);
}
