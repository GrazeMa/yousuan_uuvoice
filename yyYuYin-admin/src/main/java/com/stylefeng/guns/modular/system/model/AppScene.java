package com.stylefeng.guns.modular.system.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 道具管理
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-01
 */
@TableName("app_scene")
public class AppScene extends Model<AppScene> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 礼物名字
     */
    private String name;
    /**
     * 礼物图片
     */
    private String img;
    /**
     * 需要优币
     */
    private Integer gold;
    /**
     * l礼物封面
     */
    private String imgFm;
    private Date createTime;
    /**
     * 1座驾，2 是头环
     */
    private Integer state;
    /**
     * 天数
     */
    private Integer day;
    /**
     * 排序
     */
    private Integer sequence;
    /**
     * 1 销售 ,2 下架
     */
    private Integer isState;
    /**
     * 1 否,2 是
     */
    private Integer isDelete;
    /**
     * 销量
     */
    private Integer volume;
    /**
     * 上架时间
     */
    private Date addTime;

    /**
     * 时间查询条件(开始)
     */
    @TableField(exist = false)
    private String beginTime;
    /**
     * 时间查询条件(结束)
     */
    @TableField(exist = false)
    private String endTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public Integer getGold() {
        return gold;
    }

    public void setGold(Integer gold) {
        this.gold = gold;
    }

    public String getImgFm() {
        return imgFm;
    }

    public void setImgFm(String imgFm) {
        this.imgFm = imgFm;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Integer getDay() {
        return day;
    }

    public void setDay(Integer day) {
        this.day = day;
    }

    public Integer getSequence() {
        return sequence;
    }

    public void setSequence(Integer sequence) {
        this.sequence = sequence;
    }

    public Integer getIsState() {
        return isState;
    }

    public void setIsState(Integer isState) {
        this.isState = isState;
    }

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    public Integer getVolume() {
        return volume;
    }

    public void setVolume(Integer volume) {
        this.volume = volume;
    }

    public Date getAddTime() {
        return addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    public String getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(String beginTime) {
        this.beginTime = beginTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "AppScene{" +
        "id=" + id +
        ", name=" + name +
        ", img=" + img +
        ", gold=" + gold +
        ", imgFm=" + imgFm +
        ", createTime=" + createTime +
        ", state=" + state +
        ", day=" + day +
        ", sequence=" + sequence +
        ", isState=" + isState +
        ", isDelete=" + isDelete +
        ", volume=" + volume +
        "}";
    }
}
