package com.stylefeng.guns.modular.system.dao;

import com.stylefeng.guns.modular.system.model.AppSetShare;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 分享赠送 Mapper 接口
 * </p>
 *
 * @author stylefeng123
 * @since 2019-02-28
 */
public interface AppSetShareMapper extends BaseMapper<AppSetShare> {

}
