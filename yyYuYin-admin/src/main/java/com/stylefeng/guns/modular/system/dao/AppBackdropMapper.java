package com.stylefeng.guns.modular.system.dao;

import com.stylefeng.guns.modular.system.model.AppBackdrop;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 背景图 Mapper 接口
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-07
 */
public interface AppBackdropMapper extends BaseMapper<AppBackdrop> {

}
