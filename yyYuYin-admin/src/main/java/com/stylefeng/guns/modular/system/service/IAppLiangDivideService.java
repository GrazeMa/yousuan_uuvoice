package com.stylefeng.guns.modular.system.service;

import com.stylefeng.guns.modular.system.model.AppLiangDivide;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 设置的一些关于分成的参数 服务类
 * </p>
 *
 * @author stylefeng123
 * @since 2019-07-09
 */
public interface IAppLiangDivideService extends IService<AppLiangDivide> {

}
