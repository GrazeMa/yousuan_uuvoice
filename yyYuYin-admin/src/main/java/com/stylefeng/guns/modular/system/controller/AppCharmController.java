package com.stylefeng.guns.modular.system.controller;

import com.stylefeng.guns.core.base.controller.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;
import com.stylefeng.guns.modular.system.model.AppCharm;
import com.stylefeng.guns.modular.system.service.IAppCharmService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 房间魅力榜控制器
 * @Date 2019-03-13 09:35:30
 */
@Controller
@RequestMapping("/appCharm")
public class AppCharmController extends BaseController {

    private String PREFIX = "/system/appCharm/";

    @Autowired
    private IAppCharmService appCharmService;

    /**
     * 跳转到房间魅力榜首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "appCharm.html";
    }

    /**
     * 跳转到添加房间魅力榜
     */
    @RequestMapping("/appCharm_add")
    public String appCharmAdd() {
        return PREFIX + "appCharm_add.html";
    }

    /**
     * 跳转到修改房间魅力榜
     */
    @RequestMapping("/appCharm_update/{appCharmId}")
    public String appCharmUpdate(@PathVariable Integer appCharmId, Model model) {
        AppCharm appCharm = appCharmService.selectById(appCharmId);
        model.addAttribute("item",appCharm);
        LogObjectHolder.me().set(appCharm);
        return PREFIX + "appCharm_edit.html";
    }

    /**
     * 获取房间魅力榜列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String usercoding,String nickname,Integer day) {
        if(day==null){
            day=3;
        }
        Map<String,Object> paMap=new HashMap<>();
        paMap.put("day",day);
        paMap.put("usercoding",usercoding);
        paMap.put("nickname",nickname);
        List<Map<String,Object>> reList=appCharmService.getAppCharm(paMap);
        for(int i=0;i<reList.size();i++){
            Map<String,Object> map=new HashMap<>();
            if(i>0){
                Integer num=Integer.valueOf(reList.get(i-1).get("value_").toString())-Integer.valueOf(reList.get(i).get("value_").toString());
                reList.get(i).put("num",num);
            }
        }
        return reList;
    }

    /**
     * 新增房间魅力榜
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(AppCharm appCharm) {
        appCharmService.insert(appCharm);
        return SUCCESS_TIP;
    }

    /**
     * 删除房间魅力榜
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer appCharmId) {
        appCharmService.deleteById(appCharmId);
        return SUCCESS_TIP;
    }

    /**
     * 修改房间魅力榜
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(AppCharm appCharm) {
        appCharmService.updateById(appCharm);
        return SUCCESS_TIP;
    }

    /**
     * 房间魅力榜详情
     */
    @RequestMapping(value = "/detail/{appCharmId}")
    @ResponseBody
    public Object detail(@PathVariable("appCharmId") Integer appCharmId) {
        return appCharmService.selectById(appCharmId);
    }
}
