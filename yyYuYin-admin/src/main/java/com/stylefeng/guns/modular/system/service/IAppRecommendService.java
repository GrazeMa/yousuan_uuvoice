package com.stylefeng.guns.modular.system.service;

import com.stylefeng.guns.modular.system.model.AppRecommend;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 推荐位置管理 服务类
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-12
 */
public interface IAppRecommendService extends IService<AppRecommend> {
    /**
     * 获取推荐管理数据
     * @param map
     * @return
     */
    List<Map<String,Object>> getAppRecommend(Map map);
}
