package com.stylefeng.guns.modular.system.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 房间信息
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-07
 */
@TableName("app_room")
public class AppRoom extends Model<AppRoom> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    private Date createTime;
    /**
     * 房间名称
     */
    private String roomName;
    private Integer uid;
    /**
     * 房间id
     */
    private String rid;
    /**
     * 房主昵称
     */
    private String name;
    /**
     * 在线人数
     */
    private Integer lineNum;
    /**
     * 房间密码
     */
    private String password;
    /**
     * 房间标签
     */
    private String roomLabel;
    /**
     * 房间话题内容
     */
    private String roomCount;
    /**
     * 房间话题
     */
    private String roomTopic;
    /**
     * 房间提示
     */
    private String roomHint;
    /**
     * 协议号数
     */
    private Integer protocolNum;
    /**
     * 是否牌子房间 1否，2是
     */
    private Integer state;
    /**
     * 是否有效 1有效，2无效
     */
    private Integer status;
    /**
     * 顺序
     */
    private Integer sequence;
    /**
     * 1是热门，2是女神，3是男神，4娱乐，5听歌，6相亲，7电台
     */
    private Integer type;
    /**
     * 是否被删除（1=否  2=是）
     */
    private Integer isDelete;
    /**
     * 背景图片
     */
    private String bjImg;
    /**
     * 是否开启屏蔽低礼物特效 1否， 2是
     */
    private Integer isState;
    /**
     * 是否开启pk 1是未，2 是开启
     */
    private Integer isPk;
    /**
     * 是否开启竞拍 1是未，2 是开启
     */
    private Integer isJp;
    /**
     * 1  开启， 2 关闭
     */
    private Integer isGp;
    /**
     * 是否推荐（1否，2是）
     */
    private Integer isRecommend;
    /**
     * 房主是否在房间 1 在， 2不在
     */
    private Integer isfz;
    /**
     * 修改时间
     */
    private Date updateTime;
    /**
     * 靓号
     */
    private String liang;
    /**
     * 公会主键ID。
     */
    private String guildId;
    @TableField(exist = false)
    private Integer price;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public String getRid() {
        return rid;
    }

    public void setRid(String rid) {
        this.rid = rid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getLineNum() {
        return lineNum;
    }

    public void setLineNum(Integer lineNum) {
        this.lineNum = lineNum;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRoomLabel() {
        return roomLabel;
    }

    public void setRoomLabel(String roomLabel) {
        this.roomLabel = roomLabel;
    }

    public String getRoomCount() {
        return roomCount;
    }

    public void setRoomCount(String roomCount) {
        this.roomCount = roomCount;
    }

    public String getRoomTopic() {
        return roomTopic;
    }

    public void setRoomTopic(String roomTopic) {
        this.roomTopic = roomTopic;
    }

    public String getRoomHint() {
        return roomHint;
    }

    public void setRoomHint(String roomHint) {
        this.roomHint = roomHint;
    }

    public Integer getProtocolNum() {
        return protocolNum;
    }

    public void setProtocolNum(Integer protocolNum) {
        this.protocolNum = protocolNum;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getSequence() {
        return sequence;
    }

    public void setSequence(Integer sequence) {
        this.sequence = sequence;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    public String getBjImg() {
        return bjImg;
    }

    public void setBjImg(String bjImg) {
        this.bjImg = bjImg;
    }

    public Integer getIsState() {
        return isState;
    }

    public void setIsState(Integer isState) {
        this.isState = isState;
    }

    public Integer getIsPk() {
        return isPk;
    }

    public void setIsPk(Integer isPk) {
        this.isPk = isPk;
    }

    public Integer getIsJp() {
        return isJp;
    }

    public void setIsJp(Integer isJp) {
        this.isJp = isJp;
    }

    public Integer getIsGp() {
        return isGp;
    }

    public void setIsGp(Integer isGp) {
        this.isGp = isGp;
    }

    public Integer getIsRecommend() {
        return isRecommend;
    }

    public void setIsRecommend(Integer isRecommend) {
        this.isRecommend = isRecommend;
    }

    public Integer getIsfz() {
        return isfz;
    }

    public void setIsfz(Integer isfz) {
        this.isfz = isfz;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getLiang() {
        return liang;
    }

    public void setLiang(String liang) {
        this.liang = liang;
    }

    public String getGuildId() {
        return guildId;
    }

    public void setGuildId(String guildId) {
        this.guildId = guildId;
    }
    
    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "AppRoom{" +
        "id=" + id +
        ", createTime=" + createTime +
        ", roomName=" + roomName +
        ", uid=" + uid +
        ", rid=" + rid +
        ", name=" + name +
        ", lineNum=" + lineNum +
        ", password=" + password +
        ", roomLabel=" + roomLabel +
        ", roomCount=" + roomCount +
        ", roomTopic=" + roomTopic +
        ", roomHint=" + roomHint +
        ", protocolNum=" + protocolNum +
        ", state=" + state +
        ", status=" + status +
        ", sequence=" + sequence +
        ", type=" + type +
        ", isDelete=" + isDelete +
        ", bjImg=" + bjImg +
        ", isState=" + isState +
        ", isPk=" + isPk +
        ", isJp=" + isJp +
        ", isGp=" + isGp +
        ", guildId=" + guildId +
        ", price=" + price +
        "}";
    }
}
