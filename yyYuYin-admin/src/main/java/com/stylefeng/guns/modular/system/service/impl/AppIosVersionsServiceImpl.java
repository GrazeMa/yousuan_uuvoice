package com.stylefeng.guns.modular.system.service.impl;

import com.stylefeng.guns.modular.system.model.AppIosVersions;
import com.stylefeng.guns.modular.system.dao.AppIosVersionsMapper;
import com.stylefeng.guns.modular.system.service.IAppIosVersionsService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * ios版本控制 服务实现类
 * </p>
 *
 * @author stylefeng123
 * @since 2019-05-21
 */
@Service
public class AppIosVersionsServiceImpl extends ServiceImpl<AppIosVersionsMapper, AppIosVersions> implements IAppIosVersionsService {

}
