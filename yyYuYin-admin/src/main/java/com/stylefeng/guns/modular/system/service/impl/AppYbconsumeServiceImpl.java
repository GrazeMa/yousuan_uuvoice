package com.stylefeng.guns.modular.system.service.impl;

import com.stylefeng.guns.modular.system.model.AppRoom;
import com.stylefeng.guns.modular.system.model.AppYbconsume;
import com.stylefeng.guns.modular.system.dao.AppYbconsumeMapper;
import com.stylefeng.guns.modular.system.service.IAppYbconsumeService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 优币消费记录 服务实现类
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-14
 */
@Service
public class AppYbconsumeServiceImpl extends ServiceImpl<AppYbconsumeMapper, AppYbconsume> implements IAppYbconsumeService {
    @Override
    public List<Map<String, Object>> getAppYbconsume(AppRoom appRoom) {
        return this.baseMapper.getAppYbconsume(appRoom);
    }
}
