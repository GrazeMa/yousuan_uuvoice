package com.stylefeng.guns.modular.system.service.impl;

import com.stylefeng.guns.modular.system.dao.AppLiangDivideMapper;
import com.stylefeng.guns.modular.system.model.AppLiangDivide;
import com.stylefeng.guns.modular.system.service.IAppLiangDivideService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 设置的一些关于分成的参数 服务实现类
 * </p>
 *
 * @author stylefeng123
 * @since 2019-07-09
 */
@Service
public class AppLiangDivideServiceImpl extends ServiceImpl<AppLiangDivideMapper, AppLiangDivide> implements IAppLiangDivideService {

}

