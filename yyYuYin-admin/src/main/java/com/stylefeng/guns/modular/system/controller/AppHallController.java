package com.stylefeng.guns.modular.system.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.common.constant.factory.PageFactory;
import com.stylefeng.guns.core.util.SinataUtil;
import com.stylefeng.guns.modular.system.model.AppRoom;
import com.stylefeng.guns.modular.system.model.AppUser;
import com.stylefeng.guns.modular.system.service.IAppRoomService;
import com.stylefeng.guns.modular.system.service.IAppUserService;
import com.stylefeng.guns.modular.system.warpper.AppUserWarpper;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;
import com.stylefeng.guns.modular.system.model.AppHall;
import com.stylefeng.guns.modular.system.service.IAppHallService;

import java.util.List;
import java.util.Map;

/**
 * 靓号控制器
 * @Date 2019-07-09 10:09:32
 */
@Controller
@RequestMapping("/appHall")
public class AppHallController extends BaseController {

    private String PREFIX = "/system/appHall/";

    @Autowired
    private IAppHallService appHallService;
    @Autowired
    private IAppUserService appUserService;
    @Autowired
    private IAppRoomService appRoomService;

    /**
     * 跳转到靓号首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "appHall.html";
    }

    /**
     * 跳转到靓号绑定用户（选择用户）页面
     */
    @RequestMapping("/appHall_userList")
    public String appHall_userList() {
        return PREFIX + "appHall_userList.html";
    }

    /**
     * 跳转到添加靓号
     */
    @RequestMapping("/appHall_add")
    public String appHallAdd() {
        return PREFIX + "appHall_add.html";
    }

    /**
     * 绑定靓号
     */
    @RequestMapping("/appHall_update/{appHallId}")
    public String appHallUpdate(@PathVariable Integer appHallId, Model model) {
        AppHall appHall = appHallService.selectById(appHallId);
        model.addAttribute("item",appHall);
        LogObjectHolder.me().set(appHall);
        return PREFIX + "appHall_edit.html";
    }

    /**
     * 获取靓号列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(AppHall appHall) {
        Wrapper appHallEntityWrapper=new EntityWrapper<AppHall>();
        /*当前房间号*/
        if(SinataUtil.isNotEmpty(appHall.getLiang())){
            appHallEntityWrapper.eq("liang",appHall.getLiang());
        }
        if(SinataUtil.isNotEmpty(appHall.getUecding())){
            appHallEntityWrapper.eq("uecding",appHall.getUecding());
        }
        Page<AppHall> page = new PageFactory<AppHall>().defaultPage();
        appHallEntityWrapper.orderBy("id",false);
        page.setRecords(appHallService.selectMapsPage(page, appHallEntityWrapper).getRecords());
        return super.packForBT(page);
    }


    /**
     * 获取用户管理列表(锁定用户不能添加)
     */
    @RequestMapping(value = "/userList")
    @ResponseBody
    public Object userList(AppUser appUser) {
        Page<Map<String,Object>> page = new PageFactory<Map<String,Object>>().defaultPage();
        List<Map<String, Object>> result = appHallService.getAppUserListExHall(appUser,page );
        page.setRecords((List<Map<String,Object>>)new AppUserWarpper(result).warp());
        return super.packForBT(page);
    }

    /**
     * 新增靓号
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(AppHall appHall) {
        /*判断不能重复*/
        Integer count=appHallService.selectCount(new EntityWrapper<AppHall>().eq("liang",appHall.getLiang()));
        if(count>0){
            return "1";
        }
        appHallService.insert(appHall);
        return SUCCESS_TIP;
    }

    /**
     * 删除靓号
     */
    @RequestMapping(value = "/unbind")
    @ResponseBody
    @Transactional
    public Object unbind(@RequestParam Integer appHallId) {
        AppHall appHall=appHallService.selectById(appHallId);
        if(appHall!=null){
            AppUser appUser=appUserService.selectOne(new EntityWrapper<AppUser>().eq("usercoding",appHall.getUecding()));
            /*删除用户表里的靓号*/
            appUser.setLiang(null);
            appUserService.updateAllColumnById(appUser);
            /*删除房间表的靓号*/
            AppRoom appRoom=appRoomService.selectOne(new EntityWrapper<AppRoom>().eq("uid",appUser.getId()));
            appRoom.setLiang(null);
            appRoomService.updateAllColumnById(appRoom);
        }
        appHall.setUecding(null);
        appHallService.updateAllColumnById(appHall);
        return SUCCESS_TIP;
    }

    /**
     * 删除靓号
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    @Transactional
    public Object delete(@RequestParam Integer appHallId) {
        appHallService.deleteById(appHallId);
        return SUCCESS_TIP;
    }

    /**
     * 修改靓号（绑定用户）
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    @Transactional
    public Object update(AppHall appHall) {
        /*根据优优id查找用户信息*/
        AppUser appUser=appUserService.selectOne(new EntityWrapper<AppUser>().eq("usercoding",appHall.getUecding()));
        if(appUser!=null){
            /*用户表需加一个靓号字段*/
            appUser.setLiang(appHall.getLiang());
            appUserService.updateById(appUser);
            /*房间表里需要加一个靓号字段*/
            AppRoom appRoom=new AppRoom();
            appRoom.setLiang(appHall.getLiang());
            appRoomService.update(appRoom,new EntityWrapper<AppRoom>().eq("uid",appUser.getId()));
        }
        /*修改靓号关系表中的数据*/
        appHallService.updateById(appHall);
        return SUCCESS_TIP;
    }

    /**
     * 靓号详情
     */
    @RequestMapping(value = "/detail/{appHallId}")
    @ResponseBody
    public Object detail(@PathVariable("appHallId") Integer appHallId) {
        return appHallService.selectById(appHallId);
    }
}
