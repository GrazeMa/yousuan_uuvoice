package com.stylefeng.guns.modular.system.service.impl;

import com.stylefeng.guns.modular.system.model.AppRoom;
import com.stylefeng.guns.modular.system.model.AppYbconsume;
import com.stylefeng.guns.modular.system.dao.AppRoomTurnoverMapper;
import com.stylefeng.guns.modular.system.service.IAppRoomTurnoverService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @Description 聊天室收益服务实现类。
 * @author Grazer_Ma
 * @Date 2020-05-15 10:00:46
 */
@Service
public class AppRoomTurnoverServiceImpl extends ServiceImpl<AppRoomTurnoverMapper, AppYbconsume> implements IAppRoomTurnoverService {
    @Override
    public List<Map<String, Object>> getAppRoomTurnover(AppRoom appRoom) {
        return this.baseMapper.getAppRoomTurnover(appRoom);
    }
}
