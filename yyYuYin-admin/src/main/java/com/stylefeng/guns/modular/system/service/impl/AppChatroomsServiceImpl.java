package com.stylefeng.guns.modular.system.service.impl;

import com.stylefeng.guns.modular.system.model.AppChatrooms;
import com.stylefeng.guns.modular.system.dao.AppChatroomsMapper;
import com.stylefeng.guns.modular.system.service.IAppChatroomsService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 加入聊天室的 服务实现类
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-07
 */
@Service
public class AppChatroomsServiceImpl extends ServiceImpl<AppChatroomsMapper, AppChatrooms> implements IAppChatroomsService {
    @Override
    public List<Map<String, Object>> getAppChatrooms(Map<String, Object> objectMap) {
        return this.baseMapper.getAppChatrooms(objectMap);
    }
}
