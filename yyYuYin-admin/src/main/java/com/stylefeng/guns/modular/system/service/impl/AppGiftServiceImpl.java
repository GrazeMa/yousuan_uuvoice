package com.stylefeng.guns.modular.system.service.impl;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.system.model.AppGift;
import com.stylefeng.guns.modular.system.dao.AppGiftMapper;
import com.stylefeng.guns.modular.system.service.IAppGiftService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 礼物管理 服务实现类
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-01
 */
@Service
public class AppGiftServiceImpl extends ServiceImpl<AppGiftMapper, AppGift> implements IAppGiftService {
    @Override
    public List<Map<String, Object>> getAppGiftName(Map<String, Object> map) {
        return this.baseMapper.getAppGiftName(map);
    }

    @Override
    public List<Map<String, Object>> getAppGiftList(AppGift appGift,Page<Map<String,Object>> page) {
        return this.baseMapper.getAppGiftList(appGift,page);
    }

    @Override
    public List<Map<String, Object>> getSceneAndGift() {
        return this.baseMapper.getSceneAndGift();
    }
}
