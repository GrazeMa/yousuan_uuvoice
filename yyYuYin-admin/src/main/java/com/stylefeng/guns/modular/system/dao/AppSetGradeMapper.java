package com.stylefeng.guns.modular.system.dao;

import com.stylefeng.guns.modular.system.model.AppSetGrade;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 等级设置 Mapper 接口
 * </p>
 *
 * @author stylefeng123
 * @since 2019-02-28
 */
public interface AppSetGradeMapper extends BaseMapper<AppSetGrade> {

}
