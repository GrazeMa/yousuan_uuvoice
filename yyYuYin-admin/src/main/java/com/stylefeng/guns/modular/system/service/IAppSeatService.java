package com.stylefeng.guns.modular.system.service;

import com.stylefeng.guns.modular.system.model.AppSeat;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 房间座位相关设置 服务类
 * </p>
 *
 * @author stylefeng123
 * @since 2019-04-03
 */
public interface IAppSeatService extends IService<AppSeat> {

}
