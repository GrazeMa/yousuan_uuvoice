package com.stylefeng.guns.modular.system.dao;

import com.stylefeng.guns.modular.system.model.AppAudit;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 实名认证 Mapper 接口
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-01
 */
public interface AppAuditMapper extends BaseMapper<AppAudit> {

}
