package com.stylefeng.guns.modular.system.warpper;

import com.stylefeng.guns.core.base.warpper.BaseControllerWarpper;
import com.stylefeng.guns.core.common.constant.factory.ConstantFactory;
import com.stylefeng.guns.core.util.SinataUtil;

import java.util.List;
import java.util.Map;

/**
 * 用户管理的包装类
 *
 * @author fengshuonan
 * @date 2017年2月13日 下午10:47:03
 */
public class AppUserWarpper extends BaseControllerWarpper {

    public AppUserWarpper(List<Map<String, Object>> list) {
        super(list);
    }

    @Override
    public void warpTheMap(Map<String, Object> map) {
        //用户性别
        String sexName="男";
        if(SinataUtil.isNotEmpty(map.get("sex")) && Integer.valueOf(map.get("sex").toString())==2){
            sexName="女";
        }
        map.put("sexName",sexName);
        //用户状态
        String stateName="正常";
        if(SinataUtil.isNotEmpty(map.get("state")) && Integer.valueOf(map.get("state").toString())==2){
            stateName="禁用";
        }
        map.put("stateName",stateName);
        //是否开通房间
        String statusName="否";
        if(SinataUtil.isNotEmpty(map.get("status")) && Integer.valueOf(map.get("status").toString())==2){
            statusName="是";
        }
        map.put("statusName",statusName);
    }

}
