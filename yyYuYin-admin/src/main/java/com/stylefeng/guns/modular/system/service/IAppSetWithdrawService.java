package com.stylefeng.guns.modular.system.service;

import com.stylefeng.guns.modular.system.model.AppSetWithdraw;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 提现设置 服务类
 * </p>
 *
 * @author stylefeng123
 * @since 2019-02-28
 */
public interface IAppSetWithdrawService extends IService<AppSetWithdraw> {

}
