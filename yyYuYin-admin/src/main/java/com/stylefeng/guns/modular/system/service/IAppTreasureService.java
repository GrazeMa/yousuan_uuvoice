package com.stylefeng.guns.modular.system.service;

import com.stylefeng.guns.modular.system.model.AppTreasure;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 房间财富值 服务类
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-11
 */
public interface IAppTreasureService extends IService<AppTreasure> {
    /**
     * 获取财富榜
     * @param map
     * @return
     */
    List<Map<String,Object>> getAppTreasure(Map<String,Object> map);
}
