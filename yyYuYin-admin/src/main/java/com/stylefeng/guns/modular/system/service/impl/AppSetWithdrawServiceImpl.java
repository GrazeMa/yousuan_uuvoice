package com.stylefeng.guns.modular.system.service.impl;

import com.stylefeng.guns.modular.system.model.AppSetWithdraw;
import com.stylefeng.guns.modular.system.dao.AppSetWithdrawMapper;
import com.stylefeng.guns.modular.system.service.IAppSetWithdrawService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 提现设置 服务实现类
 * </p>
 *
 * @author stylefeng123
 * @since 2019-02-28
 */
@Service
public class AppSetWithdrawServiceImpl extends ServiceImpl<AppSetWithdrawMapper, AppSetWithdraw> implements IAppSetWithdrawService {

}
