package com.stylefeng.guns.modular.system.service;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.system.model.AppGuildSettle;

/**
 * @Description 结算记录服务类。
 * @author Grazer_Ma
 * @Date 2020-05-20 20:35:51
 */
public interface IAppSettleTurnoverService extends IService<AppGuildSettle> {
	/**
	 * 获取结算记录。
	 * 
	 * @param appGuildSettle
	 * @return
	 */
	List<Map<String, Object>> getAppSettleTurnover(AppGuildSettle appGuildSettle, Page<Map<String, Object>> page);
}
