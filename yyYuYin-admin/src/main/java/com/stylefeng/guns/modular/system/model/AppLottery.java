package com.stylefeng.guns.modular.system.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 宝箱设置 （第一条数据保存到礼物设置里面）
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-11
 */
@TableName("app_lottery")
public class AppLottery extends Model<AppLottery> {

    private static final long serialVersionUID = 1L;

    private Integer id;
    /**
     * 礼物id
     */
    private Integer gid;
    /**
     * 剩余奖品数
     */
    private Integer num;
    /**
     * 中奖概率
     */
    private Double ratio;
    /**
     * xx等奖
     */
    private String name;
    /**
     * 1道具头环，2道具座驾，3礼物 ，4 谢谢惠顾
     */
    private Integer type;
    /**
     * 礼物名称
     */
    private String gName;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getGid() {
        return gid;
    }

    public void setGid(Integer gid) {
        this.gid = gid;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public Double getRatio() {
        return ratio;
    }

    public void setRatio(Double ratio) {
        this.ratio = ratio;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getgName() {
        return gName;
    }

    public void setgName(String gName) {
        this.gName = gName;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "AppLottery{" +
        "id=" + id +
        ", gid=" + gid +
        ", num=" + num +
        ", ratio=" + ratio +
        ", name=" + name +
        ", type=" + type +
        ", gName=" + gName +
        "}";
    }
}
