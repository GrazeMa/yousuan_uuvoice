package com.stylefeng.guns.modular.system.model;

public class UserModel {

	private Integer id;
	private String  name;
	private Integer sex;
	private String img;
	private Integer type;
	private Integer state;
	private Integer sequence;
	
	private String usercoding;
	
	 /**
     * 财富等级
     */
    private Integer treasureGrade;
    
    private Integer charmGrade;
    
    
	
	
	public Integer getTreasureGrade() {
		return treasureGrade;
	}
	public void setTreasureGrade(Integer treasureGrade) {
		this.treasureGrade = treasureGrade;
	}
	public Integer getCharmGrade() {
		return charmGrade;
	}
	public void setCharmGrade(Integer charmGrade) {
		this.charmGrade = charmGrade;
	}
	public String getUsercoding() {
		return usercoding;
	}
	public void setUsercoding(String usercoding) {
		this.usercoding = usercoding;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public Integer getState() {
		return state;
	}
	public void setState(Integer state) {
		this.state = state;
	}
	public Integer getSequence() {
		return sequence;
	}
	public void setSequence(Integer sequence) {
		this.sequence = sequence;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getSex() {
		return sex;
	}
	public void setSex(Integer sex) {
		this.sex = sex;
	}
	public String getImg() {
		return img;
	}
	public void setImg(String img) {
		this.img = img;
	}
	
}
