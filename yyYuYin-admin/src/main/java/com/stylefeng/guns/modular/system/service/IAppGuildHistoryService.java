package com.stylefeng.guns.modular.system.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.system.model.AppGuild;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;
import java.util.Map;

/**
 * @Description 入会与退会记录服务类。
 * @author Grazer_Ma
 * @Date 2020-05-14 21:51:12
 */
public interface IAppGuildHistoryService extends IService<AppGuild> {
    /**
     * 获取入会与退会记录信息
     * @param appGuild
     * @return
     */
    List<Map<String,Object>> getAppGuildHistory(AppGuild appGuild, Page<Map<String, Object>> page);
}
