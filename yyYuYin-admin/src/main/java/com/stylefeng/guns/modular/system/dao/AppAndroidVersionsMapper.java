package com.stylefeng.guns.modular.system.dao;

import com.stylefeng.guns.modular.system.model.AppAndroidVersions;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 安卓版本控制 Mapper 接口
 * </p>
 *
 * @author stylefeng123
 * @since 2019-05-21
 */
public interface AppAndroidVersionsMapper extends BaseMapper<AppAndroidVersions> {

}
