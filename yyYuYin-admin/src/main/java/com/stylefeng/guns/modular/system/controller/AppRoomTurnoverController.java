package com.stylefeng.guns.modular.system.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.core.shiro.ShiroKit;
import com.stylefeng.guns.core.util.SinataUtil;
import com.stylefeng.guns.modular.system.model.AppRoom;
import com.stylefeng.guns.modular.system.model.AppYbconsume;
import com.stylefeng.guns.modular.system.service.IAppRoomService;
import com.stylefeng.guns.modular.system.service.IAppRoomTurnoverService;

/**
 * @Description 聊天室收益控制器。
 * @author Grazer_Ma
 * @Date 2020-05-15 10:03:19
 */
@Controller
@RequestMapping("/appRoomTurnover")
public class AppRoomTurnoverController extends BaseController {

    private String PREFIX = "/system/appRoomTurnover/";

    @Autowired
    private IAppRoomTurnoverService appRoomTurnoverService;
    @Autowired
    private IAppRoomService iAppRoomService;

    /**
     * 跳转到优币消费首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "appRoomTurnover.html";
    }

    /**
     * 跳转到
     * @return
     */
    @RequestMapping("/room")
    public String indexRoom() {
        return PREFIX + "appRoomTurnoverRoom.html";
    }

    /**
     * 跳转到添加优币消费
     */
    @RequestMapping("/appRoomTurnover_add")
    public String appRoomTurnoverAdd() {
        return PREFIX + "appRoomTurnover_add.html";
    }

    /**
     * 跳转到修改优币消费
     */
    @RequestMapping("/appRoomTurnover_update/{appRoomTurnoverId}")
    public String appRoomTurnoverUpdate(@PathVariable Integer appRoomTurnoverId, Model model) {
        AppYbconsume appYbconsume = appRoomTurnoverService.selectById(appRoomTurnoverId);
        model.addAttribute("item",appYbconsume);
        LogObjectHolder.me().set(appYbconsume);
        return PREFIX + "appRoomTurnover_edit.html";
    }

	/**
	 * 获取收益统计列表。
	 */
	@RequestMapping(value = "/list")
	@ResponseBody
	public Object list(AppYbconsume appYbconsume, String beginTime, String endTime, String price) {

		EntityWrapper<AppRoom> roomEntityWrapper = new EntityWrapper<>();
		roomEntityWrapper.eq("guildId", ShiroKit.getUser().getAccount());
		
		List<AppRoom> guildLists = iAppRoomService.selectList(roomEntityWrapper);
		StringBuilder stringBuilder = new StringBuilder();
		if (SinataUtil.isNotEmpty(guildLists)) {
			System.out.println("guildLists: " + guildLists);
			for(int i = 0; i < guildLists.size(); i++) {
				stringBuilder.append(guildLists.get(i).getRid()).append(",");
			}
		}
		
		EntityWrapper<AppYbconsume> entityWrapper = new EntityWrapper<>();
		if (SinataUtil.isNotEmpty(appYbconsume.getEid())) {
			entityWrapper.eq("eid", appYbconsume.getEid());
		}
//		if (SinataUtil.isNotEmpty(appYbconsume.getRid())) {
//			entityWrapper.in("rid", stringBuilder.toString());
//			entityWrapper.eq("rid", appYbconsume.getRid());
//		}
		if (SinataUtil.isNotEmpty(appYbconsume.getName())) {
			entityWrapper.like("name", appYbconsume.getName());
		}
		if (SinataUtil.isNotEmpty(beginTime)) {
			entityWrapper.ge("createTime", beginTime + " 00:00:00");
		}
		if (SinataUtil.isNotEmpty(endTime)) {
			entityWrapper.le("createTime", endTime + " 23:59:59");
		}
		if (SinataUtil.isNotEmpty(price)) {
			entityWrapper.eq("consumeNum", 1);
			entityWrapper.ge("consumeYbNum", price);
		}
		entityWrapper.eq("isDelete", 1);
		entityWrapper.eq("state", 1);
		System.out.println("stringBuilder: " + stringBuilder);
		entityWrapper.in("rid", stringBuilder.toString());
		entityWrapper.orderBy("createTime", false);
		return appRoomTurnoverService.selectList(entityWrapper);
		
	}

    /**
     * 房间流水
     * @param appRoom
     * @return
     */
    @RequestMapping(value = "/room/list")
    @ResponseBody
    public Object roomList(AppRoom appRoom) {
        return appRoomTurnoverService.getAppRoomTurnover(appRoom);
    }


    /**
     * 新增优币消费
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(AppYbconsume appYbconsume) {
        appRoomTurnoverService.insert(appYbconsume);
        return SUCCESS_TIP;
    }

    /**
     * 删除优币消费
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer appRoomTurnoverId) {
        appRoomTurnoverService.deleteById(appRoomTurnoverId);
        return SUCCESS_TIP;
    }

    /**
     * 修改优币消费
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(AppYbconsume appYbconsume) {
        appRoomTurnoverService.updateById(appYbconsume);
        return SUCCESS_TIP;
    }

    /**
     * 优币消费详情
     */
    @RequestMapping(value = "/detail/{appRoomTurnoverId}")
    @ResponseBody
    public Object detail(@PathVariable("appRoomTurnoverId") Integer appRoomTurnoverId) {
        return appRoomTurnoverService.selectById(appRoomTurnoverId);
    }
    
}
