package com.stylefeng.guns.modular.system.service;

import com.stylefeng.guns.modular.system.model.AppIosVersions;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * ios版本控制 服务类
 * </p>
 *
 * @author stylefeng123
 * @since 2019-05-21
 */
public interface IAppIosVersionsService extends IService<AppIosVersions> {

}
