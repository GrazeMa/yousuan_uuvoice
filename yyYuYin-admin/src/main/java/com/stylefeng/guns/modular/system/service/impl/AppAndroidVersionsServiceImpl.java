package com.stylefeng.guns.modular.system.service.impl;

import com.stylefeng.guns.modular.system.model.AppAndroidVersions;
import com.stylefeng.guns.modular.system.dao.AppAndroidVersionsMapper;
import com.stylefeng.guns.modular.system.service.IAppAndroidVersionsService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 安卓版本控制 服务实现类
 * </p>
 *
 * @author stylefeng123
 * @since 2019-05-21
 */
@Service
public class AppAndroidVersionsServiceImpl extends ServiceImpl<AppAndroidVersionsMapper, AppAndroidVersions> implements IAppAndroidVersionsService {

}
