package com.stylefeng.guns.modular.system.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.system.model.AppHistory;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 推荐栏位历史 服务类
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-12
 */
public interface IAppHistoryService extends IService<AppHistory> {
    /**
     * 获取推荐记录数据
     * @param appHistory
     * @return
     */
    List<Map<String,Object>> getAppHistory(AppHistory appHistory,Page<Map<String,Object>> page);
}
