package com.stylefeng.guns.modular.system.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.common.constant.factory.PageFactory;
import com.stylefeng.guns.modular.system.model.AppHistory;
import com.stylefeng.guns.modular.system.model.AppRoom;
import com.stylefeng.guns.modular.system.model.AppSetSpeed;
import com.stylefeng.guns.modular.system.service.*;
import com.stylefeng.guns.modular.system.warpper.AppRecommendWarpper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;
import com.stylefeng.guns.modular.system.model.AppRecommend;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.SimpleFormatter;

/**
 * 推荐位置管理控制器
 * @Date 2019-03-12 14:51:57
 */
@Controller
@RequestMapping("/appRecommend")
public class AppRecommendController extends BaseController {

    private String PREFIX = "/system/appRecommend/";

    @Autowired
    private IAppRecommendService appRecommendService;
    @Autowired
    private IAppSetSpeedService appSetSpeedService;
    @Autowired
    private IAppRoomService appRoomService;
    @Autowired
    private IAppHistoryService appHistoryService;
    @Autowired
    private IAppClaimerService claimerService;


    /**
     * 跳转到推荐位置管理首页
     */
    @RequestMapping("")
    public String index(Model model) {
        AppSetSpeed appSetSpeed=appSetSpeedService.selectById(4);//获取是否开发推荐位
        model.addAttribute("item",appSetSpeed);
        //获取推荐位置的数据
        List<Map<String,Object>> mapList=appRecommendService.getAppRecommend(null);
        model.addAttribute("mapList",new AppRecommendWarpper(mapList).warp());
        return PREFIX + "appRecommend.html";
    }

    /**
     * 跳转到添加推荐位置管理
     */
    @RequestMapping("/appRecommend_add")
    public String appRecommendAdd(Integer id,Model model) {
        model.addAttribute("id",id);
        return PREFIX + "appRecommend_add.html";
    }
    /**
     * 跳转到查看历史推荐
     */
    @RequestMapping("/appRecommend_history")
    public String appRecommendHistory(Integer id,Model model) {
        model.addAttribute("id",id);
        return PREFIX + "appRecommend_history.html";
    }

    /**
     * 跳转到修改推荐位置管理
     */
    @RequestMapping("/appRecommend_update/{appRecommendId}")
    public String appRecommendUpdate(@PathVariable Integer appRecommendId, Model model) {
        AppRecommend appRecommend = appRecommendService.selectById(appRecommendId);
        model.addAttribute("item",appRecommend);
        LogObjectHolder.me().set(appRecommend);
        return PREFIX + "appRecommend_edit.html";
    }

    /**
     * 获取推荐位置管理列表
     */
    @RequestMapping(value = "/appRecommendHistory")
    @ResponseBody
    public Object appRecommendHistory_list(AppHistory appHistory) {
        Page<Map<String,Object>> page = new PageFactory<Map<String,Object>>().defaultPage();
        List<Map<String, Object>> result = appHistoryService.getAppHistory(appHistory,page);
        page.setRecords(result);
        return super.packForBT(page);
    }

    /**
     * 根据房间id查询房间名称和房主昵称
     * @param rid
     * @return
     */
    @RequestMapping(value = "/changeRid")
    @ResponseBody
    public Object changeRid(Integer rid) {
        AppRoom appRoom=appRoomService.selectOne(new EntityWrapper<AppRoom>().eq("rid",rid));
        return appRoom==null?new AppRoom():appRoom;
    }

    /**
     * 查看是否可以修改默认推荐位置
     * @param appRecommend
     * @return
     */
    @RequestMapping(value = "/isUpdate")
    @ResponseBody
    public Object isUpdate(AppRecommend appRecommend) {
        /*查看这个位置之前的用户时间是否到期*/
        AppRecommend recommend=appRecommendService.selectById(appRecommend.getId());
        if(recommend!=null && recommend.getIsDefault()==1 && recommend.getState()==1 && recommend.getsTime()!=null){//用户还没有过期的
            /*提示不可设置用户*/
            return  "1";
        }
        /*查看下个时间点有预约用户数量*/
        long currentTime = System.currentTimeMillis() ;
        currentTime +=60*60*1000;//加一个小时
        Date date=new Date(currentTime);
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH");
        Map<String,Object> map=new HashMap<>();
        map.put("time",format.format(date));
        Integer count=claimerService.getAppClaimerByTimeCount(map);
        /*查看默认推荐位置数量*/
        Integer isDefault=appRecommendService.selectCount(new EntityWrapper<AppRecommend>().eq("isDefault",2));//默认推荐数量
        if(count+isDefault==8){
            return "2";
        }
        return "0";
    }

    /**
     * 新增推荐位置管理（修改推荐位置）
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(AppRecommend appRecommend,Integer rid) {
        AppRecommend recommend=appRecommendService.selectById(appRecommend.getId());
        //根据房间号查询房主信息
        AppRoom appRoom=appRoomService.selectOne(new EntityWrapper<AppRoom>().eq("rid",rid));
        appRecommend.setCreateTime(new Date());
        appRecommend.setIsDefault(2);//设置为默认推荐
        appRecommend.setsTime(null);
        appRecommend.seteTime(null);
        appRecommend.setState(1);//位置 是否有人 1有，2没有
        if(appRecommend.getIsDefault()==2){
            appRecommend.setState(1);//位置 是否有人 1有，2没有
        }
        if(appRoom!=null){
            Integer count=appRecommendService.selectCount(new EntityWrapper<AppRecommend>().eq("uid",appRoom.getUid()));
            if(count>0){
                return "1";
            }
            appRecommend.setUid(appRoom.getUid());
        }
        appRecommendService.insertOrUpdateAllColumn(appRecommend);
        /*修改默认推荐记录的时间*/
        AppHistory history=appHistoryService.selectOne(new EntityWrapper<AppHistory>()
                .eq("sequence",recommend.getId())
                .eq("uid",recommend.getUid()).eq("isDelete",1)
                .orderBy("createTime",false));
        if(history!=null && history.geteTime()==null){//默认推荐的情况下需要添加它的结束时间
            history.seteTime(new Date());
            appHistoryService.updateById(history);
        }
        //添加历史记录
        AppHistory appHistory=new AppHistory();
        appHistory.setUid(appRecommend.getUid());
        appHistory.setCreateTime(new Date());
        appHistory.setIsDelete(1);
        appHistory.setSequence(appRecommend.getId());
        appHistory.setsTime(new Date());
        appHistory.setState(1);//1默认推荐，2用户申请
        appHistoryService.insert(appHistory);
        return SUCCESS_TIP;
    }

    /**
     * 删除推荐位置管理
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer appRecommendId) {
        appRecommendService.deleteById(appRecommendId);
        return SUCCESS_TIP;
    }

    /**
     * 修改推荐位置管理
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(AppRecommend appRecommend) {
        appRecommendService.updateById(appRecommend);
        return SUCCESS_TIP;
    }
    /**
     * 修改推荐位置管理 -- 是否开发推荐位
     */
    @RequestMapping(value = "/updateSpeed")
    @ResponseBody
    public Object updateSpeed(AppSetSpeed appSetSpeed) {
        appSetSpeedService.updateById(appSetSpeed);
        return SUCCESS_TIP;
    }
    /**
     * 推荐位置管理详情
     */
    @RequestMapping(value = "/detail/{appRecommendId}")
    @ResponseBody
    public Object detail(@PathVariable("appRecommendId") Integer appRecommendId) {
        return appRecommendService.selectById(appRecommendId);
    }
}
