package com.stylefeng.guns.modular.system.service;

import com.stylefeng.guns.modular.system.model.AppRoom;
import com.stylefeng.guns.modular.system.model.AppYbconsume;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;
import java.util.Map;

/**
 * @Description 聊天室收益服务类。
 * @author Grazer_Ma
 * @Date 2020-05-15 09:54:58
 */
public interface IAppRoomTurnoverService extends IService<AppYbconsume> {
    /**
     * 获取聊天室收益数据
     * @param appRoom
     * @return
     */
    List<Map<String,Object>> getAppRoomTurnover(AppRoom appRoom);
}
