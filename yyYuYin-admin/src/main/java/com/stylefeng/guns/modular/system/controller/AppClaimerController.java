package com.stylefeng.guns.modular.system.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.common.constant.factory.PageFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;
import com.stylefeng.guns.modular.system.model.AppClaimer;
import com.stylefeng.guns.modular.system.service.IAppClaimerService;

import java.util.List;
import java.util.Map;

/**
 * 推荐申请记录控制器
 * @Date 2019-03-12 14:52:46
 */
@Controller
@RequestMapping("/appClaimer")
public class AppClaimerController extends BaseController {

    private String PREFIX = "/system/appClaimer/";

    @Autowired
    private IAppClaimerService appClaimerService;

    /**
     * 跳转到推荐申请记录首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "appClaimer.html";
    }

    /**
     * 跳转到添加推荐申请记录
     */
    @RequestMapping("/appClaimer_add")
    public String appClaimerAdd() {
        return PREFIX + "appClaimer_add.html";
    }

    /**
     * 跳转到修改推荐申请记录
     */
    @RequestMapping("/appClaimer_update/{appClaimerId}")
    public String appClaimerUpdate(@PathVariable Integer appClaimerId, Model model) {
        AppClaimer appClaimer = appClaimerService.selectById(appClaimerId);
        model.addAttribute("item",appClaimer);
        LogObjectHolder.me().set(appClaimer);
        return PREFIX + "appClaimer_edit.html";
    }

    /**
     * 获取推荐申请记录列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(AppClaimer appClaimer) {
        Page<Map<String,Object>> page = new PageFactory<Map<String,Object>>().defaultPage();
        List<Map<String, Object>> result = appClaimerService.getAppClaimer(appClaimer,page);
        page.setRecords(result);
        return super.packForBT(page);
    }

    /**
     * 新增推荐申请记录
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(AppClaimer appClaimer) {
        appClaimerService.insert(appClaimer);
        return SUCCESS_TIP;
    }

    /**
     * 删除推荐申请记录
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(String appClaimerId) {
        EntityWrapper<AppClaimer> entityWrapper=new EntityWrapper<>();
        entityWrapper.in("id",appClaimerId);
       /* AppClaimer appClaimer=new AppClaimer();
        appClaimer.setIsDelete(2);*/
        appClaimerService.delete(entityWrapper);
        return SUCCESS_TIP;
    }

    /**
     * 修改推荐申请记录
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(AppClaimer appClaimer) {
        appClaimerService.updateById(appClaimer);
        return SUCCESS_TIP;
    }

    /**
     * 推荐申请记录详情
     */
    @RequestMapping(value = "/detail/{appClaimerId}")
    @ResponseBody
    public Object detail(@PathVariable("appClaimerId") Integer appClaimerId) {
        return appClaimerService.selectById(appClaimerId);
    }
}
