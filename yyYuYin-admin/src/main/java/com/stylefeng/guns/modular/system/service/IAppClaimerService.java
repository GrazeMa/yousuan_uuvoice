package com.stylefeng.guns.modular.system.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.system.model.AppClaimer;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 推荐位申请记录 服务类
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-12
 */
public interface IAppClaimerService extends IService<AppClaimer> {
    /**
     * 获取推荐记录数据
     * @param appClaimer
     * @return
     */
    List<Map<String,Object>> getAppClaimer(AppClaimer appClaimer,Page<Map<String,Object>> page);
    /**
     * 定时任务：根据时间点获取推荐位申请记录
     * @param map
     * @return
     */
    AppClaimer getAppClaimerByTime(Map<String,Object> map);
    /**
     * 根据时间阶段获取申请记录数
     * @param map
     * @return
     */
    Integer getAppClaimerByTimeCount(Map<String,Object> map);
}
