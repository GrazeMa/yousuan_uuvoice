package com.stylefeng.guns.modular.system.service.impl;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.system.model.AppHall;
import com.stylefeng.guns.modular.system.dao.AppHallMapper;
import com.stylefeng.guns.modular.system.model.AppUser;
import com.stylefeng.guns.modular.system.service.IAppHallService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 靓号绑定 服务实现类
 * </p>
 *
 * @author stylefeng123
 * @since 2019-07-09
 */
@Service
public class AppHallServiceImpl extends ServiceImpl<AppHallMapper, AppHall> implements IAppHallService {
    @Override
    public List<Map<String, Object>> getAppUserListExHall(AppUser appUser, Page page) {
        return this.baseMapper.getAppUserListExHall(appUser,page);
    }
}
