package com.stylefeng.guns.modular.system.dao;

import com.stylefeng.guns.modular.system.model.AppRecord;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 操作记录 Mapper 接口
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-01
 */
public interface AppRecordMapper extends BaseMapper<AppRecord> {

}
