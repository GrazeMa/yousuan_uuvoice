package com.stylefeng.guns.modular.system.controller;

import com.stylefeng.guns.core.base.controller.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;
import com.stylefeng.guns.modular.system.model.AppLiangDivide;
import com.stylefeng.guns.modular.system.service.IAppLiangDivideService;

/**
 * 分成设置控制器
 * @Date 2019-07-09 15:43:07
 */
@Controller
@RequestMapping("/appLiangDivide")
public class AppLiangDivideController extends BaseController {

    private String PREFIX = "/system/appLiangDivide/";

    @Autowired
    private IAppLiangDivideService appLiangDivideService;

    /**
     * 跳转到分成设置首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "appLiangDivide.html";
    }

    /**
     * 跳转到添加分成设置
     */
    @RequestMapping("/appLiangDivide_add")
    public String appLiangDivideAdd() {
        return PREFIX + "appLiangDivide_add.html";
    }

    /**
     * 跳转到修改分成设置
     */
    @RequestMapping("/appLiangDivide_update/{appLiangDivideId}")
    public String appLiangDivideUpdate(@PathVariable Integer appLiangDivideId, Model model) {
        AppLiangDivide appLiangDivide = appLiangDivideService.selectById(appLiangDivideId);
        model.addAttribute("item",appLiangDivide);
        LogObjectHolder.me().set(appLiangDivide);
        return PREFIX + "appLiangDivide_edit.html";
    }

    /**
     * 获取分成设置列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        return appLiangDivideService.selectList(null);
    }

    /**
     * 新增分成设置
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(AppLiangDivide appLiangDivide) {
        appLiangDivideService.insert(appLiangDivide);
        return SUCCESS_TIP;
    }

    /**
     * 删除分成设置
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer appLiangDivideId) {
        appLiangDivideService.deleteById(appLiangDivideId);
        return SUCCESS_TIP;
    }

    /**
     * 修改分成设置
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(AppLiangDivide appLiangDivide) {
        appLiangDivideService.updateById(appLiangDivide);
        return SUCCESS_TIP;
    }

    /**
     * 分成设置详情
     */
    @RequestMapping(value = "/detail/{appLiangDivideId}")
    @ResponseBody
    public Object detail(@PathVariable("appLiangDivideId") Integer appLiangDivideId) {
        return appLiangDivideService.selectById(appLiangDivideId);
    }
}
