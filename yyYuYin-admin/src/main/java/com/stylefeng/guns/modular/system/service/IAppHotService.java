package com.stylefeng.guns.modular.system.service;

import com.stylefeng.guns.modular.system.model.AppHot;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 热搜词 服务类
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-12
 */
public interface IAppHotService extends IService<AppHot> {

}
