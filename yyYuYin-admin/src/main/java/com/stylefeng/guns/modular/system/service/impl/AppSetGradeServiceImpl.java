package com.stylefeng.guns.modular.system.service.impl;

import com.stylefeng.guns.modular.system.model.AppSetGrade;
import com.stylefeng.guns.modular.system.dao.AppSetGradeMapper;
import com.stylefeng.guns.modular.system.service.IAppSetGradeService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 等级设置 服务实现类
 * </p>
 *
 * @author stylefeng123
 * @since 2019-02-28
 */
@Service
public class AppSetGradeServiceImpl extends ServiceImpl<AppSetGradeMapper, AppSetGrade> implements IAppSetGradeService {

}
