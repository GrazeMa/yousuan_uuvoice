package com.stylefeng.guns.modular.system.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.kdDemo.utils.SignUtils;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.common.constant.factory.PageFactory;
import com.stylefeng.guns.core.shiro.ShiroKit;
import com.stylefeng.guns.core.util.SinataUtil;
import com.stylefeng.guns.modular.system.model.AppRoom;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;
import com.stylefeng.guns.modular.system.model.AppBackdrop;
import com.stylefeng.guns.modular.system.service.IAppBackdropService;

import java.util.Date;

/**
 * 房间背景图控制器
 * @Date 2019-03-07 10:18:43
 */
@Controller
@RequestMapping("/appBackdrop")
public class AppBackdropController extends BaseController {

    private String PREFIX = "/system/appBackdrop/";

    @Autowired
    private IAppBackdropService appBackdropService;

    /**
     * 跳转到房间背景图首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "appBackdrop.html";
    }

    /**
     * 跳转到添加房间背景图
     */
    @RequestMapping("/appBackdrop_add")
    public String appBackdropAdd() {
        return PREFIX + "appBackdrop_add.html";
    }

    /**
     * 跳转到修改房间背景图
     */
    @RequestMapping("/appBackdrop_update/{appBackdropId}")
    public String appBackdropUpdate(@PathVariable Integer appBackdropId, Model model) {
        AppBackdrop appBackdrop = appBackdropService.selectById(appBackdropId);
        model.addAttribute("item",appBackdrop);
        LogObjectHolder.me().set(appBackdrop);
        return PREFIX + "appBackdrop_edit.html";
    }

    /**
     * 获取房间背景图列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(AppBackdrop appBackdrop,String beginTime,String endTime) {
        Wrapper entityWrapper=new EntityWrapper<AppBackdrop>();
        entityWrapper.eq("isDelete",1);
        if(SinataUtil.isNotEmpty(appBackdrop.getName())){
            entityWrapper.like("name",appBackdrop.getName());
        }
        if(SinataUtil.isNotEmpty(appBackdrop.getState())){
            entityWrapper.eq("state",appBackdrop.getState());
        }
        if(SinataUtil.isNotEmpty(beginTime)){
            entityWrapper.ge("createTime",beginTime +" 00:00:00");
        }
        if(SinataUtil.isNotEmpty(endTime)){
            entityWrapper.le("createTime",endTime +" 23:59:59");
        }
        entityWrapper.orderBy("createTime",false);
        Page<AppBackdrop> page = new PageFactory<AppBackdrop>().defaultPage();
        page.setRecords(appBackdropService.selectMapsPage(page, entityWrapper).getRecords());
        return super.packForBT(page);
    }

    /**
     * 新增房间背景图
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(AppBackdrop appBackdrop) {
        appBackdrop.setCreateTime(new Date());
        appBackdropService.insert(appBackdrop);
        return SUCCESS_TIP;
    }

    /**
     * 删除房间背景图
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(String appBackdropId) {
        EntityWrapper<AppBackdrop> entityWrapper=new EntityWrapper<>();
        entityWrapper.in("id",appBackdropId);
        AppBackdrop appBackdrop=new AppBackdrop();
        appBackdrop.setIsDelete(2);
        appBackdropService.update(appBackdrop,entityWrapper);
        return SUCCESS_TIP;
    }

    /**
     * 修改房间背景图
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(AppBackdrop appBackdrop) {
        appBackdropService.updateById(appBackdrop);
        return SUCCESS_TIP;
    }
    /**
     * 修改用户管理 冻结和解冻
     */
    @RequestMapping(value = "/updateFreeze")
    @ResponseBody
    @Transactional
    public Object updateFreeze(String ids,Integer state) {
        EntityWrapper<AppBackdrop> entityWrapper=new EntityWrapper<>();
        entityWrapper.in("id",ids);
        AppBackdrop appBackdrop=new AppBackdrop();
        appBackdrop.setState(state);
        appBackdropService.update(appBackdrop,entityWrapper);
        return SUCCESS_TIP;
    }
    /**
     * 房间背景图详情
     */
    @RequestMapping(value = "/detail/{appBackdropId}")
    @ResponseBody
    public Object detail(@PathVariable("appBackdropId") Integer appBackdropId) {
        return appBackdropService.selectById(appBackdropId);
    }
}
