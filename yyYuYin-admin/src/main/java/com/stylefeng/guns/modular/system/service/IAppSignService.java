package com.stylefeng.guns.modular.system.service;

import com.stylefeng.guns.modular.system.model.AppSign;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 签到设置 服务类
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-01
 */
public interface IAppSignService extends IService<AppSign> {
    /**
     * 获取签到和奖励
     * @param map
     * @return
     */
    List<Map<String,Object>> getAppSignAndAppAward(Map<String,Object> map);
}
