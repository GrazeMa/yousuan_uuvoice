package com.stylefeng.guns.modular.system.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.common.constant.factory.PageFactory;
import com.stylefeng.guns.core.util.SinataUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;
import com.stylefeng.guns.modular.system.model.AppHot;
import com.stylefeng.guns.modular.system.service.IAppHotService;

import java.util.Date;

/**
 * 热搜词控制器
 * @Date 2019-03-12 19:39:03
 */
@Controller
@RequestMapping("/appHot")
public class AppHotController extends BaseController {

    private String PREFIX = "/system/appHot/";

    @Autowired
    private IAppHotService appHotService;

    /**
     * 跳转到热搜词首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "appHot.html";
    }

    /**
     * 跳转到添加热搜词
     */
    @RequestMapping("/appHot_add")
    public String appHotAdd() {
        return PREFIX + "appHot_add.html";
    }

    /**
     * 跳转到修改热搜词
     */
    @RequestMapping("/appHot_update/{appHotId}")
    public String appHotUpdate(@PathVariable Integer appHotId, Model model) {
        AppHot appHot = appHotService.selectById(appHotId);
        model.addAttribute("item",appHot);
        LogObjectHolder.me().set(appHot);
        return PREFIX + "appHot_edit.html";
    }

    /**
     * 获取热搜词列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(AppHot appHot,String beginTime,String endTime) {
        Wrapper entityWrapper=new EntityWrapper<AppHot>();
        if(SinataUtil.isNotEmpty(appHot.getState())){
            entityWrapper.eq("state",appHot.getState());
        }
        if(SinataUtil.isNotEmpty(appHot.getStatus())){
            entityWrapper.eq("status",appHot.getStatus());
        }
        if(SinataUtil.isNotEmpty(appHot.getName())){
            entityWrapper.like("name",appHot.getName());
        }
        if(SinataUtil.isNotEmpty(beginTime)){
            entityWrapper.ge("createTime",beginTime +" 00:00:00");
        }
        if(SinataUtil.isNotEmpty(endTime)){
            entityWrapper.le("createTime",endTime +" 23:59:59");
        }
        entityWrapper.orderBy("num",false);
        Page<AppHot> page = new PageFactory<AppHot>().defaultPage();
        page.setRecords(appHotService.selectMapsPage(page, entityWrapper).getRecords());
        return super.packForBT(page);
    }

    /**
     * 新增热搜词
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(AppHot appHot) {
       AppHot appHot1= appHotService.selectOne(new EntityWrapper<AppHot>().eq("name",appHot.getName()));
       if(appHot1!=null){
           return  "1";
       }
        appHot.setCreateTime(new Date());
        appHot.setState(1);//平台
        appHot.setStatus(2);//设置为热搜
        appHotService.insert(appHot);
        return SUCCESS_TIP;
    }

    /**
     * 删除热搜词
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(String  appHotId) {
        EntityWrapper<AppHot> entityWrapper=new EntityWrapper<>();
        entityWrapper.in("id",appHotId);
        appHotService.delete(entityWrapper);
        return SUCCESS_TIP;
    }

    /**
     * 修改热搜词
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(AppHot appHot) {
        AppHot appHot_=appHotService.selectById(appHot.getId());
        if(!appHot.getName().equals(appHot_.getName())){
            AppHot appHot1= appHotService.selectOne(new EntityWrapper<AppHot>().eq("name",appHot.getName()));
            if(appHot1!=null){
                return  "1";
            }
        }
        appHotService.updateById(appHot);
        return SUCCESS_TIP;
    }

    /**
     * 设为热搜
     * @param appHot
     * @return
     */
    @RequestMapping(value = "/update_")
    @ResponseBody
    public Object update_(AppHot appHot) {
        appHotService.updateById(appHot);
        return SUCCESS_TIP;
    }


    /**
     * 热搜词详情
     */
    @RequestMapping(value = "/detail/{appHotId}")
    @ResponseBody
    public Object detail(@PathVariable("appHotId") Integer appHotId) {
        return appHotService.selectById(appHotId);
    }
}
