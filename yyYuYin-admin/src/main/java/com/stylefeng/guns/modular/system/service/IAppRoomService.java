package com.stylefeng.guns.modular.system.service;

import com.stylefeng.guns.modular.system.model.AppRoom;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 房间信息 服务类
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-07
 */
public interface IAppRoomService extends IService<AppRoom> {
    /**
     * 房间取消推荐
     * @param appRoom
     * @return
     */
    Integer updateRecommend(AppRoom appRoom);
}
