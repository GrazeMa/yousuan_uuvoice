package com.stylefeng.guns.modular.system.service.impl;

import com.stylefeng.guns.modular.system.model.AppSign;
import com.stylefeng.guns.modular.system.dao.AppSignMapper;
import com.stylefeng.guns.modular.system.service.IAppSignService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 签到设置 服务实现类
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-01
 */
@Service
public class AppSignServiceImpl extends ServiceImpl<AppSignMapper, AppSign> implements IAppSignService {
    @Override
    public List<Map<String, Object>> getAppSignAndAppAward(Map<String, Object> map) {
        return this.baseMapper.getAppSignAndAppAward(map);
    }
}
