package com.stylefeng.guns.modular.system.controller;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.config.properties.GunsProperties;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.common.constant.factory.PageFactory;
import com.stylefeng.guns.core.shiro.ShiroKit;
import com.stylefeng.guns.core.util.DateUtil;
import com.stylefeng.guns.core.util.SinataUtil;
import com.stylefeng.guns.modular.system.model.*;
import com.stylefeng.guns.modular.system.service.*;
import io.agora.signal.Signal;
import io.swagger.models.auth.In;
import org.omg.CORBA.OBJ_ADAPTER;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.*;

/**
 * 房间管理控制器
 * @Date 2019-03-07 09:53:23
 */
@Controller
@RequestMapping("/appRoom")
public class AppRoomController extends BaseController {

    private String PREFIX = "/system/appRoom/";
    @Autowired
    private GunsProperties gunsProperties;
    @Autowired
    private IAppRoomService appRoomService;
    @Autowired
    private IAppRecordService appRecordService;
    @Autowired
    private IAppAgreementService appAgreementService;
    @Autowired
    private IAppUserService appUserService;
    @Autowired
    private IAppChatroomsService appChatroomsService;
    @Autowired
    private IAppCharmService appCharmService;
    @Autowired
    private IAppTreasureService appTreasureService;
    @Autowired
    private IAppAuctionService appAuctionService;
    /**
     * 跳转到房间管理首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "appRoom.html";
    }

    /**
     * 跳转到添加房间管理
     */
    @RequestMapping("/appRoom_add")
    public String appRoomAdd() {
        return PREFIX + "appRoom_add.html";
    }
    /**
     * 跳转到查看房间贡献榜
     */
    @RequestMapping("/appTreasureAndCharm_list")
    public String AppTreasureAndCharm(Integer rid,Model model) {
        model.addAttribute("rid",rid);
        return PREFIX + "appTreasureAndCharm_list.html";
    }
    /**
     * 跳转到房间竞拍列表
     */
    @RequestMapping("/appAuction_list")
    public String AppAuction(Integer rid,Model model) {
        model.addAttribute("rid",rid);
        List<AppAuction> appAuctions=appAuctionService.selectPage(new Page<>(0,3),
                new EntityWrapper<AppAuction>().eq("isDelete",1)
                .eq("state",2).isNotNull("endTime").eq("rid",rid)
                .orderBy("staTime",true)).getRecords();
        model.addAttribute("size",appAuctions.size());
        for(int i=0;i<appAuctions.size();i++){
            appAuctions.get(i).setRanks(i+1);
            appAuctions.get(i).setStaTime_(DateUtil.formatDate(appAuctions.get(i).getStaTime(),"yyyy-MM-dd"));
            appAuctions.get(i).setEndTime_(DateUtil.formatDate(appAuctions.get(i).getEndTime(),"yyyy-MM-dd"));
        }
        model.addAttribute("appAuctions",appAuctions);
        return PREFIX + "appAuction_list.html";
    }
    /**
     * 跳转到查看房间贡献榜==贡献记录
     */
    @RequestMapping("/appRoom_auctionRecordById")
    public String appRoomRecordById(Integer rid,Integer id,Integer uid,Model model) {
        model.addAttribute("rid",rid);
        if(id==null){
            id=-1;
        }
        model.addAttribute("id",id);
        model.addAttribute("uid",uid);
        return PREFIX + "appRoom_auctionRecord.html";
    }
    /**
     * 跳转到添加房间管理
     * @param  type 1 设为推荐 2 分配协议号
     */
    @RequestMapping("/appRoom_recommendOrAgreement")
    public String appRoomRecommendOrAgreement(Integer id,Integer type,Model model) {
        model.addAttribute("id",id);
        model.addAttribute("type_",type);
        return PREFIX + "appRoom_recommendOrAgreement.html";
    }

    /**
     * 跳转到修改房间管理
     */
    @RequestMapping("/appRoom_update/{appRoomId}")
    public String appRoomUpdate(@PathVariable Integer appRoomId, Model model) {
        AppRoom appRoom = appRoomService.selectById(appRoomId);
        AppUser appUser=appUserService.selectById(appRoom.getUid());
        model.addAttribute("item",appRoom);
        model.addAttribute("appUser",appUser==null?new AppUser():appUser);
        String typeName="";
        if(SinataUtil.isNotEmpty(appRoom.getType()) && appRoom.getType()==1){
            typeName="热门";
        }else if(SinataUtil.isNotEmpty(appRoom.getType()) && appRoom.getType()==2){
            typeName="女神";
        }else if(SinataUtil.isNotEmpty(appRoom.getType()) && appRoom.getType()==3){
            typeName="男神";
        }else if(SinataUtil.isNotEmpty(appRoom.getType()) && appRoom.getType()==4){
            typeName="娱乐";
        }else if(SinataUtil.isNotEmpty(appRoom.getType()) && appRoom.getType()==5){
            typeName="听歌";
        }else if(SinataUtil.isNotEmpty(appRoom.getType()) && appRoom.getType()==6){
            typeName="相亲";
        }else if(SinataUtil.isNotEmpty(appRoom.getType()) && appRoom.getType()==7){
            typeName="电台";
        }
        model.addAttribute("typeName",typeName);
        LogObjectHolder.me().set(appRoom);
        return PREFIX + "appRoom_edit.html";
    }

    /**
     * 获取房间管理列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(AppRoom appRoom) {
        Wrapper entityWrapper=new EntityWrapper<AppRoom>();
        entityWrapper.eq("isDelete",1);
        if(SinataUtil.isNotEmpty(appRoom.getRoomName())){
            entityWrapper.like("roomName",appRoom.getRoomName());
        }
        if(SinataUtil.isNotEmpty(appRoom.getName())){
            entityWrapper.like("name",appRoom.getName());
        }
        if(SinataUtil.isNotEmpty(appRoom.getState())){
            entityWrapper.eq("state",appRoom.getState());
        }
        if(SinataUtil.isNotEmpty(appRoom.getStatus())){
            entityWrapper.eq("status",appRoom.getStatus());
        }
        if(SinataUtil.isNotEmpty(appRoom.getRid())){
            entityWrapper.eq("rid",appRoom.getRid());
        }
        entityWrapper.orderBy("createTime",false);

        Page<AppRoom> page = new PageFactory<AppRoom>().defaultPage();
        page.setRecords(appRoomService.selectMapsPage(page, entityWrapper).getRecords());
        return super.packForBT(page);
    }
    /**
     * 获取房间最近竞拍排名数据（前20条数据）
     * @param rid 房间id
     * @param id 时间段的id
     */
    @RequestMapping(value = "/auction/list")
    @ResponseBody
    public Object auctionList(Integer rid, Integer id) {
        if(id==null){
            id=-1;
        }
        AppAuction appAuction=appAuctionService.selectById(id);
        Map<String,Object> paMap=new HashMap<>();
        if(appAuction!=null){
            paMap.put("beginTime",DateUtil.format(appAuction.getStaTime(),"yyyy-MM-dd HH:mm:ss"));
            paMap.put("endTime",DateUtil.format(appAuction.getEndTime(),"yyyy-MM-dd HH:mm:ss"));
        }
        paMap.put("rid",rid);
        return appAuctionService.getAppAuction(paMap);
    }

    /**
     * 获取房间最近竞拍排名数据===查看贡献记录
     * @param rid
     * @param id
     * @return
     */
    @RequestMapping(value = "/auctionRecord/list")
    @ResponseBody
    public Object auctionRecord(Integer rid, Integer id,Integer uid) {
        Map<String,Object> paMap=new HashMap<>();
        if(id!=-1){
            AppAuction appAuction=appAuctionService.selectById(id);
            paMap.put("beginTime",DateUtil.format(appAuction.getStaTime(),"yyyy-MM-dd HH:mm:ss"));
            paMap.put("endTime",DateUtil.format(appAuction.getEndTime(),"yyyy-MM-dd HH:mm:ss"));
        }
        paMap.put("rid",rid);
        paMap.put("uid",uid);
        return appAuctionService.getAppAuctionRecord(paMap);
    }
    /**
     * 获取房间贡献榜数据（前20条数据）
     * @param  type =1 财富榜 2 魅力榜
     * @param day =1日榜 2 周榜 ，3 总榜
     */
    @RequestMapping(value = "/treasureAndCharm/list")
    @ResponseBody
    public Object treasureAndCharmList(Integer rid,String usercoding,String nickname, Integer type,Integer day) {
        if(day==null){
            day=3;
        }
        if(type==null){
            type=1;
        }
        Map<String,Object> paMap=new HashMap<>();
        paMap.put("rid",rid);
        paMap.put("day",day);
        paMap.put("usercoding",usercoding);
        paMap.put("nickname",nickname);
        List<Map<String,Object>> reList=new ArrayList<>();
        if(type==1){
            reList=appTreasureService.getAppTreasure(paMap);
        }else if(type==2){
            reList=appCharmService.getAppCharm(paMap);
        }
        for(int i=0;i<reList.size();i++){
            Map<String,Object> map=new HashMap<>();
            if(i>0){
                Integer num=Integer.valueOf(reList.get(i-1).get("value_").toString())-Integer.valueOf(reList.get(i).get("value_").toString());
                reList.get(i).put("num",num);
            }
        }
        return reList;
    }
    /**
     * 新增房间管理
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(AppRoom appRoom) {
        appRoomService.insert(appRoom);
        return SUCCESS_TIP;
    }

    /**
     * 删除房间管理
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(String appRoomId) {
        //删除房间信息
        EntityWrapper<AppRoom> entityWrapper=new EntityWrapper<>();
        entityWrapper.in("id",appRoomId);
        AppRoom appRoom=new AppRoom();
        appRoom.setIsDelete(2);
        appRoomService.update(appRoom,entityWrapper);
        List<AppAgreement> appAgreements=appAgreementService.selectList(new EntityWrapper<AppAgreement>().in("eid",appRoomId));
        String appAgreementId="";
        for(AppAgreement appAgreement:appAgreements){
            appAgreementId+=appAgreement.getId()+",";
        }
        if(SinataUtil.isNotEmpty(appAgreementId)){
            appAgreementId=appAgreementId.substring(0,appAgreementId.length()-1);
        }
        //释放协议号
        Map<String,Object> map=new HashMap<>();
        map.put("ids",appAgreementId);
        appAgreementService.updateIncome(map);
        return SUCCESS_TIP;
    }

    /**
     *设为推荐和分配协议号保存数据
     * @param appRoom
     * @param type_ 1 设为推荐 2 分配协议号
     * @return
     */
    @RequestMapping(value = "/updateOpen")
    @ResponseBody
    @Transactional
    public Object updateOpen(AppRoom appRoom, Integer type_, Integer days) {
        String hand="";
        if(type_==1){// 设为推荐
            String typeName="";
            if(appRoom.getType()==1){
                typeName="热门";
            }else if(appRoom.getType()==2){
                typeName="女神";
            }else if(appRoom.getType()==3){
                typeName="男神";
            }else if(appRoom.getType()==4){
                typeName="娱乐";
            }else if(appRoom.getType()==5){
                typeName="听歌";
            }else if(appRoom.getType()==6){
                typeName="相亲";
            }else if(appRoom.getType()==7){
                typeName="电台";
            }
            hand="将此房间设为了"+typeName+"推荐,排序为："+appRoom.getSequence();
            appRoom.setIsRecommend(2);
            appRoom.setUpdateTime(new Date());
        }else {//分配协议号
            //计算协议号数
            AppRoom appRoom1=appRoomService.selectById(appRoom.getId());
            Integer count=appRoom.getProtocolNum();
            //查询没有分配并且没有删除的协议号
            EntityWrapper entityWrapper= new EntityWrapper<AppAgreement>();
            entityWrapper.eq("state",1);//是否分配 1否，2 是
            entityWrapper.eq("isDelete",1);
            List<AppAgreement> appAgreementList=appAgreementService.selectPage(new Page(1 ,count), entityWrapper).getRecords();
            //如果协议号的总数小于了输入的协议号  就直接吧这些协议号分配给这个用户
            if(appAgreementList.size()==0){
                return "cont0";
            }else if(appAgreementList.size()<appRoom.getProtocolNum()){
                count=appAgreementList.size();
            }
            String ids="";
            for(AppAgreement appAgreement:appAgreementList){
                ids+=appAgreement.getId()+",";
            }
            if(SinataUtil.isNotEmpty(ids)){
                ids=ids.substring(0,ids.length()-1);
            }
            AppAgreement appAgreement=new AppAgreement();
            appAgreement.setAddTime(new Date());//分配时间
            appAgreement.setEid(appRoom1.getRid());//保存房间号
            appAgreement.setState(2);//是否分配 1否，2 是
            appAgreement.setDays(days);
            //修改协议号数据
            appAgreementService.update(appAgreement,new EntityWrapper<AppAgreement>().in("id",ids));
            String dayName="";
            if(days==-1){//时间不限制
                dayName="长期";
            }else {
                dayName=days+"天";
            }
            hand="为此房间分配了"+count+"个协议号，时间为："+dayName;
            appRoom.setProtocolNum(appRoom1.getProtocolNum()+count);
            for(AppAgreement appAgreement1:appAgreementList){
                //添加聊天室数据
                AppChatrooms appChatrooms=new AppChatrooms();
                appChatrooms.setUid(appAgreement1.getId());
                appChatrooms.setPid(appRoom1.getRid()+"");
                appChatrooms.setType(3);//类型 1是房主，2 是管理员，3用户
                appChatrooms.setState(1);
                appChatrooms.setSequence(0);
                appChatrooms.setStatus(1);
                appChatrooms.setCreateTime(new Date());
                appChatrooms.setIsAgreement(2);//是否是协议号，1否，2是
                appChatroomsService.insert(appChatrooms);
            }
        }
        /*添加操作记录*/
        AppRecord appRecord=new AppRecord();
        appRecord.setCreateTime(new Date());
        appRecord.setName(ShiroKit.getUser().getName());
        appRecord.setHand(hand);
        appRecord.setPid(appRoom.getId());
        appRecord.setType(2);//1用户 2房间
        appRecordService.insert(appRecord);
        appRoomService.updateById(appRoom);
        return SUCCESS_TIP;
    }

    /**
     * 修改房间管理
     * @param  type_ 1: 禁用和解禁 2 设定和取消牌照厅
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    @Transactional
    public Object update(AppRoom appRoom,Integer type_) {
        AppRoom appRoom_=appRoomService.selectById(appRoom.getId());
        String hand="";
        if(type_==1){//禁用和解禁
            if(appRoom.getStatus()==1){
                hand="解禁了此房间";

            }else{
                hand="禁用了此房间";
                /*禁用房间之后需要推送消息*/
                appRoom_.setStatus(2);//无效

                /*改变这个房间的数据信息1这个房间用户要退出来，2在麦上的要下麦然后退出来,3群主也要退出来*/
                AppChatrooms appChatrooms=new AppChatrooms();
                appChatrooms.setState(1);//是否上麦 1否，2 是
                appChatrooms.setStatus(2);//是否在房间 1在，2 不在
                appChatroomsService.update(appChatrooms,new EntityWrapper<AppChatrooms>().eq("pid",appRoom_.getRid()));
                //修改房主状态
                appRoom.setIsfz(2);//房主是否在房间 1 在， 2不在
                //推送
                Signal sig = new Signal(gunsProperties.getSappId());
                Signal.LoginSession loginSession=sig.login("1", "_no_need_token", new Signal.LoginCallback());
                Signal.LoginSession.Channel channel=loginSession.channelJoin(appRoom_.getRid(), new Signal.ChannelCallback());
                String string = JSONObject.toJSONString(appRoom_);
                channel.channelSetAttr("attr_xgfj",string);

            }
        }else {//设定和取消牌照厅
            if(appRoom.getState()==1){
                appRoom.setIsJp(1);//关闭竞拍
                hand="取消了此房间的牌照";
                /*设定和取消牌照厅房间之后需要推送消息*/
                appRoom_.setState(1);//是否牌子房间 1否，2是
                appRoom_.setIsJp(1);//关闭竞拍
                Signal sig = new Signal(gunsProperties.getSappId());
                Signal.LoginSession loginSession=sig.login("1", "_no_need_token", new Signal.LoginCallback());
                Signal.LoginSession.Channel channel=loginSession.channelJoin(appRoom_.getRid(), new Signal.ChannelCallback());

                String string = JSONObject.toJSONString(appRoom_);
                channel.channelSetAttr("attr_xgfj",string);
            }else{
                hand="设置此房间为牌照厅";
                /*设定和取消牌照厅之后需要推送消息*/
                appRoom_.setState(2);//是否牌子房间 1否，2是
                Signal sig = new Signal(gunsProperties.getSappId());
                Signal.LoginSession loginSession=sig.login("1", "_no_need_token", new Signal.LoginCallback());
                Signal.LoginSession.Channel channel=loginSession.channelJoin(appRoom_.getRid(), new Signal.ChannelCallback());

                String string = JSONObject.toJSONString(appRoom_);
                channel.channelSetAttr("attr_xgfj",string);
            }
        }
        /*添加操作记录*/
        AppRecord appRecord=new AppRecord();
        appRecord.setCreateTime(new Date());
        appRecord.setName(ShiroKit.getUser().getName());
        appRecord.setHand(hand);
        appRecord.setPid(appRoom.getId());
        appRecord.setType(2);//1用户 2房间
        appRecordService.insert(appRecord);
        appRoomService.updateById(appRoom);
        return SUCCESS_TIP;
    }
    /**
     * 取消推荐
     */
    @RequestMapping(value = "/unRecommend")
    @ResponseBody
    @Transactional
    public Object unRecommend(AppRoom appRoom) {
        appRoomService.updateRecommend(appRoom);
        /*添加操作记录*/
        AppRecord appRecord=new AppRecord();
        appRecord.setCreateTime(new Date());
        appRecord.setName(ShiroKit.getUser().getName());
        appRecord.setHand("取消了此房间的推荐");
        appRecord.setPid(appRoom.getId());
        appRecord.setType(2);//1用户 2房间
        appRecordService.insert(appRecord);
        return SUCCESS_TIP;
    }
    /**
     * 房间管理详情
     */
    @RequestMapping(value = "/detail/{appRoomId}")
    @ResponseBody
    public Object detail(@PathVariable("appRoomId") Integer appRoomId) {
        return appRoomService.selectById(appRoomId);
    }
}
