package com.stylefeng.guns.modular.system.dao;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.system.model.AppGuildSettle;

/**
 * @Description 结算记录 Mapper 接口。
 * @author Grazer_Ma
 * @Date 2020-05-20 20:30:03
 */
public interface AppSettleTurnoverMapper extends BaseMapper<AppGuildSettle> {
	/**
	 * 获取结算记录。
	 * 
	 * @param appGuildSettle
	 * @return
	 */
	List<Map<String, Object>> getAppSettleTurnover(AppGuildSettle appGuildSettle, Page<Map<String, Object>> page);
}
