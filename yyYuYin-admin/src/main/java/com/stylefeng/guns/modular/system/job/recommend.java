package com.stylefeng.guns.modular.system.job;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.stylefeng.guns.core.util.SinataUtil;
import com.stylefeng.guns.modular.system.model.AppClaimer;
import com.stylefeng.guns.modular.system.model.AppHistory;
import com.stylefeng.guns.modular.system.model.AppRecommend;
import com.stylefeng.guns.modular.system.service.IAppClaimerService;
import com.stylefeng.guns.modular.system.service.IAppHistoryService;
import com.stylefeng.guns.modular.system.service.IAppRecommendService;
import org.apache.shiro.crypto.hash.Hash;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 商品每天库存记录
 */
@Component
@EnableScheduling
public class recommend {
	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private IAppRecommendService recommendService;

	@Autowired
	private IAppClaimerService claimerService;

	@Autowired
	private IAppHistoryService historyService;

	@Scheduled(cron = "0 0 0-23 * * ?")
	//@Scheduled(cron = "0 0/2 * * * ? ")
	public void updateStatusToOutTime() {
		try {
			AppRecommend rc = new AppRecommend();
			rc.setIsDefault(1);
			/*获取推荐位数据*/
			List<AppRecommend> recommends = recommendService.selectList(new EntityWrapper<AppRecommend>().eq("isDefault",1));//不是默认推荐(用户可以申请的)
			Map<String,Object> map=new HashMap<>();
			DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH");
			map.put("time",format.format(new Date()));
			for(AppRecommend recommend:recommends){
				/*获取申请记录数据*/
				AppClaimer appClaimer=claimerService.getAppClaimerByTime(map);
				if (appClaimer!=null) {
					/*修改申请记录被记录过*/
					appClaimer.setState(2);
					claimerService.updateById(appClaimer);
					recommend.setUid(appClaimer.getUid());
					recommend.setsTime(appClaimer.getsTime());
					recommend.seteTime(appClaimer.geteTime());
					recommend.setState(1);
					/*修改默认推荐记录的时间*/
					AppHistory history=historyService.selectOne(new EntityWrapper<AppHistory>()
							.eq("sequence",recommend.getId())
							.eq("uid",recommend.getUid()).eq("isDelete",1)
							.orderBy("createTime",false));
					if(history!=null && history.geteTime()==null){//默认推荐的情况下需要添加它的结束时间
						history.seteTime(new Date());
						historyService.updateById(history);
					}
					/*添加记录*/
					AppHistory appHistory=new AppHistory();
					appHistory.setSequence(recommend.getId());
					appHistory.setCreateTime(new Date());
					appHistory.setUid(appClaimer.getUid());
					appHistory.setsTime(appClaimer.getsTime());
					appHistory.seteTime(appClaimer.geteTime());
					appHistory.setIsDelete(1);
					historyService.insert(appHistory);
				} else {
					recommend.setState(2);
				}
				recommendService.updateById(recommend);
			}

		} catch (Exception e) {
			e.printStackTrace();
			log.error("商品库存记录添加异常", e.getMessage());
		}
	}

}
