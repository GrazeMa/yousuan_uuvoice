package com.stylefeng.guns.modular.system.dao;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.system.model.AppGuild;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

/**
 * @Description 入会与退会记录 Mapper 接口。
 * @author Grazer_Ma
 * @Date 2020-05-14 21:47:16
 */
public interface AppGuildHistoryMapper extends BaseMapper<AppGuild> {
    /**
     * 获取入会与退会记录。
     * @param appGuild
     * @return
     */
    List<Map<String,Object>> getAppGuildHistory(AppGuild appGuild, Page<Map<String,Object>> page);
}
