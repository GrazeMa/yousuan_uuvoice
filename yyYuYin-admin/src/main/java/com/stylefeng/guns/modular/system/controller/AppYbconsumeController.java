package com.stylefeng.guns.modular.system.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.util.SinataUtil;
import com.stylefeng.guns.modular.system.model.AppRoom;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;
import com.stylefeng.guns.modular.system.model.AppYbconsume;
import com.stylefeng.guns.modular.system.service.IAppYbconsumeService;

/**
 * 优币消费控制器
 * @Date 2019-03-14 11:22:59
 */
@Controller
@RequestMapping("/appYbconsume")
public class AppYbconsumeController extends BaseController {

    private String PREFIX = "/system/appYbconsume/";

    @Autowired
    private IAppYbconsumeService appYbconsumeService;

    /**
     * 跳转到优币消费首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "appYbconsume.html";
    }

    /**
     * 跳转到
     * @return
     */
    @RequestMapping("/room")
    public String indexRoom() {
        return PREFIX + "appYbconsumeRoom.html";
    }

    /**
     * 跳转到添加优币消费
     */
    @RequestMapping("/appYbconsume_add")
    public String appYbconsumeAdd() {
        return PREFIX + "appYbconsume_add.html";
    }

    /**
     * 跳转到修改优币消费
     */
    @RequestMapping("/appYbconsume_update/{appYbconsumeId}")
    public String appYbconsumeUpdate(@PathVariable Integer appYbconsumeId, Model model) {
        AppYbconsume appYbconsume = appYbconsumeService.selectById(appYbconsumeId);
        model.addAttribute("item",appYbconsume);
        LogObjectHolder.me().set(appYbconsume);
        return PREFIX + "appYbconsume_edit.html";
    }

    /**
     * 获取优币消费列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(AppYbconsume appYbconsume,String beginTime,String endTime) {
    	
        EntityWrapper<AppYbconsume> entityWrapper=new EntityWrapper<>();
        if(SinataUtil.isNotEmpty(appYbconsume.getEid())){
            entityWrapper.eq("eid",appYbconsume.getEid());
        }
        if(SinataUtil.isNotEmpty(appYbconsume.getRid())){
            entityWrapper.eq("rid",appYbconsume.getRid());
        }
        if(SinataUtil.isNotEmpty(appYbconsume.getName())){
            entityWrapper.like("name",appYbconsume.getName());
        }
        if(SinataUtil.isNotEmpty(beginTime)){
            entityWrapper.ge("createTime",beginTime +" 00:00:00");
        }
        if(SinataUtil.isNotEmpty(endTime)){
            entityWrapper.le("createTime",endTime +" 23:59:59");
        }
        
        entityWrapper.eq("isDelete", 1);
//        entityWrapper.eq("state", 1);
        entityWrapper.orderBy("createTime", false);
        return appYbconsumeService.selectList(entityWrapper);
    }

    /**
     * 房间流水
     * @param appRoom
     * @return
     */
    @RequestMapping(value = "/room/list")
    @ResponseBody
    public Object roomList(AppRoom appRoom) {
        return appYbconsumeService.getAppYbconsume(appRoom);
    }


    /**
     * 新增优币消费
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(AppYbconsume appYbconsume) {
        appYbconsumeService.insert(appYbconsume);
        return SUCCESS_TIP;
    }

    /**
     * 删除优币消费
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer appYbconsumeId) {
        appYbconsumeService.deleteById(appYbconsumeId);
        return SUCCESS_TIP;
    }

    /**
     * 修改优币消费
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(AppYbconsume appYbconsume) {
        appYbconsumeService.updateById(appYbconsume);
        return SUCCESS_TIP;
    }

    /**
     * 优币消费详情
     */
    @RequestMapping(value = "/detail/{appYbconsumeId}")
    @ResponseBody
    public Object detail(@PathVariable("appYbconsumeId") Integer appYbconsumeId) {
        return appYbconsumeService.selectById(appYbconsumeId);
    }
}
