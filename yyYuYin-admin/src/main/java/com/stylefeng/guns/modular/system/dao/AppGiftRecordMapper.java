package com.stylefeng.guns.modular.system.dao;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.system.model.AppGiftRecord;

/**
 * @Description 礼物记录 Mapper 接口。
 * @author Grazer_Ma
 * @Date 2020-05-31 09:45:23
 */
public interface AppGiftRecordMapper extends BaseMapper<AppGiftRecord> {

	/**
	 * 获取礼物记录列表数据
	 * 
	 * @param appGiftRecord
	 * @return
	 */
	List<Map<String, Object>> getAppGiftRecordList(AppGiftRecord appGiftRecord, Page<Map<String, Object>> page);

}
