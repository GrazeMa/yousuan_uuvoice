package com.stylefeng.guns.modular.system.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.common.constant.factory.PageFactory;
import com.stylefeng.guns.modular.system.model.AppReport;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;
import com.stylefeng.guns.modular.system.model.AppGuild;
import com.stylefeng.guns.modular.system.service.IAppGuildLeaveService;

import java.util.List;
import java.util.Map;

/**
 * @Description 退会申请控制器。
 * @author Grazer_Ma
 * @Date 2020-05-14 23:31:30
 */
@Controller
@RequestMapping("/appGuildLeave")
public class AppGuildLeaveController extends BaseController {

    private String PREFIX = "/system/appGuildLeave/";

    @Autowired
    private IAppGuildLeaveService appGuildLeaveService;

    /**
     * 跳转到公会管理首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "appGuildLeave.html";
    }

    /**
     * 获取公会管理列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(AppGuild appGuild) {
        Page<Map<String,Object>> page = new PageFactory<Map<String,Object>>().defaultPage();
        List<Map<String, Object>> result = appGuildLeaveService.getAppGuildLeave(appGuild, page);
        page.setRecords(result);
        return super.packForBT(page);
    }
    
}
