package com.stylefeng.guns.modular.system.task.jobs;

import com.stylefeng.guns.modular.system.model.AppRoom;
import com.stylefeng.guns.modular.system.model.AppTimeTask;
import com.stylefeng.guns.modular.system.task.base.AbstractJob;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * 定时解禁讨论组
 *
 *
 */
public class RoomUnlockJob extends AbstractJob {

	public static final String name = "UnlockRoom_";

	@Override
	public void execute(JobExecutionContext context)
			throws JobExecutionException {
		JobDataMap maps = context.getMergedJobDataMap();
		Integer  roomId = maps.getInt("id");
		Integer timeTaskId = maps.getInt("timeTaskId");
		try{
			AppRoom appRoom=new AppRoom();
			appRoom.setId(roomId);
			appRoom.setStatus(1);//是否有效 1有效，2无效
			appRoomService.updateById(appRoom);
			//修改定时任务数据状态
			AppTimeTask timeTask=appTimeTaskService.selectById(timeTaskId);
			//0有效，1无效
			timeTask.setState(1);
			appTimeTaskService.updateById(timeTask);
			logger.debug("执行“定时解禁房间”完成：roomId={}",roomId);
		}catch(Exception e){
			logger.debug("执行“定时解禁房间”异常:roomId={}",roomId,e);
		}
	}

}
