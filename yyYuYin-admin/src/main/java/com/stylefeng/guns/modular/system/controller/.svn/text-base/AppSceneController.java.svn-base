package com.stylefeng.guns.modular.system.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.common.constant.factory.PageFactory;
import com.stylefeng.guns.core.util.OssUploadUtil;
import com.stylefeng.guns.core.util.SinataUtil;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;
import com.stylefeng.guns.modular.system.model.AppScene;
import com.stylefeng.guns.modular.system.service.IAppSceneService;

import java.util.*;

/**
 * 道具管理控制器
 * @Date 2019-03-01 09:41:01
 */
@Controller
@RequestMapping("/appScene")
public class AppSceneController extends BaseController {

    private String PREFIX = "/system/appScene/";

    @Autowired
    private IAppSceneService appSceneService;

    /**
     * 跳转到道具管理首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "appScene.html";
    }

    /**
     * 跳转到添加道具管理
     */
    @RequestMapping("/appScene_add")
    public String appSceneAdd() {
        return PREFIX + "appScene_add.html";
    }

    /**
     * 跳转到修改道具管理
     */
    @RequestMapping("/appScene_update/{appSceneId}")
    public String appSceneUpdate(@PathVariable Integer appSceneId, Model model) {
        AppScene appScene = appSceneService.selectById(appSceneId);
        model.addAttribute("item",appScene);
        if(appScene.getImg().contains(".svga")){//svga图片
            model.addAttribute("type",1);
        }else{
            model.addAttribute("type",2);
        }
        LogObjectHolder.me().set(appScene);
        return PREFIX + "appScene_edit.html";
    }

    /**
     * 获取道具管理列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(AppScene appScene) {
        Page<Map<String,Object>> page = new PageFactory<Map<String,Object>>().defaultPage();
        List<Map<String, Object>> result = appSceneService.getAppSceneList(appScene,page );
        page.setRecords(result);
        return super.packForBT(page);
    }

    /**
     * 新增道具管理
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(AppScene appScene) {
        appScene.setAddTime(new Date());
        appScene.setCreateTime(new Date());
        appSceneService.insert(appScene);
        return SUCCESS_TIP;
    }

    /**
     * 删除道具管理
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(String appSceneId) {
        EntityWrapper<AppScene> entityWrapper=new EntityWrapper<>();
        entityWrapper.in("id",appSceneId);
        AppScene appScene=new AppScene();
        appScene.setIsDelete(2);
        appSceneService.update(appScene,entityWrapper);
        List<AppScene> appSceneList=appSceneService.selectList(entityWrapper);
        if(appSceneList!=null &&  appSceneList.size()>0){
            for(AppScene appScene1:appSceneList){
                List<String> list=new ArrayList<>();
                if(SinataUtil.isNotEmpty(appScene1.getImg())){
                    list.add(appScene1.getImg());
                }
                if(SinataUtil.isNotEmpty(appScene1.getImgFm())){
                    list.add(appScene1.getImgFm());
                }
                OssUploadUtil.deleteObjects(list);
            }
        }
        return SUCCESS_TIP;
    }

    /**
     * 修改道具管理 编辑并且上架
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(AppScene appScene) {
        AppScene appScene1=appSceneService.selectById(appScene.getId());
        if(appScene1!=null){
            List<String> list=new ArrayList<>();
            if(SinataUtil.isNotEmpty(appScene1.getImg())){
                list.add(appScene1.getImg());
            }
            if(SinataUtil.isNotEmpty(appScene1.getImgFm())){
                list.add(appScene1.getImgFm());
            }
            OssUploadUtil.deleteObjects(list);
        }
       //appScene.setIsState(1);//1 销售 ,2 下架
        //appScene.setAddTime(new Date());
        appSceneService.updateById(appScene);
        return SUCCESS_TIP;
    }
    /**
     * 修改道具 上下架
     */
    @RequestMapping(value = "/updateXiajia")
    @ResponseBody
    public Object updateXiajia(String ids,Integer isState) {
        EntityWrapper<AppScene> entityWrapper=new EntityWrapper<>();
        entityWrapper.in("id",ids);
        AppScene appUser=new AppScene();
        appUser.setIsState(isState);
        if(isState==1){//需要上架
            appUser.setAddTime(new Date());
        }
        appSceneService.update(appUser,entityWrapper);
        return SUCCESS_TIP;
    }
    /**
     * 道具管理详情
     */
    @RequestMapping(value = "/detail/{appSceneId}")
    @ResponseBody
    public Object detail(@PathVariable("appSceneId") Integer appSceneId) {
        return appSceneService.selectById(appSceneId);
    }
    /**
     * 系统设置中，宝箱选择礼物后获取数据
     * @return
     */
    @RequestMapping(value = "/getSceneName")
    @ResponseBody
    public Object getSceneName(Integer state) {
        Map<String,Object> map=new HashMap<>();
        map.put("state",state);
        return appSceneService.getAppSceneName(map);
    }
}
