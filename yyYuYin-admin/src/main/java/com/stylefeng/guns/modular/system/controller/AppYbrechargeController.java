package com.stylefeng.guns.modular.system.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.util.SinataUtil;
import com.stylefeng.guns.modular.system.model.AppUser;
import com.stylefeng.guns.modular.system.service.IAppUserService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;
import com.stylefeng.guns.modular.system.model.AppYbrecharge;
import com.stylefeng.guns.modular.system.service.IAppYbrechargeService;

import java.util.Date;

/**
 * 优币充值控制器
 * @Date 2019-03-14 11:20:59
 */
@Controller
@RequestMapping("/appYbrecharge")
public class AppYbrechargeController extends BaseController {

    private String PREFIX = "/system/appYbrecharge/";

    @Autowired
    private IAppYbrechargeService appYbrechargeService;
    @Autowired
    private IAppUserService appUserService;

    /**
     * 跳转到优币充值首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "appYbrecharge.html";
    }

    /**
     * 跳转到添加优币充值
     */
    @RequestMapping("/appYbrecharge_add")
    public String appYbrechargeAdd() {
        return PREFIX + "appYbrecharge_add.html";
    }

    /**
     * 跳转到修改优币充值
     */
    @RequestMapping("/appYbrecharge_update/{appYbrechargeId}")
    public String appYbrechargeUpdate(@PathVariable Integer appYbrechargeId, Model model) {
        AppYbrecharge appYbrecharge = appYbrechargeService.selectById(appYbrechargeId);
        model.addAttribute("item",appYbrecharge);
        LogObjectHolder.me().set(appYbrecharge);
        return PREFIX + "appYbrecharge_edit.html";
    }

    /**
     * 获取优币充值列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(AppYbrecharge appYbrecharge,String beginTime,String endTime) {
        EntityWrapper<AppYbrecharge> entityWrapper=new EntityWrapper<>();
        if(SinataUtil.isNotEmpty(appYbrecharge.getEid())){
            entityWrapper.eq("eid",appYbrecharge.getEid());
        }
        if(SinataUtil.isNotEmpty(appYbrecharge.getName())){
            entityWrapper.like("name",appYbrecharge.getName());
        }
        if(SinataUtil.isNotEmpty(beginTime)){
            entityWrapper.ge("createTime",beginTime +" 00:00:00");
        }
        if(SinataUtil.isNotEmpty(endTime)){
            entityWrapper.le("createTime",endTime +" 23:59:59");
        }
        entityWrapper.eq("isDelete",1);
        entityWrapper.orderBy("createTime",false);
        return appYbrechargeService.selectList(entityWrapper);
    }

    /**
     * 新增优币充值
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(AppYbrecharge appYbrecharge) {
        AppUser appUser=appUserService.selectOne(new EntityWrapper<AppUser>().eq("usercoding",appYbrecharge.getEid()));
        if(appUser!=null){
            appYbrecharge.setName(appUser.getNickname());
            appYbrecharge.setUid(appUser.getId());
        }
        appYbrecharge.setSdGold(appYbrecharge.getGmGold()+appYbrecharge.getZsGold());
        appYbrecharge.setCreateTime(new Date());
        appYbrechargeService.insert(appYbrecharge);
        return SUCCESS_TIP;
    }

    /**
     * 删除优币充值
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer appYbrechargeId) {
        appYbrechargeService.deleteById(appYbrechargeId);
        return SUCCESS_TIP;
    }

    /**
     * 根据uuid用户是否存在
     * @param eid
     * @return
     */
    @RequestMapping(value = "/getUserByEid")
    @ResponseBody
    public Object getUserByEid(Integer eid) {
       Integer count= appUserService.selectCount(new EntityWrapper<AppUser>().eq("usercoding",eid));
        return count;
    }
    /**
     * 修改优币充值
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(AppYbrecharge appYbrecharge) {
        appYbrechargeService.updateById(appYbrecharge);
        return SUCCESS_TIP;
    }

    /**
     * 优币充值详情
     */
    @RequestMapping(value = "/detail/{appYbrechargeId}")
    @ResponseBody
    public Object detail(@PathVariable("appYbrechargeId") Integer appYbrechargeId) {
        return appYbrechargeService.selectById(appYbrechargeId);
    }
}
