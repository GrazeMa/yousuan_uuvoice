package com.stylefeng.guns.modular.system.service.impl;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.system.model.AppReport;
import com.stylefeng.guns.modular.system.dao.AppReportMapper;
import com.stylefeng.guns.modular.system.service.IAppReportService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 举报 服务实现类
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-13
 */
@Service
public class AppReportServiceImpl extends ServiceImpl<AppReportMapper, AppReport> implements IAppReportService {
    @Override
    public List<Map<String, Object>> getAppReportByUser(AppReport appReport,Page<Map<String,Object>> page) {
        return this.baseMapper.getAppReportByUser(appReport,page);
    }

    @Override
    public List<Map<String, Object>> getAppReportByRoom(AppReport appReport,Page<Map<String,Object>> page) {
        return this.baseMapper.getAppReportByRoom(appReport,page);
    }

    @Override
    public List<Map<String, Object>> getAppReportByMusic(AppReport appReport,Page<Map<String,Object>> page) {
        return this.baseMapper.getAppReportByMusic(appReport,page);
    }
}
