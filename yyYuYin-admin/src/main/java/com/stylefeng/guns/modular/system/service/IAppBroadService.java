package com.stylefeng.guns.modular.system.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.system.model.AppBroad;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 广播交友 服务类
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-12
 */
public interface IAppBroadService extends IService<AppBroad> {
    /**
     *  广播交友
     * @param appBroad
     * @param page
     * @return
     */
    List<Map<String,Object>> getAppBroad(AppBroad appBroad, Page page);
}
