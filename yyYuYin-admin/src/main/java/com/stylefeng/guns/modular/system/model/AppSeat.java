package com.stylefeng.guns.modular.system.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 房间座位相关设置
 * </p>
 *
 * @author stylefeng123
 * @since 2019-04-03
 */
@TableName("app_seat")
public class AppSeat extends Model<AppSeat> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     *  是否禁麦此座位  1否， 2 是
     */
    private Integer state;
    /**
     * 是否封锁此座位 1否， 2是
     */
    private Integer status;
    /**
     * 房间号
     */
    private String pid;
    /**
     * 用户id
     */
    private Integer uid;
    /**
     * 麦顺序
     */
    private Integer sequence;

    @TableField(exist = false)
    private UserModel userModel;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public Integer getSequence() {
        return sequence;
    }

    public void setSequence(Integer sequence) {
        this.sequence = sequence;
    }

    public UserModel getUserModel() {
        return userModel;
    }

    public void setUserModel(UserModel userModel) {
        this.userModel = userModel;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "AppSeat{" +
        "id=" + id +
        ", state=" + state +
        ", status=" + status +
        ", pid=" + pid +
        ", uid=" + uid +
        ", sequence=" + sequence +
        "}";
    }
}
