package com.stylefeng.guns.modular.system.dao;

import com.stylefeng.guns.modular.system.model.AppTreasure;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 房间财富值 Mapper 接口
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-11
 */
public interface AppTreasureMapper extends BaseMapper<AppTreasure> {
    /**
     * 获取财富榜
     * @param map
     * @return
     */
    List<Map<String,Object>> getAppTreasure(Map<String,Object> map);
}
