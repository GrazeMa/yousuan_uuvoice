package com.stylefeng.guns.modular.system.service.impl;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.system.model.AppGuild;
import com.stylefeng.guns.modular.system.dao.AppGuildLeaveMapper;
import com.stylefeng.guns.modular.system.service.IAppGuildLeaveService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
/**
 * @Description 退会申请实现类。
 * @author Grazer_Ma
 * @Date 2020-05-14 23:30:03
 */
@Service
public class AppGuildLeaveServiceImpl extends ServiceImpl<AppGuildLeaveMapper, AppGuild> implements IAppGuildLeaveService {
    @Override
    public List<Map<String, Object>> getAppGuildLeave(AppGuild appGuild, Page<Map<String,Object>> page) {
        return this.baseMapper.getAppGuildLeave(appGuild, page);
    }
}
