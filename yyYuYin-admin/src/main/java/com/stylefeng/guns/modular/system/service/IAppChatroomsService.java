package com.stylefeng.guns.modular.system.service;

import com.stylefeng.guns.modular.system.model.AppChatrooms;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 加入聊天室的 服务类
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-07
 */
public interface IAppChatroomsService extends IService<AppChatrooms> {
    /**
     * 获取加入聊天室的数据
     * @param objectMap
     * @return
     */
    List<Map<String,Object>> getAppChatrooms(Map<String,Object> objectMap);
}
