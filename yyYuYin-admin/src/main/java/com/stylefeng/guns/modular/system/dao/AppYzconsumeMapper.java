package com.stylefeng.guns.modular.system.dao;

import com.stylefeng.guns.modular.system.model.AppYzconsume;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 优钻获取记录 Mapper 接口
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-14
 */
public interface AppYzconsumeMapper extends BaseMapper<AppYzconsume> {

}
