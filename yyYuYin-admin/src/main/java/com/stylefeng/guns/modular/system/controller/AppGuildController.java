package com.stylefeng.guns.modular.system.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.common.constant.factory.PageFactory;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.core.log.LogObjectHolder;

import com.stylefeng.guns.modular.system.model.AppGuild;
import com.stylefeng.guns.modular.system.model.AppGuildGroup;
import com.stylefeng.guns.modular.system.service.IAppGuildGroupService;
import com.stylefeng.guns.modular.system.service.IAppGuildService;

import java.util.List;
import java.util.Map;

/**
 * @Description 公会管理控制器。
 * @author Grazer_Ma
 * @Date 2020-05-06 15:13:28
 */
@Controller
@RequestMapping("/appGuild")
public class AppGuildController extends BaseController {

    private String PREFIX = "/system/appGuild/";

    @Autowired
    private IAppGuildService appGuildService;
    @Autowired
    private IAppGuildGroupService appGuildGroupService;

    /**
     * 跳转到公会管理首页。
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "appGuild.html";
    }
    
    /**
     * 跳转到修改公会管理。
     */
    @RequestMapping("/appGuild_update/{appGuildId}")
    public String appGuildUpdate(@PathVariable Integer appGuildId, Model model) {
    	AppGuild appGuild = appGuildService.selectById(appGuildId);
        model.addAttribute("item", appGuild);
        LogObjectHolder.me().set(appGuild);
        return PREFIX + "appGuild_edit.html";
    }

    /**
     * 获取公会管理列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(AppGuild appGuild) {
        Page<Map<String,Object>> page = new PageFactory<Map<String,Object>>().defaultPage();
        List<Map<String, Object>> result = appGuildService.getAppGuild(appGuild, page);
        page.setRecords(result);
        return super.packForBT(page);
    }
    
    /**
     * 新增公会管理
     */
//    @RequestMapping(value = "/add")
//    @ResponseBody
//    public Object add(int uid, int gid) {
//    	System.out.println("uid: " + uid);
//    	System.out.println("gid: " + gid);
//    	AppGuild appGuild = new AppGuild();
//    	appGuild.setUid(uid);
//    	appGuild.setGid(gid);
//    	appGuild.setStatus(0);
//    	appGuild.setCreateDate(LocalDateTime.now().format(DateTimeFormatter.ofPattern("YYYY-MM-dd HH:mm:ss")));
//    	appGuildService.insert(appGuild);
//        return SUCCESS_TIP;
//    }
    
	/**
	 * 修改分组。
	 */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(AppGuild appGuild) {
    	System.out.println("appGuild: " + appGuild);
    	appGuildService.updateById(appGuild);
        return SUCCESS_TIP;
    }
    
	/**
	 * 踢出公会成员。
	 */
	@RequestMapping(value = "/delete")
	@ResponseBody
	public Object delete(String appGuildId) {
		EntityWrapper<AppGuild> entityWrapper = new EntityWrapper<>();
		entityWrapper.in("id", appGuildId);
		AppGuild appGuild = new AppGuild();
		appGuild.setStatus(2); // 状态（0：待审核；1：已审核；2：被踢出公会）。
		appGuildService.update(appGuild, entityWrapper);
		return SUCCESS_TIP;
	}
	
	/**
	 * 获取分组下拉列表信息。
	 */
	@RequestMapping(value = "/chooseGuild")
	@ResponseBody
	public List<AppGuildGroup> chooseGuild() {

		List<AppGuildGroup> appGuildGroups = appGuildGroupService
				.selectList(new EntityWrapper<AppGuildGroup>().eq("status", 1));
		return appGuildGroups;

	}
    
}
