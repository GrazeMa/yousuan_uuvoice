package com.stylefeng.guns.modular.system.service.impl;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.system.model.AppFeedback;
import com.stylefeng.guns.modular.system.dao.AppFeedbackMapper;
import com.stylefeng.guns.modular.system.service.IAppFeedbackService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 反馈 服务实现类
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-13
 */
@Service
public class AppFeedbackServiceImpl extends ServiceImpl<AppFeedbackMapper, AppFeedback> implements IAppFeedbackService {
    @Override
    public List<Map<String, Object>> getAppFeedback(AppFeedback appFeedback,Page<Map<String,Object>> page) {
        return this.baseMapper.getAppFeedback(appFeedback,page);
    }
}
