package com.stylefeng.guns.modular.system.dao;

import com.stylefeng.guns.modular.system.model.AppRecommend;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 推荐位置管理 Mapper 接口
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-12
 */
public interface AppRecommendMapper extends BaseMapper<AppRecommend> {
    /**
     * 获取推荐管理数据
     * @param map
     * @return
     */
    List<Map<String,Object>> getAppRecommend(Map map);
}
