package com.stylefeng.guns.modular.system.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 提现管理
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-14
 */
@TableName("app_withdraw")
public class AppWithdraw extends Model<AppWithdraw> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    private Date createTime;
    /**
     * 用户id
     */
    private Integer uid;
    /**
     * 用户名称
     */
    private String name;
    /**
     * 优优ID
     */
    private String eid;
    /**
     * 1 钻石，2 是分成
     */
    private Integer state;
    /**
     * 提现金额
     */
    private Double money;
    /**
     * 历史提现总金额
     */
    private Double historyMoney;
    /**
     * 提现姓名
     */
    private String witName;
    /**
     * 提现电话
     */
    private String witPhone;
    /**
     * 流水号
     */
    private String  serialNum;
    /**
     * 1 是待审核， 2已提现， 3已拒绝
     */
    private Integer status;
    /**
     * '是否被删除（1=否  2=是）'
     */
    private Integer isDelete;
    /**
     *提现消耗的优钻数
     */
    private Integer yzNum;

    /**
     * 时间查询条件(开始)
     */
    @TableField(exist = false)
    private String beginTime;
    /**
     * 时间查询条件(结束)
     */
    @TableField(exist = false)
    private String endTime;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEid() {
        return eid;
    }

    public void setEid(String eid) {
        this.eid = eid;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Double getMoney() {
        return money;
    }

    public void setMoney(Double money) {
        this.money = money;
    }

    public Double getHistoryMoney() {
        return historyMoney;
    }

    public void setHistoryMoney(Double historyMoney) {
        this.historyMoney = historyMoney;
    }

    public String getWitName() {
        return witName;
    }

    public void setWitName(String witName) {
        this.witName = witName;
    }

    public String getWitPhone() {
        return witPhone;
    }

    public void setWitPhone(String witPhone) {
        this.witPhone = witPhone;
    }

    public String getSerialNum() {
        return serialNum;
    }

    public void setSerialNum(String serialNum) {
        this.serialNum = serialNum;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    public String getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(String beginTime) {
        this.beginTime = beginTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public Integer getYzNum() {
        return yzNum;
    }

    public void setYzNum(Integer yzNum) {
        this.yzNum = yzNum;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "AppWithdraw{" +
        "id=" + id +
        ", createTime=" + createTime +
        ", uid=" + uid +
        ", name=" + name +
        ", eid=" + eid +
        ", state=" + state +
        ", money=" + money +
        ", historyMoney=" + historyMoney +
        ", witName=" + witName +
        ", witPhone=" + witPhone +
        ", serialNum=" + serialNum +
        ", status=" + status +
        ", isDelete=" + isDelete +
        "}";
    }
}
