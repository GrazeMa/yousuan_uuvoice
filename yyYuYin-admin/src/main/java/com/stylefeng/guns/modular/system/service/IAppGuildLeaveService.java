package com.stylefeng.guns.modular.system.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.system.model.AppGuild;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;
import java.util.Map;

/**
 * @Description 退会申请服务类。
 * @author Grazer_Ma
 * @Date 2020-05-14 23:28:38
 */
public interface IAppGuildLeaveService extends IService<AppGuild> {
    /**
     * 获取退会申请信息。
     * @param appGuild
     * @return
     */
    List<Map<String,Object>> getAppGuildLeave(AppGuild appGuild, Page<Map<String, Object>> page);
}
