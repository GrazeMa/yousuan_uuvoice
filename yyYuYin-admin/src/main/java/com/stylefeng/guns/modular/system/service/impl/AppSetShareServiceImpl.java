package com.stylefeng.guns.modular.system.service.impl;

import com.stylefeng.guns.modular.system.model.AppSetShare;
import com.stylefeng.guns.modular.system.dao.AppSetShareMapper;
import com.stylefeng.guns.modular.system.service.IAppSetShareService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 分享赠送 服务实现类
 * </p>
 *
 * @author stylefeng123
 * @since 2019-02-28
 */
@Service
public class AppSetShareServiceImpl extends ServiceImpl<AppSetShareMapper, AppSetShare> implements IAppSetShareService {

}
