package com.stylefeng.guns.modular.system.dao;

import com.stylefeng.guns.modular.system.model.AppWithdraw;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 提现管理 Mapper 接口
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-14
 */
public interface AppWithdrawMapper extends BaseMapper<AppWithdraw> {
    /**
     * 获取提现列表数据
     * @param appWithdraw
     * @return
     */
    List<Map<String,Object>> getAppWithdraw(AppWithdraw appWithdraw);
}
