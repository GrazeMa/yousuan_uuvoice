package com.stylefeng.guns.modular.system.controller;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.config.properties.GunsProperties;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.common.constant.factory.PageFactory;
import com.stylefeng.guns.core.util.DateUtil;
import com.stylefeng.guns.core.util.Jpush;
import com.stylefeng.guns.core.util.SinataUtil;
import com.stylefeng.guns.modular.system.model.*;
import com.stylefeng.guns.modular.system.service.*;
import com.stylefeng.guns.modular.system.task.base.QuartzManager;
import com.stylefeng.guns.modular.system.task.base.TimeJobType;
import com.stylefeng.guns.modular.system.task.jobs.RoomUnlockJob;
import com.stylefeng.guns.modular.system.task.jobs.UserUnlockJob;
import io.agora.signal.Signal;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.*;

/**
 * 举报管理控制器
 * @Date 2019-03-13 11:16:37
 */
@Controller
@RequestMapping("/appReport")
public class AppReportController extends BaseController {

    private String PREFIX = "/system/appReport/";
    @Autowired
    private GunsProperties gunsProperties;
    @Autowired
    private IAppReportService appReportService;
    @Autowired
    private IAppTimeTaskService appTimeTaskService;
    @Autowired
    private IAppUserService appUserService;
    @Autowired
    private IAppRoomService appRoomService;
    @Autowired
    private IAppMusicService appMusicService;
    @Autowired
    private IAppChatroomsService appChatroomsService;
    /**
     * 跳转到举报管理首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "appReport.html";
    }

    /**
     * 跳转到添加举报管理
     */
    @RequestMapping("/appReport_add")
    public String appReportAdd() {
        return PREFIX + "appReport_add.html";
    }

    /**
     * 跳转到修改举报管理
     */
    @RequestMapping("/appReport_update/{appReportId}")
    public String appReportUpdate(@PathVariable Integer appReportId, Model model) {
        AppReport appReport = appReportService.selectById(appReportId);
        model.addAttribute("item",appReport);
        LogObjectHolder.me().set(appReport);
        return PREFIX + "appReport_edit.html";
    }

    /**
     * 获取举报管理列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(AppReport appReport) {
        if(SinataUtil.isEmpty(appReport.getRole())){
            appReport.setRole(1);
        }
        Page<Map<String,Object>> page = new PageFactory<Map<String,Object>>().defaultPage();
        List<Map<String,Object>> reList=new ArrayList<>();
        if(appReport.getRole()==1){//用户举报
            reList =  appReportService.getAppReportByUser(appReport,page);
        }else if(appReport.getRole()==2){//房间举报
            reList =  appReportService.getAppReportByRoom(appReport,page);
        }else if (appReport.getRole()==3){//音乐举报
            reList =  appReportService.getAppReportByMusic(appReport,page);
        }
        page.setRecords(reList);
        return super.packForBT(page);
    }

    /**
     * 新增举报管理
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(AppReport appReport) {
        appReportService.insert(appReport);
        return SUCCESS_TIP;
    }

    /**
     * 删除举报管理
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(String appReportId) {
        EntityWrapper<AppReport> entityWrapper=new EntityWrapper<>();
        entityWrapper.in("id",appReportId);
        entityWrapper.eq("status",2);//已经处理了的
        AppReport appReport=new AppReport();
        appReport.setIsDelete(2);
        appReportService.update(appReport,entityWrapper);
        return SUCCESS_TIP;
    }

    /**
     * 修改举报管理
     * @param   -1不冻结，1：1天，2：2天，7：7天，0永久
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    @Transactional
    public Object update(AppReport appReport,Integer isXj) {
        //lockDays  -1不冻结，1：1天，2：2天，7：7天，0永久
        Integer day=0;
        appReportService.updateById(appReport);
        appReport=appReportService.selectById(appReport.getId());
        if(appReport.getRole()==3){//音乐处理
            if(isXj==2){
                AppMusic appMusic=new AppMusic();
                appMusic.setId(appReport.getBuid());
                appMusic.setStatus(isXj);//是否上架，1上架，2下架
                appMusicService.updateById(appMusic);
            }
        }else {
            if (appReport.getLockDays() >= 0) {
                day = appReport.getLockDays();
                if (appReport.getRole() == 1) {//用户
                    //修改用户的状态
                    AppUser appUser = new AppUser();
                    appUser.setId(appReport.getBuid());
                    appUser.setState(2);//是否冻结 1否,2是
                    appUserService.updateById(appUser);
                    if (day > 0) {
                        //添加定时任务信息
                        AppTimeTask timeTask = new AppTimeTask();
                        timeTask.setCreateDate(new Date());
                        timeTask.setExcuteDate(DateUtil.addDate(new Date(), day));
                        timeTask.setTaskName((UserUnlockJob.name + appReport.getBuid()).toUpperCase());
                        timeTask.setTypeName(UserUnlockJob.name);
                        timeTask.setMap("id:" + appReport.getBuid());
                        appTimeTaskService.insert(timeTask);
                        //添加定时任务
                        Map<String, Object> paMap = new HashMap<>();
                        paMap.put("id", appReport.getBuid());
                        paMap.put("timeTaskId", timeTask.getId());
                        QuartzManager.addJob(UserUnlockJob.class,
                                (UserUnlockJob.name + appReport.getBuid()).toUpperCase(),
                                TimeJobType.UNLOCK, DateUtil.addDate(new Date(), day),
                                paMap);
                    }
                    /*禁用用户需要极光推送*/
                    //==================循环极光推送配送员
                    Map map = new HashMap<>();
                    map.put("state", "6");//禁用id
                    map.put("id", appUser.getId());//禁用id
                    Jpush.push( appUser.getId().toString(),  "该账号已被禁用" ,map);
                } else if (appReport.getRole() == 2) {//房间
                    //修改用户的状态
                    AppRoom appRoom = new AppRoom();
                    appRoom.setId(appReport.getBuid());
                    appRoom.setStatus(2);////是否有效 1有效，2无效
                    appRoomService.updateById(appRoom);
                    if (day > 0) {
                        //添加定时任务信息
                        AppTimeTask timeTask = new AppTimeTask();
                        timeTask.setCreateDate(new Date());
                        timeTask.setExcuteDate(DateUtil.addDate(new Date(), day));
                        timeTask.setTaskName((RoomUnlockJob.name + appReport.getBuid()).toUpperCase());
                        timeTask.setTypeName(RoomUnlockJob.name);
                        timeTask.setMap("id:" + appReport.getBuid());
                        appTimeTaskService.insert(timeTask);
                        //添加定时任务
                        Map<String, Object> paMap = new HashMap<>();
                        paMap.put("id", appReport.getBuid());
                        paMap.put("timeTaskId", timeTask.getId());
                        QuartzManager.addJob(RoomUnlockJob.class,
                                (RoomUnlockJob.name + appReport.getBuid()).toUpperCase(),
                                TimeJobType.UNLOCK, DateUtil.addDate(new Date(), day),
                                paMap);
                    }
                    /*禁用用户需要声网推送房间属性变化*/
                    AppRoom appRoom_=appRoomService.selectById(appReport.getBuid());
                    appRoom_.setStatus(2);
                    /*改变这个房间的数据信息1这个房间用户要退出来，2在麦上的要下麦然后退出来,3群主也要退出来*/
                    AppChatrooms appChatrooms=new AppChatrooms();
                    appChatrooms.setState(1);//是否上麦 1否，2 是
                    appChatrooms.setStatus(2);//是否在房间 1在，2 不在
                    appChatroomsService.update(appChatrooms,new EntityWrapper<AppChatrooms>().eq("pid",appRoom_.getRid()));
                    //修改房主状态
                    appRoom_.setIsfz(2);//房主是否在房间 1 在， 2不在
                    appRoomService.updateById(appRoom_);
                    /*推送*/
                    Signal sig = new Signal(gunsProperties.getSappId());
                    Signal.LoginSession loginSession=sig.login("1", "_no_need_token", new Signal.LoginCallback());
                    Signal.LoginSession.Channel channel=loginSession.channelJoin(appRoom_.getRid(), new Signal.ChannelCallback());

                    String string = JSONObject.toJSONString(appRoom_);
                    channel.channelSetAttr("attr_xgfj",string);
                }
            }
        }

        return SUCCESS_TIP;
    }

    /**
     * 举报管理详情
     */
    @RequestMapping(value = "/detail/{appReportId}")
    @ResponseBody
    public Object detail(@PathVariable("appReportId") Integer appReportId) {
        return appReportService.selectById(appReportId);
    }
}
