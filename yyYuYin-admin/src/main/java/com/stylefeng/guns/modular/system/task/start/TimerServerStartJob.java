package com.stylefeng.guns.modular.system.task.start;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.stylefeng.guns.core.util.DateUtil;
import com.stylefeng.guns.modular.system.model.AppAgreement;
import com.stylefeng.guns.modular.system.model.AppTimeTask;
import com.stylefeng.guns.modular.system.service.IAppTimeTaskService;
import com.stylefeng.guns.modular.system.task.base.QuartzManager;
import com.stylefeng.guns.modular.system.task.base.TimeJobType;
import com.stylefeng.guns.modular.system.task.jobs.AgreementJob;
import com.stylefeng.guns.modular.system.task.jobs.RoomUnlockJob;
import com.stylefeng.guns.modular.system.task.jobs.SceneAndGiftJob;
import com.stylefeng.guns.modular.system.task.jobs.UserUnlockJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 主要用于定时任务在服务器重新启动时添加任务
 * @author Leeyns
 *
 */
@Component
public class TimerServerStartJob {
	private Logger logger = LoggerFactory.getLogger(TimerServerStartJob.class);
	
	@Autowired
	private IAppTimeTaskService appTimeTaskService;


	public TimerServerStartJob(){
		//服务器重新启动时，重新加载定时任务
		start();
	}
	
	public void start(){
		new Thread(new Runnable() {
			@Override
			public void run() {
				logger.debug("==>> Start timer job.");
				//先暂停15ms
				try {
					Thread.sleep(15000);
					//有效定时任务需要重新执行
					isUnlock();
					Thread.sleep(15000);

					System.out.println("定时启动了");
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				
				logger.debug("==>> Stop all timer job.");
			}
		}).start();
	}

	/**
	 * //有效定时任务需要重新执行
	 */
	public void isUnlock(){

		EntityWrapper<AppTimeTask> timerTaskEntityWrapper=new EntityWrapper<>();
		//0有效，1无效
		timerTaskEntityWrapper.eq("state",0);
		//还没到时间点的
		timerTaskEntityWrapper.ge("excuteDate",DateUtil.format(new Date(),"yyyy-MM-dd HH:mm:ss"));
		timerTaskEntityWrapper.like("typeName","Unlock").and("(typeName like '%SceneAndGiftJob_%' or typeName like '%AgreementJob_%')");
		/*//还没到时间点的
		timerTaskEntityWrapper.ge("excuteDate",DateUtil.format(new Date(),"yyyy-MM-dd HH:mm:ss"));*/
		List<AppTimeTask> timeTasks=appTimeTaskService.selectList(timerTaskEntityWrapper);
		for(AppTimeTask appTimeTask:timeTasks){
			Map<String,Object> paMap=new HashMap<>();
			String map=appTimeTask.getMap();
			paMap.put("timeTaskId",appTimeTask.getId());
			if(appTimeTask.getTypeName().equals("UnlockRoom_")){
				String[] maps=map.split(":");
				paMap.put("id",maps[1]);
				//房间
				QuartzManager.addJob(RoomUnlockJob.class,appTimeTask.getTaskName(),
						TimeJobType.UNLOCK,appTimeTask.getExcuteDate() ,
						paMap);

			}else if(appTimeTask.getTypeName().equals("UnlockUser_")){
				String[] maps=map.split(":");
				paMap.put("id",maps[1]);
				//用户
				QuartzManager.addJob(UserUnlockJob.class,
						appTimeTask.getTaskName(),
						TimeJobType.UNLOCK,appTimeTask.getExcuteDate() ,
						paMap);
			}else if(appTimeTask.getTypeName().equals("SceneAndGiftJob_")){//礼物和道具
				String[] par=map.split("_");
				paMap.put("id",par[0]);
				paMap.put("type",par[1]);
				QuartzManager.addJob(SceneAndGiftJob.class,
						appTimeTask.getTaskName(),
						TimeJobType.UNLOCK,appTimeTask.getExcuteDate() ,
						paMap);
			}else if(appTimeTask.getTypeName().equals("AgreementJob_")){//协议号
				paMap.put("id",appTimeTask.getMap());
				QuartzManager.addJob(AgreementJob.class,
						appTimeTask.getTaskName(),
						TimeJobType.UNLOCK,appTimeTask.getExcuteDate() ,
						paMap);
			}
		}
	}
}
