package com.stylefeng.guns.modular.system.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 图片
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-02
 */
@TableName("app_images")
public class AppImages extends Model<AppImages> {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 1评价图片,2反馈图片,3 用户上传图片,4 圈贴,5 悬赏
     */
    private Integer type;
    /**
     * 关联id（如评价id/反馈id/用户id/圈贴id/悬赏id）
     */
    private Integer relevanceId;
    /**
     * 图片路径
     */
    private String url;
    /**
     * 添加时间
     */
    private Date createDate;
    /**
     * 用户ID
     */
    private Integer uid;
    /**
     * 1 否,2 是
     */
    private Integer isDelete;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getRelevanceId() {
        return relevanceId;
    }

    public void setRelevanceId(Integer relevanceId) {
        this.relevanceId = relevanceId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "AppImages{" +
        "id=" + id +
        ", type=" + type +
        ", relevanceId=" + relevanceId +
        ", url=" + url +
        ", createDate=" + createDate +
        ", uid=" + uid +
        ", isDelete=" + isDelete +
        "}";
    }
}
