package com.stylefeng.guns.modular.system.dao;

import com.stylefeng.guns.modular.system.model.AppCharm;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 房间魅力榜 Mapper 接口
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-11
 */
public interface AppCharmMapper extends BaseMapper<AppCharm> {
    /**
     * 获取魅力榜
     * @param map
     * @return
     */
    List<Map<String,Object>> getAppCharm(Map<String,Object> map);
}
