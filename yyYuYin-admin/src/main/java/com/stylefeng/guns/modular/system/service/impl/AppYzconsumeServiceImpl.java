package com.stylefeng.guns.modular.system.service.impl;

import com.stylefeng.guns.modular.system.model.AppYzconsume;
import com.stylefeng.guns.modular.system.dao.AppYzconsumeMapper;
import com.stylefeng.guns.modular.system.service.IAppYzconsumeService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 优钻获取记录 服务实现类
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-14
 */
@Service
public class AppYzconsumeServiceImpl extends ServiceImpl<AppYzconsumeMapper, AppYzconsume> implements IAppYzconsumeService {

}
