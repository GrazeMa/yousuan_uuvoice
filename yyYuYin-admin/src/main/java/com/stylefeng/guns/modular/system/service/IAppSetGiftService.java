package com.stylefeng.guns.modular.system.service;

import com.stylefeng.guns.modular.system.model.AppSetGift;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 礼物设置 服务类
 * </p>
 *
 * @author stylefeng123
 * @since 2019-02-28
 */
public interface IAppSetGiftService extends IService<AppSetGift> {
    /**
     *获取系统设置礼物和宝箱数据
     * @param map
     * @return
     */
    List<Map<String,Object>> getAppSetGiftAndBox(Map<String,Object> map);
}
