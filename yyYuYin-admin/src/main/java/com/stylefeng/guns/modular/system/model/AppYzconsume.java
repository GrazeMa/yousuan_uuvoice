package com.stylefeng.guns.modular.system.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 优钻获取记录
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-14
 */
@TableName("app_yzconsume")
public class AppYzconsume extends Model<AppYzconsume> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    private Date createTime;
    private Integer uid;
    private String name;
    /**
     * 1是赠送礼物，2是购买花环，3购买座驾，4 发红包
     */
    private Integer state;
    /**
     * 礼物名称
     */
    private String lwName;
    /**
     * 消费数量
     */
    private Integer consumeNum;
    private Integer consumeYzNum;
    /**
     * '是否被删除（1=否  2=是）'
     */
    private Integer isDelete;

    private Integer eid;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getLwName() {
        return lwName;
    }

    public void setLwName(String lwName) {
        this.lwName = lwName;
    }

    public Integer getConsumeNum() {
        return consumeNum;
    }

    public void setConsumeNum(Integer consumeNum) {
        this.consumeNum = consumeNum;
    }

    public Integer getConsumeYzNum() {
        return consumeYzNum;
    }

    public void setConsumeYzNum(Integer consumeYzNum) {
        this.consumeYzNum = consumeYzNum;
    }

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    public Integer getEid() {
        return eid;
    }

    public void setEid(Integer eid) {
        this.eid = eid;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "AppYzconsume{" +
        "id=" + id +
        ", createTime=" + createTime +
        ", uid=" + uid +
        ", name=" + name +
        ", state=" + state +
        ", lwName=" + lwName +
        ", consumeNum=" + consumeNum +
        ", consumeYzNum=" + consumeYzNum +
        ", isDelete=" + isDelete +
        "}";
    }
}
