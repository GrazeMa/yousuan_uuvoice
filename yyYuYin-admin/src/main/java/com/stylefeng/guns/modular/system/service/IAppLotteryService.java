package com.stylefeng.guns.modular.system.service;

import com.stylefeng.guns.modular.system.model.AppLottery;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 宝箱设置 （第一条数据保存到礼物设置里面） 服务类
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-11
 */
public interface IAppLotteryService extends IService<AppLottery> {

}
