package com.stylefeng.guns.modular.system.service;

import com.stylefeng.guns.modular.system.model.AppRoom;
import com.stylefeng.guns.modular.system.model.AppYbconsume;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 优币消费记录 服务类
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-14
 */
public interface IAppYbconsumeService extends IService<AppYbconsume> {
    /**
     * 获取房间流水数据
     * @param appRoom
     * @return
     */
    List<Map<String,Object>> getAppYbconsume(AppRoom appRoom);
}
