package com.stylefeng.guns.modular.system.service.impl;

import com.stylefeng.guns.modular.system.model.AppYbexchange;
import com.stylefeng.guns.modular.system.dao.AppYbexchangeMapper;
import com.stylefeng.guns.modular.system.service.IAppYbexchangeService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 优币兑换记录 服务实现类
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-14
 */
@Service
public class AppYbexchangeServiceImpl extends ServiceImpl<AppYbexchangeMapper, AppYbexchange> implements IAppYbexchangeService {

}
