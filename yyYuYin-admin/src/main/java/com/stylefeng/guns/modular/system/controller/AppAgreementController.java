package com.stylefeng.guns.modular.system.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.config.properties.GunsProperties;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.common.constant.factory.PageFactory;
import com.stylefeng.guns.core.util.DateUtil;
import com.stylefeng.guns.core.util.SinataUtil;
import com.stylefeng.guns.modular.system.model.*;
import com.stylefeng.guns.modular.system.service.*;
import net.sf.json.JSONArray;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.ui.Model;
import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.core.log.LogObjectHolder;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

import io.agora.signal.Signal;
import io.agora.signal.Signal.LoginSession;
import io.agora.signal.Signal.LoginSession.Channel;

/**
 * 协议号管理控制器
 * @Date 2019-03-01 14:22:15
 */
@Controller
@RequestMapping("/appAgreement")
public class AppAgreementController extends BaseController {

    private String PREFIX = "/system/appAgreement/";


    @Autowired
    private GunsProperties gunsProperties;
    @Autowired
    private IAppAgreementService appAgreementService;
    @Autowired
    private IAppRoomService appRoomService;
    @Autowired
    private IAppChatroomsService appChatroomsService;
    @Autowired
    private IAppSeatService appSeatService;
    @Autowired
    private IAppUserService appUserService;


    /**
     * 跳转到协议号管理首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "appAgreement.html";
    }

    /**
     * 跳转到添加协议号管理
     */
    @RequestMapping("/appAgreement_add")
    public String appAgreementAdd() {
        return PREFIX + "appAgreement_add.html";
    }

    /**
     * 跳转到修改协议号管理
     */
    @RequestMapping("/appAgreement_update/{appAgreementId}")
    public String appAgreementUpdate(@PathVariable Integer appAgreementId, Model model) {
        AppAgreement appAgreement = appAgreementService.selectById(appAgreementId);
        model.addAttribute("item",appAgreement);
        String date=DateUtil.formatDate(appAgreement.getDateOfBirth(),"yyyy-MM-dd");
        model.addAttribute("date",date);
        LogObjectHolder.me().set(appAgreement);
        return PREFIX + "appAgreement_edit.html";
    }

    /**
     * 获取协议号管理列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(AppAgreement appAgreement,String beginTime,String endTime) {
        Wrapper appAgreementEntityWrapper=new EntityWrapper<AppAgreement>();
        /*当前房间号*/
        if(SinataUtil.isNotEmpty(appAgreement.getEid())){
            appAgreementEntityWrapper.eq("eid",appAgreement.getEid());
        }
        if(SinataUtil.isNotEmpty(appAgreement.getUsercoding())){
            appAgreementEntityWrapper.like("usercoding",appAgreement.getUsercoding());
        }
        /*state 是否分配 1否，2 是*/
        if(SinataUtil.isNotEmpty(appAgreement.getState())){
            appAgreementEntityWrapper.eq("state",appAgreement.getState());
        }
        if(SinataUtil.isNotEmpty(beginTime)){
            appAgreementEntityWrapper.ge("createTime",beginTime +" 00:00:00");
        }
        if(SinataUtil.isNotEmpty(endTime)){
            appAgreementEntityWrapper.le("createTime",endTime +" 23:59:59");
        }
        Page<AppAgreement> page = new PageFactory<AppAgreement>().defaultPage();
        appAgreementEntityWrapper.eq("isDelete",1).orderBy("createTime",false);
        page.setRecords(appAgreementService.selectMapsPage(page, appAgreementEntityWrapper).getRecords());
        return super.packForBT(page);
    }

    /**
     * 新增协议号管理
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(AppAgreement appAgreement) {
        appAgreementService.insert(appAgreement);
        return SUCCESS_TIP;
    }

    /**
     * 删除协议号管理(只删除没有分配的协议号)
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(String appAgreementId) {
        //修改房间里的信息 （协议号数量相应减少）
        EntityWrapper<AppAgreement> appAgreementEntityWrapper=new EntityWrapper<>();
        appAgreementEntityWrapper.in("id",appAgreementId);
        appAgreementEntityWrapper.eq("state",1);//是否分配 1否，2 是
        List<AppAgreement> appAgreementList=appAgreementService.selectList(appAgreementEntityWrapper);
        for(AppAgreement appAgreement1:appAgreementList){
            if(appAgreement1.getState()==2){
                AppRoom appRoom=appRoomService.selectById(appAgreement1.getEid());
                appRoom.setProtocolNum(appRoom.getProtocolNum()-1);
                appRoomService.updateById(appRoom);
            }
        }
        //删除协议号
        AppAgreement appAgreement=new AppAgreement();
        appAgreement.setIsDelete(2);
        appAgreementService.update(appAgreement,appAgreementEntityWrapper);
        return SUCCESS_TIP;
    }

    /**
     * 召回协议号
     * @param appAgreementId 协议id
     * @return
     */
    @RequestMapping(value = "/income")
    @ResponseBody
    @Transactional
    public Object income(String appAgreementId) {
        //修改房间里的信息 （协议号数量相应减少）
        EntityWrapper<AppAgreement> appAgreementEntityWrapper=new EntityWrapper<>();
        appAgreementEntityWrapper.in("id",appAgreementId);
        List<AppAgreement> appAgreementList=appAgreementService.selectList(appAgreementEntityWrapper);
        for(AppAgreement appAgreement1:appAgreementList){
            if(appAgreement1.getState()==2){
                AppRoom appRoom=appRoomService.selectOne(new EntityWrapper<AppRoom>().eq("rid",appAgreement1.getEid()));
                if(appRoom!=null){
                    appRoom.setProtocolNum(appRoom.getProtocolNum()-1);
                    appRoomService.updateById(appRoom);
                }
            }
        }
        //修改协议号数据
        Map<String,Object> map=new HashMap<>();
        map.put("ids",appAgreementId);
        appAgreementService.updateIncome(map);
        //删除聊天室的数据
        appChatroomsService.delete(new EntityWrapper<AppChatrooms>().in("uid",appAgreementId).eq("isAgreement",2));//是否是协议号，1否，2是
        //修改聊天室的数据（在麦上的需要推送的信息）
        /*Map<String,Object> reMap=new HashMap<>();
        reMap.put("type",4);//协议号
        reMap.put("uids",appAgreementId);
        //reMap.put("state",2);//是否上麦 1否，2 是
        List<Map<String,Object>> chatrooms=appChatroomsService.getAppChatrooms(reMap);
        for(Map<String,Object> appChatrooms:chatrooms){//在麦上的需要推送
            this.tuisong(appChatrooms.get("pid").toString());
        }*/
        /*EntityWrapper<AppChatrooms> chatroomsEntityWrapper=new EntityWrapper<>();
        chatroomsEntityWrapper.in("uid",appAgreementId).eq("type",4);
        AppChatrooms appChatrooms=new AppChatrooms();
        appChatrooms.setState(1);//下麦
        appChatrooms.setSequence(0);
        appChatrooms.setStatus(2);//是否在房间 1在，2 不再
        appChatroomsService.update(appChatrooms,chatroomsEntityWrapper);//类型 1是房主，2 是管理员，3用户 4 协议用户*/
        return SUCCESS_TIP;
    }

    /**
     * 修改协议号管理
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(AppAgreement appAgreement) {
        appAgreementService.updateById(appAgreement);
        return SUCCESS_TIP;
    }

    /**
     * 协议号管理详情
     */
    @RequestMapping(value = "/detail/{appAgreementId}")
    @ResponseBody
    public Object detail(@PathVariable("appAgreementId") Integer appAgreementId) {
        return appAgreementService.selectById(appAgreementId);
    }
    /**
     * 协议号导入
     * @return
     */
    @RequestMapping(value="/import",method = RequestMethod.POST)
    @ResponseBody
   public Object upload(HttpServletRequest request){
        MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
        MultipartFile file = (MultipartFile) multipartRequest.getFile("myfile");
        boolean a = false;
        String fileName = file.getOriginalFilename();
        try {
            a = appAgreementService.batchImport(fileName, file);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return a;
    }

    /**
     * 召回协议号推送信息
     * @return
     */
    public void tuisong(String pid){
        List<AppSeat> list = appSeatService.selectList(new EntityWrapper<AppSeat>().eq("pid",pid));
        Iterator<AppSeat> iterator = list.iterator();
        while (iterator.hasNext()) {
            AppSeat eva=iterator.next();
            EntityWrapper<AppChatrooms> chatroomsEntityWrapper=new EntityWrapper<>();
            chatroomsEntityWrapper.eq("pid",pid);
            chatroomsEntityWrapper.eq("state",2);
            chatroomsEntityWrapper.eq("sequence",eva.getSequence());
            AppChatrooms l = appChatroomsService.selectOne(chatroomsEntityWrapper);
            if(SinataUtil.isNotEmpty(l)){
                AppUser byID = appUserService.selectById(l.getUid());
                UserModel u1=new UserModel();
                if(byID!=null){
                    u1.setId(byID.getId());
                    u1.setImg(byID.getImgTx());
                    u1.setName(byID.getNickname());
                    u1.setSex(byID.getSex());
                    u1.setSequence(l.getSequence());
                    u1.setState(l.getState());
                    u1.setType(l.getType());
                    eva.setUserModel(u1);
                }
            }
        }
        Signal sig = new Signal(gunsProperties.getSappId());

        LoginSession loginSession=sig.login("1", "_no_need_token", new Signal.LoginCallback());
        Channel channel=loginSession.channelJoin(pid, new Signal.ChannelCallback());

        String json = JSONArray.fromObject(list).toString();
        channel.channelSetAttr("attr_mics",json);
    }

}
