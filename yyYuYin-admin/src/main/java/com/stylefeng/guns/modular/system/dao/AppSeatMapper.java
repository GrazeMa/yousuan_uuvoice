package com.stylefeng.guns.modular.system.dao;

import com.stylefeng.guns.modular.system.model.AppSeat;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 房间座位相关设置 Mapper 接口
 * </p>
 *
 * @author stylefeng123
 * @since 2019-04-03
 */
public interface AppSeatMapper extends BaseMapper<AppSeat> {

}
