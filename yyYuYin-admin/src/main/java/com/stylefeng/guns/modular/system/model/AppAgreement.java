package com.stylefeng.guns.modular.system.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;

/**
 * <p>
 * 协议号
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-01
 */
@TableName("app_agreement")
public class AppAgreement extends Model<AppAgreement> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    private Date createTime;
    /**
     * 用户优优号
     */
    private String usercoding;
    /**
     * 个性签名
     */
    private String individuation;
    /**
     * 性别(1 男, 2 女)
     */
    private Integer sex;
    /**
     * 出生日期
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateOfBirth;
    /**
     * 昵称
     */
    private String nickname;
    /**
     * 当前房间id
     */
    private String eid;
    /**
     * 是否分配 1否，2 是
     */
    private Integer state;
    /**
     * '是否被删除（1=否  2=是）'
     */
    private Integer isDelete;
    /**
     * 头像
     */
    private String img;
    /**
     * 序列号
     */
    private Integer index;
    /**
     * 分配时间
     */
    private Date addTime;
    /**
     * 分配天数
     */
    private Integer days;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getUsercoding() {
        return usercoding;
    }

    public void setUsercoding(String usercoding) {
        this.usercoding = usercoding;
    }

    public String getIndividuation() {
        return individuation;
    }

    public void setIndividuation(String individuation) {
        this.individuation = individuation;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getEid() {
        return eid;
    }

    public void setEid(String eid) {
        this.eid = eid;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public Date getAddTime() {
        return addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    public Integer getDays() {
        return days;
    }

    public void setDays(Integer days) {
        this.days = days;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "AppAgreement{" +
        "id=" + id +
        ", createTime=" + createTime +
        ", usercoding=" + usercoding +
        ", individuation=" + individuation +
        ", sex=" + sex +
        ", dateOfBirth=" + dateOfBirth +
        ", nickname=" + nickname +
        ", eid=" + eid +
        ", state=" + state +
        ", isDelete=" + isDelete +
        "}";
    }
}
