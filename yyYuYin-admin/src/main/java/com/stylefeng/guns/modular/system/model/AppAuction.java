package com.stylefeng.guns.modular.system.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 房间竞拍排名
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-11
 */
@TableName("app_auction")
public class AppAuction extends Model<AppAuction> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    private Date createTime;
    /**
     * 房间id
     */
    private String rid;
    /**
     * 开始时间
     */
    private Date staTime;
    /**
     * 结算时间
     */
    private Date endTime;
    /**
     * '是否被删除（1=否  2=是）'
     */
    private Integer isDelete;
    /**
     * 1 是开启， 2是关闭
     */
    private Integer state;
    /**
     * 排序
     */
    @TableField(exist = false)
    private Integer ranks;
    @TableField(exist = false)
    private String staTime_;
    @TableField(exist = false)
    private String endTime_;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getRid() {
        return rid;
    }

    public void setRid(String rid) {
        this.rid = rid;
    }

    public Date getStaTime() {
        return staTime;
    }

    public void setStaTime(Date staTime) {
        this.staTime = staTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Integer getRanks() {
        return ranks;
    }

    public String getStaTime_() {
        return staTime_;
    }

    public void setStaTime_(String staTime_) {
        this.staTime_ = staTime_;
    }

    public String getEndTime_() {
        return endTime_;
    }

    public void setEndTime_(String endTime_) {
        this.endTime_ = endTime_;
    }

    public void setRanks(Integer ranks) {
        this.ranks = ranks;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "AppAuction{" +
        "id=" + id +
        ", createTime=" + createTime +
        ", rid=" + rid +
        ", staTime=" + staTime +
        ", endTime=" + endTime +
        ", isDelete=" + isDelete +
        ", state=" + state +
        "}";
    }
}
