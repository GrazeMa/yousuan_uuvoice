package com.stylefeng.guns.modular.system.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.system.dao.AppSettleTurnoverMapper;
import com.stylefeng.guns.modular.system.model.AppGuildSettle;
import com.stylefeng.guns.modular.system.service.IAppSettleTurnoverService;

/**
 * @Description 结算记录实现类。
 * @author Grazer_Ma
 * @Date 2020-05-20 20:43:18
 */
@Service
public class AppSettleTurnoverServiceImpl extends ServiceImpl<AppSettleTurnoverMapper, AppGuildSettle>
		implements IAppSettleTurnoverService {
	@Override
	public List<Map<String, Object>> getAppSettleTurnover(AppGuildSettle appGuildSettle,
			Page<Map<String, Object>> page) {
		return this.baseMapper.getAppSettleTurnover(appGuildSettle, page);
	}
}
