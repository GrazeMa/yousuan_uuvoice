package com.stylefeng.guns.modular.system.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.common.constant.factory.PageFactory;
import com.stylefeng.guns.core.shiro.ShiroKit;
import com.stylefeng.guns.core.util.Jpush;
import com.stylefeng.guns.core.util.MD5;
import com.stylefeng.guns.core.util.MD5Util;
import com.stylefeng.guns.core.util.SinataUtil;
import com.stylefeng.guns.modular.system.model.AppImages;
import com.stylefeng.guns.modular.system.model.AppRecord;
import com.stylefeng.guns.modular.system.service.IAppImagesService;
import com.stylefeng.guns.modular.system.service.IAppRecordService;
import com.stylefeng.guns.modular.system.warpper.AppUserWarpper;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;
import com.stylefeng.guns.modular.system.model.AppUser;
import com.stylefeng.guns.modular.system.service.IAppUserService;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 用户管理控制器
 * @Date 2019-03-01 14:01:34
 */
@Controller
@RequestMapping("/appUser")
public class AppUserController extends BaseController {

    private String PREFIX = "/system/appUser/";

    @Autowired
    private IAppUserService appUserService;
    @Autowired
    private IAppImagesService appImagesService;
    @Autowired
    private IAppRecordService appRecordService;

    /**
     * 跳转到用户管理首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "appUser.html";
    }

    /**
     * 跳转到添加用户管理
     */
    @RequestMapping("/appUser_add")
    public String appUserAdd() {
        return PREFIX + "appUser_add.html";
    }

    /**
     * 跳转到修改用户管理
     */
    @RequestMapping("/appUser_detail/{appUserId}")
    public String appUserUpdate(@PathVariable Integer appUserId, Model model) {
        Map<String,Object> map=new HashMap<String,Object>();
        map.put("id",appUserId);
        /*获取用户基本信息*/
        List<Map<String,Object>> appUserOne= appUserService.getAppUserOne(map);
        List<Map<String,Object>> appUserOnes=(List<Map<String,Object>>)new AppUserWarpper(appUserOne).warp();
        model.addAttribute("item",appUserOnes.get(0));
        /*获取用户相册*/
        List<AppImages> appImages=appImagesService.selectList(new EntityWrapper<AppImages>().eq("type",1).eq("relevanceId",appUserId));//1 用户上传图片(相册)
        model.addAttribute("appImages",appImages);
        model.addAttribute("appImagesCount",appImages.size());
        return PREFIX + "appUser_edit.html";
    }

    /**
     * 跳转到 1重置密码 2修改优币/优钻/推荐次数 页面
     * @param id 用户id
     * @param type_ 1重置密码 2修改优币/优钻/推荐次数
     * @return
     */
    @RequestMapping("/appUser_resetPwdOrupdate")
    public String appUserResetPwdOrupdate(Integer id,Integer type_,Model model) {
        model.addAttribute("uid",id);
        model.addAttribute("type_",type_);
        return PREFIX + "appUser_resetPwdOrupdate.html";
    }



    /**
     * 获取用户管理列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(AppUser appUser) {
        Page<Map<String,Object>> page = new PageFactory<Map<String,Object>>().defaultPage();
        List<Map<String, Object>> result = appUserService.getAppUserList(appUser,page );
        page.setRecords((List<Map<String,Object>>)new AppUserWarpper(result).warp());
        return super.packForBT(page);
    }

    /**
     * 新增用户管理
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(AppUser appUser) {
        appUserService.insert(appUser);
        return SUCCESS_TIP;
    }

    /**
     * 删除用户管理
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer appUserId) {
        appUserService.deleteById(appUserId);
        return SUCCESS_TIP;
    }

    /**
     * 修改用户管理
     * 1重置密码 2修改优币和优钻
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    @Transactional
    public Object update(AppUser appUser,Integer type_) {
        if(type_==1){//======修改密码
            /*修改密码*/
            try {
                appUser.setPassword(MD5.md5(appUser.getPassword()));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            /*添加操作记录*/
            AppRecord appRecord=new AppRecord();
            appRecord.setCreateTime(new Date());
            appRecord.setName(ShiroKit.getUser().getName());
            appRecord.setHand("重置了该用户密码");
            appRecord.setPid(appUser.getId());
            appRecord.setType(1);//1用户 2房间
            appRecordService.insert(appRecord);

        }else if(type_==2){//=========修改优币和优钻
            if(SinataUtil.isNotEmpty(appUser.getGold())){//修改了优币数
                /*添加操作记录*/
                AppRecord appRecord=new AppRecord();
                appRecord.setCreateTime(new Date());
                appRecord.setName(ShiroKit.getUser().getName());
                appRecord.setHand("修改用户优币数为："+appUser.getGold());
                appRecord.setPid(appUser.getId());
                appRecord.setType(1);//1用户 2房间
                appRecordService.insert(appRecord);
            }
            if(SinataUtil.isNotEmpty(appUser.getYnum())){//修改了优钻数
                /*添加操作记录*/
                AppRecord appRecord=new AppRecord();
                appRecord.setCreateTime(new Date());
                appRecord.setName(ShiroKit.getUser().getName());
                appRecord.setHand("修改用户优钻数为："+appUser.getYnum());
                appRecord.setPid(appUser.getId());
                appRecord.setType(1);//1用户 2房间
                appRecordService.insert(appRecord);
            }
            if(SinataUtil.isNotEmpty(appUser.getRecommendCount())){//修改了推荐次数
                /*添加操作记录*/
                AppRecord appRecord=new AppRecord();
                appRecord.setCreateTime(new Date());
                appRecord.setName(ShiroKit.getUser().getName());
                appRecord.setHand("修改用户推荐次数为："+appUser.getRecommendCount());
                appRecord.setPid(appUser.getId());
                appRecord.setType(1);//1用户 2房间
                appRecordService.insert(appRecord);
            }
        }
        //保存修改数据
        appUserService.updateById(appUser);
        return SUCCESS_TIP;
    }

    /**
     * 修改用户管理 冻结和解冻
     */
    @RequestMapping(value = "/updateFreeze")
    @ResponseBody
    @Transactional
    public Object updateFreeze(String ids,Integer state) {
        EntityWrapper<AppUser> entityWrapper=new EntityWrapper<>();
        entityWrapper.in("id",ids);
        AppUser appUser=new AppUser();
        appUser.setState(state);
        appUserService.update(appUser,entityWrapper);
        /*循环添加添加操作记录*/
        String[] ids_=ids.split(",");
        for(String id:ids_){
            AppRecord appRecord=new AppRecord();
            appRecord.setCreateTime(new Date());
            appRecord.setName(ShiroKit.getUser().getName());
            String world="";
            if(state==1){
                appRecord.setHand("解禁了该用户");
            }else if(state==2){
                appRecord.setHand("禁用了该用户");
                world="该账号已被禁用";
                appRecord.setPid(Integer.valueOf(id));
                appRecord.setType(1);//1用户 2房间
                appRecordService.insert(appRecord);
                //==================循环极光推送配送员
                Map map = new HashMap<>();
                map.put("state", "6");//禁用id
                map.put("id", id);//禁用id
                Jpush.push( id,  world ,map);
            }
        }
        return SUCCESS_TIP;
    }


    /**
     * 用户管理详情
     */
    @RequestMapping(value = "/detail/{appUserId}")
    @ResponseBody
    public Object detail(@PathVariable("appUserId") Integer appUserId) {
        return appUserService.selectById(appUserId);
    }
}
