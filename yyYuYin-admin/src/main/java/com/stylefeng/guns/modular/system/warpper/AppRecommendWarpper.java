package com.stylefeng.guns.modular.system.warpper;

import com.stylefeng.guns.core.base.warpper.BaseControllerWarpper;
import com.stylefeng.guns.core.util.SinataUtil;

import java.util.List;
import java.util.Map;

/**
 * 用户管理的包装类
 *
 * @author fengshuonan
 * @date 2017年2月13日 下午10:47:03
 */
public class AppRecommendWarpper extends BaseControllerWarpper {

    public AppRecommendWarpper(List<Map<String, Object>> list) {
        super(list);
    }

    @Override
    public void warpTheMap(Map<String, Object> map) {
        if(SinataUtil.isNotEmpty(map.get("sTime"))){
            String s=map.get("sTime").toString();
            map.put("sTime_",s.substring(0,s.length()-2));
        }
        if(SinataUtil.isNotEmpty(map.get("eTime"))){
            String e=map.get("eTime").toString();
            map.put("eTime_",e.substring(0,e.length()-2));
        }
    }

}
