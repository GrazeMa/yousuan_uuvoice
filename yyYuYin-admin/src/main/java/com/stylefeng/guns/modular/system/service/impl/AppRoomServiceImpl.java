package com.stylefeng.guns.modular.system.service.impl;

import com.stylefeng.guns.modular.system.model.AppRoom;
import com.stylefeng.guns.modular.system.dao.AppRoomMapper;
import com.stylefeng.guns.modular.system.service.IAppRoomService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 房间信息 服务实现类
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-07
 */
@Service
public class AppRoomServiceImpl extends ServiceImpl<AppRoomMapper, AppRoom> implements IAppRoomService {
    @Override
    public Integer updateRecommend(AppRoom appRoom) {
        return this.baseMapper.updateRecommend(appRoom);
    }
}
