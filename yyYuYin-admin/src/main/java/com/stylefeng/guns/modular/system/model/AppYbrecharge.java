package com.stylefeng.guns.modular.system.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 优币充值记录
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-14
 */
@TableName("app_ybrecharge")
public class AppYbrecharge extends Model<AppYbrecharge> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    private Date createTime;
    private Integer uid;
    private String name;
    private Integer eid;
    /**
     * 购买金币
     */
    private Integer gmGold;
    /**
     * 赠送金币
     */
    private Integer zsGold;
    /**
     * 实得金币
     */
    private Integer sdGold;
    /**
     * 支付数金额
     */
    private Double play;
    /**
     * 1 支付宝 , 2 微信, 3苹果内购
     */
    private Integer playState;
    /**
     * '是否被删除（1=否  2=是）'
     */
    private Integer isDelete;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getEid() {
        return eid;
    }

    public void setEid(Integer eid) {
        this.eid = eid;
    }

    public Integer getGmGold() {
        return gmGold;
    }

    public void setGmGold(Integer gmGold) {
        this.gmGold = gmGold;
    }

    public Integer getZsGold() {
        return zsGold;
    }

    public void setZsGold(Integer zsGold) {
        this.zsGold = zsGold;
    }

    public Integer getSdGold() {
        return sdGold;
    }

    public void setSdGold(Integer sdGold) {
        this.sdGold = sdGold;
    }

    public Double getPlay() {
        return play;
    }

    public void setPlay(Double play) {
        this.play = play;
    }

    public Integer getPlayState() {
        return playState;
    }

    public void setPlayState(Integer playState) {
        this.playState = playState;
    }

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "AppYbrecharge{" +
        "id=" + id +
        ", createTime=" + createTime +
        ", uid=" + uid +
        ", name=" + name +
        ", eid=" + eid +
        ", gmGold=" + gmGold +
        ", zsGold=" + zsGold +
        ", sdGold=" + sdGold +
        ", play=" + play +
        ", playState=" + playState +
        ", isDelete=" + isDelete +
        "}";
    }
}
