package com.stylefeng.guns.modular.system.service;

import com.stylefeng.guns.modular.system.model.AppProtocol;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * app各种html页面 服务类
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-05
 */
public interface IAppProtocolService extends IService<AppProtocol> {
    /**
     * 获取各种页面协议
     * @return
     */
    List<Map<String,Object>> getAppProtocol();
}
