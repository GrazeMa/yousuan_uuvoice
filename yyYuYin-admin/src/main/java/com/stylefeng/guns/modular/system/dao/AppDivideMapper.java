package com.stylefeng.guns.modular.system.dao;

import com.stylefeng.guns.modular.system.model.AppDivide;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 充值分成金额 Mapper 接口
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-06
 */
public interface AppDivideMapper extends BaseMapper<AppDivide> {
    /**
     *获取邀请用户分成奖励
     * @param appDivide
     * @return
     */
    List<Map<String,Object>> getAppDivideList(AppDivide appDivide);

    /**
     * 获取邀请用户的奖励金额
     * @param appDivide
     * @return
     */
    List<Map<String,Object>> getAppDivideSum(AppDivide appDivide);
}
