package com.stylefeng.guns.modular.system.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.common.constant.factory.PageFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;
import com.stylefeng.guns.modular.system.model.AppAndroidVersions;
import com.stylefeng.guns.modular.system.service.IAppAndroidVersionsService;

import java.util.Date;

/**
 * Android版本控制控制器
 * @Date 2019-05-21 09:56:00
 */
@Controller
@RequestMapping("/appAndroidVersions")
public class AppAndroidVersionsController extends BaseController {

    private String PREFIX = "/system/appAndroidVersions/";

    @Autowired
    private IAppAndroidVersionsService appAndroidVersionsService;

    /**
     * 跳转到Android版本控制首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "appAndroidVersions.html";
    }

    /**
     * 跳转到添加Android版本控制
     */
    @RequestMapping("/appAndroidVersions_add")
    public String appAndroidVersionsAdd() {
        return PREFIX + "appAndroidVersions_add.html";
    }

    /**
     * 跳转到修改Android版本控制
     */
    @RequestMapping("/appAndroidVersions_update/{appAndroidVersionsId}")
    public String appAndroidVersionsUpdate(@PathVariable Integer appAndroidVersionsId, Model model) {
        AppAndroidVersions appAndroidVersions = appAndroidVersionsService.selectById(appAndroidVersionsId);
        model.addAttribute("item",appAndroidVersions);
        LogObjectHolder.me().set(appAndroidVersions);
        return PREFIX + "appAndroidVersions_edit.html";
    }

    /**
     * 获取Android版本控制列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        Wrapper entityWrapper=new EntityWrapper<AppAndroidVersions>();
        entityWrapper.orderBy("createDate",false);
        Page<AppAndroidVersions> page = new PageFactory<AppAndroidVersions>().defaultPage();
        page.setRecords(appAndroidVersionsService.selectMapsPage(page, entityWrapper).getRecords());
        return super.packForBT(page);
    }

    /**
     * 新增Android版本控制
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(AppAndroidVersions appAndroidVersions) {
        appAndroidVersions.setCreateDate(new Date());
        appAndroidVersionsService.insert(appAndroidVersions);
        return SUCCESS_TIP;
    }

    /**
     * 删除Android版本控制
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer appAndroidVersionsId) {
        appAndroidVersionsService.deleteById(appAndroidVersionsId);
        return SUCCESS_TIP;
    }

    /**
     * 修改Android版本控制
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(AppAndroidVersions appAndroidVersions) {
        appAndroidVersionsService.updateById(appAndroidVersions);
        return SUCCESS_TIP;
    }

    /**
     * Android版本控制详情
     */
    @RequestMapping(value = "/detail/{appAndroidVersionsId}")
    @ResponseBody
    public Object detail(@PathVariable("appAndroidVersionsId") Integer appAndroidVersionsId) {
        return appAndroidVersionsService.selectById(appAndroidVersionsId);
    }
}
