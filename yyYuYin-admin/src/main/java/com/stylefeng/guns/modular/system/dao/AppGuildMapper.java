package com.stylefeng.guns.modular.system.dao;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.system.model.AppGuild;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

/**
 * @Description 公会 Mapper 接口。
 * @author Grazer_Ma
 * @Date 2020-05-06 20:35:06
 */
public interface AppGuildMapper extends BaseMapper<AppGuild> {
    /**
     * 获取公会信息。
     * @param appGuild
     * @return
     */
    List<Map<String,Object>> getAppGuild(AppGuild appGuild, Page<Map<String,Object>> page);
}
