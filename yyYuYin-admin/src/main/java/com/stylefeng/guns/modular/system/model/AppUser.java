package com.stylefeng.guns.modular.system.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 用户
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-01
 */
@TableName("app_user")
public class AppUser extends Model<AppUser> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 注册时间
     */
    private String createDate;
    /**
     * 用户优优号
     */
    private String usercoding;
    /**
     * 性别(1 男, 2 女)
     */
    private Integer sex;
    /**
     * 用户头像
     */
    private String imgTx;
    /**
     * 魅力等级
     */
    private Integer charmGrade;
    /**
     * 财富等级
     */
    private Integer treasureGrade;
    /**
     * 礼物数
     */
    private Integer gift;
    /**
     * 优币数
     */
    private Integer gold;
    /**
     * 优钻数
     */
    private Integer ynum;
    /**
     * 出生日期
     */
    private String dateOfBirth;
    /**
     * 年龄
     */
    private Integer age;
    /**
     * 密码
     */
    private String password;
    /**
     * 手机号
     */
    private String phone;
    /**
     * 是否冻结 1否,2是
     */
    private Integer state;
    /**
     * 昵称
     */
    private String nickname;
    /**
     * 经度
     */
    private String jd;
    /**
     * 维度
     */
    private String wd;
    /**
     * 微信公众号的一标识
     */
    private String gzSid;
    /**
     * 微信标识
     */
    private String wxSid;
    /**
     * qq唯一标识
     */
    private String qqSid;
    /**
     * 是否开通房间 1否 ， 2是
     */
    private Integer status;
    /**
     * 历史充值
     */
    private Double historyRecharge;
    private Double historyDeposit;
    /**
     * 个性签名
     */
    private String individuation;
    /**
     * 声音签名
     */
    private String voice;
    /**
     * 房间名称
     */
    private String roomName;
    /**
     * 星座
     */
    private String constellation;
    /**
     * 粉丝数
     */
    private Integer fansNum;
    /**
     * 关注数
     */
    private Integer attentionNum;
    /**
     * 财富的累加值
     */
    private Integer goldNum;
    /**
     * 魅力值
     */
    private Integer yuml;
    /**
     * 推荐次数
     */
    private Integer recommendCount;
    /**
     * 用户可提现金娥
     */
    private Double userMoney;
    /**
     * 靓号
     */
    private String liang;
    
    private Integer autonym;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getUsercoding() {
        return usercoding;
    }

    public void setUsercoding(String usercoding) {
        this.usercoding = usercoding;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public String getImgTx() {
        return imgTx;
    }

    public void setImgTx(String imgTx) {
        this.imgTx = imgTx;
    }

    public Integer getCharmGrade() {
        return charmGrade;
    }

    public void setCharmGrade(Integer charmGrade) {
        this.charmGrade = charmGrade;
    }

    public Integer getTreasureGrade() {
        return treasureGrade;
    }

    public void setTreasureGrade(Integer treasureGrade) {
        this.treasureGrade = treasureGrade;
    }

    public Integer getGift() {
        return gift;
    }

    public void setGift(Integer gift) {
        this.gift = gift;
    }

    public Integer getGold() {
        return gold;
    }

    public void setGold(Integer gold) {
        this.gold = gold;
    }

    public Integer getYnum() {
        return ynum;
    }

    public void setYnum(Integer ynum) {
        this.ynum = ynum;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getJd() {
        return jd;
    }

    public void setJd(String jd) {
        this.jd = jd;
    }

    public String getWd() {
        return wd;
    }

    public void setWd(String wd) {
        this.wd = wd;
    }

    public String getGzSid() {
        return gzSid;
    }

    public void setGzSid(String gzSid) {
        this.gzSid = gzSid;
    }

    public String getWxSid() {
        return wxSid;
    }

    public void setWxSid(String wxSid) {
        this.wxSid = wxSid;
    }

    public String getQqSid() {
        return qqSid;
    }

    public void setQqSid(String qqSid) {
        this.qqSid = qqSid;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Double getHistoryRecharge() {
        return historyRecharge;
    }

    public void setHistoryRecharge(Double historyRecharge) {
        this.historyRecharge = historyRecharge;
    }

    public Double getHistoryDeposit() {
        return historyDeposit;
    }

    public void setHistoryDeposit(Double historyDeposit) {
        this.historyDeposit = historyDeposit;
    }

    public String getIndividuation() {
        return individuation;
    }

    public void setIndividuation(String individuation) {
        this.individuation = individuation;
    }

    public String getVoice() {
        return voice;
    }

    public void setVoice(String voice) {
        this.voice = voice;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public String getConstellation() {
        return constellation;
    }

    public void setConstellation(String constellation) {
        this.constellation = constellation;
    }

    public Integer getFansNum() {
        return fansNum;
    }

    public void setFansNum(Integer fansNum) {
        this.fansNum = fansNum;
    }

    public Integer getAttentionNum() {
        return attentionNum;
    }

    public void setAttentionNum(Integer attentionNum) {
        this.attentionNum = attentionNum;
    }

    public Integer getGoldNum() {
        return goldNum;
    }

    public void setGoldNum(Integer goldNum) {
        this.goldNum = goldNum;
    }

    public Integer getYuml() {
        return yuml;
    }

    public void setYuml(Integer yuml) {
        this.yuml = yuml;
    }

    public Integer getRecommendCount() {
        return recommendCount;
    }

    public void setRecommendCount(Integer recommendCount) {
        this.recommendCount = recommendCount;
    }

    public Double getUserMoney() {
        return userMoney;
    }

    public void setUserMoney(Double userMoney) {
        this.userMoney = userMoney;
    }

    public String getLiang() {
        return liang;
    }

    public void setLiang(String liang) {
        this.liang = liang;
    }
    
    public Integer getAutonym() {
        return autonym;
    }

    public void setAutonym(Integer autonym) {
        this.autonym = autonym;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "AppUser{" +
        "id=" + id +
        ", createDate=" + createDate +
        ", usercoding=" + usercoding +
        ", sex=" + sex +
        ", imgTx=" + imgTx +
        ", charmGrade=" + charmGrade +
        ", treasureGrade=" + treasureGrade +
        ", gift=" + gift +
        ", gold=" + gold +
        ", ynum=" + ynum +
        ", dateOfBirth=" + dateOfBirth +
        ", age=" + age +
        ", password=" + password +
        ", phone=" + phone +
        ", state=" + state +
        ", nickname=" + nickname +
        ", jd=" + jd +
        ", wd=" + wd +
        ", gzSid=" + gzSid +
        ", wxSid=" + wxSid +
        ", qqSid=" + qqSid +
        ", status=" + status +
        ", historyRecharge=" + historyRecharge +
        ", historyDeposit=" + historyDeposit +
        ", individuation=" + individuation +
        ", voice=" + voice +
        ", roomName=" + roomName +
        ", constellation=" + constellation +
        ", fansNum=" + fansNum +
        ", attentionNum=" + attentionNum +
        ", goldNum=" + goldNum +
        ", yuml=" + yuml +
        ", autonym=" + autonym +
        "}";
    }
}
