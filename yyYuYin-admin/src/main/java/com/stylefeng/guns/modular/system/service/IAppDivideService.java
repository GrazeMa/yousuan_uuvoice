package com.stylefeng.guns.modular.system.service;

import com.stylefeng.guns.modular.system.model.AppDivide;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 充值分成金额 服务类
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-06
 */
public interface IAppDivideService extends IService<AppDivide> {
    /**
     *获取邀请用户分成奖励
     * @param appDivide
     * @return
     */
    List<Map<String,Object>> getAppDivideList(AppDivide appDivide);
    /**
     * 获取邀请用户的奖励金额
     * @param appDivide
     * @return
     */
    List<Map<String,Object>> getAppDivideSum(AppDivide appDivide);
}
