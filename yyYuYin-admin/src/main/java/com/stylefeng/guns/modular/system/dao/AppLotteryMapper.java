package com.stylefeng.guns.modular.system.dao;

import com.stylefeng.guns.modular.system.model.AppLottery;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 宝箱设置 （第一条数据保存到礼物设置里面） Mapper 接口
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-11
 */
public interface AppLotteryMapper extends BaseMapper<AppLottery> {

}
