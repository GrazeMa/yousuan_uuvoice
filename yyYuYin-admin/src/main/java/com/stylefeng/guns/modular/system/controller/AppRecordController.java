package com.stylefeng.guns.modular.system.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.stylefeng.guns.core.base.controller.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;
import com.stylefeng.guns.modular.system.model.AppRecord;
import com.stylefeng.guns.modular.system.service.IAppRecordService;

/**
 * 操作日志控制器
 * @Date 2019-03-01 14:22:41
 */
@Controller
@RequestMapping("/appRecord")
public class AppRecordController extends BaseController {

    private String PREFIX = "/system/appRecord/";

    @Autowired
    private IAppRecordService appRecordService;

    /**
     * 跳转到操作日志首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "appRecord.html";
    }
    /**
     * 查看操作记录
     * @return
     */
    @RequestMapping("/appUser_recordById")
    public String appUserRecordById(Integer id,Integer type,Model model) {
        model.addAttribute("pid",id);
        model.addAttribute("type",type);
        return PREFIX + "appRecord.html";
    }
    /**
     * 跳转到添加操作日志
     */
    @RequestMapping("/appRecord_add")
    public String appRecordAdd() {
        return PREFIX + "appRecord_add.html";
    }

    /**
     * 跳转到修改操作日志
     */
    @RequestMapping("/appRecord_update/{appRecordId}")
    public String appRecordUpdate(@PathVariable Integer appRecordId, Model model) {
        AppRecord appRecord = appRecordService.selectById(appRecordId);
        model.addAttribute("item",appRecord);
        LogObjectHolder.me().set(appRecord);
        return PREFIX + "appRecord_edit.html";
    }

    /**
     * 获取操作日志列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(Integer pid,Integer type) {

        return appRecordService.selectList(new EntityWrapper<AppRecord>().eq("pid",pid)
                .eq("type",type)
                .orderBy("id",false));
    }

    /**
     * 新增操作日志
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(AppRecord appRecord) {
        appRecordService.insert(appRecord);
        return SUCCESS_TIP;
    }

    /**
     * 删除操作日志
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer appRecordId) {
        appRecordService.deleteById(appRecordId);
        return SUCCESS_TIP;
    }

    /**
     * 修改操作日志
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(AppRecord appRecord) {
        appRecordService.updateById(appRecord);
        return SUCCESS_TIP;
    }

    /**
     * 操作日志详情
     */
    @RequestMapping(value = "/detail/{appRecordId}")
    @ResponseBody
    public Object detail(@PathVariable("appRecordId") Integer appRecordId) {
        return appRecordService.selectById(appRecordId);
    }
}
