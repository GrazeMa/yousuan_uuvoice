package com.stylefeng.guns.modular.system.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 安卓版本控制
 * </p>
 *
 * @author stylefeng123
 * @since 2019-05-21
 */
@TableName("app_android_versions")
public class AppAndroidVersions extends Model<AppAndroidVersions> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 发布时间
     */
    private Date createDate;
    /**
     * 版本号
     */
    private String versions;
    /**
     * 版本描述
     */
    private String mark;
    /**
     * 是否强制更新 1否，2 是
     */
    private Integer state;
    /**
     * 版本链接
     */
    private String url;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getVersions() {
        return versions;
    }

    public void setVersions(String versions) {
        this.versions = versions;
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "AppAndroidVersions{" +
        "id=" + id +
        ", createDate=" + createDate +
        ", versions=" + versions +
        ", mark=" + mark +
        ", state=" + state +
        ", url=" + url +
        "}";
    }
}
