package com.stylefeng.guns.modular.system.service;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.system.model.AppGiftRecord;

/**
 * @Description 礼物记录服务类接口。
 * @author Grazer_Ma
 * @Date 2020-05-31 10:10:32
 */
public interface IAppGiftRecordService extends IService<AppGiftRecord> {

	/**
	 * 获取礼物记录列表数据。
	 * 
	 * @param appGiftRecord
	 * @return
	 */
	List<Map<String, Object>> getAppGiftRecordList(AppGiftRecord appGiftRecord, Page<Map<String, Object>> page);

}
