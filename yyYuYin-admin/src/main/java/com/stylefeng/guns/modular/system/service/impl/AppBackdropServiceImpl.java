package com.stylefeng.guns.modular.system.service.impl;

import com.stylefeng.guns.modular.system.model.AppBackdrop;
import com.stylefeng.guns.modular.system.dao.AppBackdropMapper;
import com.stylefeng.guns.modular.system.service.IAppBackdropService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 背景图 服务实现类
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-07
 */
@Service
public class AppBackdropServiceImpl extends ServiceImpl<AppBackdropMapper, AppBackdrop> implements IAppBackdropService {

}
