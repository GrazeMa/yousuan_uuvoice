package com.stylefeng.guns.modular.system.controller;

import com.stylefeng.guns.core.base.controller.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;
import com.stylefeng.guns.modular.system.model.AppTreasure;
import com.stylefeng.guns.modular.system.service.IAppTreasureService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 房间财富榜控制器
 * @Date 2019-03-13 09:36:41
 */
@Controller
@RequestMapping("/appTreasure")
public class AppTreasureController extends BaseController {

    private String PREFIX = "/system/appTreasure/";

    @Autowired
    private IAppTreasureService appTreasureService;

    /**
     * 跳转到房间财富榜首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "appTreasure.html";
    }

    /**
     * 跳转到添加房间财富榜
     */
    @RequestMapping("/appTreasure_add")
    public String appTreasureAdd() {
        return PREFIX + "appTreasure_add.html";
    }

    /**
     * 跳转到修改房间财富榜
     */
    @RequestMapping("/appTreasure_update/{appTreasureId}")
    public String appTreasureUpdate(@PathVariable Integer appTreasureId, Model model) {
        AppTreasure appTreasure = appTreasureService.selectById(appTreasureId);
        model.addAttribute("item",appTreasure);
        LogObjectHolder.me().set(appTreasure);
        return PREFIX + "appTreasure_edit.html";
    }

    /**
     * 获取房间财富榜列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String usercoding,String nickname,Integer day) {
        if(day==null){
            day=3;
        }
        Map<String,Object> paMap=new HashMap<>();
        paMap.put("day",day);
        paMap.put("usercoding",usercoding);
        paMap.put("nickname",nickname);
        List<Map<String,Object>> reList=appTreasureService.getAppTreasure(paMap);
        for(int i=0;i<reList.size();i++){
            Map<String,Object> map=new HashMap<>();
            if(i>0){
                Integer num=Integer.valueOf(reList.get(i-1).get("value_").toString())-Integer.valueOf(reList.get(i).get("value_").toString());
                reList.get(i).put("num",num);
            }
        }
        return reList;
    }

    /**
     * 新增房间财富榜
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(AppTreasure appTreasure) {
        appTreasureService.insert(appTreasure);
        return SUCCESS_TIP;
    }

    /**
     * 删除房间财富榜
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer appTreasureId) {
        appTreasureService.deleteById(appTreasureId);
        return SUCCESS_TIP;
    }

    /**
     * 修改房间财富榜
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(AppTreasure appTreasure) {
        appTreasureService.updateById(appTreasure);
        return SUCCESS_TIP;
    }

    /**
     * 房间财富榜详情
     */
    @RequestMapping(value = "/detail/{appTreasureId}")
    @ResponseBody
    public Object detail(@PathVariable("appTreasureId") Integer appTreasureId) {
        return appTreasureService.selectById(appTreasureId);
    }
}
