package com.stylefeng.guns.modular.system.service.impl;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.system.model.AppGuild;
import com.stylefeng.guns.modular.system.dao.AppGuildHistoryMapper;
import com.stylefeng.guns.modular.system.service.IAppGuildHistoryService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
/**
 * @Description 入会与退会记录实现类。
 * @author Grazer_Ma
 * @Date 2020-05-14 21:41:44
 */
@Service
public class AppGuildHistoryServiceImpl extends ServiceImpl<AppGuildHistoryMapper, AppGuild> implements IAppGuildHistoryService {
    @Override
    public List<Map<String, Object>> getAppGuildHistory(AppGuild appGuild, Page<Map<String,Object>> page) {
        return this.baseMapper.getAppGuildHistory(appGuild, page);
    }
}
