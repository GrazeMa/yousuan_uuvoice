package com.stylefeng.guns.modular.system.dao;

import com.stylefeng.guns.modular.system.model.AppRoom;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 房间信息 Mapper 接口
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-07
 */
public interface AppRoomMapper extends BaseMapper<AppRoom> {
    /**
     * 房间取消推荐
     * @param appRoom
     * @return
     */
    Integer updateRecommend(AppRoom appRoom);
}
