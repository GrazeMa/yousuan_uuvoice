package com.stylefeng.guns.modular.system.service;

import com.stylefeng.guns.modular.system.model.AppBackdrop;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 背景图 服务类
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-07
 */
public interface IAppBackdropService extends IService<AppBackdrop> {

}
