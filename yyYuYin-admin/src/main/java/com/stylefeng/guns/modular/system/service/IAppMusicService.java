package com.stylefeng.guns.modular.system.service;

import com.stylefeng.guns.modular.system.model.AppMusic;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 音乐审核 服务类
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-12
 */
public interface IAppMusicService extends IService<AppMusic> {

}
