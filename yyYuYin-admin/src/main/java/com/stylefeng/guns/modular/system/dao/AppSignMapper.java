package com.stylefeng.guns.modular.system.dao;

import com.stylefeng.guns.modular.system.model.AppSign;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 签到设置 Mapper 接口
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-01
 */
public interface AppSignMapper extends BaseMapper<AppSign> {
    /**
     * 获取签到和奖励
     * @param map
     * @return
     */
    List<Map<String,Object>> getAppSignAndAppAward(Map<String,Object> map);
}
