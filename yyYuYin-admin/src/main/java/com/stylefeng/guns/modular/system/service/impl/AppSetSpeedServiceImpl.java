package com.stylefeng.guns.modular.system.service.impl;

import com.stylefeng.guns.modular.system.model.AppSetSpeed;
import com.stylefeng.guns.modular.system.dao.AppSetSpeedMapper;
import com.stylefeng.guns.modular.system.service.IAppSetSpeedService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 速配/认证/语句设置 服务实现类
 * </p>
 *
 * @author stylefeng123
 * @since 2019-02-28
 */
@Service
public class AppSetSpeedServiceImpl extends ServiceImpl<AppSetSpeedMapper, AppSetSpeed> implements IAppSetSpeedService {

}
