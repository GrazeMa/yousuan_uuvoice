package com.stylefeng.guns.modular.system.controller;

import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.util.DateUtil;
import com.stylefeng.guns.modular.system.service.IAppUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 总览信息
 *
 * @author fengshuonan
 * @Date 2017年3月4日23:05:54
 */
@Controller
@RequestMapping("/blackboard")
public class BlackboardController extends BaseController {
    @Autowired
    private IAppUserService appUserService;


    public String Total(Object object){
        DecimalFormat df = new DecimalFormat("#.##");
        String total="0";
        if(object==null){
            total="0";
        }else if(Double.valueOf(object.toString())>10000){
            total=df.format(Double.valueOf(object.toString())/10000)+"万";
        }else {
            total=df.format(object).toString();
        }
        return  total;
    }
    /**
     * 跳转到黑板
     */
    @RequestMapping("")
    public String blackboard(Model model) {

        model.addAttribute("userList", appUserService.getUserCount(null));
        Map<String,Object> moneyList=appUserService.financialStatistics(null);//充值优币
        Map<String,Object> moneyList_=appUserService.financialStatistics_(null);//平台收入
        Map<String,Object> map=new HashMap<>();
        map.put("state",1);//1 收入，2 是支出
        Map<String,Object> moneyListJS=appUserService.financialStatisticsJ(map);//平台净收入
        map.put("state",2);//1 收入，2 是支出
        Map<String,Object> moneyListJZ=appUserService.financialStatisticsJ(map);//平台净支出


        Double totalMoney1=Double.valueOf(moneyList.get("totalMoney1").toString());
        Double totalMoney_1=Double.valueOf(moneyList.get("totalMoney_1").toString());
        Double totalMoney2=Double.valueOf(moneyList.get("totalMoney2").toString());
        Double totalMoney_2=Double.valueOf(moneyList.get("totalMoney_2").toString());
        Double totalMoney3=Double.valueOf(moneyList.get("totalMoney3").toString());
        Double totalMoney_3=Double.valueOf(moneyList.get("totalMoney_3").toString());
        Double totalMoney4=Double.valueOf(moneyList_.get("totalMoney1").toString());
        Double totalMoney_4=Double.valueOf(moneyList_.get("totalMoney_1").toString());
        Double totalMoney5=Double.valueOf(moneyList_.get("totalMoney2").toString());
        Double totalMoney_5=Double.valueOf(moneyList_.get("totalMoney_2").toString());
        Double totalMoney6=Double.valueOf(moneyList_.get("totalMoney3").toString());
        Double totalMoney_6=Double.valueOf(moneyList_.get("totalMoney_3").toString());
        /*净收入*/
        Double totalMoneyJS1=Double.valueOf(moneyListJS.get("totalMoney1").toString());
        Double totalMoneyJS_1=Double.valueOf(moneyListJS.get("totalMoney_1").toString());
        Double totalMoneyJS2=Double.valueOf(moneyListJS.get("totalMoney2").toString());
        Double totalMoneyJS_2=Double.valueOf(moneyListJS.get("totalMoney_2").toString());
        Double totalMoneyJS3=Double.valueOf(moneyListJS.get("totalMoney3").toString());
        Double totalMoneyJS_3=Double.valueOf(moneyListJS.get("totalMoney_3").toString());
        /*净支出*/
        Double totalMoneyJZ1=Double.valueOf(moneyListJZ.get("totalMoney1").toString());
        Double totalMoneyJZ_1=Double.valueOf(moneyListJZ.get("totalMoney_1").toString());
        Double totalMoneyJZ2=Double.valueOf(moneyListJZ.get("totalMoney2").toString());
        Double totalMoneyJZ_2=Double.valueOf(moneyListJZ.get("totalMoney_2").toString());
        Double totalMoneyJZ3=Double.valueOf(moneyListJZ.get("totalMoney3").toString());
        Double totalMoneyJZ_3=Double.valueOf(moneyListJZ.get("totalMoney_3").toString());


        String str1="上升";
        Double percentage1=0D;
        String str2="上升";
        Double percentage2=0D;
        String str3="上升";
        Double percentage3=0D;
        String str4="上升";
        Double percentage4=0D;
        String str5="上升";
        Double percentage5=0D;
        String str6="上升";
        Double percentage6=0D;
        String str7="上升";
        Double percentage7=0D;
        String str8="上升";
        Double percentage8=0D;
        String str9="上升";
        Double percentage9=0D;
        if(totalMoney1<totalMoney_1){
            str1="下降";
            if(!totalMoney_1.equals(0.00)){
                percentage1=Math.abs(totalMoney1-totalMoney_1)/totalMoney_1*100;
                BigDecimal bg = new BigDecimal(percentage1);
                percentage1 = bg.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
            }
        }
        if(totalMoney2<totalMoney_2){
            str2="下降";
            if(!totalMoney_2.equals(0.00)){
                percentage2=Math.abs(totalMoney2-totalMoney_2)/totalMoney_2*100;
                BigDecimal bg = new BigDecimal(percentage2);
                percentage2 = bg.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
            }
        }
        if(totalMoney3<totalMoney_3){
            str3="下降";
            if(!totalMoney_3.equals(0.00)){
                percentage3=Math.abs(totalMoney3-totalMoney_3)/totalMoney_3*100;
                BigDecimal bg = new BigDecimal(percentage3);
                percentage3 = bg.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
            }
        }
        if(totalMoney4<totalMoney_4){
            str4="下降";
            if(!totalMoney_4.equals(0.00)){
                percentage4=Math.abs(totalMoney4-totalMoney_4)/totalMoney_4*100;
                BigDecimal bg = new BigDecimal(percentage4);
                percentage4 = bg.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
            }
        }
        if(totalMoney5<totalMoney_5){
            str5="下降";
            if(!totalMoney_5.equals(0.00)){
                percentage5=Math.abs(totalMoney5-totalMoney_5)/totalMoney_5*100;
                BigDecimal bg = new BigDecimal(percentage5);
                percentage5 = bg.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
            }
        }
        if(totalMoney6<totalMoney_6){
            str6="下降";
            if(!totalMoney_6.equals(0.00)){
                percentage6=Math.abs(totalMoney6-totalMoney_6)/totalMoney_6*100;
                BigDecimal bg = new BigDecimal(percentage6);
                percentage6 = bg.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
            }
        }
        /*净收入*/
        Double totalMoneyJSZ1=totalMoneyJS1-totalMoneyJZ1;//周收入
        Double totalMoneyJSZ_1=totalMoneyJS_1-totalMoneyJZ_1;//上周收入
        Double totalMoneyJSZ2=totalMoneyJS2-totalMoneyJZ2;//月收入
        Double totalMoneyJSZ_2=totalMoneyJS_2-totalMoneyJZ_2;//上月收入
        Double totalMoneyJSZ3=totalMoneyJS3-totalMoneyJZ3;//年收入
        Double totalMoneyJSZ_3=totalMoneyJS_3-totalMoneyJZ_3;//上年收入
        if(totalMoneyJSZ1<totalMoneyJSZ_1){
            str7="下降";
            if(totalMoneyJSZ1!=0.00){
                percentage7 = Math.abs(totalMoneyJSZ1 - totalMoneyJSZ_1) / totalMoneyJSZ1 * 100;
                BigDecimal bg = new BigDecimal(percentage7);
                percentage7 = bg.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
            }
        }
        if(totalMoneyJSZ2<totalMoneyJSZ_2){
            str8="下降";
            if(!totalMoneyJSZ_2.equals(0.00)){
                percentage8=Math.abs(totalMoneyJSZ2-totalMoneyJSZ_2)/totalMoneyJSZ_2*100;
                BigDecimal bg = new BigDecimal(percentage8);
                percentage8 = bg.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
            }
        }
        if(totalMoneyJSZ3<totalMoneyJSZ_3){
            str9="下降";
            if(!totalMoneyJSZ_3.equals(0.00)){
                percentage9=Math.abs(totalMoneyJSZ3-totalMoneyJSZ_3)/totalMoneyJSZ_3*100;
                BigDecimal bg = new BigDecimal(percentage9);
                percentage9 = bg.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
            }
        }
        moneyList.put("totalMoney1_",this.Total(moneyList.get("totalMoney1")));
        moneyList.put("totalMoney2_",this.Total(moneyList.get("totalMoney2")));
        moneyList.put("totalMoney3_",this.Total(moneyList.get("totalMoney3")));
        moneyList.put("totalMoney4_",this.Total(moneyList_.get("totalMoney1")));
        moneyList.put("totalMoney5_",this.Total(moneyList_.get("totalMoney2")));
        moneyList.put("totalMoney6_",this.Total(moneyList_.get("totalMoney3")));
        moneyList.put("totalMoney7_",totalMoneyJSZ1);
        moneyList.put("totalMoney8_",totalMoneyJSZ2);
        moneyList.put("totalMoney9_",totalMoneyJSZ3);
        moneyList.put("str1",str1);
        moneyList.put("str4",str4);
        moneyList.put("percentage1",percentage1);
        moneyList.put("percentage4",percentage4);
        moneyList.put("str2",str2);
        moneyList.put("str5",str5);
        moneyList.put("percentage2",percentage2);
        moneyList.put("percentage5",percentage5);
        moneyList.put("str3",str3);
        moneyList.put("str6",str6);
        moneyList.put("percentage3",percentage3);
        moneyList.put("percentage6",percentage6);
        moneyList.put("str7",str7);
        moneyList.put("percentage7",percentage7);
        moneyList.put("str8",str8);
        moneyList.put("percentage8",percentage8);
        moneyList.put("str9",str9);
        moneyList.put("percentage9",percentage9);
        model.addAttribute("moneyList",moneyList);

        return "/blackboard.html";
    }

    /**
     * 获取用户注册，vip充值趋势图
     * @param type
     * @return
     */
    @RequestMapping("/getTrendMap")
    @ResponseBody
    public Object getTotalUser(Integer type) {
        Map<String, Object> returnObj = new HashMap<>();
        HashMap<String, Object> pmap=new HashMap<>();
        if (type == 1) {//用户注册数
            String day = DateUtil.beforeFewDayStr(null, 21);
            pmap.put("day", day);
            List<Map<String, Object>> list = appUserService.getUserStatic(pmap);
            List<Map<String, Object>> userList = new ArrayList<>();
            Map<String, Object> maps = null;
            String strDay = "[";
            String strNumber = "[{name:'注册',data: [";
            for (int i = 20; i >= 0; i--) {
                String days = DateUtil.beforeFewDayStr(null, i);
                maps = new HashMap<>();
                maps.put("day", days);
                maps.put("number", 0);
                for (Map<String, Object> map : list) {
                    String listDay = (String) map.get("day");
                    if (listDay.equals(days)) {
                        maps.remove("number");
                        maps.put("number", map.get("number"));
                    }
                }
                userList.add(maps);
                if (i != 0) {
                    strDay += "'" + days.substring(5, 10) + "',";
                    strNumber += maps.get("number") + ",";
                } else {
                    strDay += "'" + days.substring(5, 10) + "'";
                    strNumber += maps.get("number") + "";
                }
            }
            strDay += "]";
            strNumber += "]}]";
            returnObj.put("strDay", strDay);
            returnObj.put("strNumber", strNumber);
            return  returnObj;
        } else if(type == 2){//用户活跃度
            String day = DateUtil.beforeFewDayStr(null, 21);
            pmap.put("day", day);
            List<Map<String, Object>> list = appUserService.getUserStaticLogin(pmap);
            List<Map<String, Object>> userList = new ArrayList<>();
            Map<String, Object> maps = null;
            String strDay = "[";
            String strNumber = "[{name:'日活跃度',data: [";
            for (int i = 20; i >= 0; i--) {
                String days = DateUtil.beforeFewDayStr(null, i);
                maps = new HashMap<>();
                maps.put("day", days);
                maps.put("number", 0);
                for (Map<String, Object> map : list) {
                    String listDay = (String) map.get("day");
                    if (listDay.equals(days)) {
                        maps.remove("number");
                        maps.put("number", map.get("number"));
                    }
                }
                userList.add(maps);
                if (i != 0) {
                    strDay += "'" + days.substring(5, 10) + "',";
                    strNumber += maps.get("number") + ",";
                } else {
                    strDay += "'" + days.substring(5, 10) + "'";
                    strNumber += maps.get("number") + "";
                }
            }
            strDay += "]";
            strNumber += "]}]";
            returnObj.put("strDay", strDay);
            returnObj.put("strNumber", strNumber);
            return  returnObj;
        } else if(type == 3){//优币充值
        String day = DateUtil.beforeFewDayStr(null, 21);
        pmap.put("day", day);
        List<Map<String, Object>> list = appUserService.getChongzhi(pmap);
        List<Map<String, Object>> userList = new ArrayList<>();
        Map<String, Object> maps = null;
        String strDay = "[";
        String strNumber = "[{name:'优币充值',data: [";
        for (int i = 20; i >= 0; i--) {
            String days = DateUtil.beforeFewDayStr(null, i);
            maps = new HashMap<>();
            maps.put("day", days);
            maps.put("number", 0);
            for (Map<String, Object> map : list) {
                String listDay = (String) map.get("day");
                if (listDay.equals(days)) {
                    maps.remove("number");
                    maps.put("number", map.get("number"));
                }
            }
            userList.add(maps);
            if (i != 0) {
                strDay += "'" + days.substring(5, 10) + "',";
                strNumber += maps.get("number") + ",";
            } else {
                strDay += "'" + days.substring(5, 10) + "'";
                strNumber += maps.get("number") + "";
            }
        }
        strDay += "]";
        strNumber += "]}]";
        returnObj.put("strDay", strDay);
        returnObj.put("strNumber", strNumber);
        return  returnObj;
    } else if(type == 4){//平台收入
        String day = DateUtil.beforeFewDayStr(null, 21);
        pmap.put("day", day);
        List<Map<String, Object>> list = appUserService.getShouru(pmap);
        List<Map<String, Object>> userList = new ArrayList<>();
        Map<String, Object> maps = null;
        String strDay = "[";
        String strNumber = "[{name:'平台收入',data: [";
        for (int i = 20; i >= 0; i--) {
            String days = DateUtil.beforeFewDayStr(null, i);
            maps = new HashMap<>();
            maps.put("day", days);
            maps.put("number", 0);
            for (Map<String, Object> map : list) {
                String listDay = (String) map.get("day");
                if (listDay.equals(days)) {
                    maps.remove("number");
                    maps.put("number", map.get("number"));
                }
            }
            userList.add(maps);
            if (i != 0) {
                strDay += "'" + days.substring(5, 10) + "',";
                strNumber += maps.get("number") + ",";
            } else {
                strDay += "'" + days.substring(5, 10) + "'";
                strNumber += maps.get("number") + "";
            }
        }
        strDay += "]";
        strNumber += "]}]";
        returnObj.put("strDay", strDay);
        returnObj.put("strNumber", strNumber);
        return  returnObj;
    }
        return null;
    }

    /**
     * 用户注册城市分布图
     * @param
     * @return
     */
    @RequestMapping("/userByCity")
    @ResponseBody
    public Object userByCity() {
        List<Map<String, Object>> userArea = appUserService.userByCity(null);
        return userArea;
    }
    /**
     * 用户性别分布图
     * @param
     * @return
     */
    @RequestMapping("/userBySix")
    @ResponseBody
    public Object userBySix() {
        List<Map<String, Object>> userSex = appUserService.userBySix(null);
        return userSex;
    }
    /**
     * 用户年龄分布图
     * @param
     * @return
     */
    @RequestMapping("/userByAge")
    @ResponseBody
    public Object userByAge() {
        List<Map<String,Object>> list = new ArrayList<>();
        List<Map<String, Object>> userAge = appUserService.userByAge(null);
        for (Map<String, Object> m : userAge) {
            Map<String,Object> str = new HashMap<>();
            str.put("count",Long.parseLong(m.get("count1").toString()));
            str.put("name","18-25岁");
            list.add(str);
            Map<String,Object> str1 = new HashMap<>();
            str1.put("count",Long.parseLong(m.get("count2").toString()));
            str1.put("name","26-30岁");
            list.add(str1);
            Map<String,Object> str2 = new HashMap<>();
            str2.put("count",Long.parseLong(m.get("count3").toString()));
            str2.put("name","31-35岁");
            list.add(str2);
            Map<String,Object> str3 = new HashMap<>();
            str3.put("count",Long.parseLong(m.get("count4").toString()));
            str3.put("name","36-40岁");
            list.add(str3);
            Map<String,Object> str4 = new HashMap<>();
            str4.put("count",Long.parseLong(m.get("count5").toString()));
            str4.put("name","41-45岁");
            list.add(str4);
            Map<String,Object> str5 = new HashMap<>();
            str5.put("count",Long.parseLong(m.get("count6").toString()));
            str5.put("name","45岁以上");
            list.add(str5);
        }
        return list;
    }
    /**
     * 用户活跃度分布图
     * @param
     * @return
     */
    @RequestMapping("/userByHuo")
    @ResponseBody
    public Object userByHuo() {
        List<Map<String,Object>> list = new ArrayList<>();
        List<Map<String, Object>> users = appUserService.userByHuo(null);
        for (Map<String, Object> m : users) {
            Map<String,Object> str = new HashMap<>();
            str.put("count",m.get("count1"));
            str.put("name","3天以内登录");
            list.add(str);
            Map<String,Object> str1 = new HashMap<>();
            str1.put("count",m.get("count2"));
            str1.put("name","7天以内登录");
            list.add(str1);
            Map<String,Object> str2 = new HashMap<>();
            str2.put("count",m.get("count3"));
            str2.put("name","15天以内登录");
            list.add(str2);
            Map<String,Object> str3 = new HashMap<>();
            str3.put("count",m.get("count4"));
            str3.put("name","30天以内登录");
            list.add(str3);
            Map<String,Object> str4 = new HashMap<>();
            str4.put("count",m.get("count5"));
            str4.put("name","90天以内登录");
            list.add(str4);
            Map<String,Object> str5 = new HashMap<>();
            str5.put("count",m.get("count6"));
            str5.put("name","180天以内");
            list.add(str5);
        }
        return list;
    }
}
