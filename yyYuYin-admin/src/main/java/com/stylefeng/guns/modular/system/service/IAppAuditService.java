package com.stylefeng.guns.modular.system.service;

import com.stylefeng.guns.modular.system.model.AppAudit;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 实名认证 服务类
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-01
 */
public interface IAppAuditService extends IService<AppAudit> {

}
