package com.stylefeng.guns.modular.system.model;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

/**
 * @Description 礼物记录 Entity。
 * @author Grazer_Ma
 * @Date 2020-05-30 23:00:54
 */
@TableName("app_gift_record")
public class AppGiftRecord extends Model<AppGiftRecord> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id; // 主键 ID。
    private Date createDate; // 创建时间。

    private Integer uid; // 送礼物的人的ID。
    private Integer state; // 状态（1 ：送；2：收。）
    private Integer gid; // 礼物主键 ID。
    private Integer num; // 赠送礼物个数。
    private Integer buid; // 被送礼物的人的ID。
    private Integer type; // 类型（1：普通送礼；2：宝箱。）

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    
    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }
    
    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }
    
    public Integer getGid() {
        return gid;
    }

    public void setGid(Integer gid) {
        this.gid = gid;
    }
    
    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }
    
    public Integer getBuid() {
        return buid;
    }

    public void setBuid(Integer buid) {
        this.buid = buid;
    }
    
    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "AppGiftRecord{" +
        "id=" + id +
        ", createDate=" + createDate +
        ", uid=" + uid +
        ", state=" + state +
        ", gid=" + gid +
        ", num=" + num +
        ", buid=" + buid +
        ", type=" + type +
        "}";
    }
}
