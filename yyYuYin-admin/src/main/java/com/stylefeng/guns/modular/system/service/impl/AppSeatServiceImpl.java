package com.stylefeng.guns.modular.system.service.impl;

import com.stylefeng.guns.modular.system.model.AppSeat;
import com.stylefeng.guns.modular.system.dao.AppSeatMapper;
import com.stylefeng.guns.modular.system.service.IAppSeatService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 房间座位相关设置 服务实现类
 * </p>
 *
 * @author stylefeng123
 * @since 2019-04-03
 */
@Service
public class AppSeatServiceImpl extends ServiceImpl<AppSeatMapper, AppSeat> implements IAppSeatService {

}
