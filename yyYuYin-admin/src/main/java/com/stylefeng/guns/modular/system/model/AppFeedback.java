package com.stylefeng.guns.modular.system.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 反馈
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-13
 */
@TableName("app_feedback")
public class AppFeedback extends Model<AppFeedback> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 用户id
     */
    private Integer uid;
    /**
     * 反馈内容
     */
    private String content;
    /**
     * '是否被删除（1=否  2=是）'
     */
    private Integer isDelete;
    /**
     * 反馈时间
     */
    private Date createTime;
    /**
     * 处理结果
     */
    private String hand;
    /**
     * 1  个人用户
     */
    private Integer role;
    /**
     * 1 未处理, 2 已处理
     */
    private Integer status;
    /**
     * uuid
     */
    @TableField(exist = false)
    private Integer usercoding;

    /**
     * 时间查询条件(开始)
     */
    @TableField(exist = false)
    private String beginTime;
    /**
     * 时间查询条件(结束)
     */
    @TableField(exist = false)
    private String endTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getHand() {
        return hand;
    }

    public void setHand(String hand) {
        this.hand = hand;
    }

    public Integer getRole() {
        return role;
    }

    public void setRole(Integer role) {
        this.role = role;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getUsercoding() {
        return usercoding;
    }

    public void setUsercoding(Integer usercoding) {
        this.usercoding = usercoding;
    }

    public String getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(String beginTime) {
        this.beginTime = beginTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "AppFeedback{" +
        "id=" + id +
        ", uid=" + uid +
        ", content=" + content +
        ", isDelete=" + isDelete +
        ", createTime=" + createTime +
        ", hand=" + hand +
        ", role=" + role +
        ", status=" + status +
        "}";
    }
}
