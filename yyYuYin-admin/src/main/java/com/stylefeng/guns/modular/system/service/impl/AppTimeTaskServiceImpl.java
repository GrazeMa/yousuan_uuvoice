package com.stylefeng.guns.modular.system.service.impl;

import com.stylefeng.guns.modular.system.model.AppTimeTask;
import com.stylefeng.guns.modular.system.dao.AppTimeTaskMapper;
import com.stylefeng.guns.modular.system.service.IAppTimeTaskService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 定时任务数据 服务实现类
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-13
 */
@Service
public class AppTimeTaskServiceImpl extends ServiceImpl<AppTimeTaskMapper, AppTimeTask> implements IAppTimeTaskService {

}
