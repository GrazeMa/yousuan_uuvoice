package com.stylefeng.guns.modular.system.service;

import com.stylefeng.guns.modular.system.model.AppPk;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 房间pk
 服务类
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-08
 */
public interface IAppPkService extends IService<AppPk> {
    /**
     * 根据房间编号获取pk信息
     * @param appPk
     * @return
     */
    List<Map<String,Object>> getAppPkList(AppPk appPk);
}
