package com.stylefeng.guns.modular.system.service;

import com.stylefeng.guns.modular.system.model.AppRecord;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 操作记录 服务类
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-01
 */
public interface IAppRecordService extends IService<AppRecord> {

}
