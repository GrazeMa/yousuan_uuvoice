package com.stylefeng.guns.modular.system.dao;

import com.stylefeng.guns.modular.system.model.AppAuction;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 房间竞拍排名 Mapper 接口
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-11
 */
public interface AppAuctionMapper extends BaseMapper<AppAuction> {
    /**
     * 获取竞拍数据
     * @param map
     * @return
     */
    List<Map<String,Object>> getAppAuction(Map<String,Object> map);
    /**
     * 根据房间和竞拍id查看贡献记录
     * @param map
     * @return
     */
    List<Map<String,Object>> getAppAuctionRecord(Map<String,Object> map);
}
