package com.stylefeng.guns.modular.system.dao;

import com.stylefeng.guns.modular.system.model.AppSetGift;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 礼物设置 Mapper 接口
 * </p>
 *
 * @author stylefeng123
 * @since 2019-02-28
 */
public interface AppSetGiftMapper extends BaseMapper<AppSetGift> {
    /**
     *获取系统设置礼物和宝箱数据
     * @param map
     * @return
     */
    List<Map<String,Object>> getAppSetGiftAndBox(Map<String,Object> map);
}
