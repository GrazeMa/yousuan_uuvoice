package com.stylefeng.guns.modular.system.dao;

import com.stylefeng.guns.modular.system.model.AppChatrooms;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 加入聊天室的 Mapper 接口
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-07
 */
public interface AppChatroomsMapper extends BaseMapper<AppChatrooms> {
    /**
     * 获取加入聊天室的数据
     * @param objectMap
     * @return
     */
    List<Map<String,Object>> getAppChatrooms(Map<String,Object> objectMap);
}
