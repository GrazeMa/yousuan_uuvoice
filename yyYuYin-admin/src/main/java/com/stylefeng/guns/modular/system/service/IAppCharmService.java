package com.stylefeng.guns.modular.system.service;

import com.stylefeng.guns.modular.system.model.AppCharm;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 房间魅力榜 服务类
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-11
 */
public interface IAppCharmService extends IService<AppCharm> {
    /**
     * 获取魅力榜
     * @param map
     * @return
     */
    List<Map<String,Object>> getAppCharm(Map<String,Object> map);
}
