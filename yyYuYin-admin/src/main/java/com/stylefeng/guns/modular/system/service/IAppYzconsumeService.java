package com.stylefeng.guns.modular.system.service;

import com.stylefeng.guns.modular.system.model.AppYzconsume;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 优钻获取记录 服务类
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-14
 */
public interface IAppYzconsumeService extends IService<AppYzconsume> {

}
