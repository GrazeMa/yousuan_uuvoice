package com.stylefeng.guns.modular.system.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.common.constant.factory.PageFactory;
import com.stylefeng.guns.modular.system.model.AppReport;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;
import com.stylefeng.guns.modular.system.model.AppFeedback;
import com.stylefeng.guns.modular.system.service.IAppFeedbackService;

import java.util.List;
import java.util.Map;

/**
 * 反馈管理控制器
 * @Date 2019-03-13 11:16:06
 */
@Controller
@RequestMapping("/appFeedback")
public class AppFeedbackController extends BaseController {

    private String PREFIX = "/system/appFeedback/";

    @Autowired
    private IAppFeedbackService appFeedbackService;

    /**
     * 跳转到反馈管理首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "appFeedback.html";
    }

    /**
     * 跳转到添加反馈管理
     */
    @RequestMapping("/appFeedback_add")
    public String appFeedbackAdd() {
        return PREFIX + "appFeedback_add.html";
    }

    /**
     * 跳转到修改反馈管理
     */
    @RequestMapping("/appFeedback_update/{appFeedbackId}")
    public String appFeedbackUpdate(@PathVariable Integer appFeedbackId, Model model) {
        AppFeedback appFeedback = appFeedbackService.selectById(appFeedbackId);
        model.addAttribute("item",appFeedback);
        LogObjectHolder.me().set(appFeedback);
        return PREFIX + "appFeedback_edit.html";
    }

    /**
     * 获取反馈管理列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(AppFeedback appFeedback) {
        Page<Map<String,Object>> page = new PageFactory<Map<String,Object>>().defaultPage();
        List<Map<String, Object>> result = appFeedbackService.getAppFeedback(appFeedback,page );
        page.setRecords(result);
        return super.packForBT(page);
    }

    /**
     * 新增反馈管理
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(AppFeedback appFeedback) {
        appFeedbackService.insert(appFeedback);
        return SUCCESS_TIP;
    }

    /**
     * 删除反馈管理
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(String appFeedbackId) {
        EntityWrapper<AppFeedback> entityWrapper=new EntityWrapper<>();
        entityWrapper.in("id",appFeedbackId);
        entityWrapper.eq("status",2);//已经处理了的
        appFeedbackService.delete(entityWrapper);
        return SUCCESS_TIP;
    }

    /**
     * 修改反馈管理
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(AppFeedback appFeedback) {
        appFeedbackService.updateById(appFeedback);
        return SUCCESS_TIP;
    }

    /**
     * 反馈管理详情
     */
    @RequestMapping(value = "/detail/{appFeedbackId}")
    @ResponseBody
    public Object detail(@PathVariable("appFeedbackId") Integer appFeedbackId) {
        return appFeedbackService.selectById(appFeedbackId);
    }
}
