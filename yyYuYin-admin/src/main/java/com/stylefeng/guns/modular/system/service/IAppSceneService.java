package com.stylefeng.guns.modular.system.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.system.model.AppScene;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 道具管理 服务类
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-01
 */
public interface IAppSceneService extends IService<AppScene> {
    /**
     * 获取道具名称，下拉列表获取数据
     * @param map
     * @return
     */
    List<Map<String,Object>> getAppSceneName(Map<String,Object> map);
    /**
     * 获取道具列表数据
     * @param appScene
     * @return
     */
    List<Map<String,Object>> getAppSceneList(AppScene appScene, Page<Map<String,Object>> page);
}
