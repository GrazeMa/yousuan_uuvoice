package com.stylefeng.guns.modular.system.service;

import com.stylefeng.guns.modular.system.model.AppSetRecharge;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 充值设置 服务类
 * </p>
 *
 * @author stylefeng123
 * @since 2019-02-28
 */
public interface IAppSetRechargeService extends IService<AppSetRecharge> {

}
