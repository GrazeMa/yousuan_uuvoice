package com.stylefeng.guns.modular.system.service.impl;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.system.model.AppGuild;
import com.stylefeng.guns.modular.system.dao.AppGuildMapper;
import com.stylefeng.guns.modular.system.service.IAppGuildService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
/**
 * @Description 公会服务实现类。
 * @author Grazer_Ma
 * @Date 2020-05-07 09:06:22
 */
@Service
public class AppGuildServiceImpl extends ServiceImpl<AppGuildMapper, AppGuild> implements IAppGuildService {
    @Override
    public List<Map<String, Object>> getAppGuild(AppGuild appGuild, Page<Map<String,Object>> page) {
        return this.baseMapper.getAppGuild(appGuild, page);
    }
}
