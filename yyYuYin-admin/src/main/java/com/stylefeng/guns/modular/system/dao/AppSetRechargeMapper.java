package com.stylefeng.guns.modular.system.dao;

import com.stylefeng.guns.modular.system.model.AppSetRecharge;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 充值设置 Mapper 接口
 * </p>
 *
 * @author stylefeng123
 * @since 2019-02-28
 */
public interface AppSetRechargeMapper extends BaseMapper<AppSetRecharge> {

}
