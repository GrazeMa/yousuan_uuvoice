package com.stylefeng.guns.modular.system.dao;

import com.stylefeng.guns.modular.system.model.AppInvite;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 邀请用户（邀请明细） Mapper 接口
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-06
 */
public interface AppInviteMapper extends BaseMapper<AppInvite> {
    /**
     * 获取邀请用户的奖励金额
     * @param appInvite
     * @return
     */
    List<Map<String,Object>> getAppInviteSum(AppInvite appInvite);
}
