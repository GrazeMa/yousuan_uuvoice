package com.stylefeng.guns.modular.system.dao;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.system.model.AppFeedback;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 反馈 Mapper 接口
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-13
 */
public interface AppFeedbackMapper extends BaseMapper<AppFeedback> {
    /**
     *获取反馈信息
     * @param appFeedback
     * @return
     */
    List<Map<String,Object>> getAppFeedback(AppFeedback appFeedback,Page<Map<String,Object>> page);
}
