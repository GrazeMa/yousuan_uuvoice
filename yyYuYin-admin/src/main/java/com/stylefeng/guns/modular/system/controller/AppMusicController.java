package com.stylefeng.guns.modular.system.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.common.constant.factory.PageFactory;
import com.stylefeng.guns.core.util.OssUploadUtil;
import com.stylefeng.guns.core.util.SinataUtil;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;
import com.stylefeng.guns.modular.system.model.AppMusic;
import com.stylefeng.guns.modular.system.service.IAppMusicService;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 音乐管理控制器
 * @Date 2019-03-12 11:22:33
 */
@Controller
@RequestMapping("/appMusic")
public class AppMusicController extends BaseController {

    private String PREFIX = "/system/appMusic/";

    @Autowired
    private IAppMusicService appMusicService;

    /**
     * 跳转到音乐管理首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "appMusic.html";
    }

    @RequestMapping("/audit")
    public String auditIndex() {
        return PREFIX + "appMusicAudit.html";
    }

    /**
     * 跳转到添加音乐管理
     */
    @RequestMapping("/appMusic_add")
    public String appMusicAdd() {
        return PREFIX + "appMusic_add.html";
    }

    /**
     * 跳转到修改音乐管理
     */
    @RequestMapping("/appMusic_update/{appMusicId}")
    public String appMusicUpdate(@PathVariable Integer appMusicId, Model model) {
        AppMusic appMusic = appMusicService.selectById(appMusicId);
        model.addAttribute("item",appMusic);
        LogObjectHolder.me().set(appMusic);
        return PREFIX + "appMusic_edit.html";
    }

    /**
     * 获取音乐管理列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(AppMusic appMusic,String beginTime,String endTime) {
        Wrapper entityWrapper=new EntityWrapper<AppMusic>();
        entityWrapper.eq("isDelete",1);
        if(SinataUtil.isNotEmpty(appMusic.getUid())){
            entityWrapper.eq("uid",appMusic.getUid());
        }
        if(SinataUtil.isNotEmpty(appMusic.getGsNane())){
            entityWrapper.like("gsNane",appMusic.getGsNane());
        }
        if(SinataUtil.isNotEmpty(appMusic.getMusicName())){
            entityWrapper.like("musicName",appMusic.getMusicName());
        }
        /*1 未审核， 2 审核通过 ，3已拒绝*/
        if(SinataUtil.isNotEmpty(appMusic.getState())){
            entityWrapper.eq("state",appMusic.getState());
        }
        /*是否上架，1否，2是*/
        if(SinataUtil.isNotEmpty(appMusic.getStatus())){
            entityWrapper.eq("status",appMusic.getStatus());
        }
        if(SinataUtil.isNotEmpty(beginTime)){
            entityWrapper.ge("createDate",beginTime +" 00:00:00");
        }
        if(SinataUtil.isNotEmpty(endTime)){
            entityWrapper.le("createDate",endTime +" 23:59:59");
        }
        entityWrapper.orderBy("createDate",false);

        Page<AppMusic> page = new PageFactory<AppMusic>().defaultPage();
        page.setRecords(appMusicService.selectMapsPage(page, entityWrapper).getRecords());
        return super.packForBT(page);
    }

    /**
     * 新增音乐管理
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(AppMusic appMusic) {
        appMusic.setCreateDate(new Date());
        appMusic.setUid(0);
        appMusic.setNickName("系统管理员");
        appMusic.setState(2);//审核通过
        appMusicService.insert(appMusic);
        return SUCCESS_TIP;
    }

    /**
     * 删除音乐管理
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    @Transactional
    public Object delete(String appMusicId) {
        EntityWrapper<AppMusic> entityWrapper=new EntityWrapper<>();
        entityWrapper.in("id",appMusicId);
        AppMusic appMusic=new AppMusic();
        appMusic.setIsDelete(2);
        appMusicService.update(appMusic,entityWrapper);
        List<AppMusic> appMusicList=appMusicService.selectList(entityWrapper);
        List<String> urls=new ArrayList<>();
        for(AppMusic appMusic1:appMusicList){
            if(SinataUtil.isNotEmpty(appMusic1.getUrl()) && appMusic1.getUrl().contains("http://yoyovoice.oss-cn-shanghai.aliyuncs.com")){
                urls.add(appMusic1.getUrl());
            }
        }
        //删除oss上的音频文件
        if(urls.size()>0){
            OssUploadUtil.deleteObjects(urls);
        }
        return SUCCESS_TIP;
    }

    /**
     * 修改音乐管理
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(AppMusic appMusic) {
        appMusicService.updateById(appMusic);
        return SUCCESS_TIP;
    }
    /**
     * 修改音乐 上下架
     */
    @RequestMapping(value = "/updateXiajia")
    @ResponseBody
    public Object updateXiajia(String ids,Integer status) {
        EntityWrapper<AppMusic> entityWrapper=new EntityWrapper<>();
        entityWrapper.in("id",ids);
        AppMusic appMusic=new AppMusic();
        appMusic.setStatus(status);
        appMusicService.update(appMusic,entityWrapper);
        return SUCCESS_TIP;
    }
    /**
     * 音乐管理详情
     */
    @RequestMapping(value = "/detail/{appMusicId}")
    @ResponseBody
    public Object detail(@PathVariable("appMusicId") Integer appMusicId) {
        return appMusicService.selectById(appMusicId);
    }
}
