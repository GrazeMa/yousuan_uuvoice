package com.stylefeng.guns.modular.system.dao;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.system.model.AppUser;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 用户 Mapper 接口
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-01
 */
public interface AppUserMapper extends BaseMapper<AppUser> {
    /**
     * 获取用户列表数据
     * @param appUser
     * @return
     */
    List<Map<String,Object>> getAppUserList(AppUser appUser, Page<Map<String,Object>> page);

    /**
     * 根据用户id获取用户的详情
     * @param map
     * @return
     */
    List<Map<String,Object>> getAppUserOne(Map<String,Object> map);

    /**
     *=======================首页统计数据===========================
     * @param map
     * @return
     */
    List<Map<String,Object>> getUserCount(Map<String,Object> map);
    /**
     * 用户注册趋势
     * @param paramMap
     * @return
     */
    List<Map<String,Object>> getUserStatic(Map<String, Object> paramMap);

    /**
     * 用户日活跃度 趋势
     * @param paramMap
     * @return
     */
    List<Map<String,Object>> getUserStaticLogin(Map<String, Object> paramMap);

    /**
     * 优币充值 趋势
     * @param paramMap
     * @return
     */
    List<Map<String,Object>> getChongzhi(Map<String, Object> paramMap);

    /**
     * 平台收入 趋势
     * @param paramMap
     * @return
     */
    List<Map<String,Object>> getShouru(Map<String, Object> paramMap);
    /**
     * 用户注册城市分布图
     * @param paramMap
     * @return
     */
    List<Map<String,Object>> userByCity(Map<String, Object> paramMap);

    /**
     * 用户性别分布图
     * @param paramMap
     * @return
     */
    List<Map<String,Object>> userBySix(Map<String, Object> paramMap);

    /**
     * 用户年龄分布图
     * @param paramMap
     * @return
     */
    List<Map<String,Object>> userByAge(Map<String, Object> paramMap);
    /**
     * 用户活跃度分布图
     * @param paramMap
     * @return
     */
    List<Map<String,Object>> userByHuo(Map<String, Object> paramMap);

    /*=====================财务分析==================*/
    /**
     * 充值优币
     * @param paramMap
     * @return
     */
    Map<String,Object> financialStatistics(Map<String, Object> paramMap);

    /**
     * 平台收入
     * @param paramMap
     * @return
     */
    Map<String,Object> financialStatistics_(Map<String, Object> paramMap);

    /**
     * 平台净收入
     * @param paramMap
     * @return
     */
    Map<String,Object> financialStatisticsJ(Map<String, Object> paramMap);
}
