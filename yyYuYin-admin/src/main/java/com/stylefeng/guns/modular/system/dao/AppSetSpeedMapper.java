package com.stylefeng.guns.modular.system.dao;

import com.stylefeng.guns.modular.system.model.AppSetSpeed;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 速配/认证/语句设置 Mapper 接口
 * </p>
 *
 * @author stylefeng123
 * @since 2019-02-28
 */
public interface AppSetSpeedMapper extends BaseMapper<AppSetSpeed> {

}
