package com.stylefeng.guns.modular.system.service.impl;

import com.stylefeng.guns.core.util.RandomNumberGeneratorUtil;
import com.stylefeng.guns.core.util.SinataUtil;
import com.stylefeng.guns.modular.system.model.AppAgreement;
import com.stylefeng.guns.modular.system.dao.AppAgreementMapper;
import com.stylefeng.guns.modular.system.service.IAppAgreementService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 协议号 服务实现类
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-01
 */
@Service
public class AppAgreementServiceImpl extends ServiceImpl<AppAgreementMapper, AppAgreement> implements IAppAgreementService {
    @Override
    public boolean batchImport(String fileName, MultipartFile file) throws Exception {

        boolean notNull = false;
        List<AppAgreement> userList = new ArrayList<AppAgreement>();
        if (!fileName.matches("^.+\\.(?i)(xls)$") && !fileName.matches("^.+\\.(?i)(xlsx)$")) {
            throw new Exception("上传文件格式不正确");
        }
        boolean isExcel2003 = true;
        if (fileName.matches("^.+\\.(?i)(xlsx)$")) {
            isExcel2003 = false;
        }
        InputStream is = file.getInputStream();
        Workbook wb = null;
        if (isExcel2003) {
            wb = new HSSFWorkbook(is);
        } else {
            wb = new XSSFWorkbook(is);
        }
        Sheet sheet = wb.getSheetAt(0);
        if (sheet != null) {
            notNull = true;
        }
        AppAgreement appAgreement;
        for (int r = 1; r <= sheet.getLastRowNum(); r++) {
            Row row = sheet.getRow(r);
            if (row == null) {
                continue;
            }
            appAgreement = new AppAgreement();
            Integer id = 0;
            String img = "";
            String nickname = "";
            Date date = new Date();
            Integer sex = 1;
            String individuation = "";
            for (int c = 0; c < 6; c++) {
                Cell cell = row.getCell(c);
                if (c == 0) {//第一个值
                    if (cell.getCellType() == HSSFCell.CELL_TYPE_NUMERIC) {
                        id = (int) cell.getNumericCellValue();
                    }
                }
                if (c == 1) {//第二个值
                    if (cell.getCellType() == HSSFCell.CELL_TYPE_STRING) {
                        img = cell.getStringCellValue();
                    }
                }
                if (c== 2) {//第三个值
                    if (cell.getCellType() == HSSFCell.CELL_TYPE_STRING) {
                        nickname = cell.getStringCellValue();
                    }
                }
                //判断是否为日期类型
                if (c == 3) {//第四个值
                    if (0 == cell.getCellType()) {
                        if (DateUtil.isCellDateFormatted(cell)) {
                            //用于转化为日期格式
                            date = cell.getDateCellValue();
                        }
                    }
                }
                if (c == 4) {//第五个值
                    String sexName = "男";
                    if (cell.getCellType() == HSSFCell.CELL_TYPE_STRING) {
                        sexName = cell.getStringCellValue();
                    }
                    if (sexName.equals("女")) {
                        sex = 2;
                    }
                }
                if (c == 5) {//第六个值
                    if (cell.getCellType() == HSSFCell.CELL_TYPE_STRING) {
                        individuation = cell.getStringCellValue();
                    }
                }
            }
            appAgreement.setIndex(id);
            appAgreement.setImg(img);
            appAgreement.setNickname(nickname);
            appAgreement.setDateOfBirth(date);
            appAgreement.setSex(sex);
            appAgreement.setIndividuation(individuation);
            appAgreement.setCreateTime(new Date());
            StringBuilder app = new StringBuilder();
            String num=RandomNumberGeneratorUtil.generateNumber();
            app.append(num);
            appAgreement.setUsercoding(app.toString());
            this.baseMapper.insert(appAgreement);
        }
        return  notNull;
    }

    @Override
    public Integer updateIncome(Map map) {
        return this.baseMapper.updateIncome(map);
    }
}
