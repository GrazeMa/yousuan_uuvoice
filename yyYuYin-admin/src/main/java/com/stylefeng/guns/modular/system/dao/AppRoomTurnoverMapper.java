package com.stylefeng.guns.modular.system.dao;

import com.stylefeng.guns.modular.system.model.AppRoom;
import com.stylefeng.guns.modular.system.model.AppYbconsume;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

/**
 * @Description 聊天室收益 Mapper 接口。
 * @author Grazer_Ma
 * @Date 2020-05-15 09:52:30
 */
public interface AppRoomTurnoverMapper extends BaseMapper<AppYbconsume> {
    /**
     * 获取聊天室收益数据
     * @param appRoom
     * @return
     */
    List<Map<String,Object>> getAppRoomTurnover(AppRoom appRoom);
}
