package com.stylefeng.guns.modular.system.controller;

import com.stylefeng.guns.config.properties.GunsProperties;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.util.SinataUtil;
import com.stylefeng.guns.modular.system.model.*;
import com.stylefeng.guns.modular.system.service.*;
import net.sf.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

/**
 * 系统设置控制器
 * @Date 2018-06-19 14:57:56
 */
@Controller
@RequestMapping("/setInfo")
public class SetInfoController extends BaseController {

    private String PREFIX = "/system/setInfo/";

    @Autowired
    private GunsProperties gunsProperties;

    @Autowired
    private ISetInfoService setInfoService;
    @Autowired
    private IAppAwardService appAwardService;
    @Autowired
    private IAppSetGiftService appSetGiftService;
    @Autowired
    private IAppSetShareService appSetShareService;
    @Autowired
    private IAppSetSpeedService appSetSpeedService;
    @Autowired
    private IAppSetGradeService appSetGradeService;
    @Autowired
    private IAppSetWithdrawService appSetWithdrawService;
    @Autowired
    private IAppSetRechargeService appSetRechargeService;
    @Autowired
    private IAppSignService appSignService;
    @Autowired
    private IAppProtocolService appProtocolService;
    @Autowired
    private IAppLotteryService appLotteryService;
    @Autowired
    private IAppLiangDivideService appLiangDivideService;

    /**
     * 跳转到系统设置首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "setInfo.html";
    }

    /**
     * 跳转到添加系统设置
     */
    @RequestMapping("/setInfo_add")
    public String setInfoAdd() {
        return PREFIX + "setInfo_add.html";
    }

    /**
     * 跳转到修改系统设置
     */
    @RequestMapping("/setInfo_update/{setInfoId}")
    public String setInfoUpdate(@PathVariable Integer setInfoId, Model model) {
        SetInfo setInfo = setInfoService.selectById(setInfoId);
        model.addAttribute("item",setInfo);
        LogObjectHolder.me().set(setInfo);
        return PREFIX + "setInfo_edit.html";
    }

    /**
     * 获取系统设置列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        return setInfoService.selectList(null);
    }

    /**
     * 新增系统设置
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(SetInfo setInfo) {
        setInfoService.insert(setInfo);
        return SUCCESS_TIP;
    }

    /**
     * 删除系统设置
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer setInfoId) {
        setInfoService.deleteById(setInfoId);
        return SUCCESS_TIP;
    }

    /**
     * 修改各种html代码
     */
    /**
     * 修改各种html代码
     */
    @RequestMapping(value = "/update1")
    @ResponseBody
    public Object update1(SetInfo setInfo) {
        setInfoService.updateById(setInfo);
        return SUCCESS_TIP;
    }



    /**
     * 修改系统设置
     *
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    @Transactional
    public Object update(Integer type,String content,String content1) {
        if(type==1){//优币奖励
            //签到
            String[] contents=content.split(";");
            for(String con:contents){
                JSONObject jsonObject = JSONObject.fromObject(con);
                Integer id = jsonObject.getInt("id");
                Integer x = jsonObject.getInt("x");
                AppSign appSign=new AppSign();
                appSign.setId(id);
                appSign.setX(x);
                appSignService.updateById(appSign);
            }
            //优币兑换
            String[] contents1=content1.split(";");
            for(String con:contents1){
                JSONObject jsonObject = JSONObject.fromObject(con);
                Integer id = jsonObject.getInt("id");
                Integer x = jsonObject.getInt("x");
                Integer y = jsonObject.getInt("y");
                AppAward appAward=new AppAward();
                appAward.setId(id);
                appAward.setX(x);
                appAward.setY(y);
                appAwardService.updateById(appAward);
            }

        }else if(type==2){//礼物和宝箱设置
            //礼物
            String[] contents=content.split(";");
            for(String con:contents){
                JSONObject jsonObject = JSONObject.fromObject(con);
                Integer id = jsonObject.getInt("id");
                Integer x = jsonObject.getInt("x");
                AppSetGift appSetGift=new AppSetGift();
                appSetGift.setId(id);
                appSetGift.setNum(x);
                appSetGiftService.updateById(appSetGift);
            }
            //宝箱设置
            String[] contents1=content1.split(";");
            for(String con:contents1){
                JSONObject jsonObject = JSONObject.fromObject(con);
                Integer id = jsonObject.getInt("id");
                String x = jsonObject.getString("x");
                String y = jsonObject.getString("y");
                String z = jsonObject.getString("z");
                String h = jsonObject.getString("h");
                AppLottery appLottery=new AppLottery();
                appLottery.setId(id);
                if(!x.equals("-1")){
                    appLottery.setType(Integer.valueOf(x));
                }
                if(!y.equals("-1")) {
                    appLottery.setGid(Integer.valueOf(y));
                }
                appLottery.setRatio(Double.valueOf(z));
                appLottery.setgName(h);
                appLotteryService.updateById(appLottery);
            }

        }else if(type==3){//分享赠送
            String[] contents=content.split(";");
            for(String con:contents){
                JSONObject jsonObject = JSONObject.fromObject(con);
                Integer id = jsonObject.getInt("id");
                Integer x = jsonObject.getInt("x");
                Integer y = jsonObject.getInt("y");
                AppSetShare appSetShare=new AppSetShare();
                appSetShare.setId(id);
                appSetShare.setX(x);
                if(y==-1){
                    appSetShare.setY(null);
                }else{
                    appSetShare.setY(y);
                }
                appSetShareService.updateById(appSetShare);
            }
        }else if(type==4){//速配设置
            String[] contents=content.split(";");
            for(String con:contents) {
                JSONObject jsonObject = JSONObject.fromObject(con);
                Integer id = jsonObject.getInt("id");
                String x = jsonObject.getString("x");
                AppSetSpeed appSetSpeed = new AppSetSpeed();
                appSetSpeed.setId(id);
                appSetSpeed.setX(x);
                appSetSpeedService.updateById(appSetSpeed);
            }
        }else if(type==5){//等级设置
            String[] contents=content.split(";");
            for(String con:contents) {
                JSONObject jsonObject = JSONObject.fromObject(con);
                Integer id = jsonObject.getInt("id");
                Integer x = jsonObject.getInt("x");
                Integer y = jsonObject.getInt("y");
                Integer z = jsonObject.getInt("z");
                Integer p = jsonObject.getInt("p");
                Integer b = jsonObject.getInt("b");
                AppSetGrade appSetGrade = new AppSetGrade();
                appSetGrade.setId(id);
                appSetGrade.setX(x);
                appSetGrade.setY(y);
                appSetGrade.setZ(z);
                appSetGrade.setP(p);
                appSetGrade.setB(b);
                appSetGradeService.updateById(appSetGrade);
            }
            String[] contents1=content1.split(";");
            for(String con:contents1) {
                JSONObject jsonObject = JSONObject.fromObject(con);
                Integer id = jsonObject.getInt("id");
                Integer x = jsonObject.getInt("x");
                Integer y = jsonObject.getInt("y");
                Integer z = jsonObject.getInt("z");
                Integer p = jsonObject.getInt("p");
                Integer b = jsonObject.getInt("b");
                AppSetGrade appSetGrade = new AppSetGrade();
                appSetGrade.setId(10+id);
                appSetGrade.setX(x);
                appSetGrade.setY(y);
                appSetGrade.setZ(z);
                appSetGrade.setP(p);
                appSetGrade.setB(b);
                appSetGradeService.updateById(appSetGrade);
            }
        }else if(type==6){//提现设置
            String[] contents=content.split(";");
            for(String con:contents) {
                JSONObject jsonObject = JSONObject.fromObject(con);
                Integer id = jsonObject.getInt("id");
                Integer x = jsonObject.getInt("x");
                Integer y = jsonObject.getInt("y");
                AppSetWithdraw appSetWithdraw = new AppSetWithdraw();
                appSetWithdraw.setId(id);
                appSetWithdraw.setX(x);
                appSetWithdraw.setY(y);
                appSetWithdrawService.updateById(appSetWithdraw);
            }
        }else if(type==7){//充值设置
            String[] contents=content.split(";");
            for(String con:contents) {
                JSONObject jsonObject = JSONObject.fromObject(con);
                Integer id = jsonObject.getInt("id");
                Integer x = jsonObject.getInt("x");
                double y = jsonObject.getDouble("y");
                AppSetRecharge appSetRecharge = new AppSetRecharge();
                appSetRecharge.setId(id);
                appSetRecharge.setX(x);
                if(y==-1){
                    appSetRecharge.setY(null);
                }else{
                    appSetRecharge.setY(y);
                }
                appSetRechargeService.updateById(appSetRecharge);
            }
        }else if(type==8){//分成设置
            String[] contents=content.split(";");
            for(String con:contents) {
                JSONObject jsonObject = JSONObject.fromObject(con);
                Integer id = jsonObject.getInt("id");
                Double liang = jsonObject.getDouble("liang");//百分数
                AppLiangDivide appLiangDivide=new AppLiangDivide();
                appLiangDivide.setId(id);
                appLiangDivide.setLiang(liang/100);//百分数需要转成小数
                appLiangDivideService.updateById(appLiangDivide);
            }
        }
        return SUCCESS_TIP;
    }

    /**
     * 系统设置详情
     */
    @RequestMapping(value = "/detail/{setInfoId}")
    @ResponseBody
    public Object detail(@PathVariable("setInfoId") Integer setInfoId) {
        return setInfoService.selectById(setInfoId);
    }

    /**
     * 跳转到修改相关介绍页
     */
    @RequestMapping("/html")
    public String html(Model model) {
        //model.addAttribute("item",appProtocolService.getAppProtocol());
        return PREFIX + "html.html";
    }

    /**
     * 修改相关介绍页面内容
     */

    @RequestMapping(value = "/updateHtml")
    @ResponseBody
    public Object updateHtml(AppProtocol appProtocol) {
        if(SinataUtil.isNotEmpty(appProtocol.getId())){
            appProtocolService.updateById(appProtocol);
        }else {
            appProtocolService.insert(appProtocol);
        }
        return SUCCESS_TIP;
    }

    /**
     * 获取设置列表
     */
    @RequestMapping(value = "/getHtmlContentById")
    @ResponseBody
    public Object list(Integer type) {
        AppProtocol appProtocol=appProtocolService.selectById(type);
        return appProtocol;
    }

    /**
     * 根据id获取数据
     * @param type
     * @return
     */
    @RequestMapping("/getContentByType")
    @ResponseBody
    public Object getYxjyAgreementByType(Integer type) {
        if(type==1){//优币奖励
            List<Map<String,Object>> mapList = appSignService.getAppSignAndAppAward(null);
            return mapList;
        }else if(type==2){//礼物和宝箱设置
            List<Map<String,Object>> appSetGifts=appSetGiftService.getAppSetGiftAndBox(null);
            return  appSetGifts;
        }else if(type==3){//分享赠送
            List<AppSetShare> appSetShares=appSetShareService.selectList(null);
            return appSetShares;
        }else if(type==4){//速配设置
            List<AppSetSpeed> appSetSpeeds=appSetSpeedService.selectList(null);
            return appSetSpeeds;
        }else if(type==5){//等级设置
            List<AppSetGrade> appSetGrades=appSetGradeService.selectList(null);
            return appSetGrades;
        }else if(type==6){//提现设置
            List<AppSetWithdraw> appSetWithdraws=appSetWithdrawService.selectList(null);
            return appSetWithdraws;
        }else if(type==7){//充值设置
            List<AppSetRecharge> appSetRecharges=appSetRechargeService.selectList(null);
            return appSetRecharges;
        }else if(type==8){//分成设置
            List<AppLiangDivide> appSetRecharges=appLiangDivideService.selectList(null);
            return appSetRecharges;
        }
        return null;
    }
}
