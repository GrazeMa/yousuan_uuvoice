package com.stylefeng.guns.modular.system.service;

import com.stylefeng.guns.modular.system.model.AppAgreement;
import com.baomidou.mybatisplus.service.IService;
import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

/**
 * <p>
 * 协议号 服务类
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-01
 */
public interface IAppAgreementService extends IService<AppAgreement> {
    /**
     * 导入协议号
     * @param fileName
     * @param file
     * @return
     * @throws Exception
     */
    boolean batchImport(String fileName, MultipartFile file) throws Exception;
    /**
     * 召回协议号
     * @param map
     * @return
     */
    Integer updateIncome(Map map);
}
