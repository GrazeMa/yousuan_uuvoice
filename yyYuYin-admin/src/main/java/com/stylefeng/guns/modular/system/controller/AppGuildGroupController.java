package com.stylefeng.guns.modular.system.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.common.constant.factory.ConstantFactory;
import com.stylefeng.guns.core.common.constant.factory.PageFactory;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.core.shiro.ShiroKit;
import com.stylefeng.guns.core.shiro.ShiroUser;
import com.stylefeng.guns.modular.system.model.AppFeedback;
import com.stylefeng.guns.modular.system.model.AppGuildGroup;
import com.stylefeng.guns.modular.system.service.IAppGuildGroupService;

/**
 * @Description 公会组管理控制器。
 * @author Grazer_Ma
 * @Date 2020-05-07 21:00:28
 */
@Controller
@RequestMapping("/appGuildGroup")
public class AppGuildGroupController extends BaseController {

    private String PREFIX = "/system/appGuildGroup/";

    @Autowired
    private IAppGuildGroupService appGuildGroupService;

    /**
     * 跳转到公会管理首页。
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "appGuildGroup.html";
    }
    
    /**
     * 跳转到添加公会管理页。
     */
    @RequestMapping("/appGuildGroup_add")
    public String appGuildGroupAdd() {
        return PREFIX + "appGuildGroup_add.html";
    }

    /**
     * 获取公会组管理列表。
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(AppGuildGroup appGuildGroup) {
        Page<Map<String,Object>> page = new PageFactory<Map<String,Object>>().defaultPage();
        List<Map<String, Object>> result = appGuildGroupService.getAppGuildGroup(appGuildGroup, page);
        page.setRecords(result);
        return super.packForBT(page);
    }
    
    /**
     * 跳转到修改公会组。
     */
    @RequestMapping("/appGuildGroup_update/{appGuildGroupId}")
    public String appGuildGroupUpdate(@PathVariable Integer appGuildGroupId, Model model) {
    	AppGuildGroup appGuildGroup = appGuildGroupService.selectById(appGuildGroupId);
        model.addAttribute("item", appGuildGroup);
        LogObjectHolder.me().set(appGuildGroup);
        return PREFIX + "appGuildGroup_edit.html";
    }
    
    /**
     * 新增公会组管理。
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(AppGuildGroup appGuildGroup) {
    	appGuildGroup.setGroupId(ShiroKit.getUser().getAccount());
    	appGuildGroup.setStatus(1);
    	appGuildGroupService.insert(appGuildGroup);
        return SUCCESS_TIP;
    }
    
    /**
     * 修改公会组管理。
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(AppGuildGroup appGuildGroup) {
    	appGuildGroupService.updateById(appGuildGroup);
        return SUCCESS_TIP;
    }
    
	/**
	 * 删除公会组管理
	 */
	@RequestMapping(value = "/delete")
	@ResponseBody
	public Object delete(String appGuildGroupId) {
		EntityWrapper<AppGuildGroup> entityWrapper = new EntityWrapper<>();
		entityWrapper.in("id", appGuildGroupId);
		AppGuildGroup appGuildGroup = new AppGuildGroup();
		appGuildGroup.setStatus(0);
		appGuildGroupService.update(appGuildGroup, entityWrapper);
		return SUCCESS_TIP;
	}
    
}
