package com.stylefeng.guns.modular.system.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.modular.system.model.AppBackdrop;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;
import com.stylefeng.guns.modular.system.model.AppPk;
import com.stylefeng.guns.modular.system.service.IAppPkService;

/**
 * 房间pk控制器
 * @Date 2019-03-08 16:26:57
 */
@Controller
@RequestMapping("/appPk")
public class AppPkController extends BaseController {

    private String PREFIX = "/system/appPk/";

    @Autowired
    private IAppPkService appPkService;

    /**
     * 跳转到房间pk首页
     */
    @RequestMapping("")
    public String index(Integer rid,Model model) {
        model.addAttribute("rid",rid);
        return PREFIX + "appPk.html";
    }

    /**
     * 跳转到添加房间pk
     */
    @RequestMapping("/appPk_add")
    public String appPkAdd() {
        return PREFIX + "appPk_add.html";
    }

    /**
     * 跳转到修改房间pk
     */
    @RequestMapping("/appPk_update/{appPkId}")
    public String appPkUpdate(@PathVariable Integer appPkId, Model model) {
        AppPk appPk = appPkService.selectById(appPkId);
        model.addAttribute("item",appPk);
        LogObjectHolder.me().set(appPk);
        return PREFIX + "appPk_edit.html";
    }

    /**
     * 获取房间pk列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(AppPk appPk) {
        return appPkService.getAppPkList(appPk);
    }

    /**
     * 新增房间pk
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(AppPk appPk) {
        appPkService.insert(appPk);
        return SUCCESS_TIP;
    }

    /**
     * 删除房间pk
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer appPkId) {
        appPkService.deleteById(appPkId);
        return SUCCESS_TIP;
    }

    /**
     * 修改房间pk
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(AppPk appPk) {
        appPkService.updateById(appPk);
        return SUCCESS_TIP;
    }

    /**
     * 房间pk详情
     */
    @RequestMapping(value = "/detail/{appPkId}")
    @ResponseBody
    public Object detail(@PathVariable("appPkId") Integer appPkId) {
        return appPkService.selectById(appPkId);
    }
}
