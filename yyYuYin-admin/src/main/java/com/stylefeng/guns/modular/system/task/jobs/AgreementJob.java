package com.stylefeng.guns.modular.system.task.jobs;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.stylefeng.guns.core.util.SinataUtil;
import com.stylefeng.guns.modular.system.model.*;
import com.stylefeng.guns.modular.system.task.base.AbstractJob;
import io.agora.signal.Signal;
import net.sf.json.JSONArray;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * 定时解禁讨论组
 *
 *
 */
public class AgreementJob extends AbstractJob {

	public static final String name = "AgreementJob_";

	@Override
	public void execute(JobExecutionContext context)
			throws JobExecutionException {
		JobDataMap maps = context.getMergedJobDataMap();
		Integer  id = maps.getInt("id");
		Integer timeTaskId = maps.getInt("timeTaskId");
		try{
			//修改房间里的信息 （协议号数量相应减少）
			EntityWrapper<AppAgreement> appAgreementEntityWrapper=new EntityWrapper<>();
			appAgreementEntityWrapper.eq("id",id);
			AppAgreement appAgreement=appAgreementService.selectById(id);
			if(appAgreement!=null && appAgreement.getState()==2){//已经召回了就不需要操作了（是否分配 1否，2 是）
				AppRoom appRoom=appRoomService.selectOne(new EntityWrapper<AppRoom>().eq("rid",appAgreement.getEid()));
				if(appRoom!=null){
					appRoom.setProtocolNum(appRoom.getProtocolNum()-1);
					appRoomService.updateById(appRoom);
				}
				//修改协议号数据
				Map<String,Object> map=new HashMap<>();
				map.put("ids",id);
				appAgreementService.updateIncome(map);
				//修改聊天室的数据
				EntityWrapper<AppChatrooms> chatroomsEntityWrapper=new EntityWrapper<>();
				chatroomsEntityWrapper.eq("uid",id).eq("type",4);
				AppChatrooms appChatrooms=appChatroomsService.selectOne(chatroomsEntityWrapper);
				if(appChatrooms!=null){
					if(appChatrooms.getState()==2){//如果在麦上需要推送房间消息
						this.tuisong(appChatrooms.getPid());
					}
					appChatrooms.setState(1);//下麦
					appChatrooms.setSequence(0);
					appChatrooms.setStatus(2);//是否在房间 1在，2 不再
					appChatroomsService.updateById(appChatrooms);//类型 1是房主，2 是管理员，3用户 4 协议用户
				}
			}
			//修改定时任务数据状态
			AppTimeTask timeTask=appTimeTaskService.selectById(timeTaskId);
			//0有效，1无效
			timeTask.setState(1);
			appTimeTaskService.updateById(timeTask);
		}catch(Exception e){
			logger.debug("执行“修改协议号”异常:id={}",id,e);
		}
	}
	/**
	 * 召回协议号推送信息
	 * @return
	 */
	public void tuisong(String pid){
		List<AppSeat> list = appSeatService.selectList(new EntityWrapper<AppSeat>().eq("pid",pid));
		Iterator<AppSeat> iterator = list.iterator();
		while (iterator.hasNext()) {
			AppSeat eva=iterator.next();
			EntityWrapper<AppChatrooms> chatroomsEntityWrapper=new EntityWrapper<>();
			chatroomsEntityWrapper.eq("pid",pid);
			chatroomsEntityWrapper.eq("state",2);
			chatroomsEntityWrapper.eq("status",1);
			chatroomsEntityWrapper.eq("sequence",eva.getSequence());
			AppChatrooms l = appChatroomsService.selectOne(chatroomsEntityWrapper);
			if(SinataUtil.isNotEmpty(l)){
				AppUser byID = appUserService.selectById(l.getUid());
				UserModel u1=new UserModel();
				if(byID!=null){
					u1.setId(byID.getId());
					u1.setImg(byID.getImgTx());
					u1.setName(byID.getNickname());
					u1.setSex(byID.getSex());
					u1.setSequence(l.getSequence());
					u1.setState(l.getState());
					u1.setType(l.getType());
					eva.setUserModel(u1);
				}
			}
		}
		Signal sig = new Signal(SappID);

		Signal.LoginSession loginSession=sig.login("1", "_no_need_token", new Signal.LoginCallback());
		Signal.LoginSession.Channel channel=loginSession.channelJoin(pid, new Signal.ChannelCallback());

		String json = JSONArray.fromObject(list).toString();
		channel.channelSetAttr("attr_mics",json);
	}
}
