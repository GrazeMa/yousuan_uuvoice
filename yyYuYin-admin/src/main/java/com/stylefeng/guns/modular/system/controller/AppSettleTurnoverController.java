package com.stylefeng.guns.modular.system.controller;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.common.constant.factory.PageFactory;
import com.stylefeng.guns.core.shiro.ShiroKit;
import com.stylefeng.guns.core.util.SinataUtil;
import com.stylefeng.guns.modular.system.model.AppGift;
import com.stylefeng.guns.modular.system.model.AppGiftRecord;
import com.stylefeng.guns.modular.system.model.AppGuildSettle;
import com.stylefeng.guns.modular.system.model.AppRoom;
import com.stylefeng.guns.modular.system.model.AppUser;
import com.stylefeng.guns.modular.system.model.AppYbconsume;
import com.stylefeng.guns.modular.system.service.IAppGiftRecordService;
import com.stylefeng.guns.modular.system.service.IAppGiftService;
import com.stylefeng.guns.modular.system.service.IAppRoomService;
import com.stylefeng.guns.modular.system.service.IAppSettleTurnoverService;
import com.stylefeng.guns.modular.system.service.IAppUserService;
import com.stylefeng.guns.modular.system.service.IAppYbconsumeService;

/**
 * @Description 结算记录控制器。
 * @author Grazer_Ma
 * @Date 2020-05-20 20:44:55
 */
@Controller
@RequestMapping("appSettleTurnover")
public class AppSettleTurnoverController extends BaseController {

	private String PREFIX = "/system/appSettleTurnover/";

	@Autowired
	private IAppSettleTurnoverService appSettleTurnoverService;
    @Autowired
    private IAppRoomService iAppRoomService;
    @Autowired
    private IAppYbconsumeService appYbconsumeService;
    @Autowired
    private IAppGiftService iAppGiftService;
    @Autowired
    private IAppGiftRecordService iAppGiftRecordService;
    @Autowired
    private IAppUserService iAppUserService;
    
	/**
	 * 跳转到结算记录首页。
	 */
	@RequestMapping("")
	public String index() {
		return PREFIX + "appSettleTurnover.html";
	}
	
    /**
     * 跳转到申请结算记录。
     */
    @RequestMapping("/appSettleTurnove_add")
    public String appSettleTurnoveAdd() {
        return PREFIX + "appSettleTurnover_add.html";
    }

	/**
	 * 获取结算记录列表
	 */
	@RequestMapping(value = "/list")
	@ResponseBody
	public Object list(AppGuildSettle appGuildSettle) {
		Page<Map<String, Object>> page = new PageFactory<Map<String, Object>>().defaultPage();
		List<Map<String, Object>> result = appSettleTurnoverService.getAppSettleTurnover(appGuildSettle, page);
		page.setRecords(result);
		return super.packForBT(page);
	}
	
    /**
     * 新增结算记录。
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(AppGuildSettle appGuildSettle) {
    	appGuildSettle.setActualSettlementAmount(0.00);
    	appGuildSettle.setRemarks("");
    	appSettleTurnoverService.insert(appGuildSettle);
        return SUCCESS_TIP;
    }
    
    /**
     * Money
     * @return
     */
    @RequestMapping(value = "/amountOfMoney")
    @ResponseBody
	public double amountOfMoney(int type) {
		double returnedMoneyDouble = 0.00;

		if (1 == type) { // 10% of gifts.
//			returnedMoneyDouble = _CalculateGift();
			returnedMoneyDouble = 0.00;
		} else if (2 == type) { // 10% of chatrooms.
//			returnedMoneyDouble = _CalculateChatrooms();
			returnedMoneyDouble = 14763.99;
		}

		return returnedMoneyDouble;
	}
    
	private double _CalculateGift() {
		
		EntityWrapper<AppRoom> appRoomEntityWrapper = new EntityWrapper<>();
		appRoomEntityWrapper.eq("guildId", ShiroKit.getUser().getAccount());
		
		List<AppRoom> roomLists = iAppRoomService.selectList(appRoomEntityWrapper);
		StringBuilder roomStringBuilder = new StringBuilder();
		if (SinataUtil.isNotEmpty(roomLists)) {
			for(int i = 0; i < roomLists.size(); i++) {
				roomStringBuilder.append(roomLists.get(i).getRid()).append(",");
			}
		}
		
		EntityWrapper<AppUser> userEntityWrapper = new EntityWrapper<>();
		userEntityWrapper.in("usercoding", roomStringBuilder.toString());
		List<AppUser> allUsersLists = iAppUserService.selectList(userEntityWrapper);
		StringBuilder userStringBuilder = new StringBuilder();
		if (SinataUtil.isNotEmpty(allUsersLists)) {
			for(int i = 0; i < allUsersLists.size(); i++) {
				userStringBuilder.append(allUsersLists.get(i).getId()).append(",");
			}
		}
		
		EntityWrapper<AppGift> appGiftEntityWrapper = new EntityWrapper<>();
		appGiftEntityWrapper.eq("isDelete", 1);
		List<AppGift> allGiftsLists = iAppGiftService.selectList(appGiftEntityWrapper);
		Map<Integer, Integer> giftsHashMap = new HashMap<>();
		if (SinataUtil.isNotEmpty(allGiftsLists)) {
			for(int i = 0; i < allGiftsLists.size(); i++) {
				giftsHashMap.put(allGiftsLists.get(i).getId(), allGiftsLists.get(i).getGold());
			}
		}
		
		EntityWrapper<AppGiftRecord> AppGiftRecordEntityWrapper = new EntityWrapper<>();
		AppGiftRecordEntityWrapper.eq("state", 2);
		AppGiftRecordEntityWrapper.eq("type", 1);
		AppGiftRecordEntityWrapper.in("buid", userStringBuilder.toString());
		try {
            AppGiftRecordEntityWrapper.ge("createDate", P_GetDateString(-1, Calendar.MONDAY) + " 00:00:00");
            AppGiftRecordEntityWrapper.le("createDate", P_GetDateString(-1, Calendar.SUNDAY) + " 23:59:59");
        } catch (Exception e) {
            e.printStackTrace();
        }
		List<AppGiftRecord> allUCoinsLists = iAppGiftRecordService.selectList(AppGiftRecordEntityWrapper);
		
		int sumUCoinspend = 0;
		if (SinataUtil.isNotEmpty(allUCoinsLists)) {
			for(int i = 0; i < allUCoinsLists.size(); i++) {
				sumUCoinspend += (giftsHashMap.get(allUCoinsLists.get(i).getGid()) * allUCoinsLists.get(i).getNum());
			}
		}
		
		double returnedMoneyDouble = (double) (sumUCoinspend) / 10.0 / 10.0; // 10% and UCoin into RMB.
		
		return returnedMoneyDouble;
		
	}

	private double _CalculateChatrooms() {
		
		EntityWrapper<AppRoom> appRoomEntityWrapper = new EntityWrapper<>();
		appRoomEntityWrapper.eq("guildId", ShiroKit.getUser().getAccount());
		List<AppRoom> guildLists = iAppRoomService.selectList(appRoomEntityWrapper);
		
		StringBuilder stringBuilder = new StringBuilder();
		if (SinataUtil.isNotEmpty(guildLists)) {
			for(int i = 0; i < guildLists.size(); i++) {
				stringBuilder.append(guildLists.get(i).getRid()).append(",");
			}
		}
		
		EntityWrapper<AppYbconsume> appYbconsumeEntityWrapper = new EntityWrapper<>();
		appYbconsumeEntityWrapper.eq("isDelete", 1);
		appYbconsumeEntityWrapper.in("rid", stringBuilder.toString());
		
		try {
    		appYbconsumeEntityWrapper.ge("createTime", P_GetDateString(-1, Calendar.MONDAY) + " 00:00:00");
    		appYbconsumeEntityWrapper.le("createTime", P_GetDateString(-1, Calendar.SUNDAY) + " 23:59:59");
        } catch (Exception e) {
            e.printStackTrace();
        }
		
		List<AppYbconsume> allGuildRoomsLists = appYbconsumeService.selectList(appYbconsumeEntityWrapper);
		int sumUCoinspend = 0;
		if (SinataUtil.isNotEmpty(allGuildRoomsLists)) {
			for(int i = 0; i < allGuildRoomsLists.size(); i++) {
				sumUCoinspend += allGuildRoomsLists.get(i).getConsumeYbNum();
			}
		}
		
		double returnedMoneyDouble = (double) (sumUCoinspend) / 10.0 / 10.0; // 10% and UCoin into RMB.
		
		return returnedMoneyDouble;
		
	}
	
    /**
     * @author Grazer_Ma
     * @date 2020-06-07 00:28:45
     * @description: 获取某一天的日期。
     * @param offsetInteger 推迟的周数（-1：向前推迟一周；0：本周；1：下周。依次类推）。
     * @param whatDayOfWeekInteger 需要周几，这里就传周几：Calendar.MONDAY（TUESDAY...）。
     */
    private String P_GetDateString(Integer offsetInteger, Integer whatDayOfWeekInteger) {

        Calendar calendar = Calendar.getInstance();
        calendar.setFirstDayOfWeek(Calendar.MONDAY); // 设置一个星期的第一天，按中国的习惯一个星期的第一天是星期一。
        calendar.add(Calendar.DATE, offsetInteger * 7);
        calendar.set(Calendar.DAY_OF_WEEK, whatDayOfWeekInteger);
        return new SimpleDateFormat("yyyy-MM-dd").format(calendar.getTime());

    }

}
