package com.stylefeng.guns.modular.system.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 房间魅力榜
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-11
 */
@TableName("app_charm")
public class AppCharm extends Model<AppCharm> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    private String eid;
    /**
     * 被送用户id
     */
    private Integer uid;
    /**
     * 魅力值
     */
    private Integer num;
    /**
     * '是否被删除（1=否  2=是）'
     */
    private Integer isDelete;
    /**
     * 房间id
     */
    private String rid;
    private Date createTime;
    /**
     * 送用户的id
     */
    private Integer buid;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEid() {
        return eid;
    }

    public void setEid(String eid) {
        this.eid = eid;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    public String getRid() {
        return rid;
    }

    public void setRid(String rid) {
        this.rid = rid;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getBuid() {
        return buid;
    }

    public void setBuid(Integer buid) {
        this.buid = buid;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "AppCharm{" +
        "id=" + id +
        ", eid=" + eid +
        ", uid=" + uid +
        ", num=" + num +
        ", isDelete=" + isDelete +
        ", rid=" + rid +
        ", createTime=" + createTime +
        ", buid=" + buid +
        "}";
    }
}
