package com.stylefeng.guns.modular.system.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.modular.system.model.AppDivide;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;
import com.stylefeng.guns.modular.system.model.AppInvite;
import com.stylefeng.guns.modular.system.service.IAppInviteService;

import java.util.List;
import java.util.Map;

/**
 * 用户邀请明细控制器
 * @Date 2019-03-06 18:11:13
 */
@Controller
@RequestMapping("/appInvite")
public class AppInviteController extends BaseController {

    private String PREFIX = "/system/appInvite/";

    @Autowired
    private IAppInviteService appInviteService;

    /**
     * 跳转到用户邀请明细首页
     */
    @RequestMapping("")
    public String index(Model model,Integer uid,AppInvite appInvite) {
        model.addAttribute("uid",uid);
        Double sum=0d;
        List<Map<String,Object>> mapList=appInviteService.getAppInviteSum(appInvite);
        if(mapList.size()>0){
            sum = Double.valueOf(mapList.get(0).get("sum").toString());
        }
        model.addAttribute("sum",sum);
        return PREFIX + "appInvite.html";
    }

    /**
     * 跳转到添加用户邀请明细
     */
    @RequestMapping("/appInvite_add")
    public String appInviteAdd() {
        return PREFIX + "appInvite_add.html";
    }

    /**
     * 跳转到修改用户邀请明细
     */
    @RequestMapping("/appInvite_update/{appInviteId}")
    public String appInviteUpdate(@PathVariable Integer appInviteId, Model model) {
        AppInvite appInvite = appInviteService.selectById(appInviteId);
        model.addAttribute("item",appInvite);
        LogObjectHolder.me().set(appInvite);
        return PREFIX + "appInvite_edit.html";
    }

    /**
     * 获取用户邀请明细列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(AppInvite appInvite) {
        return appInviteService.selectList(new EntityWrapper<AppInvite>()
                .eq("uid",appInvite.getUid()).eq("isDelete",1));
    }

    /**
     * 新增用户邀请明细
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(AppInvite appInvite) {
        appInviteService.insert(appInvite);
        return SUCCESS_TIP;
    }

    /**
     * 删除用户邀请明细
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer appInviteId) {
        appInviteService.deleteById(appInviteId);
        return SUCCESS_TIP;
    }

    /**
     * 修改用户邀请明细
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(AppInvite appInvite) {
        appInviteService.updateById(appInvite);
        return SUCCESS_TIP;
    }

    /**
     * 用户邀请明细详情
     */
    @RequestMapping(value = "/detail/{appInviteId}")
    @ResponseBody
    public Object detail(@PathVariable("appInviteId") Integer appInviteId) {
        return appInviteService.selectById(appInviteId);
    }
}
