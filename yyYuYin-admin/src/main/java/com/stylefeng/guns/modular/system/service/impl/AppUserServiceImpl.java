package com.stylefeng.guns.modular.system.service.impl;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.system.model.AppUser;
import com.stylefeng.guns.modular.system.dao.AppUserMapper;
import com.stylefeng.guns.modular.system.service.IAppUserService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 用户 服务实现类
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-01
 */
@Service
public class AppUserServiceImpl extends ServiceImpl<AppUserMapper, AppUser> implements IAppUserService {
    @Override
    public List<Map<String, Object>> getAppUserList(AppUser appUser, Page<Map<String,Object>> page) {
        return this.baseMapper.getAppUserList(appUser,page);
    }

    @Override
    public List<Map<String, Object>> getAppUserOne(Map<String, Object> map) {
        return this.baseMapper.getAppUserOne(map);
    }

    @Override
    public List<Map<String, Object>> getUserCount(Map<String, Object> map) {
        return this.baseMapper.getUserCount(map);
    }

    @Override
    public List<Map<String, Object>> getUserStatic(Map<String, Object> paramMap) {
        return this.baseMapper.getUserStatic(paramMap);
    }

    @Override
    public List<Map<String, Object>> getUserStaticLogin(Map<String, Object> paramMap) {
        return this.baseMapper.getUserStaticLogin(paramMap);
    }

    @Override
    public List<Map<String, Object>> userByCity(Map<String, Object> paramMap) {
        return this.baseMapper.userByCity(paramMap);
    }

    @Override
    public List<Map<String, Object>> userBySix(Map<String, Object> paramMap) {
        return this.baseMapper.userBySix(paramMap);
    }

    @Override
    public List<Map<String, Object>> userByAge(Map<String, Object> paramMap) {
        return this.baseMapper.userByAge(paramMap);
    }

    @Override
    public List<Map<String, Object>> userByHuo(Map<String, Object> paramMap) {
        return this.baseMapper.userByHuo(paramMap);
    }

    @Override
    public  Map<String,Object> financialStatistics(Map<String, Object> paramMap) {
        return this.baseMapper.financialStatistics(paramMap);
    }

    @Override
    public  Map<String,Object> financialStatistics_(Map<String, Object> paramMap) {
        return this.baseMapper.financialStatistics_(paramMap);
    }

    @Override
    public List<Map<String, Object>> getChongzhi(Map<String, Object> paramMap) {
        return this.baseMapper.getChongzhi(paramMap);
    }

    @Override
    public List<Map<String, Object>> getShouru(Map<String, Object> paramMap) {
        return this.baseMapper.getShouru(paramMap);
    }

    @Override
    public Map<String, Object> financialStatisticsJ(Map<String, Object> paramMap) {
        return this.baseMapper.financialStatisticsJ(paramMap);
    }
}
