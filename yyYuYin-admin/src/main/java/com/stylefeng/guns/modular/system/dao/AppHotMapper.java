package com.stylefeng.guns.modular.system.dao;

import com.stylefeng.guns.modular.system.model.AppHot;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 热搜词 Mapper 接口
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-12
 */
public interface AppHotMapper extends BaseMapper<AppHot> {

}
