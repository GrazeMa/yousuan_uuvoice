package com.stylefeng.guns.modular.system.service.impl;

import com.stylefeng.guns.modular.system.model.AppTreasure;
import com.stylefeng.guns.modular.system.dao.AppTreasureMapper;
import com.stylefeng.guns.modular.system.service.IAppTreasureService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 房间财富值 服务实现类
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-11
 */
@Service
public class AppTreasureServiceImpl extends ServiceImpl<AppTreasureMapper, AppTreasure> implements IAppTreasureService {
    @Override
    public List<Map<String, Object>> getAppTreasure(Map<String, Object> map) {
        return this.baseMapper.getAppTreasure(map);
    }
}
