package com.stylefeng.guns.modular.system.service.impl;

import com.stylefeng.guns.modular.system.model.AppBlock;
import com.stylefeng.guns.modular.system.dao.AppBlockMapper;
import com.stylefeng.guns.modular.system.service.IAppBlockService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 房间拉黑的 服务实现类
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-08
 */
@Service
public class AppBlockServiceImpl extends ServiceImpl<AppBlockMapper, AppBlock> implements IAppBlockService {

}
