package com.stylefeng.guns.modular.system.dao;

import com.stylefeng.guns.modular.system.model.AppTimeTask;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 定时任务数据 Mapper 接口
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-13
 */
public interface AppTimeTaskMapper extends BaseMapper<AppTimeTask> {

}
