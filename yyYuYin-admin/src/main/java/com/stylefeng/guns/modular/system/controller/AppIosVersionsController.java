package com.stylefeng.guns.modular.system.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.common.constant.factory.PageFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;
import com.stylefeng.guns.modular.system.model.AppIosVersions;
import com.stylefeng.guns.modular.system.service.IAppIosVersionsService;

import java.util.Date;

/**
 * ios版本控制控制器
 * @Date 2019-05-21 09:54:40
 */
@Controller
@RequestMapping("/appIosVersions")
public class AppIosVersionsController extends BaseController {

    private String PREFIX = "/system/appIosVersions/";

    @Autowired
    private IAppIosVersionsService appIosVersionsService;

    /**
     * 跳转到ios版本控制首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "appIosVersions.html";
    }

    /**
     * 跳转到添加ios版本控制
     */
    @RequestMapping("/appIosVersions_add")
    public String appIosVersionsAdd() {
        return PREFIX + "appIosVersions_add.html";
    }

    /**
     * 跳转到修改ios版本控制
     */
    @RequestMapping("/appIosVersions_update/{appIosVersionsId}")
    public String appIosVersionsUpdate(@PathVariable Integer appIosVersionsId, Model model) {
        AppIosVersions appIosVersions = appIosVersionsService.selectById(appIosVersionsId);
        model.addAttribute("item",appIosVersions);
        LogObjectHolder.me().set(appIosVersions);
        return PREFIX + "appIosVersions_edit.html";
    }

    /**
     * 获取ios版本控制列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        Wrapper entityWrapper=new EntityWrapper<AppIosVersions>();
        entityWrapper.orderBy("createDate",false);
        Page<AppIosVersions> page = new PageFactory<AppIosVersions>().defaultPage();
        page.setRecords(appIosVersionsService.selectMapsPage(page, entityWrapper).getRecords());
        return super.packForBT(page);
    }

    /**
     * 新增ios版本控制
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(AppIosVersions appIosVersions) {
        appIosVersions.setCreateDate(new Date());
        appIosVersionsService.insert(appIosVersions);
        return SUCCESS_TIP;
    }

    /**
     * 删除ios版本控制
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer appIosVersionsId) {
        appIosVersionsService.deleteById(appIosVersionsId);
        return SUCCESS_TIP;
    }

    /**
     * 修改ios版本控制
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(AppIosVersions appIosVersions) {
        appIosVersionsService.updateById(appIosVersions);
        return SUCCESS_TIP;
    }

    /**
     * ios版本控制详情
     */
    @RequestMapping(value = "/detail/{appIosVersionsId}")
    @ResponseBody
    public Object detail(@PathVariable("appIosVersionsId") Integer appIosVersionsId) {
        return appIosVersionsService.selectById(appIosVersionsId);
    }
}
