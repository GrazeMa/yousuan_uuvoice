package com.stylefeng.guns.modular.system.service.impl;

import com.stylefeng.guns.modular.system.model.AppAward;
import com.stylefeng.guns.modular.system.dao.AppAwardMapper;
import com.stylefeng.guns.modular.system.service.IAppAwardService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 优钻兑换优币奖励 服务实现类
 * </p>
 *
 * @author stylefeng123
 * @since 2019-02-28
 */
@Service
public class AppAwardServiceImpl extends ServiceImpl<AppAwardMapper, AppAward> implements IAppAwardService {

}
