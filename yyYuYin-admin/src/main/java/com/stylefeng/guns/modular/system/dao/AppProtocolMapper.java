package com.stylefeng.guns.modular.system.dao;

import com.stylefeng.guns.modular.system.model.AppProtocol;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * app各种html页面 Mapper 接口
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-05
 */
public interface AppProtocolMapper extends BaseMapper<AppProtocol> {
    /**
     * 获取各种页面协议
     * @return
     */
    List<Map<String,Object>> getAppProtocol();
}
