package com.stylefeng.guns.modular.system.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.system.model.AppFeedback;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 反馈 服务类
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-13
 */
public interface IAppFeedbackService extends IService<AppFeedback> {
    /**
     *获取反馈信息
     * @param appFeedback
     * @return
     */
    List<Map<String,Object>> getAppFeedback(AppFeedback appFeedback,Page<Map<String,Object>> page);
}
