package com.stylefeng.guns.modular.system.service;

import com.stylefeng.guns.modular.system.model.AppImages;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 图片 服务类
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-02
 */
public interface IAppImagesService extends IService<AppImages> {

}
