package com.stylefeng.guns.modular.system.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.common.constant.factory.PageFactory;
import com.stylefeng.guns.modular.system.model.AppReport;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;
import com.stylefeng.guns.modular.system.model.AppGuild;
import com.stylefeng.guns.modular.system.service.IAppGuildJoinService;

import java.util.List;
import java.util.Map;

/**
 * @Description 入会申请控制器。
 * @author Grazer_Ma
 * @Date 2020-05-14 22:27:43
 */
@Controller
@RequestMapping("/appGuildJoin")
public class AppGuildJoinController extends BaseController {

    private String PREFIX = "/system/appGuildJoin/";

    @Autowired
    private IAppGuildJoinService appGuildJoinService;

    /**
     * 跳转到公会管理首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "appGuildJoin.html";
    }

    /**
     * 获取公会管理列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(AppGuild appGuild) {
        Page<Map<String,Object>> page = new PageFactory<Map<String,Object>>().defaultPage();
        List<Map<String, Object>> result = appGuildJoinService.getAppGuildJoin(appGuild, page);
        page.setRecords(result);
        return super.packForBT(page);
    }
    
	/**
	 * 同意入会申请。
	 */
	@RequestMapping(value = "/agreed")
	@ResponseBody
	public Object agreed(String appGuildJoinId) {
		EntityWrapper<AppGuild> entityWrapper = new EntityWrapper<>();
		entityWrapper.in("id", appGuildJoinId);
		AppGuild appGuild = new AppGuild();
		appGuild.setStatus(1); // 状态（0：待审核；1：已审核；2：被踢出公会）。
		appGuildJoinService.update(appGuild, entityWrapper);
		return SUCCESS_TIP;
	}
	
	/**
	 * 拒绝入会申请。
	 */
	@RequestMapping(value = "/refused")
	@ResponseBody
	public Object refused(String appGuildJoinId) {
		EntityWrapper<AppGuild> entityWrapper = new EntityWrapper<>();
		entityWrapper.in("id", appGuildJoinId);
		AppGuild appGuild = new AppGuild();
		appGuild.setStatus(2); // 状态（0：待审核；1：已审核；2：被踢出公会）。
		appGuildJoinService.update(appGuild, entityWrapper);
		return SUCCESS_TIP;
	}
    
}
