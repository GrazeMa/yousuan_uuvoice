package com.stylefeng.guns.modular.system.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.kdDemo.utils.SignUtils;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.common.constant.factory.PageFactory;
import com.stylefeng.guns.core.util.SinataUtil;
import com.stylefeng.guns.modular.system.model.AppUser;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;
import com.stylefeng.guns.modular.system.model.AppAudit;
import com.stylefeng.guns.modular.system.service.IAppAuditService;
import com.stylefeng.guns.modular.system.service.IAppUserService;

/**
 * 实名认证管理控制器
 * @Date 2019-03-01 14:21:52
 */
@Controller
@RequestMapping("/appAudit")
public class AppAuditController extends BaseController {

    private String PREFIX = "/system/appAudit/";

    @Autowired
    private IAppAuditService appAuditService;
    @Autowired
    private IAppUserService appUserService;

    /**
     * 跳转到实名认证管理首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "appAudit.html";
    }

    /**
     * 跳转到添加实名认证管理
     */
    @RequestMapping("/appAudit_add")
    public String appAuditAdd() {
        return PREFIX + "appAudit_add.html";
    }

    /**
     * 跳转到修改实名认证管理
     */
    @RequestMapping("/appAudit_update/{appAuditId}")
    public String appAuditUpdate(@PathVariable Integer appAuditId, Model model) {
        //AppAudit appAudit = appAuditService.selectById(appAuditId);
        model.addAttribute("appAuditId",appAuditId);
        return PREFIX + "appAudit_edit.html";
    }

    /**
     * 获取实名认证管理列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(AppAudit appAudit,String beginTime,String endTime) {
        Wrapper appAuditEntityWrapper=new EntityWrapper<AppAudit>();
        if(SinataUtil.isNotEmpty(appAudit.getEid())){
            appAuditEntityWrapper.like("eid",appAudit.getEid());
        }
        if(SinataUtil.isNotEmpty(appAudit.getState())){
            appAuditEntityWrapper.like("state",appAudit.getState());
        }
        if(SinataUtil.isNotEmpty(beginTime)){
            appAuditEntityWrapper.ge("createTime",beginTime +" 00:00:00");
        }
        if(SinataUtil.isNotEmpty(endTime)){
            appAuditEntityWrapper.le("createTime",endTime +" 23:59:59");
        }

        Page<AppAudit> page = new PageFactory<AppAudit>().defaultPage();
        appAuditEntityWrapper.eq("isDelete",1).orderBy("createTime",false);
        page.setRecords(appAuditService.selectMapsPage(page, appAuditEntityWrapper).getRecords());
        return super.packForBT(page);
    }

    /**
     * 新增实名认证管理
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(AppAudit appAudit) {
        appAuditService.insert(appAudit);
        return SUCCESS_TIP;
    }

    /**
     * 删除实名认证管理
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(String appAuditId) {
        EntityWrapper<AppAudit> appAuditEntityWrapper=new EntityWrapper<>();
        appAuditEntityWrapper.in("id",appAuditId);
        AppAudit appAudit=new AppAudit();
        appAudit.setIsDelete(2);
        appAuditService.update(appAudit,appAuditEntityWrapper);
        return SUCCESS_TIP;
    }

    /**
     * 修改实名认证管理
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(AppAudit appAudit) {
        appAuditService.updateById(appAudit);
        
        System.out.println("appAudit: " + appAudit);
        AppAudit appAudit2 = appAuditService.selectById(appAudit.getId());
        System.out.println("appAudit2: " + appAudit2);
        
        System.out.println("appAudit2.getUid(): " + appAudit2.getUid());
        AppUser appUser = appUserService.selectById(appAudit2.getUid());
        System.out.println("appUser1: " + appUser);
        appUser.setAutonym(2);
        System.out.println("appUser2: " + appUser);
        appUserService.insertOrUpdate(appUser);
        return SUCCESS_TIP;
    }

    /**
     * 实名认证管理详情
     */
    @RequestMapping(value = "/detail/{appAuditId}")
    @ResponseBody
    public Object detail(@PathVariable("appAuditId") Integer appAuditId) {
        return appAuditService.selectById(appAuditId);
    }
}
