package com.stylefeng.guns.modular.system.job;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.stylefeng.guns.core.util.DateUtil;
import com.stylefeng.guns.modular.system.model.AppAgreement;
import com.stylefeng.guns.modular.system.model.AppGift;
import com.stylefeng.guns.modular.system.model.AppScene;
import com.stylefeng.guns.modular.system.model.AppTimeTask;
import com.stylefeng.guns.modular.system.service.IAppAgreementService;
import com.stylefeng.guns.modular.system.service.IAppGiftService;
import com.stylefeng.guns.modular.system.service.IAppSceneService;
import com.stylefeng.guns.modular.system.service.IAppTimeTaskService;
import com.stylefeng.guns.modular.system.task.base.QuartzManager;
import com.stylefeng.guns.modular.system.task.base.TimeJobType;
import com.stylefeng.guns.modular.system.task.jobs.AgreementJob;
import com.stylefeng.guns.modular.system.task.jobs.SceneAndGiftJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 每天凌晨生成一个当天的一些（修改礼物和道具修改售卖剩余天数）定时任务
 */
@Component
@EnableScheduling
public class SceneAndGift {
    Logger log = LoggerFactory.getLogger(this.getClass());
    @Autowired
    private IAppTimeTaskService appTimeTaskService;
    @Autowired
    private IAppGiftService appGiftService;
    @Autowired
    private IAppAgreementService appAgreementService;

    //@Scheduled(cron = "0 0/2 * * * ? ")
    @Scheduled(cron = "0 0 0 * * ?  ")
    public void updateSceneAndGift() {
        try {
            List<Map<String,Object>> list=appGiftService.getSceneAndGift();
            for(Map<String,Object> map:list){
                String addTime= map.get("addTime").toString();
                //Integer day=Integer.valueOf(map.get("day").toString());
                Integer type=Integer.valueOf(map.get("type").toString());
                Integer id=Integer.valueOf(map.get("id").toString());
                String startTime=DateUtil.formatDate(new Date(),"yyyy-MM-dd")+" "+addTime;
                //开始添加定时任务
                AppTimeTask timeTask=new AppTimeTask();
                timeTask.setCreateDate(new Date());
                timeTask.setExcuteDate(DateUtil.parse(startTime,"yyyy-MM-dd HH:mm:ss"));
                timeTask.setTaskName((SceneAndGiftJob.name+"_"+id+"_"+type).toUpperCase());
                timeTask.setTypeName(SceneAndGiftJob.name);
                timeTask.setMap(id+"_"+type);
                appTimeTaskService.insert(timeTask);

                Map<String,Object> paMap=new HashMap<>();
                paMap.put("id",id);
                paMap.put("type",type);
                paMap.put("timeTaskId",timeTask.getId());
                QuartzManager.addJob(SceneAndGiftJob.class,
                        timeTask.getTaskName(),
                        TimeJobType.UNLOCK,timeTask.getExcuteDate() ,
                        paMap);
            }

        } catch (Exception e) {
            e.printStackTrace();
            log.error("修改礼物和道具修改售卖剩余天数订单任务", e.getMessage());
        }
    }

    /**
     * 修改协议号分配的天数
     */
    //@Scheduled(cron = "0 0/2 * * * ? ")
    @Scheduled(cron = "0 0 1 * * ?  ")
    public void updateAgreement() {
        try {
            List<AppAgreement> appAgreements=appAgreementService.selectList(new EntityWrapper<AppAgreement>().eq("state",2).eq("isDelete",1));//已经分配的（是否分配 1否，2 是）
            for(AppAgreement appAgreement:appAgreements){
                Date d1=DateUtil.parseDate(DateUtil.formatDate(DateUtil.addDate(appAgreement.getAddTime(),appAgreement.getDays()),"yyyy-MM-dd"));
                Date d2=DateUtil.parseDate(DateUtil.formatDate(new Date(),"yyyy-MM-dd"));
                if(d1.equals(d2)){
                    Date t = appAgreement.getAddTime();
                    SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
                    String addTime=df.format(t);
                    String startTime=DateUtil.formatDate(new Date(),"yyyy-MM-dd")+" "+ addTime;
                    //开始添加定时任务
                    AppTimeTask timeTask=new AppTimeTask();
                    timeTask.setCreateDate(new Date());
                    timeTask.setExcuteDate(DateUtil.parse(startTime,"yyyy-MM-dd HH:mm:ss"));
                    timeTask.setTaskName((AgreementJob.name+"_"+appAgreement.getId()).toUpperCase());
                    timeTask.setTypeName(AgreementJob.name);
                    timeTask.setMap(appAgreement.getId()+"");
                    appTimeTaskService.insert(timeTask);

                    Map<String,Object> paMap=new HashMap<>();
                    paMap.put("id",appAgreement.getId());
                    paMap.put("timeTaskId",timeTask.getId());
                    QuartzManager.addJob(AgreementJob.class,
                            timeTask.getTaskName(),
                            TimeJobType.UNLOCK,timeTask.getExcuteDate() ,
                            paMap);
                    }
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("", e.getMessage());
        }

    }
}
