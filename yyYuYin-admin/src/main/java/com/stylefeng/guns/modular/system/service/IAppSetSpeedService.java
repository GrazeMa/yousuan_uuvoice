package com.stylefeng.guns.modular.system.service;

import com.stylefeng.guns.modular.system.model.AppSetSpeed;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 速配/认证/语句设置 服务类
 * </p>
 *
 * @author stylefeng123
 * @since 2019-02-28
 */
public interface IAppSetSpeedService extends IService<AppSetSpeed> {

}
