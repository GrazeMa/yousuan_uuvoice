package com.stylefeng.guns.modular.system.service.impl;

import com.stylefeng.guns.modular.system.model.AppSetRecharge;
import com.stylefeng.guns.modular.system.dao.AppSetRechargeMapper;
import com.stylefeng.guns.modular.system.service.IAppSetRechargeService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 充值设置 服务实现类
 * </p>
 *
 * @author stylefeng123
 * @since 2019-02-28
 */
@Service
public class AppSetRechargeServiceImpl extends ServiceImpl<AppSetRechargeMapper, AppSetRecharge> implements IAppSetRechargeService {

}
