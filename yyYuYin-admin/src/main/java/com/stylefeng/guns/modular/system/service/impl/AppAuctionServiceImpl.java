package com.stylefeng.guns.modular.system.service.impl;

import com.stylefeng.guns.modular.system.model.AppAuction;
import com.stylefeng.guns.modular.system.dao.AppAuctionMapper;
import com.stylefeng.guns.modular.system.service.IAppAuctionService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 房间竞拍排名 服务实现类
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-11
 */
@Service
public class AppAuctionServiceImpl extends ServiceImpl<AppAuctionMapper, AppAuction> implements IAppAuctionService {
    @Override
    public List<Map<String, Object>> getAppAuction(Map<String, Object> map) {
        return this.baseMapper.getAppAuction(map);
    }

    @Override
    public List<Map<String, Object>> getAppAuctionRecord(Map<String, Object> map) {
        return this.baseMapper.getAppAuctionRecord(map);
    }
}
