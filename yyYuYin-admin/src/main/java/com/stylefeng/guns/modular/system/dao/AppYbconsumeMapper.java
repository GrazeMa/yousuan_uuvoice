package com.stylefeng.guns.modular.system.dao;

import com.stylefeng.guns.modular.system.model.AppRoom;
import com.stylefeng.guns.modular.system.model.AppYbconsume;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 优币消费记录 Mapper 接口
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-14
 */
public interface AppYbconsumeMapper extends BaseMapper<AppYbconsume> {
    /**
     * 获取房间流水数据
     * @param appRoom
     * @return
     */
    List<Map<String,Object>> getAppYbconsume(AppRoom appRoom);
}
