package com.stylefeng.guns.modular.system.dao;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.system.model.AppReport;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 举报 Mapper 接口
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-13
 */
public interface AppReportMapper extends BaseMapper<AppReport> {
    /**
     *获取用户举报信息
     * @param appReport
     * @return
     */
    List<Map<String,Object>> getAppReportByUser(AppReport appReport,Page<Map<String,Object>> page);

    /**
     * 获取房间举报信息
     * @param appReport
     * @return
     */
    List<Map<String,Object>> getAppReportByRoom(AppReport appReport,Page<Map<String,Object>> page);

    /**
     * 获取音乐举报信息
     * @param appReport
     * @return
     */
    List<Map<String,Object>> getAppReportByMusic(AppReport appReport,Page<Map<String,Object>> page);
}
