package com.stylefeng.guns.modular.system.dao;

import com.stylefeng.guns.modular.system.model.AppAgreement;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.Map;

/**
 * <p>
 * 协议号 Mapper 接口
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-01
 */
public interface AppAgreementMapper extends BaseMapper<AppAgreement> {
    /**
     * 召回协议号
     * @param map
     * @return
     */
    Integer updateIncome(Map map);
}
