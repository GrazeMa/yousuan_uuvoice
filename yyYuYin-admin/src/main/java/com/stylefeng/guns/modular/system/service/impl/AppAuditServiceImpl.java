package com.stylefeng.guns.modular.system.service.impl;

import com.stylefeng.guns.modular.system.model.AppAudit;
import com.stylefeng.guns.modular.system.dao.AppAuditMapper;
import com.stylefeng.guns.modular.system.service.IAppAuditService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 实名认证 服务实现类
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-01
 */
@Service
public class AppAuditServiceImpl extends ServiceImpl<AppAuditMapper, AppAudit> implements IAppAuditService {

}
