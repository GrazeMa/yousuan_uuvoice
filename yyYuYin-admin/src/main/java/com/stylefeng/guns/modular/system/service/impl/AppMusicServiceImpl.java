package com.stylefeng.guns.modular.system.service.impl;

import com.stylefeng.guns.modular.system.model.AppMusic;
import com.stylefeng.guns.modular.system.dao.AppMusicMapper;
import com.stylefeng.guns.modular.system.service.IAppMusicService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 音乐审核 服务实现类
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-12
 */
@Service
public class AppMusicServiceImpl extends ServiceImpl<AppMusicMapper, AppMusic> implements IAppMusicService {

}
