package com.stylefeng.guns.modular.system.dao;

import com.stylefeng.guns.modular.system.model.AppMusic;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 音乐审核 Mapper 接口
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-12
 */
public interface AppMusicMapper extends BaseMapper<AppMusic> {

}
