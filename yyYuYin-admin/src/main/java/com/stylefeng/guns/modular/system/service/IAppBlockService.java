package com.stylefeng.guns.modular.system.service;

import com.stylefeng.guns.modular.system.model.AppBlock;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 房间拉黑的 服务类
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-08
 */
public interface IAppBlockService extends IService<AppBlock> {

}
