package com.stylefeng.guns.modular.system.service.impl;

import com.stylefeng.guns.modular.system.model.AppWithdraw;
import com.stylefeng.guns.modular.system.dao.AppWithdrawMapper;
import com.stylefeng.guns.modular.system.service.IAppWithdrawService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 提现管理 服务实现类
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-14
 */
@Service
public class AppWithdrawServiceImpl extends ServiceImpl<AppWithdrawMapper, AppWithdraw> implements IAppWithdrawService {
    @Override
    public List<Map<String, Object>> getAppWithdraw(AppWithdraw appWithdraw) {
        return this.baseMapper.getAppWithdraw(appWithdraw);
    }
}
