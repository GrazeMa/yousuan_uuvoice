package com.stylefeng.guns.modular.system.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.system.dao.AppGiftRecordMapper;
import com.stylefeng.guns.modular.system.model.AppGiftRecord;
import com.stylefeng.guns.modular.system.service.IAppGiftRecordService;

/**
 * @Description 礼物记录服务实现类。
 * @author Grazer_Ma
 * @Date 2020-05-31 10:14:26
 */
@Service
public class AppGiftRecordServiceImpl extends ServiceImpl<AppGiftRecordMapper, AppGiftRecord> implements IAppGiftRecordService {

    @Override
    public List<Map<String, Object>> getAppGiftRecordList(AppGiftRecord appGiftRecord,Page<Map<String,Object>> page) {
        return this.baseMapper.getAppGiftRecordList(appGiftRecord,page);
    }

}
