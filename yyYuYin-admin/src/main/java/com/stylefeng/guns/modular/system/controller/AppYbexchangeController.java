package com.stylefeng.guns.modular.system.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.util.SinataUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;
import com.stylefeng.guns.modular.system.model.AppYbexchange;
import com.stylefeng.guns.modular.system.service.IAppYbexchangeService;

/**
 * 优币兑换控制器
 * @Date 2019-03-14 11:22:07
 */
@Controller
@RequestMapping("/appYbexchange")
public class AppYbexchangeController extends BaseController {

    private String PREFIX = "/system/appYbexchange/";

    @Autowired
    private IAppYbexchangeService appYbexchangeService;

    /**
     * 跳转到优币兑换首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "appYbexchange.html";
    }

    /**
     * 跳转到添加优币兑换
     */
    @RequestMapping("/appYbexchange_add")
    public String appYbexchangeAdd() {
        return PREFIX + "appYbexchange_add.html";
    }

    /**
     * 跳转到修改优币兑换
     */
    @RequestMapping("/appYbexchange_update/{appYbexchangeId}")
    public String appYbexchangeUpdate(@PathVariable Integer appYbexchangeId, Model model) {
        AppYbexchange appYbexchange = appYbexchangeService.selectById(appYbexchangeId);
        model.addAttribute("item",appYbexchange);
        LogObjectHolder.me().set(appYbexchange);
        return PREFIX + "appYbexchange_edit.html";
    }

    /**
     * 获取优币兑换列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(AppYbexchange appYbexchange,String beginTime,String endTime) {
        EntityWrapper<AppYbexchange> entityWrapper=new EntityWrapper<>();
        if(SinataUtil.isNotEmpty(appYbexchange.getEid())){
            entityWrapper.eq("eid",appYbexchange.getEid());
        }
        if(SinataUtil.isNotEmpty(appYbexchange.getName())){
            entityWrapper.like("name",appYbexchange.getName());
        }
        if(SinataUtil.isNotEmpty(beginTime)){
            entityWrapper.ge("createTime",beginTime +" 00:00:00");
        }
        if(SinataUtil.isNotEmpty(endTime)){
            entityWrapper.le("createTime",endTime +" 23:59:59");
        }
        entityWrapper.eq("isDelete",1);
        entityWrapper.orderBy("createTime",false);
        return appYbexchangeService.selectList(entityWrapper);
    }

    /**
     * 新增优币兑换
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(AppYbexchange appYbexchange) {
        appYbexchangeService.insert(appYbexchange);
        return SUCCESS_TIP;
    }

    /**
     * 删除优币兑换
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer appYbexchangeId) {
        appYbexchangeService.deleteById(appYbexchangeId);
        return SUCCESS_TIP;
    }

    /**
     * 修改优币兑换
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(AppYbexchange appYbexchange) {
        appYbexchangeService.updateById(appYbexchange);
        return SUCCESS_TIP;
    }

    /**
     * 优币兑换详情
     */
    @RequestMapping(value = "/detail/{appYbexchangeId}")
    @ResponseBody
    public Object detail(@PathVariable("appYbexchangeId") Integer appYbexchangeId) {
        return appYbexchangeService.selectById(appYbexchangeId);
    }
}
