package com.stylefeng.guns.modular.system.task.base;

import com.stylefeng.guns.core.util.SpringContextHolder;
import com.stylefeng.guns.modular.system.service.*;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public abstract class AbstractJob implements Job{
	protected Logger logger = LoggerFactory.getLogger(getClass());
	
	@Override
	public abstract void execute(JobExecutionContext context) throws JobExecutionException;


	protected static String SappID ="011b2ffd560644e8a9d78e1ada8f1c93";

	protected IAppUserService appUserService;

	protected IAppRoomService  appRoomService;

	protected IAppTimeTaskService appTimeTaskService;

	protected IAppGiftService appGiftService;

	protected IAppSceneService appSceneService;

	protected IAppAgreementService appAgreementService;

	protected IAppChatroomsService appChatroomsService;

	protected IAppSeatService appSeatService;


	public AbstractJob(){
		this.appUserService = SpringContextHolder.getBean( IAppUserService.class);
		this.appRoomService = SpringContextHolder.getBean(IAppRoomService.class);
		this.appTimeTaskService = SpringContextHolder.getBean(IAppTimeTaskService.class);
		this.appGiftService = SpringContextHolder.getBean(IAppGiftService.class);
		this.appSceneService = SpringContextHolder.getBean(IAppSceneService.class);
		this.appAgreementService = SpringContextHolder.getBean(IAppAgreementService.class);
		this.appChatroomsService = SpringContextHolder.getBean(IAppChatroomsService.class);
		this.appSeatService = SpringContextHolder.getBean(IAppSeatService.class);
	}

	 
}
