package com.stylefeng.guns.modular.system.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.common.constant.factory.PageFactory;
import com.stylefeng.guns.core.util.OssUploadUtil;
import com.stylefeng.guns.core.util.SinataUtil;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;
import com.stylefeng.guns.modular.system.model.AppGift;
import com.stylefeng.guns.modular.system.service.IAppGiftService;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 礼物管理控制器
 * @Date 2019-03-01 09:41:24
 */
@Controller
@RequestMapping("/appGift")
public class AppGiftController extends BaseController {

    private String PREFIX = "/system/appGift/";

    @Autowired
    private IAppGiftService appGiftService;

    /**
     * 跳转到礼物管理首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "appGift.html";
    }

    /**
     * 跳转到添加礼物管理
     */
    @RequestMapping("/appGift_add")
    public String appGiftAdd() {
        return PREFIX + "appGift_add.html";
    }

    /**
     * 跳转到修改礼物管理
     */
    @RequestMapping("/appGift_update/{appGiftId}")
    public String appGiftUpdate(@PathVariable Integer appGiftId, Model model) {
        AppGift appGift = appGiftService.selectById(appGiftId);
        model.addAttribute("item",appGift);
        if(appGift.getImg().contains(".svga")){//svga图片
            model.addAttribute("type",1);
        }else{
            model.addAttribute("type",2);
        }
        LogObjectHolder.me().set(appGift);
        return PREFIX + "appGift_edit.html";
    }

    /**
     * 获取礼物管理列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(AppGift appGift) {
        Page<Map<String,Object>> page = new PageFactory<Map<String,Object>>().defaultPage();
        List<Map<String, Object>> result = appGiftService.getAppGiftList(appGift,page);
        page.setRecords(result);
        return super.packForBT(page);
    }

    /**
     * 新增礼物管理
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(AppGift appGift) {
        appGift.setAddTime(new Date());
        appGift.setCreateTime(new Date());
        appGiftService.insert(appGift);
        return SUCCESS_TIP;
    }

    /**
     * 删除礼物管理
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    @Transactional
    public Object delete(String appGiftId) {
        EntityWrapper<AppGift> entityWrapper=new EntityWrapper<>();
        entityWrapper.in("id",appGiftId);
        AppGift appGift=new AppGift();
        appGift.setIsDelete(2);
        appGiftService.update(appGift,entityWrapper);
        List<AppGift> appGiftList=appGiftService.selectList(entityWrapper);
        if(appGiftList!=null && appGiftList.size()>0){
            //删除oss上的图片
            for(AppGift appGift1:appGiftList){
                List<String> list=new ArrayList<>();
                if(SinataUtil.isNotEmpty(appGift1.getImg())){
                    list.add(appGift1.getImg());
                }
                if(SinataUtil.isNotEmpty(appGift1.getImgFm())){
                    list.add(appGift1.getImgFm());
                }
                OssUploadUtil.deleteObjects(list);
            }
        }
        return SUCCESS_TIP;
    }

    /**
     * 修改礼物管理 编辑并且上架
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    @Transactional
    public Object update(AppGift appGift) {
        AppGift appGift1=appGiftService.selectById(appGift.getId());
        if(appGift1!=null){
            List<String> list=new ArrayList<>();
            if(SinataUtil.isNotEmpty(appGift1.getImg())){
                list.add(appGift1.getImg());
            }
            if(SinataUtil.isNotEmpty(appGift1.getImgFm())){
                list.add(appGift1.getImgFm());
            }
            OssUploadUtil.deleteObjects(list);
        }
        //appGift.setIsState(1);//1 销售 ,2 下架
        //appGift.setAddTime(new Date());
        appGiftService.updateById(appGift);
        return SUCCESS_TIP;
    }
    /**
     * 修改道具 上下架
     */
    @RequestMapping(value = "/updateXiajia")
    @ResponseBody
    public Object updateXiajia(String ids,Integer isState) {
        EntityWrapper<AppGift> entityWrapper=new EntityWrapper<>();
        entityWrapper.in("id",ids);
        AppGift appUser=new AppGift();
        appUser.setIsState(isState);
        if(isState==1){//需要上架
            appUser.setAddTime(new Date());
        }
        appGiftService.update(appUser,entityWrapper);
        return SUCCESS_TIP;
    }
    /**
     * 礼物管理详情
     */
    @RequestMapping(value = "/detail/{appGiftId}")
    @ResponseBody
    public Object detail(@PathVariable("appGiftId") Integer appGiftId) {
        return appGiftService.selectById(appGiftId);
    }

    /**
     * 系统设置中，宝箱选择礼物后获取数据
     * @return
     */
    @RequestMapping(value = "/getGiftName")
    @ResponseBody
    public Object getGiftName() {
        return appGiftService.getAppGiftName(null);
    }
}
