package com.stylefeng.guns.modular.system.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 充值分成金额
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-06
 */
@TableName("app_divide")
public class AppDivide extends Model<AppDivide> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 邀请人id
     */
    private Integer uid;
    /**
     * 被邀请用户id
     */
    private Integer buid;
    private Date createTime;
    /**
     * 邀请奖励
     */
    private Double money;
    /**
     * 充值金额
     */
    private Double czMoney;
    /**
     * '是否被删除（1=否  2=是）'
     */
    private Integer isDelete;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public Integer getBuid() {
        return buid;
    }

    public void setBuid(Integer buid) {
        this.buid = buid;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Double getMoney() {
        return money;
    }

    public void setMoney(Double money) {
        this.money = money;
    }

    public Double getCzMoney() {
        return czMoney;
    }

    public void setCzMoney(Double czMoney) {
        this.czMoney = czMoney;
    }

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "AppDivide{" +
        "id=" + id +
        ", uid=" + uid +
        ", buid=" + buid +
        ", createTime=" + createTime +
        ", money=" + money +
        ", czMoney=" + czMoney +
        ", isDelete=" + isDelete +
        "}";
    }
}
