package com.stylefeng.guns.modular.system.service.impl;

import com.stylefeng.guns.modular.system.model.AppProtocol;
import com.stylefeng.guns.modular.system.dao.AppProtocolMapper;
import com.stylefeng.guns.modular.system.service.IAppProtocolService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * app各种html页面 服务实现类
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-05
 */
@Service
public class AppProtocolServiceImpl extends ServiceImpl<AppProtocolMapper, AppProtocol> implements IAppProtocolService {
    @Override
    public List<Map<String, Object>> getAppProtocol() {
        return this.baseMapper.getAppProtocol();
    }
}
