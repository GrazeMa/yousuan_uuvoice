package com.stylefeng.guns.modular.system.service.impl;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.system.model.AppScene;
import com.stylefeng.guns.modular.system.dao.AppSceneMapper;
import com.stylefeng.guns.modular.system.service.IAppSceneService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 道具管理 服务实现类
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-01
 */
@Service
public class AppSceneServiceImpl extends ServiceImpl<AppSceneMapper, AppScene> implements IAppSceneService {
    @Override
    public List<Map<String, Object>> getAppSceneName(Map<String, Object> map) {
        return this.baseMapper.getAppSceneName(map);
    }

    @Override
    public List<Map<String, Object>> getAppSceneList(AppScene appScene, Page<Map<String,Object>> page) {
        return this.baseMapper.getAppSceneList(appScene,page);
    }
}
