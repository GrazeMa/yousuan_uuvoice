package com.stylefeng.guns.modular.system.service;

import com.stylefeng.guns.modular.system.model.AppSetShare;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 分享赠送 服务类
 * </p>
 *
 * @author stylefeng123
 * @since 2019-02-28
 */
public interface IAppSetShareService extends IService<AppSetShare> {

}
