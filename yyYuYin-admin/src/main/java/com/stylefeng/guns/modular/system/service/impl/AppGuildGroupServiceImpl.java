package com.stylefeng.guns.modular.system.service.impl;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.system.model.AppGuildGroup;
import com.stylefeng.guns.modular.system.dao.AppGuildGroupMapper;
import com.stylefeng.guns.modular.system.service.IAppGuildGroupService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
/**
 * @Description 公会组服务实现类。
 * @author Grazer_Ma
 * @Date 2020-05-07 20:48:21
 */
@Service
public class AppGuildGroupServiceImpl extends ServiceImpl<AppGuildGroupMapper, AppGuildGroup> implements IAppGuildGroupService {
    @Override
    public List<Map<String, Object>> getAppGuildGroup(AppGuildGroup appGuildGroup, Page<Map<String,Object>> page) {
        return this.baseMapper.getAppGuildGroup(appGuildGroup, page);
    }
}
