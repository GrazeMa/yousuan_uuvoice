package com.stylefeng.guns.modular.system.service;

import com.stylefeng.guns.modular.system.model.AppAward;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 优钻兑换优币奖励 服务类
 * </p>
 *
 * @author stylefeng123
 * @since 2019-02-28
 */
public interface IAppAwardService extends IService<AppAward> {

}
