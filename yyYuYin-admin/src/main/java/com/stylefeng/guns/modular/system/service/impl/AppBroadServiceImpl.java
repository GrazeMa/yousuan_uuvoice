package com.stylefeng.guns.modular.system.service.impl;


import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.system.model.AppBroad;
import com.stylefeng.guns.modular.system.dao.AppBroadMapper;
import com.stylefeng.guns.modular.system.service.IAppBroadService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


/**
 * <p>
 * 广播交友 服务实现类
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-12
 */
@Service
public class AppBroadServiceImpl extends ServiceImpl<AppBroadMapper, AppBroad> implements IAppBroadService {
    @Override
    public List<Map<String, Object>> getAppBroad(AppBroad appBroad, Page page) {
        return this.baseMapper.getAppBroad(appBroad,page);
    }
}
