package com.stylefeng.guns.modular.system.service.impl;

import com.stylefeng.guns.modular.system.model.AppLottery;
import com.stylefeng.guns.modular.system.dao.AppLotteryMapper;
import com.stylefeng.guns.modular.system.service.IAppLotteryService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 宝箱设置 （第一条数据保存到礼物设置里面） 服务实现类
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-11
 */
@Service
public class AppLotteryServiceImpl extends ServiceImpl<AppLotteryMapper, AppLottery> implements IAppLotteryService {

}
