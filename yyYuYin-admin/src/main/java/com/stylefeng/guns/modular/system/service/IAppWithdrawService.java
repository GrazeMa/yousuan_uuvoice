package com.stylefeng.guns.modular.system.service;

import com.stylefeng.guns.modular.system.model.AppWithdraw;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 提现管理 服务类
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-14
 */
public interface IAppWithdrawService extends IService<AppWithdraw> {
    /**
     * 获取提现列表数据
     * @param appWithdraw
     * @return
     */
    List<Map<String,Object>> getAppWithdraw(AppWithdraw appWithdraw);
}
