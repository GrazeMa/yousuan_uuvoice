package com.stylefeng.guns.modular.system.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.system.model.AppGuild;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;
import java.util.Map;

/**
 * @Description 公会服务类。
 * @author Grazer_Ma
 * @Date 2020-05-06 20:56:46
 */
public interface IAppGuildService extends IService<AppGuild> {
    /**
     * 获取公会信息
     * @param appGuild
     * @return
     */
    List<Map<String,Object>> getAppGuild(AppGuild appGuild, Page<Map<String, Object>> page);
}
