package com.stylefeng.guns.modular.system.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.util.SinataUtil;
import com.stylefeng.guns.modular.system.model.AppUser;
import com.stylefeng.guns.modular.system.service.IAppUserService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;
import com.stylefeng.guns.modular.system.model.AppWithdraw;
import com.stylefeng.guns.modular.system.service.IAppWithdrawService;

/**
 * 提现控制器
 * @Date 2019-03-14 15:25:05
 */
@Controller
@RequestMapping("/appWithdraw")
public class AppWithdrawController extends BaseController {

    private String PREFIX = "/system/appWithdraw/";

    @Autowired
    private IAppWithdrawService appWithdrawService;
    @Autowired
    private IAppUserService appUserService;

    /**
     * 跳转到提现首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "appWithdraw.html";
    }

    /**
     * 跳转到添加提现
     */
    @RequestMapping("/appWithdraw_add")
    public String appWithdrawAdd() {
        return PREFIX + "appWithdraw_add.html";
    }

    /**
     * 跳转到修改提现
     */
    @RequestMapping("/appWithdraw_update/{appWithdrawId}")
    public String appWithdrawUpdate(@PathVariable Integer appWithdrawId, Model model) {
        AppWithdraw appWithdraw = appWithdrawService.selectById(appWithdrawId);
        model.addAttribute("item",appWithdraw);
        LogObjectHolder.me().set(appWithdraw);
        return PREFIX + "appWithdraw_edit.html";
    }

    /**
     * 获取提现列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(AppWithdraw appWithdraw) {
        return appWithdrawService.getAppWithdraw(appWithdraw);
    }

    /**
     * 新增提现
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(AppWithdraw appWithdraw) {
        appWithdrawService.insert(appWithdraw);
        return SUCCESS_TIP;
    }

    /**
     * 删除提现
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer appWithdrawId) {
        appWithdrawService.deleteById(appWithdrawId);
        return SUCCESS_TIP;
    }

    /**
     * 修改提现(审核)
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(AppWithdraw appWithdraw) {
        AppWithdraw appWithdraw_=appWithdrawService.selectById(appWithdraw.getId());
        if(appWithdraw.getStatus()==3){//拒绝申请提现  需要修改可提现金额
            //根据用户id查询用户信息
            AppUser appUser=appUserService.selectById(appWithdraw_.getUid());
            if(SinataUtil.isNotEmpty(appUser)){
                if(appWithdraw_.getState()==1){//1 钻石，2 是分成
                    appUser.setYnum(appUser.getYnum()+ appWithdraw_.getYzNum());//优钻增加
                }else {
                    appUser.setUserMoney(appUser.getUserMoney() + appWithdraw_.getMoney());//可提现金额需要增加
                }
                //修改保存数据
                appUserService.updateById(appUser);
            }
        }else if(appWithdraw.getStatus()==2){//同意的时候 需要增加用户的历史提现金额
            //根据用户id查询用户信息
            AppUser appUser=appUserService.selectById(appWithdraw_.getUid());
            if(SinataUtil.isNotEmpty(appUser)){
                appUser.setHistoryDeposit(appUser.getHistoryDeposit()+appWithdraw_.getMoney() );//历史提现金额需要增加
                //修改保存数据
                appUserService.updateById(appUser);
            }
        }
        appWithdrawService.updateById(appWithdraw);
        return SUCCESS_TIP;
    }

    /**
     * 提现详情
     */
    @RequestMapping(value = "/detail/{appWithdrawId}")
    @ResponseBody
    public Object detail(@PathVariable("appWithdrawId") Integer appWithdrawId) {
        return appWithdrawService.selectById(appWithdrawId);
    }
}
