package com.stylefeng.guns.modular.system.controller;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.util.DateUtil;
import com.stylefeng.guns.core.util.ExcelUtil;
import com.stylefeng.guns.core.util.SinataUtil;
import com.stylefeng.guns.modular.system.model.*;
import com.stylefeng.guns.modular.system.service.*;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;


@Controller
@RequestMapping("/export")
public class ExportController extends BaseController {
	@Autowired
	private IAppTreasureService appTreasureService;
	@Autowired
	private IAppCharmService appCharmService;
	@Autowired
	private IAppYbrechargeService appYbrechargeService;
	@Autowired
	private IAppYbexchangeService appYbexchangeService;
	@Autowired
	private IAppYbconsumeService appYbconsumeService;
	@Autowired
	private IAppYzconsumeService appYzconsumeService;
	@Autowired
	private IAppWithdrawService appWithdrawService;
    /**
     * 房间财富，魅力榜导出
     * @param
     * @param response
     * @return
     */
	@RequestMapping(value = "/appRoom")
	@ResponseBody
	public Object exportAppRoom(Integer rid,String usercoding,String nickname, Integer type,Integer day,HttpServletResponse response) {
		try {
			Map<String,Object> paMap=new HashMap<>();
			paMap.put("rid",rid);
			paMap.put("day",day);
			paMap.put("usercoding",usercoding);
			paMap.put("nickname",nickname);

			List<Map<String,Object>> reList=new ArrayList<>();
			if(type==1){
				reList=appTreasureService.getAppTreasure(paMap);
			}else if(type==2){
				reList=appCharmService.getAppCharm(paMap);
			}
			for(int i=0;i<reList.size();i++){
				Map<String,Object> map=new HashMap<>();
				if(i>0){
					Integer num=Integer.valueOf(reList.get(i-1).get("value_").toString())-Integer.valueOf(reList.get(i).get("value_").toString());
					reList.get(i).put("num",num);
				}
			}
			Date date = new Date();
			DateFormat format = new SimpleDateFormat("yyyyMMdd");
			String time = format.format(date);
			String typeName="";
			if(type==1){
				if(day==1){
					typeName="财富总榜";
				}else if(day==2){
					typeName="财富周榜";
				}else {
					typeName="财富日榜";
				}
			}else{
				if(day==1){
					typeName="魅力总榜";
				}else if(day==2){
					typeName="魅力周榜";
				}else {
					typeName="魅力日榜";
				}
			}
		    String fileName = typeName+time+".xls";
			String[] title = new String[] {"名次","优优ID","昵称","性别","魅力等级","魅力值","差距魅力值（相对上一名）"};

			String[][] values = new String[reList.size()][];
			for (int i = 0; i < reList.size(); i++) {
				Map<String,Object> d=reList.get(i);
				values[i] = new String[title.length];
				values[i][0]=d.get("ranks")==null?null:d.get("ranks").toString();
				values[i][1]=d.get("usercoding")==null?"":d.get("usercoding").toString();
				values[i][2]=d.get("nickname")==null?"":d.get("nickname").toString();
				String sex=d.get("sex")==null?"0":d.get("sex").toString();
				if(sex.equals("1")){
					values[i][3]="男";
				}else{
					values[i][3]="女";
				}

				values[i][4]=d.get("value_")==null?"":d.get("value_").toString();
				values[i][5]=d.get("level_")==null?"":d.get("level_").toString();
				values[i][6]=d.get("num")==null?"":d.get("num").toString();
			}
			HSSFWorkbook wb = ExcelUtil.getHSSFWorkbook(typeName+time, title, values, null);
			this.setResponseHeader(response, fileName);
			OutputStream os = response.getOutputStream();
			wb.write(os);
			os.flush();
			os.close();

		} catch (Exception e) {
			e.printStackTrace();
			/*return ApiUtil.putFailObj("导出失败");*/
		}
		return SUCCESS_TIP;
	}

	/**
	 * 充值记录
	 * @param appYbrecharge
	 * @param beginTime
	 * @param endTime
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/appYbrecharge")
	@ResponseBody
	public Object exportAppRoom(AppYbrecharge appYbrecharge,String beginTime,String endTime, HttpServletResponse response) {
		try {
			EntityWrapper<AppYbrecharge> entityWrapper=new EntityWrapper<>();
			if(SinataUtil.isNotEmpty(appYbrecharge.getEid())){
				entityWrapper.eq("eid",appYbrecharge.getEid());
			}
			if(SinataUtil.isNotEmpty(appYbrecharge.getName())){
				entityWrapper.like("name",appYbrecharge.getName());
			}
			if(SinataUtil.isNotEmpty(beginTime)){
				entityWrapper.ge("createTime",beginTime +" 00:00:00");
			}
			if(SinataUtil.isNotEmpty(endTime)){
				entityWrapper.le("createTime",endTime +" 23:59:59");
			}
			entityWrapper.eq("isDelete",1);
			entityWrapper.orderBy("createTime",false);
			List<AppYbrecharge> reList=appYbrechargeService.selectList(entityWrapper);
			Date date = new Date();
			DateFormat format = new SimpleDateFormat("yyyyMMdd");
			String time = format.format(date);
			String fileName = "优币充值记录"+time+".xls";
			String[] title = new String[] {"充值时间","充值用户","优优ID","充值优币","赠送优币","实得优币","支付金额","支付方式"};

			String[][] values = new String[reList.size()][];
			for (int i = 0; i < reList.size(); i++) {
				AppYbrecharge d=reList.get(i);
				values[i] = new String[title.length];
				values[i][0]=DateUtil.formatDate(d.getCreateTime(),"yyyy-MM-dd HH:mm:ss");
				values[i][1]=d.getName();
				values[i][2]=d.getEid().toString();
				values[i][3]=d.getGmGold().toString();
				values[i][4]=d.getZsGold().toString();
				values[i][5]=d.getSdGold().toString();
				values[i][6]=d.getPlay().toString();
				String payName="";
				if(d.getPlayState()==1){
					payName="支付宝";
				}else if(d.getPlayState()==2){
					payName="微信";
				}else if(d.getPlayState()==3){
					payName="苹果内购";
				}else if(d.getPlayState()==4){
					payName="公众号";
				}else if(d.getPlayState()==5){
					payName="手动添加";
				}
				values[i][7]=payName;

			}
			HSSFWorkbook wb = ExcelUtil.getHSSFWorkbook("优币充值记录"+time, title, values, null);
			this.setResponseHeader(response, fileName);
			OutputStream os = response.getOutputStream();
			wb.write(os);
			os.flush();
			os.close();

		} catch (Exception e) {
			e.printStackTrace();
			/*return ApiUtil.putFailObj("导出失败");*/
		}
		return SUCCESS_TIP;
	}
	/**
	 * 兑换记录
	 * @param appYbexchange
	 * @param beginTime
	 * @param endTime
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/appYbexchange")
	@ResponseBody
	public Object exportAppRoom(AppYbexchange appYbexchange, String beginTime, String endTime, HttpServletResponse response) {
		try {
			EntityWrapper<AppYbexchange> entityWrapper=new EntityWrapper<>();
			if(SinataUtil.isNotEmpty(appYbexchange.getEid())){
				entityWrapper.eq("eid",appYbexchange.getEid());
			}
			if(SinataUtil.isNotEmpty(appYbexchange.getName())){
				entityWrapper.like("name",appYbexchange.getName());
			}
			if(SinataUtil.isNotEmpty(beginTime)){
				entityWrapper.ge("createTime",beginTime +" 00:00:00");
			}
			if(SinataUtil.isNotEmpty(endTime)){
				entityWrapper.le("createTime",endTime +" 23:59:59");
			}
			entityWrapper.eq("isDelete",1);
			entityWrapper.orderBy("createTime",false);
			List<AppYbexchange> reList=appYbexchangeService.selectList(entityWrapper);
			Date date = new Date();
			DateFormat format = new SimpleDateFormat("yyyyMMdd");
			String time = format.format(date);
			String fileName = "优币兑换记录"+time+".xls";
			String[] title = new String[] {"兑换时间","兑换用户","优优ID","兑换钻石","获得优币","其他奖励"};

			String[][] values = new String[reList.size()][];
			for (int i = 0; i < reList.size(); i++) {
				AppYbexchange d=reList.get(i);
				values[i] = new String[title.length];
				values[i][0]=DateUtil.formatDate(d.getCreateTime(),"yyyy-MM-dd HH:mm:ss");
				values[i][1]=d.getName();
				values[i][2]=d.getEid().toString();
				values[i][3]=d.getDhDiamond().toString();
				values[i][4]=d.getHdGold().toString();
				values[i][5]=d.getQtGold();
			}
			HSSFWorkbook wb = ExcelUtil.getHSSFWorkbook("优币兑换记录"+time, title, values, null);
			this.setResponseHeader(response, fileName);
			OutputStream os = response.getOutputStream();
			wb.write(os);
			os.flush();
			os.close();

		} catch (Exception e) {
			e.printStackTrace();
			/*return ApiUtil.putFailObj("导出失败");*/
		}
		return SUCCESS_TIP;
	}
	/**
	 * 消费记录
	 * @param appYbconsume
	 * @param beginTime
	 * @param endTime
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/appYbconsume")
	@ResponseBody
	public Object exportAppRoom(AppYbconsume  appYbconsume, String beginTime, String endTime, HttpServletResponse response) {
		try {
			EntityWrapper<AppYbconsume> entityWrapper=new EntityWrapper<>();
			if(SinataUtil.isNotEmpty(appYbconsume.getEid())){
				entityWrapper.eq("eid",appYbconsume.getEid());
			}
			if(SinataUtil.isNotEmpty(appYbconsume.getRid())){
				entityWrapper.eq("rid",appYbconsume.getRid());
			}
			if(SinataUtil.isNotEmpty(appYbconsume.getName())){
				entityWrapper.like("name",appYbconsume.getName());
			}
			if(SinataUtil.isNotEmpty(beginTime)){
				entityWrapper.ge("createTime",beginTime +" 00:00:00");
			}
			if(SinataUtil.isNotEmpty(endTime)){
				entityWrapper.le("createTime",endTime +" 23:59:59");
			}
			entityWrapper.eq("isDelete",1);
			entityWrapper.orderBy("createTime",false);
			List<AppYbconsume> reList=appYbconsumeService.selectList(entityWrapper);
			System.out.println("reList: " + reList);
			Date date = new Date();
			DateFormat format = new SimpleDateFormat("yyyyMMdd");
			String time = format.format(date);
			String fileName = "优币消费记录"+time+".xls";
			String[] title = new String[] {"消费时间","消费用户","优优ID","房间ID","消费类型","消费数量","消费优币数"};

			String[][] values = new String[reList.size()][];
			for (int i = 0; i < reList.size(); i++) {
				AppYbconsume d=reList.get(i);
				values[i] = new String[title.length];
				values[i][0]=DateUtil.formatDate(d.getCreateTime(),"yyyy-MM-dd HH:mm:ss");
				values[i][1]=d.getName();
				values[i][2]=d.getEid().toString();
				values[i][3]=d.getRid();
				String lwName="";
				if(d.getState()==1){
					lwName="赠送礼物"+"-"+d.getLwName();
				}else if(d.getState()==2){
					lwName="购买花环"+"-"+d.getLwName();
				}else if(d.getState()==3){
					lwName="购买座驾"+"-"+d.getLwName();
				}else if(d.getState()==4){
					lwName="发红包";
				}
				values[i][4]=lwName;
				values[i][5]=d.getConsumeNum().toString();
				values[i][6]=d.getConsumeYbNum().toString();
			}
			HSSFWorkbook wb = ExcelUtil.getHSSFWorkbook("优币消费记录"+time, title, values, null);
			this.setResponseHeader(response, fileName);
			OutputStream os = response.getOutputStream();
			wb.write(os);
			os.flush();
			os.close();

		} catch (Exception e) {
			e.printStackTrace();
			/*return ApiUtil.putFailObj("导出失败");*/
		}
		return SUCCESS_TIP;
	}
	/**
	 * 获取优钻记录
	 * @param appYzconsume
	 * @param beginTime
	 * @param endTime
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/appYzconsume")
	@ResponseBody
	public Object appYzconsume(AppYzconsume appYzconsume, String beginTime, String endTime, HttpServletResponse response) {
		try {
			EntityWrapper<AppYzconsume> entityWrapper=new EntityWrapper<>();
			if(SinataUtil.isNotEmpty(appYzconsume.getEid())){
				entityWrapper.eq("eid",appYzconsume.getEid());
			}
			if(SinataUtil.isNotEmpty(appYzconsume.getName())){
				entityWrapper.like("name",appYzconsume.getName());
			}
			if(SinataUtil.isNotEmpty(beginTime)){
				entityWrapper.ge("createTime",beginTime +" 00:00:00");
			}
			if(SinataUtil.isNotEmpty(endTime)){
				entityWrapper.le("createTime",endTime +" 23:59:59");
			}
			entityWrapper.eq("isDelete",1);
			entityWrapper.orderBy("createTime",false);
			List<AppYzconsume> reList=appYzconsumeService.selectList(entityWrapper);
			Date date = new Date();
			DateFormat format = new SimpleDateFormat("yyyyMMdd");
			String time = format.format(date);
			String fileName = "优钻获取记录"+time+".xls";
			String[] title = new String[] {"获取时间","获取用户","优优ID","获取类型","获取数量","获取优钻数"};

			String[][] values = new String[reList.size()][];
			for (int i = 0; i < reList.size(); i++) {
				AppYzconsume d=reList.get(i);
				values[i] = new String[title.length];
				values[i][0]=DateUtil.formatDate(d.getCreateTime(),"yyyy-MM-dd HH:mm:ss");
				values[i][1]=d.getName();
				values[i][2]=d.getEid().toString();
				String lwName="";
				if(d.getState()==1){
					lwName="收到礼物"+"-"+d.getLwName();
				}else if(d.getState()==2){
					lwName="购买花环"+"-"+d.getLwName();
				}else if(d.getState()==3){
					lwName="购买座驾"+"-"+d.getLwName();
				}else if(d.getState()==4){
					lwName="发红包";
				}
				values[i][3]=lwName;
				values[i][4]=d.getConsumeNum().toString();
				values[i][5]=d.getConsumeYzNum().toString();
			}
			HSSFWorkbook wb = ExcelUtil.getHSSFWorkbook("获取优钻记录"+time, title, values, null);
			this.setResponseHeader(response, fileName);
			OutputStream os = response.getOutputStream();
			wb.write(os);
			os.flush();
			os.close();

		} catch (Exception e) {
			e.printStackTrace();
			/*return ApiUtil.putFailObj("导出失败");*/
		}
		return SUCCESS_TIP;
	}

	/**
	 * 提现记录导出
	 * @param appWithdraw
	 * @return
	 */
	@RequestMapping(value = "/appWithdraw")
	@ResponseBody
	public Object appWithdraw(AppWithdraw appWithdraw, HttpServletResponse response) {
		try {
			List<Map<String,Object>> reList=appWithdrawService.getAppWithdraw(appWithdraw);
			Date date = new Date();
			DateFormat format = new SimpleDateFormat("yyyyMMdd");
			String time = format.format(date);
			String fileName = "提现记录"+time+".xls";
			String[] title = new String[] {"申请时间","申请用户","优优ID","提现来源","提现金额","历史提现金额","提现账户","交易流水","状态"};

			String[][] values = new String[reList.size()][];
			for (int i = 0; i < reList.size(); i++) {
				Map<String,Object> d=reList.get(i);
				values[i] = new String[title.length];
				values[i][0]=d.get("createTime").toString();
				values[i][1]=d.get("name").toString();
				values[i][2]=d.get("eid").toString();
				String lyName="";
				if(Integer.valueOf(d.get("state").toString())==1){
					lyName="钻石";
				}else if(Integer.valueOf(d.get("state").toString())==2){
					lyName="分成";
				}
				values[i][3]=lyName;
				values[i][4]=d.get("money").toString();
				values[i][5]=d.get("historyMoney_").toString();
				values[i][6]=d.get("witName")==null?"":d.get("witName").toString()+"-"+d.get("witPhone")==null?"":d.get("witPhone").toString();
				values[i][7]=d.get("serialNum")==null?"":d.get("serialNum").toString();
				String statusNam="";
				if(Integer.valueOf(d.get("status").toString())==1){
					statusNam="待审核";
				}else if(Integer.valueOf(d.get("status").toString())==2){
					statusNam="已提现";
				}else{
					statusNam="已拒绝";
				}
				values[i][8]=statusNam;
			}
			HSSFWorkbook wb = ExcelUtil.getHSSFWorkbook("提现记录"+time, title, values, null);
			this.setResponseHeader(response, fileName);
			OutputStream os = response.getOutputStream();
			wb.write(os);
			os.flush();
			os.close();

		} catch (Exception e) {
			e.printStackTrace();
			/*return ApiUtil.putFailObj("导出失败");*/
		}
		return SUCCESS_TIP;
	}

	/**
	 * 房间流水导出
	 * @param appRoom
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/appYbconsumeRoom")
	@ResponseBody
	public Object appYbconsumeRoom(AppRoom appRoom, HttpServletResponse response) {
		try {
			List<Map<String,Object>> reList=appYbconsumeService.getAppYbconsume(appRoom);
			Date date = new Date();
			DateFormat format = new SimpleDateFormat("yyyyMMdd");
			String time = format.format(date);
			String fileName = "房间流水"+time+".xls";
			String[] title = new String[] {"房间名称","房间ID","房主昵称","是否为牌照房间","本周使用优币","上周使用优币","上上周使用优币","本月使用优币","上月使用优币","历史使用优币"};

			String[][] values = new String[reList.size()][];
			for (int i = 0; i < reList.size(); i++) {
				Map<String,Object> d=reList.get(i);
				values[i] = new String[title.length];
				values[i][0]=d.get("roomName")==null?"":d.get("roomName").toString();
				values[i][1]=d.get("rid").toString();
				values[i][2]=d.get("name")==null?"":d.get("name").toString();
				String fjName="";
				if(Integer.valueOf(d.get("state").toString())==1){
					fjName="否";
				}else if(Integer.valueOf(d.get("state").toString())==2){
					fjName="是";
				}
				values[i][3]=fjName;
				values[i][4]=d.get("consumeNum1").toString();
				values[i][5]=d.get("consumeNum2").toString();
				values[i][6]=d.get("consumeNum3").toString();
				values[i][7]=d.get("consumeNum4").toString();
				values[i][8]=d.get("consumeNum5").toString();;
				values[i][9]=d.get("consumeNum6").toString();;
			}
			HSSFWorkbook wb = ExcelUtil.getHSSFWorkbook("房间流水"+time, title, values, null);
			this.setResponseHeader(response, fileName);
			OutputStream os = response.getOutputStream();
			wb.write(os);
			os.flush();
			os.close();

		} catch (Exception e) {
			e.printStackTrace();
			/*return ApiUtil.putFailObj("导出失败");*/
		}
		return SUCCESS_TIP;
	}
	/**
	 * 协议号模版导出
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/appAgreement")
	@ResponseBody
	public Object appAgreement( HttpServletResponse response) {
		try {
			Date date = new Date();
			DateFormat format = new SimpleDateFormat("yyyyMMdd");
			String time = format.format(date);
			String fileName = "协议号模版"+time+".xls";
			String[] title = new String[] {"协议号系列号(1)","头像(可访问的图片路径)","昵称","出生年月(2019/3/1)","性别(男、女)","个性签名"};

			HSSFWorkbook wb = ExcelUtil.getHSSFWorkbook("协议号模版"+time, title, null, null);
			this.setResponseHeader(response, fileName);
			OutputStream os = response.getOutputStream();
			wb.write(os);
			os.flush();
			os.close();

		} catch (Exception e) {
			e.printStackTrace();
			/*return ApiUtil.putFailObj("导出失败");*/
		}
		return SUCCESS_TIP;
	}
    private void setResponseHeader(HttpServletResponse response, String fileName) {
		try {
			/*try {
				fileName = new String(fileName.getBytes(), "UTF-8");
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}*/
			response.setContentType("application/octet-stream;charset=UTF-8");
			response.setHeader("Content-Disposition", "attachment;filename=" + new String(fileName.getBytes(),"iso-8859-1"));
			response.addHeader("Pargam", "no-cache");
			response.addHeader("Cache-Control", "no-cache");
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
}
