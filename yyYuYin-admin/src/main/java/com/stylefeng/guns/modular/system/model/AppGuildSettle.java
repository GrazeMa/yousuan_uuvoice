package com.stylefeng.guns.modular.system.model;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

/**
 * @Description 公会结算 Entity。
 * @author Grazer_Ma
 * @Date 2020-05-20 20:04:43
 */
@TableName("app_guild_settle")
public class AppGuildSettle extends Model<AppGuildSettle> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    private Date createTime; // 提交时间。
    
    private Double amountOfMoney; // 结算金额。
    private String remarks; // 结算金额。
    private Integer gid; // 结算金额。
    private Double actualSettlementAmount; // 结算金额。
    private Date updateTime; // 审核时间。
    private Integer status; // 状态（0：等待审核；1：审核成功，等待结算；2：审核成功，结算成功；3：审核成功，异常结算；4：已审核，取消结算。）
    private String beginDate; // 开始日期。
    private String endDate; // 结束日期。
    @TableField(exist = false)
    private String guildId;
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
    
    public Double getAmountOfMoney() {
        return amountOfMoney;
    }

    public void setAmountOfMoney(Double amountOfMoney) {
        this.amountOfMoney = amountOfMoney;
    }
    
    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }
    
    public Integer getGid() {
        return gid;
    }

    public void setGid(Integer gid) {
        this.gid = gid;
    }
    
    public Double getActualSettlementAmount() {
        return actualSettlementAmount;
    }

    public void setActualSettlementAmount(Double actualSettlementAmount) {
        this.actualSettlementAmount = actualSettlementAmount;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(String beginDate) {
        this.beginDate = beginDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getGuildId() {
        return guildId;
    }

    public void setGuildId(String guildId) {
        this.guildId = guildId;
    }
    
    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "AppGuildSettle{" +
        "id=" + id +
        ", createTime=" + createTime +
        ", amountOfMoney=" + amountOfMoney +
        ", remarks=" + remarks +
        ", gid=" + gid +
        ", actualSettlementAmount=" + actualSettlementAmount +
        ", updateTime=" + updateTime +
        ", status=" + status +
        ", beginDate=" + beginDate +
        ", endDate=" + endDate +
        ", guildId=" + guildId +
        "}";
    }
}
