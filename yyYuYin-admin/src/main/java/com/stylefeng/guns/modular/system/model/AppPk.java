package com.stylefeng.guns.modular.system.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 房间pk

 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-08
 */
@TableName("app_pk")
public class AppPk extends Model<AppPk> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    private Date createTime;
    /**
     * 用户名称（1）
     */
    private String name;
    /**
     * 用户id（1）
     */
    private Integer uid;
    /**
     * 票数（1）
     */
    private Integer num;
    /**
     * 被一个buid（2）
     */
    private Integer buid;
    /**
     * 票数（2）
     */
    private Integer bnum;
    /**
     * 用户名称（2）
     */
    private String bname;
    /**
     * 1 按人数投票，2 按礼物价值投票
     */
    private Integer state;
    /**
     * 房间的id
     */
    private Integer rid;
    /**
     * '是否被删除（1=否  2=是）'
     */
    private Integer isDelete;
    /**
     * 开始时间
     */
    private Date staTime;
    /**
     * 结束时间
     */
    private Date endTime;
    /**
     * 1 是uid获胜，2是buid获胜，3 平手
     */
    private Integer status;
    /**
     * 秒数
     */
    private Integer second;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public Integer getBuid() {
        return buid;
    }

    public void setBuid(Integer buid) {
        this.buid = buid;
    }

    public Integer getBnum() {
        return bnum;
    }

    public void setBnum(Integer bnum) {
        this.bnum = bnum;
    }

    public String getBname() {
        return bname;
    }

    public void setBname(String bname) {
        this.bname = bname;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Integer getRid() {
        return rid;
    }

    public void setRid(Integer rid) {
        this.rid = rid;
    }

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    public Date getStaTime() {
        return staTime;
    }

    public void setStaTime(Date staTime) {
        this.staTime = staTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getSecond() {
        return second;
    }

    public void setSecond(Integer second) {
        this.second = second;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "AppPk{" +
        "id=" + id +
        ", createTime=" + createTime +
        ", name=" + name +
        ", uid=" + uid +
        ", num=" + num +
        ", buid=" + buid +
        ", bnum=" + bnum +
        ", bname=" + bname +
        ", state=" + state +
        ", rid=" + rid +
        ", isDelete=" + isDelete +
        ", staTime=" + staTime +
        ", endTime=" + endTime +
        ", status=" + status +
        ", second=" + second +
        "}";
    }
}
