package com.stylefeng.guns.modular.system.task.base;

/**
 * @Description 按时间点发布的任务类型
 * @author Leeyns
 * @date 2016年5月17日 下午7:22:28
 */
public enum TimeJobType {
	UNLOCK("UNLOCK","解禁");
	private String type;
	private String desc;
	private TimeJobType(String type, String desc) {
		this.type = type;
		this.desc = desc;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	
}
