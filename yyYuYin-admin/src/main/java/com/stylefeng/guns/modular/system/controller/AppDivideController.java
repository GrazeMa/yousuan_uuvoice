package com.stylefeng.guns.modular.system.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.stylefeng.guns.core.base.controller.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;
import com.stylefeng.guns.modular.system.model.AppDivide;
import com.stylefeng.guns.modular.system.service.IAppDivideService;

import java.util.List;
import java.util.Map;

/**
 * 用户分成控制器
 * @Date 2019-03-06 18:10:46
 */
@Controller
@RequestMapping("/appDivide")
public class AppDivideController extends BaseController {

    private String PREFIX = "/system/appDivide/";

    @Autowired
    private IAppDivideService appDivideService;

    /**
     * 跳转到用户分成首页
     */
    @RequestMapping("")
    public String index(Model model,Integer uid,AppDivide appDivide) {
        model.addAttribute("uid",uid);
        Double sum=0d;
        List<Map<String,Object>> mapList=appDivideService.getAppDivideSum(appDivide);
        if(mapList.size()>0){
            sum = Double.valueOf(mapList.get(0).get("sum").toString());
        }
        model.addAttribute("sum",sum);
        return PREFIX + "appDivide.html";
    }

    /**
     * 跳转到添加用户分成
     */
    @RequestMapping("/appDivide_add")
    public String appDivideAdd() {
        return PREFIX + "appDivide_add.html";
    }

    /**
     * 跳转到修改用户分成
     */
    @RequestMapping("/appDivide_update/{appDivideId}")
    public String appDivideUpdate(@PathVariable Integer appDivideId, Model model) {
        AppDivide appDivide = appDivideService.selectById(appDivideId);
        model.addAttribute("item",appDivide);
        LogObjectHolder.me().set(appDivide);
        return PREFIX + "appDivide_edit.html";
    }

    /**
     * 获取用户分成列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(AppDivide appDivide) {
        return appDivideService.getAppDivideList(appDivide);
    }

    /**
     * 新增用户分成
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(AppDivide appDivide) {
        appDivideService.insert(appDivide);
        return SUCCESS_TIP;
    }

    /**
     * 删除用户分成
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer appDivideId) {
        appDivideService.deleteById(appDivideId);
        return SUCCESS_TIP;
    }

    /**
     * 修改用户分成
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(AppDivide appDivide) {
        appDivideService.updateById(appDivide);
        return SUCCESS_TIP;
    }

    /**
     * 用户分成详情
     */
    @RequestMapping(value = "/detail/{appDivideId}")
    @ResponseBody
    public Object detail(@PathVariable("appDivideId") Integer appDivideId) {
        return appDivideService.selectById(appDivideId);
    }
}
