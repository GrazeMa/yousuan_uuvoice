package com.stylefeng.guns.modular.system.service;

import com.stylefeng.guns.modular.system.model.AppTimeTask;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 定时任务数据 服务类
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-13
 */
public interface IAppTimeTaskService extends IService<AppTimeTask> {

}
