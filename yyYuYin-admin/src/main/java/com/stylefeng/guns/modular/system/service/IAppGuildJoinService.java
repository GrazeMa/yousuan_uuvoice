package com.stylefeng.guns.modular.system.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.system.model.AppGuild;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;
import java.util.Map;

/**
 * @Description 入会申请服务类。
 * @author Grazer_Ma
 * @Date 2020-05-14 22:23:12
 */
public interface IAppGuildJoinService extends IService<AppGuild> {
    /**
     * 获取入会申请信息。
     * @param appGuild
     * @return
     */
    List<Map<String,Object>> getAppGuildJoin(AppGuild appGuild, Page<Map<String, Object>> page);
}
