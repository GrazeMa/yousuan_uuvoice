package com.stylefeng.guns.modular.system.task.jobs;

import com.stylefeng.guns.modular.system.model.AppGift;
import com.stylefeng.guns.modular.system.model.AppRoom;
import com.stylefeng.guns.modular.system.model.AppScene;
import com.stylefeng.guns.modular.system.model.AppTimeTask;
import com.stylefeng.guns.modular.system.task.base.AbstractJob;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * 定时解禁讨论组
 *
 *
 */
public class SceneAndGiftJob extends AbstractJob {

	public static final String name = "SceneAndGiftJob_";

	@Override
	public void execute(JobExecutionContext context)
			throws JobExecutionException {
		JobDataMap maps = context.getMergedJobDataMap();
		Integer  id = maps.getInt("id");
		Integer  type = maps.getInt("type");
		Integer timeTaskId = maps.getInt("timeTaskId");
		try{
			if(type==1){//道具
				AppScene appScene=appSceneService.selectById(id);
				if(appScene.getDay()==0){
					//当天就该下架了
					appScene.setIsState(2);//下架
				}else{
					//如果大于1就需要减掉一天
					appScene.setDay(appScene.getDay()-1);
				}
				appSceneService.updateById(appScene);
			}else if(type==2){//礼物
				AppGift appGift=appGiftService.selectById(id);
				if(appGift.getDay()==0){
					//当天就该下架了
					appGift.setIsState(2);//下架
				}else{
					//如果大于1就需要减掉一天
					appGift.setDay(appGift.getDay()-1);
				}
				appGiftService.updateById(appGift);
			}
			//修改定时任务数据状态
			AppTimeTask timeTask=appTimeTaskService.selectById(timeTaskId);
			//0有效，1无效
			timeTask.setState(1);
			appTimeTaskService.updateById(timeTask);
		}catch(Exception e){
			logger.debug("执行“修改礼物和道具”异常:id={}",id,e);
		}
	}

}
