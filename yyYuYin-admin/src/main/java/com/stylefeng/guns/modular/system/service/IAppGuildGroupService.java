package com.stylefeng.guns.modular.system.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.system.model.AppGuildGroup;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;
import java.util.Map;

/**
 * @Description 公会组服务类。
 * @author Grazer_Ma
 * @Date 2020-05-07 20:42:28
 */
public interface IAppGuildGroupService extends IService<AppGuildGroup> {
    /**
     * 获取公会组信息
     * @param appGuildGroup
     * @return
     */
    List<Map<String,Object>> getAppGuildGroup(AppGuildGroup appGuildGroup, Page<Map<String, Object>> page);
}
