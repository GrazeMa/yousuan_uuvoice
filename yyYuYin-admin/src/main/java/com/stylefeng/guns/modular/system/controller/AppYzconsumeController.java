package com.stylefeng.guns.modular.system.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.util.SinataUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;
import com.stylefeng.guns.modular.system.model.AppYzconsume;
import com.stylefeng.guns.modular.system.service.IAppYzconsumeService;

/**
 * 优钻获取控制器
 * @Date 2019-03-14 11:23:19
 */
@Controller
@RequestMapping("/appYzconsume")
public class AppYzconsumeController extends BaseController {

    private String PREFIX = "/system/appYzconsume/";

    @Autowired
    private IAppYzconsumeService appYzconsumeService;

    /**
     * 跳转到优钻获取首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "appYzconsume.html";
    }

    /**
     * 跳转到添加优钻获取
     */
    @RequestMapping("/appYzconsume_add")
    public String appYzconsumeAdd() {
        return PREFIX + "appYzconsume_add.html";
    }

    /**
     * 跳转到修改优钻获取
     */
    @RequestMapping("/appYzconsume_update/{appYzconsumeId}")
    public String appYzconsumeUpdate(@PathVariable Integer appYzconsumeId, Model model) {
        AppYzconsume appYzconsume = appYzconsumeService.selectById(appYzconsumeId);
        model.addAttribute("item",appYzconsume);
        LogObjectHolder.me().set(appYzconsume);
        return PREFIX + "appYzconsume_edit.html";
    }

    /**
     * 获取优钻获取列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(AppYzconsume appYzconsume,String beginTime,String endTime) {
        EntityWrapper<AppYzconsume> entityWrapper=new EntityWrapper<>();
        if(SinataUtil.isNotEmpty(appYzconsume.getEid())){
            entityWrapper.eq("eid",appYzconsume.getEid());
        }
        if(SinataUtil.isNotEmpty(appYzconsume.getName())){
            entityWrapper.like("name",appYzconsume.getName());
        }
        if(SinataUtil.isNotEmpty(beginTime)){
            entityWrapper.ge("createTime",beginTime +" 00:00:00");
        }
        if(SinataUtil.isNotEmpty(endTime)){
            entityWrapper.le("createTime",endTime +" 23:59:59");
        }
        entityWrapper.eq("isDelete",1);
        entityWrapper.orderBy("createTime",false);
        return appYzconsumeService.selectList(entityWrapper);
    }

    /**
     * 新增优钻获取
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(AppYzconsume appYzconsume) {
        appYzconsumeService.insert(appYzconsume);
        return SUCCESS_TIP;
    }

    /**
     * 删除优钻获取
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer appYzconsumeId) {
        appYzconsumeService.deleteById(appYzconsumeId);
        return SUCCESS_TIP;
    }

    /**
     * 修改优钻获取
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(AppYzconsume appYzconsume) {
        appYzconsumeService.updateById(appYzconsume);
        return SUCCESS_TIP;
    }

    /**
     * 优钻获取详情
     */
    @RequestMapping(value = "/detail/{appYzconsumeId}")
    @ResponseBody
    public Object detail(@PathVariable("appYzconsumeId") Integer appYzconsumeId) {
        return appYzconsumeService.selectById(appYzconsumeId);
    }
}
