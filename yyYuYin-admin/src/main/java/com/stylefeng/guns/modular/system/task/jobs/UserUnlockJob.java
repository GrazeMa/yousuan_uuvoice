package com.stylefeng.guns.modular.system.task.jobs;

import com.stylefeng.guns.modular.system.model.AppTimeTask;
import com.stylefeng.guns.modular.system.model.AppUser;
import com.stylefeng.guns.modular.system.task.base.AbstractJob;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * 定时解禁用户
 *
 *
 */
public class UserUnlockJob extends AbstractJob {

	public static final String name = "UnlockUser_";


	@Override
	public void execute(JobExecutionContext context)
			throws JobExecutionException {
		JobDataMap maps = context.getMergedJobDataMap();
		Integer userId = maps.getInt("id");
		Integer timeTaskId = maps.getInt("timeTaskId");
		try{
			//修改用户状态
			AppUser  appUser=new AppUser();
			appUser.setId(userId);
			appUser.setState(1);//是否冻结 1否,2是
			appUserService.updateById(appUser);
			//修改定时任务数据状态
			AppTimeTask timeTask=appTimeTaskService.selectById(timeTaskId);
			//0有效，1无效
			timeTask.setState(1);
			appTimeTaskService.updateById(timeTask);
			logger.debug("执行“定时解禁用户”完成：userId={}",userId);
		}catch(Exception e){
			logger.debug("执行“定时解禁用户”异常:userId={}",userId,e);
		}
	}

}
