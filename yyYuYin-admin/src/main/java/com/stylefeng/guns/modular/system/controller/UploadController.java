package com.stylefeng.guns.modular.system.controller;

import com.stylefeng.guns.config.properties.GunsProperties;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.common.exception.BizExceptionEnum;
import com.stylefeng.guns.core.exception.GunsException;
import com.stylefeng.guns.core.util.OssUploadUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.util.UUID;

/**
 * 上传文件
 */
@RestController
@RequestMapping("/upload")
public class UploadController extends BaseController {
    private final static Logger log = LoggerFactory.getLogger(UploadController.class);

    @Autowired
    private GunsProperties gunsProperties;

    /**
     * 上传图片(上传到项目的webapp/static/img)
     */
    /*@RequestMapping("/image")
    public String image(@RequestPart("file") MultipartFile picture) {
        String pictureName = UUID.randomUUID().toString() + ".jpg";
        try {
            // 文件目录路径
            String fileSavePath = gunsProperties.getFileUploadPath();
            picture.transferTo(new File(fileSavePath + pictureName));
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new GunsException(BizExceptionEnum.UPLOAD_ERROR);
        }
        return pictureName;
    }*/

    /**
     * 上传图片
     * @param picture
     * @param request
     * @return
     */
    @RequestMapping("/image")
    @ResponseBody
    public String image(@RequestPart("file") MultipartFile picture,HttpServletRequest request) {
        try {
            MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
            MultipartFile file = (MultipartFile) picture;

            String pictureName = OssUploadUtil.ossUpload(request,"img/", file);
            return pictureName;
        } catch (IOException e1) {
            return null;
        }
    }

    /**
     * 上传音乐
     * @param picture
     * @param request
     * @return
     */
    @RequestMapping("/music")
    @ResponseBody
    public String music(@RequestPart("file") MultipartFile picture,HttpServletRequest request) {
        try {
            MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
            MultipartFile file = (MultipartFile) picture;

            String pictureName = OssUploadUtil.ossUpload(request,"music/", file);
            //String pictureName="http://yoyovoice.oss-cn-shanghai.aliyuncs.com/music/f0fe4ca11dff436babe97c9b04174b53.mp3";
            return pictureName;
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 上传svga动图
     * @param picture
     * @param request
     * @return
     */
    @RequestMapping("/svga")
    @ResponseBody
    public String svga(@RequestPart("file") MultipartFile picture,HttpServletRequest request) {
        try {
            MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
            MultipartFile file = (MultipartFile) picture;
            String pictureName = UUID.randomUUID().toString() + ".svga";
            String fileSavePath = gunsProperties.getSvgaUploadPath();
            // 文件目录路径
            file.transferTo(new File(fileSavePath + pictureName));
            return gunsProperties.getSvgaServerAddress() + pictureName;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new GunsException(BizExceptionEnum.UPLOAD_ERROR);
        }
    }

    /**
     * UEditor编辑器上传图片
     */
    @RequestMapping("/imageUp")
    public String imageUp(@RequestPart("upfile") MultipartFile picture, HttpServletRequest request) {
        String callback = request.getParameter("callback");
        String pictureName = UUID.randomUUID().toString() + ".jpg";
        try {
            // 上传文件目录
            String fileSavePath = gunsProperties.getFileUploadPath();
            picture.transferTo(new File(fileSavePath + pictureName));
            // 文件全路径
            pictureName = gunsProperties.getPictureServerAddress() + pictureName;

            String result = "{'original': '" + picture.getOriginalFilename() + "', 'state': 'SUCCESS', 'url': '" + pictureName + "'}";
            if (callback == null) {
                return result;
            } else {
                return "<script>" + callback + "(" + result + ")</script>";
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            String result = "{'original': '', 'state': '文件上传失败','url': ''}";
            if (callback == null) {
                return result;
            } else {
                return "<script>" + callback + "(" + result + ")</script>";
            }
        }
    }

}
