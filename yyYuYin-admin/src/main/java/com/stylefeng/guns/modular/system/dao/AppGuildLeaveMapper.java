package com.stylefeng.guns.modular.system.dao;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.system.model.AppGuild;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

/**
 * @Description 退会申请 Mapper 接口。
 * @author Grazer_Ma
 * @Date 2020-05-14 23:26:42
 */
public interface AppGuildLeaveMapper extends BaseMapper<AppGuild> {
    /**
     * 获取退会申请。
     * @param appGuild
     * @return
     */
    List<Map<String,Object>> getAppGuildLeave(AppGuild appGuild, Page<Map<String,Object>> page);
}
