package com.stylefeng.guns.modular.system.dao;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.system.model.AppGuildGroup;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

/**
 * @Description 公会组 Mapper 接口。
 * @author Grazer_Ma
 * @Date 2020-05-07 20:40:12
 */
public interface AppGuildGroupMapper extends BaseMapper<AppGuildGroup> {
    /**
     * 获取公会组信息。
     * @param appGuildGroup
     * @return
     */
    List<Map<String,Object>> getAppGuildGroup(AppGuildGroup appGuildGroup, Page<Map<String,Object>> page);
}
