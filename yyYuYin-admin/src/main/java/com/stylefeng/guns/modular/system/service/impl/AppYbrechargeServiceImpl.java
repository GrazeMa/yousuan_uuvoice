package com.stylefeng.guns.modular.system.service.impl;

import com.stylefeng.guns.modular.system.model.AppYbrecharge;
import com.stylefeng.guns.modular.system.dao.AppYbrechargeMapper;
import com.stylefeng.guns.modular.system.service.IAppYbrechargeService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 优币充值记录 服务实现类
 * </p>
 *
 * @author stylefeng123
 * @since 2019-03-14
 */
@Service
public class AppYbrechargeServiceImpl extends ServiceImpl<AppYbrechargeMapper, AppYbrecharge> implements IAppYbrechargeService {

}
