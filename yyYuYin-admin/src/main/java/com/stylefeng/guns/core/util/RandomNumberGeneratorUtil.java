package com.stylefeng.guns.core.util;

import java.util.Random;

public class RandomNumberGeneratorUtil {
	/** 
	* 这是典型的随机洗牌算法。 
	* 流程是从备选数组中选择一个放入目标数组中，将选取的数组从备选数组移除（放至最后，并缩小选择区域） 
	* 算法时间复杂度O(n) 
	* @return 随机8为不重复数组 
	*/ 
	public static String generateNumber() { 
	String no=""; 
	//初始化备选数组 
	int[] defaultNums = new int[10]; 
	for (int i = 0; i < defaultNums.length; i++) { 
	defaultNums[i] = i; 
	} 

	Random random = new Random(); 
	int[] nums = new int[LENGTH]; 
	//默认数组中可以选择的部分长度 
	int canBeUsed = 10; 
	//填充目标数组 
	for (int i = 0; i < nums.length; i++) { 
	//将随机选取的数字存入目标数组 
	int index = random.nextInt(canBeUsed); 
	nums[i] = defaultNums[index]; 
	//将已用过的数字扔到备选数组最后，并减小可选区域 
	swap(index, canBeUsed - 1, defaultNums); 
	canBeUsed--; 
	} 
	if (nums.length>0) { 
	for (int i = 0; i < nums.length; i++) { 
	no+=nums[i]; 
	} 
	} 

	return no; 
	} 
	private static final int LENGTH = 8; 

	private static void swap(int i, int j, int[] nums) { 
	int temp = nums[i]; 
	nums[i] = nums[j]; 
	nums[j] = temp; 
	} 

	public static String generateNumber2() { 
	String no=""; 
	int num[]=new int[8]; 
	int c=0; 
	for (int i = 0; i < 8; i++) { 
	num[i] = new Random().nextInt(10); 
	c = num[i]; 
	for (int j = 0; j < i; j++) { 
	if (num[j] == c) { 
	i--; 
	break; 
	} 
	} 
	} 
	if (num.length>0) { 
	for (int i = 0; i < num.length; i++) { 
	no+=num[i]; 
	} 
	} 
	return no; 
	} 
	


public static String generate() {

    char[] letters = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
            'K', 'L', 'M', 'N', 'O', 'P','Q', 'R', 'S', 'T', 'U', 'V',
            'W', 'X', 'Y', 'Z','0','1','2','3','4','5','6','7','8','9'};
    boolean[] flags = new boolean[letters.length];
    char[] chs = new char[6];
    for (int i = 0; i < chs.length; i++) {
        int index;
        do {
            index = (int) (Math.random() * (letters.length));
        } while (flags[index]);// 判断生成的字符是否重复
        chs[i] = letters[index];
        flags[index] = true;
    }
    return chs.toString();
}



public static String getStringRandom(int length) {  

    String val = "";  
    Random random = new Random();        
    //length为几位密码 
    for(int i = 0; i < length; i++) {          
        String charOrNum = random.nextInt(2) % 2 == 0 ? "char" : "num";  
        //输出字母还是数字  
        if( "char".equalsIgnoreCase(charOrNum) ) {  
            //输出是大写字母还是小写字母  
            int temp = random.nextInt(2) % 2 == 0 ? 65 : 97;  
            val += (char)(random.nextInt(26) + temp);  
        } else if( "num".equalsIgnoreCase(charOrNum) ) {  
            val += String.valueOf(random.nextInt(10));  
        }  
    }  
    return val;  
}  


//public static void main(String [] arg){
//	String ss = generate();
//	System.out.println(ss);
//}
}
