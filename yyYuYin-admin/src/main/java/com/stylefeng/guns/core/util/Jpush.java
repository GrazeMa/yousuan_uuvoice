package com.stylefeng.guns.core.util;


import cn.jiguang.common.ClientConfig;
import cn.jiguang.common.resp.APIConnectionException;
import cn.jiguang.common.resp.APIRequestException;
import cn.jpush.api.JPushClient;
import cn.jpush.api.push.PushResult;
import cn.jpush.api.push.model.Options;
import cn.jpush.api.push.model.Platform;
import cn.jpush.api.push.model.PushPayload;
import cn.jpush.api.push.model.audience.Audience;
import cn.jpush.api.push.model.audience.AudienceTarget;
import cn.jpush.api.push.model.notification.AndroidNotification;
import cn.jpush.api.push.model.notification.IosNotification;
import cn.jpush.api.push.model.notification.Notification;
import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.Map;
import com.stylefeng.guns.core.util.ParamUtil;

public class Jpush {

	private static Logger LOG = Logger.getLogger(Jpush.class);

	//public static String masterSecret = "5c77ab215d42f82c5a4896ed";
	//public static String appKey = "89786eac2d09202739777a20";
	  public static String masterSecret = ParamUtil.getValue("jpush_app_secret");
	  public static String appKey = ParamUtil.getValue("jpush_app_key");

	public static void push(Map<String, String> map, String alert) {
		System.out.println("jPush__" + map + "__" + alert);
		if (map == null || map.size() == 0) {
			return;
		}
		String[] alias = new String[map.size()];
		int i = 0;
		for (Map.Entry<String, String> m : map.entrySet()) {
			alias[i] = m.getKey();
			i++;
		}
		// For push, all you need do is to build PushPayload object.
		PushPayload payload = buildPushObject_ios_audienceMore_messageWithExtras(alias, alert);
		try {
			// JPushClient jpushClient = new JPushClient(masterSecret, appKey,
			// 3);
			JPushClient jpushClient = new JPushClient(masterSecret, appKey, null, ClientConfig.getInstance());
			PushResult result = jpushClient.sendPush(payload);
			LOG.info("Got result - " + result);

		} catch (APIConnectionException e) {
			// Connection error, should retry later
			LOG.error("Connection error, should retry later", e);

		} catch (APIRequestException e) {
			// Should review the error, and fix the request
			LOG.error("Should review the error, and fix the request", e);
			LOG.info("HTTP Status: " + e.getStatus());
			LOG.info("Error Code: " + e.getErrorCode());
			LOG.info("Error Message: " + e.getErrorMessage());
		}
	}

	/**
	 * 推送安卓（别名）
	 */
	public static PushPayload buildPushObject_all_alias_alert(String alias, String alert) {
		return PushPayload.newBuilder().setPlatform(Platform.all()).setAudience(Audience.alias(alias))
				.setNotification(Notification.alert(alert)).build();
	}

	/**
	 * 推送安卓和IOS（别名）
	 */
	public static PushPayload buildPushObject_ios_audienceMore_messageWithExtras(String[] alias, String alert) {
		return PushPayload.newBuilder().setPlatform(Platform.android_ios())
				.setAudience(Audience.newBuilder().addAudienceTarget(AudienceTarget.alias(alias)).build())
				// //发送推送自定义消息
				// .setMessage(Message.newBuilder()
				// .setMsgContent(alert)
				// .addExtra("from", "JPush")
				// .build())
				// 发送通知
				.setNotification(Notification.alert(alert)).build();
	}

	/**
	 * 极光推送
	 *
	 * @param alias
	 * @param alert
	 */
	public static void jiguangPush(String alias, String alert, Map<String, String> extra) {
		LOG.info("对别名" + alias + "的用户推送信息:" + alert);
		PushResult result = push(alias, alert, extra);
		if (result != null && result.isResultOK()) {
			LOG.info("针对别名" + alias + "的信息推送成功！");
		} else {
			LOG.info("针对别名" + alias + "的信息推送失败！");
		}
	}

	/**
	 * 生成极光推送对象PushPayload（采用java SDK）
	 *
	 * @param alias
	 * @param alert
	 * @return PushPayload
	 */
	public static PushPayload buildPushObject_android_ios_alias_alert(String alias, String alert,
																	  Map<String, String> extra) {
		return PushPayload.newBuilder()
				.setPlatform(
						Platform.android_ios())
				.setAudience(Audience.alias(alias))
				.setNotification(Notification.newBuilder()
						.addPlatformNotification(AndroidNotification.newBuilder().addExtra("type", "infomation")
								.setAlert(alert).addExtras(extra).build())
						.addPlatformNotification(
								IosNotification.newBuilder().addExtra("type", "infomation").setAlert(alert).setBadge(5)
										.setSound("default").addExtras(extra).addExtra("from", "JPush").build())
						.build())
				.setOptions(Options.newBuilder().setApnsProduction(false)// true-推送生产环境
						// false-推送开发环境（测试使用参数）
						.setTimeToLive(90)// 消息在JPush服务器的失效时间（测试使用参数）
						.build())
				.build();
	}

	/**
	 * 极光推送方法(采用java SDK)
	 *
	 * @param alias
	 * @param alert
	 * @return PushResult
	 */
	public static PushResult push(String alias, String alert, Map<String, String> extra) {
		ClientConfig clientConfig = ClientConfig.getInstance();
		JPushClient jpushClient = new JPushClient(masterSecret, appKey, null, clientConfig);
		PushPayload payload = buildPushObject_android_ios_alias_alert(alias+"", alert, extra);
		try {
			return jpushClient.sendPush(payload);
		} catch (APIConnectionException e) {
			e.printStackTrace();
			LOG.error("Connection error. Should retry later. ", e);
			return null;
		} catch (APIRequestException e) {
			e.printStackTrace();
			LOG.info("Error Message: " + e.getErrorMessage());
			LOG.info("Msg ID: " + e.getMsgId());
			LOG.error("Error response from JPush server. Should review and fix it. ", e);
			LOG.info("HTTP Status: " + e.getStatus());
			LOG.info("Error Code: " + e.getErrorCode());
			System.out.println(e.getStatus());
			System.out.println(e.getErrorCode());
			System.out.println(e.getErrorMessage());
			System.out.println(e.getMsgId());
			return null;
		}
	}

	public static void main(String[] args) {
		/*
		 * Map<String, String> map = new HashMap<String, String>();
		 * map.put("26", ""); map.put("17", ""); Jpush.push(map,
		 * "公告消息来了：测试Jpush"); Jpush.push("t1", "及地方大富科技！");
		 * sendNotificationWithAlias("回复接单后","gfgf","t1",map);
		 */
		Map<String, String> map = new HashMap<String, String>();
		map.put("26", "");
		map.put("17", "");
		Integer m=12;
		Jpush.push(m.toString(), "测试成功",map);
	}
}
