package com.stylefeng.guns.core.util;

import com.aliyun.oss.OSSClient;
import com.aliyun.oss.model.DeleteObjectsRequest;
import com.aliyun.oss.model.DeleteObjectsResult;
import com.aliyun.oss.model.ObjectMetadata;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.UUID;

import com.stylefeng.guns.core.util.ParamUtil;

public class OssUploadUtil {
	//名的OSS图片访问域名	
//	public static String oss_domain = "http://uuvoices.oss-cn-shanghai.aliyuncs.com/";
//	public static String accessKeyId = "LTAI4FscTNLWYui8mPGAdugs";
//	public static String accessKeySecret = "K1QBvSgovldMrmLhtoNvbjxrowdvcC";
//	public static String bucketName="uuvoices";
//	public static String endpoint = "oss-cn-shanghai.aliyuncs.com";
	
	public static String oss_domain = ParamUtil.getValue("aliyun_oss_domain");
	public static String accessKeyId = ParamUtil.getValue("aliyun_accessKeyId");
	public static String accessKeySecret = ParamUtil.getValue("aliyun_accessKeySecret");
	public static String bucketName = ParamUtil.getValue("aliyun_bucketName");
	public static String endpoint = ParamUtil.getValue("aliyun_endpoint");
	
	public static OSSClient ossClient = new OSSClient(endpoint, accessKeyId,accessKeySecret);
	
	public static String ossUpload(HttpServletRequest request,String fileType,MultipartFile file) throws IOException{
		//CommonsMultipartFile file = (CommonsMultipartFile)multipartFile;
		String fileName = "";
			if(file!=null && !"".equals(file.getOriginalFilename()) && file.getOriginalFilename()!=null){
				InputStream content = file.getInputStream();//获得指定文件的输入流
				ObjectMetadata meta = new ObjectMetadata();// 创建上传Object的Metadata
				meta.setContentLength(file.getSize());  // 必须设置ContentLength
				String originalFilename = file.getOriginalFilename();
				fileName =  UUID.randomUUID().toString().replaceAll("-","") + originalFilename.subSequence(originalFilename.lastIndexOf("."), originalFilename.length());
				ossClient.putObject(bucketName,fileType+fileName,content,meta);// 上传Object.
				if(fileName != null && !"".equals(fileName)){
					System.out.println(fileName);
					fileName = oss_domain+fileType+fileName;
				}
			}
		return fileName;
	}

	/**
	 * 删除某个Object
	 *
	 * @param bucketUrl
	 * @return
	 */
	public static boolean deleteObject(String bucketUrl) {
		try {
			// 删除Object.
			ossClient.deleteObject(bucketName, bucketUrl);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			//ossClient.shutdown();
		}
		return true;
	}

	/**
	 * 删除多个Object
	 *
	 * @param
	 * @param bucketUrls
	 * @return
	 */
	public static boolean deleteObjects(List<String> bucketUrls) {
		try {
			// 删除Object.
			DeleteObjectsResult deleteObjectsResult = ossClient.deleteObjects(new DeleteObjectsRequest(bucketName).withKeys(bucketUrls));
			List<String> deletedObjects = deleteObjectsResult.getDeletedObjects();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			//ossClient.shutdown();
		}
		return true;
	}
	
//	 public static void createBucket(String bucketName)
//     {
//         //初始化 OSSClient
////          ossClient = new OssClient(endPoint, accessKeyId, accessKeySecret);
//
//         // 新建一个Bucket
//         Bucket bucket = ossClient.createBucket(bucketName);
//         System.out.println(bucket.getName());
//         System.out.println(bucket.getCreationDate());
//     }
//	 
//	 public static void main(String[] args) {
//		 OssUploadUtil.createBucket("ssfdfsd");
//	}
}
