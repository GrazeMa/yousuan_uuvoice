package com.stylefeng.guns;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Author Grazer_Ma
 * @Date 2020-05-07 09:23:28
 * @Description SpringBoot 启动类。
 */
@SpringBootApplication
public class GunsApplication {

    private final static Logger logger = LoggerFactory.getLogger(GunsApplication.class);

    public static void main(String[] args) {
    	
        SpringApplication.run(GunsApplication.class, args);
        logger.warn("====== SpringBoot GunsApplication is success. ======");
        
    }
    
}
