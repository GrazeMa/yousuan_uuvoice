package com.stylefeng.guns.rest.modular.system.service;

import com.stylefeng.guns.rest.modular.system.model.Treasure;
import com.stylefeng.guns.rest.modular.system.model.Treasure;

import java.util.List;

import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 房间财富值 服务类
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
public interface ITreasureService extends IService<Treasure> {
public List<Treasure> getList(Treasure model);

     List<Treasure> getSum(Treasure model);
    
    Treasure getOne(Treasure model);
    
    List<Treasure> getSumH(Treasure model);
}
