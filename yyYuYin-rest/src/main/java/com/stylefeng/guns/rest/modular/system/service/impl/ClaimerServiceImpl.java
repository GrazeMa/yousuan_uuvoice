package com.stylefeng.guns.rest.modular.system.service.impl;

import com.stylefeng.guns.rest.modular.system.model.Claimer;
import com.stylefeng.guns.rest.modular.system.dao.ClaimerMapper;
import com.stylefeng.guns.rest.modular.system.service.IClaimerService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import java.util.List;

import org.springframework.stereotype.Service;

/**
 * <p>
 * 推荐位申请记录 服务实现类
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
@Service
public class ClaimerServiceImpl extends ServiceImpl<ClaimerMapper, Claimer> implements IClaimerService {

	@Override
	public List<Claimer> getList(Claimer model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getList(model);
	}

	@Override
	public Claimer getOne(Claimer model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getOne(model);
	}

}
