package com.stylefeng.guns.rest.modular.system.service;

import java.util.List;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.rest.modular.system.model.BulletScreen;

/**
 * @Description 弹幕服务类。
 * @author Grazer_Ma
 * @Date 2020-05-20 11:13:23
 */
public interface IBulletScreenService extends IService<BulletScreen> {
	
	public List<BulletScreen> getList(BulletScreen model);
    
	BulletScreen getOne(BulletScreen model);
	
}
