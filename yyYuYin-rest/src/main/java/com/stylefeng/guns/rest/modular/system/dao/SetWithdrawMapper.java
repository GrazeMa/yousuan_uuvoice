package com.stylefeng.guns.rest.modular.system.dao;

import com.stylefeng.guns.rest.modular.system.model.SetWithdraw;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 提现设置 Mapper 接口
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
public interface SetWithdrawMapper extends BaseMapper<SetWithdraw> {

	List<SetWithdraw> getList(SetWithdraw model);

	SetWithdraw getOne(SetWithdraw model);

}
