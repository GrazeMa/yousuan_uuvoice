package com.stylefeng.guns.rest.modular.system.controller.model;

public class AttentionModel {
	private Integer id;
	// 姓名
	private String name;
	// 性别
	private Integer sex;
	// 头像
	private String img;
	// 粉丝
	private Integer num;
	// 财富
	private Integer treasureGrade;
	// 魅力
	private Integer charmGrade;
	// 唯一标识id
	private String usercoding;

	// 星座
	private String constellation;
	
	private String liang;
	
	

	public String getLiang() {
		return liang;
	}

	public void setLiang(String liang) {
		this.liang = liang;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getSex() {
		return sex;
	}

	public void setSex(Integer sex) {
		this.sex = sex;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public Integer getNum() {
		return num;
	}

	public void setNum(Integer num) {
		this.num = num;
	}

	public Integer getTreasureGrade() {
		return treasureGrade;
	}

	public void setTreasureGrade(Integer treasureGrade) {
		this.treasureGrade = treasureGrade;
	}

	public Integer getCharmGrade() {
		return charmGrade;
	}

	public void setCharmGrade(Integer charmGrade) {
		this.charmGrade = charmGrade;
	}

	public String getUsercoding() {
		return usercoding;
	}

	public void setUsercoding(String usercoding) {
		this.usercoding = usercoding;
	}

	public String getConstellation() {
		return constellation;
	}

	public void setConstellation(String constellation) {
		this.constellation = constellation;
	}

}
