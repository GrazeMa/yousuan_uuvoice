package com.stylefeng.guns.rest.modular.system.service.impl;

import com.stylefeng.guns.rest.modular.system.model.MessageInfo;
import com.stylefeng.guns.rest.modular.system.dao.MessageInfoMapper;
import com.stylefeng.guns.rest.modular.system.service.IMessageInfoService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import java.util.List;

import org.springframework.stereotype.Service;

/**
 * <p>
 * 系统消息 服务实现类
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
@Service
public class MessageInfoServiceImpl extends ServiceImpl<MessageInfoMapper, MessageInfo> implements IMessageInfoService {

	@Override
	public List<MessageInfo> getList(MessageInfo model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getList(model);
	}

	@Override
	public MessageInfo getOne(MessageInfo model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getOne(model);
	}

}
