package com.stylefeng.guns.rest.modular.system.service.impl;

import com.stylefeng.guns.rest.modular.system.model.Hot;
import com.stylefeng.guns.rest.modular.system.dao.HotMapper;
import com.stylefeng.guns.rest.modular.system.service.IHotService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import java.util.List;

import org.springframework.stereotype.Service;

/**
 * <p>
 * 热搜词 服务实现类
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
@Service
public class HotServiceImpl extends ServiceImpl<HotMapper, Hot> implements IHotService {

	@Override
	public List<Hot> getList(Hot model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getList(model);
	}

	@Override
	public Hot getOne(Hot model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getOne(model);
	}

}
