package com.stylefeng.guns.rest.modular.system.service;

import com.stylefeng.guns.rest.modular.system.model.Scene;
import com.stylefeng.guns.rest.modular.system.model.Scene;

import java.util.List;

import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 道具管理 服务类
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
public interface ISceneService extends IService<Scene> {
public List<Scene> getList(Scene model);
public List<Scene> getList1(Scene model);
    
    Scene getOne(Scene model);
    Scene getOne1(Scene model);
}
