package com.stylefeng.guns.rest.modular.system.service.impl;

import com.stylefeng.guns.rest.modular.system.model.Recommend;
import com.stylefeng.guns.rest.modular.system.dao.RecommendMapper;
import com.stylefeng.guns.rest.modular.system.service.IRecommendService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import java.util.List;

import org.springframework.stereotype.Service;

/**
 * <p>
 * 推荐位置管理 服务实现类
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
@Service
public class RecommendServiceImpl extends ServiceImpl<RecommendMapper, Recommend> implements IRecommendService {

	@Override
	public List<Recommend> getList(Recommend model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getList(model);
	}

	@Override
	public Recommend getOne(Recommend model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getOne(model);
	}

}
