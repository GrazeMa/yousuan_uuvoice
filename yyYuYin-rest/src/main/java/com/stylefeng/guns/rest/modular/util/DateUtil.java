package com.stylefeng.guns.rest.modular.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

/**
 * <h3>处理时间的工具类</h3>
 */
public class DateUtil {

	private static TimeZone tz = TimeZone.getTimeZone("GMT+8");

	// private static TimeZone tz = TimeZone.getTimeZone("Asia/Shanghai");
	
	public static boolean hourMinuteBetween(String nowDate, String startDate, String endDate) throws Exception{
		
		SimpleDateFormat format = new SimpleDateFormat("HH:mm");
		
		Date now = format.parse(nowDate);
		Date start = format.parse(startDate);
		Date end = format.parse(endDate);
		
		long nowTime = now.getTime();
		long startTime = start.getTime();
		long endTime = end.getTime();
		
		return nowTime >= startTime && nowTime <= endTime;
	}

	/**
	 * 得到系统日期
	 * 
	 * @return
	 */
	public static Date getDate() {
		TimeZone.setDefault(tz);
		return new Date();
	}

	/**
	 * 获取当然凌晨的时间
	 * 
	 * @return Date
	 */
	public static Date getZero() {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		return calendar.getTime();
	}

	/**
	 * 判断日期是否在from，to之内 "yyyy-MM-dd" 格式
	 * 
	 * @param time
	 *            指定日期
	 * @param from
	 *            开始日期
	 * @param to
	 *            结束日期
	 * @return true 在之间 false 不在之间
	 */
	public static boolean belongCalendar(Date time, Date from, Date to) {
		Calendar date = Calendar.getInstance();
		date.setTime(time);

		Calendar after = Calendar.getInstance();
		after.setTime(from);

		Calendar before = Calendar.getInstance();
		before.setTime(to);

		if ((date.after(after) && date.before(before)) || (time.compareTo(from) == 0 || time.compareTo(to) == 0)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 两个时间之差
	 * 
	 * @param startTime
	 * @param endTime
	 * @param format
	 * @return
	 * @throws ParseException
	 */
	public static String dateDiff(String startTime, String endTime, String format) throws ParseException {
		// 按照传入的格式生成一个simpledateformate对象
		SimpleDateFormat sd = new SimpleDateFormat(format);
		long nd = 1000 * 24 * 60 * 60;// 一天的毫秒数
		long nh = 1000 * 60 * 60;// 一小时的毫秒数
		long nm = 1000 * 60;// 一分钟的毫秒数
		long ns = 1000;// 一秒钟的毫秒数
		long diff;
		long day = 0;
		long hour = 0;
		long min = 0;
		long sec = 0;
		// long time=0;
		String strTime = "";
		// 获得两个时间的毫秒时间差异
		diff = sd.parse(endTime).getTime() - sd.parse(startTime).getTime();
		day = diff / nd;// 计算差多少天
		hour = diff % nd / nh + day * 24;// 计算差多少小时
		min = diff % nd % nh / nm + day * 24 * 60;// 计算差多少分钟
		sec = diff % nd % nh % nm / ns;// 计算差多少秒
		// 输出结果
		/*
		 * System.out.println("时间相差：" + day + "天" + (hour - day * 24) + "小时" +
		 * (min - day * 24 * 60) + "分钟" + sec + "秒。");
		 * System.out.println("hour=" + hour + ",min=" + min);
		 */
		if (day == 1) {
			strTime = "昨天";
		} else if (day > 1) {
			// strTime=day+"天前";
			strTime = startTime.substring(0, 10);
		} else if (hour >= 1 && hour < 24) {
			strTime = hour + "小时前";
		} else {
			if (min == 0) {
				strTime = sec + "秒钟前";
			} else {
				strTime = min + "分钟前";
			}

		}
		// if (str.equalsIgnoreCase("h")) {
		// return hour;
		// } else {
		// return min;
		// }

		// if (str.equalsIgnoreCase("h")) {
		// return hour;
		// } else {
		// return min;
		// }
		return strTime;
	}

	/**
	 * 得到系统Calendar日期
	 * 
	 * @return
	 */
	public static Calendar getCalendar() {
		TimeZone.setDefault(tz);
		Calendar cal = Calendar.getInstance();
		return cal;
	}

	/**
	 * 获取当前时间
	 * 
	 * @return
	 */
	public static long getMillisecond() {
		long millisecond = 0;
		TimeZone.setDefault(tz);
		Calendar cal = Calendar.getInstance();
		millisecond = cal.getTimeInMillis();
		return millisecond;
	}

	/**
	 * 获取本月1号的时间戳
	 * 
	 * @return
	 */
	public static long getMillisecond_MONTH() {
		long millisecond = 0;
		TimeZone.setDefault(tz);
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.DAY_OF_MONTH, 1);
		millisecond = cal.getTimeInMillis();
		return millisecond;
	}

	/**
	 * 获取上个月1号的时间戳
	 * 
	 * @return
	 */
	public static long getMillisecond_FRONTMONTH() {
		long millisecond = 0;
		Calendar cal = getCalendar();
		cal.set(Calendar.DAY_OF_MONTH, 1);
		cal.set(Calendar.MONTH, Calendar.MONTH - 2);
		millisecond = cal.getTimeInMillis();
		return millisecond;
	}

	/**
	 * 获取当前毫秒数
	 * 
	 * @return long
	 */
	public static long getCurMilli() {
		long millisecond = 0;
		Calendar cal = Calendar.getInstance();
		millisecond = cal.getTimeInMillis();
		return millisecond;
	}

	/**
	 * 日期转毫秒
	 * 
	 * @param date
	 * @return
	 */
	public static long getMillisecond(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String newDate = "";
		if (!"".equals(date)) {
			newDate = sdf.format(date);
		} else {
			newDate = sdf.format(DateUtil.getDate());
		}
		long millisecond = 0;
		try {
			millisecond = sdf.parse(newDate).getTime();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return millisecond;
	}

	/**
	 * 日期转毫秒（加24小时,yyyy-MM-dd HH:mm:ss）
	 * 
	 * @param date
	 * @return
	 */
	public static long getMillisecond_24h(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String newDate = "";
		if (!"".equals(date)) {
			newDate = sdf.format(date);
		} else {
			newDate = sdf.format(DateUtil.getDate());
		}
		long millisecond = 24 * 3600 * 1000;
		try {
			millisecond += sdf.parse(newDate).getTime();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return millisecond;
	}

	/**
	 * 日期转毫秒（加N年）
	 * 
	 * @param date
	 * @return
	 */
	public static long getMillisecond_year(String date, Integer year) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String newDate = "";
		if ("".equals(date)) {
			newDate = sdf.format(DateUtil.getDate());
		} else {
			newDate = getDateTime(Long.parseLong(date));
		}
		Date dt = null;
		try {
			dt = sdf.parse(newDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		Calendar rightNow = Calendar.getInstance();
		rightNow.setTime(dt);
		rightNow.add(Calendar.YEAR, year);
		Date dt1 = rightNow.getTime();
		return dt1.getTime();
	}

	/**
	 * 日期转毫秒（加N天）
	 * 
	 * @param date
	 *            毫秒字符串
	 * @return
	 */
	public static Date getMillisecond_day(String date, Integer day) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String newDate = "";
		if ("".equals(date)) {
			newDate = sdf.format(DateUtil.getDate());
		} else {
			newDate = date;
		}
		Date dt = null;
		try {
			dt = sdf.parse(newDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		Calendar rightNow = Calendar.getInstance();
		rightNow.setTime(dt);
		rightNow.add(Calendar.DATE, day);
		Date dt1 = rightNow.getTime();
		Date dates = new Date(dt1.getTime());
		return dates;
	}

	/**
	 * 日期转毫秒（加N月）
	 * 
	 * @param date
	 * @return
	 */
	public static long getMillisecond_month(String date, Integer day) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String newDate = "";
		if ("".equals(date)) {
			newDate = sdf.format(DateUtil.getDate());
		} else {
			newDate = getDateTime(Long.parseLong(date));
		}
		Date dt = null;
		try {
			dt = sdf.parse(newDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		Calendar rightNow = Calendar.getInstance();
		rightNow.setTime(dt);
		rightNow.add(Calendar.MONTH, day);
		Date dt1 = rightNow.getTime();
		return dt1.getTime();
	}
	
	/**
	 * 日期转毫秒（加分钟）
	 * 
	 * @param date
	 * @return
	 */
	public static long getMillisecond_fz(String date, Integer day) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String newDate = "";
		if ("".equals(date)) {
			newDate = sdf.format(DateUtil.getDate());
		} else {
			newDate = getDateTime(Long.parseLong(date));
		}
		Date dt = null;
		try {
			dt = sdf.parse(newDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		Calendar rightNow = Calendar.getInstance();
		rightNow.setTime(dt);
		rightNow.add(Calendar.MINUTE, day);
		Date dt1 = rightNow.getTime();
		return dt1.getTime();
	}
	
	public static Date getMillisecond_fz1(String date, Integer day) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date dt = null;
		try {
			dt = sdf.parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		Calendar rightNow = Calendar.getInstance();
		rightNow.setTime(dt);
		rightNow.add(Calendar.MINUTE, day);
		Date dt1 = rightNow.getTime();
		return dt1;
	}

	/**
	 * 字符串日期转毫秒
	 * 
	 * @param date
	 * @return
	 */
	public static long getMillisecond_str(String date) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		if ("".equals(date)) {
			date = sdf.format(DateUtil.getDate());
		}
		long millisecond = 0;
		try {
			millisecond = sdf.parse(date).getTime();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return millisecond;
	}

	/**
	 * 字符串日期转毫秒
	 * 
	 * @param date
	 * @return
	 */
	public static long getMillisecond_strYmd(String date) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		if ("".equals(date)) {
			date = sdf.format(DateUtil.getDate());
		}
		long millisecond = 0;
		try {
			millisecond = sdf.parse(date).getTime();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return millisecond;
	}

	/**
	 * 字符串日期转Date
	 * 
	 * @param string
	 * @return date
	 * @throws ParseException
	 */
	public static Date getStrToDate(String dateString) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date date = sdf.parse(dateString);
		return date;
	}

	/**
	 * 字符串日期转Date
	 * 
	 * @param date
	 * @return
	 */
	public static Date getDate_str(String dateStr) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		sdf.setTimeZone(tz);
		if ("".equals(dateStr)) {
			dateStr = sdf.format(DateUtil.getDate());
		}
		Date date = null;
		try {
			date = sdf.parse(dateStr);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}

	/**
	 * 字符串日期转Date yyyy-MM-dd HH:mm
	 * 
	 * @param date
	 * @return
	 */
	public static Date getDate_str2(String dateStr) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		sdf.setTimeZone(tz);
		if ("".equals(dateStr)) {
			dateStr = sdf.format(DateUtil.getDate());
		}
		Date date = null;
		try {
			date = sdf.parse(dateStr);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}

	/**
	 * 字符串日期转Date yyyy-MM-dd HH:mm:ss
	 * 
	 * @param date
	 * @return
	 */
	public static Date getDate_str3(String dateStr) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		sdf.setTimeZone(tz);
		if ("".equals(dateStr)) {
			dateStr = sdf.format(DateUtil.getDate());
		}
		Date date = null;
		try {
			date = sdf.parse(dateStr);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}

	/**
	 * 字符串日期转Date
	 * 
	 * @param date
	 * @return
	 */
	public static Date getDate_strYMd(Long dateStr) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		if (dateStr == null) {
			dateStr = DateUtil.getCurMilli();
		}
		Date date = null;
		try {
			date = sdf.parse(sdf.format(new Date(dateStr)));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}

	/**
	 * 毫秒转Date
	 * 
	 * @param date
	 * @return
	 */
	public static Date getDate_strYMdHms(Long dateStr) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		if (dateStr == null) {
			dateStr = DateUtil.getCurMilli();
		}
		Date date = null;
		try {
			date = sdf.parse(sdf.format(new Date(dateStr)));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}

	/**
	 * 字符串日期转Date
	 * 
	 * @param date
	 * @return
	 */
	public static Date getDate_strYMdHm(Long dateStr) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		if (dateStr == null) {
			dateStr = DateUtil.getCurMilli();
		}
		Date date = null;
		try {
			date = sdf.parse(sdf.format(new Date(dateStr)));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}

	/**
	 * 字符串日期转毫秒
	 * 
	 * @param date
	 * @return
	 */
	public static long getMillisecond_strDmy(String date) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		if ("".equals(date)) {
			date = sdf.format(DateUtil.getDate());
		}
		long millisecond = 0;
		try {
			millisecond = sdf.parse(date).getTime();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return millisecond;
	}

	/**
	 * 字符串日期转毫秒转毫秒（加24小时）
	 * 
	 * @param date
	 * @return
	 */
	public static long getMillisecond_str_24h(String date) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		if ("".equals(date)) {
			date = sdf.format(DateUtil.getDate());
		}
		long millisecond = 24 * 3600 * 1000;
		try {
			millisecond += sdf.parse(date).getTime();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return millisecond;
	}

	/**
	 * 字符串日期转毫秒转毫秒（加24小时）
	 * 
	 * @param date
	 * @return
	 */
	public static long getMillisecond_strYmd_24h(String date) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		if ("".equals(date)) {
			date = sdf.format(DateUtil.getDate());
		}
		long millisecond = 24 * 3600 * 1000;
		try {
			millisecond += sdf.parse(date).getTime();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return millisecond;
	}

	/**
	 * 毫秒转日期
	 * 
	 * @param millisecond
	 * @return
	 */
	public static String getDate(long millisecond) {
		if (millisecond == 0) {
			millisecond = getCurMilli();
		}
		SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd");
		Calendar calendar = getCalendar();
		calendar.setTimeInMillis(millisecond);
		return dateformat.format(calendar.getTime());
	}

	/**
	 * 转换为指定格式的时间
	 * 
	 * @return Date
	 */
	public static Date getDate(String date, String pattern) {
		SimpleDateFormat format = new SimpleDateFormat(pattern);
		Date d = null;
		try {
			d = format.parse(date);
		} catch (ParseException ex) {
			return null;
		}
		return d;
	}

	/**
	 * 毫秒转日期
	 * 
	 * @param millisecond
	 * @return
	 */
	public static String getDate_HH(long millisecond) {
		if (millisecond == 0) {
			millisecond = getCurMilli();
		}
		SimpleDateFormat dateformat = new SimpleDateFormat("HH");
		Calendar calendar = getCalendar();
		calendar.setTimeInMillis(millisecond);
		return dateformat.format(calendar.getTime());
	}

	/**
	 * 毫秒转日期时间
	 * 
	 * @param millisecond
	 * @return
	 */
	public static String getDateTime(long millisecond) {
		if (millisecond == 0) {
			millisecond = getCurMilli();
		}
		SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Calendar calendar = getCalendar();
		calendar.setTimeInMillis(millisecond);
		return dateformat.format(calendar.getTime());
	}

	/**
	 * 毫秒转年月日
	 * 
	 * @param millisecond
	 * @return
	 */
	public static String getDateYMD(long millisecond) {
		if (millisecond == 0) {
			millisecond = getCurMilli();
		}
		SimpleDateFormat dateformat = new SimpleDateFormat("yyyy年MM月dd日");
		Calendar calendar = getCalendar();
		calendar.setTimeInMillis(millisecond);
		return dateformat.format(calendar.getTime());
	}

	/**
	 * 两日期相差毫秒
	 * 
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public static long getMinusMillisecond(Date startDate, Date endDate) {
		long startMillisecond = getMillisecond(startDate);
		long endMillisecond = getMillisecond(endDate);
		long minusMillisecond = endMillisecond - startMillisecond;
		if (minusMillisecond < 0) {
			minusMillisecond = 0;
		}
		return minusMillisecond;
	}

	/**
	 * 两日期相差天数
	 * 
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public static long getMinusDay(Date startDate, Date endDate) {
		long startMillisecond = getMillisecond(startDate);
		long endMillisecond = getMillisecond(endDate);
		long minusMillisecond = endMillisecond - startMillisecond;
		long day = 0;
		if (minusMillisecond < 0) {
			day = 0;
		} else {
			day = minusMillisecond / (24 * 3600 * 1000);
		}
		return day;
	}

	/**
	 * 前N天毫秒
	 * 
	 * @param day
	 * @return
	 */
	public static long getRetreatDay_millisecond(int day) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		long nowMillisecond = 0;
		try {
			nowMillisecond = sdf.parse(sdf.format(DateUtil.getDate())).getTime();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		nowMillisecond += 24 * 3600 * 1000;
		long retreatMillisecond = 24 * 3600 * 1000 * day;
		return nowMillisecond - retreatMillisecond;
	}

	/**
	 * 前N天时间
	 * 
	 * @param day
	 * @return
	 */
	public static String getRetreatDay_millisecond1(int day) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		long nowMillisecond = 0;
		try {
			nowMillisecond = sdf.parse(sdf.format(DateUtil.getDate())).getTime();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		nowMillisecond += 24 * 3600 * 1000;
		long retreatMillisecond = 24 * 3600 * 1000 * day;
		long s = nowMillisecond - retreatMillisecond;
		Date date = new Date(s);
		String res = sdf.format(date);
		return res;
	}

	// 当前时间前30天
	public static String getRetreatDay_millisecond2() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date today = new Date();
		String endDate = sdf.format(today);// 当前日期
		// 获取三十天前日期
		Calendar theCa = Calendar.getInstance();
		theCa.setTime(today);
		theCa.add(theCa.DATE, -30);// 最后一个数字30可改，30天的意思
		Date start = theCa.getTime();
		String startDate = sdf.format(start);// 三十天之前日期
		return startDate;
	}

	/**
	 * 日期转秒
	 * 
	 * @param date
	 * @return
	 */
	public static long getDecond(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String newDate = "";
		if (!"".equals(date)) {
			newDate = sdf.format(date);
		} else {
			newDate = sdf.format(DateUtil.getDate());
		}
		long second = 0;
		try {
			second = sdf.parse(newDate).getTime() / 1000;
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return second;
	}

	/**
	 * 日期转String
	 * 
	 * @param date
	 * @return
	 * @throws ParseException
	 */
	public static String getDateToString(Date date) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String newDate = sdf.format(date);
		long millisecond = sdf.parse(newDate).getTime();
		Calendar calendar = getCalendar();
		calendar.setTimeInMillis(millisecond);
		return sdf.format(calendar.getTime());
	}

	/**
	 * 毫秒转星期XX
	 * 
	 * @param millisecond
	 * @return
	 */
	public static int getDate_week(Long millisecond) {
		if (millisecond == null) {
			millisecond = getCurMilli();
		}
		Calendar cal = getCalendar();
		cal.setTimeInMillis(millisecond);
		return cal.get(Calendar.DAY_OF_WEEK) - 1;

	}

	/**
	 * 获取当前系统时间已yyyy-MM-dd HH:mm:ss格式化的字符串
	 */
	public static String nowStr() {

		SimpleDateFormat dateFormat = new SimpleDateFormat("yy-MM-dd HH:mm:ss");
		return dateFormat.format(getDate());
	}

	/**
	 * 
	 * 获取之前几天日期
	 * 
	 * @param pattern
	 *            yyyy-MM-dd（默认）
	 * @param few
	 *            之前几天
	 */
	public static String beforeFewDayStr(String pattern, Integer few) {

		if (pattern == null || "".equals(pattern)) {
			pattern = "yyyy-MM-dd";
		}
		Calendar c = getCalendar();
		c.add(Calendar.DATE, -few);
		return new SimpleDateFormat(pattern).format(c.getTime());
	}
	
	
	/**
	 * 得到几天后的时间
	 *
	 * @param d
	 * @param day
	 * @return
	 */
	public static Date getDateAfter(Date d, int day) {
	    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
	    Calendar now = Calendar.getInstance();
	    now.setTime(d);
	    now.set(Calendar.DATE, now.get(Calendar.DATE) - day);//+后 -前
	    return now.getTime();
	}
	

	/**
	 * 获取今天日期
	 * 
	 * @param pattern
	 *            yyyy-MM-dd（默认）
	 */
	public static String todayStr(String pattern) {

		if (pattern == null || "".equals(pattern)) {
			pattern = "yyyy-MM-dd";
		}
		return new SimpleDateFormat(pattern).format(getDate());
	}

	/**
	 * 获取当前系统时间戳字符串
	 */
	public static String nowDateLongStr() {

		return getDate().getTime() + "";
	}

	/**
	 * 获取当前系统时间
	 * 
	 * @return
	 */
	public static Date now() {

		return getDate();
	}

	/*public static void main(String[] args) throws ParseException {
		// 打印测试日期包含
		Date time1 = getDate_str("2017-3-11");
		Date time2 = getDate_str("2017-3-15");
		Date time3 = getDate_str("2017-3-17");
		Date time4 = getDate_str("2017-3-12");
		Date time5 = getDate_str("2017-3-16");
		Date from = getDate_str("2017-3-12");
		Date to = getDate_str("2017-3-16");
		System.out.println(belongCalendar(time1, from, to));
		System.out.println(belongCalendar(time2, from, to));
		System.out.println(belongCalendar(time3, from, to));
		System.out.println(belongCalendar(time4, from, to));
		System.out.println(belongCalendar(time5, from, to));
		System.out.println(nowStr());

	}
*/
	/**
	 * 把日期往后增加一天. 正数往后推,负数往前移动
	 * 
	 * @param day
	 * @return
	 */
	public static String getString(int day) {
		Date date = new Date();// 取时间
		Calendar calendar = new GregorianCalendar();
		calendar.setTime(date);
		calendar.add(calendar.DATE, day);// 把日期往后增加一天.整数往后推,负数往前移动
		date = calendar.getTime(); // 这个时间就是日期往后推一天的结果
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String dateString = formatter.format(date);
		return dateString;
	}

	/**
	 * 根据当前日期获得所在周的日期区间（周一和周日日期）
	 * 
	 * @return
	 * @author zhaoxuepu
	 * @throws ParseException
	 */
	public static String getTimeInterval(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		// 判断要计算的日期是否是周日，如果是则减一天计算周六的，否则会出问题，计算到下一周去了
		int dayWeek = cal.get(Calendar.DAY_OF_WEEK);// 获得当前日期是一个星期的第几天
		if (1 == dayWeek) {
			cal.add(Calendar.DAY_OF_MONTH, -1);
		}
		// System.out.println("要计算日期为:" + sdf.format(cal.getTime())); // 输出要计算日期
		// 设置一个星期的第一天，按中国的习惯一个星期的第一天是星期一
		cal.setFirstDayOfWeek(Calendar.MONDAY);
		// 获得当前日期是一个星期的第几天
		int day = cal.get(Calendar.DAY_OF_WEEK);
		// 根据日历的规则，给当前日期减去星期几与一个星期第一天的差值
		cal.add(Calendar.DATE, cal.getFirstDayOfWeek() - day);
		String imptimeBegin = sdf.format(cal.getTime());
		// System.out.println("所在周星期一的日期：" + imptimeBegin);
		cal.add(Calendar.DATE, 6);
		String imptimeEnd = sdf.format(cal.getTime());
		// System.out.println("所在周星期日的日期：" + imptimeEnd);
		return imptimeBegin + "," + imptimeEnd;
	}

	/**
	 * 根据当前日期获得上周的日期区间（上周周一和周日日期）
	 * 
	 * @return
	 * @author zhaoxuepu
	 */
	public static String getLastTimeInterval() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Calendar calendar1 = Calendar.getInstance();
		Calendar calendar2 = Calendar.getInstance();
		int dayOfWeek = calendar1.get(Calendar.DAY_OF_WEEK) - 1;
		int offset1 = 1 - dayOfWeek;
		int offset2 = 7 - dayOfWeek;
		calendar1.add(Calendar.DATE, offset1 - 7);
		calendar2.add(Calendar.DATE, offset2 - 7);
		// System.out.println(sdf.format(calendar1.getTime()));// last Monday
		String lastBeginDate = sdf.format(calendar1.getTime());
		// System.out.println(sdf.format(calendar2.getTime()));// last Sunday
		String lastEndDate = sdf.format(calendar2.getTime());
		return lastBeginDate + "," + lastEndDate;
	}

	public static String DateYUE() {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Calendar c = Calendar.getInstance();
		c.add(Calendar.MONTH, 0);
		c.set(Calendar.DAY_OF_MONTH, 1);// 设置为1号,当前日期既为本月第一天
		String first = format.format(c.getTime());

		// 获取当前月最后一天
		Calendar ca = Calendar.getInstance();
		ca.set(Calendar.DAY_OF_MONTH, ca.getActualMaximum(Calendar.DAY_OF_MONTH));
		String last = format.format(ca.getTime());
		return first + "," + last;
	}

	public static String getBeforeFirstMonthdate() {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Calendar calendar = Calendar.getInstance();
		Calendar calendar1 = Calendar.getInstance();
		calendar.add(Calendar.MONTH, -1);
		calendar.set(Calendar.DAY_OF_MONTH, 1);

		int month = calendar1.get(Calendar.MONTH);
		calendar1.set(Calendar.MONTH, month - 1);
		calendar1.set(Calendar.DAY_OF_MONTH, calendar1.getActualMaximum(Calendar.DAY_OF_MONTH));
		String str = format.format(calendar.getTime());
		String str1 = format.format(calendar1.getTime());

		return str + "," + str1;
	}

	/**
	 * 获取某年第一天和最后一天日期
	 * 
	 * @param year
	 *            年份
	 * @return Date
	 */

	public static String getYearFirst(int year) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Calendar calendar = Calendar.getInstance();
		calendar.clear();
		calendar.set(Calendar.YEAR, year);

		Calendar calendar1 = Calendar.getInstance();
		calendar1.clear();
		calendar1.set(Calendar.YEAR, year);
		calendar1.roll(Calendar.DAY_OF_YEAR, -1);

		String str = format.format(calendar.getTime());
		String str1 = format.format(calendar1.getTime());

		return str + "," + str1;
	}
	
	public static Date addDate(Date date,long day) throws ParseException {
		 long time = date.getTime(); // 得到指定日期的毫秒数
		 day = day*24*60*60*1000; // 要加上的天数转换成毫秒数
		 time+=day; // 相加得到新的毫秒数
		 return new Date(time); // 将毫秒数转换成日期
		}
	
	// 获取一天开始和结束
	public static String getDay() {
		Date dBefore = new Date();
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd"); //设置时间格式
		String defaultStartDate = sdf.format(dBefore); //格式化前一天
		defaultStartDate = defaultStartDate+" 00:00:00";
		String defaultEndDate = defaultStartDate.substring(0,10)+" 23:59:59";

		return defaultStartDate + "," + defaultEndDate;
	}
	
	
	
	public static void main(String[] args){
		Double a=5.00;
		Double b=(double) (a/1000.00);
		System.out.println(b);
	}

}
