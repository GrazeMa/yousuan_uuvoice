package com.stylefeng.guns.rest.modular.system.service;

import com.stylefeng.guns.rest.modular.system.model.Room;
import com.stylefeng.guns.rest.modular.system.model.Room;

import java.util.List;

import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 房间信息 服务类
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
public interface IRoomService extends IService<Room> {
public List<Room> getList(Room model);
    
    Room getOne(Room model);
}
