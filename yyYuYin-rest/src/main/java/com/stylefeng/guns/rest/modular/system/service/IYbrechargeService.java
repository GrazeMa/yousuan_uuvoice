package com.stylefeng.guns.rest.modular.system.service;

import com.stylefeng.guns.rest.modular.system.model.Ybrecharge;
import com.stylefeng.guns.rest.modular.system.model.Ybrecharge;

import java.util.List;

import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 优币充值记录 服务类
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
public interface IYbrechargeService extends IService<Ybrecharge> {
public List<Ybrecharge> getList(Ybrecharge model);
    
    Ybrecharge getOne(Ybrecharge model);
}
