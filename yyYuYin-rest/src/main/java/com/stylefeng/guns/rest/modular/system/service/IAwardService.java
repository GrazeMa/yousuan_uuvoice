package com.stylefeng.guns.rest.modular.system.service;

import com.stylefeng.guns.rest.modular.system.model.Award;
import com.stylefeng.guns.rest.modular.system.model.Award;

import java.util.List;

import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 优钻兑换优币奖励 服务类
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
public interface IAwardService extends IService<Award> {
	public List<Award> getList(Award model);
    
    Award getOne(Award model);
}
