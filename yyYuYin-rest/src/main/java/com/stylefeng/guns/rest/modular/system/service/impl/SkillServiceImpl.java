package com.stylefeng.guns.rest.modular.system.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.rest.modular.system.dao.SkillMapper;
import com.stylefeng.guns.rest.modular.system.model.Skill;
import com.stylefeng.guns.rest.modular.system.service.ISkillService;

/**
 * @Description 技能实现类。
 * @author Grazer_Ma
 * @Date 2020-06-05 10:05:28
 */
@Service
public class SkillServiceImpl extends ServiceImpl<SkillMapper, Skill> implements ISkillService {

	@Override
	public List<Skill> getList(Skill skillModel) {
		return this.baseMapper.getList(skillModel);
	}

	@Override
	public Skill getOne(Skill skillModel) {
		return this.baseMapper.getOne(skillModel);
	}

}
