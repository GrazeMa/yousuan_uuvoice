package com.stylefeng.guns.rest.modular.system.service.impl;

import com.stylefeng.guns.rest.modular.system.model.Divide;
import com.stylefeng.guns.rest.modular.system.dao.DivideMapper;
import com.stylefeng.guns.rest.modular.system.service.IDivideService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import java.util.List;

import org.springframework.stereotype.Service;

/**
 * <p>
 * 充值分成金额 服务实现类
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
@Service
public class DivideServiceImpl extends ServiceImpl<DivideMapper, Divide> implements IDivideService {

	@Override
	public List<Divide> getList(Divide model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getList(model);
	}

	@Override
	public Divide getOne(Divide model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getOne(model);
	}

	@Override
	public Double getSum(Divide model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getSum(model);
	}

}
