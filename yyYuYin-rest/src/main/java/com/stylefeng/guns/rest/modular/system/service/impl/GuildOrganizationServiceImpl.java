package com.stylefeng.guns.rest.modular.system.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.rest.modular.system.dao.GuildOrganizationMapper;
import com.stylefeng.guns.rest.modular.system.model.GuildOrganization;
import com.stylefeng.guns.rest.modular.system.service.IGuildOrganizationService;

/**
 * @Description 公会服务实现类。
 * @author Grazer_Ma
 * @Date 2020-05-30 09:21:56
 */
@Service
public class GuildOrganizationServiceImpl extends ServiceImpl<GuildOrganizationMapper, GuildOrganization>
		implements IGuildOrganizationService {

	@Override
	public List<GuildOrganization> getList(GuildOrganization model) {
		return this.baseMapper.getList(model);
	}

	@Override
	public GuildOrganization getOne(GuildOrganization model) {
		return this.baseMapper.getOne(model);
	}

}
