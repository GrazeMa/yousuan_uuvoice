package com.stylefeng.guns.rest.modular.system.dao;

import com.stylefeng.guns.rest.modular.system.model.IosVersions;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * ios版本控制 Mapper 接口
 * </p>
 *
 * @author wumeng123
 * @since 2019-05-17
 */
public interface IosVersionsMapper extends BaseMapper<IosVersions> {

	List<IosVersions> getList(IosVersions model);

	IosVersions getOne(IosVersions model);

}
