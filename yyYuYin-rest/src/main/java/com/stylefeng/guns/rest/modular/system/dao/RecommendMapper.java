package com.stylefeng.guns.rest.modular.system.dao;

import com.stylefeng.guns.rest.modular.system.model.Recommend;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 推荐位置管理 Mapper 接口
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
public interface RecommendMapper extends BaseMapper<Recommend> {

	List<Recommend> getList(Recommend model);

	Recommend getOne(Recommend model);

}
