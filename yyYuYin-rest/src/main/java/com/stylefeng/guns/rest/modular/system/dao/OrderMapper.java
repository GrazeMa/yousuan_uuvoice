package com.stylefeng.guns.rest.modular.system.dao;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.stylefeng.guns.rest.modular.system.model.Order;

/**
 * @Description 订单 Mapper 接口。
 * @author Grazer_Ma
 * @Date 2020-06-04 21:57:42
 */
public interface OrderMapper extends BaseMapper<Order> {

	List<Order> getList(Order orderModel);

	Order getOne(Order orderModel);

}
