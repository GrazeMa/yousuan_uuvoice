package com.stylefeng.guns.rest.modular.system.dao;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.stylefeng.guns.rest.modular.system.model.Favorite;
import com.stylefeng.guns.rest.modular.system.model.Friend;

/**
 * @Description 收藏 Mapper 接口。
 * @author Grazer_Ma
 * @Date 2020-05-25 12:04:38
 */
public interface FavoriteMapper extends BaseMapper<Favorite> {

	List<Favorite> getList(Favorite favoriteModel);
	
	List<Favorite> getFavoriteRoomsList(Favorite favoriteModel);
	
	Favorite getOne(Favorite favoriteModel);

}
