package com.stylefeng.guns.rest.modular.system.tencent.service;

import com.stylefeng.guns.rest.modular.system.tencent.common.Configure;
import com.stylefeng.guns.rest.modular.system.tencent.protocol.UnifiedorderReqData;

/**
 * 统一下单服务
 * @author pen
 */
public class UnifiedorderService extends BaseService{

    public UnifiedorderService(Integer apptype) throws IllegalAccessException, InstantiationException, ClassNotFoundException {
        super(apptype,Configure.UNIFIEDORDER_API);
    }

    /**
     * 统一下单服务
     * @param UnifiedorderReqData 这个数据对象里面包含了API要求提交的各种数据字段
     */
    public String request(UnifiedorderReqData unifiedorderReqData) throws Exception {

        //--------------------------------------------------------------------
        //发送HTTPS的Post请求到API地址
        //--------------------------------------------------------------------
        String responseString = sendPost(unifiedorderReqData);

        return responseString;
    }
}
