package com.stylefeng.guns.rest.modular.system.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;
import java.util.Date;

/**
 * @Description 技能 Entity。
 * @author Grazer_Ma
 * @Date 2020-06-04 15:26:28
 */
@TableName("app_order_skill")
public class OrderSkill extends Model<OrderSkill> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id; // 技能主键 ID。
    private Date createTime; // 技能创建时间。
    private Date updateTime; // 技能修改时间。

    private Integer uid; // 用户主键 ID。
    private Integer skillId; // 技能主键 ID。
    private Integer skillPriceId; // 技能价格主键 ID。
    private Integer skillLevelId; // 技能段位主键 ID。

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    
    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public Integer getSkillId() {
        return skillId;
    }

    public void setSkillId(Integer skillId) {
        this.skillId = skillId;
    }

    public Integer getSkillPriceId() {
        return skillPriceId;
    }

    public void setSkillPriceId(Integer skillPriceId) {
        this.skillPriceId = skillPriceId;
    }

    public Integer getSkillLevelId() {
        return skillLevelId;
    }

    public void setSkillLevelId(Integer skillLevelId) {
        this.skillLevelId = skillLevelId;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "OrderSkill{" +
        "id=" + id +
        ", createTime=" + createTime +
        ", updateTime=" + updateTime +
        ", uid=" + uid +
        ", skillId=" + skillId +
        ", skillPriceId=" + skillPriceId +
        ", skillLevelId=" + skillLevelId +
        "}";
    }

}
