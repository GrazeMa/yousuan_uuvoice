package com.stylefeng.guns.rest.modular.system.controller.model;

import java.util.Date;

public class InviteModel {

	private Date createTime;
	private String name;
	private Double money;

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getMoney() {
		return money;
	}

	public void setMoney(Double money) {
		this.money = money;
	}

}
