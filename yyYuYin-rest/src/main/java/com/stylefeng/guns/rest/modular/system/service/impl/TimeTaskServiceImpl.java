package com.stylefeng.guns.rest.modular.system.service.impl;

import com.stylefeng.guns.rest.modular.system.model.TimeTask;
import com.stylefeng.guns.rest.modular.system.dao.TimeTaskMapper;
import com.stylefeng.guns.rest.modular.system.service.ITimeTaskService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 定时任务数据 服务实现类
 * </p>
 *
 * @author lyouyou123
 * @since 2018-11-30
 */
@Service
public class TimeTaskServiceImpl extends ServiceImpl<TimeTaskMapper, TimeTask> implements ITimeTaskService {

}
