package com.stylefeng.guns.rest.modular.system.dao;

import com.stylefeng.guns.rest.modular.system.model.Ybrecharge;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 优币充值记录 Mapper 接口
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
public interface YbrechargeMapper extends BaseMapper<Ybrecharge> {

	List<Ybrecharge> getList(Ybrecharge model);

	Ybrecharge getOne(Ybrecharge model);

}
