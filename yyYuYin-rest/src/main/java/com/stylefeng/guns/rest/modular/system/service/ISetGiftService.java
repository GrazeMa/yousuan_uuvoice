package com.stylefeng.guns.rest.modular.system.service;

import com.stylefeng.guns.rest.modular.system.model.SetGift;
import com.stylefeng.guns.rest.modular.system.model.SetGift;

import java.util.List;

import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 礼物设置 服务类
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
public interface ISetGiftService extends IService<SetGift> {
public List<SetGift> getList(SetGift model);
    
    SetGift getOne(SetGift model);
}
