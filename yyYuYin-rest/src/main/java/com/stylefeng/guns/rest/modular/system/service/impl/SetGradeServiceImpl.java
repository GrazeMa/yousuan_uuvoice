package com.stylefeng.guns.rest.modular.system.service.impl;

import com.stylefeng.guns.rest.modular.system.model.SetGrade;
import com.stylefeng.guns.rest.modular.system.dao.SetGradeMapper;
import com.stylefeng.guns.rest.modular.system.service.ISetGradeService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import java.util.List;

import org.springframework.stereotype.Service;

/**
 * <p>
 * 等级设置 服务实现类
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
@Service
public class SetGradeServiceImpl extends ServiceImpl<SetGradeMapper, SetGrade> implements ISetGradeService {

	@Override
	public List<SetGrade> getList(SetGrade model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getList(model);
	}

	@Override
	public SetGrade getOne(SetGrade model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getOne(model);
	}

	@Override
	public SetGrade getJie(SetGrade model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getJie(model);
	}

}
