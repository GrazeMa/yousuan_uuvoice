package com.stylefeng.guns.rest.modular.system.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 优币兑换记录
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
@TableName("app_ybexchange")
public class Ybexchange extends Model<Ybexchange> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    private Date createTime;
    private Integer uid;
    private String name;
    /**
     * 兑换钻石
     */
    private Integer eid;
    /**
     * 兑换钻石
     */
    private Integer dhDiamond;
    /**
     * 获得金币
     */
    private Integer hdGold;
    /**
     * 其他金币
     */
    private Integer qtGold;
    /**
     * '是否被删除（1=否  2=是）'
     */
    private Integer isDelete;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getEid() {
        return eid;
    }

    public void setEid(Integer eid) {
        this.eid = eid;
    }

    public Integer getDhDiamond() {
        return dhDiamond;
    }

    public void setDhDiamond(Integer dhDiamond) {
        this.dhDiamond = dhDiamond;
    }

    public Integer getHdGold() {
        return hdGold;
    }

    public void setHdGold(Integer hdGold) {
        this.hdGold = hdGold;
    }

    

    public Integer getQtGold() {
		return qtGold;
	}

	public void setQtGold(Integer qtGold) {
		this.qtGold = qtGold;
	}

	public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "Ybexchange{" +
        "id=" + id +
        ", createTime=" + createTime +
        ", uid=" + uid +
        ", name=" + name +
        ", eid=" + eid +
        ", dhDiamond=" + dhDiamond +
        ", hdGold=" + hdGold +
        ", qtGold=" + qtGold +
        ", isDelete=" + isDelete +
        "}";
    }
}
