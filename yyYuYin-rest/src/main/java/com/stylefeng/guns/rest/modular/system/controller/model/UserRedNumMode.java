package com.stylefeng.guns.rest.modular.system.controller.model;

import java.util.Date;

public class UserRedNumMode {
	private Integer id;
	/**
	 * 房间id
	 */
	private Date createTime;
	/**
	 * 金币
	 */
	private Integer gold;
	/**
	 * 被送用户id
	 */
	private Integer uid;
	/**
	 * 是否被定时器 1否，2是
	 */
	private Integer state;
	/**
	 * 红包的id
	 */
	private Integer redId;
	/**
	 * 是否被领取红包 1 未，2 是
	 */
	private Integer status;

	private String rid;

	private Date lqTime;

	private String name;

	private String img;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Integer getGold() {
		return gold;
	}

	public void setGold(Integer gold) {
		this.gold = gold;
	}

	public Integer getUid() {
		return uid;
	}

	public void setUid(Integer uid) {
		this.uid = uid;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public Integer getRedId() {
		return redId;
	}

	public void setRedId(Integer redId) {
		this.redId = redId;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getRid() {
		return rid;
	}

	public void setRid(String rid) {
		this.rid = rid;
	}

	public Date getLqTime() {
		return lqTime;
	}

	public void setLqTime(Date lqTime) {
		this.lqTime = lqTime;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

}
