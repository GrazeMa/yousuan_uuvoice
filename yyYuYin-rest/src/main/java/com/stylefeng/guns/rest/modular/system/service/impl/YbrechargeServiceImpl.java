package com.stylefeng.guns.rest.modular.system.service.impl;

import com.stylefeng.guns.rest.modular.system.model.Ybrecharge;
import com.stylefeng.guns.rest.modular.system.dao.YbrechargeMapper;
import com.stylefeng.guns.rest.modular.system.service.IYbrechargeService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import java.util.List;

import org.springframework.stereotype.Service;

/**
 * <p>
 * 优币充值记录 服务实现类
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
@Service
public class YbrechargeServiceImpl extends ServiceImpl<YbrechargeMapper, Ybrecharge> implements IYbrechargeService {

	@Override
	public List<Ybrecharge> getList(Ybrecharge model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getList(model);
	}

	@Override
	public Ybrecharge getOne(Ybrecharge model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getOne(model);
	}

}
