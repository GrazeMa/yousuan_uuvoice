package com.stylefeng.guns.rest.modular.system.service;

import com.stylefeng.guns.rest.modular.system.model.Ybexchange;
import com.stylefeng.guns.rest.modular.system.model.Ybexchange;

import java.util.List;

import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 优币兑换记录 服务类
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
public interface IYbexchangeService extends IService<Ybexchange> {
public List<Ybexchange> getList(Ybexchange model);
    
    Ybexchange getOne(Ybexchange model);
}
