package com.stylefeng.guns.rest.modular.system.dao;

import com.stylefeng.guns.rest.modular.system.model.Sign;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 签到设置 Mapper 接口
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-16
 */
public interface SignMapper extends BaseMapper<Sign> {

	List<Sign> getList(Sign model);

	Sign getOne(Sign model);

}
