package com.stylefeng.guns.rest.modular.system.service.impl;

import com.stylefeng.guns.rest.modular.system.model.Seat;
import com.stylefeng.guns.rest.modular.system.dao.SeatMapper;
import com.stylefeng.guns.rest.modular.system.service.ISeatService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import java.util.List;

import org.springframework.stereotype.Service;

/**
 * <p>
 * 房间座位相关设置 服务实现类
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
@Service
public class SeatServiceImpl extends ServiceImpl<SeatMapper, Seat> implements ISeatService {

	@Override
	public List<Seat> getList(Seat seat) {
		// TODO Auto-generated method stub
		return this.baseMapper.getList(seat);
	}

	@Override
	public Seat getOne(Seat seat) {
		// TODO Auto-generated method stub
		return this.baseMapper.getOne(seat);
	}

}
