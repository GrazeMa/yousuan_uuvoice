package com.stylefeng.guns.rest.modular.system.dao;

import com.stylefeng.guns.rest.modular.system.model.UserBag;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 用户背包信息 Mapper 接口
 * </p>
 *
 * @author lyouyou123
 * @since 2018-10-15
 */
public interface UserBagMapper extends BaseMapper<UserBag> {

	List<UserBag> getByUid(Integer uid);
	UserBag getOne(UserBag model);
}
