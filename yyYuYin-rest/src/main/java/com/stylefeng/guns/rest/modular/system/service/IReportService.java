package com.stylefeng.guns.rest.modular.system.service;

import com.stylefeng.guns.rest.modular.system.model.Report;
import com.stylefeng.guns.rest.modular.system.model.Report;

import java.util.List;

import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 举报 服务类
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
public interface IReportService extends IService<Report> {
public List<Report> getList(Report model);
    
    Report getOne(Report model);
}
