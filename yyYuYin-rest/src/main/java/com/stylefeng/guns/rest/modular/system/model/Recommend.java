package com.stylefeng.guns.rest.modular.system.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 推荐位置管理
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
@TableName("app_recommend")
public class Recommend extends Model<Recommend> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 房主id
     */
    private Integer uid;
    private Date sTime;
    private Date eTime;
    private Date createTime;
    
    private Integer isDefault;
    
   private Integer state;
   
   /**
    * 时间查询条件(开始)
    */
   @TableField(exist = false)
   private String beginTime;
   /**
    * 时间查询条件(结束)
    */
   @TableField(exist = false)
   private String endTime;
   
   



	public String getBeginTime() {
	return beginTime;
}

public void setBeginTime(String beginTime) {
	this.beginTime = beginTime;
}

public String getEndTime() {
	return endTime;
}

public void setEndTime(String endTime) {
	this.endTime = endTime;
}

	public Integer getState() {
	return state;
}

public void setState(Integer state) {
	this.state = state;
}

	public Integer getIsDefault() {
		return isDefault;
	}

	public void setIsDefault(Integer isDefault) {
		this.isDefault = isDefault;
	}

	public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public Date getsTime() {
        return sTime;
    }

    public void setsTime(Date sTime) {
        this.sTime = sTime;
    }

    public Date geteTime() {
        return eTime;
    }

    public void seteTime(Date eTime) {
        this.eTime = eTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "Recommend{" +
        "id=" + id +
        ", uid=" + uid +
        ", sTime=" + sTime +
        ", eTime=" + eTime +
        ", createTime=" + createTime +
        "}";
    }
}
