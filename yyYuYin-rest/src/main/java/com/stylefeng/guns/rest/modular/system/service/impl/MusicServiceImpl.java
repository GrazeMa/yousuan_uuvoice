package com.stylefeng.guns.rest.modular.system.service.impl;

import com.stylefeng.guns.rest.modular.system.model.Music;
import com.stylefeng.guns.rest.modular.system.dao.MusicMapper;
import com.stylefeng.guns.rest.modular.system.service.IMusicService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import java.util.List;

import org.springframework.stereotype.Service;

/**
 * <p>
 * 音乐审核 服务实现类
 * </p>
 *
 * @author wumeng123
 * @since 2019-03-05
 */
@Service
public class MusicServiceImpl extends ServiceImpl<MusicMapper, Music> implements IMusicService {

	@Override
	public List<Music> getList(Music model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getList(model);
	}

	@Override
	public Music getOne(Music model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getOne(model);
	}

	@Override
	public Integer getConut(Music Music) {
		// TODO Auto-generated method stub
		return this.baseMapper.getConut(Music);
	}

}
