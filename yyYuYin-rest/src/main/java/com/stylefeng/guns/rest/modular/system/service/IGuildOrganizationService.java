package com.stylefeng.guns.rest.modular.system.service;

import java.util.List;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.rest.modular.system.model.GuildOrganization;

/**
 * @Description 公会服务类。
 * @author Grazer_Ma
 * @Date 2020-05-30 09:19:36
 */
public interface IGuildOrganizationService extends IService<GuildOrganization> {
	
	public List<GuildOrganization> getList(GuildOrganization model);

	GuildOrganization getOne(GuildOrganization model);

}
