package com.stylefeng.guns.rest.modular.system.dao;

import com.stylefeng.guns.rest.modular.system.model.RoomUser;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 流水 Mapper 接口
 * </p>
 *
 * @author wumeng123
 * @since 2019-07-04
 */
public interface RoomUserMapper extends BaseMapper<RoomUser> {

	Integer getSum(RoomUser roomUser);

}
