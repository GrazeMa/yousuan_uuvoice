package com.stylefeng.guns.rest.modular.system.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;
import java.util.Date;

/**
 * @Description 订单 Entity。
 * @author Grazer_Ma
 * @Date 2020-06-04 15:06:49
 */
@TableName("app_order")
public class Order extends Model<Order> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id; // 订单主键 ID。
    private Date createTime; // 订单创建时间。
    private Date updateTime; // 订单修改时间。

    private Integer status; // 订单状态。
    private String orderNumber; // 订单编号。
    private Integer orderUserId; // 下单用户主键 ID。
    private Integer orderSkillId; // 技能主键 ID。
    private Date serviceBeginTime; // 服务开始时间。
    private Date serviceEndTime; // 服务结束时间。
    private Integer servedUserId; // 接单用户主键 ID。
    private Integer type; // 订单支付类型（1：微信支付；2：支付宝支付；3：U币支付。）。
    private Integer amount; // 支付金额（单位：元（人民币））。
    private Integer state; // 订单支付状态。
    private Integer orderCount; // 订单数量。
    private Integer cancelReason; // 取消原因（0：默认；1：陪陪拒绝；2：支付超时。）。
    private Double fee; // 手续费。
    
    @TableField(exist = false)
    private String username;
    @TableField(exist = false)
    private String userAvatarUrl;
    @TableField(exist = false)
    private String skillName;
    @TableField(exist = false)
    private Integer remainingTime; // 剩余支付时间。
    @TableField(exist = false)
    private String skillLevel;
    private Integer payLogId;
    @TableField(exist = false)
    private String payOrderNumber;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    
    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public Integer getOrderUserId() {
        return orderUserId;
    }

    public void setOrderUserId(Integer orderUserId) {
        this.orderUserId = orderUserId;
    }

    public Integer getOrderSkillId() {
        return orderSkillId;
    }

    public void setOrderSkillId(Integer orderSkillId) {
        this.orderSkillId = orderSkillId;
    }

    public Date getServiceBeginTime() {
        return serviceBeginTime;
    }

    public void setServiceBeginTime(Date serviceBeginTime) {
        this.serviceBeginTime = serviceBeginTime;
    }

    public Date getServiceEndTime() {
        return serviceEndTime;
    }

    public void setServiceEndTime(Date serviceEndTime) {
        this.serviceEndTime = serviceEndTime;
    }
    
    public Integer getServedUserId() {
        return servedUserId;
    }

    public void setServedUserId(Integer servedUserId) {
        this.servedUserId = servedUserId;
    }
    
    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }
    
    public Integer getOrderCount() {
        return orderCount;
    }

    public void setOrderCount(Integer orderCount) {
        this.orderCount = orderCount;
    }
    
    public Integer getCancelReason() {
        return cancelReason;
    }

    public void setCancelReason(Integer cancelReason) {
        this.cancelReason = cancelReason;
    }
    
    public Double getFee() {
        return fee;
    }

    public void setFee(Double fee) {
        this.fee = fee;
    }
    
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
    
    public String getUserAvatarUrl() {
        return userAvatarUrl;
    }

    public void setUserAvatarUrl(String userAvatarUrl) {
        this.userAvatarUrl = userAvatarUrl;
    }
    
    public String getSkillName() {
        return skillName;
    }

    public void setSkillName(String skillName) {
        this.skillName = skillName;
    }
    
    public Integer getRemainingTime() {
        return remainingTime;
    }

    public void setRemainingTime(Integer remainingTime) {
        this.remainingTime = remainingTime;
    }
    
    public String getSkillLevel() {
        return skillLevel;
    }

    public void setSkillLevel(String skillLevel) {
        this.skillLevel = skillLevel;
    }
    
    public Integer getPayLogId() {
        return payLogId;
    }

    public void setPayLogId(Integer payLogId) {
        this.payLogId = payLogId;
    }
    
    public String getPayOrderNumber() {
        return payOrderNumber;
    }

    public void setPayOrderNumber(String payOrderNumber) {
        this.payOrderNumber = payOrderNumber;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "Order{" +
        "id=" + id +
        ", createTime=" + createTime +
        ", updateTime=" + updateTime +
        ", status=" + status +
        ", orderNumber=" + orderNumber +
        ", orderUserId=" + orderUserId +
        ", orderSkillId=" + orderSkillId +
        ", serviceBeginTime=" + serviceBeginTime +
        ", serviceEndTime=" + serviceEndTime +
        ", servedUserId=" + servedUserId +
        ", type=" + type +
        ", amount=" + amount +
        ", state=" + state +
        ", orderCount=" + orderCount +
        ", cancelReason=" + cancelReason +
        ", fee=" + fee +
        ", username=" + username +
        ", userAvatarUrl=" + userAvatarUrl +
        ", skillName=" + skillName +
        ", remainingTime=" + remainingTime +
        ", skillLevel=" + skillLevel +
        ", payLogId=" + payLogId +
        ", payOrderNumber=" + payOrderNumber +
        "}";
    }
    
}
