package com.stylefeng.guns.rest.modular.system.dao;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.stylefeng.guns.rest.modular.system.model.OrderSkill;

/**
 * @Description 技能 Mapper 接口。
 * @author Grazer_Ma
 * @Date 2020-06-04 22:01:22
 */
public interface OrderSkillMapper extends BaseMapper<OrderSkill> {

	List<OrderSkill> getList(OrderSkill orderSkillModel);

	OrderSkill getOne(OrderSkill orderSkillModel);
	
	List<OrderSkill> getServedUserList();

}
