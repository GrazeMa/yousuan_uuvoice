package com.stylefeng.guns.rest.modular.system.service.impl;

import com.stylefeng.guns.rest.modular.system.model.SetWithdraw;
import com.stylefeng.guns.rest.modular.system.dao.SetWithdrawMapper;
import com.stylefeng.guns.rest.modular.system.service.ISetWithdrawService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import java.util.List;

import org.springframework.stereotype.Service;

/**
 * <p>
 * 提现设置 服务实现类
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
@Service
public class SetWithdrawServiceImpl extends ServiceImpl<SetWithdrawMapper, SetWithdraw> implements ISetWithdrawService {

	@Override
	public List<SetWithdraw> getList(SetWithdraw model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getList(model);
	}

	@Override
	public SetWithdraw getOne(SetWithdraw model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getOne(model);
	}

}
