package com.stylefeng.guns.rest.modular.system.dao;

import com.stylefeng.guns.rest.modular.system.model.GiftRecord;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 礼物记录 Mapper 接口
 * </p>
 *
 * @author wumeng123
 * @since 2019-03-07
 */
public interface GiftRecordMapper extends BaseMapper<GiftRecord> {

	List<GiftRecord> getList(GiftRecord model);

	List<GiftRecord> getList1(GiftRecord model);

}
