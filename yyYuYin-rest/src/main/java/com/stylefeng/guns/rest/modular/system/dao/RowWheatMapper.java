package com.stylefeng.guns.rest.modular.system.dao;

import com.stylefeng.guns.rest.modular.system.model.RowWheat;
import com.stylefeng.guns.rest.modular.system.model.RowWheat;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wumeng123
 * @since 2019-03-05
 */
public interface RowWheatMapper extends BaseMapper<RowWheat> {
	List<RowWheat> getList(RowWheat model);

	RowWheat getOne(RowWheat model);
}
