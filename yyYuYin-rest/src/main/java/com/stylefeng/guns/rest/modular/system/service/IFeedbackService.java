package com.stylefeng.guns.rest.modular.system.service;

import com.stylefeng.guns.rest.modular.system.model.Feedback;
import com.stylefeng.guns.rest.modular.system.model.Feedback;

import java.util.List;

import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 反馈 服务类
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
public interface IFeedbackService extends IService<Feedback> {
public List<Feedback> getList(Feedback model);
    
    Feedback getOne(Feedback model);
}
