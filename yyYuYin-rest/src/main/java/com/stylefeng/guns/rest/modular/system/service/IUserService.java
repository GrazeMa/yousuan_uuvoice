package com.stylefeng.guns.rest.modular.system.service;

import com.stylefeng.guns.rest.modular.system.model.User;

import java.util.List;

import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 用户 服务类
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
public interface IUserService extends IService<User> {
	User getByPhone(String phone);

	User getByWXsid(String phone);

	User getByQQsid(String phone);

	User getByGZsid(String phone);
	
	User getByRoomID(String usercoding);
	
	List<User> getList(User model);
	
	User getOne(User model);
	
	List<User> getList1(User model);
	
	List<User> getList2(User model);
	
	List<User> getList3(User model);
}
