package com.stylefeng.guns.rest.modular.system.dao;

import com.stylefeng.guns.rest.modular.system.model.Music;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 音乐审核 Mapper 接口
 * </p>
 *
 * @author wumeng123
 * @since 2019-03-05
 */
public interface MusicMapper extends BaseMapper<Music> {

	List<Music> getList(Music model);

	Music getOne(Music model);

	Integer getConut(Music music);

}
