package com.stylefeng.guns.rest.modular.system.service.impl;

import com.stylefeng.guns.rest.modular.system.model.SetRecharge;
import com.stylefeng.guns.rest.modular.system.dao.SetRechargeMapper;
import com.stylefeng.guns.rest.modular.system.service.ISetRechargeService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import java.util.List;

import org.springframework.stereotype.Service;

/**
 * <p>
 * 充值设置 服务实现类
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
@Service
public class SetRechargeServiceImpl extends ServiceImpl<SetRechargeMapper, SetRecharge> implements ISetRechargeService {

	@Override
	public List<SetRecharge> getList(SetRecharge model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getList(model);
	}

	@Override
	public SetRecharge getOne(SetRecharge model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getOne(model);
	}

}
