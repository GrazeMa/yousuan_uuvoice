package com.stylefeng.guns.rest.modular.system.service.impl;

import com.stylefeng.guns.rest.modular.system.model.Sign;
import com.stylefeng.guns.rest.modular.system.dao.SignMapper;
import com.stylefeng.guns.rest.modular.system.service.ISignService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import java.util.List;

import org.springframework.stereotype.Service;

/**
 * <p>
 * 签到设置 服务实现类
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
@Service
public class SignServiceImpl extends ServiceImpl<SignMapper, Sign> implements ISignService {

	@Override
	public List<Sign> getList(Sign model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getList(model);
	}

	@Override
	public Sign getOne(Sign model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getOne(model);
	}

}
