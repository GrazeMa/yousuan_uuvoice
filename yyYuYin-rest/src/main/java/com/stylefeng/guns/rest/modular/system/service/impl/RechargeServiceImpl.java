package com.stylefeng.guns.rest.modular.system.service.impl;

import com.stylefeng.guns.rest.modular.system.model.Recharge;
import com.stylefeng.guns.rest.modular.system.dao.RechargeMapper;
import com.stylefeng.guns.rest.modular.system.service.IRechargeService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import java.util.List;

import org.springframework.stereotype.Service;

/**
 * <p>
 * 充值明细 服务实现类
 * </p>
 *
 * @author wumeng123
 * @since 2019-02-14
 */
@Service
public class RechargeServiceImpl extends ServiceImpl<RechargeMapper, Recharge> implements IRechargeService {

	@Override
	public List<Recharge> getList(Recharge model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getList(model);
	}

	@Override
	public Recharge getOne(Recharge model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getOne(model);
	}

	@Override
	public List<Recharge> getList1(Recharge model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getList1(model);
	}

}
