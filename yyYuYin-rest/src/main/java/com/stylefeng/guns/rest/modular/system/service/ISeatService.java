package com.stylefeng.guns.rest.modular.system.service;

import com.stylefeng.guns.rest.modular.system.model.Seat;

import java.util.List;

import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 房间座位相关设置 服务类
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-16
 */
public interface ISeatService extends IService<Seat> {
	
	List<Seat>getList(Seat Seat);
	Seat getOne(Seat Seat);

}
