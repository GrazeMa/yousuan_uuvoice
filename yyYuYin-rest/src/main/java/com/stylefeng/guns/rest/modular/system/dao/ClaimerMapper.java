package com.stylefeng.guns.rest.modular.system.dao;

import com.stylefeng.guns.rest.modular.system.model.Claimer;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 推荐位申请记录 Mapper 接口
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
public interface ClaimerMapper extends BaseMapper<Claimer> {

	List<Claimer> getList(Claimer model);

	Claimer getOne(Claimer model);

}
