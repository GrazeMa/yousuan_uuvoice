package com.stylefeng.guns.rest.modular.system.im;
	import java.net.HttpURLConnection;
import java.net.URLEncoder;

import org.junit.Assert;
import org.junit.Test;


	public class TlsSigTest {
	    @Test
	    public void genAndVerify() {
	        try {
	            //Use pemfile keys to test
	            String privStr = "-----BEGIN PRIVATE KEY-----\n" +
	                "MIGHAgEAMBMGByqGSM49AgEGCCqGSM49AwEHBG0wawIBAQQg2tZa1POUgA8eZkrk\n" +
	                "E/X0Lti0VsuQ68KyoAHpCHvQWSahRANCAAQqGxxAgvqc68wPUyVJBNe4hpiVx4mG\n" +
	                "CSoFK/A3PmGZ3G1psQ0+yOOlQxU+GPoKEtR3z9/sd3yZ9cc8895AXwrP\n" +
	                "-----END PRIVATE KEY-----";

	            //change public pem string to public string
	            String pubStr = "-----BEGIN PUBLIC KEY-----\n" +
	                "MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEKhscQIL6nOvMD1MlSQTXuIaYlceJ\n" +
	                "hgkqBSvwNz5hmdxtabENPsjjpUMVPhj6ChLUd8/f7Hd8mfXHPPPeQF8Kzw==\n" +
	                "-----END PUBLIC KEY-----";

	            // generate signature
	            tls_sigature.GenTLSSignatureResult result = tls_sigature.GenTLSSignatureEx(1400203579, "xiaojun", privStr);
	            Assert.assertNotEquals(null, result);
	            Assert.assertNotEquals(null, result.urlSig);
	            Assert.assertNotEquals(0, result.urlSig.length());

	            // check signature
	            tls_sigature.CheckTLSSignatureResult checkResult = tls_sigature.CheckTLSSignatureEx(result.urlSig, 1400000000, "xiaojun", pubStr);
	            Assert.assertNotEquals(null, checkResult);
	            Assert.assertTrue(checkResult.verifyResult);

	            checkResult = tls_sigature.CheckTLSSignatureEx(result.urlSig, 1400203579, "xiaojun2", pubStr);
	            Assert.assertNotEquals(null, checkResult);
	            Assert.assertFalse( checkResult.verifyResult);


	            // new interface generate signature
	            result = tls_sigature.genSig(1400000000, "xiaojun", privStr);
	            Assert.assertNotEquals(null, result);
	            Assert.assertNotEquals(null, result.urlSig);
	            Assert.assertNotEquals(0, result.urlSig.length());

	            // check signature
	            checkResult = tls_sigature.CheckTLSSignatureEx(result.urlSig, 1400203579, "xiaojun", pubStr);
	            Assert.assertNotEquals(null, checkResult);
	            Assert.assertTrue(checkResult.verifyResult);

	            checkResult = tls_sigature.CheckTLSSignatureEx(result.urlSig, 1400203579, "xiaojun2", pubStr);
	            Assert.assertNotEquals(null, checkResult);
	            Assert.assertFalse( checkResult.verifyResult);
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	    }
	    
	    
	    public String getToken() throws Exception {
	    	 //Use pemfile keys to test
            String privStr = "-----BEGIN PRIVATE KEY-----\n" +
                "MIGHAgEAMBMGByqGSM49AgEGCCqGSM49AwEHBG0wawIBAQQg2tZa1POUgA8eZkrk\n" +
                "E/X0Lti0VsuQ68KyoAHpCHvQWSahRANCAAQqGxxAgvqc68wPUyVJBNe4hpiVx4mG\n" +
                "CSoFK/A3PmGZ3G1psQ0+yOOlQxU+GPoKEtR3z9/sd3yZ9cc8895AXwrP\n" +
                "-----END PRIVATE KEY-----";

            //change public pem string to public string
            String pubStr = "-----BEGIN PUBLIC KEY-----\n" +
                "MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEKhscQIL6nOvMD1MlSQTXuIaYlceJ\n" +
                "hgkqBSvwNz5hmdxtabENPsjjpUMVPhj6ChLUd8/f7Hd8mfXHPPPeQF8Kzw==\n" +
                "-----END PUBLIC KEY-----";
            // generate signature
            tls_sigature.GenTLSSignatureResult result = tls_sigature.GenTLSSignatureEx(1400203579, "170", privStr);
            
			return result.urlSig;
            
		}
	    
	    
	}


