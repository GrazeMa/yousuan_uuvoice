package com.stylefeng.guns.rest.modular.system.dao;

import com.stylefeng.guns.rest.modular.system.model.AndroidVersions;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 安卓版本控制 Mapper 接口
 * </p>
 *
 * @author wumeng123
 * @since 2019-05-17
 */
public interface AndroidVersionsMapper extends BaseMapper<AndroidVersions> {

	List<AndroidVersions> getList(AndroidVersions model);

	AndroidVersions getOne(AndroidVersions model);

}
