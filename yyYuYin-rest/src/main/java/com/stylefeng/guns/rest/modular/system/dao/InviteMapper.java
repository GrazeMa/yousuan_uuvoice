package com.stylefeng.guns.rest.modular.system.dao;

import com.stylefeng.guns.rest.modular.system.model.Invite;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 邀请用户 Mapper 接口
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
public interface InviteMapper extends BaseMapper<Invite> {

	List<Invite> getList(Invite model);

	Invite getOne(Invite model);

	List<Invite> getList1(Invite model);

	Integer getCount(Invite model);

}
