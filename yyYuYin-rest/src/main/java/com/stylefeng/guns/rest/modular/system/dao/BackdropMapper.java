package com.stylefeng.guns.rest.modular.system.dao;

import com.stylefeng.guns.rest.modular.system.model.Backdrop;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 背景图 Mapper 接口
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-16
 */
public interface BackdropMapper extends BaseMapper<Backdrop> {

	List<Backdrop> getList(Backdrop model);

	Backdrop getOne(Backdrop model);

}
