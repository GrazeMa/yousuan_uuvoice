package com.stylefeng.guns.rest.modular.system.service;

import com.stylefeng.guns.rest.modular.system.model.GfitNum;
import com.stylefeng.guns.rest.modular.system.model.Gift;

import java.util.List;

import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 送的礼物，自己购买的头环，座驾，自己开宝箱得道的礼物，头环，座驾，  服务类
 * </p>
 *
 * @author wumeng123
 * @since 2019-03-02
 */
public interface IGfitNumService extends IService<GfitNum> {
	public List<GfitNum> getList(GfitNum gf);
	public List<GfitNum> getList1(GfitNum model);
}
