package com.stylefeng.guns.rest.modular.system.service.impl;

import com.stylefeng.guns.rest.modular.system.model.LiangDivide;
import com.stylefeng.guns.rest.modular.system.dao.LiangDivideMapper;
import com.stylefeng.guns.rest.modular.system.service.ILiangDivideService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 设置的一些关于分成的参数 服务实现类
 * </p>
 *
 * @author wumeng123
 * @since 2019-07-04
 */
@Service
public class LiangDivideServiceImpl extends ServiceImpl<LiangDivideMapper, LiangDivide> implements ILiangDivideService {

}
