package com.stylefeng.guns.rest.modular.system.tencent.common;

import com.stylefeng.guns.rest.modular.util.ParamUtil;

/**
 * User: TZJ
 * Date: 2017/08/29
 * Time: 14:40
 * 这里放置微信支付的各种配置数据
 */
public class Configure {
//这个就是自己要保管好的私有Key了（切记只能放在自己的后台代码里，不能放在任何可能被看到源代码的客户端程序中）
	// 每次自己Post数据给API的时候都要用这个key来对所有字段进行签名，生成的签名会放在Sign这个字段，API收到Post数据的时候也会用同样的签名算法对Post过来的数据进行签名和验证
	// 收到API的返回的时候也要用这个key来对返回的数据算下签名，跟API的Sign数据进行比较，如果值不一致，有可能数据被第三方给篡改

	 
	// yuanxiaoshu 微信相关配置
	
	
	private static String key = ParamUtil.getValue("wx_mch_key");

	//用户端开发者平台的appid
	private static String appID = ParamUtil.getValue("wx_app_id");
	//公众号的appid
	private static String GappID = ParamUtil.getValue("wx_pub_app_id");
	//小程序的appid
	private static String XappID = ParamUtil.getValue("XappID");
	
	//公众号的appSecret
	private static String appSecret = ParamUtil.getValue("wx_pub_appSecret");

	//微信支付分配的商户号ID（开通公众号的微信支付功能之后可以获取到）
	private static String mchID = ParamUtil.getValue("wx_mch_ID");
	
	
	private static String key_2 = ParamUtil.getValue("wx_mch_key_2");
	//微信分配的公众号ID（开通公众号之后可以获取到）
	private static String appID_2 = ParamUtil.getValue("appID_2");
	//微信支付分配的商户号ID（开通公众号的微信支付功能之后可以获取到）
	private static String mchID_2 = ParamUtil.getValue("mchID_2");
	
	
	//公众号的商户号
	private static String GmchID = ParamUtil.getValue("wx_pub_mch_ID");
	//小程序的商户号
	private static String XmchID = ParamUtil.getValue("XmchID");
	
	//微信回调地址
	public static String wx_notify_url = ParamUtil.getValue("wx_notify_url");

	//受理模式下给子商户分配的子商户号
	private static String subMchID = "";

	//HTTPS证书的本地路径
	private static String certLocalPath;
	static{//从服务器相对路径中获取
		certLocalPath = Configure.class.getClassLoader().getResource("").getPath() + "com/stylefeng/guns/rest/modular/system/tencent/common/cert/apiclient_cert.p12";
	}
	
	//HTTPS证书密码，默认密码等于商户号MCHID
		private static String certPassword = mchID;

	//HTTPS证书的本地路径
	private static String certLocalPath_2;
	static{//从服务器相对路径中获取
		certLocalPath_2 = Configure.class.getClassLoader().getResource("").getPath() + "com/stylefeng/guns/rest/modular/system/tencent/common/cert2/apiclient_cert.p12";
	}	
	
	//HTTPS证书密码，默认密码等于商户号MCHID
	private static String certPassword_2 = GmchID;
	
	//是否使用异步线程的方式来上报API测速，默认为异步模式
	private static boolean useThreadToDoReport = true;

	//机器IP
	private static String ip = "";

	//以下是几个API的路径：
	//1）被扫支付API
	public static String PAY_API = "https://api.mch.weixin.qq.com/pay/micropay";

	//2）被扫支付查询API
	public static String PAY_QUERY_API = "https://api.mch.weixin.qq.com/pay/orderquery";

	//3）退款API
	public static String REFUND_API = "https://api.mch.weixin.qq.com/secapi/pay/refund";

	//4）退款查询API
	public static String REFUND_QUERY_API = "https://api.mch.weixin.qq.com/pay/refundquery";

	//5）撤销API
	public static String REVERSE_API = "https://api.mch.weixin.qq.com/secapi/pay/reverse";

	//6）下载对账单API
	public static String DOWNLOAD_BILL_API = "https://api.mch.weixin.qq.com/pay/downloadbill";

	//7) 统计上报API
	public static String REPORT_API = "https://api.mch.weixin.qq.com/payitil/report";
	
	//商家支付用户（提现）API
	public static String PayToTheUser_API = "https://api.mch.weixin.qq.com/mmpaymkttransfers/promotion/transfers";

	//统一下单API（预支付生成接口）
	public static String UNIFIEDORDER_API = "https://api.mch.weixin.qq.com/pay/unifiedorder";
	
	public static boolean isUseThreadToDoReport() {
		return useThreadToDoReport;
	}

	public static void setUseThreadToDoReport(boolean useThreadToDoReport) {
		Configure.useThreadToDoReport = useThreadToDoReport;
	}

	public static String HttpsRequestClassName = "com.stylefeng.guns.rest.modular.system.tencent.common.HttpsRequest";
	
	public static String HttpsRequestClassName_2 = "com.stylefeng.guns.rest.modular.system.tencent.common.HttpsRequest_2";

	public static void setKey(String key) {
		Configure.key = key;
	}

	public static void setAppID(String appID) {
		Configure.appID = appID;
	}

	public static void setMchID(String mchID) {
		Configure.mchID = mchID;
	}

	public static void setSubMchID(String subMchID) {
		Configure.subMchID = subMchID;
	}

	public static void setCertLocalPath(String certLocalPath) {
		Configure.certLocalPath = certLocalPath;
	}

	public static void setCertPassword(String certPassword) {
		Configure.certPassword = certPassword;
	}

	public static void setIp(String ip) {
		Configure.ip = ip;
	}

	public static String getKey(){
		return key;
	}
	
	public static String getAppid(){
		return appID;
	}
	public static String getGappid(){
		return GappID;
	}
	public static String getXappid(){
		return XappID;
	}	
	public static String getMchid(){
		return mchID;
	}
	
	public static String getGmchid(){
		return GmchID;
	}
	public static String getXmchid(){
		return XmchID;
	}

	public static String getCertPassword_2() {
		return certPassword_2;
	}

	public static void setCertPassword_2(String certPassword_2) {
		Configure.certPassword_2 = certPassword_2;
	}

	public static String getSubMchid(){
		return subMchID;
	}
	
	public static String getCertLocalPath(){
		return certLocalPath;
	}
	
	public static String getCertLocalPath_2() {
		return certLocalPath_2;
	}

	public static void setCertLocalPath_2(String certLocalPath_2) {
		Configure.certLocalPath_2 = certLocalPath_2;
	}

	public static String getCertPassword(){
		return certPassword;
	}

	public static String getIP(){
		return ip;
	}

	public static String getAppSecret() {
		return appSecret;
	}

	public static void setAppSecret(String appSecret) {
		Configure.appSecret = appSecret;
	}

	public static void setHttpsRequestClassName(String name){
		HttpsRequestClassName = name;
	}

	public static String getKey_2() {
		return key_2;
	}

	public static void setKey_2(String key_2) {
		Configure.key_2 = key_2;
	}

	public static String getAppID_2() {
		return appID_2;
	}

	public static void setAppID_2(String appID_2) {
		Configure.appID_2 = appID_2;
	}

	public static String getAppID() {
		return appID;
	}

	
	
}
