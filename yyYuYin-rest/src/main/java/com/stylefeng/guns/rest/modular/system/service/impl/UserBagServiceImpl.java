package com.stylefeng.guns.rest.modular.system.service.impl;

import com.stylefeng.guns.rest.modular.system.model.UserBag;
import com.stylefeng.guns.rest.modular.system.dao.UserBagMapper;
import com.stylefeng.guns.rest.modular.system.service.IUserBagService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import java.util.List;

import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户背包服务实现类
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
@Service
public class UserBagServiceImpl extends ServiceImpl<UserBagMapper, UserBag> implements IUserBagService {

	@Override
	public List<UserBag> getByUid(Integer uid) {
		// TODO Auto-generated method stub
		return this.baseMapper.getByUid(uid);
	}
	
	@Override
	public UserBag getOne(UserBag model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getOne(model);
	}

}
