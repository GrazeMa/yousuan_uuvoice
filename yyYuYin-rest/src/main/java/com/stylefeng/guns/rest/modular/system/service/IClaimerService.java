package com.stylefeng.guns.rest.modular.system.service;

import com.stylefeng.guns.rest.modular.system.model.Claimer;
import com.stylefeng.guns.rest.modular.system.model.Claimer;

import java.util.List;

import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 推荐位申请记录 服务类
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
public interface IClaimerService extends IService<Claimer> {
public List<Claimer> getList(Claimer model);
    
    Claimer getOne(Claimer model);
}
