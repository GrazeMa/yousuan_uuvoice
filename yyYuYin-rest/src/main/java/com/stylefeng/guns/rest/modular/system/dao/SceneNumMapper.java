package com.stylefeng.guns.rest.modular.system.dao;

import com.stylefeng.guns.rest.modular.system.model.SceneNum;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wumeng123
 * @since 2019-02-18
 */
public interface SceneNumMapper extends BaseMapper<SceneNum> {

	List<SceneNum> getList(SceneNum model);

	SceneNum getOne(SceneNum model);

}
