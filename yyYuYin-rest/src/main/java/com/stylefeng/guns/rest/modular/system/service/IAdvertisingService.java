package com.stylefeng.guns.rest.modular.system.service;

import com.stylefeng.guns.rest.modular.system.model.Advertising;

import java.util.List;

import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 广告设置 服务类
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
public interface IAdvertisingService extends IService<Advertising> {
	
	    public List<Advertising> getList(Advertising model);
	    
	    Advertising getOne(Advertising model);

}
