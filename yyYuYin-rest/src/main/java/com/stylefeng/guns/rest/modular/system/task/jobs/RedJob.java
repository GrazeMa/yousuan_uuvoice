package com.stylefeng.guns.rest.modular.system.task.jobs;

import com.alibaba.fastjson.JSONObject;

import com.baomidou.mybatisplus.mapper.EntityWrapper;

import com.stylefeng.guns.rest.modular.system.service.impl.UserServiceImpl;
import com.stylefeng.guns.rest.modular.system.task.base.AbstractJob;
import com.stylefeng.guns.rest.modular.util.ParamUtil;

import io.agora.signal.Signal;
import io.agora.signal.Signal.LoginSession;
import io.agora.signal.Signal.LoginSession.Channel;

import com.stylefeng.guns.rest.modular.system.model.Pk;
import com.stylefeng.guns.rest.modular.system.model.Red;
import com.stylefeng.guns.rest.modular.system.model.RedNum;
import com.stylefeng.guns.rest.modular.system.model.Room;
import com.stylefeng.guns.rest.modular.system.model.TimeTask;
import com.stylefeng.guns.rest.modular.system.model.User;

import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import java.util.Iterator;
import java.util.List;

/**
 * 下单定时任务
 *
 *
 */
public class RedJob extends AbstractJob {

	public static final String name = "RedJob";
	
	public static String SappID = ParamUtil.getValue("SappID");

	@Override
	public void execute(JobExecutionContext context)
			throws JobExecutionException {
		JobDataMap maps = context.getMergedJobDataMap();
		Integer  pid = maps.getInt("oid");
		Integer timeTaskId = maps.getInt("timeTaskId");
		try{
			
			Red red = RedService.selectById(pid);
			User user = UserService.selectById(red.getUid());
			
			RedNum r=new RedNum();
			r.setRedId(pid);
			r.setState(1);
			r.setStatus(1);
			List<RedNum> list = RedNumService.getList(r);
			Iterator<RedNum> iterator = list.iterator();
			while (iterator.hasNext()) {
				   RedNum next = iterator.next();
				   user.setGold(user.getGold()+next.getGold());
				   next.setStatus(2);
				   RedNumService.updateById(next);
			}
			
			UserService.updateById(user);
			
			
			
			//修改定时任务数据状态
			TimeTask timeTask=TimeTaskService.selectById(timeTaskId);
			//0有效，1无效
			timeTask.setState(1);
			TimeTaskService.updateById(timeTask);
			
		}catch(Exception e){
			logger.debug("执行“下单定时任务自动取消”异常:orderNum={}",pid,e);
		}
	}

}
