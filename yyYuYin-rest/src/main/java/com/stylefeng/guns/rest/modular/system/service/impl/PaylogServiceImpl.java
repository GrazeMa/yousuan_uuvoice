package com.stylefeng.guns.rest.modular.system.service.impl;

import com.stylefeng.guns.rest.modular.system.model.Paylog;
import com.stylefeng.guns.rest.modular.system.dao.PaylogMapper;
import com.stylefeng.guns.rest.modular.system.service.IPaylogService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 支付记录 服务实现类
 * </p>
 *
 * @author wumeng123
 * @since 2019-02-14
 */
@Service
public class PaylogServiceImpl extends ServiceImpl<PaylogMapper, Paylog> implements IPaylogService {

}
