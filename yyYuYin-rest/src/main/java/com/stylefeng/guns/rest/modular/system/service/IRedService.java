package com.stylefeng.guns.rest.modular.system.service;

import com.stylefeng.guns.rest.modular.system.model.Red;


import java.util.List;

import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 红包 服务类
 * </p>
 *
 * @author wumeng123
 * @since 2019-02-20
 */
public interface IRedService extends IService<Red> {
     public List<Red> getList(Red model);
    
    Red getOne(Red model);
}
