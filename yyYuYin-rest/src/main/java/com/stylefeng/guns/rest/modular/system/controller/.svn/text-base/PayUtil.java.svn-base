package com.stylefeng.guns.rest.modular.system.controller;

import java.io.IOException;

import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.plaf.metal.MetalIconFactory.PaletteCloseIcon;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.domain.AlipayTradeRefundModel;
import com.alipay.api.internal.util.AlipaySignature;
import com.alipay.api.request.AlipayTradeRefundRequest;
import com.alipay.api.response.AlipayTradeRefundResponse;
import com.stylefeng.guns.rest.modular.system.alipay.config.AlipayConfig;
import com.stylefeng.guns.rest.modular.system.alipay.util.PayDemoActivity;
import com.stylefeng.guns.rest.modular.system.model.Divide;
import com.stylefeng.guns.rest.modular.system.model.Invite;
import com.stylefeng.guns.rest.modular.system.model.Paylog;
import com.stylefeng.guns.rest.modular.system.model.Recharge;
import com.stylefeng.guns.rest.modular.system.model.RechargeOrder;
import com.stylefeng.guns.rest.modular.system.model.SetInfo;
import com.stylefeng.guns.rest.modular.system.model.SetShare;
import com.stylefeng.guns.rest.modular.system.model.User;
import com.stylefeng.guns.rest.modular.system.model.Ybrecharge;
import com.stylefeng.guns.rest.modular.system.service.IDivideService;
import com.stylefeng.guns.rest.modular.system.service.IInviteService;
import com.stylefeng.guns.rest.modular.system.service.IPaylogService;
import com.stylefeng.guns.rest.modular.system.service.IRechargeOrderService;
import com.stylefeng.guns.rest.modular.system.service.IRechargeService;
import com.stylefeng.guns.rest.modular.system.service.ISetShareService;
import com.stylefeng.guns.rest.modular.system.service.IUserService;
import com.stylefeng.guns.rest.modular.system.service.IYbrechargeService;
import com.stylefeng.guns.rest.modular.system.tencent.WXPay;
import com.stylefeng.guns.rest.modular.system.tencent.common.Configure;
import com.stylefeng.guns.rest.modular.system.tencent.common.Signature;
import com.stylefeng.guns.rest.modular.system.tencent.common.XMLParser;
import com.stylefeng.guns.rest.modular.system.tencent.protocol.AppPayReqData;
import com.stylefeng.guns.rest.modular.system.tencent.protocol.RefundReqData;
import com.stylefeng.guns.rest.modular.system.tencent.protocol.UnifiedorderReqData;
import com.stylefeng.guns.rest.modular.util.ApiUtil;
import com.stylefeng.guns.rest.modular.util.DateUtil;
import com.stylefeng.guns.rest.modular.util.RandomNumberGeneratorUtil;
import com.stylefeng.guns.rest.modular.util.SinataUtil;

/**
 * <b>支付处理工具类</b>
 * <p>
 * 封装【支付宝支付、微信支付、余额支付】
 * </p>
 * 
 * @author tzj
 * @data 2017年6月17日
 * @version 1.0
 */
@Controller
@RequestMapping("api/pay")
public class PayUtil {

	/**
	 * 日志记录（记录打印报文）
	 */
	Logger log = LoggerFactory.getLogger(getClass());
	//充值订单
	@Autowired
	private IRechargeOrderService RechargeOrderService;
	//支付记录
	@Autowired
	private IPaylogService paylogService;
	//充值明细
	@Autowired
	private IRechargeService RechargeService;
	//用户信息
	@Autowired
	private IUserService UserService;
	//优币充值记录
	@Autowired
	private IYbrechargeService YbrechargeService;
	//充值分成金额
	@Autowired
	private IDivideService DivideService;
	//邀请用户（邀请明细
	@Autowired
	private IInviteService InviteService;
	//分享赠送
	@Autowired
	private ISetShareService SetShareService;

	/**
	 * 获取支付信息
	 * 
	 * @param userId
	 *            用户ID
	 * @param type
	 *            支付类型 1=支付宝,2 = 微信, 3=微信公众号
	 * @param ordernum
	 *            订单编号
	 * @param price
	 *            金额
	 * @return
	 */
	@ResponseBody
	@RequestMapping("getPayInfo")
	public Map<String, Object> getPayInfo(Integer uid, Integer type, String openId, String OrderNum,
			HttpServletRequest request) {
		
		String stmp = "PayUtil->getPayInfo:uid="+uid+",type="+type+",openId="+openId+",OrderNum="+OrderNum;
		try {
			System.out.println(java.net.URLDecoder.decode(stmp, "UTF-8"));
		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		Double price = 0.0;
		String subject = "";
		String body = "";
		try {
			if (OrderNum.contains("R")) {
				RechargeOrder order = RechargeOrderService.getorderNum(OrderNum);
				price = Double.valueOf(order.getPayment());
				body = "充值";
				subject = "充值";
				Map<String, Object> map = new HashMap<String, Object>();
				if (type == 1) {
					// 支付宝预下单
					return alipay(order.getOrderNum(), subject, body, price, request);
				}
				if (type == 2) {
					// 微信预下单
					return wxpay(1, order.getOrderNum(), body, price, request);
				}
				if (type == 3) {
					// 微信公众号
					return ApiUtil.returnObj(getPayDateG(order.getOrderNum(), body, price, "JSAPI", openId));
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return ApiUtil.putFailObj("获取异常");
	}

	/**
	 * 支付宝订单退款
	 * 
	 * @author tzj
	 * @param out_trade_no
	 *            商品订单号
	 * @param trade_no
	 *            支付宝交易号
	 * @param refund_amount
	 *            退款金额
	 * @return
	 */
	public static boolean refundForAlipay(String out_trade_no, String trade_no, Double refund_amount) {
		try {
			String random = RandomNumberGeneratorUtil.getStringRandom(9);
			// 实例化客户端
			AlipayClient alipayClient = new DefaultAlipayClient("https://openapi.alipay.com/gateway.do",
					AlipayConfig.app_id, AlipayConfig.private_key, "json", AlipayConfig.input_charset,
					AlipayConfig.ali_public_key, "RSA2");
			// 实例化具体API对应的request类,类名称和接口名称对应
			AlipayTradeRefundRequest request = new AlipayTradeRefundRequest();
			// SDK已经封装掉了公共参数，这里只需要传入业务参数
			// 此次只是参数展示，未进行字符串转义，实际情况下请转义
			/*
			 * request.setBizContent("{" + "\"out_trade_no\":\"" + out_trade_no
			 * + "\", " + "\"trade_no\":\"" + trade_no + "\", " +
			 * "\"refund_amount\":" + refund_amount + ", " +
			 * "\"refund_reason\":\"正常退款\"," + "\"out_request_no\":" + random +
			 * ""+ "}");
			 */
			AlipayTradeRefundModel model = new AlipayTradeRefundModel();
			model.setOutTradeNo(out_trade_no);
			model.setTradeNo(trade_no);
			model.setRefundAmount(refund_amount.toString());
			model.setRefundReason("正常退款");
			model.setOutRequestNo(random);
			request.setBizModel(model);

			AlipayTradeRefundResponse response = alipayClient.execute(request);
			// 调用成功，则处理业务逻辑
			if (response.isSuccess()) {
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return false;
	}

	/**
	 * 获取支付信息支付(签约app支付2.0)
	 * 
	 * @param orderid
	 * @param subject
	 * @param body
	 * @param price
	 * @param request
	 * @return
	 */
	public static Map<String, Object> alipay(String orderNo, String subject, String body, Double price,
			HttpServletRequest request) {
		try {
			// 接口封装支付宝请求参数
			// Map<String, Object> mData = new HashMap<String, Object>();

			// 构建请求支付签名参数
			Map<String, Object> pay = PayDemoActivity.appPay(subject, body, price, orderNo);
			/*
			 * Set<Entry<String, String>> entrySet = pay.entrySet();
			 * Iterator<Entry<String, String>> iterator = entrySet.iterator();
			 * while (iterator.hasNext()) { Entry<String, String> next =
			 * iterator.next(); mData.put(next.getKey(), next.getValue()); }
			 */
			return ApiUtil.returnObj(pay);
		} catch (Exception e) {
			e.printStackTrace();
			return ApiUtil.putFailObj("支付异常!");
		}
	}

	/**
	 * 服务器异步通知处理支付宝
	 * 
	 * @param request
	 * @param res
	 */
	@RequestMapping("/alipay/notify")
	public void notifyUrl(HttpServletRequest request, HttpServletResponse res) {
		HttpServletResponse response = (HttpServletResponse) res;
		response.setContentType("text/html;charset=UTF-8");
		PrintWriter out;
		try {
			out = response.getWriter();
			// 获取支付宝POST过来反馈信息
			Map<String, String> params = new HashMap<String, String>();
			Map requestParams = request.getParameterMap();
			log.debug("AlipayController.notifyUrl__requestParams：\n" + requestParams);
			for (Iterator iter = requestParams.keySet().iterator(); iter.hasNext();) {
				String name = (String) iter.next();
				String[] values = (String[]) requestParams.get(name);
				String valueStr = "";
				for (int i = 0; i < values.length; i++) {
					valueStr = (i == values.length - 1) ? valueStr + values[i] : valueStr + values[i] + ",";
				}
				// 乱码解决，这段代码在出现乱码时使用。如果mysign和sign不相等也可以使用这段代码转化
				valueStr = new String(valueStr.getBytes("ISO-8859-1"), "gbk");
				params.put(name, valueStr);
			}

			Paylog paylog = getPayLog_alipay(request);

			// 验证成功
			if ("TRADE_FINISHED".equals(paylog.getTradeStatus())) {
				System.out.println("AlipayController.notifyUrl__验证成功：success");
				// 支付失败
			} else if ("TRADE_SUCCESS".equals(paylog.getTradeStatus())) {
				// 支付成功
				try {
					// 开通会员
					if (paylog.getOutTradeNo().contains("R")) {
						RechargeOrder getorderNum = RechargeOrderService.getorderNum(paylog.getOutTradeNo());
						getorderNum.setState(2);
						getorderNum.setIsPay(1);
						RechargeOrderService.updateById(getorderNum);

						User user = UserService.selectById(getorderNum.getUid());

						user.setGold(user.getGold() + getorderNum.getPresented() + getorderNum.getGiveMoney());

						user.setHistoryRecharge(
								user.getHistoryRecharge() + getorderNum.getPresented() + getorderNum.getGiveMoney());

						UserService.updateById(user);

						Invite di = new Invite();
						di.setBuid(getorderNum.getUid());
						Invite one = InviteService.getOne(di);
						if (SinataUtil.isNotEmpty(one)) {
							User user2 = UserService.selectById(one.getUid());
							SetShare share = SetShareService.selectById(3);
							Double num = (double) (share.getY() / 100);
							Double money = getorderNum.getPayment();
							Double ma = money * num;

							Divide div = new Divide();
							div.setUid(one.getUid());
							div.setBuid(one.getBuid());
							div.setCreateTime(new Date());
							div.setMoney(ma);
							div.setCzMoney(money);
							div.setIsDelete(1);

							DivideService.insert(div);

							user2.setDivideAward(user2.getDivideAward() + ma);
							user2.setUserMoney(user2.getUserMoney() + ma);
							user2.setLsdivideAward(user2.getLsdivideAward() + ma);

							UserService.updateById(user2);
						}

						Recharge re = new Recharge();
						re.setCreateTime(new Date());
						re.setUid(getorderNum.getUid());
						re.setRechargeMoney(getorderNum.getPayment());
						re.setRestitution(getorderNum.getPresented());
						Integer m = getorderNum.getPresented() + getorderNum.getGiveMoney();
						re.setSumMoney(m);
						re.setIsDelete(0);
						re.setState(getorderNum.getIsState());
						re.setGiveMoney(getorderNum.getGiveMoney());
						re.setState(getorderNum.getIsState());
						RechargeService.insert(re);

						Ybrecharge f = new Ybrecharge();
						f.setCreateTime(new Date());
						f.setUid(getorderNum.getUid());
						f.setEid(user.getUsercoding());
						f.setPlay(getorderNum.getPayment());
						f.setIsDelete(1);
						f.setGmGold(getorderNum.getPresented());
						f.setZsGold(getorderNum.getGiveMoney());
						f.setSdGold(m);
						f.setUid(getorderNum.getUid());
						f.setName(user.getNickname());
						f.setPlayState(1);
						YbrechargeService.insert(f);

					}

					// ——请根据您的业务逻辑来编写程序（以上代码仅作参考）——
					log.debug("AlipayController.notifyUrl__回调处理：success");
					out.println("success"); // 请不要修改或删除
				} catch (Exception e) {
					log.debug("AlipayController.notifyUrl__回调逻辑代码处理异常！fail");
					// 返回失败
					out.println("fail");
					e.printStackTrace();
				}
				// ////////////////////////////////////////////////////////////////////////////////////////
			} else {// 验证失败
				log.debug("AlipayController.notifyUrl__回调处理失败！fail");
				out.println("fail");
			}
		} catch (IOException e) {
			log.debug("AlipayController.notifyUrl__支付宝服务器异步通知数据处理失败！");
			e.printStackTrace();
		}
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////
	/**
	 * 统一下单
	 * 
	 * @param apptype
	 * @param outTradeNo
	 * @param body
	 * @param price
	 * @param request
	 * @return
	 */
	public static Map<String, Object> wxpay(Integer apptype, String outTradeNo, String body, Double price,
			HttpServletRequest request) {
		// 获取预支付接口返回参数
		Map<String, Object> map = new HashMap<String, Object>();
		Map<String, Object> appPayMap = new HashMap<String, Object>();
		try {
			// 构建接口请求参数
			UnifiedorderReqData unifiedorderReqData = new UnifiedorderReqData(apptype, outTradeNo, body, price,
					Configure.wx_notify_url);
			System.out.println("wxpay:request=\n" + unifiedorderReqData.toString());
			
			// 请求接口获取返回接口
			String result = WXPay.requestUnifiedorderService(apptype, unifiedorderReqData);
			System.out.println("wxpay:requestUnifiedorderService=" + result);
			// 获取预支付接口返回参数
			map = XMLParser.getMapFromXML(result);
			// 捕获预支付接口错误提示
			if ("FAIL".equals(map.get("result_code")) || "FAIL".equals(map.get("return_code"))) {
				return ApiUtil.putFailObj(String.valueOf(map.get("return_msg")));
			}

			// 对获取预支付返回接口参数进行封装（生成支付订单接口数据）
			AppPayReqData appPay = new AppPayReqData(apptype, (String) map.get("appid"), (String) map.get("mch_id"),
					(String) map.get("prepay_id"), unifiedorderReqData.getNonce_str());

			// 对获取预支付返回接口参数进行封装（生成支付订单接口数据）
			appPayMap.put("appid", appPay.getAppid());// 公众账号ID
			appPayMap.put("nonceStr", appPay.getNoncestr());// 随机字符串(32位)
			appPayMap.put("package", appPay.get_package());// 扩展字段(暂填写固定值Sign=WXPay)
			appPayMap.put("partnerId", appPay.getPartnerid());// 商户号
			appPayMap.put("prepayId", appPay.getPrepayid());// 预支付编号（微信返回的支付交易会话ID）
			appPayMap.put("timeStamp", appPay.getTimestamp());// 时间戳
			appPayMap.put("sign", appPay.getSign());// 根据API给的签名规则进行签名
			return ApiUtil.returnObj(appPayMap);
		} catch (Exception e) {
			System.out.println("统一下单_API_处理异常！");
			e.printStackTrace();
		}
		return ApiUtil.putFailObj("统一下单失败");
	}

	/**
	 * 微信支付回调（参考财付通回调接口）
	 * 
	 * @param request
	 * @param response
	 */
	@RequestMapping("/wxpay/notify")
	public void wxnotify(HttpServletRequest request, HttpServletResponse response) {
		try {
			System.out.println("微信支付回调！！！！！！！！！！！！！！！！！！！！！！！！！");
			// 异步通知返回报文
			StringBuffer notityXml = new StringBuffer();
			String inputLine;
			while ((inputLine = request.getReader().readLine()) != null) {
				notityXml.append(inputLine);
			}
			request.getReader().close();
			// log.debug("WxpayController.notify__notityXml：\n" + notityXml);
			System.out.println("WxpayController.notify__notityXml：\n" + notityXml);

			// 验证签名
			if (Signature.checkIsSignValidFromResponseString(1, notityXml.toString())
					|| Signature.checkIsSignValidFromResponseString(2, notityXml.toString())) {
				Map<String, Object> map = XMLParser.getMapFromXML(notityXml.toString());
				// log.debug("WxpayController.notify__map：\n" + map);
				// 接口返回状态
				String result_code = (String) map.get("result_code");
				if ("SUCCESS".equals(result_code)) {
					// // 商户订单号
					String out_trade_no = (String) map.get("out_trade_no");
					// // 微信支付交易号
					String trade_no = (String) map.get("transaction_id");
					// // 金额,以分为单位
					String total_fee = (String) map.get("total_fee");
					// // 优惠金额
					// String discount = (String) map.get("discount");
					// // 支付完成时间
					String time_end = (String) map.get("time_end");
					// // 支付者唯一Id（对应买家账号的一个加密串 ）
					String buyer_id = (String) map.get("buyer_id");

					///////////////////////////// 这里程序处理支付回调逻辑
					///////////////////////////// ////////////////////
					Paylog paylog = new Paylog();
					paylog.setOutTradeNo(out_trade_no);
					paylog.setPayType(2);
					paylog.setBuyerId(buyer_id);
					paylog.setTradeNo(trade_no);
					paylog.setPayMoney(Double.parseDouble(total_fee) / 100);
					paylog.setState(1);
					paylog.setCreateTime(DateUtil.getDate());
					paylogService.insert(paylog);

					if (paylog.getOutTradeNo().contains("R")) {
						RechargeOrder getorderNum = RechargeOrderService.getorderNum(paylog.getOutTradeNo());
						getorderNum.setState(2);
						getorderNum.setIsPay(2);
						RechargeOrderService.updateById(getorderNum);

						User user = UserService.selectById(getorderNum.getUid());

						user.setGold(user.getGold() + getorderNum.getPresented() + getorderNum.getGiveMoney());

						user.setHistoryRecharge(
								user.getHistoryRecharge() + getorderNum.getPresented() + getorderNum.getGiveMoney());

						UserService.updateById(user);

						Invite di = new Invite();
						di.setBuid(getorderNum.getUid());
						Invite one = InviteService.getOne(di);
						if (SinataUtil.isNotEmpty(one)) {
							User user2 = UserService.selectById(one.getUid());
							SetShare share = SetShareService.selectById(3);
							Double num = (double) (share.getY() / 100);
							Double money = getorderNum.getPayment();
							Double ma = money * num;

							Divide div = new Divide();
							div.setUid(one.getUid());
							div.setBuid(one.getBuid());
							div.setCreateTime(new Date());
							div.setMoney(ma);
							div.setCzMoney(money);
							div.setIsDelete(1);

							DivideService.insert(div);

							user2.setDivideAward(user2.getDivideAward() + ma);
							user2.setUserMoney(user2.getUserMoney() + ma);
							user2.setLsdivideAward(user2.getLsdivideAward() + ma);

							UserService.updateById(user2);
						}

						Recharge re = new Recharge();
						re.setCreateTime(new Date());
						re.setUid(getorderNum.getUid());
						re.setRechargeMoney(getorderNum.getPayment());
						re.setRestitution(getorderNum.getPresented());
						Integer m = getorderNum.getPresented() + getorderNum.getGiveMoney();
						re.setSumMoney(m);
						re.setIsDelete(0);

						re.setGiveMoney(getorderNum.getGiveMoney());
						re.setState(getorderNum.getIsState());
						RechargeService.insert(re);

						Ybrecharge f = new Ybrecharge();
						f.setCreateTime(new Date());
						f.setUid(getorderNum.getUid());
						f.setEid(user.getUsercoding());
						f.setPlay(getorderNum.getPayment());
						f.setIsDelete(1);
						f.setGmGold(getorderNum.getPresented());
						f.setZsGold(getorderNum.getGiveMoney());
						f.setSdGold(m);
						f.setUid(getorderNum.getUid());
						f.setName(user.getNickname());
						f.setPlayState(2);
						YbrechargeService.insert(f);

					}

					log.debug("WxpayController.notify__回调处理成功：SUCCESS");
					response.getOutputStream().print("success");
				} else {
					log.debug("WxpayController.notify__回调处理：验证状态错误！" + result_code);
					System.out.println("验证状态错误！" + result_code);
				}
			} else {
				log.debug("WxpayController.notify__回调处理：通知签名验证失败！");
				System.out.println("通知签名验证失败！");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 微信退款
	 * 
	 * @param transactionID
	 *            【支付交易号】是微信系统为每一笔支付交易分配的订单号，通过这个订单号可以标识这笔交易，
	 *            它由支付订单API支付成功时返回的数据里面获取到。建议优先使用
	 * @param outTradeNo
	 *            【商品订单编号】商户系统内部的订单号,transaction_id
	 *            、out_trade_no二选一，如果同时存在优先级：transaction_id>out_trade_no
	 * @param outRefundNo
	 *            【退款编号】商户系统内部的退款单号，商户系统内部唯一，同一退款单号多次请求只退一笔
	 * @param totalFeetotalFee
	 *            订单总金额，单位为分
	 * @param refundFee
	 *            退款总金额，单位为分,可以做部分退款
	 * @return
	 */
	public static boolean refundForWxpay(Integer apptype, String transactionID, String outTradeNo, String outRefundNo,
			Integer totalFee, Integer refundFee, String pay_type) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			// 构建接口请求参数
			RefundReqData refundReqData = new RefundReqData(transactionID, outTradeNo, outRefundNo, totalFee, refundFee,
					pay_type);

			// 请求接口返回结果
			String result = WXPay.requestRefundService(apptype, refundReqData);
			System.out.println("微信退款返回内容：" + result);
			// 获取预支付接口返回参数
			map = XMLParser.getMapFromXML(result);
			System.out.println("微信退款返回参数：" + map);
			// 退款成功处理
			if ("SUCCESS".equals(map.get("result_code"))) {
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return false;
	}

	/**
	 * 获取支付宝
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	private Paylog getPayLog_alipay(HttpServletRequest request) throws IOException {
		//////// 获取支付宝的通知返回参数，可参考技术文档中页面跳转同步通知参数列表(以下仅供参考)////////
		// 商户订单号
		String out_trade_no = new String(request.getParameter("out_trade_no").getBytes("ISO-8859-1"), "UTF-8");
		// 支付宝交易号
		String trade_no = new String(request.getParameter("trade_no").getBytes("ISO-8859-1"), "UTF-8");
		// 交易状态
		String trade_status = new String(request.getParameter("trade_status").getBytes("ISO-8859-1"), "UTF-8");
		// 支付者唯一Id
		String buyer_id = new String(request.getParameter("buyer_id").getBytes("ISO-8859-1"), "UTF-8");
		// 支付帐号
		String buyer_email = "";
		if (SinataUtil.isNotEmpty(request.getParameter("buyer_logon_id"))) {
			buyer_email = new String(request.getParameter("buyer_logon_id").getBytes("ISO-8859-1"), "UTF-8");
		}
		// 支付金额
		String total_fee = new String(request.getParameter("total_amount").getBytes("ISO-8859-1"), "UTF-8");
		// 支付时间
		String notify_time = new String(request.getParameter("notify_time").getBytes("ISO-8859-1"), "UTF-8");

		//////// 获取支付宝的通知返回参数，可参考技术文档中页面跳转同步通知参数列表(以下仅供参考)////////
		Paylog paylog = new Paylog();
		paylog.setOutTradeNo(out_trade_no);
		paylog.setPayType(1);
		paylog.setBuyerId(buyer_id);
		paylog.setTradeNo(trade_no);
		paylog.setPayMoney(Double.parseDouble(total_fee));
		paylog.setState(1);
		paylog.setCreateTime(DateUtil.getDate());
		paylog.setTradeStatus(trade_status.toString());
		paylogService.insert(paylog);
		return paylog;
	}

	/**
	 * 公众号支付
	 */
	private Map<String, Object> getPayDateG(String orderNo, String body, Double price, String trade_type, String openId)
			throws Exception {// JSAPI
		System.out.println("getPayDateG start:orderNo="+orderNo+",body=" + body + ",price="+price+",trade_type="+trade_type+",openId=" + openId);
		
		Map<String, Object> map = new HashMap<String, Object>();
		Map<String, Object> appPayMap = new HashMap<String, Object>();
		// 构建接口请求参数
		UnifiedorderReqData unifiedorderReqData = new UnifiedorderReqData(orderNo, body, price, Configure.wx_notify_url,
				trade_type, openId);

		// 请求接口获取返回接口
		System.out.println("requestUnifiedorderService start");
		String result = WXPay.requestUnifiedorderService(2, unifiedorderReqData);
		System.out.println("WxpayController.createOrder__result：\n" + result);

		// 获取预支付接口返回参数
		map = XMLParser.getMapFromXML(result);
		System.out.println("WxpayController.createOrder__result：\n" + result);

		// 捕获预支付接口错误提示
		if ("FAIL".equals(map.get("result_code")) || "FAIL".equals(map.get("return_code"))) {
			return ApiUtil.putFailObj(String.valueOf(map.get("return_msg")));
		}

		// 对获取预支付返回接口参数进行封装（生成支付订单接口数据）
		AppPayReqData appPay = new AppPayReqData(1, (String) map.get("appid"), (String) map.get("mch_id"),
				(String) map.get("prepay_id"), unifiedorderReqData.getNonce_str());

		// 对获取预支付返回接口参数进行封装（生成支付订单接口数据）
		appPayMap.put("appId", appPay.getAppid());// 公众账号ID
		appPayMap.put("nonceStr", appPay.getNoncestr());// 随机字符串(32位)
		appPayMap.put("package", "prepay_id=" + appPay.getPrepayid());
		appPayMap.put("signType", "MD5");// 签名类型
		appPayMap.put("timeStamp", appPay.getTimestamp());// 时间戳
		// 公众号参与签名的参数： appId, timeStamp, nonceStr, package, signType
		appPayMap.put("paySign", Signature.getSign(2, appPayMap));// 根据API给的签名规则进行签名
		return appPayMap;
	}

	public boolean judgeContainsStr(String cardNum) {
		String regex = ".*[a-zA-Z]+.*";
		Matcher m = Pattern.compile(regex).matcher(cardNum);
		return m.matches();
	}
	
	@ResponseBody
	@RequestMapping("iosPayCheck")
	public Map<String, Object> getPayInfo(Integer uid, Integer goldNum, String receipt) {
		User user = UserService.selectById(uid);
		user.setGold(user.getGold() + goldNum);
		user.setHistoryRecharge(user.getHistoryRecharge() + goldNum);
		UserService.updateById(user);
		
		return ApiUtil.putSuccessObj("success");
	}

}