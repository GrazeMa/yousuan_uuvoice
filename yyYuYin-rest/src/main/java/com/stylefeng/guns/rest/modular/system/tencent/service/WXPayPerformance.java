package com.stylefeng.guns.rest.modular.system.tencent.service;
/*package com.tencent.service;

import java.util.HashMap;
import java.util.Map;

import com.tencent.WXPay;
import com.tencent.common.WXPayUtil;

public class WXPayPerformance {


	private WXPay wxpay;
	private WXPayConfigImpl config;

	public WXPayPerformance() throws Exception {
		config = WXPayConfigImpl.getInstance();
		wxpay = new WXPay(config);
	}

	*//**
	 * 商户 扫码抢 扫码支付
	 * @param out_trade_no
	 * @param body
	 * @param total_fee
	 * @param auth_code
	 *//*
	public Map<String, String> doMicropayWithPos(String out_trade_no,String body,String total_fee,String auth_code) {
		HashMap<String, String> data = new HashMap<String, String>();
		data.put("out_trade_no", out_trade_no);
		data.put("body", body);
		data.put("total_fee", total_fee);
		data.put("auth_code", auth_code);
		Map<String, String> r=new HashMap<>();
		try {
			 r = wxpay.microPayWithPos(data);
			System.out.println(r);
		} catch (Exception e) {			
			e.printStackTrace();
			return r;
		}
		return r;
	}

	
    *//**
     * 统一 下单2.0
     *//*
    public Map<String, String> doUnifiedOrder(String out_trade_no,String total_fee,String notify_url) {
        HashMap<String, String> data = new HashMap<String, String>();
        data.put("body", "超级教材");
        data.put("out_trade_no", out_trade_no);
        data.put("device_info", "");
        data.put("fee_type", "CNY");
        data.put("total_fee", total_fee);
        //data.put("spbill_create_ip", "123.12.12.123");
        data.put("notify_url", notify_url);
        data.put("trade_type", "APP");
        //data.put("product_id", "12");
        // data.put("time_expire", "20170112104120");

        try {
            Map<String, String> r = wxpay.unifiedOrder(data);
            System.out.println(r);
            return r;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


	//    public void doReport() {
	//        HashMap<String, String> data = new HashMap<String, String>();
	//        data.put("interface_url", "20160822");
	//        data.put("bill_type", "ALL");
	//    }

	*//**
	 * 小测试
	 *//*
	public void test001() {
		String xmlStr="<xml><return_code><![CDATA[SUCCESS]]></return_code>\n" +
				"<return_msg><![CDATA[OK]]></return_msg>\n" +
				"<appid><![CDATA[wx273fe72f2db863ed]]></appid>\n" +
				"<mch_id><![CDATA[1228845802]]></mch_id>\n" +
				"<nonce_str><![CDATA[lCXjx3wNx45HfTV2]]></nonce_str>\n" +
				"<sign><![CDATA[68D7573E006F0661FD2A77BA59124E87]]></sign>\n" +
				"<result_code><![CDATA[SUCCESS]]></result_code>\n" +
				"<openid><![CDATA[oZyc_uPx_oed7b4q1yKmj_3M2fTU]]></openid>\n" +
				"<is_subscribe><![CDATA[N]]></is_subscribe>\n" +
				"<trade_type><![CDATA[NATIVE]]></trade_type>\n" +
				"<bank_type><![CDATA[CFT]]></bank_type>\n" +
				"<total_fee>1</total_fee>\n" +
				"<fee_type><![CDATA[CNY]]></fee_type>\n" +
				"<transaction_id><![CDATA[4008852001201608221983528929]]></transaction_id>\n" +
				"<out_trade_no><![CDATA[20160822162018]]></out_trade_no>\n" +
				"<attach><![CDATA[]]></attach>\n" +
				"<time_end><![CDATA[20160822202556]]></time_end>\n" +
				"<trade_state><![CDATA[SUCCESS]]></trade_state>\n" +
				"<cash_fee>1</cash_fee>\n" +
				"</xml>";
		try {
			System.out.println(xmlStr);
			System.out.println("+++++++++++++++++");
			System.out.println(WXPayUtil.isSignatureValid(xmlStr, config.getKey()));
			Map<String, String> hm = WXPayUtil.xmlToMap(xmlStr);
			System.out.println("+++++++++++++++++");
			System.out.println(hm);
			System.out.println(hm.get("attach").length());

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void testHelloWorld() throws Exception {
		for (int i=0; i<1000; ++i) {
			System.out.println("hi");
			Thread.sleep(1000);
		}
	}


	public static void main(String[] args) throws Exception {    	
       //测试商户扫码支付
		WXPayPerformance dodo = new WXPayPerformance();
		Map<String, String> r =dodo.doMicropayWithPos("11151521","测试","1","130226992988325162");
		if(r.isEmpty()){
			System.out.println("--------------->");
		}else{
			if(r.containsKey("err_code_des")){
				System.out.println("支付失败:"+r.get("err_code_des"));
			}else{
				if(r.get("return_msg").equals("OK")){
					System.out.println("支付成功："+r.get("err_code_des"));
				}
			}
		}
		 //测试app支付
		WXPayPerformance dodo1 = new WXPayPerformance();
		dodo1.doUnifiedOrder("12121", "1", "http://192.168.1.90:8080/textbook/app/pay/wxpay/notify");

	}
}


*/