package com.stylefeng.guns.rest.modular.system.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 定时任务数据
 * </p>
 *
 * @author lyouyou123
 * @since 2018-11-30
 */
@TableName("app_time_task")
public class TimeTask extends Model<TimeTask> {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    private String taskName;
    private String typeName;
    /**
     * 0有效，1无效
     */
    private Integer state;
    /**
     * 添加时间
     */
    private Date createDate;
    /**
     * 执行时间
     */
    private Date excuteDate;
    /**
     * 参数
     */
    private String map;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getExcuteDate() {
        return excuteDate;
    }

    public void setExcuteDate(Date excuteDate) {
        this.excuteDate = excuteDate;
    }

    public String getMap() {
        return map;
    }

    public void setMap(String map) {
        this.map = map;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "TimeTask{" +
        "id=" + id +
        ", taskName=" + taskName +
        ", typeName=" + typeName +
        ", state=" + state +
        ", createDate=" + createDate +
        ", excuteDate=" + excuteDate +
        ", map=" + map +
        "}";
    }
}
