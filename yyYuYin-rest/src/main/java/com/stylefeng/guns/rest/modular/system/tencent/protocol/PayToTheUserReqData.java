package com.stylefeng.guns.rest.modular.system.tencent.protocol;
/*package com.tencent.protocol;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import com.tencent.common.Configure;
import com.tencent.common.RandomStringGenerator;
import com.tencent.common.Signature;

*//**
 * 商户想用户付款（提现）
 *//*
public class PayToTheUserReqData {

	//每个字段具体的意思请查看API文档 https://pay.weixin.qq.com/wiki/doc/api/tools/mch_pay.php?chapter=14_2
	private String mch_appid;//公众账号appid		是	wx8888888888888888	String	微信分配的公众账号ID（企业号corpid即为此appId）
	private String mchid;//商户号		是	1900000109	String(32)	微信支付分配的商户号
	private String nonce_str;// 随机字符串		是	5K8264ILTKCH16CQ2502SI8ZNMTM67VS	String(32)	随机字符串，不长于32位
	private String sign;// 签名		是	C380BEC2BFD727A4B6845133519F3AD6	String(32)	签名，详见签名算法
	private String partner_trade_no;// 商户订单号		是	10000098201411111234567890	String	商户订单号，需保持唯一性
	private String openid;// 用户openid	是	oxTWIuGaIt6gTKsQRLau2M0yL16E	String	商户appid下，某用户的openid
	private String check_name;// 校验用户姓名选项	是	OPTION_CHECK	String	NO_CHECK：不校验真实姓名 	FORCE_CHECK：强校验真实姓名（未实名认证的用户会校验失败，无法转账）	OPTION_CHECK：针对已实名认证的用户才校验真实姓名（未实名认证用户不校验，可以转账成功）
	private String re_user_name;// 收款用户姓名		可选	马花花	String	收款用户真实姓名。 	如果check_name设置为FORCE_CHECK或OPTION_CHECK，则必填用户真实姓名
	private Integer amount;// 金额		是	10099	int	企业付款金额，单位为分
	private String desc;// 企业付款描述信息		是	理赔	String	企业付款操作说明信息。必填。
	private String spbill_create_ip = "127.0.0.1";// Ip地址		是	192.168.0.1	String(32)	调用接口的机器Ip地址

	*//**
	 * 企业向用户付款
	 * @param partner_trade_no 商户订单号，需保持唯一性
	 * @param openid 商户appid下，某用户的openid
	 * @param check_name 校验用户姓名选项：NO_CHECK：不校验真实姓名  FORCE_CHECK：强校验真实姓名（未实名认证的用户会校验失败，无法转账）OPTION_CHECK：针对已实名认证的用户才校验真实姓名（未实名认证用户不校验，可以转账成功）
	 * @param re_user_name 收款用户姓名
	 * @param amount 企业付款金额，单位为分
	 * @param desc 企业付款描述信息
	 * @param spbill_create_ip 调用接口的机器Ip地址
	 *//*
	public PayToTheUserReqData(String partner_trade_no, String openid, String re_user_name, Integer amount, String desc){

		//微信分配的公众号ID（开通公众号之后可以获取到）
		setMch_appid(Configure.getAppid());

		//微信支付分配的商户号ID（开通公众号的微信支付功能之后可以获取到）
		setMchid(Configure.getMchid());
		setPartner_trade_no(partner_trade_no);
		setOpenid(openid);
		setCheck_name("OPTION_CHECK");//默认：针对已实名认证的用户才校验真实姓名（未实名认证用户不校验，可以转账成功）
		//      setRe_user_name(re_user_name);
		setAmount(amount);
		setDesc(desc);

		//随机字符串，不长于32 位
		setNonce_str(RandomStringGenerator.getRandomStringByLength(32));

		//根据API给的签名规则进行签名
		String sign;
		try {
			sign = Signature.getSign(toMap());
			setSign(sign);//把签名数据设置到Sign这个属性中
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}

	}

	public String getMch_appid() {
		return mch_appid;
	}

	public void setMch_appid(String mch_appid) {
		this.mch_appid = mch_appid;
	}

	public String getMchid() {
		return mchid;
	}

	public void setMchid(String mchid) {
		this.mchid = mchid;
	}

	public String getNonce_str() {
		return nonce_str;
	}

	public void setNonce_str(String nonce_str) {
		this.nonce_str = nonce_str;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	public String getPartner_trade_no() {
		return partner_trade_no;
	}

	public void setPartner_trade_no(String partner_trade_no) {
		this.partner_trade_no = partner_trade_no;
	}

	public String getOpenid() {
		return openid;
	}

	public void setOpenid(String openid) {
		this.openid = openid;
	}

	public String getCheck_name() {
		return check_name;
	}

	public void setCheck_name(String check_name) {
		this.check_name = check_name;
	}

	public String getRe_user_name() {
		return re_user_name;
	}

	public void setRe_user_name(String re_user_name) {
		this.re_user_name = re_user_name;
	}

	public Integer getAmount() {
		return amount;
	}

	public void setAmount(Integer amount) {
		this.amount = amount;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getSpbill_create_ip() {
		return spbill_create_ip;
	}

	public void setSpbill_create_ip(String spbill_create_ip) {
		this.spbill_create_ip = spbill_create_ip;
	}


	public Map<String,Object> toMap(){
		Map<String,Object> map = new HashMap<String, Object>();
		Field[] fields = this.getClass().getDeclaredFields();
		for (Field field : fields) {
			Object obj;
			try {
				obj = field.get(this);
				if(obj!=null){
					map.put(field.getName(), obj);
				}
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		}
		return map;
	}

}
*/