package com.stylefeng.guns.rest.modular.system.dao;

import com.stylefeng.guns.rest.modular.system.model.SetSpeed;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 速配/认证/语句设置 Mapper 接口
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
public interface SetSpeedMapper extends BaseMapper<SetSpeed> {

	List<SetSpeed> getList(SetSpeed model);

	SetSpeed getOne(SetSpeed model);

}
