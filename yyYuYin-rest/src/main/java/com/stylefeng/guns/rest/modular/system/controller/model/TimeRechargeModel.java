package com.stylefeng.guns.rest.modular.system.controller.model;

import java.util.Date;
import java.util.List;

public class TimeRechargeModel {
	private Date createTime;

	private List<RechargeModel> Recharge;

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public List<RechargeModel> getRecharge() {
		return Recharge;
	}

	public void setRecharge(List<RechargeModel> recharge) {
		Recharge = recharge;
	}

}
