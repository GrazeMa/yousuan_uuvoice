package com.stylefeng.guns.rest.modular.system.service.impl;

import com.stylefeng.guns.rest.modular.system.model.PkNum;
import com.stylefeng.guns.rest.modular.system.dao.PkNumMapper;
import com.stylefeng.guns.rest.modular.system.service.IPkNumService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import java.util.List;

import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wumeng123
 * @since 2019-02-19
 */
@Service
public class PkNumServiceImpl extends ServiceImpl<PkNumMapper, PkNum> implements IPkNumService {

	@Override
	public List<PkNum> getList(PkNum model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getList(model);
	}

	@Override
	public PkNum getOne(PkNum model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getOne(model);
	}

}
