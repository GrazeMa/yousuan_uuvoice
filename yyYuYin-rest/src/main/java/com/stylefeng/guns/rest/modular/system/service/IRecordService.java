package com.stylefeng.guns.rest.modular.system.service;

import com.stylefeng.guns.rest.modular.system.model.Record;
import com.stylefeng.guns.rest.modular.system.model.Record;

import java.util.List;

import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 操作记录 服务类
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
public interface IRecordService extends IService<Record> {
public List<Record> getList(Record model);
    
    Record getOne(Record model);
}
