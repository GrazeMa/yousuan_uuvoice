package com.stylefeng.guns.rest.modular.system.task.base;

import com.stylefeng.guns.core.util.SpringContextHolder;
import com.stylefeng.guns.rest.modular.system.service.IPkService;
import com.stylefeng.guns.rest.modular.system.service.IRedNumService;
import com.stylefeng.guns.rest.modular.system.service.IRedService;
import com.stylefeng.guns.rest.modular.system.service.IRoomService;
import com.stylefeng.guns.rest.modular.system.service.ISetInfoService;
import com.stylefeng.guns.rest.modular.system.service.ITimeTaskService;
import com.stylefeng.guns.rest.modular.system.service.IUserService;


import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;


public abstract class AbstractJob implements Job{
	protected Logger logger = LoggerFactory.getLogger(getClass());
	
	@Override
	public abstract void execute(JobExecutionContext context) throws JobExecutionException;

        // 用户订单关联
		@Autowired
		protected IPkService PkService;
		
		//定时任务
		@Autowired
		protected ITimeTaskService TimeTaskService;

		//
		@Autowired
		protected IRoomService RoomService;
		
		@Autowired
		protected IRedNumService  RedNumService;
		
		@Autowired
		protected  IRedService    RedService;
		
		@Autowired
		protected  IUserService  UserService;
	


	
		
		

	public AbstractJob(){
		this.PkService = SpringContextHolder.getBean(IPkService.class);
		this.RoomService = SpringContextHolder.getBean(IRoomService.class);
		this.RedNumService = SpringContextHolder.getBean(IRedNumService.class);
		this.RedService = SpringContextHolder.getBean(IRedService.class);
		this.UserService = SpringContextHolder.getBean(IUserService.class);
		this.TimeTaskService=SpringContextHolder.getBean(ITimeTaskService.class);
	}

	 
}
