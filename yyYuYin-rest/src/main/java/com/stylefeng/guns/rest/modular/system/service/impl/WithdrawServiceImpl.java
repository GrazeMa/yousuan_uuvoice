package com.stylefeng.guns.rest.modular.system.service.impl;

import com.stylefeng.guns.rest.modular.system.model.Withdraw;
import com.stylefeng.guns.rest.modular.system.dao.WithdrawMapper;
import com.stylefeng.guns.rest.modular.system.service.IWithdrawService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import java.util.List;

import org.springframework.stereotype.Service;

/**
 * <p>
 * 提现管理 服务实现类
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
@Service
public class WithdrawServiceImpl extends ServiceImpl<WithdrawMapper, Withdraw> implements IWithdrawService {

	@Override
	public List<Withdraw> getList(Withdraw model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getList(model);
	}

	@Override
	public Withdraw getOne(Withdraw model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getOne(model);
	}

	@Override
	public List<Withdraw> getList1(Withdraw model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getList1(model);
	}

}
