package com.stylefeng.guns.rest.modular.system.service.impl;

import com.stylefeng.guns.rest.modular.system.model.Agreement;
import com.stylefeng.guns.rest.modular.system.dao.AgreementMapper;
import com.stylefeng.guns.rest.modular.system.service.IAgreementService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import java.util.List;

import org.springframework.stereotype.Service;

/**
 * <p>
 * 协议号 服务实现类
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
@Service
public class AgreementServiceImpl extends ServiceImpl<AgreementMapper, Agreement> implements IAgreementService {

	@Override
	public List<Agreement> getList(Agreement model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getList(model);
	}

	@Override
	public Agreement getOne(Agreement model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getOne(model);
	}

}
