package com.stylefeng.guns.rest.modular.system.dao;

import com.stylefeng.guns.rest.modular.system.model.NoticeInfo;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 系统公告 Mapper 接口
 * </p>
 *
 * @author wumeng
 * @since 2018-06-01
 */
public interface NoticeInfoMapper extends BaseMapper<NoticeInfo> {

	List<NoticeInfo> getList(NoticeInfo model);

	NoticeInfo getOne(NoticeInfo model);

}
