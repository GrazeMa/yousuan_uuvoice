package com.stylefeng.guns.rest.modular.system.controller.model;

import java.util.Date;
import java.util.List;

public class TimeGiftRecordModel {
	private Date createTime;

	private List<GiftRecordModel> GiftRecord;

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public List<GiftRecordModel> getGiftRecord() {
		return GiftRecord;
	}

	public void setGiftRecord(List<GiftRecordModel> giftRecord) {
		GiftRecord = giftRecord;
	}

}
