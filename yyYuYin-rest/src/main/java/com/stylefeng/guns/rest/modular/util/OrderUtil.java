package com.stylefeng.guns.rest.modular.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

/**
 * 订单处理工具类
 * 
 * @author taonb
 * @createDate 2016年5月27日
 * @version 1.0
 */
public class OrderUtil {

	/* 订单号 */
	private static long orderNum = 0l;

	/* 日期 */
	private static String date;

	/**
	 * 生成不重复的订单号 【纯数字】
	 * 
	 * @return
	 */
	public static synchronized String getOrderNo() {
		String str = new SimpleDateFormat("yyMMddHHmm").format(new Date());
		if (date == null || !date.equals(str)) {
			date = str;
			orderNum = 0l;
		}
		orderNum++;
		long orderNo = Long.parseLong((date)) * 10000;
		orderNo += orderNum;
		return orderNo + "";
	}

	/**
	 * 生成不重复的订单号 【含前缀】
	 * 
	 * @param prefix
	 * @return
	 */
	public static synchronized String getOrderNoForPrefix(String prefix) {
		String code="";
		for (int i = 0; i < 6; i++) {
            code += new Random().nextInt(10);
        }

		return prefix + getOrderNo()+code;
	}

	/**
	 * 生成不重复的订单号 【含后缀】
	 * 
	 * @param suffix
	 * @return
	 */
	public static synchronized String getOrderNoForSuffix(String suffix) {
		return getOrderNo() + suffix;
	}

	public static void main(String[] args) {
		// System.out.println(OrderUtil.getOrderNo());
		// System.out.println(OrderUtil.getOrderNo());
		// System.out.println(OrderUtil.getOrderNoForSuffix("LTG"));
		System.out.println(OrderUtil.getOrderNoForPrefix("LTG"));
		System.out.println(OrderUtil.getOrderNoForPrefix("LTG"));
		System.out.println(OrderUtil.getOrderNoForPrefix("LTG"));
		System.out.println(OrderUtil.getOrderNoForPrefix("LTG"));
		System.out.println(OrderUtil.getOrderNoForPrefix("LTG"));
	}
}
