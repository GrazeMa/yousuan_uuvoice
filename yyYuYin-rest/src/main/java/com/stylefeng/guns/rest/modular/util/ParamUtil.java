package com.stylefeng.guns.rest.modular.util;

import java.io.IOException;
import java.util.Properties;

/**
 * 配置信息
 * 
 * @author TZj
 * @data 2017年8月03日
 * @version 1.0
 */
public class ParamUtil {

	/**
	 * 获取配置信息
	 */
	private static Properties properties = new Properties();
	static{
		try {
			//获取properties文件
			properties.load(ParamUtil.class.getClassLoader().getResourceAsStream("conf/param.properties"));
		} catch (IOException e) {e.printStackTrace();}
	}
	
	/**
	 * 获取配置参数值
	 * 
	 * @param key
	 * @return
	 */
	public static String getValue(String key){		
		return (String)properties.get(key);
	}
	
}