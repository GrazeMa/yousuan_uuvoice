package com.stylefeng.guns.rest.modular.system.controller.model;

public class RedModel {
	private int grade;// 财富等级
	private String messageShow;// 消息内容
	private String name;// 名称
	private int uid;// 用户id

	public int getGrade() {
		return grade;
	}

	public void setGrade(int grade) {
		this.grade = grade;
	}

	public String getMessageShow() {
		return messageShow;
	}

	public void setMessageShow(String messageShow) {
		this.messageShow = messageShow;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getUid() {
		return uid;
	}

	public void setUid(int uid) {
		this.uid = uid;
	}

}
