package com.stylefeng.guns.rest.modular.system.dao;

import com.stylefeng.guns.rest.modular.system.model.RedNum;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 红包内容 Mapper 接口
 * </p>
 *
 * @author wumeng123
 * @since 2019-02-20
 */
public interface RedNumMapper extends BaseMapper<RedNum> {

	List<RedNum> getList(RedNum model);

	RedNum getOne(RedNum model);

}
