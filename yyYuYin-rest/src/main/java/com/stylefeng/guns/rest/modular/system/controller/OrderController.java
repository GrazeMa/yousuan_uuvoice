package com.stylefeng.guns.rest.modular.system.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.github.pagehelper.PageHelper;
import com.stylefeng.guns.rest.modular.system.controller.model.LotteryModel;
import com.stylefeng.guns.rest.modular.system.controller.model.OrderSkillModel;
import com.stylefeng.guns.rest.modular.system.controller.model.OrderUserModel;
import com.stylefeng.guns.rest.modular.system.controller.model.ServedUserModel;
import com.stylefeng.guns.rest.modular.system.model.Attention;
import com.stylefeng.guns.rest.modular.system.model.Order;
import com.stylefeng.guns.rest.modular.system.model.OrderSkill;
import com.stylefeng.guns.rest.modular.system.model.Paylog;
import com.stylefeng.guns.rest.modular.system.model.RechargeOrder;
import com.stylefeng.guns.rest.modular.system.model.SetRecharge;
import com.stylefeng.guns.rest.modular.system.model.Skill;
import com.stylefeng.guns.rest.modular.system.model.SkillLevel;
import com.stylefeng.guns.rest.modular.system.model.SkillPrice;
import com.stylefeng.guns.rest.modular.system.model.User;
import com.stylefeng.guns.rest.modular.system.service.IAttentionService;
import com.stylefeng.guns.rest.modular.system.service.IOrderService;
import com.stylefeng.guns.rest.modular.system.service.IOrderSkillService;
import com.stylefeng.guns.rest.modular.system.service.IPaylogService;
import com.stylefeng.guns.rest.modular.system.service.IRechargeOrderService;
import com.stylefeng.guns.rest.modular.system.service.ISkillLevelService;
import com.stylefeng.guns.rest.modular.system.service.ISkillPriceService;
import com.stylefeng.guns.rest.modular.system.service.ISkillService;
import com.stylefeng.guns.rest.modular.system.service.IUserService;
import com.stylefeng.guns.rest.modular.util.ApiUtil;
import com.stylefeng.guns.rest.modular.util.OrderUtil;
import com.stylefeng.guns.rest.modular.util.SinataUtil;

@Controller
@RequestMapping("api")
public class OrderController {
	Logger log = LoggerFactory.getLogger(getClass());
	
	// 'bout attention.
	@Autowired
	private IAttentionService iAttentionService;
	// 'bout order.
	@Autowired
	private IOrderService iOrderService;
	// 'bout order skill.
	@Autowired
	private IOrderSkillService iOrderSkillService;
	// 'bout recharge.
	@Autowired
	private IRechargeOrderService iRechargeOrderService;
	// 'bout skill name.
	@Autowired
	private ISkillService iSkillService;
	// 'bout skill level.
	@Autowired
	private ISkillLevelService iSkillLevelService;
	// 'bout skill price.
	@Autowired
	private ISkillPriceService iSkillPriceService;
	// 'bout user.
	@Autowired
	private IUserService iUserService;
	@Autowired
	private IPaylogService iPaylogService;

	// Get the names of skills.
	@RequestMapping("/skill/getSkillNames")
	@ResponseBody
	public Map<String, Object> getSkillNames() {
		try {
			// Initialization.
			List<Skill> skillNamesList = new ArrayList<>();
			Skill skillModel = new Skill();
			// Search all the names of skills.
			skillNamesList = iSkillService.getList(skillModel);
			return ApiUtil.returnedData(0, "", skillNamesList);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("获取技能名异常", e);
			return ApiUtil.returnedData(1, "获取技能名失败！！！", "");
		}
	}
	
	// Get the levels of skills.
	@RequestMapping("/skill/getSkillLevels")
	@ResponseBody
	public Map<String, Object> getSkillLevels(Integer skillType, Integer userId) {
		try {
			if (SinataUtil.isEmpty(userId)) return ApiUtil.returnedData(1, "用户ID不能为空！！！", "");
			if (3 == skillType) { // Male: 1, 98. Female: 2, 99.
				skillType = (1 == iUserService.selectById(userId).getSex()) ? 98: 99;
			}
			// Initialization.
			SkillLevel skillLevelModel = new SkillLevel();
			skillLevelModel.setSkillType(skillType);
			// Search all the levels of skills.
			List<SkillLevel> skillLevelsList = iSkillLevelService.getList(skillLevelModel);
			return ApiUtil.returnedData(0, "", skillLevelsList);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("获取技能段位异常", e);
			return ApiUtil.returnedData(1, "获取技能段位失败！！！", "");
		}
	}
	
	// Get the prices of skills. TODO...
	@RequestMapping("/skill/getSkillPrices")
	@ResponseBody
	public Map<String, Object> getSkillPrices() {
		try {
			// Initialization.
			List<SkillPrice> skillPricesList = new ArrayList<>();
			SkillPrice skillPriceModel = new SkillPrice();
			// Search all the prices of skills.
			skillPricesList = iSkillPriceService.getList(skillPriceModel);
			return ApiUtil.returnedData(0, "", skillPricesList);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("获取技能价格异常", e);
			return ApiUtil.returnedData(1, "获取技能价格失败！！！", "");
		}
	}
	
	// Get the skills of the user.
	@RequestMapping("/skill/getMySkills")
	@ResponseBody
	public Map<String, Object> getMySkills(Integer userId) {
		try {
			if (SinataUtil.isEmpty(userId)) return ApiUtil.returnedData(1, "用户ID不能为空！！！", "");
			OrderSkill orderSkill = new OrderSkill();
			orderSkill.setUid(userId);
			List<OrderSkill> orderSkillLists = iOrderSkillService.getList(orderSkill);
			List<OrderSkillModel> orderSkillModelLists = new ArrayList<>();
			
			// Use two maps to save the time. TODO...
			
			orderSkillLists.stream().forEach(eachOrderSkill -> {
				Skill skill = new Skill();
				Skill searchedSkill = skill.selectById(eachOrderSkill.getSkillId());
				
				SkillLevel skillLevel = new SkillLevel();
				SkillLevel searchedSkillLevel = skillLevel.selectById(eachOrderSkill.getSkillLevelId());
				
				OrderSkillModel orderSkillModel = new OrderSkillModel();
				orderSkillModel.setId(eachOrderSkill.getId());
				orderSkillModel.setSkillId(searchedSkill.getId());
				orderSkillModel.setSkillIcon(searchedSkill.getSkillIcon());
				orderSkillModel.setSkillName(searchedSkill.getSkillName());
				orderSkillModel.setSkillLevel(searchedSkillLevel.getSkillLevel());
				orderSkillModel.setSkillPrice(10); // TODO...
				orderSkillModelLists.add(orderSkillModel);
			});
			return ApiUtil.returnedData(0, "", orderSkillModelLists);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("获取当前用户技能异常", e);
			return ApiUtil.returnedData(1, "获取当前用户技能失败！！！", "");
		}
	}
	
	// Add a skill to the user.
	@RequestMapping("/skill/addOneSkill")
	@ResponseBody
	public Map<String, Object> addOneSkill(Integer userId, Integer skillNameId, Integer skillLevelId, Integer skillPriceId) {
		try {
			if (SinataUtil.isEmpty(userId)) return ApiUtil.returnedData(1, "用户ID不能为空！！！", "");
			
			OrderSkill searchedOrderSkillModel = new OrderSkill();
			searchedOrderSkillModel.setUid(userId);
			searchedOrderSkillModel.setSkillId(skillNameId);
			OrderSkill ifEmptyOrderSkillModel = iOrderSkillService.getOne(searchedOrderSkillModel);
			if (SinataUtil.isEmpty(ifEmptyOrderSkillModel)) {
				OrderSkill orderSkillModel = new OrderSkill();
				orderSkillModel.setUid(userId);
				orderSkillModel.setSkillId(skillNameId);
				orderSkillModel.setSkillLevelId(skillLevelId);
				orderSkillModel.setSkillPriceId(1);
//				orderSkillModel.setSkillPriceId(skillPriceId);
				orderSkillModel.insert();
				return ApiUtil.returnedData(0, "插入技能成功。", "");
			} else {
				return ApiUtil.returnedData(96, "您已经添加过此技能。", "");
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error("插入技能异常", e);
			return ApiUtil.returnedData(1, "插入技能失败！！！", "");
		}
	}
	
	// Modify the price of the skill.
	@RequestMapping("/skill/modifySkillPrice")
	@ResponseBody
	public Map<String, Object> modifySkillPrice(Integer userId, Integer skillNameId, Integer skillPriceId) {
		try {
			if (SinataUtil.isEmpty(userId)) return ApiUtil.returnedData(1, "用户ID不能为空！！！", "");
			if (SinataUtil.isEmpty(skillNameId)) return ApiUtil.returnedData(1, "技能ID不能为空！！！", "");
			if (SinataUtil.isEmpty(skillPriceId)) return ApiUtil.returnedData(1, "价格ID不能为空！！！", "");
			OrderSkill searchedOrderSkill = new OrderSkill();
			searchedOrderSkill.setUid(userId);
			searchedOrderSkill.setSkillId(skillNameId);
			OrderSkill ifEmptyOrderSkill = iOrderSkillService.getOne(searchedOrderSkill);
			if (SinataUtil.isEmpty(ifEmptyOrderSkill)) return ApiUtil.returnedData(1, "获取技能失败！！！", "");
			OrderSkill updatedOrderSkill = new OrderSkill();
			updatedOrderSkill.setSkillPriceId(skillPriceId);
			iOrderSkillService.updateById(updatedOrderSkill);
			return ApiUtil.returnedData(0, "技能价格修改成功。", "");
		} catch (Exception e) {
			e.printStackTrace();
			log.error("技能价格修改异常", e);
			return ApiUtil.returnedData(1, "技能价格修改失败！！！", "");
		}
	}
	
	// Get the orders of the user.
	@RequestMapping("/order/getMyOrders")
	@ResponseBody
	public Map<String, Object> getMyOrders(Integer userId, Integer type, Integer status, Integer pageNum, Integer pageSize) {
		try {
			if (SinataUtil.isEmpty(userId)) return ApiUtil.returnedData(1, "用户ID不能为空！！！", "");
			PageHelper.startPage(pageNum, pageSize, false);
			Order searchedOrder = new Order();
			if (1 == type) { // 我的下单。
				searchedOrder.setOrderUserId(userId);
			} else { // 我的接单。
				searchedOrder.setServedUserId(userId);
			}
			if (0 != status) searchedOrder.setStatus(status);
			List<Order> orderList = iOrderService.getList(searchedOrder);
			
			Skill skill = new Skill();
			List<Skill> skillLists = iSkillService.getList(skill);
			Map<Integer, String> skillHashMap = new HashMap<>();
			
			for (int i = 0; i < skillLists.size(); i++) {
				skillHashMap.put(skillLists.get(i).getId(), skillLists.get(i).getSkillName());
			}
			
			if (SinataUtil.isNotEmpty(orderList)) {
				for (int i = 0; i < orderList.size(); i++) {
					orderList.get(i).setSkillName(skillHashMap.get(iOrderSkillService.selectById(orderList.get(i).getOrderSkillId()).getSkillId()));
					User user = new User();
					if (1 == type) { // 我的下单。
						user = iUserService.selectById(orderList.get(i).getServedUserId());
					} else { // 我的接单。
						user = iUserService.selectById(orderList.get(i).getOrderUserId());
					}
					orderList.get(i).setUsername(user.getNickname());
					orderList.get(i).setUserAvatarUrl(user.getImgTx());
					
					RechargeOrder searchedRechargeOrder = new RechargeOrder();
					searchedRechargeOrder.setGeneratedOrderNumber(orderList.get(i).getOrderNumber());
					String payOrderNumberString = iRechargeOrderService.getOne(searchedRechargeOrder).getOrderNum();
					orderList.get(i).setPayOrderNumber(payOrderNumberString);
				}
			}
			
			return ApiUtil.returnedData(0, "获取我的订单成功。", orderList);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("插入技能异常", e);
			return ApiUtil.returnedData(1, "获取我的订单失败！！！", "");
		}
	}
	
	// Modify the status of the order.
	@RequestMapping("/order/modifyOrderStatus")
	@ResponseBody
	public Map<String, Object> modifyOrderStatus(Integer userId, Integer orderId, Integer status) {
		try {
			if (SinataUtil.isEmpty(userId)) return ApiUtil.returnedData(1, "用户ID不能为空！！！", "");
			if (SinataUtil.isEmpty(orderId)) return ApiUtil.returnedData(1, "订单ID不能为空！！！", "");
			Order updatedOrder = new Order();
			updatedOrder.setId(orderId);
			updatedOrder.setStatus(status);
			if (6 == status) {
				if ((int)userId == (int)iOrderService.selectById(orderId).getOrderUserId()) {
					updatedOrder.setCancelReason(3);
				} else {
					updatedOrder.setCancelReason(1);
				}
				
				if (SinataUtil.isNotEmpty(iOrderService.selectById(orderId).getPayLogId())) {
					Paylog paylog = iPaylogService.selectById(iOrderService.selectById(orderId).getPayLogId());
					if (1 == paylog.getPayType()) {
						PayUtil.refundForAlipay(paylog.getOutTradeNo(), paylog.getTradeNo(), paylog.getPayMoney());
					}
					
					if (2 == paylog.getPayType()) {
						PayUtil.refundForWxpay(1, paylog.getBuyerId(), paylog.getOutTradeNo(), paylog.getOutTradeNo(), (int)(paylog.getPayMoney() * 100), (int)(paylog.getPayMoney() * 100), "2");
					}
				} else {
					User updatedUser = iUserService.selectById(iOrderService.selectById(orderId).getOrderUserId());
					updatedUser.setGold((int)updatedUser.getGold() + (int)(iOrderService.selectById(orderId).getAmount() * iOrderService.selectById(orderId).getOrderCount() * 10));
					iUserService.updateById(updatedUser);
				}
			}
			if (4 == status) {
				User updatedUser = iUserService.selectById(iOrderService.selectById(orderId).getServedUserId());
				updatedUser.setYnum(updatedUser.getYnum() + (int)(iOrderService.selectById(orderId).getAmount() * iOrderService.selectById(orderId).getOrderCount() * 10 * 0.9));
				iUserService.updateById(updatedUser);
			}
			iOrderService.updateById(updatedOrder);
			return ApiUtil.returnedData(0, "修改订单状态成功。", "");
		} catch (Exception e) {
			e.printStackTrace();
			log.error("修改订单状态异常", e);
			return ApiUtil.returnedData(1, "修改订单状态失败！！！", "");
		}
	}
	
	// Show detail of some order.
	@RequestMapping("/order/showOrderDetail")
	@ResponseBody
	public Map<String, Object> showOrderDetail(Integer orderId, Integer userId) {
		try {
			if (SinataUtil.isEmpty(orderId)) return ApiUtil.returnedData(1, "订单ID不能为空！！！", "");
			if (SinataUtil.isEmpty(userId)) return ApiUtil.returnedData(1, "用户ID不能为空！！！", "");
			Order ifEmptyOrder = iOrderService.selectById(orderId);
			if (SinataUtil.isEmpty(ifEmptyOrder)) {
				return ApiUtil.returnedData(1, "无此订单，请重试！！！", "");
			} else {
				int userIdInt = ((int)userId == (int)ifEmptyOrder.getOrderUserId()) ? ifEmptyOrder.getServedUserId() : ifEmptyOrder.getOrderUserId();
				User searchedUser = iUserService.selectById(userIdInt);
				ifEmptyOrder.setUsername(searchedUser.getNickname());
				ifEmptyOrder.setUserAvatarUrl(searchedUser.getImgTx());
				int remainingTimeSeconds = 0;
				if(5 == ifEmptyOrder.getStatus()) {
					long currentTime = new Date().getTime();
					long orderTime = ifEmptyOrder.getCreateTime().getTime();
					long differTime = currentTime - orderTime;
					if(differTime < (15 * 60 * 1000)) remainingTimeSeconds = 900 - (int)(differTime / 1000);
				}
				ifEmptyOrder.setRemainingTime(remainingTimeSeconds);
				ifEmptyOrder.setSkillName(iSkillService.selectById(iOrderSkillService.selectById(ifEmptyOrder.getOrderSkillId()).getSkillId()).getSkillName());
				ifEmptyOrder.setFee(ifEmptyOrder.getAmount() * 0.1);

				RechargeOrder searchedRechargeOrder = new RechargeOrder();
				searchedRechargeOrder.setGeneratedOrderNumber(ifEmptyOrder.getOrderNumber());
				String payOrderNumberString = iRechargeOrderService.getOne(searchedRechargeOrder).getOrderNum();
				ifEmptyOrder.setPayOrderNumber(payOrderNumberString);
				
				return ApiUtil.returnedData(0, "显示订单详情成功。", ifEmptyOrder);
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error("显示订单详情异常", e);
			return ApiUtil.returnedData(1, "显示订单详情失败！！！", "");
		}
	}
	
	// Add one order.
	@RequestMapping("/order/addOneOrder")
	@ResponseBody
	public Map<String, Object> addOneOrder(Integer userId, Integer orderSkillId, Integer servedUserId, Long serviceTime, Integer orderCount, Integer amount, Integer type) {
		try {
			if (SinataUtil.isEmpty(userId)) return ApiUtil.returnedData(1, "用户ID不能为空！！！", "");
			String generatedOrderNumberString = ControllerUtils.S_GenerateOrderNumber();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date beginTime = new Date(serviceTime);
			String serviceBeginTime = sdf.format(beginTime);
			serviceTime += (30 * 60 * 1000) * orderCount;
			Date endTime = new Date(serviceTime);
			String serviceEndTime = sdf.format(endTime);
			
			Order insertedOrder = new Order();
			insertedOrder.setStatus(5);
			insertedOrder.setOrderNumber(generatedOrderNumberString);
			insertedOrder.setOrderUserId(userId);
			insertedOrder.setOrderSkillId(orderSkillId);
			insertedOrder.setServedUserId(servedUserId);
			insertedOrder.setServiceBeginTime(sdf.parse(serviceBeginTime));
			insertedOrder.setServiceEndTime(sdf.parse(serviceEndTime));
			insertedOrder.setType(type);
			insertedOrder.setAmount(amount);
			insertedOrder.setOrderCount(orderCount);
			insertedOrder.setFee(amount * orderCount * 0.1);
			insertedOrder.insert();
			
			// 支付。
			RechargeOrder insertedRechargeOrder = new RechargeOrder();
			insertedRechargeOrder.setCreateTime(new Date());
			String orderNumberString = OrderUtil.getOrderNoForPrefix("ORDER");
			insertedRechargeOrder.setOrderNum(orderNumberString);
			insertedRechargeOrder.setPayment(amount * orderCount * 1.0);
			insertedRechargeOrder.setUid(userId);
			insertedRechargeOrder.setPresented(0);
			insertedRechargeOrder.setRid(101);
			insertedRechargeOrder.setIsState(2);
			insertedRechargeOrder.setGiveMoney(0);
			insertedRechargeOrder.setType(1);
			insertedRechargeOrder.setGeneratedOrderNumber(generatedOrderNumberString);
			iRechargeOrderService.insert(insertedRechargeOrder);
			
			Map returnedHashMap = new HashMap<>();
			returnedHashMap.put("Order", insertedRechargeOrder.getOrderNum());
			returnedHashMap.put("pay", insertedRechargeOrder.getPayment());
			returnedHashMap.put("id", insertedRechargeOrder.getId());
			returnedHashMap.put("orderNumber", generatedOrderNumberString);
			
			return ApiUtil.returnedData(0, "插入订单成功。", returnedHashMap);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("插入订单异常", e);
			return ApiUtil.returnedData(1, "插入订单失败！！！", "");
		}
	}
	
	// Show the served user's list.
	@RequestMapping("/order/showServedUser")
	@ResponseBody
	public Map<String, Object> showServedUser(Integer type, Integer pageNum, Integer pageSize) {
		try {
			PageHelper.startPage(pageNum, pageSize, false);
			List<OrderSkill> servedUserLists = iOrderSkillService.getServedUserList();
			StringBuilder servedUserStringBuilder = new StringBuilder();
			if (SinataUtil.isNotEmpty(servedUserLists)) {
				for (int i = 0; i < servedUserLists.size(); i++) {
					servedUserStringBuilder.append(servedUserLists.get(i).getUid()).append(",");
				}
			}
			if (0 == servedUserStringBuilder.length()) {
				servedUserStringBuilder.append("GRAZER");
			}
			EntityWrapper<User> userEntityWrapper = new EntityWrapper<>();
			userEntityWrapper.in("id", servedUserStringBuilder.toString());
			List<User> userLists = iUserService.selectList(userEntityWrapper);
			List<ServedUserModel> returnedServedUserLists = new ArrayList<>();
			if (SinataUtil.isNotEmpty(userLists)) {
				
				Skill skill = new Skill();
				List<Skill> skillLists = iSkillService.getList(skill);
				Map<Integer, String> skillHashMap = new HashMap<>();
				
				for (int i = 0; i < skillLists.size(); i++) {
					skillHashMap.put(skillLists.get(i).getId(), skillLists.get(i).getSkillName());
				}
				
				for (int i = 0; i < userLists.size(); i++) {
					ServedUserModel servedUserModel = new ServedUserModel();
					int userIdInt = userLists.get(i).getId();
					servedUserModel.setUserId(userIdInt);
					servedUserModel.setUsercoding(userLists.get(i).getUsercoding());
					servedUserModel.setName(userLists.get(i).getNickname());
					servedUserModel.setSex(userLists.get(i).getSex());
					servedUserModel.setAvatarUrl(userLists.get(i).getImgTx());
					servedUserModel.setIndividuation(userLists.get(i).getIndividuation());
					
					EntityWrapper<Order> orderEntityWrapper = new EntityWrapper<>();
					orderEntityWrapper.eq("servedUserId", userIdInt);
					orderEntityWrapper.eq("status", 4);
					int orderNumberInt = iOrderService.selectCount(orderEntityWrapper);
					
					EntityWrapper<OrderSkill> orderSkillEntityWrapper = new EntityWrapper<>();
					orderSkillEntityWrapper.eq("uid", userIdInt);
					List<OrderSkill> orderSkillLists = iOrderSkillService.selectList(orderSkillEntityWrapper);
					
					StringBuilder skillNamesStringBuilder = new StringBuilder();
					if (SinataUtil.isNotEmpty(orderSkillLists)) {
						for (int j = 0; j < orderSkillLists.size(); j++) {
							skillNamesStringBuilder.append(skillHashMap.get(orderSkillLists.get(j).getSkillId())).append(",");
						}
					}
					
					servedUserModel.setOrderNumber(orderNumberInt);
					servedUserModel.setSkillName(skillNamesStringBuilder.toString());
					returnedServedUserLists.add(servedUserModel);
				}
				
				if (1 == type) { // 猜你喜欢。
					Collections.sort(returnedServedUserLists, new Comparator<ServedUserModel>() {
						public int compare(ServedUserModel o1, ServedUserModel o2) {
							return o2.getOrderNumber().compareTo(o1.getOrderNumber());
						}
					});
				}
				
				if (2 == type) { // 新人推荐。
					Collections.sort(returnedServedUserLists, new Comparator<ServedUserModel>() {
						public int compare(ServedUserModel o1, ServedUserModel o2) {
							return o2.getUserId().compareTo(o1.getUserId());
						}
					});
				}
				
			}
			
			return ApiUtil.returnedData(0, "显示陪练精英列表成功。", returnedServedUserLists);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("显示陪练精英列表异常", e);
			return ApiUtil.returnedData(1, "显示陪练精英列表失败！！！", "");
		}
	}
	
	// Get the information of the user.
	@RequestMapping("/order/getOrderUserInfo")
	@ResponseBody
	public Map<String, Object> getOrderUserInfo(Integer userId, Integer servedUserId) {
		try {
			if (SinataUtil.isEmpty(userId)) return ApiUtil.returnedData(1, "用户ID不能为空！！！", "");
			if (SinataUtil.isEmpty(servedUserId)) return ApiUtil.returnedData(1, "被点用户ID不能为空！！！", "");
			User searchedUser = iUserService.selectById(servedUserId);
			OrderUserModel returnedOrderUserModel = new OrderUserModel();
			returnedOrderUserModel.setId(searchedUser.getId());
			returnedOrderUserModel.setUsercoding(searchedUser.getUsercoding());
			returnedOrderUserModel.setName(searchedUser.getNickname());
			returnedOrderUserModel.setSex(searchedUser.getSex());
			returnedOrderUserModel.setUserAvaterUrl(searchedUser.getImgTx());
			returnedOrderUserModel.setCharmGrade(searchedUser.getCharmGrade());
			returnedOrderUserModel.setTreasureGrade(searchedUser.getTreasureGrade());
			returnedOrderUserModel.setIndividuation(searchedUser.getIndividuation());
			returnedOrderUserModel.setVoice(searchedUser.getVoice());
			returnedOrderUserModel.setVoiceTime(searchedUser.getVoiceTime());
			
			Attention searchedAttention = new Attention();
			searchedAttention.setUid(userId);
			searchedAttention.setBuid(servedUserId);
			Attention ifEmptyAttention = iAttentionService.getOne(searchedAttention);
			if (SinataUtil.isEmpty(ifEmptyAttention)) { // 0：未关注；1：已关注。
				returnedOrderUserModel.setIsAttention(0);
			} else {
				returnedOrderUserModel.setIsAttention(1);
			}
			return ApiUtil.returnedData(0, "获取派单首页用户信息成功。", returnedOrderUserModel);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("获取派单首页用户信息异常", e);
			return ApiUtil.returnedData(1, "获取派单首页用户信息失败！！！", "");
		}
	}
	
	// Get the banner of the order page.
	@RequestMapping("/order/getOrderBanner")
	@ResponseBody
	public Map<String, Object> getOrderBanner(Integer type) {
		try {
			
			
			return ApiUtil.returnedData(0, "获取派单首页 Banner 成功。", "");
		} catch (Exception e) {
			e.printStackTrace();
			log.error("获取派单首页 Banner 异常", e);
			return ApiUtil.returnedData(1, "获取派单首页 Banner 失败！！！", "");
		}
	}
	
	// Get the needed information after payment.
	@RequestMapping("/order/getInfoAfterPayment")
	@ResponseBody
	public Map<String, Object> getInfoAfterPayment(String orderNumber) {
		try {
			if (SinataUtil.isEmpty(orderNumber)) return ApiUtil.returnedData(1, "订单号不能为空！！！", "");
			Order searchedOrder = new Order();
			searchedOrder.setOrderNumber(orderNumber);
			Order ifEmptyOrder = iOrderService.getOne(searchedOrder);
			if (SinataUtil.isEmpty(ifEmptyOrder)) {
				return ApiUtil.returnedData(1, "无此订单，请重试！！！", "");
			} else {
				User searchedUser = iUserService.selectById(ifEmptyOrder.getServedUserId());
				ifEmptyOrder.setUsername(searchedUser.getNickname());
				ifEmptyOrder.setUserAvatarUrl(searchedUser.getImgTx());
				ifEmptyOrder.setSkillName(iSkillService.selectById(iOrderSkillService.selectById(ifEmptyOrder.getOrderSkillId()).getSkillId()).getSkillName());
				ifEmptyOrder.setSkillLevel(iSkillLevelService.selectById(iOrderSkillService.selectById(ifEmptyOrder.getOrderSkillId()).getSkillLevelId()).getSkillLevel());
				ifEmptyOrder.setFee(ifEmptyOrder.getAmount() * 0.1);
				return ApiUtil.returnedData(0, "显示支付成功详情成功。", ifEmptyOrder);
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error("显示支付成功详情异常", e);
			return ApiUtil.returnedData(1, "显示支付成功详情失败！！！", "");
		}
	}

	
}
