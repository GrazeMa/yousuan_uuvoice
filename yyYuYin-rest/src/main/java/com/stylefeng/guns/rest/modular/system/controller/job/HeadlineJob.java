package com.stylefeng.guns.rest.modular.system.controller.job;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.stylefeng.guns.rest.modular.system.model.Headline;
import com.stylefeng.guns.rest.modular.system.service.IHeadlineService;
import com.stylefeng.guns.rest.modular.util.SinataUtil;

@Component
@EnableScheduling
public class HeadlineJob {

	Logger log = LoggerFactory.getLogger(this.getClass());

	// 'bout headline.
	@Autowired
	private IHeadlineService iHeadlineService;

	@Scheduled(cron = "0 0/5 * * * ?")
	public void changeHeadlineStatus() {
		System.out.println("changeHeadlineStatus begins.");
		try {
			Headline headline = new Headline();
			headline.setStatus(0);
			Headline searchedHeadline = iHeadlineService.getOne2(headline);
			if (!SinataUtil.isEmpty(searchedHeadline)) {
				searchedHeadline.setStatus(1);
				iHeadlineService.updateById(searchedHeadline);
			}

			Headline searchedH = iHeadlineService.getOne2(headline);
			if (!SinataUtil.isEmpty(searchedH)) {
				searchedH.setRemainingTime(0);
				searchedH.setUpdateTime(new Date());
				iHeadlineService.updateById(searchedH);
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error("修改头条状态异常", e.getMessage());
		}
	}

}
