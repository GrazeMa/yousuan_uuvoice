package com.stylefeng.guns.rest.modular.system.controller;

import java.text.DateFormat;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.stylefeng.guns.rest.modular.system.service.IAuctionService;
import com.stylefeng.guns.rest.modular.system.service.IAuditService;
import com.stylefeng.guns.rest.modular.system.service.IBroadService;
import com.stylefeng.guns.rest.modular.system.service.IChatroomsService;
import com.stylefeng.guns.rest.modular.system.service.IDivideService;
import com.stylefeng.guns.rest.modular.system.service.IInviteService;
import com.stylefeng.guns.rest.modular.system.service.ISetShareService;
import com.stylefeng.guns.rest.modular.system.service.ISetSpeedService;
import com.stylefeng.guns.rest.modular.system.service.IUserService;
import com.stylefeng.guns.rest.modular.system.service.IUserlistService;
import com.stylefeng.guns.rest.modular.system.service.IWithdrawService;
import com.stylefeng.guns.rest.modular.util.ApiUtil;
import com.stylefeng.guns.rest.modular.util.HttpRequestUtil;
import com.stylefeng.guns.rest.modular.util.SinataUtil;

import net.sf.json.JSONObject;

import com.github.pagehelper.PageHelper;
import com.stylefeng.guns.rest.modular.system.controller.model.BroadModel;
import com.stylefeng.guns.rest.modular.system.controller.model.InviteModel;
import com.stylefeng.guns.rest.modular.system.controller.model.UserModel;
import com.stylefeng.guns.rest.modular.system.model.Audit;
import com.stylefeng.guns.rest.modular.system.model.Broad;
import com.stylefeng.guns.rest.modular.system.model.Chatrooms;
import com.stylefeng.guns.rest.modular.system.model.Divide;
import com.stylefeng.guns.rest.modular.system.model.Invite;
import com.stylefeng.guns.rest.modular.system.model.Room;
import com.stylefeng.guns.rest.modular.system.model.SetShare;
import com.stylefeng.guns.rest.modular.system.model.SetSpeed;
import com.stylefeng.guns.rest.modular.system.model.SetWithdraw;
import com.stylefeng.guns.rest.modular.system.model.User;
import com.stylefeng.guns.rest.modular.system.model.Userlist;
import com.stylefeng.guns.rest.modular.system.model.Withdraw;
//发现模块
@Controller
@RequestMapping("api")
public class DiscoverController {
	Logger log = LoggerFactory.getLogger(getClass());

	// 房间
	@Autowired
	private IBroadService BroadService;
    //用户信息
	@Autowired
	private IUserService userService;
   //实名认证
	@Autowired
	private IAuditService AuditService;
   //分享赠送
	@Autowired
	private ISetShareService SetShareService;
	 //邀请用户
	@Autowired
	private IInviteService InviteService;
	 //提现
	@Autowired
	private IWithdrawService WithdrawService;
	 //邀请排行
	@Autowired
	private IUserlistService UserlistService;
	 //充值金额分成
	@Autowired
	private IDivideService DivideService;

	// 房间
	@Autowired
	private IChatroomsService IChatroomsService;
	 //速配/认证/语句设置/是否开放推荐位
	@Autowired
	private ISetSpeedService SetSpeedService;

	// 广播交友
	@RequestMapping("/Discover/Broad")
	@ResponseBody
	public Map<String, Object> Broad(Integer uid, String content) {

		try {
			Map map = new HashMap<>();
			Chatrooms cs = new Chatrooms();
			cs.setUid(uid);
			cs.setState(2);
			Chatrooms one = IChatroomsService.getOne(cs);
//			if (SinataUtil.isEmpty(one)) {
//				map.put("state", 1);
//			} else {

				Broad b = new Broad();
				User user = userService.selectById(uid);
				b.setContent(content);
				b.setCreateTime(new Date());
				b.setEid(user.getUsercoding());
				b.setName(user.getNickname());
				b.setUid(uid);
				BroadService.insert(b);

				map.put("state", 2);
//			}

			return ApiUtil.returnArray(map);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("获取用户信息异常", e);
			return ApiUtil.putFailArray("获取广播交友失败");
		}
	}

	// 广播交友最新20条
	@RequestMapping("/Discover/BroadList")
	@ResponseBody
	public Map<String, Object> BroadList() {

		try {

			List<BroadModel> br = new ArrayList<>();
			Broad b = new Broad();
			List<Broad> list = BroadService.getList(b);
			Iterator<Broad> iterator = list.iterator();
			while (iterator.hasNext()) {
				Broad next = iterator.next();
				BroadModel bro = new BroadModel();
				User user = userService.selectById(next.getUid());
				bro.setCharm(user.getCharmGrade());
				bro.setGrade(user.getTreasureGrade());
				bro.setHeader(user.getImgTx());
				bro.setMessageShow(next.getContent());
				bro.setName(user.getNickname());
				bro.setSex(user.getSex());
				bro.setUid(user.getId());

				br.add(bro);
			}
			return ApiUtil.returnArray(br);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("获取用户信息异常", e);
			return ApiUtil.putFailArray("获取广播交友失败");
		}
	}

	// 实名认证
	@RequestMapping("/Discover/Addaudit")
	@ResponseBody
	public Map<String, Object> Addaudit(Integer uid, String name, String card, String photoUrl) {

		try {

			SetSpeed selectById = SetSpeedService.selectById(2);
			if (selectById.equals(1)) {
				Map<String, String> map = new LinkedHashMap();
				map.put("key", "c9bdfa3191810d70c0efca2328ba6e0c");
				map.put("idcard", card);
				map.put("realname", name);

				String s = HttpRequestUtil.postRequest("http://op.juhe.cn/idcard/query", map);
				JSONObject fromObject = JSONObject.fromObject(s);
				Object object = fromObject.get("result");
				JSONObject fromObject2 = JSONObject.fromObject(object);
				Object object1 = fromObject2.get("res");
				if (object1.equals(2)) {
					return ApiUtil.putFailArray("身份证号码和姓名不匹配 ");
				} else {
					Audit au = new Audit();
					User user = userService.selectById(uid);
					au.setCreateTime(new Date());
					au.setEid(user.getUsercoding());
					au.setName(user.getNickname());
					au.setXm(name);
					au.setUid(uid);
					au.setCard(card);
					au.setState("2");
					au.setIsDelete(1);
					au.setPhotoUrl(photoUrl);
					AuditService.insert(au);
				}
			} else {
				Audit au = new Audit();
				User user = userService.selectById(uid);
				au.setCreateTime(new Date());
				au.setEid(user.getUsercoding());
				au.setName(user.getNickname());
				au.setXm(name);
				au.setUid(uid);
				au.setCard(card);
				au.setState("1");
				au.setIsDelete(1);
				au.setPhotoUrl(photoUrl);
				AuditService.insert(au);
			}

			return ApiUtil.putSuccessObj("成功");
		} catch (Exception e) {
			e.printStackTrace();
			log.error("获取用户信息异常", e);
			return ApiUtil.putFailArray("获取实名认证失败");
		}
	}

	// 实名认证列表
	@RequestMapping("/Discover/audit")
	@ResponseBody
	public Map<String, Object> audit(Integer uid) {

		try {
			Map map = new HashMap<>();
			Audit au = new Audit();
			au.setUid(uid);
			Audit audit = AuditService.getOne(au);
			if (SinataUtil.isNotEmpty(audit)) {
				map.put("state", audit.getState());
			} else {
				map.put("state", 0);
			}
			return ApiUtil.returnArray(map);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("获取用户信息异常", e);
			return ApiUtil.putFailArray("获取实名认证列表失败");
		}
	}

	// 我的邀请奖励页面
	@RequestMapping("/Discover/invite")
	@ResponseBody
	public Map<String, Object> invite(Integer uid) {

		try {
			Map map = new HashMap<>();
			User user = userService.selectById(uid);
			map.put("money", user.getUserMoney());
			SetShare share = SetShareService.selectById(4);
			map.put("txMoney", share.getX());

			map.put("manNum", user.getInviteCount());

			map.put("fcMoney", user.getLsdivideAward());

			return ApiUtil.returnArray(map);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("获取用户信息异常", e);
			return ApiUtil.putFailArray("获取我的邀请奖励");
		}
	}

	// 获取邀记录排行
	@RequestMapping("/User/userList")
	@ResponseBody
	public Map<String, Object> userList(Integer uid) {
		try {
			Map map = new HashMap<>();
			User user = userService.selectById(uid);
			UserModel u = new UserModel();
			u.setName(user.getNickname());
			u.setImg(user.getImgTx());
			u.setMoney(user.getInviteAward());
			u.setSex(user.getSex());
			u.setId(user.getId());
			Userlist ul = new Userlist();
			ul.setUid(uid);
			Userlist one = UserlistService.getOne(ul);
			if (SinataUtil.isNotEmpty(one)) {
				u.setNum(one.getNum());
			} else {
				u.setNum(0);
			}

			map.put("user", u);

			List<UserModel> us = new ArrayList<>();
			Userlist usl = new Userlist();
			List<Userlist> list = UserlistService.getList(usl);
			Iterator<Userlist> iterator = list.iterator();
			while (iterator.hasNext()) {
				Userlist userlist = iterator.next();
				UserModel um = new UserModel();
				User user1 = userService.selectById(userlist.getUid());
				um.setName(user1.getNickname());
				um.setImg(user1.getImgTx());
				um.setMoney(user1.getInviteAward());
				um.setId(user1.getId());
				um.setSex(user1.getSex());
				um.setNum(0);
				us.add(um);
			}
			map.put("userList", us);

			return ApiUtil.returnArray(map);
		} catch (Exception e) {
			e.printStackTrace();
			return ApiUtil.putFailObj("获取邀记录排行失败");
		}
	}

	// 我的邀请奖励
	@RequestMapping("/Discover/getUserInvite")
	@ResponseBody
	public Map<String, Object> getlocal(Integer uid, int pageNum, int pageSize) {

		try {

			Map map = new HashMap<>();

			List<InviteModel> u = new ArrayList<>();

			User user = userService.selectById(uid);
			Invite i = new Invite();
			DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			i.setBeginTime(format.format(new Date()));
			i.setEndTime(format.format(new Date()));
			i.setUid(uid);
			Integer count = InviteService.getCount(i);
			map.put("today", count);

			map.put("sum", user.getInviteCount());

			map.put("sumMoney", user.getInviteAward());

			Invite in = new Invite();
			in.setUid(uid);
			PageHelper.startPage(pageNum, pageSize, false);
			List<Invite> list = InviteService.getList(in);
			Iterator<Invite> iterator = list.iterator();
			while (iterator.hasNext()) {
				Invite invite = iterator.next();
				User user2 = userService.selectById(invite.getBuid());
				InviteModel im = new InviteModel();
				im.setName("你邀请“" + user2.getNickname() + "”给你贡献了红包");
				im.setCreateTime(invite.getCreateTime());
				im.setMoney(invite.getMoney());
				u.add(im);
			}
			map.put("InviteList", u);
			return ApiUtil.returnArray(map);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("获取用户信息异常", e);
			return ApiUtil.putFailArray("获取我的邀请奖励失败");
		}

	}

	// 我的分成奖励
	@RequestMapping("/Discover/getUserDivide")
	@ResponseBody
	public Map<String, Object> getUserDivide(Integer uid, int pageNum, int pageSize) {

		try {

			Map map = new HashMap<>();

			List<InviteModel> u = new ArrayList<>();

			User user = userService.selectById(uid);
			Divide i = new Divide();
			DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			i.setBeginTime(format.format(new Date()));
			i.setEndTime(format.format(new Date()));
			i.setUid(uid);
			Double count = DivideService.getSum(i);
			if (SinataUtil.isNotEmpty(count)) {
				map.put("today", count);
			} else {
				map.put("today", 0.00);
			}

			map.put("sumMoney", user.getDivideAward());

			Divide in = new Divide();
			in.setUid(uid);
			PageHelper.startPage(pageNum, pageSize, false);
			List<Divide> list = DivideService.getList(in);
			Iterator<Divide> iterator = list.iterator();
			while (iterator.hasNext()) {
				Divide invite = iterator.next();
				User user2 = userService.selectById(invite.getBuid());
				InviteModel im = new InviteModel();
				im.setName(user2.getNickname() + "充值" + invite.getCzMoney());
				im.setCreateTime(invite.getCreateTime());
				im.setMoney(invite.getMoney());
				u.add(im);
			}
			map.put("DivideList", u);
			return ApiUtil.returnArray(map);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("获取用户信息异常", e);
			return ApiUtil.putFailArray("获取我的分成奖励失败");
		}

	}

	// 分享获取用户信息
	@RequestMapping("/Discover/getUser")
	@ResponseBody
	public Map<String, Object> getdivideInvite(Integer uid) {

		try {

			Map map = new HashMap<>();

			List<InviteModel> u = new ArrayList<>();

			User user = userService.selectById(uid);
			map.put("name", user.getNickname());
			map.put("img", user.getImgTx());
			map.put("usercoding", user.getUsercoding());
			return ApiUtil.returnArray(map);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("获取用户信息异常", e);
			return ApiUtil.putFailArray("获取分享获取用户信息失败");
		}

	}

	// 公众号用户信息
	@RequestMapping("/Discover/getUserGZH")
	@ResponseBody
	public Map<String, Object> getUserGZH(String uid) {

		try {

			Map map = new HashMap<>();

			List<InviteModel> u = new ArrayList<>();
			User user1 = new User();
			user1.setUsercoding(uid);
			User user = userService.getOne(user1);
			map.put("name", user.getNickname());
			map.put("img", user.getImgTx());
			map.put("usercoding", user.getUsercoding());
			map.put("id", user.getId());
			return ApiUtil.returnArray(map);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("获取用户信息异常", e);
			return ApiUtil.putFailArray("获取用户信息失败");
		}

	}

	// 获取提现的申请
	@RequestMapping("/Discover/AddSetWithdraw")
	@ResponseBody
	public Map<String, Object> AddSetWithdraw(Integer uid, Double money) {
		try {
			User user = userService.selectById(uid);
			if (user.getUserMoney() <= money) {
				return ApiUtil.putFailObj("余额不足");
			}

			user.setUserMoney(user.getUserMoney() - money);

			userService.updateById(user);

			Withdraw w = new Withdraw();
			w.setCreateTime(new Date());
			w.setUid(uid);
			w.setName(user.getNickname());
			w.setEid(user.getUsercoding());
			w.setState(2);
			w.setMoney(money);
			w.setHistoryMoney(user.getHistoryDeposit());
			w.setWitName(user.getTrueName());
			w.setWitPhone(user.getPayAccount());
			w.setStatus(1);
			w.setIsDelete(1);
			WithdrawService.insert(w);

			return ApiUtil.putSuccessObj("成功");
		} catch (Exception e) {
			e.printStackTrace();
			return ApiUtil.putFailObj("提现的申请失败");
		}
	}

}
