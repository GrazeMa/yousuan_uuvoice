package com.stylefeng.guns.rest.modular.system.service.impl;

import com.stylefeng.guns.rest.modular.system.model.AndroidVersions;
import com.stylefeng.guns.rest.modular.system.dao.AndroidVersionsMapper;
import com.stylefeng.guns.rest.modular.system.service.IAndroidVersionsService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import java.util.List;

import org.springframework.stereotype.Service;

/**
 * <p>
 * 安卓版本控制 服务实现类
 * </p>
 *
 * @author wumeng123
 * @since 2019-05-17
 */
@Service
public class AndroidVersionsServiceImpl extends ServiceImpl<AndroidVersionsMapper, AndroidVersions> implements IAndroidVersionsService {

	@Override
	public List<AndroidVersions> getList(AndroidVersions model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getList(model);
	}

	@Override
	public AndroidVersions getOne(AndroidVersions model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getOne(model);
	}

}
