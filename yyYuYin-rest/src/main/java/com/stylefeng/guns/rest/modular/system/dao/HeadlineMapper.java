package com.stylefeng.guns.rest.modular.system.dao;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.stylefeng.guns.rest.modular.system.model.Headline;

/**
 * @Description 头条 Mapper 接口。
 * @author Grazer_Ma
 * @Date 2020-05-21 23:02:42
 */
public interface HeadlineMapper extends BaseMapper<Headline> {

	List<Headline> getList(Headline model);

	Headline getOne(Headline model);

	Headline getOne2(Headline model);
	
}
