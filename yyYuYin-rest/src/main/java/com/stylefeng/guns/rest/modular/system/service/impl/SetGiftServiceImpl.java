package com.stylefeng.guns.rest.modular.system.service.impl;

import com.stylefeng.guns.rest.modular.system.model.SetGift;
import com.stylefeng.guns.rest.modular.system.dao.SetGiftMapper;
import com.stylefeng.guns.rest.modular.system.service.ISetGiftService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import java.util.List;

import org.springframework.stereotype.Service;

/**
 * <p>
 * 礼物设置 服务实现类
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
@Service
public class SetGiftServiceImpl extends ServiceImpl<SetGiftMapper, SetGift> implements ISetGiftService {

	@Override
	public List<SetGift> getList(SetGift model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getList(model);
	}

	@Override
	public SetGift getOne(SetGift model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getOne(model);
	}

}
