package com.stylefeng.guns.rest.modular.system.service.impl;

import com.stylefeng.guns.rest.modular.system.model.SetSpeed;
import com.stylefeng.guns.rest.modular.system.dao.SetSpeedMapper;
import com.stylefeng.guns.rest.modular.system.service.ISetSpeedService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import java.util.List;

import org.springframework.stereotype.Service;

/**
 * <p>
 * 速配/认证/语句设置 服务实现类
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
@Service
public class SetSpeedServiceImpl extends ServiceImpl<SetSpeedMapper, SetSpeed> implements ISetSpeedService {

	@Override
	public List<SetSpeed> getList(SetSpeed model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getList(model);
	}

	@Override
	public SetSpeed getOne(SetSpeed model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getOne(model);
	}

}
