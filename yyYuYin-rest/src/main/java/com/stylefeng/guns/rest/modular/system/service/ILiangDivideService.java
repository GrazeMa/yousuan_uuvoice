package com.stylefeng.guns.rest.modular.system.service;

import com.stylefeng.guns.rest.modular.system.model.LiangDivide;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 设置的一些关于分成的参数 服务类
 * </p>
 *
 * @author wumeng123
 * @since 2019-07-04
 */
public interface ILiangDivideService extends IService<LiangDivide> {

}
