package com.stylefeng.guns.rest.modular.util;


import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Formatter;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class Sign {
    public static void main(String[] args) {
        String jsapi_ticket = JsapiTicketUtil.getJSApiTicket();

        // 注意 URL 一定要动态获取，不能 hardcode
        //jsapi_ticket=HoagFKDcsGMVCIY2vOjf9mBNDeeIwVFa87uu4eC580T8dD8QHL3b2UPfwlJICyKEblD53boLr2El0_ehC3eA1Q
        //&noncestr=360580f1-d5f1-40ae-8f9d-164a48cce732&timestamp=1490077002&url=http://www.txciot.com/resources/html/test.html
    	
        String url = "http://m.sinata.cn/test.html";
        Map<String, String> ret = sign("HoagFKDcsGMVCIY2vOjf9mBNDeeIwVFa87uu4eC580T8dD8QHL3b2UPfwlJICyKEblD53boLr2El0_ehC3eA1Q", "http://www.txciot.com/resources/html/test.html");
        for (Map.Entry entry : ret.entrySet()) {
            System.out.println(entry.getKey() + ", " + entry.getValue());
        }
    };

    public static Map<String, String> sign(String jsapi_ticket, String url) {
        Map<String, String> ret = new HashMap<String, String>();
        String nonce_str = create_nonce_str();
        String timestamp = create_timestamp();
        String string1;
        String signature = "";

        //注意这里参数名必须全部小写，且必须有序
        string1 = "jsapi_ticket=" + jsapi_ticket +
                  "&noncestr=" + nonce_str +
                  "&timestamp=" + timestamp +
                  "&url=" + url;
        System.out.println(string1);

        try
        {
            MessageDigest crypt = MessageDigest.getInstance("SHA-1");
            crypt.reset();
            crypt.update(string1.getBytes("UTF-8"));
            signature = byteToHex(crypt.digest());
        }
        catch (NoSuchAlgorithmException e)
        {
            e.printStackTrace();
        }
        catch (UnsupportedEncodingException e)
        {
            e.printStackTrace();
        }

        ret.put("url", url);
        ret.put("jsapi_ticket", jsapi_ticket);
        ret.put("nonceStr", nonce_str);
        ret.put("timestamp", timestamp);
        ret.put("signature", signature);

        return ret;
    }

    private static String byteToHex(final byte[] hash) {
        Formatter formatter = new Formatter();
        for (byte b : hash)
        {
            formatter.format("%02x", b);
        }
        String result = formatter.toString();
        formatter.close();
        return result;
    }

    private static String create_nonce_str() {
        return UUID.randomUUID().toString();
    }

    private static String create_timestamp() {
        return Long.toString(System.currentTimeMillis() / 1000);
    }
}
