package com.stylefeng.guns.rest.modular.system.dao;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.stylefeng.guns.rest.modular.system.model.Skill;

/**
 * @Description 技能 Mapper 接口。
 * @author Grazer_Ma
 * @Date 2020-06-04 22:08:19
 */
public interface SkillMapper extends BaseMapper<Skill> {

	List<Skill> getList(Skill skillModel);

	Skill getOne(Skill skillModel);

}
