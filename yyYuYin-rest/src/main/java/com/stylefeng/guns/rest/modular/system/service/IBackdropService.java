package com.stylefeng.guns.rest.modular.system.service;

import com.stylefeng.guns.rest.modular.system.model.Backdrop;
import com.stylefeng.guns.rest.modular.system.model.Backdrop;

import java.util.List;

import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 背景图 服务类
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
public interface IBackdropService extends IService<Backdrop> {
	public List<Backdrop> getList(Backdrop model);
    
    Backdrop getOne(Backdrop model);
}
