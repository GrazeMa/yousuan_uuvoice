package com.stylefeng.guns.rest.modular.system.dao;

import com.stylefeng.guns.rest.modular.system.model.Red;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 红包 Mapper 接口
 * </p>
 *
 * @author wumeng123
 * @since 2019-02-20
 */
public interface RedMapper extends BaseMapper<Red> {

	List<Red> getList(Red model);

	Red getOne(Red model);

}
