package com.stylefeng.guns.rest.modular.system.controller;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

import com.stylefeng.guns.rest.modular.util.SinataUtil;

/**
 * @Description 逻辑中抽出的公共方法类，可方便复用。
 * @author Grazer_Ma
 * @Date 2020-05-21 21:00:53
 */
public class ControllerUtils {
	
	// Calculate user's age.
	public static int S_CalculateUserAge(String dateOfBirthString) {
        
		if (SinataUtil.isEmpty(dateOfBirthString)) return 0;

        int currentYearInt = Calendar.getInstance().get(Calendar.YEAR); // Get current year.
        int yearOfBirthInt = Integer.valueOf(dateOfBirthString.substring(0, 4)); // Get year of birth.
        int ageInt = currentYearInt - yearOfBirthInt; // Get the age.
        
        return ageInt;
        
	}
	
	// Generate the order number.
	public static String S_GenerateOrderNumber() {
		
		return new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date()) + UUID.randomUUID().toString().substring(0, 7);
		
	}
	
}