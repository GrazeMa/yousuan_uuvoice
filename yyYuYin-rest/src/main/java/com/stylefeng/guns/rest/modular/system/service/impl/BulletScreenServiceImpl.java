package com.stylefeng.guns.rest.modular.system.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.rest.modular.system.dao.BulletScreenMapper;
import com.stylefeng.guns.rest.modular.system.model.BulletScreen;
import com.stylefeng.guns.rest.modular.system.service.IBulletScreenService;

/**
 * @Description 弹幕服务实现类。
 * @author Grazer_Ma
 * @Date 2020-05-20 11:18:45
 */
@Service
public class BulletScreenServiceImpl extends ServiceImpl<BulletScreenMapper, BulletScreen>
		implements IBulletScreenService {

	@Override
	public List<BulletScreen> getList(BulletScreen model) {
		return this.baseMapper.getList(model);
	}

	@Override
	public BulletScreen getOne(BulletScreen model) {
		return this.baseMapper.getOne(model);
	}

}
