package com.stylefeng.guns.rest.modular.system.service;

import com.stylefeng.guns.rest.modular.system.model.Agreement;
import com.stylefeng.guns.rest.modular.system.model.Agreement;

import java.util.List;

import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 协议号 服务类
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
public interface IAgreementService extends IService<Agreement> {
	 public List<Agreement> getList(Agreement model);
	    
	    Agreement getOne(Agreement model);
}
