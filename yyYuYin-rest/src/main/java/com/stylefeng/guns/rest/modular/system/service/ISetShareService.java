package com.stylefeng.guns.rest.modular.system.service;

import com.stylefeng.guns.rest.modular.system.model.SetShare;
import com.stylefeng.guns.rest.modular.system.model.SetShare;

import java.util.List;

import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 分享赠送 服务类
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
public interface ISetShareService extends IService<SetShare> {
public List<SetShare> getList(SetShare model);
    
    SetShare getOne(SetShare model);
}
