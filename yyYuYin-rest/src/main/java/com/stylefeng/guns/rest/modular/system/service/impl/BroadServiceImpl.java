package com.stylefeng.guns.rest.modular.system.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.rest.modular.system.dao.BroadMapper;
import com.stylefeng.guns.rest.modular.system.model.Broad;
import com.stylefeng.guns.rest.modular.system.service.IBroadService;

/**
 * <p>
 * 广播交友 服务实现类
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
@Service
public class BroadServiceImpl extends ServiceImpl<BroadMapper, Broad> implements IBroadService {

	@Override
	public List<Broad> getList(Broad model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getList(model);
	}

	@Override
	public Broad getOne(Broad model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getOne(model);
	}

}
