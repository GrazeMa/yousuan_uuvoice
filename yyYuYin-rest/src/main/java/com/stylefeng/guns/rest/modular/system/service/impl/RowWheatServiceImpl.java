package com.stylefeng.guns.rest.modular.system.service.impl;

import com.stylefeng.guns.rest.modular.system.model.RowWheat;
import com.stylefeng.guns.rest.modular.system.dao.RowWheatMapper;
import com.stylefeng.guns.rest.modular.system.service.IRowWheatService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import java.util.List;

import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wumeng123
 * @since 2019-03-05
 */
@Service
public class RowWheatServiceImpl extends ServiceImpl<RowWheatMapper, RowWheat> implements IRowWheatService {

	@Override
	public List<RowWheat> getList(RowWheat model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getList(model);
	}

	@Override
	public RowWheat getOne(RowWheat model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getOne(model);
	}

}
