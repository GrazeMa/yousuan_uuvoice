package com.stylefeng.guns.rest.modular.system.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 优币消费记录
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
@TableName("app_ybconsume")
public class Ybconsume extends Model<Ybconsume> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    private Date createTime;
    private Integer uid;
    private String name;
    /**
     * 1是赠送礼物，2是购买花环，3购买座驾，4 发红包
     */
    private Integer state;
    /**
     * 礼物名称
     */
    private String lwName;
    /**
     * 消费数量
     */
    private Integer consumeNum;
    private Integer consumeYbNum;
    /**
     * '是否被删除（1=否  2=是）'
     */
    private Integer isDelete;

    private String eid;
    
    
    private String rid;
    
    
    

    public String getRid() {
		return rid;
	}

	public void setRid(String rid) {
		this.rid = rid;
	}

	public String getEid() {
		return eid;
	}

	public void setEid(String eid) {
		this.eid = eid;
	}

	public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

   

    public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public String getLwName() {
        return lwName;
    }

    public void setLwName(String lwName) {
        this.lwName = lwName;
    }

    public Integer getConsumeNum() {
        return consumeNum;
    }

    public void setConsumeNum(Integer consumeNum) {
        this.consumeNum = consumeNum;
    }

    public Integer getConsumeYbNum() {
        return consumeYbNum;
    }

    public void setConsumeYbNum(Integer consumeYbNum) {
        this.consumeYbNum = consumeYbNum;
    }

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "Ybconsume{" +
        "id=" + id +
        ", createTime=" + createTime +
        ", uid=" + uid +
        ", name=" + name +
        ", state=" + state +
        ", lwName=" + lwName +
        ", consumeNum=" + consumeNum +
        ", consumeYbNum=" + consumeYbNum +
        ", isDelete=" + isDelete +
        "}";
    }
}
