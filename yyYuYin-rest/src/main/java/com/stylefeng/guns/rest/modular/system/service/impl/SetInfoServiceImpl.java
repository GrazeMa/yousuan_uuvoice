package com.stylefeng.guns.rest.modular.system.service.impl;

import com.stylefeng.guns.rest.modular.system.model.SetInfo;
import com.stylefeng.guns.rest.modular.system.dao.SetInfoMapper;
import com.stylefeng.guns.rest.modular.system.service.ISetInfoService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import java.util.List;

import org.springframework.stereotype.Service;

/**
 * <p>
 * 系统设置 服务实现类
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
@Service
public class SetInfoServiceImpl extends ServiceImpl<SetInfoMapper, SetInfo> implements ISetInfoService {

	@Override
	public List<SetInfo> getList(SetInfo model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getList(model);
	}

	@Override
	public SetInfo getOne(SetInfo model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getOne(model);
	}

}
