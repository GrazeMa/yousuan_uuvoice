package com.stylefeng.guns.rest.modular.system.service.impl;

import com.stylefeng.guns.rest.modular.system.model.RedNum;
import com.stylefeng.guns.rest.modular.system.dao.RedNumMapper;
import com.stylefeng.guns.rest.modular.system.service.IRedNumService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import java.util.List;

import org.springframework.stereotype.Service;

/**
 * <p>
 * 红包内容 服务实现类
 * </p>
 *
 * @author wumeng123
 * @since 2019-02-20
 */
@Service
public class RedNumServiceImpl extends ServiceImpl<RedNumMapper, RedNum> implements IRedNumService {

	@Override
	public List<RedNum> getList(RedNum model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getList(model);
	}

	@Override
	public RedNum getOne(RedNum model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getOne(model);
	}

}
