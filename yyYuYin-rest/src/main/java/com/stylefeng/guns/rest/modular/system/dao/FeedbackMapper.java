package com.stylefeng.guns.rest.modular.system.dao;

import com.stylefeng.guns.rest.modular.system.model.Feedback;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 反馈 Mapper 接口
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-16
 */
public interface FeedbackMapper extends BaseMapper<Feedback> {

	List<Feedback> getList(Feedback model);

	Feedback getOne(Feedback model);

}
