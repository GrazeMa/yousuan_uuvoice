package com.stylefeng.guns.rest.modular.system.dao;

import com.stylefeng.guns.rest.modular.system.model.SignNum;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 签到天数 Mapper 接口
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-16
 */
public interface SignNumMapper extends BaseMapper<SignNum> {

	List<SignNum> getList(SignNum model);

	SignNum getOne(SignNum model);
	
	//查最近签到
		public SignNum getLastTimeLoginInfo(Integer uid);

}
