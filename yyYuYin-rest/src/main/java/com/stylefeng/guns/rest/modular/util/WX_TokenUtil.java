package com.stylefeng.guns.rest.modular.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;
import com.stylefeng.guns.rest.modular.system.service.IUserService;
import com.stylefeng.guns.rest.modular.system.service.impl.UserServiceImpl;

public class WX_TokenUtil {
	private static String appid = "wxcef4a6098854b3a5";
	private static String appSecret = "90c43a8c3459f17e36c9ec97df6111bd";
	
	 private static Logger log = LoggerFactory.getLogger(WX_TokenUtil.class); /**
	     *  获得微信 AccessToken
	     * access_token是公众号的全局唯一接口调用凭据，公众号调用各接口时都需使用access_token。
	     * 开发者需要access_token的有效期目前为2个小时，需定时刷新，重复获取将导致上次获取
	     * 的access_token失效。  
	     * （此处我是把token存在Redis里面了）   
	     */ public static String getWXToken(String access_token) {
	    	 if(access_token == null || access_token.equals("")){
	    		 String tokenUrl = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid="+ appid+"&secret="+ appSecret; 
	    		 JSONObject jsonObject = WX_HttpsUtil.httpsRequest(tokenUrl, "GET", null); 
	    		 if (null != jsonObject) { 
	    	try { 
	    		String access_token1=jsonObject.getString("access_token").toString(); 
	    		 return access_token1; 
	    		 } catch (JSONException e) { 
	    			 access_token = null; 
	    			 // 获取token失败 
	    			 log.error("获取token失败 errcode:{} errmsg:{}", jsonObject.getInteger("errcode"), jsonObject.getString("errmsg")); 
	    			 } 
	    		  } 
	        } 
	    	    return access_token; 	    
	     }
}
