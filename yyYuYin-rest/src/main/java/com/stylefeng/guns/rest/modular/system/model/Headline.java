package com.stylefeng.guns.rest.modular.system.model;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

/**
 * @Description 头条 Entity。
 * @author Grazer_Ma
 * @Date 2020-05-21 22:26:39
 */
@TableName("app_headline")
public class Headline extends Model<Headline> {

	private static final long serialVersionUID = 1L;

	@TableId(value = "id", type = IdType.AUTO)
	private Integer id; // 主键 ID。

	private Date createTime; // 头条创建时间。

	private String username; // 用户名。
	private String userAvatar; // 用户头像。
	private String comments; // 用户头条评论。
	private Date updateTime; // 上头条时间。
	private Integer remainingTime; // 头条剩余时间（单位：秒）。
	private Integer status; // 头条状态（0：未上过头条；1：已上过头条。）。
	private Integer userSex; // 用户性别（1：男；2：女。）。

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getUserAvatar() {
		return userAvatar;
	}

	public void setUserAvatar(String userAvatar) {
		this.userAvatar = userAvatar;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public Integer getRemainingTime() {
		return remainingTime;
	}

	public void setRemainingTime(Integer remainingTime) {
		this.remainingTime = remainingTime;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getUserSex() {
		return userSex;
	}

	public void setUserSex(Integer userSex) {
		this.userSex = userSex;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "Headline{" + "id=" + id + ", username=" + username + ", userAvatar=" + userAvatar + ", comments="
				+ comments + ", updateTime=" + updateTime + ", remainingTime=" + remainingTime + ", status=" + status
				+ ", userSex=" + userSex + "}";
	}

}
