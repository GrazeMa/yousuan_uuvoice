package com.stylefeng.guns.rest.modular.system.service;

import com.stylefeng.guns.rest.modular.system.model.Broad;
import com.stylefeng.guns.rest.modular.system.model.Broad;

import java.util.List;

import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 广播交友 服务类
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
public interface IBroadService extends IService<Broad> {
public List<Broad> getList(Broad model);
    
    Broad getOne(Broad model);
}
