package com.stylefeng.guns.rest.modular.system.dao;

import com.stylefeng.guns.rest.modular.system.model.User;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 用户信息 Mapper 接口
 * </p>
 *
 * @author lyouyou123
 * @since 2018-10-15
 */
public interface UserMapper extends BaseMapper<User> {

	User getByPhone(String phone);

	User getByWXsid(String phone);

	User getByQQsid(String phone);

	User getByGZsid(String phone);
	
	User getByRoomID(String usercoding);
	
	List<User> getList(User model);

	List<User> getList1(User model);

	List<User> getList2(User model);

	List<User> getList3(User model);

	User getOne(User model);
   
}
