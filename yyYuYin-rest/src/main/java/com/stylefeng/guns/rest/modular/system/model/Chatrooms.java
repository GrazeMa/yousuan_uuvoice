package com.stylefeng.guns.rest.modular.system.model;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.enums.IdType;
import com.stylefeng.guns.rest.modular.system.controller.model.UserModel;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 加入聊天室的
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-10
 */
@TableName("app_chatrooms")
public class Chatrooms extends Model<Chatrooms> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    private Integer uid;
    /**
     * 房间id
     */
    private String pid;
    /**
     * 类型 1是房主，2 是管理员，3用户
     */
    private Integer type;
    /**
     * 是否上麦 1否，2 是
     */
    private Integer state;
    /**
     * 上麦顺序
     */
    private Integer sequence;

    private Integer status;
    
    private Date createTime;
    
    private Integer isAgreement;
    
    private Date smtime;
    
  private Date addtime;
    
    
    

    public Date getAddtime() {
		return addtime;
	}

	public void setAddtime(Date addtime) {
		this.addtime = addtime;
	}

	public Integer getIsAgreement() {
		return isAgreement;
	}

	public void setIsAgreement(Integer isAgreement) {
		this.isAgreement = isAgreement;
	}

	public Date getSmtime() {
		return smtime;
	}

	public void setSmtime(Date smtime) {
		this.smtime = smtime;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

   

    public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Integer getSequence() {
        return sequence;
    }

    public void setSequence(Integer sequence) {
        this.sequence = sequence;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "Chatrooms{" +
        "id=" + id +
        ", uid=" + uid +
        ", pid=" + pid +
        ", type=" + type +
        ", state=" + state +
        ", sequence=" + sequence +
        "}";
    }
}
