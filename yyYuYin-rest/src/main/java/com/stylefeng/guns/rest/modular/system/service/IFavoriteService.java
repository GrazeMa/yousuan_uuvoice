package com.stylefeng.guns.rest.modular.system.service;

import java.util.List;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.rest.modular.system.model.Favorite;

/**
 * @Description 收藏服务类。
 * @author Grazer_Ma
 * @Date 2020-05-25 13:04:20
 */
public interface IFavoriteService extends IService<Favorite> {

	public List<Favorite> getList(Favorite favoriteModel);
	
	public List<Favorite> getFavoriteRoomsList(Favorite favoriteModel);
	
	Favorite getOne(Favorite favoriteModel);

}
