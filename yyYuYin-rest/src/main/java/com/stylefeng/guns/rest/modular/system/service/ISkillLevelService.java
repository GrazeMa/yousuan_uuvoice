package com.stylefeng.guns.rest.modular.system.service;

import java.util.List;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.rest.modular.system.model.SkillLevel;

/**
 * @Description 技能段位服务类。
 * @author Grazer_Ma
 * @Date 2020-06-04 22:22:55
 */
public interface ISkillLevelService extends IService<SkillLevel> {

	public List<SkillLevel> getList(SkillLevel skillLevelModel);

	SkillLevel getOne(SkillLevel skillLevelModel);

}
