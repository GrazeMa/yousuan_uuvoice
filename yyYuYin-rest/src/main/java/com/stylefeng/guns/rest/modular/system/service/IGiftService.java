package com.stylefeng.guns.rest.modular.system.service;

import com.stylefeng.guns.rest.modular.system.model.Gift;

import java.util.List;

import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 礼物管理 服务类
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
public interface IGiftService extends IService<Gift> {
public List<Gift> getList(Gift model);
    
    Gift getOne(Gift model);
}
