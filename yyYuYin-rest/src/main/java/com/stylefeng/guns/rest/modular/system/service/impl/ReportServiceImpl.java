package com.stylefeng.guns.rest.modular.system.service.impl;

import com.stylefeng.guns.rest.modular.system.model.Report;
import com.stylefeng.guns.rest.modular.system.dao.ReportMapper;
import com.stylefeng.guns.rest.modular.system.service.IReportService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import java.util.List;

import org.springframework.stereotype.Service;

/**
 * <p>
 * 举报 服务实现类
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
@Service
public class ReportServiceImpl extends ServiceImpl<ReportMapper, Report> implements IReportService {

	@Override
	public List<Report> getList(Report model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getList(model);
	}

	@Override
	public Report getOne(Report model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getOne(model);
	}

}
