package com.stylefeng.guns.rest.modular.system.controller.model;

import java.util.Date;

import com.baomidou.mybatisplus.annotations.TableField;

public class GiftRecordModel {
	private Date createTime;

	private String name;

	private String img;

	private Integer num;
	
	private Integer type;
	
	@TableField(exist = false)
	private String benificial;

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public Integer getNum() {
		return num;
	}

	public void setNum(Integer num) {
		this.num = num;
	}
	
	public String getBenificial() {
		return benificial;
	}

	public void setBenificial(String benificial) {
		this.benificial = benificial;
	}

}
