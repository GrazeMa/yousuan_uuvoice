package com.stylefeng.guns.rest.modular.system.controller.job;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.stylefeng.guns.rest.modular.system.controller.PayUtil;
import com.stylefeng.guns.rest.modular.system.model.Order;
import com.stylefeng.guns.rest.modular.system.model.Paylog;
import com.stylefeng.guns.rest.modular.system.model.User;
import com.stylefeng.guns.rest.modular.system.service.IOrderService;
import com.stylefeng.guns.rest.modular.system.service.IPaylogService;
import com.stylefeng.guns.rest.modular.system.service.IUserService;
import com.stylefeng.guns.rest.modular.util.SinataUtil;
import com.stylefeng.guns.rest.modular.util.TXSmSend;
import com.stylefeng.guns.rest.modular.util.Jpush;

@Component
@EnableScheduling
public class CancelOrderJob {

	Logger log = LoggerFactory.getLogger(this.getClass());

	// 'bout order.
	@Autowired
	private IOrderService iOrderService;
	@Autowired
	private IPaylogService iPaylogService;
	// 'bout user.
	@Autowired
	private IUserService iUserService;

	@Scheduled(cron = "0 0/1 * * * ?")
	public void cancelOrder() {
		System.out.println("cancelOrder begins.");
		
		// 推送。
		Map<String, String> map = new HashMap<>();
		map.put("2", "222");
		
		Jpush.push(map, "This is a push info.");
		
		try {
			EntityWrapper<Order> cancelOrderEntityWrapper = new EntityWrapper<>();
			cancelOrderEntityWrapper.eq("status", 5);
			cancelOrderEntityWrapper.le("createTime", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(System.currentTimeMillis() - (15 * 60 * 1000))));
			List<Order> cancelOrderLists = iOrderService.selectList(cancelOrderEntityWrapper);
			if (SinataUtil.isNotEmpty(cancelOrderLists)) {
				for (int i = 0; i < cancelOrderLists.size(); i++) {
					cancelOrderLists.get(i).setStatus(6);
					cancelOrderLists.get(i).setCancelReason(2);
					iOrderService.updateById(cancelOrderLists.get(i));
				}
				// 推送。
//				Map map = new HashMap();
//				map.put(111, "222");
				
//				iOrderService.updateBatchById(orderLists);
				
//				Jpush.push("484", "aaa", map);
//				TXSmSend.sendSMS("123456", "18301779150");
			}
			
//			EntityWrapper<Order> unServedOrderEntityWrapper = new EntityWrapper<>();
//			String statusString = "1,2,";
//			unServedOrderEntityWrapper.in("status", 1);
//			unServedOrderEntityWrapper.le("serviceBeginTime", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
//			
//			List<Order> unServedOrderLists = iOrderService.selectList(unServedOrderEntityWrapper);
//			if (SinataUtil.isNotEmpty(unServedOrderLists)) {
//				for (int i = 0; i < unServedOrderLists.size(); i++) {
//					TXSmSend.sendSMS("921225", "18301779150");
//				}
//				
//				// 推送。
//				Map<String, String> map = new HashMap<>();
//				map.put("2", "222");
//				
//				Jpush.push(map, "This is a push info.");
//				
//			}
			
		} catch (Exception e) {
			e.printStackTrace();
			log.error("取消订单异常", e.getMessage());
		}
	}

}
