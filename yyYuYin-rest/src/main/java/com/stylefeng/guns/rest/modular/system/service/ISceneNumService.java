package com.stylefeng.guns.rest.modular.system.service;

import com.stylefeng.guns.rest.modular.system.model.Scene;
import com.stylefeng.guns.rest.modular.system.model.SceneNum;

import java.util.List;

import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 用户获取的道具 服务类
 * </p>
 *
 * @author wumeng123
 * @since 2019-04-11
 */
public interface ISceneNumService extends IService<SceneNum> {
	public List<SceneNum> getList(SceneNum model);
	    
	SceneNum getOne(SceneNum model);
}
