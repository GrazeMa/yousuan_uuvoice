package com.stylefeng.guns.rest.modular.system.service.impl;

import com.stylefeng.guns.rest.modular.system.model.Protocol;
import com.stylefeng.guns.rest.modular.system.dao.ProtocolMapper;
import com.stylefeng.guns.rest.modular.system.service.IProtocolService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import java.util.List;

import org.springframework.stereotype.Service;

/**
 * <p>
 * app各种html页面 服务实现类
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
@Service
public class ProtocolServiceImpl extends ServiceImpl<ProtocolMapper, Protocol> implements IProtocolService {

	@Override
	public List<Protocol> getList(Protocol model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getList(model);
	}

	@Override
	public Protocol getOne(Protocol model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getOne(model);
	}

}
