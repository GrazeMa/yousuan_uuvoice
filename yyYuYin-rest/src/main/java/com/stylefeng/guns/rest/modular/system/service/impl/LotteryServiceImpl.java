package com.stylefeng.guns.rest.modular.system.service.impl;

import com.stylefeng.guns.rest.modular.system.model.Lottery;
import com.stylefeng.guns.rest.modular.system.dao.LotteryMapper;
import com.stylefeng.guns.rest.modular.system.service.ILotteryService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import java.util.List;

import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wumeng123
 * @since 2019-02-24
 */
@Service
public class LotteryServiceImpl extends ServiceImpl<LotteryMapper, Lottery> implements ILotteryService {

	@Override
	public List<Lottery> getList(Lottery model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getList(model);
	}

	@Override
	public Lottery getOne(Lottery model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getOne(model);
	}

}
