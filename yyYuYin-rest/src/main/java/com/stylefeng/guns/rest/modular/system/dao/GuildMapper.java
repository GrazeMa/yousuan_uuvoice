package com.stylefeng.guns.rest.modular.system.dao;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.stylefeng.guns.rest.modular.system.model.Guild;

/**
 * @Description 公会 Mapper 接口。
 * @author Grazer_Ma
 * @Date 2020-05-19 09:54:54
 */
public interface GuildMapper extends BaseMapper<Guild> {

	List<Guild> getList(Guild model);

	Guild getOne(Guild model);

}
