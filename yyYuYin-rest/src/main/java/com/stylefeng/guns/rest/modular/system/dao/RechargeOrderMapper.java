package com.stylefeng.guns.rest.modular.system.dao;

import com.stylefeng.guns.rest.modular.system.model.RechargeOrder;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 充值订单 Mapper 接口
 * </p>
 *
 * @author wumeng123
 * @since 2019-02-14
 */
public interface RechargeOrderMapper extends BaseMapper<RechargeOrder> {

	List<RechargeOrder> getList(RechargeOrder model);

	RechargeOrder getOne(RechargeOrder model);

	RechargeOrder getorderNum(String rechargeOrder);

}
