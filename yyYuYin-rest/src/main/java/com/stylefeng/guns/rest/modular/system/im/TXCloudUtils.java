package com.stylefeng.guns.rest.modular.system.im;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

public class TXCloudUtils {	     
	    /**
	     * 账号绑定
	     * @param uid 用户uid
	     * @param nickname 用户昵称
	     * @param image 用户头像
	     * @return
	     * @throws Exception 
	     */
	    public static boolean AccountImport(Integer uid,String nickname,String image) throws Exception
	    {
	        Map<String, String> map = new HashMap<String,String>();
	        map.put("Identifier", String.valueOf(uid));
	        map.put("Tag_Profile_IM_Nick", nickname);
	        map.put("Tag_Profile_IM_Image", image);
	        String json = JSON.toJSONString(map);//拼装json数据
	        JSONObject jObject = JSON.parseObject(TXCloudHelper.executePost(TXCloudHelper.getUrl(CloudData.accountImport,uid), json));
	        return jObject.get("ActionStatus").toString().equals("OK") ? true : false;
	    }
	     
	     
	    /**
	     * @see 设置用户属性
	     * @param userInfo
	     * @return
	     * @throws Exception 
	     */
	    public static boolean setPortrait(Integer uid,UserInfo userInfo) throws Exception
	    {
	        List<Map<String, Object>> list = setProfileItem(userInfo);
	        Map<Object, Object> map= new HashMap<Object, Object>();//拼装json数据
	        map.put("From_Account", String.valueOf(uid));
	        map.put("ProfileItem", list);
	        String json = JSON.toJSONString(map);
	        JSONObject jObject = JSON.parseObject(TXCloudHelper.executePost(TXCloudHelper.getUrl(CloudData.setPortrait,uid), json));
	        return jObject.get("ActionStatus").toString().equals("OK") ? true : false;  
	    }

	    /**
	     * @see 设置资料对象数组(设置用户属性)
	     * @param userInfo
	     * @return
	     */
	    public static List<Map<String, Object>> setProfileItem(UserInfo userInfo)
	    {
	        List<Map<String, Object>> list = new LinkedList<Map<String,Object>>();
	        if(!userInfo.getNickname().isEmpty()) 
	        {
	            Map<String, Object> map = new HashMap<String, Object>();
	            map.put("Tag", "Tag_Profile_IM_Nick");
	            map.put("Value", userInfo.getNickname());
	            list.add(map);
	        }
	         
	        if(!userInfo.getImage().isEmpty()) 
	        {
	            Map<String, Object> map = new HashMap<String, Object>();
	            map.put("Tag", "Tag_Profile_IM_Image");
	            map.put("Value", userInfo.getImage());
	            list.add(map);
	        }
	        return list;
	    }
		
	    
	    /**
	     * 拉黑
	     * @param uid 用户uid
	     * @param nickname 用户昵称
	     * @param image 用户头像
	     * @return
	     * @throws Exception 
	     */
	    public static boolean blackList(Integer uid,Integer buid) throws Exception
	    {
	        Map<String, Object> map = new HashMap<String,Object>();
	        map.put("From_Account", String.valueOf(uid));
	        List one=new ArrayList<>();
	        one.add(String.valueOf(buid));
	        map.put("To_Account", one);
	        String json = JSON.toJSONString(map);//拼装json数据
	        JSONObject jObject = JSON.parseObject(TXCloudHelper.executePost(TXCloudHelper.getUrl(CloudData.blackListAdd,uid), json));
	        return jObject.get("ActionStatus").toString().equals("OK") ? true : false;
	    }
	    //取消拉黑
	    public static boolean blackdel(Integer uid,Integer buid) throws Exception
	    {
	        Map<String, Object> map = new HashMap<String,Object>();
	        map.put("From_Account", String.valueOf(uid));
	        List one=new ArrayList<>();
	        one.add(String.valueOf(buid));
	        map.put("To_Account", one);
	        String json = JSON.toJSONString(map);//拼装json数据
	        JSONObject jObject = JSON.parseObject(TXCloudHelper.executePost(TXCloudHelper.getUrl(CloudData.blackListDelete,uid), json));
	        return jObject.get("ActionStatus").toString().equals("OK") ? true : false;
	    }
	   
}
