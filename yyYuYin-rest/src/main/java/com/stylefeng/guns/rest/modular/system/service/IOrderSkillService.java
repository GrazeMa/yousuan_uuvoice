package com.stylefeng.guns.rest.modular.system.service;

import java.util.List;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.rest.modular.system.model.OrderSkill;

/**
 * @Description 技能服务类。
 * @author Grazer_Ma
 * @Date 2020-06-04 22:19:50
 */
public interface IOrderSkillService extends IService<OrderSkill> {

	public List<OrderSkill> getList(OrderSkill orderSkillModel);

	OrderSkill getOne(OrderSkill orderSkillModel);
	
	List<OrderSkill> getServedUserList();

}
