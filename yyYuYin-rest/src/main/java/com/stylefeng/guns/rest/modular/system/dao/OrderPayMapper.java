package com.stylefeng.guns.rest.modular.system.dao;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.stylefeng.guns.rest.modular.system.model.OrderPay;

/**
 * @Description 订单支付 Mapper 接口。
 * @author Grazer_Ma
 * @Date 2020-06-04 22:03:11
 */
public interface OrderPayMapper extends BaseMapper<OrderPay> {

	List<OrderPay> getList(OrderPay orderPayModel);

	OrderPay getOne(OrderPay orderPayModel);

}
