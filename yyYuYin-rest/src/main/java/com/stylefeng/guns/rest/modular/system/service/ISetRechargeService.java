package com.stylefeng.guns.rest.modular.system.service;

import com.stylefeng.guns.rest.modular.system.model.SetRecharge;
import com.stylefeng.guns.rest.modular.system.model.SetRecharge;

import java.util.List;

import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 充值设置 服务类
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
public interface ISetRechargeService extends IService<SetRecharge> {
public List<SetRecharge> getList(SetRecharge model);
    
    SetRecharge getOne(SetRecharge model);
}
