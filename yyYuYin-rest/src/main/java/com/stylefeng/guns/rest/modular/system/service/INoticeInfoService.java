package com.stylefeng.guns.rest.modular.system.service;

import com.stylefeng.guns.rest.modular.system.model.NoticeInfo;
import com.stylefeng.guns.rest.modular.system.model.NoticeInfo;

import java.util.List;

import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 系统公告 服务类
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
public interface INoticeInfoService extends IService<NoticeInfo> {
public List<NoticeInfo> getList(NoticeInfo model);
    
    NoticeInfo getOne(NoticeInfo model);
}
