package com.stylefeng.guns.rest.modular.system.dao;

import com.stylefeng.guns.rest.modular.system.model.BannerInfo;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * Banner信息 Mapper 接口
 * </p>
 *
 * @author wumeng
 * @since 2018-05-28
 */
public interface BannerInfoMapper extends BaseMapper<BannerInfo> {

	List<BannerInfo> getList(BannerInfo model);

	BannerInfo getOne(BannerInfo model);

}
