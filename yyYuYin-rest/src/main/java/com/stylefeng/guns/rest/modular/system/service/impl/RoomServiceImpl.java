package com.stylefeng.guns.rest.modular.system.service.impl;

import com.stylefeng.guns.rest.modular.system.model.Room;
import com.stylefeng.guns.rest.modular.system.dao.RoomMapper;
import com.stylefeng.guns.rest.modular.system.service.IRoomService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import java.util.List;

import org.springframework.stereotype.Service;

/**
 * <p>
 * 房间信息 服务实现类
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
@Service
public class RoomServiceImpl extends ServiceImpl<RoomMapper, Room> implements IRoomService {

	@Override
	public List<Room> getList(Room model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getList(model);
	}

	@Override
	public Room getOne(Room model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getOne(model);
	}

}
