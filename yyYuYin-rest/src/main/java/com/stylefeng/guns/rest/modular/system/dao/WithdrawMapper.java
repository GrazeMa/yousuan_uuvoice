package com.stylefeng.guns.rest.modular.system.dao;

import com.stylefeng.guns.rest.modular.system.model.Withdraw;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 提现管理 Mapper 接口
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
public interface WithdrawMapper extends BaseMapper<Withdraw> {

	List<Withdraw> getList(Withdraw model);
	
	List<Withdraw> getList1(Withdraw model);

	Withdraw getOne(Withdraw model);

}
