package com.stylefeng.guns.rest.modular.system.im;
import com.stylefeng.guns.rest.modular.util.ParamUtil;

public class userUtil {

	public static String getParamPEMKey(String key) {
		String str = ParamUtil.getValue(key);
		if(str == null) return "";
		return str.replace("\\n", "\n");
	}

	private static long appId = Integer.parseInt( ParamUtil.getValue("txim_app_id") );
	private static String userAdmin = ParamUtil.getValue("txim_user_admin");
	private static String privStr = getParamPEMKey("txim_priv_key");
	private static String pubStr = getParamPEMKey("txim_pub_key");

 public static String getToken(Integer id) throws Exception {
//     //Use pemfile keys to test 优优语音 1400241678
//         String privStr = "-----BEGIN PRIVATE KEY-----\n" +
//            "MIGHAgEAMBMGByqGSM49AgEGCCqGSM49AwEHBG0wawIBAQQgXI8kw1wQe2nht/sW\n" +
//            "KVLLmijUjWw/HzKgLWWTeMAgZoahRANCAASfWiPURsPTBncqkpezBDXNb4s4fX9p\n" +
//            "zAP4/YwXmDcnX3ZaL4Ra1S8yWQkMbQV4eIvYcQnbIKHOPWD3pZbNwT/k\n" +
//            "-----END PRIVATE KEY-----";
//        //change public pem string to public string 优优语音  1400241678
//       String pubStr = "-----BEGIN PUBLIC KEY-----\n" +
//            "MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEn1oj1EbD0wZ3KpKXswQ1zW+LOH1/\n" +
//            "acwD+P2MF5g3J192Wi+EWtUvMlkJDG0FeHiL2HEJ2yChzj1g96WWzcE/5A==\n" +
//            "-----END PUBLIC KEY-----";

        // generate signature
        tls_sigature.GenTLSSignatureResult result = tls_sigature.GenTLSSignatureEx(appId, String.valueOf(id), privStr);

		return result.urlSig;

	}

	 public static String getToken1(Integer id) throws Exception {
//        String privStr = "-----BEGIN PRIVATE KEY-----\n" +
//                 "MIGHAgEAMBMGByqGSM49AgEGCCqGSM49AwEHBG0wawIBAQQgXI8kw1wQe2nht/sW\n" +
//                 "KVLLmijUjWw/HzKgLWWTeMAgZoahRANCAASfWiPURsPTBncqkpezBDXNb4s4fX9p\n" +
//                 "zAP4/YwXmDcnX3ZaL4Ra1S8yWQkMbQV4eIvYcQnbIKHOPWD3pZbNwT/k\n" +
//                 "-----END PRIVATE KEY-----";
//        String pubStr = "-----BEGIN PUBLIC KEY-----\n" +
//                 "MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEn1oj1EbD0wZ3KpKXswQ1zW+LOH1/\n" +
//                 "acwD+P2MF5g3J192Wi+EWtUvMlkJDG0FeHiL2HEJ2yChzj1g96WWzcE/5A==\n" +
//                 "-----END PUBLIC KEY-----";

        tls_sigature.GenTLSSignatureResult result = tls_sigature.GenTLSSignatureEx(appId, userAdmin, privStr);

		return result.urlSig;

	}
}
