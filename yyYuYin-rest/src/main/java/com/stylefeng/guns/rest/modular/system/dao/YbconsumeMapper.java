package com.stylefeng.guns.rest.modular.system.dao;

import com.stylefeng.guns.rest.modular.system.model.Ybconsume;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 优币消费记录 Mapper 接口
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
public interface YbconsumeMapper extends BaseMapper<Ybconsume> {

	List<Ybconsume> getList(Ybconsume model);

	Ybconsume getOne(Ybconsume model);

}
