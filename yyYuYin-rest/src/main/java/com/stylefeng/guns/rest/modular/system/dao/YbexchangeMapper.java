package com.stylefeng.guns.rest.modular.system.dao;

import com.stylefeng.guns.rest.modular.system.model.Ybexchange;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 优币兑换记录 Mapper 接口
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
public interface YbexchangeMapper extends BaseMapper<Ybexchange> {

	List<Ybexchange> getList(Ybexchange model);

	Ybexchange getOne(Ybexchange model);

}
