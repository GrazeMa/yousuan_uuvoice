package com.stylefeng.guns.rest.modular.system.controller.model;

import java.util.Date;
import java.util.List;

public class TimeInviteModel {

	private Date createTime;

	private List<InviteModel> Invite;

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public List<InviteModel> getInvite() {
		return Invite;
	}

	public void setInvite(List<InviteModel> invite) {
		Invite = invite;
	}

}
