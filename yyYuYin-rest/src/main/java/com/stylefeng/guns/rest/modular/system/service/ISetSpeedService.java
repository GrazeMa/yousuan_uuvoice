package com.stylefeng.guns.rest.modular.system.service;

import com.stylefeng.guns.rest.modular.system.model.SetSpeed;
import com.stylefeng.guns.rest.modular.system.model.SetSpeed;

import java.util.List;

import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 速配/认证/语句设置 服务类
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
public interface ISetSpeedService extends IService<SetSpeed> {
public List<SetSpeed> getList(SetSpeed model);
    
    SetSpeed getOne(SetSpeed model);
}
