package com.stylefeng.guns.rest.modular.system.controller.model;

/**
 * @Description 陪练精英 Model。
 * @author Grazer_Ma
 * @Date 2020-06-09 11:49:28
 */
public class ServedUserModel {

	private Integer userId;
	private String name;
	private Integer sex;
	private String avatarUrl;
	private String individuation;
	private String usercoding;
	private Integer orderNumber;
	private String skillName;
	
	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getSex() {
		return sex;
	}

	public void setSex(Integer sex) {
		this.sex = sex;
	}

	public String getAvatarUrl() {
		return avatarUrl;
	}

	public void setAvatarUrl(String avatarUrl) {
		this.avatarUrl = avatarUrl;
	}
	
	public String getIndividuation() {
		return individuation;
	}

	public void setIndividuation(String individuation) {
		this.individuation = individuation;
	}
	
	public String getUsercoding() {
		return usercoding;
	}

	public void setUsercoding(String usercoding) {
		this.usercoding = usercoding;
	}
	
	public Integer getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(Integer orderNumber) {
		this.orderNumber = orderNumber;
	}
	
	public String getSkillName() {
		return skillName;
	}

	public void setSkillName(String skillName) {
		this.skillName = skillName;
	}
	
}
