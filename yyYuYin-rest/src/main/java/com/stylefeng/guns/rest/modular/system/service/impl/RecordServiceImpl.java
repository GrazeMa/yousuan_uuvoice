package com.stylefeng.guns.rest.modular.system.service.impl;

import com.stylefeng.guns.rest.modular.system.model.Record;
import com.stylefeng.guns.rest.modular.system.dao.RecordMapper;
import com.stylefeng.guns.rest.modular.system.service.IRecordService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import java.util.List;

import org.springframework.stereotype.Service;

/**
 * <p>
 * 操作记录 服务实现类
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
@Service
public class RecordServiceImpl extends ServiceImpl<RecordMapper, Record> implements IRecordService {

	@Override
	public List<Record> getList(Record model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getList(model);
	}

	@Override
	public Record getOne(Record model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getOne(model);
	}

}
