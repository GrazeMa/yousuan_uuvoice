package com.stylefeng.guns.rest.modular.system.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 礼物管理
 * </p>
 *
 * @author wumeng123
 * @since 2019-02-13
 */
@TableName("app_gift")
public class Gift extends Model<Gift> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 礼物名字
     */
    private String name;
    /**
     * 礼物图片
     */
    private String img;
    /**
     * 需要优币
     */
    private Integer gold;
    /**
     * 可兑换优钻
     */
    private Integer yz;
    /**
     * 销量
     */
    private Integer volume;
    /**
     * 排序
     */
    private String sequence;
    /**
     * 1 销售 ,2 下架
     */
    private Integer isState;
    /**
     * 1 否,2 是
     */
    private Integer isDelete;
    /**
     * l礼物封面
     */
    private String imgFm;
    private Date createTime;
    /**
     * 1包裹，2普通礼物，3神奇礼物，4 专属礼物
     */
    private Integer state;
    /**
     * 是否是特价 1否，2是
     */
    private Integer status;
    /**
     * 天数
     */
    private Integer day;
    
    //1是显示，2是不显示
    @TableField(exist = false)
    private Integer restrict;
    
    //1是显示，2是不显示
    @TableField(exist = false)
    private Integer xin;

    

    public Integer getRestrict() {
		return restrict;
	}

	public void setRestrict(Integer restrict) {
		this.restrict = restrict;
	}

	public Integer getXin() {
		return xin;
	}

	public void setXin(Integer xin) {
		this.xin = xin;
	}

	public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public Integer getGold() {
        return gold;
    }

    public void setGold(Integer gold) {
        this.gold = gold;
    }

    public Integer getYz() {
        return yz;
    }

    public void setYz(Integer yz) {
        this.yz = yz;
    }

    public Integer getVolume() {
        return volume;
    }

    public void setVolume(Integer volume) {
        this.volume = volume;
    }

    public String getSequence() {
        return sequence;
    }

    public void setSequence(String sequence) {
        this.sequence = sequence;
    }

    public Integer getIsState() {
        return isState;
    }

    public void setIsState(Integer isState) {
        this.isState = isState;
    }

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    public String getImgFm() {
        return imgFm;
    }

    public void setImgFm(String imgFm) {
        this.imgFm = imgFm;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getDay() {
        return day;
    }

    public void setDay(Integer day) {
        this.day = day;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "Gift{" +
        "id=" + id +
        ", name=" + name +
        ", img=" + img +
        ", gold=" + gold +
        ", yz=" + yz +
        ", volume=" + volume +
        ", sequence=" + sequence +
        ", isState=" + isState +
        ", isDelete=" + isDelete +
        ", imgFm=" + imgFm +
        ", createTime=" + createTime +
        ", state=" + state +
        ", status=" + status +
        ", day=" + day +
        "}";
    }
}
