package com.stylefeng.guns.rest.modular.system.service;

import com.stylefeng.guns.rest.modular.system.model.Logbook;
import com.stylefeng.guns.rest.modular.system.model.Logbook;

import java.util.List;

import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 日志 服务类
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
public interface ILogbookService extends IService<Logbook> {
public List<Logbook> getList(Logbook model);
    
    Logbook getOne(Logbook model);
}
