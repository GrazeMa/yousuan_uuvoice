package com.stylefeng.guns.rest.modular.system.controller.job;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.stylefeng.guns.rest.modular.system.controller.AgoraUtil;
import com.stylefeng.guns.rest.modular.system.controller.model.UserModel;
import com.stylefeng.guns.rest.modular.system.model.Charm;
import com.stylefeng.guns.rest.modular.system.model.Chatrooms;
import com.stylefeng.guns.rest.modular.system.model.Seat;
import com.stylefeng.guns.rest.modular.system.model.User;
import com.stylefeng.guns.rest.modular.system.service.ICharmService;
import com.stylefeng.guns.rest.modular.system.service.IChatroomsService;
import com.stylefeng.guns.rest.modular.system.service.ISeatService;
import com.stylefeng.guns.rest.modular.system.service.IUserService;
import com.stylefeng.guns.rest.modular.util.ParamUtil;
import com.stylefeng.guns.rest.modular.util.SinataUtil;

import net.sf.json.JSONArray;

/**
 * @Description 心跳下麦任务。
 * @author Grazer_Ma
 * @Date 2020-05-11 10:40:26
 */
@Component
@EnableScheduling
public class RoomJob {

	// Initialization.
	Logger log = LoggerFactory.getLogger(this.getClass());

	// 房间魅力值
	@Autowired
	private ICharmService iCharmService;
	// 房间
	@Autowired
	private IChatroomsService iChatroomsService;
	@Autowired
	private ISeatService iSeatService;
	@Autowired
	private IUserService iUserService;

	// 声网's APPID.
	public static String SappID = ParamUtil.getValue("SappID");

	@Scheduled(cron = "0 */1 * * * ?") // Repeat per minute.
	public void updateStatusToOutTime() {
		log.info("RoomJob.updateStatusToOutTime: Beginning of RoomJob.");

		Chatrooms chatrooms = new Chatrooms(); // app_chatrooms.
		chatrooms.setState(2); // state means '是否上麦（1：否；2：是）'.
		List<Chatrooms> sequenceList = iChatroomsService.getList1(chatrooms); // ORDER BY sequence ASC（按照上麦顺序正序排列）.

		try {
			if (SinataUtil.isNotEmpty(sequenceList)) { // 'sequenceList' is not empty.
				Iterator<Chatrooms> iterator = sequenceList.iterator();
				while (iterator.hasNext()) {
					Chatrooms one = iterator.next();
					if (!one.getType().equals(1)) { // 不是房主类型。type means '类型（1：房主；2：管理员；3：用户；4：协议用户(暂不用)）'.
						Date date = new Date(); // Get the current time.
						long intervalMinute = 0;
						if (null != one.getAddtime()) {
							intervalMinute = (date.getTime() - one.getAddtime().getTime()) / 1000 / 60; // Get the interval time (in minutes).
						}
						if (intervalMinute > 2) { // More than 2 minutes.
							one.setStatus(2);
							one.setState(1);
							one.setSequence(0);
							iChatroomsService.updateById(one);

							User user = iUserService.selectById(one.getUid());
							System.out.println("user: " + user);
							user.setIsR(3); // isR means '在哪个房间（1：自己房间；2：别人房间；3：不在房间）'
							iUserService.updateById(user);

							Seat s = new Seat();
							s.setPid(one.getPid());
							List<Seat> list = iSeatService.getList(s);
							Iterator<Seat> iterator1 = list.iterator();
							while (iterator1.hasNext()) {
								// 拿到麦位对应的人
								Seat eva = iterator1.next();
								Chatrooms f = new Chatrooms();
								f.setPid(one.getPid());
								f.setState(2);
								f.setStatus(1);
								f.setSequence(eva.getSequence());
								Chatrooms l = iChatroomsService.getOne(f);
								if (SinataUtil.isNotEmpty(l)) {
									User byID = iUserService.selectById(l.getUid());
									UserModel u1 = new UserModel();
									u1.setId(byID.getId());
									u1.setImg(byID.getImgTx());
									u1.setName(byID.getNickname());
									u1.setSex(byID.getSex());
									u1.setSequence(l.getSequence());
									u1.setState(l.getState());
									u1.setType(l.getType());

									u1.setUserTh(byID.getUserTh());
									u1.setUserZj(byID.getUserZj());
									u1.setUserThfm(byID.getUserThfm());
									u1.setUserZjfm(byID.getUserZjfm());

									Charm se = new Charm();
									se.setRid(one.getPid());
									DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
									String time = format.format(l.getSmtime());
									se.setBeginTime(time);

									se.setRid(one.getPid());
									se.setUid(l.getUid());
									// 获取上麦后获得魅力值
									Charm sum1 = iCharmService.getSum1(se);

									if (SinataUtil.isEmpty(sum1)) {
										u1.setNum(0);
									} else {
										u1.setNum(sum1.getNum());
									}
									eva.setUserModel(u1);
								}

							}

							// 推送在麦位上的
							String json = JSONArray.fromObject(list).toString();
							log.info("RoomJob.updateStatusToOutTime: Ending of RoomJob.");
							AgoraUtil.getInstance().setChannelAttr(one.getPid(), "attr_mics", json);
						}
					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			log.error("商品库存记录添加异常", e.getMessage());
		}
	}

}



//package com.stylefeng.guns.rest.modular.system.controller.job;
//
//import com.alibaba.fastjson.JSONObject;
//import com.baomidou.mybatisplus.mapper.EntityWrapper;
//
//import com.stylefeng.guns.rest.modular.system.controller.AgoraUtil;
//import com.stylefeng.guns.rest.modular.system.controller.model.UserModel;
//import com.stylefeng.guns.rest.modular.system.model.Agreement;
//import com.stylefeng.guns.rest.modular.system.model.Charm;
//import com.stylefeng.guns.rest.modular.system.model.Chatrooms;
//import com.stylefeng.guns.rest.modular.system.model.Claimer;
//import com.stylefeng.guns.rest.modular.system.model.Recommend;
//import com.stylefeng.guns.rest.modular.system.model.Room;
//import com.stylefeng.guns.rest.modular.system.model.Seat;
//import com.stylefeng.guns.rest.modular.system.model.SetSpeed;
//import com.stylefeng.guns.rest.modular.system.model.User;
//import com.stylefeng.guns.rest.modular.system.service.ICharmService;
//import com.stylefeng.guns.rest.modular.system.service.IChatroomsService;
//import com.stylefeng.guns.rest.modular.system.service.IClaimerService;
//import com.stylefeng.guns.rest.modular.system.service.IRecommendService;
//import com.stylefeng.guns.rest.modular.system.service.IRoomService;
//import com.stylefeng.guns.rest.modular.system.service.ISeatService;
//import com.stylefeng.guns.rest.modular.system.service.IUserService;
//import com.stylefeng.guns.rest.modular.util.ParamUtil;
//import com.stylefeng.guns.rest.modular.util.SinataUtil;
//
//import io.agora.signal.Signal;
//import io.agora.signal.Signal.LoginSession;
//import io.agora.signal.Signal.LoginSession.Channel;
//import net.sf.json.JSONArray;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.scheduling.annotation.EnableScheduling;
//import org.springframework.scheduling.annotation.Scheduled;
//import org.springframework.stereotype.Component;
//
//import java.math.BigDecimal;
//import java.text.DateFormat;
//import java.text.SimpleDateFormat;
//import java.util.Date;
//import java.util.Iterator;
//import java.util.List;
//import java.util.Map;
//
///**
// * 
// */
//@Component
//@EnableScheduling
//public class RoomJob {
//	Logger log = LoggerFactory.getLogger(this.getClass());
//
//	// 房间
//	@Autowired
//	private IChatroomsService IChatroomsService;
//	@Autowired 
//	private IUserService   UserService;
//	@Autowired
//	private IRoomService   RoomService;
//	
//	@Autowired
//	private ISeatService SeatService;
//	
//	
//	// 房间魅力值
//		@Autowired
//		private ICharmService charmService;
//		
//	public static String SappID = ParamUtil.getValue("SappID");
//
//	@Scheduled(cron = "0 */1 * * * ?")
//	public void updateStatusToOutTime() {
//		System.out.println("roomJob:updateStatusToOutTime");
//		
//		try {
//			Chatrooms c=new Chatrooms();
//			c.setState(2);
//			List<Chatrooms> list1 = IChatroomsService.getList1(c);
//			if(SinataUtil.isNotEmpty(list1)){
//				Iterator<Chatrooms> iterator = list1.iterator();
//				while (iterator.hasNext()) {
//					Chatrooms one = iterator.next();
//					if(!one.getType().equals(1)){
//						if(SinataUtil.isEmpty(one.getAddtime().getTime())){
//							if (one.getType().equals(1)) {
//							    User user = UserService.selectById(one.getUid());
//							    Room r=new Room();
//							    r.setRid(one.getPid());
//							    Room room2 = RoomService.getOne(r);
//								one.setStatus(2);
//								one.setSequence(0);
//								one.setState(1);
//								room2.setIsfz(2);
//								RoomService.updateById(room2);
//
//								user.setIsR(3);
//
//								UserService.updateById(user);
//
//								
//								//推送的房间的信息
//								String string = JSONObject.toJSONString(room2);
//								System.out.println("roomJob:updateStatusToOutTime:A");
//								AgoraUtil.getInstance().setChannelAttr(one.getPid(), "attr_xgfj", string);
//
//							} else {
//								  User user = UserService.selectById(one.getUid());
//								one.setStatus(2);
//								one.setState(1);
//								one.setSequence(0);
//
//								user.setIsR(3);
//
//								UserService.updateById(user);
//							}
//
//							IChatroomsService.updateById(one);
//							
//							
//							Seat s = new Seat();
//							s.setPid(one.getPid());
//							List<Seat> list = SeatService.getList(s);
//							Iterator<Seat> iterator1 = list.iterator();
//							while (iterator1.hasNext()) {
//								//拿到麦位对应的人
//								Seat eva = iterator1.next();
//								Chatrooms f = new Chatrooms();
//								f.setPid(one.getPid());
//								f.setState(2);
//								f.setStatus(1);
//								f.setSequence(eva.getSequence());
//								Chatrooms l = IChatroomsService.getOne(f);
//								if (SinataUtil.isNotEmpty(l)) {
//										User byID = UserService.selectById(l.getUid());
//										UserModel u1 = new UserModel();
//										u1.setId(byID.getId());
//										u1.setImg(byID.getImgTx());
//										u1.setName(byID.getNickname());
//										u1.setSex(byID.getSex());
//										u1.setSequence(l.getSequence());
//										u1.setState(l.getState());
//										u1.setType(l.getType());
//
//										u1.setUserTh(byID.getUserTh());
//										u1.setUserZj(byID.getUserZj());
//										u1.setUserThfm(byID.getUserThfm());
//										u1.setUserZjfm(byID.getUserZjfm());
//
//										Charm se = new Charm();
//										se.setRid(one.getPid());
//										DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//										String time = format.format(l.getSmtime());
//										se.setBeginTime(time);
//
//										se.setRid(one.getPid());
//										se.setUid(l.getUid());
//										//获取上麦后获得魅力值
//										Charm sum1 = charmService.getSum1(se);
//
//										if (SinataUtil.isEmpty(sum1)) {
//											u1.setNum(0);
//										} else {
//											u1.setNum(sum1.getNum());
//										}
//										eva.setUserModel(u1);
//								}
//										
//							}
//							
//							//推送在麦位上的
//							String json = JSONArray.fromObject(list).toString();
//							System.out.println("roomJob:updateStatusToOutTime:B");
//							AgoraUtil.getInstance().setChannelAttr(one.getPid(), "attr_mics", json);
//						}else{
//							Date date = new Date();
//							long between = (date.getTime() - one.getAddtime().getTime()) / 1000;// 除以1000是为了转换成秒
//							long min = between / 60;
//							if (min >= 2) {
//								if (one.getType().equals(1)) {
//									    User user = UserService.selectById(one.getUid());
//									    Room r=new Room();
//									    r.setRid(one.getPid());
//									    Room room2 = RoomService.getOne(r);
//										one.setStatus(2);
//										one.setSequence(0);
//										one.setState(1);
//										room2.setIsfz(2);
//										RoomService.updateById(room2);
//
//										user.setIsR(3);
//
//										UserService.updateById(user);
//
//										//推送的房间的信息
//										String string = JSONObject.toJSONString(room2);
//										System.out.println("roomJob:updateStatusToOutTime:C");
//										AgoraUtil.getInstance().setChannelAttr(one.getPid(), "attr_xgfj", string);
//
//									} else {
//										  User user = UserService.selectById(one.getUid());
//										one.setStatus(2);
//										one.setState(1);
//										one.setSequence(0);
//
//										user.setIsR(3);
//
//										UserService.updateById(user);
//									}
//
//									IChatroomsService.updateById(one);
//									
//									
//									Seat s = new Seat();
//									s.setPid(one.getPid());
//									List<Seat> list = SeatService.getList(s);
//									Iterator<Seat> iterator1 = list.iterator();
//									while (iterator1.hasNext()) {
//										//拿到麦位对应的人
//										Seat eva = iterator1.next();
//										Chatrooms f = new Chatrooms();
//										f.setPid(one.getPid());
//										f.setState(2);
//										f.setStatus(1);
//										f.setSequence(eva.getSequence());
//										Chatrooms l = IChatroomsService.getOne(f);
//										if (SinataUtil.isNotEmpty(l)) {
//												User byID = UserService.selectById(l.getUid());
//												UserModel u1 = new UserModel();
//												u1.setId(byID.getId());
//												u1.setImg(byID.getImgTx());
//												u1.setName(byID.getNickname());
//												u1.setSex(byID.getSex());
//												u1.setSequence(l.getSequence());
//												u1.setState(l.getState());
//												u1.setType(l.getType());
//
//												u1.setUserTh(byID.getUserTh());
//												u1.setUserZj(byID.getUserZj());
//												u1.setUserThfm(byID.getUserThfm());
//												u1.setUserZjfm(byID.getUserZjfm());
//
//												Charm se = new Charm();
//												se.setRid(one.getPid());
//												DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//												String time = format.format(l.getSmtime());
//												se.setBeginTime(time);
//
//												se.setRid(one.getPid());
//												se.setUid(l.getUid());
//												//获取上麦后获得魅力值
//												Charm sum1 = charmService.getSum1(se);
//
//												if (SinataUtil.isEmpty(sum1)) {
//													u1.setNum(0);
//												} else {
//													u1.setNum(sum1.getNum());
//												}
//												eva.setUserModel(u1);
//										}
//												
//									}
//									
//									//推送在麦位上的
//									String json = JSONArray.fromObject(list).toString();
//									System.out.println("roomJob:updateStatusToOutTime:D");
//									AgoraUtil.getInstance().setChannelAttr(one.getPid(), "attr_mics", json);
//							}
//						}
//					}
//				}
//			}
//			
//		} catch (Exception e) {
//			e.printStackTrace();
//			log.error("商品库存记录添加异常", e.getMessage());
//		}
//	}
//
//}
