package com.stylefeng.guns.rest.modular.system.service;

import com.stylefeng.guns.rest.modular.system.model.Ybconsume;
import com.stylefeng.guns.rest.modular.system.model.Ybconsume;

import java.util.List;

import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 优币消费记录 服务类
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
public interface IYbconsumeService extends IService<Ybconsume> {
public List<Ybconsume> getList(Ybconsume model);
    
    Ybconsume getOne(Ybconsume model);
}
