package com.stylefeng.guns.rest.modular.system.service;

import com.stylefeng.guns.rest.modular.system.model.Audit;
import com.stylefeng.guns.rest.modular.system.model.Audit;

import java.util.List;

import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 实名认证 服务类
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
public interface IAuditService extends IService<Audit> {
	public List<Audit> getList(Audit model);
    
    Audit getOne(Audit model);
}
