package com.stylefeng.guns.rest.modular.system.service.impl;

import com.stylefeng.guns.rest.modular.system.model.Ybexchange;
import com.stylefeng.guns.rest.modular.system.dao.YbexchangeMapper;
import com.stylefeng.guns.rest.modular.system.service.IYbexchangeService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import java.util.List;

import org.springframework.stereotype.Service;

/**
 * <p>
 * 优币兑换记录 服务实现类
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
@Service
public class YbexchangeServiceImpl extends ServiceImpl<YbexchangeMapper, Ybexchange> implements IYbexchangeService {

	@Override
	public List<Ybexchange> getList(Ybexchange model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getList(model);
	}

	@Override
	public Ybexchange getOne(Ybexchange model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getOne(model);
	}

}
