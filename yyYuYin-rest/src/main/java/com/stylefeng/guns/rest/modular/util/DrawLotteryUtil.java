package com.stylefeng.guns.rest.modular.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.stylefeng.guns.rest.modular.system.model.Lottery;

public class DrawLotteryUtil {
		public static Lottery drawLottery(List<Lottery> giftList){ 
			if(null != giftList && giftList.size()>0){ 
				List<Lottery> orgProbList = new ArrayList<Lottery>(giftList.size()); 
				for(Lottery gift:giftList){ 
					//按顺序将概率添加到集合中 
					orgProbList.add(gift); 
					} 
				    return draw(orgProbList); 
					}
			         return null; 
		         } 
		
		
		        public static Lottery draw(List<Lottery> giftProbList){ 
		        	 List<Double> sortRateList = new ArrayList<Double>(); 
		        	 // 计算概率总和 
		        	 Double sumRate = 0D; 
		        	 for(Lottery prob : giftProbList){ 
		        		 sumRate += prob.getRatio(); 
		        		 } if(sumRate != 0){
                            double rate = 0D; 
                            //概率所占比例
                            for(Lottery prob : giftProbList){ 
                            	rate += prob.getRatio(); 
                            	// 构建一个比例区段组成的集合(避免概率和不为1) 
                            	sortRateList.add(rate / sumRate); } 
                            // 随机生成一个随机数，并排序
                            double random = Math.random(); 
                            sortRateList.add(random); 
                            Collections.sort(sortRateList); 
                            // 返回该随机数在比例集合中的索引 
                           
                            int index = sortRateList.indexOf(random);
                            
                            return giftProbList.get(index);
                            } 
		        		 return null; 
		}
		        
		        
		        public static void main(String[] args) { 
		        	Lottery iphone = new Lottery();
		        	iphone.setGid(101); 
		        	iphone.setName("苹果手机"); 
		        	iphone.setRatio(0.05D); 
		        	Lottery thanks = new Lottery(); 
		        	thanks.setGid(102); 
		        	thanks.setName("再接再厉"); 
		        	thanks.setRatio(0.9D); 
		        	Lottery vip = new Lottery(); 
		        	
		        	vip.setGid(103); 
		        	vip.setName("优酷会员"); 
		        	vip.setRatio(0.01D); 
		        	
		        	Lottery iphone1 = new Lottery();
		        	iphone1.setGid(104); 
		        	iphone1.setName("苹果手机4"); 
		        	iphone1.setRatio(0.01D); 
		        	
		        	Lottery iphone2 = new Lottery();
		        	iphone2.setGid(105); 
		        	iphone2.setName("苹果手机5"); 
		        	iphone2.setRatio(0.01D); 
		        	
		        	
		        	Lottery iphone3 = new Lottery();
		        	iphone3.setGid(106); 
		        	iphone3.setName("苹果手机6"); 
		        	iphone3.setRatio(0.02D); 
		        	
		        	
		        	Lottery iphone4 = new Lottery();
		        	iphone4.setGid(107); 
		        	iphone4.setName("苹果手机7"); 
		        	iphone4.setRatio(0.01D); 
		        	
		        	List<Lottery> list = new ArrayList<Lottery>(); 
		        	list.add(vip); 
		        	list.add(thanks); 
		        	list.add(iphone); 
		        	list.add(iphone1);
		        	list.add(iphone2);
		        	list.add(iphone3);
		        	list.add(iphone4);
		        	for(int i=0;i<10;i++){ 
		        		 Lottery drawLottery = drawLottery(list); 
		        		System.out.println(drawLottery); 
		        		} 
		        	}
		    
		     
		        
		        
	}


	

