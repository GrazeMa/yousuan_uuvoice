package com.stylefeng.guns.rest.modular.system.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.aliyuncs.http.ProtocolType;
import com.aliyuncs.sts.model.v20150401.AssumeRoleResponse;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.stylefeng.guns.rest.modular.system.controller.model.UserModel;
import com.stylefeng.guns.rest.modular.system.im.userUtil;
import com.stylefeng.guns.rest.modular.system.model.Agreement;
import com.stylefeng.guns.rest.modular.system.model.Charm;
import com.stylefeng.guns.rest.modular.system.model.Chatrooms;
import com.stylefeng.guns.rest.modular.system.model.Friend;
import com.stylefeng.guns.rest.modular.system.model.Invite;
import com.stylefeng.guns.rest.modular.system.model.Logbook;
import com.stylefeng.guns.rest.modular.system.model.Room;
import com.stylefeng.guns.rest.modular.system.model.Scene;
import com.stylefeng.guns.rest.modular.system.model.Seat;
import com.stylefeng.guns.rest.modular.system.model.SetShare;
import com.stylefeng.guns.rest.modular.system.model.SetSpeed;
import com.stylefeng.guns.rest.modular.system.model.SmsCode;
import com.stylefeng.guns.rest.modular.system.model.User;
import com.stylefeng.guns.rest.modular.system.service.IAgreementService;
import com.stylefeng.guns.rest.modular.system.service.ICharmService;
import com.stylefeng.guns.rest.modular.system.service.IChatroomsService;
import com.stylefeng.guns.rest.modular.system.service.IFriendService;
import com.stylefeng.guns.rest.modular.system.service.IInviteService;
import com.stylefeng.guns.rest.modular.system.service.ILogbookService;
import com.stylefeng.guns.rest.modular.system.service.IRoomService;
import com.stylefeng.guns.rest.modular.system.service.ISceneService;
import com.stylefeng.guns.rest.modular.system.service.ISeatService;
import com.stylefeng.guns.rest.modular.system.service.ISetInfoService;
import com.stylefeng.guns.rest.modular.system.service.ISetShareService;
import com.stylefeng.guns.rest.modular.system.service.ISetSpeedService;
import com.stylefeng.guns.rest.modular.system.service.ISmsCodeService;
import com.stylefeng.guns.rest.modular.system.service.IUserService;
import com.stylefeng.guns.rest.modular.util.ApiUtil;
import com.stylefeng.guns.rest.modular.util.ConstellationUtil;
import com.stylefeng.guns.rest.modular.util.DateUtil;
import com.stylefeng.guns.rest.modular.util.ParamUtil;
import com.stylefeng.guns.rest.modular.util.RandomNumberGeneratorUtil;
import com.stylefeng.guns.rest.modular.util.SinataUtil;
import com.stylefeng.guns.rest.modular.util.StsServiceSample;
import com.stylefeng.guns.rest.modular.util.TXSmSend;

import io.agora.signal.Signal;
import io.agora.signal.Signal.LoginSession;
import io.agora.signal.Signal.LoginSession.Channel;
import net.sf.json.JSONArray;

@Controller
@RequestMapping("api")
public class UserAppController {
	Logger log = LoggerFactory.getLogger(getClass());
	// 用户
	@Autowired
	private IUserService userService;

	// 短信
	@Autowired
	private ISmsCodeService smsService;
	
	//推送红包的信息
	@Autowired
	private ISetInfoService SetInfoService;
 //房间座位相关设置
	@Autowired
	private ISeatService SeatService;
	//房间
	@Autowired
	private IRoomService roomService;
	//登录日志
	@Autowired
	private ILogbookService LogbookService;
	//邀请用户（邀请明细）
	@Autowired
	private IInviteService InviteService;
//分享赠送
	@Autowired
	private ISetShareService SetShareService;

	// 这个是道具
	@Autowired
	private ISceneService SceneService;

	// 房间里面的信息
	@Autowired
	private IChatroomsService IChatroomsService;
   //协议号
	@Autowired
	private IAgreementService AgreementService;

	// 房间魅力值
	@Autowired
	private ICharmService charmService;

	@Autowired
	private ISetSpeedService SetSpeedService;
	
	
	@Autowired
	private IRoomService RoomService;
	
	// 'Friend' part.
	@Autowired
	private IFriendService iFriendService;
	 
	public static String SappID = ParamUtil.getValue("SappID");

	/**
	 * @Description Get user's information.
	 * @author Grazer_Ma
	 * @Date 2020-05-19 14:00:00
	 * @param uid
	 * @return Map<String, Object>
	 */
	@RequestMapping("/user/getUserInfo")
	@ResponseBody
	public Map<String, Object> getUserInfo(Integer uid) {
		Logger log = LoggerFactory.getLogger(getClass());
		try {
			if (SinataUtil.isEmpty(uid)) {
				return ApiUtil.putFailObj("uid不能为空！");
			}
			// Find the user.
			User user = userService.selectById(uid);
			
			String dateOfBirthString = "";
			if(null != user.getDateOfBirth()) dateOfBirthString = user.getDateOfBirth();
			
			int age = ControllerUtils.S_CalculateUserAge(dateOfBirthString);
			user.setAge(age);
			
//			// Find the user's friends.
//			Integer addFriendsInteger = iFriendService.selectCount(new EntityWrapper<Friend>().eq("uid", uid).eq("state", 2));
//			Integer addedFriendsInteger = iFriendService.selectCount(new EntityWrapper<Friend>().eq("buid", uid).eq("state", 2));
//			int friendsCountInt = (addFriendsInteger >= addedFriendsInteger) ? addedFriendsInteger : addFriendsInteger;
//			
			Friend addFriends = new Friend();
			addFriends.setUid(uid);
			addFriends.setState(2);
			List<Friend> addFriendsLists = iFriendService.getList(addFriends);
			
			Friend addedFriends = new Friend();
			addedFriends.setBuid(uid);
			addedFriends.setState(2);
			List<Friend> addedFriendsLists = iFriendService.getList(addedFriends);
			
			List<Integer> addFriendsComparedLists = new ArrayList<>();
			List<Integer> addedFriendsComparedLists = new ArrayList<>();
			
			for (int i = 0; i < addFriendsLists.size(); i++) {
				addFriendsComparedLists.add(addFriendsLists.get(i).getUid());
	        }
			
			for (int i = 0; i < addedFriendsLists.size(); i++) {
				addedFriendsComparedLists.add(addedFriendsLists.get(i).getBuid());
	        }
			
			int friendsCountInt = (int) addFriendsComparedLists.parallelStream().
	                filter(item -> addedFriendsComparedLists.contains(item)).count();
	        System.out.println("friendsCountInt:" + friendsCountInt);
			
//			List<Friend> l1 = new ArrayList<>();
//	        l1.add(friend1);
//	        l1.add(friend2);
//	        l1.add(friend3);
//
//	        List<Friend> l2 = new ArrayList<>();
//	        l2.add(friend4);
//	        l2.add(friend5);
//
////	        System.out.println("retainAll: " + l1.retainAll(l2));
//	        System.out.println("l1: " + l1);
//	        System.out.println("l2: " + l2);
//
//	        List<Integer> list1 = new ArrayList<>();
//	        for (int i = 0; i < l1.size(); i++) {
//	            list1.add(l1.get(i).buid);
//	        }
//	        System.out.println("list1: " + list1);
//
//	        List<Integer> list2 = new ArrayList<>();
//	        for (int i = 0; i < l2.size(); i++) {
//	            list2.add(l2.get(i).uid);
//	        }
//	        System.out.println("list2: " + list2);
//
//	        long count = list1.parallelStream().
//	                filter(item -> list2.contains(item)).count();
//	        System.out.println("交集的个数为:" + count);
//
//	        BitSet bitSet = new BitSet(Collections.max(list1));
//	        BitSet bitSet1 = new BitSet(Collections.max(list2));
//	        for (int i = 0; i < list1.size(); i++) {
//	            bitSet.set(list1.get(i));
//	        }
//	        System.out.println("bitSet: " + bitSet);
//	        for (int i = 0; i < list2.size(); i++) {
//	            bitSet1.set(list2.get(i));
//	        }
//	        System.out.println("bitSet1: " + bitSet1);
//	        bitSet.and(bitSet1);
//	        System.out.println("交集的个数为:" + bitSet.cardinality());	
			
			user.setFriendsCount(friendsCountInt);

			return ApiUtil.returnObj(user);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("获取用户信息异常", e);
			return ApiUtil.putFailObj("获取用户信息失败");
		}
	}

	// 公众号获取用户信息
	@RequestMapping("/user/gzUserInfo")
	@ResponseBody
	public Map<String, Object> gzUserInfo(String ecoding) {
		Logger log = LoggerFactory.getLogger(getClass());
		try {
			User user = new User();

			user.setUsercoding(ecoding);

			User user1 = userService.getOne(user);

			UserModel use = new UserModel();

			use.setImg(user1.getImgTx());
			use.setName(user1.getNickname());
			use.setUsercoding(user1.getUsercoding());
			use.setId(user1.getId());

			return ApiUtil.returnObj(use);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("获取用户信息异常", e);
			return ApiUtil.putFailObj("获取用户信息失败");
		}
	}

	// 手机注册
	@RequestMapping("/user/PhoneRegistered")
	@ResponseBody
	public Map<String, Object> PhoneRegistered(String phone, String smsCode, String password, String imgTx, Integer sex,
			String nickname, String city, String dateOfBirth, String usercoding, HttpServletRequest request) {
		try {
			if (SinataUtil.isEmpty(smsCode)) {
				return ApiUtil.putFailObj("验证码不能为空！");
			}
			if (SinataUtil.isEmpty(phone)) {
				return ApiUtil.putFailObj("手机号不能为空！");
			}
			User byPhone = userService.getByPhone(phone);
			if (byPhone != null) {
				return ApiUtil.putFailObj("手机号已经被注册！");
			}

			if (SinataUtil.isNotEmpty(usercoding)) {
				User us = new User();
				us.setUsercoding(usercoding);
				User one = userService.getOne(us);
				if (SinataUtil.isEmpty(one)) {
					return ApiUtil.putFailObj("你的邀请码无效！");
				}

			}

			SmsCode sms2 = smsService.getByPhone(phone);
			if (sms2 != null) {
				// 判读验证码是否有效
				if (smsValid(sms2.getCreateTime()).equals("00")) {
					return ApiUtil.putFailObj("验证码已过期！");
				}
				if (!smsCode.equals(sms2.getCode())) {
					return ApiUtil.putFailObj("验证码错误！");
				}
			} else {
				return ApiUtil.putFailObj("请发送验证码！");
			}
			User user = new User();
			Date date = new Date();
			DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String time = format.format(date);
			user.setCreateDate(time);
			user.setPhone(phone);
			user.setPassword(password);
			user.setState(1);
			user.setSex(sex);
			user.setAge(0);
			user.setGift(0);
			user.setGold(0);
			if (SinataUtil.isEmpty(city)) {
				user.setCity("成都市");
			} else {
				user.setCity(city);
			}
//算星座
			SetShare share1 = SetShareService.selectById(1);
			user.setUserMoney(Double.parseDouble(share1.getX().toString()));
			user.setLoginDate(new Date());
			user.setDateOfBirth(dateOfBirth);
			if (SinataUtil.isNotEmpty(dateOfBirth)) {
				String[] sourceStrArray = dateOfBirth.split("-");
				String constellation = ConstellationUtil.constellation(Integer.parseInt(sourceStrArray[1]),
						Integer.parseInt(sourceStrArray[2]));
				if (SinataUtil.isNotEmpty(constellation)) {
					user.setConstellation(constellation);
				} else {
					user.setConstellation("双鱼");
				}
			}

			StringBuilder app = new StringBuilder();
			String num = RandomNumberGeneratorUtil.generateNumber().toString();
			app.append(num);
			user.setUsercoding(app.toString());
			user.setJd("104.07728530701289");
			user.setWd("30.559209990493553");
			user.setYnum(0);
			if (imgTx != null) {
				user.setImgTx(imgTx);
			} else {
				user.setImgTx("http://cyqqmeitu.oss-cn-shenzhen.aliyuncs.com/img/249d6336e2d84973b546706a7504af2f.png");
			}
			if (nickname != null) {
				user.setNickname(nickname);
			} else {
				user.setNickname("昵称");
			}

			userService.insert(user);

			User user2 = userService.selectById(user.getId());

			for (int i = 1; i <= 8; i++) {
				Seat s = new Seat();
				s.setPid(num);
				s.setUid(user2.getId());
				s.setSequence(i);
				s.setState(1);
				s.setStatus(1);
				SeatService.insert(s);
			}

			Room room = new Room();
			room.setCreateTime(new Date());
			room.setRoomName(user2.getNickname());
			room.setUid(user2.getId());
			room.setRid(user2.getUsercoding());
			room.setName(user2.getNickname());
			room.setRoomLabel("聊天");
			roomService.insert(room);

			Logbook lg = new Logbook();
			lg.setUid(user2.getId());
			lg.setTime(time);

			LogbookService.insert(lg);

			String token = userUtil.getToken(user2.getId());
			user2.setToken(token);

			/*
			 * boolean accountImport = TXCloudUtils.AccountImport(user2.getId(),
			 * user2.getNickname(), user2.getImgTx());
			 */
//算邀请的收益
			if (SinataUtil.isNotEmpty(usercoding)) {
				User us = new User();
				us.setUsercoding(usercoding);
				User one = userService.getOne(us);

				Invite i = new Invite();
				i.setUid(one.getId());
				i.setBuid(user2.getId());
				i.setCreateTime(new Date());
				i.setIsDelete(1);
				SetShare share = SetShareService.selectById(2);
				String string = String.format("%.2f", (Math.random() * (share.getY() - share.getX()) + share.getX()));
				i.setMoney(Double.parseDouble(string));
				InviteService.insert(i);
				one.setInviteCount(one.getInviteCount() + 1);
				one.setInviteAward(one.getInviteAward() + Double.parseDouble(string));
				one.setUserMoney(one.getUserMoney() + Double.parseDouble(string));
				one.setLsinviteAward(one.getLsinviteAward() + Double.parseDouble(string));

				userService.updateById(one);
			}

			return ApiUtil.returnObj(user2);
		} catch (Exception e) {
			e.printStackTrace();
			return ApiUtil.putFailObj("注册失败");
		}
	}

	// 手机登录
	@RequestMapping("/user/loginPhone")
	@ResponseBody
	public Map<String, Object> loginPhone(String phone, String password, HttpServletRequest request) {
		try {
			if (SinataUtil.isEmpty(password)) {
				return ApiUtil.putFailObj("密码不能为空！");
			}
			if (SinataUtil.isEmpty(phone)) {
				return ApiUtil.putFailObj("手机号不能为空！");
			}
			User user2 = userService.getByPhone(phone);
			if (user2 == null) {
				return ApiUtil.putFailObj("该手机号未注册");
			}
			if (user2.getState() == 2) {
				return ApiUtil.putFailObj("账号被冻结,请联系平台！");
			}
			userService.updateById(user2);

			// 判断密码输入是否正确
			if (password.equals(user2.getPassword())) {
				Date date = new Date();
				DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				String time = format.format(date);
				Logbook lg = new Logbook();
				lg.setUid(user2.getId());
				lg.setTime(time);
				LogbookService.insert(lg);

				String token = userUtil.getToken(user2.getId());
				user2.setToken(token);
				user2.setLoginDate(new Date());
				userService.updateById(user2);
				return ApiUtil.returnObj(user2);
			} else {
				return ApiUtil.putFailObj("密码错误");
			}
		} catch (Exception e) {
			e.printStackTrace();
			return ApiUtil.putFailObj("登录失败");
		}
	}

	// PC登录
	@RequestMapping("/user/loginPc")
	@ResponseBody
	public Map<String, Object> loginPc(String usercoding, String password, HttpServletRequest request) {
		try {
			if (SinataUtil.isEmpty(password)) {
				return ApiUtil.putFailObj("密码不能为空！");
			}
			if (SinataUtil.isEmpty(usercoding)) {
				return ApiUtil.putFailObj("优优号不能为空！");
			}
			User user = new User();
			user.setUsercoding(usercoding);
			User user2 =new User();
			user2 = userService.getOne(user);
			if (SinataUtil.isEmpty(user2)){
				User user3 = new User();
				user3.setLiang(usercoding);
				user2 = userService.getOne(user3);
				if (user2 == null) {
					return ApiUtil.putFailObj("无优优号");
				}
			}
			if (user2.getState() == 2) {
				return ApiUtil.putFailObj("账号被冻结,请联系平台！");
			}
			if (SinataUtil.isEmpty(user2.getPassword())) {
				return ApiUtil.putFailObj("前前往APP设置密码");
			}

			// 判断密码输入是否正确
			if (password.equals(user2.getPassword())) {

				UserModel u = new UserModel();

				u.setImg(user2.getImgTx());
				u.setUsercoding(user2.getUsercoding());
				u.setId(user2.getId());
				u.setName(user2.getNickname());
				u.setLiang(user2.getLiang());
				Room room =new Room();
				room.setRid(user2.getUsercoding());
				Room one = RoomService.getOne(room);
                u.setType(one.getState());
				return ApiUtil.returnObj(u);
			} else {
				return ApiUtil.putFailObj("密码错误");
			}
		} catch (Exception e) {
			e.printStackTrace();
			return ApiUtil.putFailObj("登录失败");
		}
	}

	// 第三方注册和登录
	@RequestMapping("/user/loginThird")
	@ResponseBody
	public Map<String, Object> loginThird(String nickname, String usercoding, String img, String city, Integer sex,
			String sid, Integer type, HttpServletRequest request) {
		try {
			if (type == 1) {
				User user = userService.getByWXsid(sid);
				if (SinataUtil.isNotEmpty(user)) {

					if (user.getState() == 2) {
						return ApiUtil.putFailObj("账号被冻结,请联系平台！");
					}

					if (SinataUtil.isEmpty(user.getDateOfBirth())) {

						Date date = new Date();
						DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
						String time = format.format(date);
						Logbook lg = new Logbook();
						lg.setUid(user.getId());
						lg.setTime(time);
						LogbookService.insert(lg);

						String token = userUtil.getToken(user.getId());
						user.setToken(token);

						user.setLoginDate(new Date());
						userService.updateById(user);

						return ApiUtil.returnObj_1(user);
					} else {

						Date date = new Date();
						DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
						String time = format.format(date);
						Logbook lg = new Logbook();
						lg.setUid(user.getId());
						lg.setTime(time);
						LogbookService.insert(lg);

						user.setLoginDate(new Date());
						userService.updateById(user);

						String token = userUtil.getToken(user.getId());
						user.setToken(token);

						return ApiUtil.returnObj(user);
					}

				} else {
					User user1 = new User();
					Date date = new Date();

					user1.setState(1);
					user1.setAge(0);
					user1.setSex(sex);
					user1.setWxSid(sid);
					SetShare share1 = SetShareService.selectById(1);
					user1.setUserMoney(Double.parseDouble(share1.getX().toString()));

					DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					String time = format.format(date);
					user1.setCreateDate(time);
					user1.setState(1);
					user1.setGift(0);
					user1.setGold(0);

					user1.setLoginDate(new Date());

					if (SinataUtil.isEmpty(city)) {
						user1.setCity("成都市");
					} else {
						user1.setCity(city);
					}

					user1.setConstellation("双鱼");

					StringBuilder app = new StringBuilder();
					String num = RandomNumberGeneratorUtil.generateNumber().toString();
					app.append(num);
					user1.setUsercoding(app.toString());
					user1.setJd("104.07728530701289");
					user1.setWd("30.559209990493553");
					user1.setYnum(0);
					if (SinataUtil.isEmpty(img)) {
						user1.setImgTx(
								"http://cyqqmeitu.oss-cn-shenzhen.aliyuncs.com/img/249d6336e2d84973b546706a7504af2f.png");
					} else {
						user1.setImgTx(img);
					}

					if (nickname != null) {
						user1.setNickname(nickname);
					} else {
						user1.setNickname("昵称");
					}

					userService.insert(user1);
					User by = userService.selectById(user1.getId());

					for (int i = 1; i <= 8; i++) {
						Seat s = new Seat();
						s.setPid(num);
						s.setUid(by.getId());
						s.setSequence(i);
						s.setState(1);
						s.setStatus(1);
						SeatService.insert(s);
					}

					Room room = new Room();
					room.setCreateTime(new Date());
					room.setRoomName(by.getNickname());
					room.setUid(by.getId());
					room.setRid(by.getUsercoding());
					room.setName(by.getNickname());
					room.setRoomLabel("聊天");
					roomService.insert(room);

					Logbook lg = new Logbook();
					lg.setUid(by.getId());
					lg.setTime(time);
					LogbookService.insert(lg);

					String token = userUtil.getToken(by.getId());
					by.setToken(token);

					/*
					 * boolean accountImport =
					 * TXCloudUtils.AccountImport(by.getId(), by.getNickname(),
					 * by.getImgTx());
					 */

					if (SinataUtil.isNotEmpty(usercoding)) {
						User us = new User();
						us.setUsercoding(usercoding);
						User one = userService.getOne(us);

						Invite i = new Invite();
						i.setUid(one.getId());
						i.setBuid(by.getId());
						i.setCreateTime(new Date());
						i.setIsDelete(1);
						SetShare share = SetShareService.selectById(2);
						String string = String.format("%.2f",
								(Math.random() * (share.getY() - share.getX()) + share.getX()));
						i.setMoney(Double.parseDouble(string));
						InviteService.insert(i);

						one.setInviteCount(one.getInviteCount() + 1);
						one.setInviteAward(one.getInviteAward() + Double.parseDouble(string));
						one.setUserMoney(one.getUserMoney() + Double.parseDouble(string));
						one.setLsinviteAward(one.getLsinviteAward() + Double.parseDouble(string));

						userService.updateById(one);
					}

					if (SinataUtil.isEmpty(by.getDateOfBirth())) {
						return ApiUtil.returnObj_1(by);
					} else {
						return ApiUtil.returnObj(by);
					}

				}
			}
			if (type == 2) {
				User user = userService.getByQQsid(sid);
				if (SinataUtil.isNotEmpty(user)) {

					if (SinataUtil.isEmpty(user.getDateOfBirth())) {

						Date date = new Date();
						DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
						String time = format.format(date);
						Logbook lg = new Logbook();
						lg.setUid(user.getId());
						lg.setTime(time);
						LogbookService.insert(lg);

						String token = userUtil.getToken(user.getId());
						user.setToken(token);

						user.setLoginDate(new Date());
						userService.updateById(user);

						return ApiUtil.returnObj_1(user);
					} else {

						Date date = new Date();
						DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
						String time = format.format(date);
						Logbook lg = new Logbook();
						lg.setUid(user.getId());
						lg.setTime(time);
						LogbookService.insert(lg);

						String token = userUtil.getToken(user.getId());
						user.setToken(token);

						user.setLoginDate(new Date());
						userService.updateById(user);

						return ApiUtil.returnObj(user);
					}
				} else {
					User user1 = new User();
					Date date = new Date();

					user1.setState(1);
					user1.setAge(0);
					user1.setSex(sex);
					user1.setQqSid(sid);
					SetShare share1 = SetShareService.selectById(1);
					user1.setUserMoney(Double.parseDouble(share1.getX().toString()));

					DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					String time = format.format(date);
					user1.setCreateDate(time);
					user1.setState(1);
					user1.setGift(0);
					user1.setGold(0);
					if (SinataUtil.isEmpty(city)) {
						user1.setCity("成都市");
					} else {
						user1.setCity(city);
					}

					user1.setLoginDate(new Date());

					StringBuilder app = new StringBuilder();
					String num = RandomNumberGeneratorUtil.generateNumber().toString();
					app.append(num);
					user1.setUsercoding(app.toString());
					user1.setJd("104.07728530701289");
					user1.setWd("30.559209990493553");
					user1.setYnum(0);
					if (SinataUtil.isEmpty(img)) {
						user1.setImgTx(
								"http://cyqqmeitu.oss-cn-shenzhen.aliyuncs.com/img/249d6336e2d84973b546706a7504af2f.png");
					} else {
						user1.setImgTx(img);
					}
					if (nickname != null) {
						user1.setNickname(nickname);
					} else {
						user1.setNickname("昵称");
					}

					userService.insert(user1);
					User by = userService.selectById(user1.getId());

					for (int i = 1; i <= 8; i++) {
						Seat s = new Seat();
						s.setPid(num);
						s.setUid(by.getId());
						s.setSequence(i);
						s.setState(1);
						s.setStatus(1);
						SeatService.insert(s);
					}

					Room room = new Room();
					room.setCreateTime(new Date());
					room.setRoomName(by.getNickname());
					room.setUid(by.getId());
					room.setRid(by.getUsercoding());
					room.setName(by.getNickname());
					room.setRoomLabel("聊天");
					roomService.insert(room);

					Logbook lg = new Logbook();
					lg.setUid(by.getId());
					lg.setTime(time);
					LogbookService.insert(lg);

					String token = userUtil.getToken(by.getId());
					by.setToken(token);

					/*
					 * boolean accountImport =
					 * TXCloudUtils.AccountImport(by.getId(), by.getNickname(),
					 * by.getImgTx());
					 */

					if (SinataUtil.isNotEmpty(usercoding)) {
						User us = new User();
						us.setUsercoding(usercoding);
						User one = userService.getOne(us);

						Invite i = new Invite();
						i.setUid(one.getId());
						i.setBuid(by.getId());
						i.setCreateTime(new Date());
						i.setIsDelete(1);
						SetShare share = SetShareService.selectById(2);
						String string = String.format("%.2f",
								(Math.random() * (share.getY() - share.getX()) + share.getX()));
						i.setMoney(Double.parseDouble(string));
						InviteService.insert(i);

						one.setInviteCount(one.getInviteCount() + 1);
						one.setInviteAward(one.getInviteAward() + Double.parseDouble(string));
						one.setUserMoney(one.getUserMoney() + Double.parseDouble(string));
						one.setLsinviteAward(one.getLsinviteAward() + Double.parseDouble(string));

						userService.updateById(one);
					}

					if (SinataUtil.isEmpty(by.getDateOfBirth())) {
						return ApiUtil.returnObj_1(by);
					} else {
						return ApiUtil.returnObj(by);
					}

				}
			}
			if (type == 3) {
				User user = userService.getByGZsid(sid);
				if (SinataUtil.isNotEmpty(user)) {

					if (SinataUtil.isEmpty(user.getDateOfBirth())) {

						Date date = new Date();
						DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
						String time = format.format(date);
						Logbook lg = new Logbook();
						lg.setUid(user.getId());
						lg.setTime(time);
						LogbookService.insert(lg);

						String token = userUtil.getToken(user.getId());
						user.setToken(token);

						user.setLoginDate(new Date());
						userService.updateById(user);

						return ApiUtil.returnObj_1(user);
					} else {

						Date date = new Date();
						DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
						String time = format.format(date);
						Logbook lg = new Logbook();
						lg.setUid(user.getId());
						lg.setTime(time);
						LogbookService.insert(lg);

						String token = userUtil.getToken(user.getId());
						user.setToken(token);

						user.setLoginDate(new Date());
						userService.updateById(user);

						return ApiUtil.returnObj(user);
					}
				} else {
					User user1 = new User();
					Date date = new Date();

					user1.setState(1);
					user1.setAge(0);
					user1.setSex(sex);
					user1.setGzSid(sid);
					SetShare share1 = SetShareService.selectById(1);
					user1.setUserMoney(Double.parseDouble(share1.getX().toString()));

					DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					String time = format.format(date);
					user1.setCreateDate(time);
					user1.setState(1);
					user1.setGift(0);
					user1.setGold(0);

					if (SinataUtil.isEmpty(city)) {
						user1.setCity("成都市");
					} else {
						user1.setCity(city);
					}

					user1.setLoginDate(new Date());

					StringBuilder app = new StringBuilder();
					String num = RandomNumberGeneratorUtil.generateNumber().toString();
					app.append(num);
					user1.setUsercoding(app.toString());
					user1.setJd("104.07728530701289");
					user1.setWd("30.559209990493553");
					user1.setYnum(0);
					if (SinataUtil.isEmpty(img)) {
						user1.setImgTx(
								"http://cyqqmeitu.oss-cn-shenzhen.aliyuncs.com/img/249d6336e2d84973b546706a7504af2f.png");
					} else {
						user1.setImgTx(img);
					}
					if (nickname != null) {
						user1.setNickname(nickname);
					} else {
						user1.setNickname("昵称");
					}

					userService.insert(user1);
					User by = userService.selectById(user1.getId());

					for (int i = 1; i <= 8; i++) {
						Seat s = new Seat();
						s.setPid(num);
						s.setUid(by.getId());
						s.setSequence(i);
						s.setState(1);
						s.setStatus(1);
						SeatService.insert(s);
					}
					Room room = new Room();
					room.setCreateTime(new Date());
					room.setRoomName(by.getNickname());
					room.setUid(by.getId());
					room.setRid(by.getUsercoding());
					room.setName(by.getNickname());
					room.setRoomLabel("聊天");
					roomService.insert(room);

					Logbook lg = new Logbook();
					lg.setUid(by.getId());
					lg.setTime(time);
					LogbookService.insert(lg);

					String token = userUtil.getToken(by.getId());
					by.setToken(token);

					/*
					 * boolean accountImport =
					 * TXCloudUtils.AccountImport(by.getId(), by.getNickname(),
					 * by.getImgTx());
					 */

					if (SinataUtil.isNotEmpty(usercoding)) {
						User us = new User();
						us.setUsercoding(usercoding);
						User one = userService.getOne(us);

						Invite i = new Invite();
						i.setUid(one.getId());
						i.setBuid(by.getId());
						i.setCreateTime(new Date());
						i.setIsDelete(1);
						SetShare share = SetShareService.selectById(2);
						String string = String.format("%.2f",
								(Math.random() * (share.getY() - share.getX()) + share.getX()));
						i.setMoney(Double.parseDouble(string));
						InviteService.insert(i);

						one.setInviteCount(one.getInviteCount() + 1);
						one.setInviteAward(one.getInviteAward() + Double.parseDouble(string));
						one.setUserMoney(one.getUserMoney() + Double.parseDouble(string));
						one.setLsinviteAward(one.getLsinviteAward() + Double.parseDouble(string));

						userService.updateById(one);
					}

					if (SinataUtil.isEmpty(by.getDateOfBirth())) {
						return ApiUtil.returnObj_1(by);
					} else {
						return ApiUtil.returnObj(by);
					}

				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			return ApiUtil.putFailObj("注册失败");
		}
		return null;
	}

	// 获取最近登录一次的登录时间
	@RequestMapping("/user/Logbook")
	@ResponseBody
	public Map<String, Object> Logbook(Integer uid, HttpServletRequest request) {
		try {
			if (SinataUtil.isEmpty(uid)) {
				return ApiUtil.putFailObj("uid不能为空！");
			}

			User user = userService.selectById(uid);
			Map map = new HashMap<>();
			Date loginDate = user.getLoginDate();
			map.put("time", loginDate);
			user.setLoginDate(new Date());
			userService.updateById(user);

			return ApiUtil.returnObj(map);
		} catch (Exception e) {
			e.printStackTrace();
			return ApiUtil.putFailObj("失败");
		}
	}

	// 忘记密码
	@RequestMapping("/user/ForgotPassword")
	@ResponseBody
	public Map<String, Object> ForgotPassword(String phone, String password, String smsCode,
			HttpServletRequest request) {
		try {
			if (SinataUtil.isEmpty(phone)) {
				return ApiUtil.putFailObj("手机号不能为空！");
			}
			if (SinataUtil.isEmpty(password)) {
				return ApiUtil.putFailObj("密码不能为空！");
			}
			User user = userService.getByPhone(phone);
			if (user == null) {
				return ApiUtil.putFailObj("该手机号未注册");
			}
			SmsCode sms = new SmsCode();
			sms.setPhone(phone);
			SmsCode sms2 = smsService.getByPhone(phone);
			if (sms2 != null) {
				// 判读验证码是否有效
				if (smsValid(sms2.getCreateTime()).equals("00")) {
					return ApiUtil.putFailObj("验证码已过期！");
				}
				if (!smsCode.equals(sms2.getCode())) {
					return ApiUtil.putFailObj("验证码错误！");
				}
			} else {
				return ApiUtil.putFailObj("请发送验证码！");
			}
			user.setPassword(password);
			userService.updateById(user);
			return ApiUtil.returnObj("修改成功");
		} catch (Exception e) {
			e.printStackTrace();
			return ApiUtil.putFailObj("修改失败");
		}
	}

	// 修改密码
	@RequestMapping("/user/ModPassword")
	@ResponseBody
	public Map<String, Object> ModPassword(Integer uid, String oldpassword, String password,
			HttpServletRequest request) {
		try {
			if (SinataUtil.isEmpty(uid)) {
				return ApiUtil.putFailObj("uid不能为空！");
			}
			if (SinataUtil.isEmpty(oldpassword)) {
				return ApiUtil.putFailObj("原密码不能为空！");
			}
			if (SinataUtil.isEmpty(oldpassword)) {
				return ApiUtil.putFailObj("原密码不能为空！");
			}
			if (SinataUtil.isEmpty(password)) {
				return ApiUtil.putFailObj("新密码不能为空！");
			}
			User user = userService.selectById(uid);
			if (!oldpassword.equals(user.getPassword())) {
				return ApiUtil.putFailObj("原密码输入有误！");
			}

			if (oldpassword.equals(password)) {
				return ApiUtil.putFailObj("原密码和新密码一样！");
			}
			user.setPassword(password);
			userService.updateById(user);
			return ApiUtil.putSuccessObj("修改成功");
		} catch (Exception e) {
			e.printStackTrace();
			return ApiUtil.putFailObj("修改失败");
		}
	}

	// 绑定手机
	@RequestMapping("/user/bindPhone")
	@ResponseBody
	public Map<String, Object> modification4(String phone, Integer uid, String password, String smsCode,
			HttpServletRequest request) {
		try {
			if (SinataUtil.isEmpty(uid)) {
				return ApiUtil.putFailObj("uid不能为空！");
			}
			if (SinataUtil.isEmpty(phone)) {
				return ApiUtil.putFailObj("手机号不能为空！");
			}
			if (SinataUtil.isEmpty(password)) {
				return ApiUtil.putFailObj("密码不能为空！");
			}

			SmsCode sms = new SmsCode();
			sms.setPhone(phone);
			SmsCode sms2 = smsService.getByPhone(phone);
			if (sms2 != null) {
				// 判读验证码是否有效
				if (smsValid(sms2.getCreateTime()).equals("00")) {
					return ApiUtil.putFailObj("验证码已过期！");
				}
				if (!smsCode.equals(sms2.getCode())) {
					return ApiUtil.putFailObj("验证码错误！");
				}
			} else {
				return ApiUtil.putFailObj("请发送验证码！");
			}

			User byPhone = userService.getByPhone(phone);
			if (byPhone != null) {
				return ApiUtil.putFailObj("手机号已经被注册或绑定！");
			}
			User user1 = userService.selectById(uid);
			user1.setPhone(phone);
			user1.setPassword(password);
			userService.updateById(user1);
			return ApiUtil.putSuccessObj("绑定成功");
		} catch (Exception e) {
			e.printStackTrace();
			log.error("绑定手机异常", e);
			return ApiUtil.putFailObj("绑定失败");
		}
	}

	// 验证证码
	@RequestMapping("/user/isCode")
	@ResponseBody
	public Map<String, Object> isCode(HttpServletRequest request, String phone, String smsCode) {
		try {
			SmsCode sms2 = smsService.getByPhone(phone);
			if (sms2 != null) {
				// 判读验证码是否有效
				if (smsValid(sms2.getCreateTime()).equals("00")) {
					return ApiUtil.putFailObj("验证码已过期！");
				}
				if (!smsCode.equals(sms2.getCode())) {
					return ApiUtil.putFailObj("验证码错误！");
				}
			} else {
				return ApiUtil.putFailObj("请发送验证码！");
			}
			return ApiUtil.putSuccessObj("成功");
		} catch (Exception e) {
			e.printStackTrace();
			return ApiUtil.putFailObj("失败");
		}
	}

	// 修改绑定手机
	@RequestMapping("/user/ModifyPhone")
	@ResponseBody
	public Map<String, Object> ModifyPhone(String phone, String oldphone, Integer uid, String smsCode,
			String oldsmsCode, HttpServletRequest request) {
		try {
			if (SinataUtil.isEmpty(phone)) {
				return ApiUtil.putFailObj("手机号不能为空！");
			}
			User byPhone = userService.getByPhone(phone);
			if (byPhone != null) {
				return ApiUtil.putFailObj("手机号已经被注册！");
			}

			User user = userService.selectById(uid);
			if (user.getPhone() == oldphone) {
				return ApiUtil.putFailObj("原手机号输入有误！");
			}

			SmsCode sms2 = smsService.getByPhone(phone);
			if (!smsCode.equals(sms2.getCode())) {
				return ApiUtil.putFailObj("新手机号验证码有误！");
			}
			user.setPhone(phone);
			userService.updateById(user);
			return ApiUtil.returnObj("修改成功");
		} catch (Exception e) {
			e.printStackTrace();
			log.error("修改绑定手机异常", e);
			return ApiUtil.putFailObj("更换失败");
		}
	}

	// 修改用户
	@RequestMapping("/user/updateUser")
	@ResponseBody
	public Object userUpda(HttpServletRequest request, User user, String coding, Integer seId, Integer isSeId)
			throws Exception {
		if (SinataUtil.isNotEmpty(coding)) {
			User us = new User();
			us.setUsercoding(coding);
			User one = userService.getOne(us);
			if (SinataUtil.isEmpty(one)) {
				return ApiUtil.putFailObj("你的邀请码无效！");
			}

		}

		if (SinataUtil.isNotEmpty(user.getDateOfBirth())) {
			user.setDateOfBirth(user.getDateOfBirth());
			if (SinataUtil.isNotEmpty(user.getDateOfBirth())) {
				String[] sourceStrArray = user.getDateOfBirth().split("-");
				String constellation = ConstellationUtil.constellation(Integer.parseInt(sourceStrArray[1]),
						Integer.parseInt(sourceStrArray[2]));
				if (SinataUtil.isNotEmpty(constellation)) {
					user.setConstellation(constellation);
				} else {
					user.setConstellation("双鱼");
				}
			}
		}

		if (SinataUtil.isNotEmpty1(seId) && seId != 0) {
			Scene scene = SceneService.selectById(seId);
			if (scene.getState() == 1) {
				user.setUserZj(scene.getImg());
				user.setUserZjfm(scene.getImgFm());
				user.setZjid(scene.getId());

			} else {
				user.setUserTh(scene.getImg());
				user.setUserThfm(scene.getImgFm());
				user.setThid(scene.getId());
			}
		} else {
			if (SinataUtil.isNotEmpty1(seId) && seId == 0) {
				if (isSeId == 1) {
					user.setUserZj("x");
					user.setUserZjfm("x");
					user.setZjid(0);

				} else {
					user.setUserTh("x");
					user.setUserThfm("x");
					user.setThid(0);
				}
			}

		}

		if (SinataUtil.isNotEmpty(coding)) {
			User us = new User();
			us.setUsercoding(coding);
			User one = userService.getOne(us);

			Invite i = new Invite();
			i.setUid(one.getId());
			i.setBuid(user.getId());
			i.setCreateTime(new Date());
			i.setIsDelete(1);
			SetShare share = SetShareService.selectById(2);
			String string = String.format("%.2f", (Math.random() * (share.getY() - share.getX()) + share.getX()));
			i.setMoney(Double.parseDouble(string));
			InviteService.insert(i);

			one.setInviteCount(one.getInviteCount() + 1);
			one.setInviteAward(one.getInviteAward() + Double.parseDouble(string));
			one.setUserMoney(one.getUserMoney() + Double.parseDouble(string));
			one.setLsinviteAward(one.getLsinviteAward() + Double.parseDouble(string));

			userService.updateById(one);
		}

		userService.updateById(user);

		if (SinataUtil.isNotEmpty(user.getNickname()) || SinataUtil.isNotEmpty(user.getImgTx())) {
			Chatrooms cs = new Chatrooms();
			cs.setUid(user.getId());
			cs.setStatus(1);
			Chatrooms lists = IChatroomsService.getOne(cs);
			if (SinataUtil.isNotEmpty(lists)) {
				Seat s = new Seat();
				s.setPid(lists.getPid());
				List<Seat> list = SeatService.getList(s);
				Iterator<Seat> iterator = list.iterator();
				while (iterator.hasNext()) {
					Seat eva = iterator.next();
					Chatrooms f = new Chatrooms();
					f.setPid(lists.getPid());
					f.setState(2);
					f.setStatus(1);
					f.setSequence(eva.getSequence());
					Chatrooms l = IChatroomsService.getOne(f);
					if (SinataUtil.isNotEmpty(l)) {
						if (l.getIsAgreement() == 2) {
							Agreement byID = AgreementService.selectById(l.getUid());
							UserModel u1 = new UserModel();
							u1.setId(byID.getId());
							u1.setImg(byID.getImg());
							u1.setName(byID.getNickname());
							u1.setSex(byID.getSex());
							u1.setSequence(l.getSequence());
							u1.setState(l.getState());
							u1.setType(l.getType());

							Charm se = new Charm();
							se.setRid(lists.getPid());
							DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
							String time = format.format(l.getSmtime());

							Date date = new Date();
							String time2 = format.format(date);
							se.setEndTime(time2);

							se.setRid(lists.getPid());
							se.setUid(l.getUid());
							Charm sum1 = charmService.getSum1(se);

							if (SinataUtil.isEmpty(sum1)) {
								u1.setNum(0);
							} else {
								u1.setNum(sum1.getNum());
							}

							eva.setUserModel(u1);

						} else {
							User byID = userService.selectById(l.getUid());
							UserModel u1 = new UserModel();
							u1.setId(byID.getId());
							u1.setImg(byID.getImgTx());
							u1.setName(byID.getNickname());
							u1.setSex(byID.getSex());
							u1.setSequence(l.getSequence());
							u1.setState(l.getState());
							u1.setType(l.getType());
							u1.setUserTh(byID.getUserTh());
							u1.setUserZj(byID.getUserZj());
							u1.setUserThfm(byID.getUserThfm());
							u1.setUserZjfm(byID.getUserZjfm());
							Charm se = new Charm();
							se.setRid(lists.getPid());
							DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
							String time = format.format(l.getSmtime());

							Date date = new Date();
							String time2 = format.format(date);
							se.setEndTime(time2);

							se.setRid(lists.getPid());
							se.setUid(l.getUid());
							Charm sum1 = charmService.getSum1(se);

							if (SinataUtil.isEmpty(sum1)) {
								u1.setNum(0);
							} else {
								u1.setNum(sum1.getNum());
							}

							eva.setUserModel(u1);
						}
					}
				}

				Signal sig = new Signal(SappID);

				String token = getSWToken1(user.getId());
				LoginSession loginSession = sig.login("1", "_no_need_token", new Signal.LoginCallback());
				Channel channel = loginSession.channelJoin(lists.getPid(), new Signal.ChannelCallback());

				String json = JSONArray.fromObject(list).toString();
				channel.channelSetAttr("attr_mics", json);

				Room room = new Room();
				room.setRid(lists.getPid());
				Room room2 = roomService.getOne(room);

				SetSpeed byId = SetSpeedService.selectById(3);
				room2.setMark(byId.getX().toString());

				String string = JSONObject.toJSONString(room2);
				channel.channelSetAttr("attr_xgfj", string);

			}
		}

		/*
		 * User user2 = userService.selectById(user.getId()); UserInfo userInfo
		 * = new UserInfo(user2.getNickname(), user2.getImgTx(), (byte)0);
		 * boolean result = TXCloudUtils.setPortrait(user2.getId(), userInfo);
		 * if(result){ System.out.println(
		 * "dsadsadsadasdassadsadasdsadsadsadsasasasasasasasasasasasasasasasasa"
		 * ); }
		 */

		return ApiUtil.returnObj(user);
	}

	private String getSWToken1(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	// 获取阿里云的token
	@RequestMapping("/Token/Token")
	@ResponseBody
	public Map<String, Object> getToken(HttpServletRequest request) {
		try {
			// yuanxiaoshu 阿里云OSS?
			String accessKeyId = "LTAIxfAnFn2kMeZ3";
			String accessKeySecret = "rs3k7dScex4SWixRfPDhFe3XX0mmCK";// 需要在RAM控制台获取，此时要给子账号权限，并建立一个角色，把这个角色赋给子账户，这个角色会有一串值，就是rolearn要填的
																		// //记得角色的权限，子账户的权限要分配好，不然会报错
			String roleArn = "acs:ram::1773013998662691:role/aliyunosstokengeneratorrole";// 临时Token的会话名称，自己指定用于标识你的用户，主要用于审计，或者用于区分Token颁发给谁
			String roleSessionName = "external-username";// 这个可以为空，不好写，格式要对，无要求建议为空
			String policy = "{\n" + "    \"Version\": \"1\", \n" + "    \"Statement\": [\n" + "        {\n"
					+ "            \"Action\": [\n" + "                \"oss:*\"\n" + "            ], \n"
					+ "            \"Resource\": [\n" + "                \"*\" \n" + "            ], \n"
					+ "            \"Effect\": \"Allow\"\n" + "        }\n" + "    ]\n" + "}";
			ProtocolType protocolType = ProtocolType.HTTPS;
			AssumeRoleResponse response = StsServiceSample.assumeRole(accessKeyId, accessKeySecret, roleArn,
					roleSessionName, policy, protocolType);
			Map<String, String> respMap = new LinkedHashMap<String, String>();
			respMap.put("StatusCode", "200");
			respMap.put("AccessKeyId", response.getCredentials().getAccessKeyId());
			respMap.put("AccessKeySecret", response.getCredentials().getAccessKeySecret());
			respMap.put("SecurityToken", response.getCredentials().getSecurityToken());
			respMap.put("Expiration", response.getCredentials().getExpiration());
			return ApiUtil.returnObj(respMap);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("获取阿里云的token异常", e);
			return null;
		}
	}

	// 判断手机
	@RequestMapping("/user/isPhone")
	@ResponseBody
	public Map<String, Object> isPhone(String phone, HttpServletRequest request) {
		try {
			if (SinataUtil.isEmpty(phone)) {
				return ApiUtil.putFailObj("手机号不能为空！");
			}
			User byPhone = userService.getByPhone(phone);
			if (SinataUtil.isEmpty(byPhone)) {
				return ApiUtil.putFailObj("手机未注册！");
			}
			return ApiUtil.putSuccessObj("手机号注册");
		} catch (Exception e) {
			e.printStackTrace();
			log.error("修改绑定手机异常", e);
			return ApiUtil.putFailObj("修改失败");
		}
	}

	/**
	 * 获取短信验证码
	 * 
	 * @param sms
	 * @param request
	 * @return
	 */
	@RequestMapping("/sms/SmsCode")
	@ResponseBody
	public Map<String, Object> sendSmsCode(Integer uid, String phone, Integer type, HttpServletRequest request) {
		try {
			if (SinataUtil.isEmpty(type)) {
				return ApiUtil.putFailObj("类型必传！");
			}
			if (SinataUtil.isEmpty(phone)) {
				return ApiUtil.putFailObj("手机号必传！");
			}
			User byPhone = userService.getByPhone(phone);
			// 手机用户注册
			if (type == 1) {
				if (byPhone != null) {
					return ApiUtil.putFailObj("手机号已经被注册！");
				}
			}
			// 忘记密码
			if (type == 2) {
				if (byPhone == null) {
					return ApiUtil.putFailObj("手机号没有注册！");
				}
			}
			// 修改绑定手机
			if (type == 3) {

			}

			// 绑定支付密码
			if (type == 4) {
				if (byPhone == null) {
					return ApiUtil.putFailObj("手机号没有注册！");
				}
				if (SinataUtil.isNotEmpty(uid)) {
					User user = userService.selectById(uid);
					if (!user.getPhone().equals(phone)) {
						return ApiUtil.putFailObj("非当前登录的手机号");
					}
				}

			}
			// 修改手机
			if (type == 5) {
				if (byPhone == null) {
					return ApiUtil.putFailObj("手机号没有注册！");
				}
			}
			// 验证新手机
			if (type == 6) {
				if (byPhone != null) {
					return ApiUtil.putFailObj("手机号已经被注册！");
				}
			}

			// 生成随机验证码
			String checkCode = "";
			for (int i = 0; i < 6; i++) {
				checkCode += new Random().nextInt(10);
			}
			SmsCode sms1 = new SmsCode();
			sms1.setPhone(phone);
			SmsCode sms2 = smsService.getByPhone(phone);
			Date date = new Date();
			if (sms2 == null) {
				sms1.setCreateTime(date);
				sms1.setCode(checkCode);
				sms1.setType(type);
				smsService.insert(sms1);
			} else {
				sms2.setCreateTime(date);
				sms2.setCode(checkCode);
				sms2.setType(type);
				smsService.updateById(sms2);
			}

			/*
			 * Map<String, Object> map =
			 * SendSmsUtil.sendSMS(checkCode+"。如非本人操作，请忽略本短信", type, phone);
			 */

			String string = TXSmSend.sendSMS(checkCode, phone);

			return ApiUtil.putSuccessObj("发送成功");
		} catch (Exception e) {
			e.printStackTrace();
			return ApiUtil.putFailObj("验证码发送失败");
		}
	}

	/**
	 * 判读验证码是否有效
	 * 
	 * @param addTime
	 * @return
	 * @author TaoNingBo
	 */
	public static String smsValid(Date addTime) {
		long nowdate = DateUtil.getCurMilli();
		long xiangcha = (nowdate - DateUtil.getMillisecond(addTime)) / 1000 / 60; // 时间相差分钟数
		if (xiangcha >= 10) {
			return "00";
		}
		return "";
	}

}
