package com.stylefeng.guns.rest.modular.system.tencent.protocol;

import java.lang.reflect.Field;


import java.util.HashMap;
import java.util.Map;

import com.stylefeng.guns.rest.modular.system.tencent.common.Configure;
import com.stylefeng.guns.rest.modular.system.tencent.common.RandomStringGenerator;
import com.stylefeng.guns.rest.modular.system.tencent.common.Signature;
import com.stylefeng.guns.rest.modular.util.SinataUtil;




/**
 * 请求统一下单-API需要提交的数据
 * @author pan
 */
public class UnifiedorderReqData {

	//每个字段具体的意思请查看API文档
	private String appid;//公众账号ID （内置）
	private String mch_id;//商户号  （内置）
	private String nonce_str;//随机字符串 （内置）
	private String sign;//签名 


	//接口调用需要的参数
	private String body;//商品描述
	private String out_trade_no;//商户订单号
	private Integer total_fee;//总金额
	private String spbill_create_ip;//终端IP
	private String notify_url;//通知地址
	private String trade_type;//交易类型
	private String openid;//用户标识

	/**
	 * 预支付请求需要的参数（获取微信统一下单-必填参数）
	 * 公众账号ID 	appid （内置）
	 * 商户号  	mch_id （内置）
	 * 随机字符串  	nonce_str （内置）
	 * 签名 	sign （内置）
	 * @param body				商品描述 
	 * @param out_trade_no		商户订单号
	 * @param total_fee			总金额 （单位：分）
	 * @param notify_url		通知地址
	 */
	public UnifiedorderReqData(Integer apptype, String out_trade_no, String body, Double total_fee, String notify_url){

		//乘客appID与商户号
		this.appid = Configure.getAppid();

		//微信支付分配的商户号ID（开通公众号的微信支付功能之后可以获取到）
		this.mch_id = Configure.getMchid();

		//随机字符串，不长于32 位
		//        setNonce_str(RandomStringGenerator.getRandomStringByLength(32));
		this.nonce_str = RandomStringGenerator.getRandomStringByLength(32);


		//		setBody(body);// 商品描述
		this.body = body;

		//		setOut_trade_no(out_trade_no);// 商户订单号
		this.out_trade_no = out_trade_no;

		//		setTotal_fee();// 总金额
		String money=SinataUtil.doubleRetainTwo(total_fee*100d);
		this.total_fee = Integer.parseInt(money.substring(0,money.length()-3));

		//		setNotify_url(notify_url);// 通知地址
		this.notify_url = notify_url;
		//		setTrade_type("APP");// 交易类型，默认：APP
		this.trade_type = "APP";
		// 根据API给的签名规则进行签名
		this.sign =  Signature.getSign(apptype, toMap());

	}


	/**
	 * 预支付请求需要的参数（获取微信统一下单-必填参数）(公众号)
	 * 公众账号ID 	appid （内置）
	 * 商户号  	mch_id （内置）
	 * 随机字符串  	nonce_str （内置）
	 * 签名 	sign （内置）
	 * @param body				商品描述 
	 * @param out_trade_no		商户订单号
	 * @param total_fee			总金额 （单位：分）
	 * @param notify_url		通知地址
	 * @throws IllegalAccessException 
	 */
	public UnifiedorderReqData(String out_trade_no, String body, Double total_fee, String notify_url,String trade_type,String openid) throws IllegalAccessException{

		//appID与商户号
		this.appid = Configure.getGappid();

		//微信支付分配的商户号ID（开通公众号的微信支付功能之后可以获取到）
		this.mch_id = Configure.getGmchid();

		//随机字符串，不长于32 位
		//        setNonce_str(RandomStringGenerator.getRandomStringByLength(32));
		this.nonce_str = RandomStringGenerator.getRandomStringByLength(32);


		//		setBody(body);// 商品描述
		this.body = body;

		//		setOut_trade_no(out_trade_no);// 商户订单号
		this.out_trade_no = out_trade_no;

		//		setTotal_fee();// 总金额
		String money=SinataUtil.doubleRetainTwo(total_fee*100d);
		this.total_fee = Integer.parseInt(money.substring(0,money.length()-3));
		//this.total_fee = (int)(total_fee * 100);

		//		setNotify_url(notify_url);// 通知地址
		this.notify_url = notify_url;
		/*trade_type 交易类型JSAPI--公众号支付、NATIVE--原生扫码支付、APP--app支付，统一下单接口trade_type的传参可参考这里
		MICROPAY--刷卡支付，刷卡支付有单独的支付接口，不调用统一下单接口*/

		this.trade_type = trade_type;
		this.openid = openid;
		// 根据API给的签名规则进行签名
		Integer apptype = 1;   // 1-APP支付
		if (trade_type.equals("JSAPI"))
		{
			apptype = 2;    //2-公众号支付
		}
		this.sign =  Signature.getSign(apptype,toMap());

	}
	
	/**
	 * 预支付请求需要的参数（获取微信统一下单-必填参数）(小程序)
	 * 公众账号ID 	appid （内置）
	 * 商户号  	mch_id （内置）
	 * 随机字符串  	nonce_str （内置）
	 * 签名 	sign （内置）
	 * @param body				商品描述 
	 * @param out_trade_no		商户订单号
	 * @param total_fee			总金额 （单位：分）
	 * @param notify_url		通知地址
	 * @throws IllegalAccessException 
	 */
	public UnifiedorderReqData(String out_trade_no, Double total_fee, String notify_url,String trade_type,String openid) throws IllegalAccessException{

		//appID与商户号
		this.appid = Configure.getXappid();

		//微信支付分配的商户号ID（开通公众号的微信支付功能之后可以获取到）
		this.mch_id = Configure.getXmchid();

		//随机字符串，不长于32 位
		//        setNonce_str(RandomStringGenerator.getRandomStringByLength(32));
		this.nonce_str = RandomStringGenerator.getRandomStringByLength(32);


		//		setBody(body);// 商品描述
		this.body = "小程序支付";

		//		setOut_trade_no(out_trade_no);// 商户订单号
		this.out_trade_no = out_trade_no;

		//		setTotal_fee();// 总金额
		String money=SinataUtil.doubleRetainTwo(total_fee*100d);
		this.total_fee = Integer.parseInt(money.substring(0,money.length()-3));
		//this.total_fee = (int)(total_fee * 100);

		//		setNotify_url(notify_url);// 通知地址
		this.notify_url = notify_url;
		/*trade_type 交易类型JSAPI--公众号支付、NATIVE--原生扫码支付、APP--app支付，统一下单接口trade_type的传参可参考这里
		MICROPAY--刷卡支付，刷卡支付有单独的支付接口，不调用统一下单接口*/

		this.trade_type = trade_type;
		this.openid = openid;
		// 根据API给的签名规则进行签名
		this.sign =  Signature.getSign(1,toMap());

	}

	public Map<String,Object> toMap(){
		Map<String,Object> map = new HashMap<String, Object>();
		Field[] fields = this.getClass().getDeclaredFields();
		for (Field field : fields) {
			Object obj;
			try {
				obj = field.get(this);
				if(obj!=null){
					map.put(field.getName(), obj);
				}
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		}
		return map;
	}

	public String getAppid() {
		return appid;
	}

	public void setAppid(String appid) {
		this.appid = appid;
	}

	public String getMch_id() {
		return mch_id;
	}

	public void setMch_id(String mch_id) {
		this.mch_id = mch_id;
	}

	public String getNonce_str() {
		return nonce_str;
	}

	public void setNonce_str(String nonce_str) {
		this.nonce_str = nonce_str;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public String getOut_trade_no() {
		return out_trade_no;
	}

	public void setOut_trade_no(String out_trade_no) {
		this.out_trade_no = out_trade_no;
	}

	public Integer getTotal_fee() {
		return total_fee;
	}

	public void setTotal_fee(Integer total_fee) {
		this.total_fee = total_fee;
	}

	public String getSpbill_create_ip() {
		return spbill_create_ip;
	}

	public void setSpbill_create_ip(String spbill_create_ip) {
		this.spbill_create_ip = spbill_create_ip;
	}

	public String getNotify_url() {
		return notify_url;
	}

	public void setNotify_url(String notify_url) {
		this.notify_url = notify_url;
	}

	public String getTrade_type() {
		return trade_type;
	}

	public void setTrade_type(String trade_type) {
		this.trade_type = trade_type;
	}


	public String getOpenid() {
		return openid;
	}


	public void setOpenid(String openid) {
		this.openid = openid;
	}

}