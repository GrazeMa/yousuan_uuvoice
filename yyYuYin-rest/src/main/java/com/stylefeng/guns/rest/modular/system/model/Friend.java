package com.stylefeng.guns.rest.modular.system.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 好友
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-16
 */
@TableName("app_friend")
public class Friend extends Model<Friend> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 加好友id
     */
    private Integer uid;
    /**
     * 被加好友id
     */
    private Integer buid;
    /**
     * 创建时间
     */
    private Date createDate;
    /**
     * 状态(1 待加好友,2 好友,3:拉黑)
     */
    private Integer state;
    /**
     * 用户留下的话
     */
    private String content;
    /**
     * 是否 之前是好友
     */
    private Integer isType;
    /**
     * 备注
     */
    private String mark;
    
    
    private Integer isDelete;
    
    


    public Integer getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Integer isDelete) {
		this.isDelete = isDelete;
	}

	public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public Integer getBuid() {
        return buid;
    }

    public void setBuid(Integer buid) {
        this.buid = buid;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getIsType() {
        return isType;
    }

    public void setIsType(Integer isType) {
        this.isType = isType;
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "Friend{" +
        "id=" + id +
        ", uid=" + uid +
        ", buid=" + buid +
        ", createDate=" + createDate +
        ", state=" + state +
        ", content=" + content +
        ", isType=" + isType +
        ", mark=" + mark +
        "}";
    }
}
