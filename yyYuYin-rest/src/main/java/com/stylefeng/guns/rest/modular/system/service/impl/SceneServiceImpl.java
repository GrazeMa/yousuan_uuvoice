package com.stylefeng.guns.rest.modular.system.service.impl;

import com.stylefeng.guns.rest.modular.system.model.Scene;
import com.stylefeng.guns.rest.modular.system.dao.SceneMapper;
import com.stylefeng.guns.rest.modular.system.service.ISceneService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import java.util.List;

import org.springframework.stereotype.Service;

/**
 * <p>
 * 道具管理 服务实现类
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
@Service
public class SceneServiceImpl extends ServiceImpl<SceneMapper, Scene> implements ISceneService {

	@Override
	public List<Scene> getList(Scene model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getList(model);
	}

	@Override
	public Scene getOne(Scene model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getOne(model);
	}
	
	@Override
	public Scene getOne1(Scene model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getOne1(model);
	}

	@Override
	public List<Scene> getList1(Scene model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getList1(model);
	}

}
