package com.stylefeng.guns.rest.modular.system.service;

import com.stylefeng.guns.rest.modular.system.model.RoomUser;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 流水 服务类
 * </p>
 *
 * @author wumeng123
 * @since 2019-07-04
 */
public interface IRoomUserService extends IService<RoomUser> {
      Integer getSum(RoomUser RoomUser);
}
