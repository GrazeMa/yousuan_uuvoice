package com.stylefeng.guns.rest.modular.system.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 红包内容
 * </p>
 *
 * @author wumeng123
 * @since 2019-02-20
 */
@TableName("app_red_num")
public class RedNum extends Model<RedNum> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 房间id
     */
    private Date createTime;
    /**
     * 金币
     */
    private Integer gold;
    /**
     * 被送用户id
     */
    private Integer uid;
    /**
     * 是否被定时器 1否，2是
     */
    private Integer state;
    /**
     * 红包的id
     */
    private Integer redId;
    /**
     * 是否被领取红包 1 未，2  是
     */
    private Integer status;
    
    private String rid;
    
    private Date lqTime;
    
    


    public Date getLqTime() {
		return lqTime;
	}

	public void setLqTime(Date lqTime) {
		this.lqTime = lqTime;
	}

	public String getRid() {
		return rid;
	}

	public void setRid(String rid) {
		this.rid = rid;
	}

	public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getGold() {
        return gold;
    }

    public void setGold(Integer gold) {
        this.gold = gold;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Integer getRedId() {
        return redId;
    }

    public void setRedId(Integer redId) {
        this.redId = redId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "RedNum{" +
        "id=" + id +
        ", createTime=" + createTime +
        ", gold=" + gold +
        ", uid=" + uid +
        ", state=" + state +
        ", redId=" + redId +
        ", status=" + status +
        "}";
    }
}
