package com.stylefeng.guns.rest.modular.system.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 充值明细
 * </p>
 *
 * @author wumeng123
 * @since 2019-02-14
 */
@TableName("app_recharge")
public class Recharge extends Model<Recharge> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 充值时间
     */
    private Date createTime;
    /**
     * 用户id
     */
    private Integer uid;
    /**
     * 充值金额
     */
    private Double rechargeMoney;
    /**
     * 获得金币
     */
    private Integer restitution;
    /**
     * 总金额
     */
    private Integer sumMoney;
    /**
     * 0正常，1删除
     */
    private Integer isDelete;
    //赠送的金额
    private Integer giveMoney;
    //1 是第一次充值，2 不是第二次充值
    private Integer state;
    
    
    /**
     * 时间查询条件(开始)
     */
    @TableField(exist = false)
    private String beginTime;
    /**
     * 时间查询条件(结束)
     */
    @TableField(exist = false)
    private String endTime;
    
    
    
   

	public String getBeginTime() {
		return beginTime;
	}

	public void setBeginTime(String beginTime) {
		this.beginTime = beginTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public Integer getRestitution() {
		return restitution;
	}

	public void setRestitution(Integer restitution) {
		this.restitution = restitution;
	}

	public Integer getSumMoney() {
		return sumMoney;
	}

	public void setSumMoney(Integer sumMoney) {
		this.sumMoney = sumMoney;
	}

	public Integer getGiveMoney() {
		return giveMoney;
	}

	public void setGiveMoney(Integer giveMoney) {
		this.giveMoney = giveMoney;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public Double getRechargeMoney() {
        return rechargeMoney;
    }

    public void setRechargeMoney(Double rechargeMoney) {
        this.rechargeMoney = rechargeMoney;
    }

    

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "Recharge{" +
        "id=" + id +
        ", createTime=" + createTime +
        ", uid=" + uid +
        ", rechargeMoney=" + rechargeMoney +
        ", restitution=" + restitution +
        ", sumMoney=" + sumMoney +
        ", isDelete=" + isDelete +
        "}";
    }
}
