package com.stylefeng.guns.rest.modular.system.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.rest.modular.system.dao.SkillLevelMapper;
import com.stylefeng.guns.rest.modular.system.model.SkillLevel;
import com.stylefeng.guns.rest.modular.system.service.ISkillLevelService;

/**
 * @Description 技能段位实现类。
 * @author Grazer_Ma
 * @Date 2020-06-05 10:07:38
 */
@Service
public class SkillLevelServiceImpl extends ServiceImpl<SkillLevelMapper, SkillLevel> implements ISkillLevelService {

	@Override
	public List<SkillLevel> getList(SkillLevel skillLevelModel) {
		return this.baseMapper.getList(skillLevelModel);
	}

	@Override
	public SkillLevel getOne(SkillLevel skillLevelModel) {
		return this.baseMapper.getOne(skillLevelModel);
	}

}
