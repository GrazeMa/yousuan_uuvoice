package com.stylefeng.guns.rest.modular.system.service;

import java.util.List;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.rest.modular.system.model.Headline;

/**
 * @Description 头条服务类。
 * @author Grazer_Ma
 * @Date 2020-05-21 23:05:05
 */
public interface IHeadlineService extends IService<Headline> {

	public List<Headline> getList(Headline model);

	Headline getOne(Headline model);

	Headline getOne2(Headline model);
	
}
