package com.stylefeng.guns.rest.modular.system.dao;

import com.stylefeng.guns.rest.modular.system.model.Treasure;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 房间财富值 Mapper 接口
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
public interface TreasureMapper extends BaseMapper<Treasure> {

	List<Treasure> getList(Treasure model);

	Treasure getOne(Treasure model);

	List<Treasure> getSum(Treasure model);

	List<Treasure> getSumH(Treasure model);

}
