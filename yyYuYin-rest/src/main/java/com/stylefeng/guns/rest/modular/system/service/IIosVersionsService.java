package com.stylefeng.guns.rest.modular.system.service;

import com.stylefeng.guns.rest.modular.system.model.IosVersions;


import java.util.List;

import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * ios版本控制 服务类
 * </p>
 *
 * @author wumeng123
 * @since 2019-05-17
 */
public interface IIosVersionsService extends IService<IosVersions> {
	public List<IosVersions> getList(IosVersions model);
    
    IosVersions getOne(IosVersions model);
}
