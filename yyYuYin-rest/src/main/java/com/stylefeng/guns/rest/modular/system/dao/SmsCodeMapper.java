package com.stylefeng.guns.rest.modular.system.dao;

import com.stylefeng.guns.rest.modular.system.model.SmsCode;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;


/**
 * <p>
 * 短信验证码 Mapper 接口
 * </p>
 *
 * @author wumeng
 * @since 2018-05-22
 */
public interface SmsCodeMapper extends BaseMapper<SmsCode> {

	SmsCode getByPhone( @Param("phone") String phone);
	
}
