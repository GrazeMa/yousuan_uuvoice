package com.stylefeng.guns.rest.modular.system.service;

import com.stylefeng.guns.rest.modular.system.model.Paylog;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 支付记录 服务类
 * </p>
 *
 * @author wumeng123
 * @since 2019-02-14
 */
public interface IPaylogService extends IService<Paylog> {	

}
