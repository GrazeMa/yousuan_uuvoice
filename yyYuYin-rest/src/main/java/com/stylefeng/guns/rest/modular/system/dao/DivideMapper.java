package com.stylefeng.guns.rest.modular.system.dao;

import com.stylefeng.guns.rest.modular.system.model.Divide;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 充值分成金额 Mapper 接口
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
public interface DivideMapper extends BaseMapper<Divide> {

	List<Divide> getList(Divide model);

	Divide getOne(Divide model);

	Double getSum(Divide model);

}
