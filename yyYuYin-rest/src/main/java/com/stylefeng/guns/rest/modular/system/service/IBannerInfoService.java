package com.stylefeng.guns.rest.modular.system.service;

import com.stylefeng.guns.rest.modular.system.model.BannerInfo;
import com.stylefeng.guns.rest.modular.system.model.BannerInfo;

import java.util.List;

import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * Banner信息 服务类
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
public interface IBannerInfoService extends IService<BannerInfo> {
public List<BannerInfo> getList(BannerInfo model);
    
    BannerInfo getOne(BannerInfo model);
}
