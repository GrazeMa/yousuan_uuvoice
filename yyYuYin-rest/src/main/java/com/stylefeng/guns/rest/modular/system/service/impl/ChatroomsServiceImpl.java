package com.stylefeng.guns.rest.modular.system.service.impl;

import com.stylefeng.guns.rest.modular.system.model.Chatrooms;
import com.stylefeng.guns.rest.modular.system.dao.ChatroomsMapper;
import com.stylefeng.guns.rest.modular.system.service.IChatroomsService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import java.util.List;

import org.springframework.stereotype.Service;

/**
 * <p>
 * 加入聊天室的 服务实现类
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
@Service
public class ChatroomsServiceImpl extends ServiceImpl<ChatroomsMapper, Chatrooms> implements IChatroomsService {

	@Override
	public List<Chatrooms> getList(Chatrooms Chatrooms) {
		// TODO Auto-generated method stub
		return this.baseMapper.getList(Chatrooms);
	}

	@Override
	public List<Chatrooms> getList1(Chatrooms Chatrooms) {
		// TODO Auto-generated method stub
		return this.baseMapper.getList1(Chatrooms);
	}

	@Override
	public Chatrooms getOne(Chatrooms Chatrooms) {
		// TODO Auto-generated method stub
		return this.baseMapper.getOne(Chatrooms);
	}

	@Override
	public List<Chatrooms> getList2(Chatrooms Chatrooms) {
		// TODO Auto-generated method stub
		return this.baseMapper.getList2(Chatrooms);
	}

}
