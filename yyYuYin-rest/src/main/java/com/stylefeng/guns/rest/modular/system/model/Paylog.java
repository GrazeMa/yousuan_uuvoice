package com.stylefeng.guns.rest.modular.system.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 支付记录
 * </p>
 *
 * @author wumeng123
 * @since 2019-02-14
 */
@TableName("app_paylog")
public class Paylog extends Model<Paylog> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 商户订单号
     */
    private String outTradeNo;
    private String tradeNo;
    private String buyerId;
    /**
     * 用户ID
     */
    private Integer uid;
    /**
     * 支付类型 1=支付宝,2 = 微信
     */
    private Integer payType;
    /**
     * 支付金额
     */
    private Double payMoney;
    /**
     * 状态(1=有效，2=无效，3=退款)
     */
    private Integer state;
    private Date createTime;
    /**
     * 支付账号
     */
    private String payAccount;
    /**
     * 支付状态 
     */
    private String tradeStatus;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOutTradeNo() {
        return outTradeNo;
    }

    public void setOutTradeNo(String outTradeNo) {
        this.outTradeNo = outTradeNo;
    }

    public String getTradeNo() {
        return tradeNo;
    }

    public void setTradeNo(String tradeNo) {
        this.tradeNo = tradeNo;
    }

    public String getBuyerId() {
        return buyerId;
    }

    public void setBuyerId(String buyerId) {
        this.buyerId = buyerId;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public Integer getPayType() {
        return payType;
    }

    public void setPayType(Integer payType) {
        this.payType = payType;
    }

    public Double getPayMoney() {
        return payMoney;
    }

    public void setPayMoney(Double payMoney) {
        this.payMoney = payMoney;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getPayAccount() {
        return payAccount;
    }

    public void setPayAccount(String payAccount) {
        this.payAccount = payAccount;
    }

    public String getTradeStatus() {
        return tradeStatus;
    }

    public void setTradeStatus(String tradeStatus) {
        this.tradeStatus = tradeStatus;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "Paylog{" +
        "id=" + id +
        ", outTradeNo=" + outTradeNo +
        ", tradeNo=" + tradeNo +
        ", buyerId=" + buyerId +
        ", uid=" + uid +
        ", payType=" + payType +
        ", payMoney=" + payMoney +
        ", state=" + state +
        ", createTime=" + createTime +
        ", payAccount=" + payAccount +
        ", tradeStatus=" + tradeStatus +
        "}";
    }
}
