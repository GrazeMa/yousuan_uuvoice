package com.stylefeng.guns.rest.modular.system.service;

import com.stylefeng.guns.rest.modular.system.model.Images;
import com.stylefeng.guns.rest.modular.system.model.Images;

import java.util.List;

import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 图片 服务类
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
public interface IImagesService extends IService<Images> {
public List<Images> getList(Images model);
    
    Images getOne(Images model);
}
