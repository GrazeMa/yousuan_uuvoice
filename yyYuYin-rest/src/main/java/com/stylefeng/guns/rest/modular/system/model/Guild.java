package com.stylefeng.guns.rest.modular.system.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;
import java.util.Date;

/**
 * @Description 公会 Entity。
 * @author Grazer_Ma
 * @Date 2020-05-19 09:38:10
 */
@TableName("app_guild")
public class Guild extends Model<Guild> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 用户主键
     */
    private Integer uid;
    /**
     * 公会分组主键
     */
    private Integer gid;
    /**
     * 状态（0：待审核；1：已审核；2：被踢出公会）
     */
    private Integer status;
    /**
     * 注册时间
     */
    private String createDate;
    
    @TableField(exist = false)
    private String usercoding;
    
    @TableField(exist = false)
    private String nickname;
    
    @TableField(exist = false)
    private String phone;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public Integer getGid() {
        return gid;
    }

    public void setGid(Integer gid) {
        this.gid = gid;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
    
    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }
    
    public String getUsercoding() {
        return usercoding;
    }

    public void setUsercoding(String usercoding) {
        this.usercoding = usercoding;
    }
    
    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }
    
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
    
    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "Guild{" +
        "id=" + id +
        ", uid=" + uid +
        ", gid=" + gid +
        ", status=" + status +
        ", createDate=" + createDate +
        ", usercoding=" + usercoding +
        ", nickname=" + nickname +
        ", phone=" + phone +
        "}";
    }
}
