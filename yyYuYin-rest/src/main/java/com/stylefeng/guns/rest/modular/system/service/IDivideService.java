package com.stylefeng.guns.rest.modular.system.service;

import com.stylefeng.guns.rest.modular.system.model.Divide;
import com.stylefeng.guns.rest.modular.system.model.Divide;

import java.util.List;

import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 充值分成金额 服务类
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
public interface IDivideService extends IService<Divide> {
public List<Divide> getList(Divide model);
    
    Divide getOne(Divide model);
    
    Double getSum(Divide model);
}
