package com.stylefeng.guns.rest.modular.system.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 音乐审核
 * </p>
 *
 * @author wumeng123
 * @since 2019-03-05
 */
@TableName("app_music")
public class Music extends Model<Music> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    private Date createDate;
    /**
     * 名称
     */
    private String musicName;
    private String gsNane;
    /**
     * 音乐路径
     */
    private String url;
    private Integer uid;
    /**
     * 1 未审核， 2 审核通过 ，3已拒绝
     */
    private Integer state;
    /**
     * 1 否,2 是
     */
    private Integer isDelete;
    
    
    private Integer status;
    /**
     * 音乐时长
     */
    private Integer times;
    /**
     * 下载音乐的key
     */
    @TableField(exist = false)
    private String  objectKey;
    
    private Integer downloads;
    
    
    
    
    

    public Integer getDownloads() {
		return downloads;
	}

	public void setDownloads(Integer downloads) {
		this.downloads = downloads;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getMusicName() {
        return musicName;
    }

    public void setMusicName(String musicName) {
        this.musicName = musicName;
    }

    public String getGsNane() {
        return gsNane;
    }

    public void setGsNane(String gsNane) {
        this.gsNane = gsNane;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    public String getObjectKey() {
        return objectKey;
    }

    public void setObjectKey(String objectKey) {
        this.objectKey = objectKey;
    }

    public Integer getTimes() {
        return times;
    }

    public void setTimes(Integer times) {
        this.times = times;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "Music{" +
        "id=" + id +
        ", createDate=" + createDate +
        ", musicName=" + musicName +
        ", gsNane=" + gsNane +
        ", url=" + url +
        ", uid=" + uid +
        ", state=" + state +
        ", isDelete=" + isDelete +
        "}";
    }
}
