package com.stylefeng.guns.rest.modular.system.dao;

import com.stylefeng.guns.rest.modular.system.model.Userlist;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 用户邀请的排行 Mapper 接口
 * </p>
 *
 * @author wumeng123
 * @since 2019-04-24
 */
public interface UserlistMapper extends BaseMapper<Userlist> {

	List<Userlist> getList(Userlist model);

	Userlist getOne(Userlist model);

}
