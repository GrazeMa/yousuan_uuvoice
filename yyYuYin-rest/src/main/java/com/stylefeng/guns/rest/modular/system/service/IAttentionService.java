package com.stylefeng.guns.rest.modular.system.service;


import com.stylefeng.guns.rest.modular.system.model.Attention;

import java.util.List;

import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wumeng123
 * @since 2019-02-18
 */
public interface IAttentionService extends IService<Attention> {
	 public List<Attention> getList(Attention model);
	    
	    Attention getOne(Attention model);
}
