package com.stylefeng.guns.rest.modular.system.service.impl;

import com.stylefeng.guns.rest.modular.system.model.GiftRecord;
import com.stylefeng.guns.rest.modular.system.dao.GiftRecordMapper;
import com.stylefeng.guns.rest.modular.system.service.IGiftRecordService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import java.util.List;

import org.springframework.stereotype.Service;

/**
 * <p>
 * 礼物记录 服务实现类
 * </p>
 *
 * @author wumeng123
 * @since 2019-03-07
 */
@Service
public class GiftRecordServiceImpl extends ServiceImpl<GiftRecordMapper, GiftRecord> implements IGiftRecordService {

	@Override
	public List<GiftRecord> getList(GiftRecord model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getList(model);
	}

	@Override
	public List<GiftRecord> getList1(GiftRecord model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getList1(model);
	}

}
