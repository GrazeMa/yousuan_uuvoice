
package com.stylefeng.guns.rest.modular.system.model;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 用户的背包
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
@TableName("app_user_bag")
public class UserBag extends Model<UserBag> {

    private static final long serialVersionUID = 1L;
    
    @Override
	protected Serializable pkVal() {
		return this.uid;
	}
    
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    
    private Integer uid;
    private Integer gid;
    private Integer num;
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    
	public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

	public Integer getGid() {
        return gid;
    }

    public void setGid(Integer gid) {
        this.gid = gid;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }
    
    @Override
    public String toString() {
        return "UserBag{" +
        "uid=" + uid +
        "gid=" + gid +
        "num=" + num +
        "}";
    }

	
}
