package com.stylefeng.guns.rest.modular.system.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 充值设置
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
@TableName("app_set_recharge")
public class SetRecharge extends Model<SetRecharge> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    private Integer x;
    private Double y;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getX() {
        return x;
    }

    public void setX(Integer x) {
        this.x = x;
    }

    

    public Double getY() {
		return y;
	}

	public void setY(Double y) {
		this.y = y;
	}

	@Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "SetRecharge{" +
        "id=" + id +
        ", x=" + x +
        ", y=" + y +
        "}";
    }
}
