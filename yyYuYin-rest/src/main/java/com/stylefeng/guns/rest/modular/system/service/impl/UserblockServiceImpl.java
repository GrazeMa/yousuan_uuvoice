package com.stylefeng.guns.rest.modular.system.service.impl;

import com.stylefeng.guns.rest.modular.system.model.Userblock;
import com.stylefeng.guns.rest.modular.system.dao.UserblockMapper;
import com.stylefeng.guns.rest.modular.system.service.IUserblockService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import java.util.List;

import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户拉黑 服务实现类
 * </p>
 *
 * @author wumeng123
 * @since 2019-03-21
 */
@Service
public class UserblockServiceImpl extends ServiceImpl<UserblockMapper, Userblock> implements IUserblockService {

	@Override
	public List<Userblock> getList(Userblock model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getList(model);
	}

	@Override
	public Userblock getOne(Userblock model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getOne(model);
	}

}
