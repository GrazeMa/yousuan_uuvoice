package com.stylefeng.guns.rest.modular.system.service;

import java.util.List;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.rest.modular.system.model.SkillPrice;

/**
 * @Description 技能价格服务类。
 * @author Grazer_Ma
 * @Date 2020-06-04 22:29:40
 */
public interface ISkillPriceService extends IService<SkillPrice> {

	public List<SkillPrice> getList(SkillPrice skillPriceModel);

	SkillPrice getOne(SkillPrice skillPriceModel);

}
