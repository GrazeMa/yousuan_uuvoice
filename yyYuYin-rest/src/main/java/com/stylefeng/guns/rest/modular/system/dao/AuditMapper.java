package com.stylefeng.guns.rest.modular.system.dao;

import com.stylefeng.guns.rest.modular.system.model.Audit;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 实名认证 Mapper 接口
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
public interface AuditMapper extends BaseMapper<Audit> {

	List<Audit> getList(Audit model);

	Audit getOne(Audit model);

}
