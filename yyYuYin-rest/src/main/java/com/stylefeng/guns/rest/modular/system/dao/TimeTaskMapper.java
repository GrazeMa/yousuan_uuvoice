package com.stylefeng.guns.rest.modular.system.dao;

import com.stylefeng.guns.rest.modular.system.model.TimeTask;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 定时任务数据 Mapper 接口
 * </p>
 *
 * @author lyouyou123
 * @since 2018-11-30
 */
public interface TimeTaskMapper extends BaseMapper<TimeTask> {

}
