package com.stylefeng.guns.rest.modular.system.service.impl;

import com.stylefeng.guns.rest.modular.system.model.BannerInfo;
import com.stylefeng.guns.rest.modular.system.dao.BannerInfoMapper;
import com.stylefeng.guns.rest.modular.system.service.IBannerInfoService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import java.util.List;

import org.springframework.stereotype.Service;

/**
 * <p>
 * Banner信息 服务实现类
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
@Service
public class BannerInfoServiceImpl extends ServiceImpl<BannerInfoMapper, BannerInfo> implements IBannerInfoService {

	@Override
	public List<BannerInfo> getList(BannerInfo model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getList(model);
	}

	@Override
	public BannerInfo getOne(BannerInfo model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getOne(model);
	}

}
