package com.stylefeng.guns.rest.modular.system.controller.model;

import java.util.Date;
import java.util.List;

public class TimeWithdrawModel {
	private Date createTime;

	private List<WithdrawModel> Withdraw;

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public List<WithdrawModel> getWithdraw() {
		return Withdraw;
	}

	public void setWithdraw(List<WithdrawModel> withdraw) {
		Withdraw = withdraw;
	}

}
