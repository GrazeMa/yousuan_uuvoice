package com.stylefeng.guns.rest.modular.system.controller.model;

import java.util.Date;

public class PKModel {
	private Integer state;
	private Date createTime;

	private Integer second;

	private PKNumModel wz1;

	private PKNumModel wz2;

	public Integer getSecond() {
		return second;
	}

	public void setSecond(Integer second) {
		this.second = second;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public PKNumModel getWz1() {
		return wz1;
	}

	public void setWz1(PKNumModel wz1) {
		this.wz1 = wz1;
	}

	public PKNumModel getWz2() {
		return wz2;
	}

	public void setWz2(PKNumModel wz2) {
		this.wz2 = wz2;
	}

}
