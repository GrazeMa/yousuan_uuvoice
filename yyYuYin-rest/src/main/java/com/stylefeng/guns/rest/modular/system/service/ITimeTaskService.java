package com.stylefeng.guns.rest.modular.system.service;

import com.stylefeng.guns.rest.modular.system.model.TimeTask;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 定时任务数据 服务类
 * </p>
 *
 * @author lyouyou123
 * @since 2018-11-30
 */
public interface ITimeTaskService extends IService<TimeTask> {

}
