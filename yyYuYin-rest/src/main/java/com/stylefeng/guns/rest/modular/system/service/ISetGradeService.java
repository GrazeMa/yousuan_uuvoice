package com.stylefeng.guns.rest.modular.system.service;

import com.stylefeng.guns.rest.modular.system.model.SetGrade;
import com.stylefeng.guns.rest.modular.system.model.SetGrade;

import java.util.List;

import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 等级设置 服务类
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
public interface ISetGradeService extends IService<SetGrade> {
public List<SetGrade> getList(SetGrade model);
    
    SetGrade getOne(SetGrade model);
    
    SetGrade getJie(SetGrade model);
}
