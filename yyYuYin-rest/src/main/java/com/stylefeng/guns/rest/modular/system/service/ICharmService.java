package com.stylefeng.guns.rest.modular.system.service;

import com.stylefeng.guns.rest.modular.system.model.Charm;
import com.stylefeng.guns.rest.modular.system.model.Charm;

import java.util.List;

import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 房间魅力榜 服务类
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
public interface ICharmService extends IService<Charm> {
public List<Charm> getList(Charm model);
    
    Charm getOne(Charm model);
    
    List<Charm> getSum(Charm model);
    
    List<Charm> getSum2(Charm model);
    
    Charm getSum1(Charm model);
    
    List<Charm> getSumH(Charm model);
    
    List<Charm> getSum3(Charm model);
}
