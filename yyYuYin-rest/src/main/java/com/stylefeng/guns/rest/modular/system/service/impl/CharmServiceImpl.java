package com.stylefeng.guns.rest.modular.system.service.impl;

import com.stylefeng.guns.rest.modular.system.model.Charm;
import com.stylefeng.guns.rest.modular.system.dao.CharmMapper;
import com.stylefeng.guns.rest.modular.system.service.ICharmService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import java.util.List;

import org.springframework.stereotype.Service;

/**
 * <p>
 * 房间魅力榜 服务实现类
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
@Service
public class CharmServiceImpl extends ServiceImpl<CharmMapper, Charm> implements ICharmService {

	@Override
	public List<Charm> getList(Charm model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getList(model);
	}

	@Override
	public Charm getOne(Charm model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getOne(model);
	}

	@Override
	public List<Charm> getSum(Charm model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getSum(model);
	}
	
	

	@Override
	public Charm getSum1(Charm model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getSum1(model);
	}

	@Override
	public List<Charm> getSum2(Charm model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getSum2(model);
	}

	@Override
	public List<Charm> getSumH(Charm model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getSumH(model);
	}
	
	@Override
	public List<Charm> getSum3(Charm model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getSum3(model);
	}

}
