package com.stylefeng.guns.rest.modular.system.dao;

import com.stylefeng.guns.rest.modular.system.model.Logbook;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 日志 Mapper 接口
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-16
 */
public interface LogbookMapper extends BaseMapper<Logbook> {

	List<Logbook> getList(Logbook model);

	Logbook getOne(Logbook model);

}
