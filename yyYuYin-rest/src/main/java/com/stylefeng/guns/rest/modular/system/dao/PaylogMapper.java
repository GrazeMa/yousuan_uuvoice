package com.stylefeng.guns.rest.modular.system.dao;

import com.stylefeng.guns.rest.modular.system.model.Paylog;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 支付记录 Mapper 接口
 * </p>
 *
 * @author wumeng123
 * @since 2019-02-14
 */
public interface PaylogMapper extends BaseMapper<Paylog> {

}
