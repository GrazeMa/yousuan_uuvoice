package com.stylefeng.guns.rest.modular.system.service.impl;

import com.stylefeng.guns.rest.modular.system.model.User;
import com.stylefeng.guns.rest.modular.system.dao.UserMapper;
import com.stylefeng.guns.rest.modular.system.service.IUserService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import java.util.List;

import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户 服务实现类
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {

	@Override
	public User getByPhone(String phone) {
		// TODO Auto-generated method stub
		return this.baseMapper.getByPhone(phone);
	}

	@Override
	public User getByWXsid(String phone) {
		// TODO Auto-generated method stub
		return this.baseMapper.getByWXsid(phone);
	}

	@Override
	public User getByQQsid(String phone) {
		// TODO Auto-generated method stub
		return this.baseMapper.getByQQsid(phone);
	}

	@Override
	public User getByGZsid(String phone) {
		// TODO Auto-generated method stub
		return this.baseMapper.getByGZsid(phone);
	}
	
	@Override
	public User getByRoomID(String usercoding) {
		return this.baseMapper.getByRoomID(usercoding);
	}

	@Override
	public List<User> getList(User model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getList(model);
	}
	
	@Override
	public User getOne(User model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getOne(model);
	}


	@Override
	public List<User> getList1(User model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getList1(model);
	}

	@Override
	public List<User> getList2(User model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getList2(model);
	}
	
	@Override
	public List<User> getList3(User model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getList3(model);
	}


}
