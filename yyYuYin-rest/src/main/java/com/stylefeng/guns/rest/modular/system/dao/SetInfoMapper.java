package com.stylefeng.guns.rest.modular.system.dao;

import com.stylefeng.guns.rest.modular.system.model.SetInfo;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 系统设置 Mapper 接口
 * </p>
 *
 * @author wumeng
 * @since 2018-05-23
 */
public interface SetInfoMapper extends BaseMapper<SetInfo> {

	List<SetInfo> getList(SetInfo model);

	SetInfo getOne(SetInfo model);

}
