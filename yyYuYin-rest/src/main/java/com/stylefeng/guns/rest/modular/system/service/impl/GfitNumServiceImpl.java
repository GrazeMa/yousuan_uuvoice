package com.stylefeng.guns.rest.modular.system.service.impl;

import com.stylefeng.guns.rest.modular.system.model.GfitNum;
import com.stylefeng.guns.rest.modular.system.model.Gift;
import com.stylefeng.guns.rest.modular.system.dao.GfitNumMapper;
import com.stylefeng.guns.rest.modular.system.service.IGfitNumService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import java.util.List;

import org.springframework.stereotype.Service;

/**
 * <p>
 * 送的礼物，自己购买的头环，座驾，自己开宝箱得道的礼物，头环，座驾，  服务实现类
 * </p>
 *
 * @author wumeng123
 * @since 2019-03-02
 */
@Service
public class GfitNumServiceImpl extends ServiceImpl<GfitNumMapper, GfitNum> implements IGfitNumService {

	@Override
	public List<GfitNum> getList(GfitNum model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getList(model);
	}

	@Override
	public List<GfitNum> getList1(GfitNum model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getList1(model);
	}

}
