package com.stylefeng.guns.rest.modular.system.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 实名认证
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
@TableName("app_audit")
public class Audit extends Model<Audit> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    private Date createTime;
    /**
     * 优优id
     */
    private String eid;
    /**
     * 昵称
     */
    private String name;
    /**
     * 姓名
     */
    private String xm;
    /**
     * 身份证
     */
    private String card;
    /**
     * 1 待审核，2 审核通过， 3 已拒绝
     */
    private String state;
    /**
     * 备注
     */
    private String mark;
    /**
     * '是否被删除（1=否  2=是）'
     */
    private Integer isDelete;
    
    
    private Integer uid;
    
    private String photoUrl;
    


    public Integer getUid() {
		return uid;
	}

	public void setUid(Integer uid) {
		this.uid = uid;
	}

	public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getEid() {
        return eid;
    }

    public void setEid(String eid) {
        this.eid = eid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getXm() {
        return xm;
    }

    public void setXm(String xm) {
        this.xm = xm;
    }

    public String getCard() {
        return card;
    }

    public void setCard(String card) {
        this.card = card;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }
    
    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "Audit{" +
        "id=" + id +
        ", createTime=" + createTime +
        ", eid=" + eid +
        ", name=" + name +
        ", xm=" + xm +
        ", card=" + card +
        ", state=" + state +
        ", mark=" + mark +
        ", isDelete=" + isDelete +
        ", photoUrl=" + photoUrl +
        "}";
    }
}
