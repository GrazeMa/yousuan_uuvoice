package com.stylefeng.guns.rest.modular.system.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 协议号
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
@TableName("app_agreement")
public class Agreement extends Model<Agreement> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    private Date createTime;
    /**
     * 用户优优号
     */
    private String usercoding;
    /**
     * 个性签名
     */
    private String individuation;
    /**
     * 性别(1 男, 2 女)
     */
    private Integer sex;
    /**
     * 出生日期
     */
    private String dateOfBirth;
    /**
     * 昵称
     */
    private String nickname;
    /**
     * 当前房间id
     */
    private Integer eid;
    /**
     * 是否分配 1否，2 是
     */
    private String state;
    /**
     * '是否被删除（1=否  2=是）'
     */
    private Integer isDelete;
    
    private String img;
    
    


    public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getUsercoding() {
        return usercoding;
    }

    public void setUsercoding(String usercoding) {
        this.usercoding = usercoding;
    }

    public String getIndividuation() {
        return individuation;
    }

    public void setIndividuation(String individuation) {
        this.individuation = individuation;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public Integer getEid() {
        return eid;
    }

    public void setEid(Integer eid) {
        this.eid = eid;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "Agreement{" +
        "id=" + id +
        ", createTime=" + createTime +
        ", usercoding=" + usercoding +
        ", individuation=" + individuation +
        ", sex=" + sex +
        ", dateOfBirth=" + dateOfBirth +
        ", nickname=" + nickname +
        ", eid=" + eid +
        ", state=" + state +
        ", isDelete=" + isDelete +
        "}";
    }
}
