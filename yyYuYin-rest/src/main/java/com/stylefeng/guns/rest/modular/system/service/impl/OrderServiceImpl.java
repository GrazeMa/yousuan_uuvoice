package com.stylefeng.guns.rest.modular.system.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.rest.modular.system.dao.OrderMapper;
import com.stylefeng.guns.rest.modular.system.model.Order;
import com.stylefeng.guns.rest.modular.system.service.IOrderService;

/**
 * @Description 订单实现类。
 * @author Grazer_Ma
 * @Date 2020-06-04 22:31:31
 */
@Service
public class OrderServiceImpl extends ServiceImpl<OrderMapper, Order> implements IOrderService {

	@Override
	public List<Order> getList(Order orderModel) {
		return this.baseMapper.getList(orderModel);
	}

	@Override
	public Order getOne(Order orderModel) {
		return this.baseMapper.getOne(orderModel);
	}

}
