package com.stylefeng.guns.rest.modular.system.controller.model;

public class LotteryModel {
	// 礼物id
	private Integer id;
	// 图片
	private String img;

	//
	private String imgFM;

	// 礼物名称
	private String name;
	// 时间
	private Integer day;

	// 类型
	private Integer type;

	private Integer count;

	private Integer price;

	public String getImgFM() {
		return imgFM;
	}

	public void setImgFM(String imgFM) {
		this.imgFM = imgFM;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getDay() {
		return day;
	}

	public void setDay(Integer day) {
		this.day = day;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public Integer getPrice() {
		return price;
	}

	public void setPrice(Integer price) {
		this.price = price;
	}

}
