package com.stylefeng.guns.rest.modular.system.controller.model;

public class AuctionModel {
	private Integer id;
	private String name;
	private Integer sex;
	private String img;
	private Integer num;
	private Integer Grade;

	CharmModel charmModels;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getSex() {
		return sex;
	}

	public void setSex(Integer sex) {
		this.sex = sex;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public Integer getNum() {
		return num;
	}

	public void setNum(Integer num) {
		this.num = num;
	}

	public Integer getGrade() {
		return Grade;
	}

	public void setGrade(Integer grade) {
		Grade = grade;
	}

	public CharmModel getCharmModels() {
		return charmModels;
	}

	public void setCharmModels(CharmModel charmModels) {
		this.charmModels = charmModels;
	}

}
