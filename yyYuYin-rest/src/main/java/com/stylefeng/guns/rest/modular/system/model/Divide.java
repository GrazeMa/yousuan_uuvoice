package com.stylefeng.guns.rest.modular.system.model;

import java.io.Serializable;

import java.util.Date;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 充值分成金额
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
@TableName("app_divide")
public class Divide extends Model<Divide> {

    private static final long serialVersionUID = 1L;

    private Integer id;
    /**
     * 邀请人id
     */
    private Integer uid;
    /**
     * 被邀请用户id
     */
    private Integer buid;
    private Date createTime;
    /**
     * 邀请奖励
     */
    private Double money;
    /**
     * 充值金额
     */
    private Double czMoney;
    /**
     * '是否被删除（1=否  2=是）'
     */
    private Integer isDelete;
    
    
    /**
     * 时间查询条件(开始)
     */
    @TableField(exist = false)
    private String beginTime;
    /**
     * 时间查询条件(结束)
     */
    @TableField(exist = false)
    private String endTime;
    
    


    public String getBeginTime() {
		return beginTime;
	}

	public void setBeginTime(String beginTime) {
		this.beginTime = beginTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public Integer getBuid() {
        return buid;
    }

    public void setBuid(Integer buid) {
        this.buid = buid;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Double getMoney() {
        return money;
    }

    public void setMoney(Double money) {
        this.money = money;
    }

    public Double getCzMoney() {
        return czMoney;
    }

    public void setCzMoney(Double czMoney) {
        this.czMoney = czMoney;
    }

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "Divide{" +
        "id=" + id +
        ", uid=" + uid +
        ", buid=" + buid +
        ", createTime=" + createTime +
        ", money=" + money +
        ", czMoney=" + czMoney +
        ", isDelete=" + isDelete +
        "}";
    }
}
