package com.stylefeng.guns.rest.modular.system.dao;

import com.stylefeng.guns.rest.modular.system.model.SetRecharge;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 充值设置 Mapper 接口
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
public interface SetRechargeMapper extends BaseMapper<SetRecharge> {

	List<SetRecharge> getList(SetRecharge model);

	SetRecharge getOne(SetRecharge model);

}
