package com.stylefeng.guns.rest.modular.system.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import com.stylefeng.guns.rest.modular.system.controller.model.UserModel;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 房间座位相关设置
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
@TableName("app_seat")
public class Seat extends Model<Seat> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     *  是否禁麦此座位  1否， 2 是
     */
    private Integer state;
    /**
     * 是否封锁此座位 1否， 2是
     */
    private Integer status;
    /**
     * 房间号
     */
    private String pid;
    /**
     * 用户id
     */
    private Integer uid;
    /**
     * 麦顺序
     */
    private Integer sequence;
    
    
    private UserModel userModel;
    
    


    public UserModel getUserModel() {
		return userModel;
	}

	public void setUserModel(UserModel userModel) {
		this.userModel = userModel;
	}

	public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public Integer getSequence() {
        return sequence;
    }

    public void setSequence(Integer sequence) {
        this.sequence = sequence;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "Seat{" +
        "id=" + id +
        ", state=" + state +
        ", status=" + status +
        ", pid=" + pid +
        ", uid=" + uid +
        ", sequence=" + sequence +
        "}";
    }
}
