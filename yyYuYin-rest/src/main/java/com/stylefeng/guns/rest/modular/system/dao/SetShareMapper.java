package com.stylefeng.guns.rest.modular.system.dao;

import com.stylefeng.guns.rest.modular.system.model.SetShare;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 分享赠送 Mapper 接口
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
public interface SetShareMapper extends BaseMapper<SetShare> {

	List<SetShare> getList(SetShare model);

	SetShare getOne(SetShare model);

}
