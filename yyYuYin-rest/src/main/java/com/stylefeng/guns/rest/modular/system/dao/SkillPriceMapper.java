package com.stylefeng.guns.rest.modular.system.dao;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.stylefeng.guns.rest.modular.system.model.SkillPrice;

/**
 * @Description 技能价格 Mapper 接口。
 * @author Grazer_Ma
 * @Date 2020-06-04 22:11:14
 */
public interface SkillPriceMapper extends BaseMapper<SkillPrice> {

	List<SkillPrice> getList(SkillPrice skillPriceModel);

	SkillPrice getOne(SkillPrice skillPriceModel);

}
