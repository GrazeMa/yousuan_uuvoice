package com.stylefeng.guns.rest.modular.system.service;

import com.stylefeng.guns.rest.modular.system.model.RedNum;
import java.util.List;

import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 红包内容 服务类
 * </p>
 *
 * @author wumeng123
 * @since 2019-02-20
 */
public interface IRedNumService extends IService<RedNum> {
    public List<RedNum> getList(RedNum model);
    
    RedNum getOne(RedNum model);
}
