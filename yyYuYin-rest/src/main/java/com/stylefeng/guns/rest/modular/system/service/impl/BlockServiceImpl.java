package com.stylefeng.guns.rest.modular.system.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.rest.modular.system.model.Block;
import com.stylefeng.guns.rest.modular.system.dao.BlockMapper;
import com.stylefeng.guns.rest.modular.system.service.IBlockService;

/**
 * <p>
 * 房间拉黑的 服务实现类
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
@Service
public class BlockServiceImpl extends ServiceImpl<BlockMapper, Block> implements IBlockService {

	@Override
	public List<Block> getList(Block model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getList(model);
	}

	@Override
	public Block getOne(Block model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getOne(model);
	}

}
