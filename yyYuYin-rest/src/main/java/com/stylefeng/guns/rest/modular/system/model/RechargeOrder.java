package com.stylefeng.guns.rest.modular.system.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 充值订单
 * </p>
 *
 * @author wumeng123
 * @since 2019-02-14
 */
@TableName("app_recharge_order")
public class RechargeOrder extends Model<RechargeOrder> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 分配时间
     */
    private Date createTime;
    /**
     * 购买人id
     */
    private Integer uid;
    /**
     * 实际支付金额
     */
    private Double payment;
    /**
     * 赠送
     */
    private Integer presented;
    /**
     * 订单编号
     */
    private String orderNum;
    private Integer state;
    /**
     * 1 是支付宝，2是 微信
     */
    private Integer isPay;
    
    private Integer rid;
    
    private Integer isState;
    
    private Integer giveMoney;
    
    private Integer type;
    
    private String generatedOrderNumber;

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getPresented() {
		return presented;
	}

	public void setPresented(Integer presented) {
		this.presented = presented;
	}

	public Integer getGiveMoney() {
		return giveMoney;
	}

	public void setGiveMoney(Integer giveMoney) {
		this.giveMoney = giveMoney;
	}

	public Integer getRid() {
		return rid;
	}

	public void setRid(Integer rid) {
		this.rid = rid;
	}

	public Integer getIsState() {
		return isState;
	}

	public void setIsState(Integer isState) {
		this.isState = isState;
	}

	public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public Double getPayment() {
        return payment;
    }

    public void setPayment(Double payment) {
        this.payment = payment;
    }

 

    public String getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(String orderNum) {
        this.orderNum = orderNum;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Integer getIsPay() {
        return isPay;
    }

    public void setIsPay(Integer isPay) {
        this.isPay = isPay;
    }
    
    public String getGeneratedOrderNumber() {
        return generatedOrderNumber;
    }

    public void setGeneratedOrderNumber(String generatedOrderNumber) {
        this.generatedOrderNumber = generatedOrderNumber;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "RechargeOrder{" +
        "id=" + id +
        ", createTime=" + createTime +
        ", uid=" + uid +
        ", payment=" + payment +
        ", presented=" + presented +
        ", orderNum=" + orderNum +
        ", state=" + state +
        ", isPay=" + isPay +
        ", generatedOrderNumber=" + generatedOrderNumber +
        "}";
    }
}
