package com.stylefeng.guns.rest.modular.system.controller.model;

public class HotModel {
	private Integer uid;
	private String img;
	private String userName;
	private String roomName;
	private String usercoding;
	private String roomLabel;
	private Integer num;
	
	private String liang;
	
	

	public String getLiang() {
		return liang;
	}

	public void setLiang(String liang) {
		this.liang = liang;
	}

	public String getRoomLabel() {
		return roomLabel;
	}

	public void setRoomLabel(String roomLabel) {
		this.roomLabel = roomLabel;
	}

	public Integer getNum() {
		return num;
	}

	public void setNum(Integer num) {
		this.num = num;
	}

	public Integer getUid() {
		return uid;
	}

	public void setUid(Integer uid) {
		this.uid = uid;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getRoomName() {
		return roomName;
	}

	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}

	public String getUsercoding() {
		return usercoding;
	}

	public void setUsercoding(String usercoding) {
		this.usercoding = usercoding;
	}

}
