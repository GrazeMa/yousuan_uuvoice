package com.stylefeng.guns.rest.modular.system.service.impl;

import com.stylefeng.guns.rest.modular.system.model.NoticeInfo;
import com.stylefeng.guns.rest.modular.system.dao.NoticeInfoMapper;
import com.stylefeng.guns.rest.modular.system.service.INoticeInfoService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import java.util.List;

import org.springframework.stereotype.Service;

/**
 * <p>
 * 系统公告 服务实现类
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
@Service
public class NoticeInfoServiceImpl extends ServiceImpl<NoticeInfoMapper, NoticeInfo> implements INoticeInfoService {

	@Override
	public List<NoticeInfo> getList(NoticeInfo model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getList(model);
	}

	@Override
	public NoticeInfo getOne(NoticeInfo model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getOne(model);
	}

}
