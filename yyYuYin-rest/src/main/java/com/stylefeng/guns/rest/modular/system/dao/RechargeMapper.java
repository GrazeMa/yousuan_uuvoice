package com.stylefeng.guns.rest.modular.system.dao;

import com.stylefeng.guns.rest.modular.system.model.Recharge;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 充值明细 Mapper 接口
 * </p>
 *
 * @author wumeng123
 * @since 2019-02-14
 */
public interface RechargeMapper extends BaseMapper<Recharge> {

	List<Recharge> getList(Recharge model);

	Recharge getOne(Recharge model);

	List<Recharge> getList1(Recharge model);

}
