package com.stylefeng.guns.rest.modular.system.service.impl;

import com.stylefeng.guns.rest.modular.system.model.SetShare;
import com.stylefeng.guns.rest.modular.system.dao.SetShareMapper;
import com.stylefeng.guns.rest.modular.system.service.ISetShareService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import java.util.List;

import org.springframework.stereotype.Service;

/**
 * <p>
 * 分享赠送 服务实现类
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
@Service
public class SetShareServiceImpl extends ServiceImpl<SetShareMapper, SetShare> implements ISetShareService {

	@Override
	public List<SetShare> getList(SetShare model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getList(model);
	}

	@Override
	public SetShare getOne(SetShare model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getOne(model);
	}

}
