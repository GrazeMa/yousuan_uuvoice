package com.stylefeng.guns.rest.modular.system.service;

import java.util.List;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.rest.modular.system.model.Order;

/**
 * @Description 订单服务类。
 * @author Grazer_Ma
 * @Date 2020-06-04 22:15:47
 */
public interface IOrderService extends IService<Order> {

	public List<Order> getList(Order orderModel);

	Order getOne(Order orderModel);

}
