package com.stylefeng.guns.rest.modular.system.service;

import com.stylefeng.guns.rest.modular.system.model.AndroidVersions;

import java.util.List;

import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 安卓版本控制 服务类
 * </p>
 *
 * @author wumeng123
 * @since 2019-05-17
 */
public interface IAndroidVersionsService extends IService<AndroidVersions> {
	public List<AndroidVersions> getList(AndroidVersions model);
    
    AndroidVersions getOne(AndroidVersions model);
}
