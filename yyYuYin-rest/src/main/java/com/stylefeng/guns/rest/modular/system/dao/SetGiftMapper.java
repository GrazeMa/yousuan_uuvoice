package com.stylefeng.guns.rest.modular.system.dao;

import com.stylefeng.guns.rest.modular.system.model.SetGift;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 礼物设置 Mapper 接口
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
public interface SetGiftMapper extends BaseMapper<SetGift> {

	List<SetGift> getList(SetGift model);

	SetGift getOne(SetGift model);

}
