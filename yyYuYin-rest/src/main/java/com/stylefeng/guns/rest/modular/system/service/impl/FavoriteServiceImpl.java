package com.stylefeng.guns.rest.modular.system.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.rest.modular.system.dao.FavoriteMapper;
import com.stylefeng.guns.rest.modular.system.model.Favorite;
import com.stylefeng.guns.rest.modular.system.service.IFavoriteService;

/**
 * @Description 收藏服务实现类。
 * @author Grazer_Ma
 * @Date 2020-05-25 13:10:02
 */
@Service
public class FavoriteServiceImpl extends ServiceImpl<FavoriteMapper, Favorite> implements IFavoriteService {

	@Override
	public List<Favorite> getList(Favorite favoriteModel) {
		return this.baseMapper.getList(favoriteModel);
	}

	@Override
	public List<Favorite> getFavoriteRoomsList(Favorite favoriteModel) {
		return this.baseMapper.getFavoriteRoomsList(favoriteModel);
	}
	
	@Override
	public Favorite getOne(Favorite favoriteModel) {
		return this.baseMapper.getOne(favoriteModel);
	}

}
