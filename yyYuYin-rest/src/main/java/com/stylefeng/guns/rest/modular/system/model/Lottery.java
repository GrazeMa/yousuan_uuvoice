package com.stylefeng.guns.rest.modular.system.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author wumeng123
 * @since 2019-02-24
 */
@TableName("app_lottery")
public class Lottery extends Model<Lottery> {

    private static final long serialVersionUID = 1L;

    private Integer id;
    
    private Integer gid;
    /**
     * 剩余奖品数
     */
    private Integer num;
    /**
     * 中奖概率
     */
    private Double ratio;
    
    private String name;
    
    private Integer type;   // 1=头环，2-座驾，3=礼物,4=空
    
   private String gName;
   
   


    public String getgName() {
	return gName;
}

public void setgName(String gName) {
	this.gName = gName;
}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getGid() {
        return gid;
    }

    public void setGid(Integer gid) {
        this.gid = gid;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

  

    public Double getRatio() {
		return ratio;
	}

	public void setRatio(Double ratio) {
		this.ratio = ratio;
	}

	@Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "Lottery{" +
        "id=" + id +
        ", gid=" + gid +
        ", num=" + num +
        ", ratio=" + ratio +
        "}";
    }
}
