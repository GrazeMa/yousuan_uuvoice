package com.stylefeng.guns.rest.modular.system.dao;

import com.stylefeng.guns.rest.modular.system.model.MessageInfo;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 系统消息 Mapper 接口
 * </p>
 *
 * @author wumeng
 * @since 2018-05-18
 */
public interface MessageInfoMapper extends BaseMapper<MessageInfo> {

	List<MessageInfo> getList(MessageInfo model);

	MessageInfo getOne(MessageInfo model);

}
