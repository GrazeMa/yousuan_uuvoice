package com.stylefeng.guns.rest.modular.system.service;

import com.stylefeng.guns.rest.modular.system.model.PkNum;


import java.util.List;

import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wumeng123
 * @since 2019-02-19
 */
public interface IPkNumService extends IService<PkNum> {
public List<PkNum> getList(PkNum model);
    
    PkNum getOne(PkNum model);
}
