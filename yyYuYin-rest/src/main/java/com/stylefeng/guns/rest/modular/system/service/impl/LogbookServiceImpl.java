package com.stylefeng.guns.rest.modular.system.service.impl;

import com.stylefeng.guns.rest.modular.system.model.Logbook;
import com.stylefeng.guns.rest.modular.system.dao.LogbookMapper;
import com.stylefeng.guns.rest.modular.system.service.ILogbookService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import java.util.List;

import org.springframework.stereotype.Service;

/**
 * <p>
 * 日志 服务实现类
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
@Service
public class LogbookServiceImpl extends ServiceImpl<LogbookMapper, Logbook> implements ILogbookService {

	@Override
	public List<Logbook> getList(Logbook model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getList(model);
	}

	@Override
	public Logbook getOne(Logbook model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getOne(model);
	}

}
