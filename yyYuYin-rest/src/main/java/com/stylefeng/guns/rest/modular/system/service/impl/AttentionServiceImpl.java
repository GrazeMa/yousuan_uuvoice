package com.stylefeng.guns.rest.modular.system.service.impl;

import com.stylefeng.guns.rest.modular.system.model.Attention;
import com.stylefeng.guns.rest.modular.system.dao.AttentionMapper;
import com.stylefeng.guns.rest.modular.system.service.IAttentionService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import java.util.List;

import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wumeng123
 * @since 2019-02-18
 */
@Service
public class AttentionServiceImpl extends ServiceImpl<AttentionMapper, Attention> implements IAttentionService {

	@Override
	public List<Attention> getList(Attention model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getList(model);
	}

	@Override
	public Attention getOne(Attention model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getOne(model);
	}

}
