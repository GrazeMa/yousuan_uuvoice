package com.stylefeng.guns.rest.modular.system.service;

import com.stylefeng.guns.rest.modular.system.model.MessageInfo;
import com.stylefeng.guns.rest.modular.system.model.MessageInfo;

import java.util.List;

import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 系统消息 服务类
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
public interface IMessageInfoService extends IService<MessageInfo> {
public List<MessageInfo> getList(MessageInfo model);
    
    MessageInfo getOne(MessageInfo model);
}
