package com.stylefeng.guns.rest.modular.system.controller;

import java.io.BufferedReader;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLDecoder;
import java.security.AlgorithmParameters;
import java.security.Security;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.kdDemo.utils.Base64;
import com.stylefeng.guns.rest.modular.util.ApiUtil;
import com.stylefeng.guns.rest.modular.util.JsapiTicketUtil;
import com.stylefeng.guns.rest.modular.util.ParamUtil;
import com.stylefeng.guns.rest.modular.util.Sign;
import com.stylefeng.guns.rest.modular.util.SinataUtil;
import com.stylefeng.guns.rest.modular.util.TemplateData;
import com.stylefeng.guns.rest.modular.util.WX_TemplateMsgUtil;
import com.stylefeng.guns.rest.modular.util.WX_UserUtil;

import org.bouncycastle.jce.provider.BouncyCastleProvider;

/**
 * 微信授权方面
 *
 * @author tuzx
 * @date 2018年8月9日
 * @version 1.0
 */
@Controller
@RequestMapping("/api/weixin")
public class WeiXinAppController {

	private static String appid = ParamUtil.getValue("GappID");
	private static String appSecret = ParamUtil.getValue("appSecret");

	@RequestMapping("getAddress")
	@ResponseBody
	public Map<String, Object> getAddress(String url, HttpServletRequest request) {
		Map<String, Object> data = new HashMap<>();
		String jsapiTicket = JsapiTicketUtil.getJSApiTicket();
		Map<String, String> map = Sign.sign(jsapiTicket, url);
		request.getSession().setAttribute("nonceStr", map.get("nonceStr"));
		request.getSession().setAttribute("timestamp", map.get("timestamp"));
		request.getSession().setAttribute("signature", map.get("signature"));
		data.put("nonceStr", map.get("nonceStr"));
		data.put("timestamp", map.get("timestamp"));
		data.put("signature", map.get("signature"));
		data.put("url", url);
		data.put("jsapi_ticket", jsapiTicket);
		return ApiUtil.returnObj(map);
	}

	@ResponseBody
	@RequestMapping("getOpenId")
	public Map<String, Object> getOpenId(String code) {
		System.out.println("/api/getOpenId code="+code);
		try {

			String openID = JsapiTicketUtil.getOpenId(code);
			System.out.println("ret="+openID);
			return ApiUtil.returnObj(openID);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ApiUtil.putFailObj("获取异常");
	}

	@ResponseBody
	@RequestMapping("getUserInfoAccessToken")
	public Map<String, Object> getUserInfoAccessToken(String code) {
		System.out.println("/api/getUserInfoAccessToken code="+code);
		try {
			Map<String, String> map = JsapiTicketUtil.getUserInfoAccessToken(code);
			System.out.println("ret="+map.toString());
			return ApiUtil.returnObj(map);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ApiUtil.putFailObj("获取异常");
	}

	@ResponseBody
	public String getCodeRequest() {
		try {

			/*
			 * String code = request.getParameter("code");
			 * 
			 * System.out.println(code+
			 * "-------------------------------------------------------------------------------"
			 * ); Map<String, String> map =
			 * JsapiTicketUtil.getUserInfoAccessToken(code); return
			 * ApiUtil.returnObj(map);
			 */
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;

	}

	@ResponseBody
	@RequestMapping("getRdSession")
	public Map<String, Object> getRdSession(String code, String rdSession, HttpServletRequest request) {
		try {
			Map<String, String> map = new HashMap<>();
			if (SinataUtil.isNotEmpty(rdSession)) {
				String str = (String) request.getSession().getAttribute(rdSession);
				if (SinataUtil.isNotEmpty(str)) {
					map = new HashMap<String, String>();
					map.put("rdSession", rdSession);
					map.put("status", "1");
					map.put("appId", appid);
					map.put("session_key", str);
				} else {
					map = new HashMap<String, String>();
					map.put("rdSession", rdSession);
					map.put("status", "2");// 过期
					map.put("appId", appid);
					map.put("session_key", str);
				}
			}
			map = JsapiTicketUtil.getToken(code);
			String openid = map.get("openid");
			String session_key = map.get("session_key");
			String rdSessions = new Date().getTime() + "";
			if (SinataUtil.isNotEmpty(openid)) {
				request.getSession().setAttribute(rdSessions, session_key + openid);
				map = new HashMap<String, String>();
				map.put("rdSession", rdSessions);
				map.put("status", "1");
				map.put("appId", appid);
				map.put("session_key", session_key);
			} else {
				map = new HashMap<String, String>();
				map.put("rdSession", rdSessions);
				map.put("status", "3");// 获取失败
				map.put("appId", appid);
				map.put("session_key", session_key);
			}

			return ApiUtil.returnObj(map);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ApiUtil.putFailObj("获取异常");
	}

	/**
	 * 获取微信用户信息
	 * 
	 * @param token
	 * @param openid
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/getWXinfo")
	public Object getWXinfo(String token, String openid) {
		try {
			String str = getJSApiTicket(token, openid);
			return ApiUtil.returnObj(str);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ApiUtil.putFailObj("获取异常");
	}

	public static String getJSApiTicket(String token, String openid) {
		// 获取token
		// String acess_token= JsapiTicketUtil.getAccessToken();

		String urlStr = "https://api.weixin.qq.com/sns/userinfo?access_token=" + token + "&openid=" + openid
				+ "&lang=zh_CN";
		String backData = sendGet(urlStr, "utf-8", 10000);
		// String ticket = (String)
		// JSONObject.fromObject(backData).get("ticket");
		return backData;

	}

	/***
	 * 模拟get请求
	 * 
	 * @param url
	 * @param charset
	 * @param timeout
	 * @return
	 */
	public static String sendGet(String url, String charset, int timeout) {
		String result = "";
		try {
			URL u = new URL(url);
			try {
				URLConnection conn = u.openConnection();
				conn.connect();
				conn.setConnectTimeout(timeout);
				BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), charset));
				String line = "";
				while ((line = in.readLine()) != null) {

					result = result + line;
				}
				in.close();
			} catch (IOException e) {
				return result;
			}
		} catch (MalformedURLException e) {
			return result;
		}

		return result;
	}

	static void senMsg(String openId, String access_token, String namen, String OrderNum, String isstate) {
		// 用户是否订阅该公众号标识 (0代表此用户没有关注该公众号 1表示关注了该公众号)
		Integer state = WX_UserUtil.subscribeState(openId, access_token);
		// 绑定了微信并且关注了服务号的用户 , 注册成功-推送注册短信
		if (state == 1) {
			Map<String, TemplateData> param = new HashMap<>();
			param.put("first", new TemplateData(namen, "#696969"));
			param.put("OrderSn", new TemplateData(OrderNum, "#696969"));
			param.put("OrderStatus", new TemplateData(isstate, "#696969"));
			// 调用发送微信消息给用户的接口
			WX_TemplateMsgUtil.sendWechatMsgToUser(openId, "RZfKB9pj1rIjrBIjFTiTss_NJzxGU11s0k6nf9fyi6w", "", "#000000",
					WX_TemplateMsgUtil.packJsonmsg(param), access_token);
		}

	}

	public static void main(String[] args) {
		Object phoneNumber = getPhoneNumber(
				"vgq1QAPOVeBN3F4x5a/Gcq8SuCUiDb3S4g6AlMLnkueD3Gs0sVDygNd6JKSMgQjuYtEwFeFJH8jkR95pAUJIaedNiZSvwtwtuVBtgO0bs5n0aL3ALnTvyJ7j48x15mDi1tGQhSVamzi0qSf2lVCAsIvmoahraUdRts+HexODDr5YueIj9VELFLkyFP12ve2dO/vNwiUiNVnqwbGL4gq34Q==",
				"0332NDK62xknkK0UAVL62oCvK622NDKr", "nB7w8ZM8obq3wY5OzMbDPg==");
		System.out.println(phoneNumber);
		net.sf.json.JSONObject parse = net.sf.json.JSONObject.fromObject(phoneNumber);
		System.out.println(parse.get("phoneNumber"));
	}

	public static Object getPhoneNumber(String encryptedData, String code, String iv) {
		Map<String, String> map = JsapiTicketUtil.getToken(code);
		String session_key = "";
		if (map != null) {
			// session_key = json.getString("session_key");
			session_key = map.get("session_key");
			// 被加密的数据
			byte[] dataByte = Base64.decode(encryptedData);
			// 加密秘钥
			byte[] keyByte = Base64.decode(session_key);
			// 偏移量
			byte[] ivByte = Base64.decode(iv);
			try {
				// 如果密钥不足16位，那么就补足. 这个if 中的内容很重要
				int base = 16;
				if (keyByte.length % base != 0) {
					int groups = keyByte.length / base + (keyByte.length % base != 0 ? 1 : 0);
					byte[] temp = new byte[groups * base];
					Arrays.fill(temp, (byte) 0);
					System.arraycopy(keyByte, 0, temp, 0, keyByte.length);
					keyByte = temp;
				}
				// 初始化
				Security.addProvider(new BouncyCastleProvider());
				Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
				SecretKeySpec spec = new SecretKeySpec(keyByte, "AES");
				AlgorithmParameters parameters = AlgorithmParameters.getInstance("AES");
				parameters.init(new IvParameterSpec(ivByte));
				cipher.init(Cipher.DECRYPT_MODE, spec, parameters);// 初始化
				byte[] resultByte = cipher.doFinal(dataByte);
				if (null != resultByte && resultByte.length > 0) {
					String result = new String(resultByte, "UTF-8");
					return JSONObject.parseObject(result);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return null;
	}
}
