package com.stylefeng.guns.rest.modular.system.dao;

import com.stylefeng.guns.rest.modular.system.model.Gift;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 礼物管理 Mapper 接口
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-16
 */
public interface GiftMapper extends BaseMapper<Gift> {

	List<Gift> getList(Gift model);

	Gift getOne(Gift model);

}
