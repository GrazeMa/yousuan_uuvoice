package com.stylefeng.guns.rest.modular.system.controller;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.stylefeng.guns.rest.modular.system.model.AndroidVersions;
import com.stylefeng.guns.rest.modular.system.model.IosVersions;
import com.stylefeng.guns.rest.modular.system.model.User;
import com.stylefeng.guns.rest.modular.system.service.IAndroidVersionsService;
import com.stylefeng.guns.rest.modular.system.service.IIosVersionsService;
import com.stylefeng.guns.rest.modular.util.ApiUtil;
import com.stylefeng.guns.rest.modular.util.SinataUtil;

@Controller
@RequestMapping("api")
public class VersionsUtil {
	@Autowired
	private IAndroidVersionsService AndroidVersionsService;

	@Autowired
	private IIosVersionsService IosVersionsService;

	@RequestMapping("/Versions/getVersions")
	@ResponseBody
	public Map<String, Object> getUserInfo(Integer state) {
		Logger log = LoggerFactory.getLogger(getClass());
		try {

			if (state == 1) {
				AndroidVersions a = new AndroidVersions();
				AndroidVersions one = AndroidVersionsService.getOne(a);
				return ApiUtil.returnObj(one);
			} else {
				IosVersions s = new IosVersions();
				IosVersions one = IosVersionsService.getOne(s);
				return ApiUtil.returnObj(one);
			}

		} catch (Exception e) {
			e.printStackTrace();
			log.error("获取用户信息异常", e);
			return ApiUtil.putFailObj("失败");
		}
	}
}
