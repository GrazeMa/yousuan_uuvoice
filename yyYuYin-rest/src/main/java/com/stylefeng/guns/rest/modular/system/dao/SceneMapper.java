package com.stylefeng.guns.rest.modular.system.dao;

import com.stylefeng.guns.rest.modular.system.model.Scene;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 道具管理 Mapper 接口
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
public interface SceneMapper extends BaseMapper<Scene> {

	List<Scene> getList(Scene model);

	Scene getOne(Scene model);

	List<Scene> getList1(Scene model);

	Scene getOne1(Scene model);

}
