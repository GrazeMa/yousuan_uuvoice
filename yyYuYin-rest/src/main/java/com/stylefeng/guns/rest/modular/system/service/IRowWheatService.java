package com.stylefeng.guns.rest.modular.system.service;

import com.stylefeng.guns.rest.modular.system.model.RowWheat;
import com.stylefeng.guns.rest.modular.system.model.RowWheat;

import java.util.List;

import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wumeng123
 * @since 2019-03-05
 */
public interface IRowWheatService extends IService<RowWheat> {
public List<RowWheat> getList(RowWheat model);
    
    RowWheat getOne(RowWheat model);
}
