package com.stylefeng.guns.rest.modular.system.service.impl;

import com.stylefeng.guns.rest.modular.system.model.Award;
import com.stylefeng.guns.rest.modular.system.dao.AwardMapper;
import com.stylefeng.guns.rest.modular.system.service.IAwardService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import java.util.List;

import org.springframework.stereotype.Service;

/**
 * <p>
 * 优钻兑换优币奖励 服务实现类
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
@Service
public class AwardServiceImpl extends ServiceImpl<AwardMapper, Award> implements IAwardService {

	@Override
	public List<Award> getList(Award model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getList(model);
	}

	@Override
	public Award getOne(Award model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getOne(model);
	}

}
