package com.stylefeng.guns.rest.modular.system.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.rest.modular.system.dao.GuildMapper;
import com.stylefeng.guns.rest.modular.system.model.Guild;
import com.stylefeng.guns.rest.modular.system.service.IGuildService;

/**
 * @Description 公会服务实现类。
 * @author Grazer_Ma
 * @Date 2020-05-19 10:54:00
 */
@Service
public class GuildServiceImpl extends ServiceImpl<GuildMapper, Guild> implements IGuildService {

	@Override
	public List<Guild> getList(Guild model) {
		return this.baseMapper.getList(model);
	}

	@Override
	public Guild getOne(Guild model) {
		return this.baseMapper.getOne(model);
	}

}
