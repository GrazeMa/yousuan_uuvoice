package com.stylefeng.guns.rest.modular.system.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;
import java.util.Date;

/**
 * @Description 技能价格 Entity。
 * @author Grazer_Ma
 * @Date 2020-06-04 15:55:47
 */
@TableName("app_skill_price")
public class SkillPrice extends Model<SkillPrice> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id; // 技能价格主键 ID。
    private Date createTime; // 技能价格创建时间。
    private Date updateTime; // 技能价格修改时间。

    private Integer skillPrice; // 技能价格。

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    
    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getSkillPrice() {
        return skillPrice;
    }

    public void setSkillPrice(Integer skillPrice) {
        this.skillPrice = skillPrice;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "SkillPrice{" +
        "id=" + id +
        ", createTime=" + createTime +
        ", updateTime=" + updateTime +
        ", skillPrice=" + skillPrice +
        "}";
    }

}
