package com.stylefeng.guns.rest.modular.system.service.impl;

import com.stylefeng.guns.rest.modular.system.model.Gift;
import com.stylefeng.guns.rest.modular.system.dao.GiftMapper;
import com.stylefeng.guns.rest.modular.system.service.IGiftService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import java.util.List;

import org.springframework.stereotype.Service;

/**
 * <p>
 * 礼物管理 服务实现类
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
@Service
public class GiftServiceImpl extends ServiceImpl<GiftMapper, Gift> implements IGiftService {

	@Override
	public List<Gift> getList(Gift model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getList(model);
	}

	@Override
	public Gift getOne(Gift model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getOne(model);
	}

}
