package com.stylefeng.guns.rest.modular.system.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;
import java.util.Date;

/**
 * @Description 公会 Entity。
 * @author Grazer_Ma
 * @Date 2020-05-29 16:27:19
 */
@TableName("app_guild_organization")
public class GuildOrganization extends Model<GuildOrganization> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id; // 公会主键 ID。
    private Date createTime; // 公会创建时间。
    
    private Integer guildId; // 公会 ID。
    private String guildName; // 公会名。
    private String remarks; // 备注。
    private Integer state; // 公会状态（0：待审核；1：有效；2：无效。）。

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    
	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

    public Integer getGuildId() {
        return guildId;
    }

    public void setGuildId(Integer guildId) {
        this.guildId = guildId;
    }

    public String getGuildName() {
        return guildName;
    }

    public void setGuildName(String guildName) {
        this.guildName = guildName;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }
    
    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }
    
    @Override
    protected Serializable pkVal() {
        return this.id;
    }
    
    @Override
    public String toString() {
        return "GuildOrganization{" +
        "id=" + id +
        ", createTime=" + createTime +
        ", guildId=" + guildId +
        ", guildName=" + guildName +
        ", remarks=" + remarks +
        ", state=" + state +
        "}";
    }
}
