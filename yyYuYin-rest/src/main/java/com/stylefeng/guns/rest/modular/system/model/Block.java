package com.stylefeng.guns.rest.modular.system.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 房间拉黑的
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
@TableName("app_block")
public class Block extends Model<Block> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    private Date createTime;
    /**
     * 优优id
     */
    private String eid;
    /**
     * 昵称
     */
    private String name;
    private Integer sex;
    /**
     * 1是管理员，2是房主
     */
    private Integer state;
    /**
     * 房管名字
     */
    private String fgName;
    /**
     * 是否有效 1有效，2无效
     */
    private Integer status;
    /**
     * '是否被删除（1=否  2=是）'
     */
    private Integer isDelete;
    
    private Integer uid;
    
    private Integer fgid;

    private String rid;

    
	public String getRid() {
		return rid;
	}

	public void setRid(String rid) {
		this.rid = rid;
	}

	public Integer getUid() {
		return uid;
	}

	public void setUid(Integer uid) {
		this.uid = uid;
	}

	public Integer getFgid() {
		return fgid;
	}

	public void setFgid(Integer fgid) {
		this.fgid = fgid;
	}

	public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getEid() {
        return eid;
    }

    public void setEid(String eid) {
        this.eid = eid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getFgName() {
        return fgName;
    }

    public void setFgName(String fgName) {
        this.fgName = fgName;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "Block{" +
        "id=" + id +
        ", createTime=" + createTime +
        ", eid=" + eid +
        ", name=" + name +
        ", sex=" + sex +
        ", state=" + state +
        ", fgName=" + fgName +
        ", status=" + status +
        ", isDelete=" + isDelete +
        "}";
    }
}
