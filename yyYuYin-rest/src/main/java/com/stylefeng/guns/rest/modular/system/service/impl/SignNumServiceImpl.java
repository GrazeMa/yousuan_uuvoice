package com.stylefeng.guns.rest.modular.system.service.impl;

import com.stylefeng.guns.rest.modular.system.model.SignNum;
import com.stylefeng.guns.rest.modular.system.dao.SignNumMapper;
import com.stylefeng.guns.rest.modular.system.service.ISignNumService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import java.util.List;

import org.springframework.stereotype.Service;

/**
 * <p>
 * 签到天数 服务实现类
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
@Service
public class SignNumServiceImpl extends ServiceImpl<SignNumMapper, SignNum> implements ISignNumService {

	@Override
	public List<SignNum> getList(SignNum model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getList(model);
	}

	@Override
	public SignNum getOne(SignNum model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getOne(model);
	}

	@Override
	public SignNum getLastTimeLoginInfo(Integer uid) {
		// TODO Auto-generated method stub
		return this.baseMapper.getLastTimeLoginInfo(uid);
	}

}
