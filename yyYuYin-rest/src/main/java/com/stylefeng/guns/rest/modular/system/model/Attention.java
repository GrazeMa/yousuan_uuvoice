package com.stylefeng.guns.rest.modular.system.model;

import java.io.Serializable;

import java.util.Date;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author wumeng123
 * @since 2019-02-18
 */
@TableName("app_attention")
public class Attention extends Model<Attention> {

    private static final long serialVersionUID = 1L;

    private Integer id;
    /**
     * 关注人的uid
     */
    private Integer uid;
    private Date createTime;
    /**
     * 被关注人的uid
     */
    private Integer buid;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getBuid() {
        return buid;
    }

    public void setBuid(Integer buid) {
        this.buid = buid;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "Attention{" +
        "id=" + id +
        ", uid=" + uid +
        ", createTime=" + createTime +
        ", buid=" + buid +
        "}";
    }
}
