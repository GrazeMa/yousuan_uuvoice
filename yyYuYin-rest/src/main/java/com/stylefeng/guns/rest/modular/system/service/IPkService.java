package com.stylefeng.guns.rest.modular.system.service;

import com.stylefeng.guns.rest.modular.system.model.Pk;
import com.stylefeng.guns.rest.modular.system.model.Pk;

import java.util.List;

import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 房间pk
 服务类
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
public interface IPkService extends IService<Pk> {
public List<Pk> getList(Pk model);
    
    Pk getOne(Pk model);
}
