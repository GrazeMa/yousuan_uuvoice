package com.stylefeng.guns.rest.modular.system.service;

import com.stylefeng.guns.rest.modular.system.model.Protocol;
import com.stylefeng.guns.rest.modular.system.model.Protocol;

import java.util.List;

import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * app各种html页面 服务类
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
public interface IProtocolService extends IService<Protocol> {
public List<Protocol> getList(Protocol model);
    
    Protocol getOne(Protocol model);
}
