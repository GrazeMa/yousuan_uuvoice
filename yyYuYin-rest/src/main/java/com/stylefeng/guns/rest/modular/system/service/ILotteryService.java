package com.stylefeng.guns.rest.modular.system.service;

import com.stylefeng.guns.rest.modular.system.model.Lottery;
import com.stylefeng.guns.rest.modular.system.model.Lottery;

import java.util.List;

import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wumeng123
 * @since 2019-02-24
 */
public interface ILotteryService extends IService<Lottery> {
    public List<Lottery> getList(Lottery model);
    
    Lottery getOne(Lottery model);
	
	 
}
