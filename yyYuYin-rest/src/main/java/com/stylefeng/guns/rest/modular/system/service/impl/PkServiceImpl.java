package com.stylefeng.guns.rest.modular.system.service.impl;

import com.stylefeng.guns.rest.modular.system.model.Pk;
import com.stylefeng.guns.rest.modular.system.dao.PkMapper;
import com.stylefeng.guns.rest.modular.system.service.IPkService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import java.util.List;

import org.springframework.stereotype.Service;

/**
 * <p>
 * 房间pk
 服务实现类
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
@Service
public class PkServiceImpl extends ServiceImpl<PkMapper, Pk> implements IPkService {

	@Override
	public List<Pk> getList(Pk model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getList(model);
	}

	@Override
	public Pk getOne(Pk model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getOne(model);
	}

}
