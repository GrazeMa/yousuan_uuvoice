package com.stylefeng.guns.rest.modular.system.service;

import com.stylefeng.guns.rest.modular.system.model.RechargeOrder;
import com.stylefeng.guns.rest.modular.system.model.RechargeOrder;

import java.util.List;

import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 充值订单 服务类
 * </p>
 *
 * @author wumeng123
 * @since 2019-02-14
 */
public interface IRechargeOrderService extends IService<RechargeOrder> {
    public List<RechargeOrder> getList(RechargeOrder model);
    
    RechargeOrder getOne(RechargeOrder model);
    
    RechargeOrder getorderNum(String RechargeOrder);
}
