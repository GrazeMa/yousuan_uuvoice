package com.stylefeng.guns.rest.modular.system.service;

import java.util.List;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.rest.modular.system.model.Skill;

/**
 * @Description 技能服务类。
 * @author Grazer_Ma
 * @Date 2020-06-04 22:22:55
 */
public interface ISkillService extends IService<Skill> {

	public List<Skill> getList(Skill skillModel);

	Skill getOne(Skill skillModel);

}
