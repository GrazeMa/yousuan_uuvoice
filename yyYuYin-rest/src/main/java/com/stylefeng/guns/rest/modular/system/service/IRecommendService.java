package com.stylefeng.guns.rest.modular.system.service;

import com.stylefeng.guns.rest.modular.system.model.Recommend;
import com.stylefeng.guns.rest.modular.system.model.Recommend;

import java.util.List;

import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 推荐位置管理 服务类
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
public interface IRecommendService extends IService<Recommend> {
public List<Recommend> getList(Recommend model);
    
    Recommend getOne(Recommend model);
}
