package com.stylefeng.guns.rest.modular.system.service.impl;

import com.stylefeng.guns.rest.modular.system.model.Audit;
import com.stylefeng.guns.rest.modular.system.dao.AuditMapper;
import com.stylefeng.guns.rest.modular.system.service.IAuditService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import java.util.List;

import org.springframework.stereotype.Service;

/**
 * <p>
 * 实名认证 服务实现类
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
@Service
public class AuditServiceImpl extends ServiceImpl<AuditMapper, Audit> implements IAuditService {

	@Override
	public List<Audit> getList(Audit model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getList(model);
	}

	@Override
	public Audit getOne(Audit model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getOne(model);
	}

}
