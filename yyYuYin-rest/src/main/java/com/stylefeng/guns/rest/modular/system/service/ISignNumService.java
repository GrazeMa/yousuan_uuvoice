package com.stylefeng.guns.rest.modular.system.service;

import com.stylefeng.guns.rest.modular.system.model.SignNum;
import com.stylefeng.guns.rest.modular.system.model.SignNum;

import java.util.List;

import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 签到天数 服务类
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
public interface ISignNumService extends IService<SignNum> {
 List<SignNum> getList(SignNum model);
    
    SignNum getOne(SignNum model);
    

	//查最近签到
	 SignNum getLastTimeLoginInfo(Integer uid);
}
