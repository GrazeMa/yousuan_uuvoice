package com.stylefeng.guns.rest.modular.system.service;

import com.stylefeng.guns.rest.modular.system.model.Hot;
import com.stylefeng.guns.rest.modular.system.model.Hot;

import java.util.List;

import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 热搜词 服务类
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
public interface IHotService extends IService<Hot> {
public List<Hot> getList(Hot model);
    
    Hot getOne(Hot model);
}
