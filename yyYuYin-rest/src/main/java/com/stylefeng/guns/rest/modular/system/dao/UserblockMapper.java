package com.stylefeng.guns.rest.modular.system.dao;

import com.stylefeng.guns.rest.modular.system.model.Userblock;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 用户拉黑 Mapper 接口
 * </p>
 *
 * @author wumeng123
 * @since 2019-03-21
 */
public interface UserblockMapper extends BaseMapper<Userblock> {

	List<Userblock> getList(Userblock model);

	Userblock getOne(Userblock model);

}
