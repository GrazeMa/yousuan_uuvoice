package com.stylefeng.guns.rest.modular.system.controller.model;

import java.util.List;

public class RowWheatModel {
	private Integer state;

	private Integer num;

	private List<UserModel> userList;

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public Integer getNum() {
		return num;
	}

	public void setNum(Integer num) {
		this.num = num;
	}

	public List<UserModel> getUserList() {
		return userList;
	}

	public void setUserList(List<UserModel> userList) {
		this.userList = userList;
	}

}
