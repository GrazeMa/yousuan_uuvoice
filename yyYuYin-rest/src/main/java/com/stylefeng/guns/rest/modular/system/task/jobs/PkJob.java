package com.stylefeng.guns.rest.modular.system.task.jobs;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.mapper.EntityWrapper;

import com.stylefeng.guns.rest.modular.system.service.impl.UserServiceImpl;
import com.stylefeng.guns.rest.modular.system.task.base.AbstractJob;
import com.stylefeng.guns.rest.modular.util.ParamUtil;

import io.agora.signal.Signal;
import io.agora.signal.Signal.LoginSession;
import io.agora.signal.Signal.LoginSession.Channel;

import com.stylefeng.guns.rest.modular.system.model.Pk;
import com.stylefeng.guns.rest.modular.system.model.Room;
import com.stylefeng.guns.rest.modular.system.model.TimeTask;

import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import java.util.Iterator;
import java.util.List;

/**
 * 下单定时任务
 *
 *
 */
public class PkJob extends AbstractJob {

	public static final String name = "PkJob";
	
	public static String SappID = ParamUtil.getValue("SappID");

	@Override
	public void execute(JobExecutionContext context)
			throws JobExecutionException {
		JobDataMap maps = context.getMergedJobDataMap();
		Integer  pid = maps.getInt("oid");
		Integer timeTaskId = maps.getInt("timeTaskId");
		try{
			
			Pk pk = PkService.selectById(pid);
			if(pk.getNum()>pk.getBnum()){
				pk.setStatus(1);
			}if(pk.getNum()<pk.getBnum()){
				pk.setStatus(2);
			}if(pk.getNum()==pk.getBnum()){
				pk.setStatus(3);
			}
			Room r=new Room();
			r.setRid(pk.getRid());
			Room one = RoomService.getOne(r);
			one.setIsPk(1);
			one.setIsPkState(1);
			RoomService.updateById(one);
			
			//User user = userService.selectById(uid);
			Signal sig = new Signal(SappID);
			//String token = getSWToken1(uid);
			LoginSession loginSession=sig.login("1", "_no_need_token", new Signal.LoginCallback());
			Channel channel=loginSession.channelJoin(one.getRid(), new Signal.ChannelCallback());

		    String string = JSONObject.toJSONString(one);
			channel.channelSetAttr("attr_xgfj",string);
			
			channel.channelDelAttr("attr_pk");
			
			PkService.updateById(pk);
			//修改定时任务数据状态
			TimeTask timeTask=TimeTaskService.selectById(timeTaskId);
			//0有效，1无效
			timeTask.setState(1);
			TimeTaskService.updateById(timeTask);
			
		}catch(Exception e){
			logger.debug("执行“下单定时任务自动取消”异常:orderNum={}",pid,e);
		}
	}

}
