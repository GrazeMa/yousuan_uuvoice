package com.stylefeng.guns.rest.modular.system.service;

import com.stylefeng.guns.rest.modular.system.model.GiftRecord;


import java.util.List;

import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 礼物记录 服务类
 * </p>
 *
 * @author wumeng123
 * @since 2019-03-07
 */
public interface IGiftRecordService extends IService<GiftRecord> {
	public List<GiftRecord> getList(GiftRecord model);
	
	public List<GiftRecord> getList1(GiftRecord model);
}
