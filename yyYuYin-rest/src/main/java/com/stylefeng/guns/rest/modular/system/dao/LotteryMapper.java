package com.stylefeng.guns.rest.modular.system.dao;

import com.stylefeng.guns.rest.modular.system.model.Lottery;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wumeng123
 * @since 2019-02-24
 */
public interface LotteryMapper extends BaseMapper<Lottery> {

	List<Lottery> getList(Lottery model);

	Lottery getOne(Lottery model); 

}
