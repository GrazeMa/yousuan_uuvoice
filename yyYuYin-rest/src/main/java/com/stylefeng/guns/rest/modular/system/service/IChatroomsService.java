package com.stylefeng.guns.rest.modular.system.service;

import com.stylefeng.guns.rest.modular.system.model.Chatrooms;

import java.util.List;

import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 加入聊天室的 服务类
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-10
 */
public interface IChatroomsService extends IService<Chatrooms> {
       List<Chatrooms> getList(Chatrooms Chatrooms);
       List<Chatrooms> getList1(Chatrooms Chatrooms);
       List<Chatrooms> getList2(Chatrooms Chatrooms);
       Chatrooms getOne(Chatrooms Chatrooms);
}
