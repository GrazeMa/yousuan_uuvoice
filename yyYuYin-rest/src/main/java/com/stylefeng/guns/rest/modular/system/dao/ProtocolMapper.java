package com.stylefeng.guns.rest.modular.system.dao;

import com.stylefeng.guns.rest.modular.system.model.Protocol;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * app各种html页面 Mapper 接口
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
public interface ProtocolMapper extends BaseMapper<Protocol> {

	List<Protocol> getList(Protocol model);

	Protocol getOne(Protocol model);

}
