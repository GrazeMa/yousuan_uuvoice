package com.stylefeng.guns.rest.modular.system.service.impl;

import com.stylefeng.guns.rest.modular.system.model.Ybconsume;
import com.stylefeng.guns.rest.modular.system.dao.YbconsumeMapper;
import com.stylefeng.guns.rest.modular.system.service.IYbconsumeService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import java.util.List;

import org.springframework.stereotype.Service;

/**
 * <p>
 * 优币消费记录 服务实现类
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
@Service
public class YbconsumeServiceImpl extends ServiceImpl<YbconsumeMapper, Ybconsume> implements IYbconsumeService {

	@Override
	public List<Ybconsume> getList(Ybconsume model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getList(model);
	}

	@Override
	public Ybconsume getOne(Ybconsume model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getOne(model);
	}

}
