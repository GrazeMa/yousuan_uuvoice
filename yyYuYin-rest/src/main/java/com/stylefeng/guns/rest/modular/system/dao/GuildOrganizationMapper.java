package com.stylefeng.guns.rest.modular.system.dao;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.stylefeng.guns.rest.modular.system.model.GuildOrganization;

/**
 * @Description 公会 Mapper 接口。
 * @author Grazer_Ma
 * @Date 2020-05-30 09:15:39
 */
public interface GuildOrganizationMapper extends BaseMapper<GuildOrganization> {

	List<GuildOrganization> getList(GuildOrganization model);

	GuildOrganization getOne(GuildOrganization model);

}
