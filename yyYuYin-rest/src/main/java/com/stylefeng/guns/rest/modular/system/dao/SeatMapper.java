package com.stylefeng.guns.rest.modular.system.dao;

import com.stylefeng.guns.rest.modular.system.model.Seat;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 房间座位相关设置 Mapper 接口
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-16
 */
public interface SeatMapper extends BaseMapper<Seat> {

	List<Seat> getList(Seat seat);

	Seat getOne(Seat seat);

}
