package com.stylefeng.guns.rest.modular.system.controller.model;

public class UserModel {

	private Integer id;
	private String name;
	private Integer sex;
	private String img;
	private Integer type;
	private Integer state;
	private Integer sequence;

	private String usercoding;

	private Integer num;

	private double distance;

	private Integer IsAgreement;

	/**
	 * 财富等级
	 */
	private Integer treasureGrade;

	private Integer charmGrade;

	private String individuation;

	private String voice;

	private Integer manNum;

	private Double Money;

	private Integer voiceTime;

	private String userThfm;

	private String userZjfm;

	private String userTh;

	private String userZj;
	
	private String liang;
	
	

	public String getLiang() {
		return liang;
	}

	public void setLiang(String liang) {
		this.liang = liang;
	}

	public String getUserThfm() {
		return userThfm;
	}

	public void setUserThfm(String userThfm) {
		this.userThfm = userThfm;
	}

	public String getUserZjfm() {
		return userZjfm;
	}

	public void setUserZjfm(String userZjfm) {
		this.userZjfm = userZjfm;
	}

	public String getUserTh() {
		return userTh;
	}

	public void setUserTh(String userTh) {
		this.userTh = userTh;
	}

	public String getUserZj() {
		return userZj;
	}

	public void setUserZj(String userZj) {
		this.userZj = userZj;
	}

	public Integer getIsAgreement() {
		return IsAgreement;
	}

	public void setIsAgreement(Integer isAgreement) {
		IsAgreement = isAgreement;
	}

	public Integer getVoiceTime() {
		return voiceTime;
	}

	public void setVoiceTime(Integer voiceTime) {
		this.voiceTime = voiceTime;
	}

	public Double getMoney() {
		return Money;
	}

	public void setMoney(Double money) {
		Money = money;
	}

	public Integer getManNum() {
		return manNum;
	}

	public void setManNum(Integer manNum) {
		this.manNum = manNum;
	}

	public String getVoice() {
		return voice;
	}

	public void setVoice(String voice) {
		this.voice = voice;
	}

	public double getDistance() {
		return distance;
	}

	public void setDistance(double distance) {
		this.distance = distance;
	}

	public String getIndividuation() {
		return individuation;
	}

	public void setIndividuation(String individuation) {
		this.individuation = individuation;
	}

	public Integer getNum() {
		return num;
	}

	public void setNum(Integer num) {
		this.num = num;
	}

	public Integer getTreasureGrade() {
		return treasureGrade;
	}

	public void setTreasureGrade(Integer treasureGrade) {
		this.treasureGrade = treasureGrade;
	}

	public Integer getCharmGrade() {
		return charmGrade;
	}

	public void setCharmGrade(Integer charmGrade) {
		this.charmGrade = charmGrade;
	}

	public String getUsercoding() {
		return usercoding;
	}

	public void setUsercoding(String usercoding) {
		this.usercoding = usercoding;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public Integer getSequence() {
		return sequence;
	}

	public void setSequence(Integer sequence) {
		this.sequence = sequence;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getSex() {
		return sex;
	}

	public void setSex(Integer sex) {
		this.sex = sex;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

}
