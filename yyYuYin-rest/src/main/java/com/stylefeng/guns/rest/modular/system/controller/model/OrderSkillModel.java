package com.stylefeng.guns.rest.modular.system.controller.model;

/**
 * @Description 技能模块 Model。
 * @author Grazer_Ma
 * @Date 2020-06-05 16:25:29
 */
public class OrderSkillModel {

	private Integer id;
	private Integer skillId; // 技能主键 ID。
	private String skillName; // 技能名称。
	private String skillIcon; // 技能图标 URL。
	private String skillLevel; // 技能段位。
	private Integer skillPrice; // 技能价格。
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getSkillId() {
		return skillId;
	}

	public void setSkillId(Integer skillId) {
		this.skillId = skillId;
	}
	
	public String getSkillName() {
		return skillName;
	}

	public void setSkillName(String skillName) {
		this.skillName = skillName;
	}
	
	public String getSkillIcon() {
		return skillIcon;
	}

	public void setSkillIcon(String skillIcon) {
		this.skillIcon = skillIcon;
	}

	public String getSkillLevel() {
		return skillLevel;
	}

	public void setSkillLevel(String skillLevel) {
		this.skillLevel = skillLevel;
	}

	public Integer getSkillPrice() {
		return skillPrice;
	}

	public void setSkillPrice(Integer skillPrice) {
		this.skillPrice = skillPrice;
	}

}
