package com.stylefeng.guns.rest.modular.system.dao;

import com.stylefeng.guns.rest.modular.system.model.Chatrooms;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 加入聊天室的 Mapper 接口
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-10
 */
public interface ChatroomsMapper extends BaseMapper<Chatrooms> {

	List<Chatrooms> getList(Chatrooms chatrooms);

	Chatrooms getOne(Chatrooms chatrooms);

	List<Chatrooms> getList1(Chatrooms chatrooms);

	List<Chatrooms> getList2(Chatrooms chatrooms);

}
