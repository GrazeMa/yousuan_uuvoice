package com.stylefeng.guns.rest.modular.system.dao;

import com.stylefeng.guns.rest.modular.system.model.PkNum;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wumeng123
 * @since 2019-02-19
 */
public interface PkNumMapper extends BaseMapper<PkNum> {

	List<PkNum> getList(PkNum model);

	PkNum getOne(PkNum model);

}
