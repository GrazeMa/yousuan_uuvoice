package com.stylefeng.guns.rest.modular.system.service;

import java.util.List;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.rest.modular.system.model.OrderPay;

/**
 * @Description 订单支付服务类。
 * @author Grazer_Ma
 * @Date 2020-06-04 22:21:25
 */
public interface IOrderPayService extends IService<OrderPay> {

	public List<OrderPay> getList(OrderPay orderPayModel);

	OrderPay getOne(OrderPay orderPayModel);

}
