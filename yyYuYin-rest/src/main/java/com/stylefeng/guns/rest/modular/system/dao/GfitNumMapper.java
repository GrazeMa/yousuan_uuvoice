package com.stylefeng.guns.rest.modular.system.dao;

import com.stylefeng.guns.rest.modular.system.model.GfitNum;
import com.stylefeng.guns.rest.modular.system.model.Gift;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 送的礼物，自己购买的头环，座驾，自己开宝箱得道的礼物，头环，座驾，  Mapper 接口
 * </p>
 *
 * @author wumeng123
 * @since 2019-03-02
 */
public interface GfitNumMapper extends BaseMapper<GfitNum> {

	List<GfitNum> getList(GfitNum model);

	List<GfitNum> getList1(GfitNum model);

}
