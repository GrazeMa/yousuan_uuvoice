package com.stylefeng.guns.rest.modular.system.dao;

import com.stylefeng.guns.rest.modular.system.model.LiangDivide;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 设置的一些关于分成的参数 Mapper 接口
 * </p>
 *
 * @author wumeng123
 * @since 2019-07-04
 */
public interface LiangDivideMapper extends BaseMapper<LiangDivide> {

}
