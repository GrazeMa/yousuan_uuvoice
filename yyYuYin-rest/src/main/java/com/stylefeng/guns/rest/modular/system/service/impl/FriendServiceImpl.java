package com.stylefeng.guns.rest.modular.system.service.impl;

import com.stylefeng.guns.rest.modular.system.model.Friend;
import com.stylefeng.guns.rest.modular.system.dao.FriendMapper;
import com.stylefeng.guns.rest.modular.system.service.IFriendService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import java.util.List;

import org.springframework.stereotype.Service;

/**
 * <p>
 * 好友 服务实现类
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
@Service
public class FriendServiceImpl extends ServiceImpl<FriendMapper, Friend> implements IFriendService {

	@Override
	public List<Friend> getList(Friend model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getList(model);
	}

	@Override
	public Friend getOne(Friend model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getOne(model);
	}

}
