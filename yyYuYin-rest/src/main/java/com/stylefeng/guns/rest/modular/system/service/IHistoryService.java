package com.stylefeng.guns.rest.modular.system.service;

import com.stylefeng.guns.rest.modular.system.model.History;
import com.stylefeng.guns.rest.modular.system.model.History;

import java.util.List;

import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 推荐栏位历史 服务类
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
public interface IHistoryService extends IService<History> {
public List<History> getList(History model);
    
    History getOne(History model);
}
