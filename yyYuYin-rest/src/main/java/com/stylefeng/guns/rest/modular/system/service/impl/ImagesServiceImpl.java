package com.stylefeng.guns.rest.modular.system.service.impl;

import com.stylefeng.guns.rest.modular.system.model.Images;
import com.stylefeng.guns.rest.modular.system.dao.ImagesMapper;
import com.stylefeng.guns.rest.modular.system.service.IImagesService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import java.util.List;

import org.springframework.stereotype.Service;

/**
 * <p>
 * 图片 服务实现类
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
@Service
public class ImagesServiceImpl extends ServiceImpl<ImagesMapper, Images> implements IImagesService {

	@Override
	public List<Images> getList(Images model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getList(model);
	}

	@Override
	public Images getOne(Images model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getOne(model);
	}

}
