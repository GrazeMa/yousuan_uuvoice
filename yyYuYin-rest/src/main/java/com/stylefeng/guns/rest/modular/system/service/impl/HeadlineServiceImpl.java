package com.stylefeng.guns.rest.modular.system.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.rest.modular.system.dao.HeadlineMapper;
import com.stylefeng.guns.rest.modular.system.model.Headline;
import com.stylefeng.guns.rest.modular.system.service.IHeadlineService;

/**
 * @Description 头条服务实现类。
 * @author Grazer_Ma
 * @Date 2020-05-21 23:07:34
 */
@Service
public class HeadlineServiceImpl extends ServiceImpl<HeadlineMapper, Headline> implements IHeadlineService {

	@Override
	public List<Headline> getList(Headline model) {
		return this.baseMapper.getList(model);
	}

	@Override
	public Headline getOne(Headline model) {
		return this.baseMapper.getOne(model);
	}
	
	@Override
	public Headline getOne2(Headline model) {
		return this.baseMapper.getOne2(model);
	}

}
