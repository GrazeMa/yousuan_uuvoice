package com.stylefeng.guns.rest.modular.system.service;

import java.util.List;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.rest.modular.system.model.Block;

/**
 * <p>
 * 房间拉黑的 服务类
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
public interface IBlockService extends IService<Block> {
	
	public List<Block> getList(Block model);
    
    Block getOne(Block model);
    
}
