package com.stylefeng.guns.rest.modular.system.service;

import com.stylefeng.guns.rest.modular.system.model.Userblock;

import java.util.List;

import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 用户拉黑 服务类
 * </p>
 *
 * @author wumeng123
 * @since 2019-03-21
 */
public interface IUserblockService extends IService<Userblock> {

public List<Userblock> getList(Userblock model);
    
        Userblock getOne(Userblock model);
	
}
