package com.stylefeng.guns.rest.modular.system.service.impl;

import com.stylefeng.guns.rest.modular.system.model.IosVersions;
import com.stylefeng.guns.rest.modular.system.dao.IosVersionsMapper;
import com.stylefeng.guns.rest.modular.system.service.IIosVersionsService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import java.util.List;

import org.springframework.stereotype.Service;

/**
 * <p>
 * ios版本控制 服务实现类
 * </p>
 *
 * @author wumeng123
 * @since 2019-05-17
 */
@Service
public class IosVersionsServiceImpl extends ServiceImpl<IosVersionsMapper, IosVersions> implements IIosVersionsService {

	@Override
	public List<IosVersions> getList(IosVersions model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getList(model);
	}

	@Override
	public IosVersions getOne(IosVersions model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getOne(model);
	}

}
