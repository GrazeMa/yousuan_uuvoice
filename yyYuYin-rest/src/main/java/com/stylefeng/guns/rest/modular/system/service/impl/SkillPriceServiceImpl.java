package com.stylefeng.guns.rest.modular.system.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.rest.modular.system.dao.SkillPriceMapper;
import com.stylefeng.guns.rest.modular.system.model.SkillPrice;
import com.stylefeng.guns.rest.modular.system.service.ISkillPriceService;

/**
 * @Description 技能价格实现类。
 * @author Grazer_Ma
 * @Date 2020-06-05 10:09:24
 */
@Service
public class SkillPriceServiceImpl extends ServiceImpl<SkillPriceMapper, SkillPrice> implements ISkillPriceService {

	@Override
	public List<SkillPrice> getList(SkillPrice skillPriceModel) {
		return this.baseMapper.getList(skillPriceModel);
	}

	@Override
	public SkillPrice getOne(SkillPrice skillPriceModel) {
		return this.baseMapper.getOne(skillPriceModel);
	}

}
