package com.stylefeng.guns.rest.modular.system.service;

import com.stylefeng.guns.rest.modular.system.model.Userlist;
import com.stylefeng.guns.rest.modular.system.model.Userlist;

import java.util.List;

import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 用户邀请的排行 服务类
 * </p>
 *
 * @author wumeng123
 * @since 2019-04-24
 */
public interface IUserlistService extends IService<Userlist> {
	 public List<Userlist> getList(Userlist model);
	    
	    Userlist getOne(Userlist model);
}
