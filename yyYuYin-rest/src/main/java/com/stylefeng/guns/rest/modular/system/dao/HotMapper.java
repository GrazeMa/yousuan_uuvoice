package com.stylefeng.guns.rest.modular.system.dao;

import com.stylefeng.guns.rest.modular.system.model.Hot;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 热搜词 Mapper 接口
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
public interface HotMapper extends BaseMapper<Hot> {

	List<Hot> getList(Hot model);

	Hot getOne(Hot model);

}
