package com.stylefeng.guns.rest.modular.system.service;

import com.stylefeng.guns.rest.modular.system.model.Withdraw;
import com.stylefeng.guns.rest.modular.system.model.Withdraw;

import java.util.List;

import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 提现管理 服务类
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
public interface IWithdrawService extends IService<Withdraw> {
public List<Withdraw> getList(Withdraw model);

public List<Withdraw> getList1(Withdraw model);
    
    Withdraw getOne(Withdraw model);
}
