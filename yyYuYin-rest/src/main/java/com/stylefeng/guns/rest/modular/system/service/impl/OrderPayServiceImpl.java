package com.stylefeng.guns.rest.modular.system.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.rest.modular.system.dao.OrderPayMapper;
import com.stylefeng.guns.rest.modular.system.model.OrderPay;
import com.stylefeng.guns.rest.modular.system.service.IOrderPayService;

/**
 * @Description 订单支付实现类。
 * @author Grazer_Ma
 * @Date 2020-06-05 09:51:49
 */
@Service
public class OrderPayServiceImpl extends ServiceImpl<OrderPayMapper, OrderPay> implements IOrderPayService {

	@Override
	public List<OrderPay> getList(OrderPay orderPayModel) {
		return this.baseMapper.getList(orderPayModel);
	}

	@Override
	public OrderPay getOne(OrderPay orderPayModel) {
		return this.baseMapper.getOne(orderPayModel);
	}

}
