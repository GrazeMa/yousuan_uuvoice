package com.stylefeng.guns.rest.modular.system.service;

import com.stylefeng.guns.rest.modular.system.model.Chatrooms;
import com.stylefeng.guns.rest.modular.system.model.Music;
import com.stylefeng.guns.rest.modular.system.model.Music;

import java.util.List;

import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 音乐审核 服务类
 * </p>
 *
 * @author wumeng123
 * @since 2019-03-05
 */
public interface IMusicService extends IService<Music> {
public List<Music> getList(Music model);
    
    Music getOne(Music model);
    
    Integer getConut( Music  Music);
}
