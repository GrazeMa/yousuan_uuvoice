package com.stylefeng.guns.rest.modular.system.service;

import com.stylefeng.guns.rest.modular.system.model.Sign;
import com.stylefeng.guns.rest.modular.system.model.Sign;

import java.util.List;

import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 签到设置 服务类
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
public interface ISignService extends IService<Sign> {
public List<Sign> getList(Sign model);
    
    Sign getOne(Sign model);
}
