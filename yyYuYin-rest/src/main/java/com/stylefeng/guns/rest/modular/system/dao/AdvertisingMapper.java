package com.stylefeng.guns.rest.modular.system.dao;

import com.stylefeng.guns.rest.modular.system.model.Advertising;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 广告设置 Mapper 接口
 * </p>
 *
 * @author wumeng
 * @since 2018-06-28
 */
public interface AdvertisingMapper extends BaseMapper<Advertising> {

	List<Advertising> getList(Advertising model);

	Advertising getOne(Advertising model);

}
