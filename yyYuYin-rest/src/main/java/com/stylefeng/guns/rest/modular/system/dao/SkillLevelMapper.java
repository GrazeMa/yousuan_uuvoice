package com.stylefeng.guns.rest.modular.system.dao;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.stylefeng.guns.rest.modular.system.model.SkillLevel;

/**
 * @Description 技能段位 Mapper 接口。
 * @author Grazer_Ma
 * @Date 2020-06-04 22:09:47
 */
public interface SkillLevelMapper extends BaseMapper<SkillLevel> {

	List<SkillLevel> getList(SkillLevel skillLevelModel);

	SkillLevel getOne(SkillLevel skillLevelModel);

}
