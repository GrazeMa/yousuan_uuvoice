package com.stylefeng.guns.rest.modular.util;
 
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
 
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.stylefeng.guns.core.util.SpringContextHolder;
import com.stylefeng.guns.rest.modular.system.im.userUtil;
import com.stylefeng.guns.rest.modular.system.model.Logbook;
import com.stylefeng.guns.rest.modular.system.model.SetGrade;
import com.stylefeng.guns.rest.modular.system.model.User;
import com.stylefeng.guns.rest.modular.system.service.ISetGradeService;
import com.stylefeng.guns.rest.modular.system.service.IUserService;
public class GradeUtil {
    
    /*@Resource
       static  ISetGradeService SetGradeService;*/
    
	ISetGradeService SetGradeService=SpringContextHolder.getBean( ISetGradeService.class);
	IUserService userService=SpringContextHolder.getBean( 	IUserService.class);
        public   Integer SetGrade(Integer nu, Integer state ,Integer uid,Integer type) {
            try {
                Integer m=0;
                SetGrade sg = new SetGrade();
                sg.setNum(nu);
                sg.setNum1(nu);
                sg.setState(state);
                SetGrade jie = SetGradeService.getJie(sg);
                
                User selectById = userService.selectById(uid);
       
            
                if(type==1){
                	   SetGrade selectById3 = SetGradeService.selectById(1);
                	   SetGrade selectById2 = SetGradeService.selectById(10);
                       if(nu<selectById3.getZ()*2||nu>=selectById2.getP()){
                    	 if(nu<selectById3.getZ()*2){
                    			m=selectById3.getX();
                    	 }
                    	 if(nu>=selectById2.getP()){
                    		 m=selectById2.getY();
                    	 }
                       	
                       }else{
                    	   Integer ma1= jie.getZ();
                      	 Integer ma=nu-ma1;
                      	if(ma>0){
                      		Integer s= ma/jie.getB();
                           	  m=jie.getX()+s;
                          	  
                            }else{
                          	  if(ma==0){
                          		  m=selectById.getTreasureGrade()+1;
                          	  }
                             
                          }
                       }
                	
                	
                }else{
                	   SetGrade selectById3 = SetGradeService.selectById(11);
                	   SetGrade selectById2 = SetGradeService.selectById(20);
                	   if(nu<selectById3.getZ()*2||nu>=selectById2.getP()){
                		   if(nu<selectById3.getZ()*2){
                			   m=selectById3.getX();
                		   }
                		   if(nu>=selectById2.getP()){
                			   m=selectById2.getY();
                		   }
                		 
                	   }else{
                		   Integer ma1= jie.getZ();
                         	 Integer ma=nu-ma1;
                         	if(ma>0){
                         		Integer s= ma/jie.getB();
                              	  m=jie.getX()+s;
                             	  
                               }else{
                             	  if(ma==0){
                             		  m=selectById.getCharmGrade()+1;
                             	  }
                                
                               }
                	   }
                	
               
               }
                
                
                return m;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return nu;
        
        }
    
    
}