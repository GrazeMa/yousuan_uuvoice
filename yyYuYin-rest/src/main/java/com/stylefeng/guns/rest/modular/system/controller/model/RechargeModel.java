package com.stylefeng.guns.rest.modular.system.controller.model;

import java.util.Date;

public class RechargeModel {
	private Date createTime;
	private String mark;
	private Integer gold;

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getMark() {
		return mark;
	}

	public void setMark(String mark) {
		this.mark = mark;
	}

	public Integer getGold() {
		return gold;
	}

	public void setGold(Integer gold) {
		this.gold = gold;
	}

}
