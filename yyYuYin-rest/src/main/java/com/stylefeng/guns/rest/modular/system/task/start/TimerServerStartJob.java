package com.stylefeng.guns.rest.modular.system.task.start;

import com.baomidou.mybatisplus.mapper.EntityWrapper;

import com.stylefeng.guns.core.util.DateUtil;
import com.stylefeng.guns.rest.modular.system.model.TimeTask;
import com.stylefeng.guns.rest.modular.system.service.ITimeTaskService;
import com.stylefeng.guns.rest.modular.system.task.base.QuartzManager;
import com.stylefeng.guns.rest.modular.system.task.base.TimeJobType;
import com.stylefeng.guns.rest.modular.system.task.jobs.PkJob;
import com.stylefeng.guns.rest.modular.system.task.jobs.RedJob;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 主要用于定时任务在服务器重新启动时添加任务
 * @author Leeyns
 *
 */
@Component
public class TimerServerStartJob {
	private Logger logger = LoggerFactory.getLogger(TimerServerStartJob.class);
	
	@Autowired
	private ITimeTaskService appTimeTaskService;


	public TimerServerStartJob(){
		//服务器重新启动时，重新加载定时任务
		start();
	}
	
	public void start(){
		new Thread(new Runnable() {
			@Override
			public void run() {
				logger.debug("==>> Start timer job.");
				//先暂停15ms
				try {
					Thread.sleep(15000);
					//有效定时任务需要重新执行
					isUnlock();
					Thread.sleep(15000);
					
					

					System.out.println("定时启动了");
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				
				logger.debug("==>> Stop all timer job.");
			}
		}).start();
	}

	/**
	 * //有效定时任务需要重新执行
	 */
	public void isUnlock(){

		EntityWrapper<TimeTask> timerTaskEntityWrapper=new EntityWrapper<>();
		//0有效，1无效
		timerTaskEntityWrapper.eq("state",0);
		//还没到时间点的
		timerTaskEntityWrapper.ge("excuteDate",DateUtil.format(new Date(),"yyyy-MM-dd HH:mm:ss"));
		timerTaskEntityWrapper.and("(typeName like '%PkJob%' or typeName like '%RedJob%')");
		/*//还没到时间点的
		timerTaskEntityWrapper.ge("excuteDate",DateUtil.format(new Date(),"yyyy-MM-dd HH:mm:ss"));*/
		List<TimeTask> timeTasks=appTimeTaskService.selectList(timerTaskEntityWrapper);
		for(TimeTask appTimeTask:timeTasks){
			Map<String,Object> paMap=new HashMap<>();
			String map=appTimeTask.getMap();
			paMap.put("timeTaskId",appTimeTask.getId());
			if(appTimeTask.getTypeName().equals("RedJob")){
				String[] maps=map.split(":");
				paMap.put("oid",maps[1]);
				//红包
				QuartzManager.addJob(RedJob.class, appTimeTask.getTaskName(), TimeJobType.REST, appTimeTask.getExcuteDate(), paMap);

			}else if(appTimeTask.getTypeName().equals("PkJob")){
				String[] maps=map.split(":");
				paMap.put("oid",maps[1]);
				//用户
				QuartzManager.addJob(PkJob.class,appTimeTask.getTaskName(), TimeJobType.REST, appTimeTask.getExcuteDate(), paMap);
		}
	}
  }
}
