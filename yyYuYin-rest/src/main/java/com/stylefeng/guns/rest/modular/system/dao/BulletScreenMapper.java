package com.stylefeng.guns.rest.modular.system.dao;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.stylefeng.guns.rest.modular.system.model.BulletScreen;

/**
 * @Description 弹幕 Mapper 接口。
 * @author Grazer_Ma
 * @Date 2020-05-20 11:00:10
 */
public interface BulletScreenMapper extends BaseMapper<BulletScreen> {

	List<BulletScreen> getList(BulletScreen model);

	BulletScreen getOne(BulletScreen model);

}
