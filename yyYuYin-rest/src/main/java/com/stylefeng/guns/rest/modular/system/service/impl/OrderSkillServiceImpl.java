package com.stylefeng.guns.rest.modular.system.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.rest.modular.system.dao.OrderSkillMapper;
import com.stylefeng.guns.rest.modular.system.model.OrderSkill;
import com.stylefeng.guns.rest.modular.system.service.IOrderSkillService;

/**
 * @Description 技能实现类。
 * @author Grazer_Ma
 * @Date 2020-06-05 09:51:49
 */
@Service
public class OrderSkillServiceImpl extends ServiceImpl<OrderSkillMapper, OrderSkill> implements IOrderSkillService {

	@Override
	public List<OrderSkill> getList(OrderSkill orderSkillModel) {
		return this.baseMapper.getList(orderSkillModel);
	}

	@Override
	public OrderSkill getOne(OrderSkill orderSkillModel) {
		return this.baseMapper.getOne(orderSkillModel);
	}

	@Override
	public List<OrderSkill> getServedUserList() {
		return this.baseMapper.getServedUserList();
	}
	
}
