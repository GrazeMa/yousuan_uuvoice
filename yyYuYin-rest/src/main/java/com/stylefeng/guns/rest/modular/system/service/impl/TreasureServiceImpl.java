package com.stylefeng.guns.rest.modular.system.service.impl;

import com.stylefeng.guns.rest.modular.system.model.Treasure;
import com.stylefeng.guns.rest.modular.system.dao.TreasureMapper;
import com.stylefeng.guns.rest.modular.system.service.ITreasureService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import java.util.List;

import org.springframework.stereotype.Service;

/**
 * <p>
 * 房间财富值 服务实现类
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
@Service
public class TreasureServiceImpl extends ServiceImpl<TreasureMapper, Treasure> implements ITreasureService {

	@Override
	public List<Treasure> getList(Treasure model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getList(model);
	}

	@Override
	public Treasure getOne(Treasure model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getOne(model);
	}

	@Override
	public List<Treasure> getSum(Treasure model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getSum(model);
	}

	@Override
	public List<Treasure> getSumH(Treasure model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getSumH(model);
	}

}
