package com.stylefeng.guns.rest.modular.system.service.impl;

import com.stylefeng.guns.rest.modular.system.model.Feedback;
import com.stylefeng.guns.rest.modular.system.dao.FeedbackMapper;
import com.stylefeng.guns.rest.modular.system.service.IFeedbackService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import java.util.List;

import org.springframework.stereotype.Service;

/**
 * <p>
 * 反馈 服务实现类
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
@Service
public class FeedbackServiceImpl extends ServiceImpl<FeedbackMapper, Feedback> implements IFeedbackService {

	@Override
	public List<Feedback> getList(Feedback model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getList(model);
	}

	@Override
	public Feedback getOne(Feedback model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getOne(model);
	}

}
