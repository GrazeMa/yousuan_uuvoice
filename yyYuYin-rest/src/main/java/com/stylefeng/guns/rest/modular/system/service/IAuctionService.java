package com.stylefeng.guns.rest.modular.system.service;

import com.stylefeng.guns.rest.modular.system.model.Auction;
import com.stylefeng.guns.rest.modular.system.model.Auction;

import java.util.List;

import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 房间竞拍排名 服务类
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
public interface IAuctionService extends IService<Auction> {
	 public List<Auction> getList(Auction model);
	    
	    Auction getOne(Auction model);
}
