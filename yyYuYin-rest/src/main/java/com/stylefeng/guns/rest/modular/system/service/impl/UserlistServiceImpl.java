package com.stylefeng.guns.rest.modular.system.service.impl;

import com.stylefeng.guns.rest.modular.system.model.Userlist;
import com.stylefeng.guns.rest.modular.system.dao.UserlistMapper;
import com.stylefeng.guns.rest.modular.system.service.IUserlistService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import java.util.List;

import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户邀请的排行 服务实现类
 * </p>
 *
 * @author wumeng123
 * @since 2019-04-24
 */
@Service
public class UserlistServiceImpl extends ServiceImpl<UserlistMapper, Userlist> implements IUserlistService {

	@Override
	public List<Userlist> getList(Userlist model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getList(model);
	}

	@Override
	public Userlist getOne(Userlist model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getOne(model);
	}

}
