package com.stylefeng.guns.rest.modular.util;


import java.net.URLDecoder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * api返回数据格式统一封装
 * @author tzj
 *
 */
public class ApiUtil {
	/**
	 * @Description 封装返回的数据。
	 * @author Grazer_Ma
	 * @param code 0成功；1 失败。
	 * @param msg 描述信息。
	 * @return resultMap(HashMap)。
	 */
	public static Map<String, Object> returnedData(int returnedCodeInt, String returnedMessageString, Object dataObject) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("code", returnedCodeInt);
		resultMap.put("msg", returnedMessageString);
		resultMap.put("sys", DateUtil.getCurMilli());
		resultMap.put("data", dataObject);
		System.out.println("dataObject=" + dataObject.toString());
		return resultMap;
	}
	
	/**
	 * 返回一个成功的对象提示{}
	 * @return
	 */
	public static Map<String, Object> putSuccessObj(String msg){
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("code", 0);
		resultMap.put("msg", msg);
		resultMap.put("sys", DateUtil.getCurMilli());
		resultMap.put("data", new HashMap<Object, Object>());
		return resultMap;
	}
	public static Map<String, Object> putSuccessObj(){
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("code", 0);
		resultMap.put("msg", "操作成功");
		resultMap.put("sys", DateUtil.getCurMilli());
		resultMap.put("data", new HashMap<Object, Object>());
		return resultMap;
	}
	
	
	

	/**
	 * 返回一个失败的对象提示{}
	 * @param msg 失败原因
	 * @return
	 */
	public static Map<String, Object> putFailObj(String msg){
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("code", 1);
		resultMap.put("msg", msg);
		resultMap.put("sys", DateUtil.getCurMilli());
		resultMap.put("data", new HashMap<Object, Object>());
		return resultMap;
	}
	
	
	/**
	 * 返回一个失败的对象提示{}
	 * @param msg 失败原因
	 * @return
	 */
	public static Map<String, Object> putFailObj_1(String msg){
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("code", 2);
		resultMap.put("msg", msg);
		resultMap.put("sys", DateUtil.getCurMilli());
		resultMap.put("data", new HashMap<Object, Object>());
		return resultMap;
	}

	/**
	 * 操作成功返回id号
	 * @param code 1 失败，0成功
	 * @param msg 描述
	 * @param sys 系统时间
	 *  @param data id
	 * @return
	 */
	public static Map<String, Object> putInfo(int code,String msg,int id){
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("code", code);
		resultMap.put("msg", msg);
		resultMap.put("sys", DateUtil.getCurMilli());
		resultMap.put("data", id);
		return resultMap;
	}

	/**
	 * 返回一个失败的集合提示[]
	 * @param msg 失败原因
	 * @return
	 */
	public static Map<String, Object> putFailArray(String msg){
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("code", 1);
		resultMap.put("msg", msg);
		resultMap.put("sys", DateUtil.getCurMilli());
		resultMap.put("data", new ArrayList<>());
		return resultMap;
	}

	/**
	 * 返回一个成功的集合提示[]
	 * @return
	 */
	public static Map<String, Object> putSuccessArray(){
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("code", 0);
		resultMap.put("msg", "操作成功！");
		resultMap.put("sys", DateUtil.getCurMilli());
		resultMap.put("data", new ArrayList<>());
		return resultMap;
	}
	
	

	/**
	 * 返回一个集合格式的数据[]
	 * @param code 1 失败，0成功
	 * @param msg 描述
	 * @return
	 */
	public static Map<String, Object> returnArray(Object data){
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("code", 0);
		resultMap.put("msg", "获取成功");
		resultMap.put("sys", DateUtil.getCurMilli());
		resultMap.put("data", data);
		return resultMap;
	}
	
	//红包消息
	public static Map<String, Object> returnArray_1(Object data){
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("code", 104);
		resultMap.put("data", data);
		return resultMap;
	}
	
	//设置管理员
	public static Map<String, Object> returnArray_2(Object data){
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("code", 105);
		resultMap.put("data", data);
		return resultMap;
	}
	
	//等级提升
	public static Map<String, Object> returnArray_3(Object data){
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("code", 106);
		resultMap.put("data", data);
		return resultMap;
	}
	
	
    //踢出房间
	public static Map<String, Object> returnArray_4(Object data){
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("code", 107);
		resultMap.put("data", data);
		return resultMap;
	}
		
  //上下麦
	public static Map<String, Object> returnArray_5(Object data){
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("code", 110);
		resultMap.put("data", data);
		return resultMap;
	}
	
	//禁麦
	public static Map<String, Object> returnArray_6(Object data){
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("code", 111);
		resultMap.put("data", data);
		return resultMap;
	}
	
	//加入黑名单
	public static Map<String, Object> returnArray_7(Object data){
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("code", 112);
		resultMap.put("data", data);
		return resultMap;
	}
	
	//抱她上麦
	public static Map<String, Object> returnArray_8(Object data){
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("code", 114);
		resultMap.put("data", data);
		return resultMap;
	}
	
	//送礼物全屏通知
	public static Map<String, Object> returnArray_9(Object data){
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("code", 115);
		resultMap.put("data", data);
		return resultMap;
	}
	
	
  //进房间通知
	public static Map<String, Object> returnArray_11(Object data){
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("code", 117);
		resultMap.put("data", data);
		return resultMap;
	}
	
	//pk通知
	public static Map<String, Object> returnArray_10(Object data){
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("code", 124);
		resultMap.put("data", data);
		return resultMap;
	}

	/**
	 * 返回一个对象格式的数据{}
	 * @param code 1 失败，0成功
	 * @param msg 描述
	 * @return
	 */
	public static Map<String, Object> returnObj(Object data){
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("code", 0);
		resultMap.put("msg", "获取成功");
		resultMap.put("sys", DateUtil.getCurMilli());
		resultMap.put("data", data);
		System.out.println("returnObj="+data.toString());
		return resultMap;
	}
	
	
	public static Map<String, Object> returnObj_1(Object data){
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("code", 2);
		resultMap.put("msg", "获取成功");
		resultMap.put("sys", DateUtil.getCurMilli());
		resultMap.put("data", data);
		return resultMap;
	}
	
	public static Map<String, Object> returnObj_20000(Object data){
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("code", 0);
		resultMap.put("msg", "1");
		resultMap.put("sys", DateUtil.getCurMilli());
		resultMap.put("data", data);
		return resultMap;
	}
	
	public static Map<String, Object> returnObj_10000(String msg){
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("code", 10000);
		resultMap.put("msg", msg);
		resultMap.put("sys", DateUtil.getCurMilli());
		resultMap.put("data", new HashMap<Object, Object>());
		return resultMap;
	}


	public static String convertToContent(String content){
		try {
			if(content == null) content = "";
			return URLDecoder.decode(content,"utf-8");
		} catch (Exception e) {
			System.err.println(e);
			return content;
		}		
	}

}
