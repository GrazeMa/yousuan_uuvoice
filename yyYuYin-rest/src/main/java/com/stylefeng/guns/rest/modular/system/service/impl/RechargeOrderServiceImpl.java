package com.stylefeng.guns.rest.modular.system.service.impl;

import com.stylefeng.guns.rest.modular.system.model.RechargeOrder;
import com.stylefeng.guns.rest.modular.system.dao.RechargeOrderMapper;
import com.stylefeng.guns.rest.modular.system.service.IRechargeOrderService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import java.util.List;

import org.springframework.stereotype.Service;

/**
 * <p>
 * 充值订单 服务实现类
 * </p>
 *
 * @author wumeng123
 * @since 2019-02-14
 */
@Service
public class RechargeOrderServiceImpl extends ServiceImpl<RechargeOrderMapper, RechargeOrder> implements IRechargeOrderService {

	@Override
	public List<RechargeOrder> getList(RechargeOrder model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getList(model);
	}

	@Override
	public RechargeOrder getOne(RechargeOrder model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getOne(model);
	}
	
	@Override
	public RechargeOrder getorderNum(String RechargeOrder) {
		// TODO Auto-generated method stub
		return this.baseMapper.getorderNum(RechargeOrder);
	}

}
