package com.stylefeng.guns.rest.modular.system.service.impl;

import com.stylefeng.guns.rest.modular.system.model.History;
import com.stylefeng.guns.rest.modular.system.dao.HistoryMapper;
import com.stylefeng.guns.rest.modular.system.service.IHistoryService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import java.util.List;

import org.springframework.stereotype.Service;

/**
 * <p>
 * 推荐栏位历史 服务实现类
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
@Service
public class HistoryServiceImpl extends ServiceImpl<HistoryMapper, History> implements IHistoryService {

	@Override
	public List<History> getList(History model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getList(model);
	}

	@Override
	public History getOne(History model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getOne(model);
	}

}
