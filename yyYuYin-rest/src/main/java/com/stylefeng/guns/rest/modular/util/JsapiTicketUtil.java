package com.stylefeng.guns.rest.modular.util;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import net.sf.json.JSONObject;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
/***
 * @author V型知识库  www.vxzsk.com
 *
 */
public class JsapiTicketUtil {
	// yuanxiaoshu
	private static String appid = ParamUtil.getValue("wx_pub_app_id");
	private static String appSecret = ParamUtil.getValue("wx_pub_appSecret");
	
     
    /***
     * 模拟get请求
     * @param url
     * @param charset
     * @param timeout
     * @return
     */
     public static String sendGet(String url, String charset, int timeout)
      {
        String result = "";
        try
        {
          URL u = new URL(url);
          try
          {
            URLConnection conn = u.openConnection();
            conn.connect();
            conn.setConnectTimeout(timeout);
            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), charset));
            String line="";
            while ((line = in.readLine()) != null)
            {
             
              result = result + line;
            }
            in.close();
          } catch (IOException e) {
            return result;
          }
        }
        catch (MalformedURLException e)
        {
          return result;
        }
       
        return result;
      }
     /***
      * 获取acess_token 
      * 来源www.vxzsk.com
      * @return
      */
     public static String getAccessToken(){
            String url ="https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid="+appid+"&secret="+appSecret+"";
            String backData=sendGet(url, "utf-8", 10000);
            String accessToken = (String) JSONObject.fromObject(backData).get("access_token");  
            return accessToken;
     }
    /***
      * 获取jsapiTicket
      * 来源 www.vxzsk.com
      * @return
      */
    public static String getJSApiTicket(){ 
        //获取token
        String acess_token= JsapiTicketUtil.getAccessToken();
           
        String urlStr = "https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token="+acess_token+"&type=jsapi";  
        String backData=sendGet(urlStr, "utf-8", 10000);  
        String ticket = (String) JSONObject.fromObject(backData).get("ticket");  
        return  ticket;  
           
    }  
    /**
     * 获取用户openid
     * @return
     */
    public static String getOpenId(String code){ 
           
        String urlStr = "https://api.weixin.qq.com/sns/oauth2/access_token?appid="+appid+"&secret="+appSecret+"&code="+code+"&grant_type=authorization_code";  
        String backData=sendGet(urlStr, "utf-8", 10000);
        System.out.println(backData);
        String ticket = (String) JSONObject.fromObject(backData).get("openid");  
        return  ticket;  
           
    }  
    /**
     * 获取请求用户信息的access_token
     *
     * @param code
     * @return
     */
    public static Map<String, String> getUserInfoAccessToken(String code) {
        JsonObject object = null;
        Map<String, String> data = new HashMap();
        try {
            String url = String.format("https://api.weixin.qq.com/sns/oauth2/access_token?appid=%s&secret=%s&code=%s&grant_type=authorization_code",
                                       appid,appSecret, code);
            System.out.println("getUserInfoAccessToken:" + url);
            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpGet httpGet = new HttpGet(url);
            HttpResponse httpResponse = httpClient.execute(httpGet);
            HttpEntity httpEntity = httpResponse.getEntity();
            String tokens = EntityUtils.toString(httpEntity, "utf-8");
            System.out.println(tokens);
            Gson token_gson = new Gson();
            object = token_gson.fromJson(tokens, JsonObject.class);
            data.put("openid", object.get("openid").toString().replaceAll("\"", ""));
            data.put("access_token", object.get("access_token").toString().replaceAll("\"", ""));
        } catch (Exception ex) {
        }
        return data;
    }
    public static String getCode(){
    	JsonObject object = null;
        Map<String, String> data = new HashMap();
        try {
        	String codes = String.format("https://open.weixin.qq.com/connect/oauth2/authorize?appid=%s&redirect_uri=%s&response_type=code&scope=%s&state=%s#wechat_redirect",
                    appid, "http://www.txciot.com/resources/html/first.html", "snsapi_base", "xxxx_state");
            DefaultHttpClient httpClient = new DefaultHttpClient();
           return sendGet(codes, "utf-8", 10000);
        } catch (Exception ex) {
        	return "";
        }
    }
    public static void main(String[] args) {
    	System.out.println(URLEncoder.encode("http://www.txciot.com/resources/html/first.html"));
    	System.out.println(getUserInfoAccessToken(getCode()));
    }
    
    public static Map<String, String> getToken(String code) {
        JsonObject object = null;
        Map<String, String> data = new HashMap();
        try {
            String url = String.format("https://api.weixin.qq.com/sns/jscode2session?appid=%s&secret=%s&js_code=%s&grant_type=authorization_code",
            		appid,appSecret, code);
            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpGet httpGet = new HttpGet(url);
            HttpResponse httpResponse = httpClient.execute(httpGet);
            HttpEntity httpEntity = httpResponse.getEntity();
            String tokens = EntityUtils.toString(httpEntity, "utf-8");
            Gson token_gson = new Gson();
            object = token_gson.fromJson(tokens, JsonObject.class);
            data.put("openid", object.get("openid").toString().replaceAll("\"", ""));
            data.put("session_key", object.get("session_key").toString().replaceAll("\"", ""));
        } catch (Exception ex) {
        }
        return data;
    }
    
}
