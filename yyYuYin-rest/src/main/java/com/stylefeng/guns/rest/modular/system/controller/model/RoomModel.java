package com.stylefeng.guns.rest.modular.system.controller.model;

public class RoomModel {
	private String messageShow;// 消息内容
	private String rid;// 房间id
	private String nickname;
	
	

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getMessageShow() {
		return messageShow;
	}

	public void setMessageShow(String messageShow) {
		this.messageShow = messageShow;
	}

	public String getRid() {
		return rid;
	}

	public void setRid(String rid) {
		this.rid = rid;
	}

}
