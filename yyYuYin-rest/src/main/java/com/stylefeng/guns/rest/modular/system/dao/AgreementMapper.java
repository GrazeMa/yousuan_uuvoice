package com.stylefeng.guns.rest.modular.system.dao;

import com.stylefeng.guns.rest.modular.system.model.Agreement;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 协议号 Mapper 接口
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
public interface AgreementMapper extends BaseMapper<Agreement> {

	List<Agreement> getList(Agreement model);

	Agreement getOne(Agreement model);

}
