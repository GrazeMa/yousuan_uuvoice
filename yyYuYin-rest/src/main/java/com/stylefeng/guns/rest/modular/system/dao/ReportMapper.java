package com.stylefeng.guns.rest.modular.system.dao;

import com.stylefeng.guns.rest.modular.system.model.Report;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 举报 Mapper 接口
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-16
 */
public interface ReportMapper extends BaseMapper<Report> {

	List<Report> getList(Report model);

	Report getOne(Report model);

}
