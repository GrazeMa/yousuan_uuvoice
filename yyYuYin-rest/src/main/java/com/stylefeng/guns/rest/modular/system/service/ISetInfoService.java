package com.stylefeng.guns.rest.modular.system.service;

import com.stylefeng.guns.rest.modular.system.model.SetInfo;
import com.stylefeng.guns.rest.modular.system.model.SetInfo;

import java.util.List;

import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 系统设置 服务类
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
public interface ISetInfoService extends IService<SetInfo> {
public List<SetInfo> getList(SetInfo model);
    
    SetInfo getOne(SetInfo model);
}
