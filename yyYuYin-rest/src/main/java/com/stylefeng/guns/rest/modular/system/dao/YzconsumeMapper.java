package com.stylefeng.guns.rest.modular.system.dao;

import com.stylefeng.guns.rest.modular.system.model.Yzconsume;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 优获取记录 Mapper 接口
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
public interface YzconsumeMapper extends BaseMapper<Yzconsume> {

	List<Yzconsume> getList(Yzconsume model);

	Yzconsume getOne(Yzconsume model);

}
