package com.stylefeng.guns.rest.modular.system.controller;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.omg.PortableInterceptor.INACTIVE;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.stylefeng.guns.rest.common.util.ApiJson;
import com.stylefeng.guns.rest.modular.system.controller.model.CharmModel;
import com.stylefeng.guns.rest.modular.system.controller.model.GiftRecordModel;
import com.stylefeng.guns.rest.modular.system.controller.model.InviteModel;
import com.stylefeng.guns.rest.modular.system.controller.model.RechargeModel;
import com.stylefeng.guns.rest.modular.system.controller.model.SceneModel;
import com.stylefeng.guns.rest.modular.system.controller.model.TimeGiftRecordModel;
import com.stylefeng.guns.rest.modular.system.controller.model.TimeInviteModel;
import com.stylefeng.guns.rest.modular.system.controller.model.TimeRechargeModel;
import com.stylefeng.guns.rest.modular.system.controller.model.TimeWithdrawModel;
import com.stylefeng.guns.rest.modular.system.controller.model.UserModel;
import com.stylefeng.guns.rest.modular.system.controller.model.WithdrawModel;
import com.stylefeng.guns.rest.modular.system.controller.model.userModels;
import com.stylefeng.guns.rest.modular.system.controller.model.zhaotaModel;
import com.stylefeng.guns.rest.modular.system.im.TXCloudUtils;
import com.stylefeng.guns.rest.modular.system.model.Advertising;
import com.stylefeng.guns.rest.modular.system.model.Agreement;
import com.stylefeng.guns.rest.modular.system.model.Attention;
import com.stylefeng.guns.rest.modular.system.model.Award;
import com.stylefeng.guns.rest.modular.system.model.Charm;
import com.stylefeng.guns.rest.modular.system.model.Chatrooms;
import com.stylefeng.guns.rest.modular.system.model.Feedback;
import com.stylefeng.guns.rest.modular.system.model.Friend;
import com.stylefeng.guns.rest.modular.system.model.GfitNum;
import com.stylefeng.guns.rest.modular.system.model.Gift;
import com.stylefeng.guns.rest.modular.system.model.GiftRecord;
import com.stylefeng.guns.rest.modular.system.model.Images;
import com.stylefeng.guns.rest.modular.system.model.Invite;
import com.stylefeng.guns.rest.modular.system.model.Logbook;
import com.stylefeng.guns.rest.modular.system.model.Music;
import com.stylefeng.guns.rest.modular.system.model.Protocol;
import com.stylefeng.guns.rest.modular.system.model.Recharge;
import com.stylefeng.guns.rest.modular.system.model.Room;
import com.stylefeng.guns.rest.modular.system.model.RoomUser;
import com.stylefeng.guns.rest.modular.system.model.Scene;
import com.stylefeng.guns.rest.modular.system.model.SceneNum;
import com.stylefeng.guns.rest.modular.system.model.SetGift;
import com.stylefeng.guns.rest.modular.system.model.SetGrade;
import com.stylefeng.guns.rest.modular.system.model.SetWithdraw;
import com.stylefeng.guns.rest.modular.system.model.SmsCode;
import com.stylefeng.guns.rest.modular.system.model.Treasure;
import com.stylefeng.guns.rest.modular.system.model.User;
import com.stylefeng.guns.rest.modular.system.model.UserBag;
import com.stylefeng.guns.rest.modular.system.model.Userblock;
import com.stylefeng.guns.rest.modular.system.model.Withdraw;
import com.stylefeng.guns.rest.modular.system.model.Ybconsume;
import com.stylefeng.guns.rest.modular.system.model.Ybexchange;
import com.stylefeng.guns.rest.modular.system.model.Yzconsume;
import com.stylefeng.guns.rest.modular.system.service.IAdvertisingService;
import com.stylefeng.guns.rest.modular.system.service.IAgreementService;
import com.stylefeng.guns.rest.modular.system.service.IAttentionService;
import com.stylefeng.guns.rest.modular.system.service.IAwardService;
import com.stylefeng.guns.rest.modular.system.service.ICharmService;
import com.stylefeng.guns.rest.modular.system.service.IChatroomsService;
import com.stylefeng.guns.rest.modular.system.service.IFeedbackService;
import com.stylefeng.guns.rest.modular.system.service.IFriendService;
import com.stylefeng.guns.rest.modular.system.service.IGfitNumService;
import com.stylefeng.guns.rest.modular.system.service.IGiftRecordService;
import com.stylefeng.guns.rest.modular.system.service.IGiftService;
import com.stylefeng.guns.rest.modular.system.service.IImagesService;
import com.stylefeng.guns.rest.modular.system.service.IInviteService;
import com.stylefeng.guns.rest.modular.system.service.IMusicService;
import com.stylefeng.guns.rest.modular.system.service.IProtocolService;
import com.stylefeng.guns.rest.modular.system.service.IRechargeService;
import com.stylefeng.guns.rest.modular.system.service.IRoomService;
import com.stylefeng.guns.rest.modular.system.service.IRoomUserService;
import com.stylefeng.guns.rest.modular.system.service.ISceneNumService;
import com.stylefeng.guns.rest.modular.system.service.ISceneService;
import com.stylefeng.guns.rest.modular.system.service.ISetGiftService;
import com.stylefeng.guns.rest.modular.system.service.ISetGradeService;
import com.stylefeng.guns.rest.modular.system.service.ISetWithdrawService;
import com.stylefeng.guns.rest.modular.system.service.ISmsCodeService;
import com.stylefeng.guns.rest.modular.system.service.ITreasureService;
import com.stylefeng.guns.rest.modular.system.service.IUserBagService;
import com.stylefeng.guns.rest.modular.system.service.IUserService;
import com.stylefeng.guns.rest.modular.system.service.IUserblockService;
import com.stylefeng.guns.rest.modular.system.service.IWithdrawService;
import com.stylefeng.guns.rest.modular.system.service.IYbconsumeService;
import com.stylefeng.guns.rest.modular.system.service.IYbexchangeService;
import com.stylefeng.guns.rest.modular.system.service.IYzconsumeService;
import com.stylefeng.guns.rest.modular.util.ApiUtil;
import com.stylefeng.guns.rest.modular.util.DateUtil;
import com.stylefeng.guns.rest.modular.util.GradeUtil;
import com.stylefeng.guns.rest.modular.util.Jpush;
import com.stylefeng.guns.rest.modular.util.SinataUtil;

import io.agora.signal.Signal;
import io.agora.signal.Signal.LoginSession;
import io.agora.signal.Signal.LoginSession.Channel;

@Controller
@RequestMapping("api")
public class UserRenGliController {

	// 反馈
	@Resource
	private IFeedbackService feedbackService;
	//用户信息
	@Resource
	private IUserService userService;

	// 这个是道具
	@Autowired
	private ISceneService SceneService;
	//礼物明细
	@Autowired
	private IGfitNumService GfitNumService;
	//好友
	@Autowired
	private IFriendService FriendService;
	//关注
	@Autowired
	private IAttentionService AttentionService;
	//提现设置
	@Autowired
	private ISetWithdrawService SetWithdrawService;
	//提现管理
	@Autowired
	private IWithdrawService WithdrawService;
	//礼物记录
	@Autowired
	private IGiftRecordService GiftRecordService;
	//充值明细
	@Autowired
	private IRechargeService RechargeService;

	// 邀请记录
	@Autowired
	private IInviteService InviteService;

	// 用户拉黑记录
	@Autowired

	private IUserblockService UserblockService;

	// 音乐
	@Autowired
	private IMusicService MusicService;
	// app各种html页面
	@Autowired
	private IProtocolService ProtocolService;
	// 协议号
	@Autowired
	private IAgreementService AgreementService;
	// 图片
	@Autowired
	private IImagesService imgesService;
	// 加入聊天室
	@Autowired
	private IChatroomsService ChatroomsService;
	// 用户获取的道具
	@Autowired
	private ISceneNumService SceneNumService;
	// 礼物管理
	@Autowired
	private IGiftService GiftService;
	//优币兑换记录
	@Autowired
	private IYbexchangeService YbexchangeService;
	//等级设置
	@Autowired
	private ISetGradeService SetGradeService;

	// 房间财富值
	@Autowired
	private ITreasureService treasureService;

	// 房间魅力值
	@Autowired
	private ICharmService charmService;
	//优币消费记录
	@Autowired
	private IYbconsumeService YbconsumeService;
	//优钻获取记录
	@Autowired
	private IYzconsumeService YzconsumeService;

	// 短信
	@Autowired
	private ISmsCodeService smsService;

	// 礼物
	@Autowired
	private IGiftService giftService;

	// 礼物
	@Autowired
	private IUserBagService userBagService;
		
	// 获取最后两天宝箱设置
	@Autowired
	private ISetGiftService SetGiftService;
	//优钻兑换优币奖励
	@Autowired
	private IAwardService AwardService;
	// 关注
	@Autowired
	private IAttentionService attentionService;
 //广告设置
	@Autowired
	private IAdvertisingService AdvertisingService;
	
	//房间流水
		@Autowired
		private IRoomUserService  RoomUserService;
		
		@Autowired
		private IRoomService RoomService;

	/**
	 * 广告页
	 */
	@RequestMapping("/Advertising/Advertising")
	@ResponseBody
	public Map<String, Object> AdvertisingMUN(HttpServletRequest request) {
		try {
			Advertising selectById = AdvertisingService.selectById(1);
			if (selectById.getUrlType() == 1) {
				String url = "http://47.103.55.172/YuYin/api/Advertising?id=1";
				selectById.setUrlHtml(url);
			}
			return ApiUtil.returnObj(selectById);
		} catch (Exception e) {
			e.printStackTrace();

			return null;
		}

	}

	@RequestMapping("/Advertising")
	public Object tobranner(Integer id) {
		return "/Advertising.html?id=" + id;
	}

	@RequestMapping("/getAdvertising")
	@ResponseBody
	public Object getAdvertising(Integer id) {
		try {
			Advertising se = AdvertisingService.selectById(1);
			return ApiJson.returnOK(se);
		} catch (Exception e) {
			e.printStackTrace();
			return ApiJson.returnNG("获取失败！");
		}
	}

	// 获取html页面
	@RequestMapping("/Protocol/Protocol")
	@ResponseBody
	public Object Protocol(HttpServletRequest request, Protocol appProtocol, Integer type) {
		try {
			Protocol ma = ProtocolService.selectById(type);
			String url = "http://shanghaiyousuan.cn/yyYuYin-rest/api/Protocol/Protocols?type=" + type;
			ma.setContent(url);
			return ApiUtil.returnObj(ma);
		} catch (Exception e) {
			e.printStackTrace();
			return ApiUtil.putFailObj("获取html页面异常");
		}

	}

	// 获取html页面
	@RequestMapping("/Protocol/Protocols")
	public Object Protocols(HttpServletRequest request, Protocol appProtocol, Integer type) {
		try {
			return "/agreement.html?type=" + type;
		} catch (Exception e) {
			e.printStackTrace();
			return ApiUtil.putFailArray("获取html页面异常");
		}
	}

	// 获取html页面
	@RequestMapping("/Protocol/toProtocol")
	@ResponseBody
	public Object getAbout(HttpServletRequest request, Protocol appProtocol, Integer type) {
		try {
			Protocol ma = ProtocolService.selectById(type);
			return ApiJson.returnOK(ma);
		} catch (Exception e) {
			e.printStackTrace();
			return ApiUtil.putFailArray("获取html页面异常");
		}
	}

	// 获取道具的列表
	@RequestMapping("/User/getSceneList")
	@ResponseBody
	public Map<String, Object> getSceneList(Integer uid, Integer state, Integer pageNum, Integer pageSize) {
		try {
			List<SceneModel> sc = new ArrayList<>();
			Scene s = new Scene();
			s.setState(state);
			s.setIsState(1);
			s.setIsDelete(1);
			List<Scene> list = SceneService.getList1(s);
			Iterator<Scene> iterator = list.iterator();
			while (iterator.hasNext()) {
				Scene next = iterator.next();
				SceneModel scm = new SceneModel();
				scm.setId(next.getId());
				scm.setDay(next.getDay());
				scm.setGold(next.getGold());
				scm.setImg(next.getImg());
				scm.setImgFm(next.getImgFm());
				scm.setName(next.getName());
				sc.add(scm);
			}

			return ApiUtil.returnArray(sc);
		} catch (Exception e) {
			e.printStackTrace();
			return ApiUtil.putFailObj("失败");
		}
	}

	// 获取购买道具
	@RequestMapping("/User/addSceneList")
	@ResponseBody
	public Map<String, Object> addSceneList(Integer uid, Integer buid, Integer gid) {
		try {
			User user = userService.selectById(uid);
			Scene scene = SceneService.selectById(gid);
			if (user.getGold() < scene.getGold()) {
				return ApiUtil.putFailObj("优币不足");
			}
			user.setGold(user.getGold() - scene.getGold());

			Integer nu = user.getGoldNum() + scene.getGold();

			Integer setGrade2 = new GradeUtil().SetGrade(nu, 1, uid, 1);

			user.setTreasureGrade(setGrade2);
			user.setGoldNum(nu);

			userService.updateById(user);

			Treasure tr = new Treasure();
			tr.setCreateTime(new Date());
			tr.setEid(user.getUsercoding());
			tr.setUid(uid);
			tr.setNum(scene.getGold());
			tr.setIsDelete(1);
			treasureService.insert(tr);
			
			
			RoomUser ru=new RoomUser();
        	ru.setUid(uid);
        	ru.setType(1);
        	ru.setZhuan(scene.getGold());
        	ru.setState(1);
        	ru.setCreateTime(new Date());
        	RoomUserService.insert(ru);
			
			

			User selectById = userService.selectById(buid);
			if (!uid.equals(buid)) {
				Map map = new HashMap<>();
				map.put("state", "3");
				map.put("id", buid.toString());
				map.put("name", user.getNickname());
				map.put("sceneName", scene.getName());
				if (scene.getState() == 2) {
					Jpush.push(buid.toString(),
							"你的好友" + user.getNickname() + "给你赠送了头饰" + scene.getName() + ".快去看看吧，点击头饰马上使用哟~", map);
				} else {
					Jpush.push(buid.toString(),
							"你的好友" + user.getNickname() + "给你赠送了座驾" + scene.getName() + ".快去看看吧，点击座驾马上使用哟~", map);
				}

			} else {

			}
			if (scene.getState() == 1) {
				/* if (SinataUtil.isEmpty(selectById.getUserZj())) { */
				selectById.setUserZj(scene.getImg());
				selectById.setUserZjfm(scene.getImgFm());
				selectById.setZjid(scene.getId());
				/* } */

			} else {
				/* if (SinataUtil.isEmpty(selectById.getUserTh())) { */
				selectById.setUserTh(scene.getImg());
				selectById.setUserThfm(scene.getImgFm());
				selectById.setThid(scene.getId());
				/* } */

			}
			scene.setVolume(scene.getVolume() + 1);
			SceneService.updateById(scene);
			selectById.setScene(selectById.getScene() + 1);
			userService.updateById(selectById);

			SceneNum gm = new SceneNum();
			gm.setSid(scene.getId());
			gm.setUid(buid);
			SceneNum one = SceneNumService.getOne(gm);
			if (SinataUtil.isEmpty(one)) {
				SceneNum gm1 = new SceneNum();
				gm1.setCreateTime(new Date());
				gm1.setSid(scene.getId());
				gm1.setState(scene.getState());
				gm1.setUid(buid);
				gm1.setNum(1);
				SceneNumService.insert(gm1);
			} else {
				one.setNum(one.getNum() + 1);
				SceneNumService.updateById(one);
			}

			return ApiUtil.putSuccessObj("成功");
		} catch (Exception e) {
			e.printStackTrace();
			return ApiUtil.putFailObj("失败");
		}
	}

	// 获取我的好友和我关注和我的粉丝
	@RequestMapping("/User/addfriendList")
	@ResponseBody
	public Map<String, Object> addfriendList(Integer uid, Integer pageNum, Integer pageSize, Integer type) {
		try {
			List<UserModel> uml = new ArrayList<>();
			if (type == 1) {
				PageHelper.startPage(pageNum, pageSize, false);
				Friend model = new Friend();
				model.setUid(uid);
				List<Friend> list = FriendService.getList(model);
				Iterator<Friend> iterator = list.iterator();
				while (iterator.hasNext()) {
					Friend friend = iterator.next();
					UserModel um = new UserModel();
					User user = userService.selectById(friend.getBuid());
					if (SinataUtil.isEmpty(user)) {
						Agreement byID = AgreementService.selectById(friend.getBuid());
						um.setId(byID.getId());
						um.setImg(byID.getImg());
						um.setName(byID.getNickname());
						um.setUsercoding("");

						Chatrooms cs = new Chatrooms();
						cs.setUid(friend.getBuid());
						cs.setStatus(1);
						cs.setIsAgreement(2);
						Chatrooms cms = ChatroomsService.getOne(cs);
						if (SinataUtil.isEmpty(cms)) {
							um.setState(1);
						} else {
							um.setState(2);
						}

						uml.add(um);
					} else {
						um.setId(user.getId());
						um.setImg(user.getImgTx());
						um.setName(user.getNickname());
						um.setUsercoding(user.getUsercoding());

						Chatrooms cs = new Chatrooms();
						cs.setUid(user.getId());
						cs.setStatus(1);
						cs.setIsAgreement(1);
						Chatrooms cms = ChatroomsService.getOne(cs);
						if (SinataUtil.isEmpty(cms)) {
							um.setState(1);
						} else {
							um.setState(2);
						}

						uml.add(um);
					}

				}
			}
			if (type == 2) {
				PageHelper.startPage(pageNum, pageSize, false);
				Attention model = new Attention();
				model.setUid(uid);
				List<Attention> list = AttentionService.getList(model);
				Iterator<Attention> iterator = list.iterator();
				while (iterator.hasNext()) {
					Attention attention = iterator.next();
					UserModel um = new UserModel();
					User user = userService.selectById(attention.getBuid());
					if (SinataUtil.isEmpty(user)) {
						Agreement byID = AgreementService.selectById(attention.getBuid());
						um.setId(byID.getId());
						um.setImg(byID.getImg());
						um.setName(byID.getNickname());
						um.setUsercoding("");

						Chatrooms cs = new Chatrooms();
						cs.setUid(attention.getBuid());
						cs.setStatus(1);
						cs.setIsAgreement(2);
						Chatrooms cms = ChatroomsService.getOne(cs);
						if (SinataUtil.isEmpty(cms)) {
							um.setState(1);
						} else {
							um.setState(2);
						}

						uml.add(um);
					} else {
						um.setId(user.getId());
						um.setImg(user.getImgTx());
						um.setUsercoding(user.getUsercoding());
						um.setName(user.getNickname());

						Chatrooms cs = new Chatrooms();
						cs.setUid(user.getId());
						cs.setStatus(1);
						cs.setIsAgreement(1);
						Chatrooms cms = ChatroomsService.getOne(cs);
						if (SinataUtil.isEmpty(cms)) {
							um.setState(1);
						} else {
							um.setState(2);
						}

						uml.add(um);
					}

				}
			}
			if (type == 3) {
				PageHelper.startPage(pageNum, pageSize, false);
				Attention model = new Attention();
				model.setBuid(uid);
				List<Attention> list = AttentionService.getList(model);
				Iterator<Attention> iterator = list.iterator();
				while (iterator.hasNext()) {
					Attention attention = iterator.next();
					UserModel um = new UserModel();
					User user = userService.selectById(attention.getUid());
					if (SinataUtil.isEmpty(user)) {
						Agreement byID = AgreementService.selectById(attention.getUid());
						um.setId(byID.getId());
						um.setImg(byID.getImg());
						um.setName(byID.getNickname());
						um.setUsercoding("");

						Attention model1 = new Attention();
						model1.setUid(uid);
						model1.setBuid(attention.getUid());
						Attention list1 = AttentionService.getOne(model);
						if (SinataUtil.isEmpty(list1)) {
							um.setState(1);
						} else {
							um.setState(2);
						}

						uml.add(um);
					} else {
						um.setId(user.getId());
						um.setImg(user.getImgTx());
						um.setUsercoding(user.getUsercoding());
						um.setName(user.getNickname());

						Attention model1 = new Attention();
						model1.setUid(uid);
						model1.setBuid(user.getId());
						Attention list1 = AttentionService.getOne(model1);
						if (SinataUtil.isEmpty(list1)) {
							um.setState(1);
						} else {
							um.setState(2);
						}

						uml.add(um);
					}

				}
			}

			return ApiUtil.returnArray(uml);
		} catch (Exception e) {
			e.printStackTrace();
			return ApiUtil.putFailObj("失败");
		}
	}

	// 获取我关注的
	@RequestMapping("/User/AttentionList")
	@ResponseBody
	public Map<String, Object> AttentionList(Integer uid, Integer pageNum, Integer pageSize) {
		try {

			List<UserModel> uml = new ArrayList<>();
			PageHelper.startPage(pageNum, pageSize, false);
			Attention model = new Attention();
			model.setUid(uid);
			List<Attention> list = AttentionService.getList(model);
			Iterator<Attention> iterator = list.iterator();
			while (iterator.hasNext()) {
				Attention attention = iterator.next();
				UserModel um = new UserModel();
				User user = userService.selectById(attention.getBuid());
				um.setId(user.getId());
				um.setImg(user.getImgTx());
				um.setName(user.getNickname());
				uml.add(um);
			}
			return ApiUtil.returnArray(uml);
		} catch (Exception e) {
			e.printStackTrace();
			return ApiUtil.putFailObj("失败");
		}
	}

	// 获取提现列表
	@RequestMapping("/User/SetWithdrawServiceList")
	@ResponseBody
	public Map<String, Object> SetWithdrawServiceList(Integer uid) {
		try {
			Map map = new HashMap<>();
			User user = userService.selectById(uid);
			if (SinataUtil.isEmpty(user.getPayAccount())) {
				map.put("state", 1);
			} else {
				map.put("state", 2);
			}
			map.put("payAccount", user.getPayAccount());
			map.put("trueName", user.getTrueName());
			map.put("ynum", user.getYnum());
			map.put("gold", user.getGold());
			SetWithdraw m = new SetWithdraw();
			List<SetWithdraw> list = SetWithdrawService.getList(m);
			map.put("Withdraw", list);

			return ApiUtil.returnArray(map);
		} catch (Exception e) {
			e.printStackTrace();
			return ApiUtil.putFailObj("失败");
		}
	}

	// 获取提现的申请
	@RequestMapping("/User/AddSetWithdraw")
	@ResponseBody
	public Map<String, Object> AddSetWithdraw(Integer uid, Integer id) {
		try {
			SetWithdraw list = SetWithdrawService.selectById(id);
			User user = userService.selectById(uid);
			if (user.getYnum() <= list.getY()) {
				return ApiUtil.putFailObj("优钻不足");
			}

			user.setYnum(user.getYnum() - list.getY());

			userService.updateById(user);

			Withdraw w = new Withdraw();
			w.setCreateTime(new Date());
			w.setUid(uid);
			w.setName(user.getNickname());
			w.setEid(user.getUsercoding());
			w.setState(1);
			w.setMoney(Double.parseDouble(list.getX().toString()));
			w.setHistoryMoney(user.getHistoryDeposit());
			w.setWitName(user.getTrueName());
			w.setWitPhone(user.getPayAccount());
			w.setStatus(1);
			w.setIsDelete(1);
			w.setYzNum(list.getY());
			WithdrawService.insert(w);

			return ApiUtil.putSuccessObj("成功");
		} catch (Exception e) {
			e.printStackTrace();
			return ApiUtil.putFailObj("失败");
		}
	}

	// 获取提现的申请列表
	@RequestMapping("/User/WithdrawList")
	@ResponseBody
	public Map<String, Object> WithdrawList(Integer uid, String beginTime, String endTime, Integer pageNum,
			Integer pageSize) {
		try {
			List<TimeWithdrawModel> twm = new ArrayList<>();
			PageHelper.startPage(pageNum, pageSize, false);

			Withdraw w = new Withdraw();
			w.setUid(uid);
			w.setBeginTime(beginTime);
			w.setEndTime(endTime);
			List<Withdraw> list = WithdrawService.getList1(w);
			Iterator<Withdraw> iterator = list.iterator();
			while (iterator.hasNext()) {
				TimeWithdrawModel tm = new TimeWithdrawModel();
				Withdraw withdra = iterator.next();
				tm.setCreateTime(withdra.getCreateTime());
				Withdraw w1 = new Withdraw();
				DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
				w1.setBeginTime(format.format(withdra.getCreateTime()));
				w1.setEndTime(format.format(withdra.getCreateTime()));
				w1.setUid(uid);
				List<Withdraw> list1 = WithdrawService.getList(w1);
				Iterator<Withdraw> iterator1 = list1.iterator();
				List<WithdrawModel> wm = new ArrayList<>();
				while (iterator1.hasNext()) {
					Withdraw withdraw = iterator1.next();
					WithdrawModel wml = new WithdrawModel();
					wml.setCreateTime(withdraw.getCreateTime());
					wml.setMoney(withdraw.getMoney());
					wml.setSerialNum(withdraw.getSerialNum());
					wml.setState(withdraw.getState());
					wml.setStatus(withdraw.getStatus());
					wml.setPhone(withdraw.getWitPhone());
					wm.add(wml);
				}
				tm.setWithdraw(wm);
				twm.add(tm);
			}
			return ApiUtil.returnArray(twm);
		} catch (Exception e) {
			e.printStackTrace();
			return ApiUtil.putFailObj("失败");
		}

	}

	// 获取礼物记录的列表
	@RequestMapping("/User/GiftRecordList")
	@ResponseBody
	public Map<String, Object> GiftRecordList(Integer uid, String beginTime, String endTime, Integer state,
			Integer pageNum, Integer pageSize) {
		try {

			List<TimeGiftRecordModel> tgrm = new ArrayList<>();
			PageHelper.startPage(pageNum, pageSize, false);

			GiftRecord gr = new GiftRecord();
			gr.setBeginTime(beginTime);
			gr.setEndTime(endTime);
			if (state == 2) {
				gr.setBuid(uid);
			} else {
				gr.setUid(uid);
			}
			gr.setState(state);

			List<GiftRecord> list = GiftRecordService.getList1(gr);
			Iterator<GiftRecord> iterator = list.iterator();
			while (iterator.hasNext()) {

				TimeGiftRecordModel tgr = new TimeGiftRecordModel();

				GiftRecord next = iterator.next();
				DateFormat format = new SimpleDateFormat("yyyy-MM-dd");

				tgr.setCreateTime(next.getCreateDate());
				GiftRecord gr1 = new GiftRecord();
				gr1.setBeginTime(format.format(next.getCreateDate()));
				gr1.setEndTime(format.format(next.getCreateDate()));
				if (state == 2) {
					gr1.setBuid(uid);
				} else {
					gr1.setUid(uid);
				}
				gr1.setState(state);
				List<GiftRecord> list1 = GiftRecordService.getList(gr1);
				Iterator<GiftRecord> iterator1 = list1.iterator();
				List<GiftRecordModel> grm = new ArrayList<>();
				while (iterator1.hasNext()) {
					GiftRecord next1 = iterator1.next();
					GiftRecordModel grd = new GiftRecordModel();
					Gift gift = GiftService.selectById(next1.getGid());
					grd.setCreateTime(next1.getCreateDate());
					DecimalFormat df = new DecimalFormat("######0.00");
					if (state == 2) {
						if (next1.getType() == 2) {
							grd.setImg(gift.getImgFm());
							grd.setBenificial(df.format(gift.getGold() * 1.0));
							grd.setName("开宝箱获得");
						} else {
							User selectById = userService.selectById(next1.getUid());
							grd.setImg(gift.getImgFm());
							grd.setBenificial(df.format(gift.getGold() * .8));
							grd.setName(selectById.getNickname());
						}

					} else {
						if (next1.getType() == 2) {
							grd.setImg(gift.getImgFm());
							grd.setBenificial(df.format(gift.getGold() * 1.0));
							grd.setName("开宝箱获得");
						} else {
							User selectById = userService.selectById(next1.getBuid());
							grd.setImg(gift.getImgFm());
							grd.setBenificial(df.format(gift.getGold() * 1.0));
							grd.setName(selectById.getNickname());
						}

					}
					grd.setType(next1.getType());
					grd.setNum(next1.getNum());

					grm.add(grd);

				}
				tgr.setGiftRecord(grm);

				tgrm.add(tgr);

			}

			return ApiUtil.returnArray(tgrm);
		} catch (Exception e) {
			e.printStackTrace();
			return ApiUtil.putFailObj("失败");
		}

	}

	// 用户充值记录
	@RequestMapping("/Recharge/UserRecharge")
	@ResponseBody
	public Map<String, Object> UserRecharge(Integer uid, String beginTime, String endTime, int pageNum, int pageSize) {
		try {
			if (SinataUtil.isEmpty(uid)) {
				return ApiUtil.putFailObj("用户ID不能为空");
			}
			List<TimeRechargeModel> trm = new ArrayList<>();

			PageHelper.startPage(pageNum, pageSize, false);

			DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			Recharge r = new Recharge();
			r.setUid(uid);
			r.setBeginTime(beginTime);
			r.setEndTime(endTime);
			List<Recharge> list = RechargeService.getList1(r);
			Iterator<Recharge> iterator = list.iterator();
			while (iterator.hasNext()) {
				Recharge next = iterator.next();
				TimeRechargeModel tm = new TimeRechargeModel();
				tm.setCreateTime(next.getCreateTime());

				Recharge r1 = new Recharge();
				r1.setUid(uid);
				r1.setBeginTime(format.format(next.getCreateTime()));
				r1.setEndTime(format.format(next.getCreateTime()));
				List<Recharge> list1 = RechargeService.getList(r1);
				List<RechargeModel> rm = new ArrayList<>();

				Iterator<Recharge> iterator1 = list1.iterator();
				while (iterator1.hasNext()) {
					Recharge next1 = iterator1.next();
					RechargeModel rml = new RechargeModel();
					rml.setCreateTime(next1.getCreateTime());
					rml.setGold(next1.getSumMoney());
					if (next1.getState() == 1) {
						String str = "充值" + next1.getRechargeMoney() + "元,首充多送" + next1.getGiveMoney() + "个优币";
						rml.setMark(str);
					} else {
						String str = "充值" + next1.getRechargeMoney() + "元";
						rml.setMark(str);
					}
					rm.add(rml);

				}
				tm.setRecharge(rm);
				trm.add(tm);

			}
			return ApiUtil.returnObj(trm);

		} catch (Exception e) {
			e.printStackTrace();
			return ApiUtil.putFailArray("财务明细失败");
		}
	}

	// 用户邀请记录
	@RequestMapping("/invite/UserInvite")
	@ResponseBody
	public Map<String, Object> UserInvite(Integer uid, String beginTime, String endTime, int pageNum, int pageSize) {
		try {
			if (SinataUtil.isEmpty(uid)) {
				return ApiUtil.putFailObj("用户ID不能为空");
			}

			List<TimeInviteModel> tim = new ArrayList<>();

			PageHelper.startPage(pageNum, pageSize, false);
			DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			Invite in = new Invite();
			in.setUid(uid);
			in.setBeginTime(beginTime);
			in.setEndTime(endTime);
			List<Invite> list = InviteService.getList1(in);
			Iterator<Invite> iterator = list.iterator();
			while (iterator.hasNext()) {
				Invite invite = iterator.next();
				List<InviteModel> ls = new ArrayList<>();
				TimeInviteModel ti = new TimeInviteModel();
				ti.setCreateTime(invite.getCreateTime());

				Invite in1 = new Invite();
				in1.setUid(uid);
				in1.setBeginTime(format.format(invite.getCreateTime()));
				in1.setEndTime(format.format(invite.getCreateTime()));
				List<Invite> list1 = InviteService.getList(in1);
				Iterator<Invite> iterator1 = list1.iterator();
				while (iterator1.hasNext()) {
					Invite invite1 = iterator1.next();
					InviteModel inv = new InviteModel();

					inv.setCreateTime(invite1.getCreateTime());
					inv.setMoney(invite1.getMoney());
					inv.setName("邀请奖励");

					ls.add(inv);

				}
				ti.setInvite(ls);
				tim.add(ti);
			}

			return ApiUtil.returnObj(tim);

		} catch (Exception e) {
			e.printStackTrace();
			return ApiUtil.putFailArray("财务明细失败");
		}
	}

	// 用户添加拉黑记录
	@RequestMapping("/Userblock/getAddUserblock")
	@ResponseBody
	public Map<String, Object> getAddUserblock(Integer uid, Integer buid) {
		try {
			if (SinataUtil.isEmpty(uid)) {
				return ApiUtil.putFailObj("用户ID不能为空");
			}

			Userblock ub = new Userblock();
			ub.setUid(uid);
			ub.setBuid(buid);
			Userblock one = UserblockService.getOne(ub);
			if (SinataUtil.isNotEmpty(one)) {
				return ApiUtil.putFailArray("用户已在拉黑列表");
			} else {
				ub.setCreateTime(new Date());
				UserblockService.insert(ub);

				Attention mf = new Attention();
				mf.setBuid(buid);
				mf.setUid(uid);

				boolean accountImport = TXCloudUtils.blackList(uid, buid);

				List<Attention> mfList = attentionService.getList(mf);
				if (SinataUtil.isNotEmpty(mfList)) {
					for (Attention m : mfList) {

						User user = userService.selectById(m.getBuid());
						if (SinataUtil.isEmpty(user)) {
							Agreement user1 = AgreementService.selectById(buid);
						} else {
							user.setFansNum(user.getFansNum() - 1);
							userService.updateById(user);
						}

						User user1 = userService.selectById(m.getUid());
						if (SinataUtil.isEmpty(user1)) {
							Agreement user2 = AgreementService.selectById(buid);
						} else {
							user1.setAttentionNum(user1.getAttentionNum() - 1);
							userService.updateById(user1);
						}

						attentionService.deleteById(m.getId());

						Friend f = new Friend();
						f.setBuid(m.getBuid());
						f.setUid(m.getUid());
						Friend one1 = FriendService.getOne(f);
						if (SinataUtil.isNotEmpty(one1)) {
							FriendService.deleteById(one1);
						}

						Friend f1 = new Friend();
						f1.setBuid(m.getUid());
						f1.setUid(m.getBuid());
						Friend one2 = FriendService.getOne(f1);
						if (SinataUtil.isNotEmpty(one2)) {
							FriendService.deleteById(one2);
						}

					}
				}
			}
			return ApiUtil.putSuccessObj("成功");

		} catch (Exception e) {
			e.printStackTrace();
			return ApiUtil.putFailArray("用户添加拉黑记录失败");
		}

	}

	// 用户拉黑记录
	@RequestMapping("/Userblock/getUserblock")
	@ResponseBody
	public Map<String, Object> UserInvite(Integer uid, int pageNum, int pageSize) {
		try {
			if (SinataUtil.isEmpty(uid)) {
				return ApiUtil.putFailObj("用户ID不能为空");
			}

			List<UserModel> um = new ArrayList<>();
			Userblock ub = new Userblock();
			ub.setUid(uid);
			PageHelper.startPage(pageNum, pageSize, false);
			List<Userblock> list = UserblockService.getList(ub);
			Iterator<Userblock> iterator = list.iterator();
			while (iterator.hasNext()) {
				Userblock userblock = iterator.next();
				UserModel uml = new UserModel();
				User user = userService.selectById(userblock.getBuid());
				if (SinataUtil.isEmpty(user)) {
					Agreement user1 = AgreementService.selectById(userblock.getBuid());
					uml.setImg(user1.getImg());
					uml.setSex(user1.getSex());
					uml.setName(user1.getNickname());
					uml.setId(userblock.getId());
				} else {
					uml.setImg(user.getImgTx());
					uml.setSex(user.getSex());
					uml.setName(user.getNickname());
					uml.setId(userblock.getId());
				}

				um.add(uml);
			}
			return ApiUtil.returnObj(um);

		} catch (Exception e) {
			e.printStackTrace();
			return ApiUtil.putFailArray("财务明细失败");
		}
	}

	// 查看我是否被别人拉黑
	@RequestMapping("/Userblock/getblock")
	@ResponseBody
	public Map<String, Object> getblock(Integer uid, Integer buid) {
		try {
			if (SinataUtil.isEmpty(uid)) {
				return ApiUtil.putFailObj("用户ID不能为空");
			}
			Map map = new HashMap<>();
			Userblock ub = new Userblock();
			ub.setUid(buid);
			ub.setBuid(uid);
			Userblock one = UserblockService.getOne(ub);
			if (SinataUtil.isNotEmpty(one)) {
				map.put("state", 2);
			} else {
				map.put("state", 1);
			}

			return ApiUtil.returnArray(map);
		} catch (Exception e) {
			e.printStackTrace();
			return ApiUtil.putFailArray("用户添加拉黑记录失败");
		}

	}

	// 用户删除拉黑记录
	@RequestMapping("/Userblock/delUserblock")
	@ResponseBody
	public Map<String, Object> delUserblock(Integer id) {
		try {
			Userblock userblock = UserblockService.selectById(id);

			boolean accountImport = TXCloudUtils.blackdel(userblock.getUid(), userblock.getBuid());

			UserblockService.deleteById(id);
			return ApiUtil.putSuccessObj("成功");

		} catch (Exception e) {
			e.printStackTrace();
			return ApiUtil.putFailArray("用户添加拉黑记录失败");
		}

	}

	// 获取我上传的音乐
	@RequestMapping("/user/userMusice")
	@ResponseBody
	public Map<String, Object> Logbook(Integer uid, int pageNum, int pageSize, HttpServletRequest request) {
		try {
			if (SinataUtil.isEmpty(uid)) {
				return ApiUtil.putFailObj("uid不能为空！");
			}
			PageHelper.startPage(pageNum, pageSize, false);
			Music music = new Music();
			music.setUid(uid);
			music.setState(2);
			music.setStatus(1);
			music.setIsDelete(1);
			List<Music> list = MusicService.getList(music);

			return ApiUtil.returnArray(list);
		} catch (Exception e) {
			e.printStackTrace();
			return ApiUtil.putFailObj("修改失败");
		}
	}

	// 下载音乐次数
	@RequestMapping("/User/userMusiceNum")
	@ResponseBody
	public Map<String, Object> UserRoom(Integer id) {
		try {
			Music music = MusicService.selectById(id);
			music.setDownloads(music.getDownloads() + 1);
			MusicService.updateById(music);

			return ApiUtil.putSuccessObj("成功");
		} catch (Exception e) {
			e.printStackTrace();
			return ApiUtil.putFailArray("用户添加拉黑记录失败");
		}

	}

	// 获取PC我上传的音乐
	@RequestMapping("/user/userPCMusice")
	@ResponseBody
	public Map<String, Object> userPCMusice(Integer uid, int pageNum, int pageSize, HttpServletRequest request) {
		try {
			if (SinataUtil.isEmpty(uid)) {
				return ApiUtil.putFailObj("uid不能为空！");
			}
			PageHelper.startPage(pageNum, pageSize, false);
			Music music = new Music();
			music.setUid(uid);
			music.setState(2);
			music.setIsDelete(1);
			List<Music> list = MusicService.getList(music);

			Map map = new HashMap<>();
			map.put("Musice", list);

			Integer conut = MusicService.getConut(music);
			map.put("num", conut);

			return ApiUtil.returnArray(map);
		} catch (Exception e) {
			e.printStackTrace();
			return ApiUtil.putFailObj("修改失败");
		}
	}

	// 获取删除PC我上传的音乐
	@RequestMapping("/user/userPCMusiceDel")
	@ResponseBody
	public Map<String, Object> userPCMusice(String id) {
		try {
			String[] sourceStrArray = id.split(",");
			for (int i = 0; i < sourceStrArray.length; i++) {
				Integer z = Integer.parseInt(sourceStrArray[i]);
				Music music = MusicService.selectById(z);
				music.setIsDelete(2);
				MusicService.updateById(music);
			}

			return ApiUtil.putSuccessObj("成功");
		} catch (Exception e) {
			e.printStackTrace();
			return ApiUtil.putFailObj("修改失败");
		}
	}

	// 反馈信息
	@RequestMapping("/Feedback/FeedbackSave")
	@ResponseBody
	public Map<String, Object> getFeedback(Integer uid, String content, String phone) {
		try {
			if (SinataUtil.isEmpty(uid)) {
				return ApiUtil.putFailObj("用户ID不能为空");
			}
			Feedback feedback = new Feedback();
			Date date = new Date();
			DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String time = format.format(date);
			feedback.setCreateTime(time);
			feedback.setIsDelete(1);
			feedback.setStatus(1);
			feedback.setUid(uid);
			feedback.setPhone(phone);
			if (SinataUtil.isNotEmpty(content)) {
				feedback.setContent(content);
			}
			User user = userService.selectById(uid);

			feedbackService.insert(feedback);

			return ApiUtil.putSuccessObj("反馈成功!");
		} catch (Exception e) {
			e.printStackTrace();
			return ApiUtil.putFailObj("反馈失败");
		}
	}

	// 用户上传图片
	@RequestMapping("/user/addUserImg")
	@ResponseBody
	public Map<String, Object> addUserImg(Integer uid, String imagess) {
		try {
			if (SinataUtil.isEmpty(uid)) {
				return ApiUtil.putFailObj("用户ID不能为空");
			}
			if (imagess != null & imagess != "") {
				String[] strArray = null;
				String replace = imagess.replace(" ", "");
				strArray = convertStrToArray(replace);
				for (int i = 0; i < strArray.length; i++) {
					Images img = new Images();
					img.setRelevanceId(uid);
					Date data = new Date();
					img.setCreateDate(data);
					img.setType(1);
					img.setIsDelete(1);
					img.setUid(uid);
					img.setUrl(strArray[i]);
					System.out.println(img.getUrl());
					imgesService.insert(img);
				}

			}
			return ApiUtil.putSuccessObj("上传成功!");
		} catch (Exception e) {
			e.printStackTrace();
			return ApiUtil.putFailObj("上传失败");
		}
	}

	// 删除上传用户图片
	@RequestMapping("/User/DeleteImg")
	@ResponseBody
	public Map<String, Object> getFeedback(Integer id) {
		try {
			if (SinataUtil.isEmpty(id)) {
				return ApiUtil.putFailObj("图片ID不能为空");
			}
			Images images = imgesService.selectById(id);
			images.setIsDelete(2);
			imgesService.updateById(images);
			return ApiUtil.putSuccessObj("删除成功!");
		} catch (Exception e) {
			e.printStackTrace();
			return ApiUtil.putFailObj("删除失败");
		}
	}

	// 去找指定用户所在房间
	@RequestMapping("/UserRoom/UserRoom")
	@ResponseBody
	public Map<String, Object> UserRoom(Integer uid, Integer buid) {
		try {
			Chatrooms cs = new Chatrooms();
			cs.setUid(buid);
			cs.setStatus(1);
			Chatrooms cms = ChatroomsService.getOne(cs);
			if (SinataUtil.isNotEmpty(cms)) {
				Chatrooms cs1 = new Chatrooms();
				cs1.setUid(uid);
				cs1.setStatus(1);
				cs1.setPid(cms.getPid());
				Chatrooms cms1 = ChatroomsService.getOne(cs1);
				if (SinataUtil.isNotEmpty(cms1)) {
					return ApiUtil.putFailArray("你已在当前房间");
				} else {
					Map map = new HashMap<>();
					map.put("rid", cms.getPid());
					return ApiUtil.returnArray(map);
				}
			} else {
				Map map = new HashMap<>();
				map.put("rid", "");
				return ApiUtil.returnArray(map);
			}

		} catch (Exception e) {
			e.printStackTrace();
			return ApiUtil.putFailArray("用户添加拉黑记录失败");
		}

	}

	// 个人资料界面
	@RequestMapping("/User/PersonageUser")
	@ResponseBody
	public Map<String, Object> PersonageUser(Integer uid) {
		try {

			Map map1 = new HashMap<>();
			User user = userService.selectById(uid);
			if (SinataUtil.isNotEmpty(user)) {
				Map map = new HashMap<>();
				map.put("imgTx", user.getImgTx());
				map.put("sex", user.getSex());
				map.put("nickname", user.getNickname());
				map.put("treasureGrade", user.getTreasureGrade());
				map.put("charmGrade", user.getCharmGrade());
				map.put("attentionNum", user.getAttentionNum());
				map.put("fansNum", user.getFansNum());
				map.put("constellation", user.getConstellation());
				map.put("usercoding", user.getUsercoding());
				map.put("individuation", user.getIndividuation());
				map.put("voice", user.getVoice());
				map.put("voicetime", user.getVoiceTime());
				map.put("isRoom", user.getIsRoom());
				map.put("userThfm", user.getUserThfm());
				map.put("userZjfm", user.getUserZjfm());
				map.put("userTh", user.getUserTh());
				map.put("userZj", user.getUserZj());
				map.put("liang", user.getLiang());
				map1.put("user", map);

				Images img = new Images();
				img.setUid(uid);
				img.setIsDelete(1);
				List<Images> list = imgesService.getList(img);

				map1.put("img", list);

				List<SceneModel> scm = new ArrayList<>();
				SceneNum sm = new SceneNum();
				sm.setUid(uid);
				List<SceneNum> list2 = SceneNumService.getList(sm);
				Iterator<SceneNum> iterator = list2.iterator();
				while (iterator.hasNext()) {
					SceneNum num = iterator.next();
					Scene selectById = SceneService.selectById(num.getSid());
					SceneModel scml = new SceneModel();
					scml.setId(selectById.getId());
					scml.setImg(selectById.getImg());
					scml.setName(selectById.getName());
					scml.setImgFm(selectById.getImgFm());
					scml.setType(selectById.getState());
					if (num.getState() == 1) {
						if (SinataUtil.isNotEmpty(user.getZjid())) {
							if (user.getZjid().equals(selectById.getId())) {
								scml.setState(2);
							} else {
								scml.setState(1);
							}
						} else {
							scml.setState(1);
						}

					} else {
						if (SinataUtil.isNotEmpty(user.getThid())) {
							if (user.getThid().equals(selectById.getId())) {
								scml.setState(2);
							} else {
								scml.setState(1);
							}
						} else {
							scml.setState(1);
						}

					}

					scm.add(scml);

				}
					
				
				map1.put("scene", scm);
				
				
				Chatrooms cs = new Chatrooms();
				cs.setUid(uid);
				cs.setStatus(1);
				Chatrooms cms = ChatroomsService.getOne(cs);
				if (SinataUtil.isNotEmpty(cms)) {
					Chatrooms cs1 = new Chatrooms();
					cs1.setUid(uid);
					cs1.setStatus(1);
					cs1.setPid(cms.getPid());
					Chatrooms cms1 = ChatroomsService.getOne(cs1);
					if (SinataUtil.isNotEmpty(cms1)) {
						Room r =new Room();
						r.setRid(cms1.getPid());
						Room room = RoomService.getOne(r);
						zhaotaModel zt=new zhaotaModel();
						zt.setRid(cms1.getPid());
						zt.setRimg(user.getImgTx());
						zt.setRname(room.getRoomName());
						map1.put("room", zt);
					} else {
						Room r =new Room();
						r.setRid(cms.getPid());
						Room room = RoomService.getOne(r);
						zhaotaModel zt=new zhaotaModel();
						zt.setRid(cms.getPid());
						zt.setRimg(user.getImgTx());
						zt.setRname(room.getRoomName());
						map1.put("room", zt);
					}
				} else {
					zhaotaModel zt=new zhaotaModel();
					zt.setRid("");
					zt.setRimg("");
					zt.setRname("");
					map1.put("room", zt);
					
				}
				
				
			} else {
				Agreement byID = AgreementService.selectById(uid);
				Map map = new HashMap<>();
				map.put("imgTx", byID.getImg());
				map.put("sex", byID.getSex());
				map.put("nickname", byID.getNickname());
				map.put("treasureGrade", 1);
				map.put("charmGrade", 1);
				map.put("attentionNum", 0);
				map.put("fansNum", 0);
				map.put("constellation", "双鱼");
				map.put("usercoding", byID.getUsercoding());
				map.put("individuation", byID.getIndividuation());
				map.put("voice", "");

				map1.put("user", map);

				Images img = new Images();
				img.setUid(0);
				img.setIsDelete(1);
				List<Images> list = imgesService.getList(img);

				map1.put("img", list);

				List<SceneModel> scm = new ArrayList<>();
				SceneNum sm = new SceneNum();
				sm.setUid(0);
				List<SceneNum> list2 = SceneNumService.getList(sm);
				Iterator<SceneNum> iterator = list2.iterator();
				while (iterator.hasNext()) {
					SceneNum num = iterator.next();
					Scene selectById = SceneService.selectById(num.getSid());
					SceneModel scml = new SceneModel();
					scml.setImg(selectById.getImg());
					scml.setName(selectById.getName());
					scml.setImgFm(selectById.getImgFm());
					scml.setType(selectById.getState());
					if (num.getState() == 1) {
						if (SinataUtil.isNotEmpty(user.getUserZj())) {
							if (user.getUserZj().equals(selectById.getImg())) {
								scml.setState(2);
							} else {
								scml.setState(1);
							}
						} else {
							scml.setState(1);
						}

					} else {
						if (SinataUtil.isNotEmpty(user.getUserTh())) {
							if (user.getUserTh().equals(selectById.getImg())) {
								scml.setState(2);
							} else {
								scml.setState(1);
							}
						} else {
							scml.setState(1);
						}

					}

					scm.add(scml);

				}
				map1.put("scene", scm);
				
				
				Chatrooms cs = new Chatrooms();
				cs.setUid(uid);
				cs.setStatus(1);
				Chatrooms cms = ChatroomsService.getOne(cs);
				if (SinataUtil.isNotEmpty(cms)) {
					Chatrooms cs1 = new Chatrooms();
					cs1.setUid(uid);
					cs1.setStatus(1);
					cs1.setPid(cms.getPid());
					Chatrooms cms1 = ChatroomsService.getOne(cs1);
					if (SinataUtil.isNotEmpty(cms1)) {
						Room r =new Room();
						r.setRid(cms1.getPid());
						Room room = RoomService.getOne(r);
						zhaotaModel zt=new zhaotaModel();
						zt.setRid(cms1.getPid());
						zt.setRimg(user.getImgTx());
						zt.setRname(room.getRoomName());
						map1.put("room", zt);
					} else {
						Room r =new Room();
						r.setRid(cms.getPid());
						Room room = RoomService.getOne(r);
						zhaotaModel zt=new zhaotaModel();
						zt.setRid(cms.getPid());
						zt.setRimg(user.getImgTx());
						zt.setRname(room.getRoomName());
						map1.put("room", zt);
					}
				} else {
					zhaotaModel zt=new zhaotaModel();
					zt.setRid("");
					zt.setRimg("");
					zt.setRname("");
					map1.put("room", zt);
					
				}
			}

			return ApiUtil.returnObj(map1);
		} catch (Exception e) {
			e.printStackTrace();
			return ApiUtil.putFailArray("个人资料失败");
		}

	}

	// 用户获取道具
	@RequestMapping("/User/userScene")
	@ResponseBody
	public Map<String, Object> userScene(Integer uid, int state, int pageNum, int pageSize) {
		try {
			PageHelper.startPage(pageNum, pageSize, false);
			List<GiftRecordModel> gif = new ArrayList<>();
			GfitNum gf = new GfitNum();
			gf.setUid(uid);
			List<GfitNum> list;
			if (state == 1) {
				list = GfitNumService.getList(gf);
				ListSort(list);
			} else {
				list = GfitNumService.getList1(gf);
			}
			Iterator<GfitNum> iterator = list.iterator();
			while (iterator.hasNext()) {
				GfitNum gfitNum = iterator.next();
				Gift gift = GiftService.selectById(gfitNum.getGid());
				GiftRecordModel grm = new GiftRecordModel();
				grm.setImg(gift.getImgFm());
				grm.setName(gift.getName());
				grm.setNum(gfitNum.getNum());
				gif.add(grm);
			}

			return ApiUtil.returnArray(gif);
		} catch (Exception e) {
			e.printStackTrace();
			return ApiUtil.putFailObj("删除失败");
		}
	}

	// 用户用优钻兑换优币
	@RequestMapping("/User/UserConvert")
	@ResponseBody
	public Map<String, Object> UserConvert(Integer uid, Integer gold) {
		try {
			User user = userService.selectById(uid);
			if (user.getYnum() < gold) {
				return ApiUtil.putFailObj("优币不足");
			}
			user.setYnum(user.getYnum() - gold);
			user.setGold(user.getGold() + gold);

			userService.updateById(user);

			Ybexchange yb = new Ybexchange();
			yb.setCreateTime(new Date());
			yb.setUid(uid);
			yb.setName(user.getNickname());
			yb.setEid(Integer.parseInt(user.getUsercoding()));
			yb.setDhDiamond(gold);
			yb.setHdGold(gold);

			Map map = new HashMap<>();
			Award award = AwardService.selectById(1);
			if (award.getY() > gold) {
				if (gold >= award.getX()) {
					Award award1 = AwardService.selectById(2);
					int i = getRandom2(award1.getX(), award1.getY());
					user.setGold(user.getGold() + i);
					userService.updateById(user);
					yb.setQtGold(i);
					map.put("state", 1);
					map.put("num", i);
				} else {
					map.put("state", 0);
					map.put("num", "");
				}
			}

			YbexchangeService.insert(yb);
			if (gold >= award.getY()) {
				Award award1 = AwardService.selectById(3);
				Scene se = new Scene();
				se.setX(award1.getX());
				se.setY(award1.getY());
				se.setState(1);
				se.setIsState(1);
				Scene se1 = SceneService.getOne1(se);
				if (SinataUtil.isNotEmpty(se1)) {
					SceneNum gm = new SceneNum();
					gm.setSid(se1.getId());
					gm.setUid(uid);
					SceneNum one = SceneNumService.getOne(gm);
					if (SinataUtil.isEmpty(one)) {
						SceneNum gm1 = new SceneNum();
						gm1.setCreateTime(new Date());
						gm1.setSid(se1.getId());
						gm1.setState(se1.getState());
						gm1.setUid(uid);
						gm1.setNum(1);
						SceneNumService.insert(gm1);
					} else {
						one.setNum(one.getNum() + 1);
						SceneNumService.updateById(one);
					}
				}
				map.put("state", 2);
				map.put("num", se1.getImgFm());

			}

			return ApiUtil.returnArray(map);
		} catch (Exception e) {
			e.printStackTrace();
			return ApiUtil.putFailObj("删除失败");
		}
	}

	// 用户用优钻兑换优币信息
	@RequestMapping("/User/UserConvertlist")
	@ResponseBody
	public Map<String, Object> UserConvertlist() {
		try {

			Award award = AwardService.selectById(1);
			Map map = new HashMap();
			String str = "单次兑换达到" + award.getX() + "优币额外爆优币，单次兑换达到" + award.getY() + "优币额外爆座驾";
			map.put("str", str);

			return ApiUtil.returnArray(map);
		} catch (Exception e) {
			e.printStackTrace();
			return ApiUtil.putFailObj("删除失败");
		}
	}

	// 用户财富，用户魅力
	@RequestMapping("/User/UserGold")
	@ResponseBody
	public Map<String, Object> UserGold(Integer uid, Integer state) {
		try {
			Map map = new HashMap<>();
			User user = userService.selectById(uid);
			if (state == 1) {
				map.put("Grade", user.getTreasureGrade());

				Integer setGrade2 = new GradeUtil().SetGrade(user.getGoldNum(), 1, uid, 1);

				if (SinataUtil.isNotEmpty1(setGrade2)) {
					SetGrade selectById3 = SetGradeService.selectById(1);
					SetGrade selectById2 = SetGradeService.selectById(10);
					SetGrade sg = new SetGrade();
					sg.setNum(user.getGoldNum());
					sg.setNum1(user.getGoldNum());
					sg.setState(1);
					SetGrade jie = SetGradeService.getJie(sg);
					if (user.getGoldNum()<selectById3.getZ()||user.getGoldNum() >= selectById2.getP()) {
						if(user.getGoldNum()<selectById3.getZ()){
							Integer b = selectById3.getB()*2;
							map.put("gradeNum", b - user.getGoldNum());
							map.put("num", b - user.getGoldNum());
							map.put("zd", b);
							map.put("xz", user.getGoldNum());
						}
						if(user.getGoldNum() >= selectById2.getP()){
							map.put("gradeNum", 0);
							map.put("num", 0);
							map.put("zd", selectById2.getP());
							map.put("xz", selectById2.getP());
						}
					} else {
						if (jie.getX().equals(jie.getY())) {

							map.put("gradeNum", jie.getP() - user.getGoldNum());
							map.put("num", jie.getP() - user.getGoldNum());
							map.put("zd", jie.getP());
							map.put("xz", user.getGoldNum());

						} else {
							if (jie.getId() == 1) {
								Integer m = user.getTreasureGrade() + 1;
								Integer m1 = m * jie.getB();

								map.put("gradeNum", m1 - user.getGoldNum());
								map.put("num", m1 - user.getGoldNum());
								map.put("zd", m1);
								map.put("xz", user.getGoldNum());
							} else {
								Integer m = user.getTreasureGrade() + 1;
								Integer m3 = m - jie.getX();
								Integer m1 = m3 * jie.getB();
								Integer m4 = m1 + jie.getZ();

								map.put("gradeNum", m4 - user.getGoldNum());
								map.put("num", m4 - user.getGoldNum());
								map.put("zd", m4);
								map.put("xz", user.getGoldNum());
							}

						}
					}

				} else {
					map.put("gradeNum", 0);
					map.put("num", 0);
				}

			} else {
				map.put("Grade", user.getCharmGrade());
				SetGrade selectById3 = SetGradeService.selectById(11);
				SetGrade selectById2 = SetGradeService.selectById(20);

				Integer setGrade2 = new GradeUtil().SetGrade(user.getYuml(), 2, uid, 2);

				if (SinataUtil.isNotEmpty1(setGrade2)) {

					SetGrade sg = new SetGrade();
					sg.setNum(user.getYuml());
					sg.setNum1(user.getYuml());
					sg.setState(2);
					SetGrade jie = SetGradeService.getJie(sg);
					
					if (user.getYuml()<selectById3 .getZ()||user.getYuml() >= selectById2.getP()) {
						if(user.getYuml()<selectById3 .getZ()){
							Integer b = selectById3.getB()*2;
							map.put("gradeNum", b- user.getYuml());
							map.put("num",  b- user.getYuml());
							map.put("zd", b);
							map.put("xz", user.getYuml());
						}
						if(user.getYuml() >= selectById2.getP()){
							map.put("gradeNum", 0);
							map.put("num", 0);
							map.put("zd", selectById2.getP());
							map.put("xz", selectById2.getP());
						}
						
					} else {
						if (jie.getX().equals(jie.getY())) {

							map.put("gradeNum", jie.getP() - user.getYuml());
							map.put("num", jie.getP() - user.getYuml());
							map.put("zd", jie.getP());
							map.put("xz", user.getYuml());

						} else {
							Integer m = user.getCharmGrade() + 1;
							Integer m3 = m - jie.getX();
							Integer m1 = m3 * jie.getB();
							Integer m4 = m1 + jie.getZ();

							map.put("gradeNum", m4 - user.getYuml());
							map.put("num", m4 - user.getYuml());
							map.put("zd", m4);
							map.put("xz", user.getYuml());
						}
					}

				} else {
					map.put("gradeNum", 0);
					map.put("num", 0);
				}
			}

			return ApiUtil.returnArray(map);
		} catch (Exception e) {
			e.printStackTrace();
			return ApiUtil.putFailObj("删除失败");
		}
	}

	// 财富，魅力基本信息
	@RequestMapping("/User/UserList")
	@ResponseBody
	public Map<String, Object> UserList(Integer state) {
		try {
			SetGrade set = new SetGrade();
			set.setState(state);
			List<SetGrade> list = SetGradeService.getList(set);

			return ApiUtil.returnArray(list);
		} catch (Exception e) {
			e.printStackTrace();
			return ApiUtil.putFailObj("删除失败");
		}
	}
	
	

	// 绑定支付宝
	@RequestMapping("/User/UserPay")
	@ResponseBody
	public Map<String, Object> UserList(Integer uid, String payAccount, String phone, String trueName, String smsCode) {
		try {
			SmsCode sms = new SmsCode();
			sms.setPhone(phone);
			SmsCode sms2 = smsService.getByPhone(phone);
			if (sms2 != null) {
				// 判读验证码是否有效
				if (smsValid(sms2.getCreateTime()).equals("00")) {
					return ApiUtil.putFailObj("验证码已过期！");
				}
				if (!smsCode.equals(sms2.getCode())) {
					return ApiUtil.putFailObj("验证码错误！");
				}
			} else {
				return ApiUtil.putFailObj("请发送验证码！");
			}
			User user = userService.selectById(uid);
			user.setPayAccount(payAccount);
			user.setTrueName(trueName);
			userService.updateById(user);
			return ApiUtil.putSuccessObj("成功");
		} catch (Exception e) {
			e.printStackTrace();
			return ApiUtil.putFailObj("删除失败");
		}
	}

	// 消息送礼物
	@RequestMapping("/user/SaveGift")
	@ResponseBody
	public Map<String, Object> SaveGift(HttpServletRequest request, Integer uid, String ids, Integer gid, Integer num,
			Integer sum, Integer isFromBag) {
		if (isFromBag == null)
		{
			isFromBag = 0;
		}
		System.out.println("/user/SaveGift:uid="+uid+",gid="+gid+",num="+num+",rid="+isFromBag);
		
		try {

			User user = userService.selectById(uid);
			Gift gift = GiftService.selectById(gid);
			Integer gum = gift.getGold() * num * sum;
			
			// 直接扣金币
			if (isFromBag == 0)
			{
				if (user.getGold() < gum) {
					return ApiUtil.putFailObj("优币不足");
			    }
				
				user.setGold(user.getGold() - gum);
			}
			// 从背包里减去
			else
			{
				UserBag userbag_model = new UserBag();
				userbag_model.setUid(uid);
				userbag_model.setGid(gid);
				UserBag userbag = userBagService.getOne(userbag_model);
				if (userbag == null)
				{
					return ApiUtil.putFailObj("包裹内礼物数不足");
				}
				if (userbag.getNum() < (num * sum))
				{
					return ApiUtil.putFailObj("包裹内礼物数不足");
				}
				
				userbag.setNum(userbag.getNum() - (num * sum));
				userBagService.updateById(userbag);
			}
			
			
			Integer nu = user.getGoldNum() + gum;
			Integer grade = user.getTreasureGrade();

			Integer setGrade2 = new GradeUtil().SetGrade(nu, 1, uid, 1);

			user.setTreasureGrade(setGrade2);
			user.setGoldNum(nu);

			user.setTreasureGrade(setGrade2);

			if (!grade.equals(setGrade2)) {
				Map map = new HashMap<>();
				map.put("state", "1");
				map.put("id", user.getId().toString());
				map.put("name", user.getNickname());
				map.put("grade", user.getTreasureGrade().toString());
				Jpush.push(uid.toString(), "等级提升了", map);
			}

			userService.updateById(user);

			gift.setVolume(gift.getVolume() + num);
			giftService.updateById(gift);

			Ybconsume yb = new Ybconsume();
			yb.setCreateTime(new Date());
			yb.setUid(uid);
			yb.setName(user.getNickname());
			yb.setState(1);
			yb.setLwName(gift.getName());
			yb.setConsumeNum(num);
			yb.setConsumeYbNum(gum);
			yb.setIsDelete(1);
			yb.setEid(user.getUsercoding());
			YbconsumeService.insert(yb);

			if (SinataUtil.isNotEmpty(ids)) {
				String[] strArray = null;
				String replace = ids.replace(" ", "");
				strArray = convertStrToArray(replace);
				for (int i = 0; i < strArray.length; i++) {

					GiftRecord gr = new GiftRecord();
					gr.setUid(uid);
					gr.setCreateDate(new Date());
					gr.setState(1);
					gr.setGid(gid);
					gr.setNum(num);
					gr.setBuid(Integer.valueOf(strArray[i]));
					GiftRecordService.insert(gr);

					GiftRecord gr1 = new GiftRecord();
					gr1.setUid(uid);
					gr1.setCreateDate(new Date());
					gr1.setState(2);
					gr1.setGid(gid);
					gr1.setNum(num);
					gr1.setBuid(Integer.valueOf(strArray[i]));
					GiftRecordService.insert(gr1);

					User user2 = userService.selectById(strArray[i]);
					Integer gu = gift.getYz() * num;

					user2.setYnum(user2.getYnum() + gu);

					Integer gumy = gift.getGold() * num;
					Integer n = user2.getYuml() + gumy;

					Integer setGrade3 = new GradeUtil().SetGrade(n, 2, Integer.valueOf(strArray[i]), 2);
					user2.setYuml(n);
					user2.setCharmGrade(setGrade3);

					userService.updateById(user2);
					
					
					RoomUser ru=new RoomUser();
                	ru.setBuid( Integer.valueOf(strArray[i]));
                	ru.setUid(uid);
                	ru.setType(1);
                	Integer gms=gumy-gu;
                	ru.setZhuan(gms);
                	ru.setState(1);
                	ru.setCreateTime(new Date());
                	RoomUserService.insert(ru);

					GfitNum gm = new GfitNum();
					gm.setCreateTime(new Date());
					gm.setGid(gid);
					gm.setType(3);
					gm.setNum(num);
					gm.setGold(gift.getGold());
					gm.setUid(Integer.valueOf(strArray[i]));
					GfitNumService.insert(gm);

					Gift gift1 = GiftService.selectById(gid);
					Integer gum1 = gift1.getYz() * num;
					Charm ch = new Charm();
					ch.setEid(user2.getUsercoding());
					ch.setUid(Integer.valueOf(strArray[i]));
					ch.setNum(gum1);
					ch.setIsDelete(1);
					ch.setBuid(uid);
					ch.setCreateTime(new Date());
					charmService.insert(ch);

					Yzconsume yz = new Yzconsume();
					yz.setCreateTime(new Date());
					yz.setUid(Integer.valueOf(strArray[i]));
					yz.setName(user2.getNickname());
					yz.setState(1);
					yz.setLwName(gift.getName());
					yz.setConsumeNum(num);
					yz.setConsumeYzNum(gu);
					yz.setIsDelete(1);
					yz.setEid(user.getUsercoding());
					YzconsumeService.insert(yz);
				}
			}

			return ApiUtil.returnObj(user);
		} catch (Exception e) {
			e.printStackTrace();
			return ApiUtil.putFailArray("财务明细失败");
		}

	}
	
	// 查询用户背包内容
	@RequestMapping("/user/GetBag")
	@ResponseBody
	public Map<String, Object> GetBag(HttpServletRequest request, Integer uid) {
		System.out.println("/user/GetBag:uid=" + uid);
		
		class UserBagGift
		{
			public Integer uid;
			public Integer gid;
			public Integer num;
			public Gift gift;
		};
		try {		
			List<UserBagGift> list = new ArrayList<UserBagGift>();

			List<UserBag> list_userbag = userBagService.getByUid(uid);
			Iterator<UserBag> iterator = list_userbag.iterator();
			while (iterator.hasNext()) {
				UserBag userbag = iterator.next();
				Gift giftmodel = new Gift();
				giftmodel.setId(userbag.getGid());
				Gift gift = giftService.getOne(giftmodel);
				if (gift != null)
				{
					UserBagGift userbaggift = new UserBagGift();
					userbaggift.uid = userbag.getUid();
					userbaggift.gid = userbag.getGid();
					userbaggift.num = userbag.getNum();
					userbaggift.gift = gift;
					
					list.add(userbaggift);
				}
			}
			String str = JSONObject.toJSONString(list);
			System.out.println("return=" + str);
			return ApiUtil.returnArray(list);
		} catch (Exception e) {
			e.printStackTrace();
			return ApiUtil.putFailArray("失败");
		}
	}
	
	   // 厅主获取一个时间段的优转
		@RequestMapping("/User/RoomUser")
		@ResponseBody
		public Map<String, Object> RoomUser(String rid,String beginTime, String endTime) {
			try {
				RoomUser set = new RoomUser();
				set.setType(1);
				set.setBeginTime(beginTime);
				set.setEndTime(endTime);
				set.setRoomid(rid);
				Integer  list = RoomUserService.getSum(set);
				Map map=new HashMap<>();
				if(SinataUtil.isEmpty(list)){
					map.put("num", 0);
				}else{
					map.put("num", list);
				}
				return ApiUtil.returnArray(map);
			} catch (Exception e) {
				e.printStackTrace();
				return ApiUtil.putFailObj("删除失败");
			}
		}
		
		
		       // 厅主一个时间段厅里人的有笔消耗
				@RequestMapping("/User/RoomUserList")
				@ResponseBody
				public Map<String, Object> RoomUserList(String rid,String beginTime, String endTime,Integer pageNum,Integer pageSize) {
					try {
						Map map=new HashMap<>();
						List<CharmModel> li = new ArrayList<>();
						Charm se = new Charm();
						se.setRid(rid);
						se.setBeginTime(beginTime);
						se.setEndTime(endTime);
						List<Charm> list1 = charmService.getSum3(se);
						map.put("num", list1.size());
						
						PageHelper.startPage(pageNum, pageSize, false);
						List<Charm> list = charmService.getSum3(se);
						Iterator<Charm> iterator = list.iterator();
						while (iterator.hasNext()) {
							Charm next = iterator.next();
							User user = userService.selectById(next.getUid());
							CharmModel cl = new CharmModel();
							cl.setGrade(user.getCharmGrade());
							cl.setId(user.getId());
							cl.setImg(user.getImgTx());
							cl.setName(user.getNickname());
							cl.setNum(next.getNum());
							cl.setSex(user.getSex());
							cl.setUsercoding(user.getUsercoding());
							li.add(cl);
						}
						map.put("userlist", li);
						return ApiUtil.returnArray(map);
					} catch (Exception e) {
						e.printStackTrace();
						return ApiUtil.putFailObj("删除失败");
					}
				}
	
	

	@RequestMapping("/FX")
	public Object FX(Integer id, Integer type) {
		if (type == 1) {
			return "/index.html?id=" + id;
		}
		if (type == 2) {
			return "/share.html";
		}
		return null;

	}
	
	

	// 使用String的split 方法
	public static String[] convertStrToArray(String str) {
		String[] strArray = null;
		strArray = str.split(","); // 拆分字符为"," ,然后把结果交给数组strArray
		return strArray;
	}

	private static int getRandom2(int x, int y) {
		Random random = new Random();
		int num = -1;
		// 说明：两个数在合法范围内，并不限制输入的数哪个更大一些
		if (x < 0 || y < 0) {
			return num;
		} else {
			int max = x > y ? x : y;
			int min = x < y ? x : y;
			int mid = max - min;// 求差
			// 产生随机数
			num = min + random.nextInt(mid + 1);
		}
		return num;
	}

	/**
	 * 判读验证码是否有效
	 * 
	 * @param addTime
	 * @return
	 * @author TaoNingBo
	 */
	public static String smsValid(Date addTime) {
		long nowdate = DateUtil.getCurMilli();
		long xiangcha = (nowdate - DateUtil.getMillisecond(addTime)) / 1000 / 60; // 时间相差分钟数
		if (xiangcha >= 10) {
			return "00";
		}
		return "";
	}

	private static void ListSort(List<GfitNum> list) {
		Collections.sort(list, new Comparator<GfitNum>() {
			@Override
			public int compare(GfitNum o1, GfitNum o2) {

				try {
					Integer dt1 = o1.getGold();
					Integer dt2 = o2.getGold();
					if (dt1 < dt2) {
						return 1;
					} else if (dt1 > dt2) {
						return -1;
					} else {
						return 0;
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				return 0;
			}
		});
	}
}
