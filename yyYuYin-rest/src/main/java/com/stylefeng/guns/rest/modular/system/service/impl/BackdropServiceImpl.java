package com.stylefeng.guns.rest.modular.system.service.impl;

import com.stylefeng.guns.rest.modular.system.model.Backdrop;
import com.stylefeng.guns.rest.modular.system.dao.BackdropMapper;
import com.stylefeng.guns.rest.modular.system.service.IBackdropService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import java.util.List;

import org.springframework.stereotype.Service;

/**
 * <p>
 * 背景图 服务实现类
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
@Service
public class BackdropServiceImpl extends ServiceImpl<BackdropMapper, Backdrop> implements IBackdropService {

	@Override
	public List<Backdrop> getList(Backdrop model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getList(model);
	}

	@Override
	public Backdrop getOne(Backdrop model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getOne(model);
	}

}
