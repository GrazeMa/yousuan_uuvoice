package com.stylefeng.guns.rest.modular.system.service.impl;

import com.stylefeng.guns.rest.modular.system.model.Yzconsume;
import com.stylefeng.guns.rest.modular.system.dao.YzconsumeMapper;
import com.stylefeng.guns.rest.modular.system.service.IYzconsumeService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import java.util.List;

import org.springframework.stereotype.Service;

/**
 * <p>
 * 优获取记录 服务实现类
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
@Service
public class YzconsumeServiceImpl extends ServiceImpl<YzconsumeMapper, Yzconsume> implements IYzconsumeService {

	@Override
	public List<Yzconsume> getList(Yzconsume model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getList(model);
	}

	@Override
	public Yzconsume getOne(Yzconsume model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getOne(model);
	}

}
