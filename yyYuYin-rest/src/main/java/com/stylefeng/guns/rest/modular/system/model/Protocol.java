package com.stylefeng.guns.rest.modular.system.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * app各种html页面
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
@TableName("app_protocol")
public class Protocol extends Model<Protocol> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 类型（1生鲜平台用户协议，2 用户隐私协议, 3 充返活动协议 ，4 邀请说明，5平台协议，6关于我们 ,7会员说明）
     */
    private Integer type;
    /**
     * html内容
     */
    private String content;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "Protocol{" +
        "id=" + id +
        ", type=" + type +
        ", content=" + content +
        "}";
    }
}
