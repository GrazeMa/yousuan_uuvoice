package com.stylefeng.guns.rest.modular.system.controller.model;

public class userModels {
	private int grade;// 财富等级
	private String name;// 名称
	private int uid;// 用户id
	private int state;// 消息状态（105 1 设置管理员，2 取消管理员）

	private String messageShow;// 消息内容

	public String getMessageShow() {
		return messageShow;
	}

	public void setMessageShow(String messageShow) {
		this.messageShow = messageShow;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public int getGrade() {
		return grade;
	}

	public void setGrade(int grade) {
		this.grade = grade;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getUid() {
		return uid;
	}

	public void setUid(int uid) {
		this.uid = uid;
	}

}
