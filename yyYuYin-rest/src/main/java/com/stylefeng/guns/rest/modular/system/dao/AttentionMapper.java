package com.stylefeng.guns.rest.modular.system.dao;

import com.stylefeng.guns.rest.modular.system.model.Attention;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wumeng123
 * @since 2019-02-18
 */
public interface AttentionMapper extends BaseMapper<Attention> {

	List<Attention> getList(Attention model);

	Attention getOne(Attention model);

}
