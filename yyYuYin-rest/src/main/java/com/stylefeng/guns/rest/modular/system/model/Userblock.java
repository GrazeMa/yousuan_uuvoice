package com.stylefeng.guns.rest.modular.system.model;

import java.io.Serializable;

import java.util.Date;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 用户拉黑
 * </p>
 *
 * @author wumeng123
 * @since 2019-03-21
 */
@TableName("app_userblock")
public class Userblock extends Model<Userblock> {

    private static final long serialVersionUID = 1L;

    private Integer id;
    private Date createTime;
    private Integer uid;
    /**
     * 被拉黑用户id
     */
    private Integer buid;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public Integer getBuid() {
        return buid;
    }

    public void setBuid(Integer buid) {
        this.buid = buid;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "Userblock{" +
        "id=" + id +
        ", createTime=" + createTime +
        ", uid=" + uid +
        ", buid=" + buid +
        "}";
    }
}
