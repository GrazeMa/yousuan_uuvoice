package com.stylefeng.guns.rest.modular.system.dao;

import com.stylefeng.guns.rest.modular.system.model.SetGrade;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 等级设置 Mapper 接口
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
public interface SetGradeMapper extends BaseMapper<SetGrade> {

	List<SetGrade> getList(SetGrade model);

	SetGrade getOne(SetGrade model);

	SetGrade getJie(SetGrade model);

}
