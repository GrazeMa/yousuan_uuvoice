package com.stylefeng.guns.rest.modular.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import com.github.qcloudsms.SmsSingleSender;
import com.github.qcloudsms.SmsSingleSenderResult;
import com.github.qcloudsms.httpclient.HTTPException;
import org.json.JSONException;

import java.io.IOException;
import com.stylefeng.guns.rest.modular.util.ParamUtil;

public class TXSmSend {

	// 短信应用SDK AppID
	//private static int appid = 1400243569; // 1400开头
  private static int appid = Integer.parseInt( ParamUtil.getValue("txsms_app_id") ); // 1400开头

	// 短信应用SDK AppKey
	//private static String appkey = "1d0460655dc1a7dfdefd66258fbcecb2";
  private static String appkey = ParamUtil.getValue("txsms_app_key");

	// 短信模板ID，需要在短信应用中申请
	//private static int templateId = 448488; // NOTE: 这里的模板ID`7839`只是一个示例，真实的模板ID需要在短信控制台中申请
  
    private static int templateId = Integer.parseInt( ParamUtil.getValue("txsms_templ_id"));
  
	//templateId7839对应的内容是"您的验证码是: {1}"
	// 签名
    // yuanxiaoshu 短信签名由"优优语音"改为""
	private static String smsSign = "悠算信息"; // NOTE: 签名参数使用的是`签名内容`，而不是`签名ID`。这里的签名"腾讯云"只是一个示例，真实的签名需要在短信控制台申请。
	
		@SuppressWarnings("unchecked")
		public static String sendSMS(String msg,String... phone) {
		try {
			
			StringBuffer telPhone = new StringBuffer();
			for (int i = 0; i < phone.length; i++) {
				telPhone.append(phone[i] + ",");
			}
			
		    String[] params = {msg,"10"};//数组具体的元素个数和模板中变量个数必须一致，例如事例中templateId:5678对应一个变量，参数数组中元素个数也必须是一个
		    SmsSingleSender ssender = new SmsSingleSender(appid, appkey);
		    SmsSingleSenderResult result = ssender.sendWithParam("86",telPhone.substring(0, telPhone.length() - 1).toString(),
		        templateId, params, smsSign, "", "");  // 签名参数未提供或者为空时，会使用默认签名发送短信
		    //msg = "【优优语音】您的登陆验证码为" + msg + "，请予10分钟内填写。如非本人操作，请忽略本短信！";
		    //SmsSingleSenderResult result = ssender.send(0, "86", telPhone.substring(0, telPhone.length() - 1).toString(),msg, "", "");
		    return result.ext;
		    
		} catch (HTTPException e) {
		    // HTTP响应码错误
		    e.printStackTrace();
		} catch (JSONException e) {
		    // json解析错误
		    e.printStackTrace();
		} catch (IOException e) {
		    // 网络IO错误
		    e.printStackTrace();
		}
		return appkey;
		
	
		
	 }
		
		
	@SuppressWarnings("unchecked")
	 public static String sendPayNotification(String... phone) {
	  try {
	   
	    StringBuffer telPhone = new StringBuffer();
	    for (int i = 0; i < phone.length; i++) {
	     telPhone.append(phone[i] + ",");
	    }
	    
	       String[] params = {};//数组具体的元素个数和模板中变量个数必须一致，例如事例中templateId:5678对应一个变量，参数数组中元素个数也必须是一个
	       SmsSingleSender ssender = new SmsSingleSender(appid, appkey);
	       SmsSingleSenderResult result = ssender.sendWithParam("86",telPhone.substring(0, telPhone.length() - 1).toString(),
	           631709, params, smsSign, "", "");  // 签名参数未提供或者为空时，会使用默认签名发送短信
	       //msg = "【悠算信息】您的登陆验证码为" + msg + "，请予10分钟内填写。如非本人操作，请忽略本短信！";
	       //SmsSingleSenderResult result = ssender.send(0, "86", telPhone.substring(0, telPhone.length() - 1).toString(),msg, "", "");
	       return result.ext;
	      
	  } catch (HTTPException e) {
	      // HTTP响应码错误
	      e.printStackTrace();
	  } catch (JSONException e) {
	      // json解析错误
	      e.printStackTrace();
	  } catch (IOException e) {
	      // 网络IO错误
	      e.printStackTrace();
	  }
	  return appkey;
	 
	 }
		
	public static void main(String[] args) {
		sendSMS("123456","18380476116");
	}
}
