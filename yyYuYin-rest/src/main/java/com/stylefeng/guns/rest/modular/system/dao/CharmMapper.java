package com.stylefeng.guns.rest.modular.system.dao;

import com.stylefeng.guns.rest.modular.system.model.Charm;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 房间魅力榜 Mapper 接口
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
public interface CharmMapper extends BaseMapper<Charm> {

	List<Charm> getList(Charm model);

	Charm getOne(Charm model);

	List<Charm> getSum(Charm model);

	Charm getSum1(Charm model);

	List<Charm> getSum2(Charm model);
	
	
	List<Charm> getSumH(Charm model);

	List<Charm> getSum3(Charm model);

}
