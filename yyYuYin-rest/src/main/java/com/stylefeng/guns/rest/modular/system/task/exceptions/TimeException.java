package com.stylefeng.guns.rest.modular.system.task.exceptions;

/** 
 * @文件说明:定时器任务执行异常 
 * @版权所有：成都喜来达
 * @项目名称: fengsheng
 * @创建者: Leeyns 
 * @创建日期: 2016年5月18日
 * @最近修改者：Leeyns 
 * @最近修改日期：2016年5月18日
 */ 
public class TimeException extends Exception {

	/**
	 *  TODO
	 */
	private static final long serialVersionUID = 5703430073981692250L;

	private String message;
	
	public TimeException() {
		super();
	}
	
	public TimeException(String message) {
		super(message);
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
