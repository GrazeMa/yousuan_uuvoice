package com.stylefeng.guns.rest.modular.system.tencent.protocol;

import java.lang.reflect.Field;


import java.util.HashMap;
import java.util.Map;

import com.stylefeng.guns.rest.modular.system.tencent.common.RandomStringGenerator;
import com.stylefeng.guns.rest.modular.system.tencent.common.Signature;
import com.stylefeng.guns.rest.modular.util.DateUtil;




/**
 * 调起支付-API需要提交的数据
 * @author pan
 */
public class AppPayReqData {

    //每个字段具体的意思请查看API文档
	private String appid;//公众账号ID
	private String partnerid;//商户号
	private String prepayid;//预支付编号（微信返回的支付交易会话ID）
	private String _package = "Sign=WXPay";//扩展字段(暂填写固定值Sign=WXPay)
	private String noncestr = RandomStringGenerator.getRandomStringByLength(32);//随机字符串 （32位）
	private String timestamp = DateUtil.nowDateLongStr().substring(0, 10);//时间戳
	private String sign;//签名 
	/**
	 * 调用微信支付
	 * @param appid 微信分配的APPID
	 * @param partnerid 微信支付分配的商户号ID
	 * @param prepayid 预支付编号（微信返回的支付交易会话ID）
	 */
    public AppPayReqData(Integer apptype, String appid, String partnerid, String prepayid, String noncestr){
    	//微信分配的APPID
    	this.appid = appid;
    	//微信支付接口与预支付接口随机字符串，保持一致！
    	this.noncestr = noncestr;
        //微信支付分配的商户号ID
    	this.partnerid = partnerid;
        //预支付编号（微信返回的支付交易会话ID）
        this.prepayid = prepayid;
		// 根据API给的签名规则进行签名
		String sign = Signature.getSign(apptype, toMap());
		setSign(sign);//微信公众号（最后参与签名的参数有appId, timeStamp, nonceStr, package, signType）
    }

    public Map<String,Object> toMap(){
        Map<String,Object> map = new HashMap<String, Object>();
        Field[] fields = this.getClass().getDeclaredFields();
        for (Field field : fields) {
            Object obj;
            try {
                obj = field.get(this);
                if(obj!=null){
                    if ("_package".equals(field.getName())) {
                    	map.put("package", obj);
					}else{
						 map.put(field.getName(), obj);
					}
                }
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return map;
    }

	public String getAppid() {
		return appid;
	}

	public void setAppid(String appid) {
		this.appid = appid;
	}

	public String getPartnerid() {
		return partnerid;
	}

	public void setPartnerid(String partnerid) {
		this.partnerid = partnerid;
	}

	public String getPrepayid() {
		return prepayid;
	}

	public void setPrepayid(String prepayid) {
		this.prepayid = prepayid;
	}

	public String get_package() {
		return _package;
	}

	public void set_package(String _package) {
		this._package = _package;
	}

	public String getNoncestr() {
		return noncestr;
	}

	public void setNoncestr(String noncestr) {
		this.noncestr = noncestr;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

}