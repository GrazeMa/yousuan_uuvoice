package com.stylefeng.guns.rest.modular.system.controller.job;

import java.util.Iterator;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.stylefeng.guns.rest.modular.system.controller.model.UserModel;
import com.stylefeng.guns.rest.modular.system.model.User;
import com.stylefeng.guns.rest.modular.system.model.Userlist;
import com.stylefeng.guns.rest.modular.system.service.IAuditService;
import com.stylefeng.guns.rest.modular.system.service.IBroadService;
import com.stylefeng.guns.rest.modular.system.service.IInviteService;
import com.stylefeng.guns.rest.modular.system.service.ISetShareService;
import com.stylefeng.guns.rest.modular.system.service.IUserService;
import com.stylefeng.guns.rest.modular.system.service.IUserlistService;
import com.stylefeng.guns.rest.modular.system.service.IWithdrawService;

@Component
@EnableScheduling
public class UserJob {

	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private IUserService userService;

	@Autowired
	private IUserlistService UserlistService;

	@Scheduled(cron = "0 */50 * * * ?")
	public void updateStatusToOutTime() {
		System.out.println("UserJob:updateStatusToOutTime");
		try {
			Userlist usl = new Userlist();
			List<Userlist> list = UserlistService.getList(usl);
			Iterator<Userlist> iterator = list.iterator();
			while (iterator.hasNext()) {
				Userlist userlist = iterator.next();
				UserlistService.deleteById(userlist.getId());
			}
			User user = new User();
			List<User> list1 = userService.getList3(user);
			for (int i = 0; i < list1.size(); i++) {
				User user2 = list1.get(i);
				Userlist u = new Userlist();
				u.setNum(i + 1);
				u.setUid(user2.getId());
				UserlistService.insert(u);
			}

		} catch (Exception e) {
			e.printStackTrace();
			log.error("商品库存记录添加异常", e.getMessage());
		}
	}

}
