package com.stylefeng.guns.rest.modular.system.service;

import com.stylefeng.guns.rest.modular.system.model.Invite;
import com.stylefeng.guns.rest.modular.system.model.Invite;

import java.util.List;

import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 邀请用户 服务类
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
public interface IInviteService extends IService<Invite> {
public List<Invite> getList(Invite model);
    
    Invite getOne(Invite model);
    
    public List<Invite> getList1(Invite model);
    
    Integer getCount(Invite model);
}
