package com.stylefeng.guns.rest.modular.system.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageHelper;
import com.stylefeng.guns.rest.common.util.ApiJson;
import com.stylefeng.guns.rest.modular.system.controller.model.CharmModel;
import com.stylefeng.guns.rest.modular.system.controller.model.ClaimerModel;
import com.stylefeng.guns.rest.modular.system.controller.model.HotModel;
import com.stylefeng.guns.rest.modular.system.controller.model.UserModel;
import com.stylefeng.guns.rest.modular.system.model.BannerInfo;
import com.stylefeng.guns.rest.modular.system.model.BulletScreen;
import com.stylefeng.guns.rest.modular.system.model.Charm;
import com.stylefeng.guns.rest.modular.system.model.Chatrooms;
import com.stylefeng.guns.rest.modular.system.model.Claimer;
import com.stylefeng.guns.rest.modular.system.model.Guild;
import com.stylefeng.guns.rest.modular.system.model.GuildOrganization;
import com.stylefeng.guns.rest.modular.system.model.Headline;
import com.stylefeng.guns.rest.modular.system.model.Hot;
import com.stylefeng.guns.rest.modular.system.model.Recommend;
import com.stylefeng.guns.rest.modular.system.model.Room;
import com.stylefeng.guns.rest.modular.system.model.SetSpeed;
import com.stylefeng.guns.rest.modular.system.model.Sign;
import com.stylefeng.guns.rest.modular.system.model.SignNum;
import com.stylefeng.guns.rest.modular.system.model.Treasure;
import com.stylefeng.guns.rest.modular.system.model.User;
import com.stylefeng.guns.rest.modular.system.service.IBannerInfoService;
import com.stylefeng.guns.rest.modular.system.service.IBulletScreenService;
import com.stylefeng.guns.rest.modular.system.service.ICharmService;
import com.stylefeng.guns.rest.modular.system.service.IChatroomsService;
import com.stylefeng.guns.rest.modular.system.service.IClaimerService;
import com.stylefeng.guns.rest.modular.system.service.IGuildOrganizationService;
import com.stylefeng.guns.rest.modular.system.service.IGuildService;
import com.stylefeng.guns.rest.modular.system.service.IHeadlineService;
import com.stylefeng.guns.rest.modular.system.service.IHotService;
import com.stylefeng.guns.rest.modular.system.service.IRecommendService;
import com.stylefeng.guns.rest.modular.system.service.IRoomService;
import com.stylefeng.guns.rest.modular.system.service.ISetSpeedService;
import com.stylefeng.guns.rest.modular.system.service.ISignNumService;
import com.stylefeng.guns.rest.modular.system.service.ISignService;
import com.stylefeng.guns.rest.modular.system.service.ITreasureService;
import com.stylefeng.guns.rest.modular.system.service.IUserService;
import com.stylefeng.guns.rest.modular.system.service.IYzconsumeService;
import com.stylefeng.guns.rest.modular.util.ApiUtil;
import com.stylefeng.guns.rest.modular.util.DateUtil;
import com.stylefeng.guns.rest.modular.util.DistanceUtil;
import com.stylefeng.guns.rest.modular.util.ParamUtil;
import com.stylefeng.guns.rest.modular.util.SinataUtil;

import io.agora.signal.Signal;
import io.agora.signal.Signal.LoginSession;
import io.agora.signal.Signal.LoginSession.Channel;

@Controller
@RequestMapping("api")
public class HomeController {

	/**
	 * 日志记录（记录打印报文）
	 */
	Logger log = LoggerFactory.getLogger(getClass());
	// 签到天数
	@Autowired
	private ISignNumService signNumService;
    //签到设置
	@Autowired
	private ISignService signService;
	// 用户信息
	@Autowired
	private IUserService userService;
	// banner
	@Autowired
	private IBannerInfoService BannerInfoService;

	// 房间魅力值
	@Autowired
	private ICharmService charmService;

	// 房间财富值
	@Autowired
	private ITreasureService treasureService;
	// 热词
	@Autowired
	private IHotService HotService;
	// 房间
	@Autowired
	private IRoomService RoomService;
	//优钻获取记录
	@Autowired
	private IYzconsumeService YzconsumeService;
	 //速配/认证/语句设置/是否开放推荐位
	@Autowired
	private ISetSpeedService SetSpeedService;
	//推荐位置管理
	@Autowired
	private IRecommendService RecommendService;
	//推荐位申请记录
	@Autowired
	private IClaimerService ClaimerService;
	////推荐位申请记录
	@Autowired
	private IChatroomsService ChatroomsService;
	// Guild Part.
	@Autowired
	private IGuildService iGuildService;
	@Autowired
	private IGuildOrganizationService iGuildOrganizationService;
	// BulletScreen Part.
	@Autowired
	private IBulletScreenService iBulletScreenService;
	// Headline Part.
	@Autowired
	private IHeadlineService iHeadlineService;
	
	public static String SappID = ParamUtil.getValue("SappID");

	// 首页banner
	@RequestMapping("/home/FLbranner")
	@ResponseBody
	public Map<String, Object> FLbranner(Integer  type) {

		try {
			BannerInfo b = new BannerInfo();
			b.setIsSue(1);
			b.setIsDelete(0);
			b.setType(type);
			List<BannerInfo> list = BannerInfoService.getList(b);
			Iterator<BannerInfo> iterator = list.iterator();
			//1不跳，2外部，3内部，4房主房
			while (iterator.hasNext()) {
				BannerInfo info = iterator.next();
				if (info.getUrlType() == 3) {
					String url = "http://shanghaiyousuan.cn/yyYuYin-rest/api/branner?id=" + info.getId();
					info.setUrlHtml(url);
				}
			}
			return ApiUtil.returnObj(list);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("获取用户信息异常", e);
			return ApiUtil.putFailArray("获取banner失败");
		}
	}

	@RequestMapping("/brannerNum")
	@ResponseBody
	public Map<String, Object> brannerNum(Integer id) {
		BannerInfo se = BannerInfoService.selectById(id);
		se.setClicks(se.getClicks() + 1);
		BannerInfoService.updateById(se);
		return ApiUtil.putSuccessObj("成功");
	}

	@RequestMapping("/branner")
	public Object tobranner(Integer id) {
		return "/branner.html?id=" + id;
	}

	@RequestMapping("/getbranner")
	@ResponseBody
	public Object getbranner(Integer id) {
		try {
			BannerInfo se = BannerInfoService.selectById(id);
			return ApiJson.returnOK(se);
		} catch (Exception e) {
			e.printStackTrace();
			log.error(ApiJson.msgException, e);
			return ApiJson.returnNG("获取失败！");
		}
	}

	// 签到
	@RequestMapping("/Home/sign")
	@ResponseBody
	public Map<String, Object> getGoodsListByName(Integer uid) {
		try {
			if (SinataUtil.isEmpty(uid)) {
				return ApiUtil.putFailObj("uid不能为空！");
			}
			Map map = new HashMap<>();

			Date date = new Date();
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			String time = formatter.format(date);
			SignNum s0 = new SignNum();
			s0.setStart_time(time + " 00:00:00");
			s0.setEnd_time(time + " 23:59:59");
			s0.setUid(uid);
			//拿到当天是否签到
			List<SignNum> list = signNumService.getList(s0);
			if (list.size() > 0) {
				map.put("is_sign", 1);
				map.put("sign", list.get(0).getNum());
			} else {
				SignNum s = new SignNum();
				s.setUid(uid);
				s.setAddTime(date);
				SignNum timeLoginInfo = signNumService.getLastTimeLoginInfo(uid);
				SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
				String string = format.format(date);
				Date firstdate = format.parse(string);
				//判断是哪个时间范围内
				if (timeLoginInfo != null) {
					String string2 = format.format(timeLoginInfo.getAddTime());
					Date seconddate = format.parse(string2);
					if ((firstdate.getTime() - seconddate.getTime()) > 0
							&& (firstdate.getTime() - seconddate.getTime()) <= 86400000) {
						//查询上一天是第几天签到
						if (timeLoginInfo.getNum() == 7) {
							s.setNum(1);
							User user = userService.selectById(uid);
							Sign s1 = signService.selectById(1);
							Integer sum = user.getGold() + s1.getX();
							user.setGold(sum);
							userService.updateById(user);
							s.setGold(s1.getX());
							signNumService.insert(s);

							map.put("money", s1.getX());
						}
						if (timeLoginInfo.getNum() == 6) {
							s.setNum(7);
							User user = userService.selectById(uid);
							Sign s1 = signService.selectById(7);
							Integer m = s1.getX();
							Integer sum = user.getGold() + m;
							user.setGold(sum);
							s.setGold(m);
							userService.updateById(user);
							signNumService.insert(s);
							map.put("money", m);
						}
						if (timeLoginInfo.getNum() < 6) {
							int ms = timeLoginInfo.getNum() + 1;
							s.setNum(timeLoginInfo.getNum() + 1);
							User user = userService.selectById(uid);
							Sign s1 = signService.selectById(ms);
							Integer sum = user.getGold() + s1.getX();
							user.setGold(sum);
							s.setGold(s1.getX());
							userService.updateById(user);
							signNumService.insert(s);

							map.put("money", s1.getX());
						}
					} else {
						s.setNum(1);
						User user = userService.selectById(uid);
						Sign s1 = signService.selectById(1);
						Integer sum = user.getGold() + s1.getX();
						user.setGold(sum);
						s.setGold(s1.getX());
						userService.updateById(user);
						signNumService.insert(s);

						map.put("money", s1.getX());
					}
				} else {
					s.setNum(1);
					User user = userService.selectById(uid);
					Sign s1 = signService.selectById(1);
					Integer sum = user.getGold() + s1.getX();
					user.setGold(sum);
					s.setGold(s1.getX());
					userService.updateById(user);
					signNumService.insert(s);

					map.put("money", s1.getX());
				}
			}

			return ApiUtil.returnArray(map);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("签到异常", e);
			return ApiUtil.putFailObj("签到失败");
		}

	}

	// 签到页面
	@RequestMapping("/Home/Addsign")
	@ResponseBody
	public Map<String, Object> Addsign(Integer uid) {
		try {
			if (SinataUtil.isEmpty(uid)) {
				return ApiUtil.putFailObj("uid不能为空！");
			}
			Map map = new HashMap<>();

			Date date = new Date();
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			String time = formatter.format(date);
			SignNum s0 = new SignNum();
			s0.setStart_time(time + " 00:00:00");
			s0.setEnd_time(time + " 23:59:59");
			s0.setUid(uid);
			//判断几天是否签到过
			List<SignNum> list = signNumService.getList(s0);
			if (list.size() > 0) {
				map.put("is_sign", 1);
				map.put("sign", list.get(0).getNum());
			} else {
				SignNum s = new SignNum();
				s.setUid(uid);
				s.setAddTime(date);
				SignNum timeLoginInfo = signNumService.getLastTimeLoginInfo(uid);
				SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
				String string = format.format(date);
				Date firstdate = format.parse(string);
				if (timeLoginInfo != null) {
					String string2 = format.format(timeLoginInfo.getAddTime());
					Date seconddate = format.parse(string2);
					//拿到签到天数
					if ((firstdate.getTime() - seconddate.getTime()) > 0
							&& (firstdate.getTime() - seconddate.getTime()) <= 86400000) {
						if (timeLoginInfo.getNum() == 7) {
							map.put("is_sign", 0);
							map.put("sign", 0);

						}
						if (timeLoginInfo.getNum() == 6) {
							map.put("is_sign", 0);
							map.put("sign", timeLoginInfo.getNum());
						}
						if (timeLoginInfo.getNum() < 6) {
							map.put("is_sign", 0);
							map.put("sign", timeLoginInfo.getNum());
						}
					} else {

						map.put("is_sign", 0);
						map.put("sign", 0);
					}
				} else {
					map.put("is_sign", 0);
					map.put("sign", 0);
				}
			}
			
			// To get the UU coin's amount of sign(Each day).
			Sign sign = new Sign();
			List<Sign> signLists = signService.getList(sign); // Get seven days' amount of sign.
			StringBuilder combinedAmountStringBuilder = new StringBuilder();
	        for (int i = 0; i < signLists.size(); i++) {
	        	int uCoinAmount = signLists.get(i).getX(); // UU coin amount.
	        	combinedAmountStringBuilder.append(uCoinAmount).append(",");
	        }

	        map.put("uCoinAmounts", combinedAmountStringBuilder);

			return ApiUtil.returnObj(map);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("签到异常", e);
			return ApiUtil.putFailObj("获取签到信息失败");
		}

	}

	// 首页魅力和财富榜
	@RequestMapping("/Charm/SaveCharmCFML")
	@ResponseBody
	public Map<String, Object> SaveCharmCFML(HttpServletRequest request, Integer type, Integer state) {
		try {
			//type 1是财富，2是魅力    sate 1是今日，2本周，3总榜
			List<CharmModel> li = new ArrayList<>();
			if (type == 1) {
				if (state == 1) {
					Treasure se = new Treasure();
					Date date = new Date();
					DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
					String time = format.format(date);
					se.setBeginTime(time);
					se.setEndTime(time);
					List<Treasure> list = treasureService.getSumH(se);
					for (int i = 0; i < list.size(); i++) {
						User user = userService.selectById(list.get(i).getUid());
						CharmModel cl = new CharmModel();
						cl.setGrade(user.getTreasureGrade());
						cl.setUsercoding(user.getUsercoding());
						cl.setId(user.getId());
						cl.setImg(user.getImgTx());
						cl.setName(user.getNickname());
						//判断比是一个的还有差距值
						if (i == 0) {
							cl.setNum(0);
						} else {
							cl.setNum(list.get(i - 1).getNum() - list.get(i).getNum());
						}

						cl.setSex(user.getSex());
						li.add(cl);
					}
				}
				if (state == 2) {
					Treasure se = new Treasure();
					Date date = new Date();
					String timeInterval = DateUtil.getTimeInterval(date);

					String[] strArray = null;
					String replace = timeInterval.replace(" ", "");
					strArray = convertStrToArray(replace);

					se.setBeginTime(strArray[0]);
					se.setEndTime(strArray[1]);
					List<Treasure> list = treasureService.getSumH(se);
					for (int i = 0; i < list.size(); i++) {
						User user = userService.selectById(list.get(i).getUid());
						CharmModel cl = new CharmModel();
						cl.setGrade(user.getTreasureGrade());
						cl.setUsercoding(user.getUsercoding());
						cl.setId(user.getId());
						cl.setImg(user.getImgTx());
						cl.setName(user.getNickname());
						//判断比是一个的还有差距值
						if (i == 0) {
							cl.setNum(0);
						} else {
							cl.setNum(list.get(i - 1).getNum() - list.get(i).getNum());
						}
						cl.setSex(user.getSex());
						li.add(cl);
					}
				}
				if (state == 3) {
					Treasure se = new Treasure();
					List<Treasure> list = treasureService.getSumH(se);
					for (int i = 0; i < list.size(); i++) {
						User user = userService.selectById(list.get(i).getUid());
						CharmModel cl = new CharmModel();
						cl.setGrade(user.getTreasureGrade());
						cl.setUsercoding(user.getUsercoding());
						cl.setId(user.getId());
						cl.setImg(user.getImgTx());
						cl.setName(user.getNickname());
						//判断比是一个的还有差距值
						if (i == 0) {
							cl.setNum(0);
						} else {
							cl.setNum(list.get(i - 1).getNum() - list.get(i).getNum());
						}
						cl.setSex(user.getSex());
						li.add(cl);
					}
				}

			} else {
				if (state == 1) {
					Charm se = new Charm();
					Date date = new Date();
					DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
					String time = format.format(date);
					se.setBeginTime(time);
					se.setEndTime(time);
					List<Charm> list = charmService.getSumH(se);
					for (int i = 0; i < list.size(); i++) {
						User user = userService.selectById(list.get(i).getUid());
						CharmModel cl = new CharmModel();
						cl.setGrade(user.getCharmGrade());
						cl.setUsercoding(user.getUsercoding());
						cl.setId(user.getId());
						cl.setImg(user.getImgTx());
						cl.setName(user.getNickname());
						if (i == 0) {
							cl.setNum(0);
						} else {
							cl.setNum(list.get(i - 1).getNum() - list.get(i).getNum());
						}
						cl.setSex(user.getSex());
						li.add(cl);
					}
				}
				if (state == 2) {
					Charm se = new Charm();
					Date date = new Date();
					String timeInterval = DateUtil.getTimeInterval(date);

					String[] strArray = null;
					String replace = timeInterval.replace(" ", "");
					strArray = convertStrToArray(replace);

					se.setBeginTime(strArray[0]);
					se.setEndTime(strArray[1]);
					List<Charm> list = charmService.getSumH(se);
					for (int i = 0; i < list.size(); i++) {
						User user = userService.selectById(list.get(i).getUid());
						CharmModel cl = new CharmModel();
						cl.setGrade(user.getCharmGrade());
						cl.setUsercoding(user.getUsercoding());
						cl.setId(user.getId());
						cl.setImg(user.getImgTx());
						cl.setName(user.getNickname());
						if (i == 0) {
							cl.setNum(0);
						} else {
							cl.setNum(list.get(i - 1).getNum() - list.get(i).getNum());
						}
						cl.setSex(user.getSex());
						li.add(cl);
					}
				}
				if (state == 3) {
					Charm se = new Charm();
					List<Charm> list = charmService.getSumH(se);
					Iterator<Charm> iterator = list.iterator();
					for (int i = 0; i < list.size(); i++) {
						User user = userService.selectById(list.get(i).getUid());
						CharmModel cl = new CharmModel();
						cl.setGrade(user.getCharmGrade());
						cl.setId(user.getId());
						cl.setImg(user.getImgTx());
						cl.setUsercoding(user.getUsercoding());
						cl.setName(user.getNickname());
						if (i == 0) {
							cl.setNum(0);
						} else {
							cl.setNum(list.get(i - 1).getNum() - list.get(i).getNum());
						}
						cl.setSex(user.getSex());
						li.add(cl);
					}
				}
			}

			return ApiUtil.returnArray(li);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("举报信息异常", e);
			return ApiUtil.putFailArray("魅力和财富榜获取失败");
		}
	}

	// 获取热搜的搜所结果
	@RequestMapping("/hot/gethotList")
	@ResponseBody
	public Map<String, Object> gethotList(String name, Integer state, Integer pageNum, Integer pageSize) {

		try {
			Hot hot = new Hot();
			hot.setName(name);
			Hot hot2 = HotService.getOne(hot);
			//添加热词
			if (SinataUtil.isEmpty(hot2)) {
				hot.setCreateTime(new Date());
				hot.setIsDelete(1);
				hot.setState(2);
				hot.setStatus(1);
				hot.setNum(1);
				HotService.insert(hot);
			} else {
				hot2.setNum(hot2.getNum() + 1);
				HotService.updateById(hot2);
			}

			PageHelper.startPage(pageNum, pageSize, false);
			List<HotModel> hm = new ArrayList<>();
			if (state == 1) {
				//判断是字还是数字
				boolean numeric = isNumeric(name);
				Room r = new Room();
				if (numeric) {
					if(name.length() == 8){
						r.setRid(name);
					}else{
						r.setLiang(name);
					}
				} else {
					r.setRoomName(name);
				}
				List<Room> list = RoomService.getList(r);
				Iterator<Room> iterator = list.iterator();
				while (iterator.hasNext()) {
					Room next = iterator.next();
					HotModel h = new HotModel();
					User user = userService.selectById(next.getUid());
					h.setImg(user.getImgTx());
					h.setRoomName(next.getRoomName());
					h.setUid(user.getId());
					h.setUsercoding(user.getUsercoding());
					h.setUserName(user.getNickname());
					h.setLiang(next.getLiang());
					hm.add(h);
				}

			} else {

				boolean numeric = isNumeric(name);
				User r = new User();
				if (numeric) {
					if(name.length() == 8){
						r.setUsercoding(name);
					}else{
						r.setLiang(name);
					}
				} else {
					r.setNickname(name);
				}

				List<User> list = userService.getList(r);
				Iterator<User> iterator = list.iterator();
				while (iterator.hasNext()) {
					User next = iterator.next();
					Room r1 = new Room();
					r1.setRid(next.getUsercoding());
					Room one = RoomService.getOne(r1);
					HotModel h = new HotModel();
					h.setImg(next.getImgTx());
					h.setRoomName(one.getRoomName());
					h.setUid(next.getId());
					h.setUsercoding(next.getUsercoding());
					h.setUserName(next.getNickname());
					h.setLiang(next.getLiang());
					hm.add(h);
				}

			}
			return ApiUtil.returnArray(hm);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("获取用户信息异常", e);
			return ApiUtil.putFailArray("获取热搜的搜所结果失败");
		}

	}

	// 获取热搜
	@RequestMapping("/hot/hotList")
	@ResponseBody
	public Map<String, Object> hotList() {

		try {
			Hot ho = new Hot();
			ho.setStatus(2);
			ho.setIsDelete(1);
			List<Hot> list = HotService.getList(ho);

			return ApiUtil.returnArray(list);

		} catch (Exception e) {
			e.printStackTrace();
			log.error("获取用户信息异常", e);
			return ApiUtil.putFailArray("获取热搜失败");
		}
	}

	// 获取首页表单数据
	@RequestMapping("/hot/getHomeList")
	@ResponseBody
	public Map<String, Object> getHomeList(Integer state, int pageNum, int pageSize) {
		//state 1是热门，2是女神，3是男神，4娱乐，5听歌，6相亲，7电台
		try {
			PageHelper.startPage(pageNum, pageSize, false);
			List<HotModel> hm = new ArrayList<>();

			Room r = new Room();
			r.setType(state);
			List<Room> list = RoomService.getList(r);
			Iterator<Room> iterator = list.iterator();
			Signal sig = new Signal(SappID);
			while (iterator.hasNext()) {
				Room next = iterator.next();
				HotModel h = new HotModel();
				User user = userService.selectById(next.getUid());
				h.setImg(user.getImgTx());
				h.setRoomName(next.getRoomName());
				h.setUid(user.getId());
				h.setUsercoding(user.getUsercoding());
				h.setUserName(user.getNickname());
				h.setRoomLabel(next.getRoomLabel());
				LoginSession loginSession = sig.login("1", "_no_need_token", new Signal.LoginCallback());
				Channel channel = loginSession.channelJoin(next.getRid(), new Signal.ChannelCallback());
				h.setNum(next.getLineNum());
				hm.add(h);
			}

			return ApiUtil.returnArray(hm);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("获取用户信息异常", e);
			return ApiUtil.putFailArray("获取首页失败");
		}

	}

	// 获取推荐位置
	@RequestMapping("/hot/getHometJ")
	@ResponseBody
	public Map<String, Object> getHometJ() {

		try {
			List<HotModel> hm = new ArrayList<>();
			Recommend rc = new Recommend();
			rc.setState(1);
			DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH");
			Date sDate = new Date();
			
			Calendar c = Calendar.getInstance();  
            c.setTime(sDate);  
            c.add(Calendar.HOUR, 1);
            
			Date eDate = c.getTime();
			
			rc.setBeginTime(format.format(sDate) + ":00:00");
			rc.setEndTime(format.format(eDate) + ":00:00");
			List<Recommend> list = RecommendService.getList(rc);
			Iterator<Recommend> iterator = list.iterator();
			while (iterator.hasNext()) {
				Recommend next = iterator.next();
				HotModel h = new HotModel();
				User user = userService.selectById(next.getUid());
				Room r = new Room();
				r.setRid(user.getUsercoding());
				Room room = RoomService.getOne(r);
				h.setImg(user.getImgTx());
				h.setRoomName(room.getRoomName());
				h.setUid(user.getId());
				h.setUsercoding(user.getUsercoding());
				h.setUserName(user.getNickname());
				h.setRoomLabel(room.getRoomLabel());
				h.setNum(room.getLineNum());
				hm.add(h);
			}
			return ApiUtil.returnArray(hm);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("获取用户信息异常", e);
			return ApiUtil.putFailArray("获取推荐位置失败");
		}

	}

	// 获取推荐位申请
	@RequestMapping("/hot/getRecommend")
	@ResponseBody
	public Map<String, Object> getRecommend(Integer uid, String time) {

		try {
			SetSpeed speed = SetSpeedService.selectById(4);
			if (speed.getX().equals(2)) {
				return ApiUtil.putFailArray("推荐位已关闭");
			}
			User user = userService.selectById(uid);
			if (user.getRecommendCount() <= 0) {
				return ApiUtil.putFailArray("以没有推荐次数");
			}

			if (SinataUtil.isEmpty(time)) {
				return ApiUtil.putFailArray("没有设置推荐时间");
			}

			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Claimer cl2 = new Claimer();
			cl2.setsTime(sdf.parse(time));
			cl2.setUid(uid);
			Claimer one = ClaimerService.getOne(cl2);
			if (SinataUtil.isNotEmpty(one)) {
				return ApiUtil.putFailArray("已经在这个时间申请了推荐位");
			}

			user.setRecommendCount(user.getRecommendCount() - 1);
			userService.updateById(user);

			Recommend rc = new Recommend();
			rc.setIsDefault(1);
			List<Recommend> list = RecommendService.getList(rc);

			Claimer cl = new Claimer();
			cl.setsTime(sdf.parse(time));
			List<Claimer> list2 = ClaimerService.getList(cl);
			if (list2.size() >= list.size()) {
				return ApiUtil.putFailArray("该时间段推荐位数量已经抢光");
			} else {
				Claimer cla = new Claimer();
				cla.setCreateTime(new Date());
				cla.setUid(uid);
				cla.setsTime(sdf.parse(time));
				Date fz = DateUtil.getMillisecond_fz1(time, 60);
				cla.seteTime(fz);
				cla.setState(1);
				ClaimerService.insert(cla);
			}
			return ApiUtil.putSuccessObj("成功");
		} catch (Exception e) {
			e.printStackTrace();
			log.error("获取用户信息异常", e);
			return ApiUtil.putFailArray("获取推荐位申请失败");
		}

	}

	// 速配
	@RequestMapping("/hot/getSpeed")
	@ResponseBody
	public Map<String, Object> getSpeed(Integer uid, String lat, String lon) {

		try {

			List<UserModel> u = new ArrayList<>();
			SetSpeed speed = SetSpeedService.selectById(1);

			User users = new User();
			Integer pageNum = 0;
			Integer pageSize = Integer.getInteger(speed.getX());
			User user = userService.selectById(uid);
			user.setWd(lat);
			user.setJd(lon);
			userService.updateById(user);
			users.setId(uid);
			//拿到数据设置条件
			if (user.getSex() == 1) {
				users.setSex(2);
			} else {
				users.setSex(1);
			}
			if (user.getAge() <= 5) {
				users.setMinAge(0);
			} else {
				users.setMinAge(user.getAge() - 5);
			}
			users.setMaxAge(user.getAge() + 5);
			users.setState(1);
			List<User> list = userService.getList1(users);
			Iterator<User> iterator = list.iterator();
			while (iterator.hasNext()) {
				User next = iterator.next();
				UserModel um = new UserModel();
				um.setCharmGrade(next.getCharmGrade());
				um.setTreasureGrade(next.getTreasureGrade());
				um.setSex(next.getSex());
				um.setName(next.getNickname());
				um.setNum(next.getFansNum());
				um.setVoice(next.getVoice());
				um.setImg(next.getImgTx());
				um.setIndividuation(next.getIndividuation());
				um.setUsercoding(next.getUsercoding());
				um.setId(next.getId());
				um.setVoiceTime(next.getVoiceTime());
				//计算两个位置的距离
				double distance = DistanceUtil.GetDistance(Double.parseDouble(user.getJd()),
						Double.parseDouble(user.getWd()), Double.parseDouble(next.getJd()),
						Double.parseDouble(next.getWd()));
				um.setDistance(distance);

				u.add(um);
			}

			return ApiUtil.returnArray(u);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("获取用户信息异常", e);
			return ApiUtil.putFailArray("速配失败");
		}

	}

	// 是否有音乐签名
	@RequestMapping("/hot/getVoiceTime")
	@ResponseBody
	public Map<String, Object> getVoiceTime(Integer uid) {

		try {

			Map map = new HashMap<>();
			User user = userService.selectById(uid);
			if (SinataUtil.isEmpty(user.getVoice())) {
				map.put("state", 1);
				map.put("voice", "");
			} else {
				map.put("state", 2);
				map.put("voice", user.getVoice());
			}
			return ApiUtil.returnObj(map);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("获取用户信息异常", e);
			return ApiUtil.putFailArray("音乐签名失败");
		}

	}

	// 同城
	@RequestMapping("/hot/getlocal")
	@ResponseBody
	public Map<String, Object> getlocal(Integer uid, String lat, String lon, int pageNum, int pageSize) {

		try {

			List<UserModel> u = new ArrayList<>();
			User users = new User();
			User user = userService.selectById(uid);
			user.setWd(lat);
			user.setJd(lon);
			userService.updateById(user);
			users.setState(1);
			users.setJd(lon);
			users.setWd(lat);
			users.setId(uid);
			PageHelper.startPage(pageNum, pageSize, false);
			List<User> list = userService.getList2(users);
			Iterator<User> iterator = list.iterator();
			while (iterator.hasNext()) {
				User next = iterator.next();
				UserModel um = new UserModel();
				um.setCharmGrade(next.getCharmGrade());
				um.setTreasureGrade(next.getTreasureGrade());
				um.setSex(next.getSex());
				um.setName(next.getNickname());
				um.setNum(next.getFansNum());
				um.setVoice(next.getVoice());
				um.setIndividuation(next.getIndividuation());
				um.setUsercoding(next.getUsercoding());
				um.setId(next.getId());
				um.setImg(next.getImgTx());
				um.setDistance(next.getDistance());
				Room room = new Room();
				room.setRid(next.getUsercoding());
				Room one = RoomService.getOne(room);
				um.setManNum(one.getLineNum());

				if (next.getIsR() < 3) {
					Chatrooms cs = new Chatrooms();
					cs.setUid(next.getId());
					cs.setStatus(1);
					Chatrooms one1 = ChatroomsService.getOne(cs);
					if (SinataUtil.isNotEmpty(one1)) {
						um.setType(1);
						um.setUsercoding(one1.getPid());
					} else {
						um.setType(1);
						um.setUsercoding(next.getUsercoding());
					}
				} else {
					um.setType(2);
					um.setUsercoding(next.getUsercoding());
				}

				um.setVoiceTime(next.getVoiceTime());
				u.add(um);
			}

			return ApiUtil.returnArray(u);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("获取用户信息异常", e);
			return ApiUtil.putFailArray("获取同城失败");
		}

	}

	// 获取推荐位历史数据
	@RequestMapping("/hot/getclaimerList")
	@ResponseBody
	public Map<String, Object> getclaimerList(Integer uid, int pageNum, int pageSize) {

		try {
			List<ClaimerModel> cl = new ArrayList<>();
			PageHelper.startPage(pageNum, pageSize, false);
			Claimer claimer = new Claimer();
			claimer.setUid(uid);
			List<Claimer> list = ClaimerService.getList(claimer);
			Iterator<Claimer> iterator = list.iterator();
			while (iterator.hasNext()) {
				Claimer claimer2 = iterator.next();
				User user = userService.selectById(claimer2.getUid());
				ClaimerModel cla = new ClaimerModel();
				cla.setRid(user.getUsercoding());
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				cla.setYtime(sdf.format(claimer2.getsTime()));
				SimpleDateFormat sdf1 = new SimpleDateFormat("HH");
				cla.setMtime(sdf1.format(claimer2.getsTime()));
				cl.add(cla);
			}
			return ApiUtil.returnArray(cl);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("获取用户信息异常", e);
			return ApiUtil.putFailArray("获取推荐位历史失败");
		}

	}

	// 获取推荐位次数
	@RequestMapping("/hot/getUserNum")
	@ResponseBody
	public Map<String, Object> getUserNum(Integer uid) {

		try {
			Map map = new HashMap<>();
			User user = userService.selectById(uid);
			map.put("num", user.getRecommendCount());
			return ApiUtil.returnObj(map);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("获取用户信息异常", e);
			return ApiUtil.putFailArray("获取推荐位次数失败");
		}

	}
	
	// Join the guild.
	@RequestMapping("/guild/joinGuild")
	@ResponseBody
	public Map<String, Object> joinGuild(int uid, int gid) {		
		try {
			if (SinataUtil.isEmpty(uid)) {
				return ApiUtil.putFailObj("uid不能为空！");
			}
			if (SinataUtil.isEmpty(gid)) {
				return ApiUtil.putFailObj("gid不能为空！");
			}
			
			GuildOrganization searchedGuildOrganization = new GuildOrganization();
			searchedGuildOrganization.setGuildId(gid);
			GuildOrganization ifEmptyGuildOrganization = iGuildOrganizationService.getOne(searchedGuildOrganization);
			
			if (SinataUtil.isEmpty(ifEmptyGuildOrganization)) {
				return ApiUtil.returnedData(-99, "不存在此公会。", "It's not a guild ID!!!");
			} else {
				Guild searchedGuild = new Guild();
				searchedGuild.setUid(uid);
				Guild ifEmptyGuild = iGuildService.getOne(searchedGuild);
				if (!SinataUtil.isEmpty(ifEmptyGuild)) {
					return ApiUtil.returnedData(-99, "您只能加入一个公会。", "You can join only one guild!!!");
				} else {
					Guild insertedGuild = new Guild();
					insertedGuild.setUid(uid);
					insertedGuild.setGid(gid);
					insertedGuild.setStatus(0);
					insertedGuild.setCreateDate(LocalDateTime.now().format(DateTimeFormatter.ofPattern("YYYY-MM-dd HH:mm:ss")));
				  	iGuildService.insert(insertedGuild);
				  	return ApiUtil.returnObj("");
				}
			}	
		} catch (Exception e) {
			e.printStackTrace();
			log.error("加入公会失败！", e);
			return ApiUtil.putFailArray("加入公会失败！");
		}
	}
	
	// Get the 'Dan'mu'.
	@RequestMapping("/bulletScreen/getList")
	@ResponseBody
	public Map<String, Object> bulletScreenGetList() {
		try {
			BulletScreen bulletScreen = new BulletScreen();
			List<BulletScreen> bulletScreenLists = iBulletScreenService.getList(bulletScreen);
			return ApiUtil.returnObj(bulletScreenLists);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("查询弹幕失败！", e);
			return ApiUtil.putFailArray("查询弹幕失败！");
		}
	}
	
	// Add a headline.
	@RequestMapping("/headline/add")
	@ResponseBody
	public Map<String, Object> headlineAdd(int uid, String comments) {		
		try {
			if (SinataUtil.isEmpty(uid)) {
				return ApiUtil.putFailObj("uid不能为空！");
			}
			User user = userService.selectById(uid);
			if (user.getGold() >= 88) {

				Headline searchedHeadline = new Headline();
				searchedHeadline.setStatus(0);
				Headline ifEmptyHeadline = iHeadlineService.getOne(searchedHeadline);
				if (SinataUtil.isEmpty(ifEmptyHeadline)) {
					Headline placeholderHeadline = new Headline();
					placeholderHeadline.setUsername("优优官方");
					placeholderHeadline.setUserAvatar("https://uuvoices.oss-cn-shanghai.aliyuncs.com/img/UU_Official.jpeg");
					placeholderHeadline.setUserSex(1);
					placeholderHeadline.setComments("大家都来抢头条哦~");
					placeholderHeadline.setRemainingTime(300);
					placeholderHeadline.setStatus(0);
				  	iHeadlineService.insert(placeholderHeadline);
				}
				
				Headline headline = new Headline();
			  	headline.setUsername(user.getNickname());
			  	headline.setUserAvatar(user.getImgTx());
			  	headline.setUserSex(user.getSex());
			  	headline.setComments(comments);
			  	headline.setRemainingTime(300);
			  	headline.setStatus(0);
			  	iHeadlineService.insert(headline);
			  	
			  	user.setGold(user.getGold() - 88);
			  	userService.updateById(user);
			  	
			  	return ApiUtil.returnObj("添加头条成功！");
			} else {
				return ApiUtil.returnObj_1("请充值！");
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error("添加头条失败！", e);
			return ApiUtil.putFailArray("添加头条失败！");
		}
	}
	
	// Delete a headline(Change the status). Using a job. ==> HeadlineJob.java

	// Get the headline.
	@RequestMapping("/headline/getOne")
	@ResponseBody
	public Map<String, Object> headlineGetOne() {
		try {
			Headline searchedHeadline = new Headline();
			searchedHeadline.setStatus(0);
			Headline returnedHeadline = iHeadlineService.getOne2(searchedHeadline);
			if (SinataUtil.isEmpty(returnedHeadline)) {
				return ApiUtil.returnObj("");
			}
			long currentTime = new Date().getTime();
			long headlineTime = returnedHeadline.getUpdateTime().getTime();
			int headlineSeconds = currentTime > headlineTime ? (int) ((currentTime - headlineTime) / 1000) : 0;
			int seconds = 300 < headlineSeconds ? 0 : (300 - headlineSeconds);
			returnedHeadline.setRemainingTime(seconds);
			return ApiUtil.returnObj(returnedHeadline);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("查询头条失败！", e);
			return ApiUtil.putFailArray("查询头条失败！");
		}
	}
	
	// 使用String的split 方法
	public static String[] convertStrToArray(String str) {
		String[] strArray = null;
		strArray = str.split(","); // 拆分字符为"," ,然后把结果交给数组strArray
		return strArray;
	}

	public static boolean isNumeric(String str) {
		Pattern pattern = Pattern.compile("[0-9]*");

		Matcher matcher = pattern.matcher((CharSequence) str);

		boolean result = matcher.matches();

		return result;
	}

}
