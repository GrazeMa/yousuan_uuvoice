package com.stylefeng.guns.rest.modular.system.service;

import java.util.List;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.rest.modular.system.model.Guild;

/**
 * @Description 公会服务类。
 * @author Grazer_Ma
 * @Date 2020-05-19 10:51:01
 */
public interface IGuildService extends IService<Guild> {
	
	public List<Guild> getList(Guild model);

	Guild getOne(Guild model);

}
