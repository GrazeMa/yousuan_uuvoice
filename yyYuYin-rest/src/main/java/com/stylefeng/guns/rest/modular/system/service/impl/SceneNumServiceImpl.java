package com.stylefeng.guns.rest.modular.system.service.impl;

import com.stylefeng.guns.rest.modular.system.model.Recharge;
import com.stylefeng.guns.rest.modular.system.model.Scene;
import com.stylefeng.guns.rest.modular.system.model.SceneNum;
import com.stylefeng.guns.rest.modular.system.dao.SceneNumMapper;
import com.stylefeng.guns.rest.modular.system.service.ISceneNumService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import java.util.List;

import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wumeng123
 * @since 2019-02-18
 */
@Service
public class SceneNumServiceImpl extends ServiceImpl<SceneNumMapper, SceneNum> implements ISceneNumService {

	@Override
	public List<SceneNum> getList(SceneNum model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getList(model);
	}

	@Override
	public SceneNum getOne(SceneNum model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getOne(model);
	}



}
