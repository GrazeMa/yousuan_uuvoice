package com.stylefeng.guns.rest.modular.system.service;

import com.stylefeng.guns.rest.modular.system.model.UserBag;

import java.util.List;

import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 用户背包服务类
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
public interface IUserBagService extends IService<UserBag> {

	List<UserBag> getByUid(Integer uid);
	UserBag getOne(UserBag model);
}
