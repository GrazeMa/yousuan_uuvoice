package com.stylefeng.guns.rest.modular.system.service;

import com.stylefeng.guns.rest.modular.system.model.Recharge;
import com.stylefeng.guns.rest.modular.system.model.Recharge;

import java.util.List;

import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 充值明细 服务类
 * </p>
 *
 * @author wumeng123
 * @since 2019-02-14
 */
public interface IRechargeService extends IService<Recharge> {
	 public List<Recharge> getList(Recharge model);
	 public List<Recharge> getList1(Recharge model);
	    Recharge getOne(Recharge model);
}
