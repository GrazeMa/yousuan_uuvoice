package com.stylefeng.guns.rest.modular.system.controller.model;

public class OrderUserModel {

	private Integer id;
	private String name;
	private Integer sex;
	private String userAvaterUrl;
	private String usercoding;
	private Integer treasureGrade;
	private Integer charmGrade;
	private String individuation;
	private String voice;
	private Integer voiceTime;
	private Integer isAttention;

	public String getVoice() {
		return voice;
	}

	public void setVoice(String voice) {
		this.voice = voice;
	}

	public String getIndividuation() {
		return individuation;
	}

	public void setIndividuation(String individuation) {
		this.individuation = individuation;
	}

	public Integer getTreasureGrade() {
		return treasureGrade;
	}

	public void setTreasureGrade(Integer treasureGrade) {
		this.treasureGrade = treasureGrade;
	}

	public Integer getCharmGrade() {
		return charmGrade;
	}

	public void setCharmGrade(Integer charmGrade) {
		this.charmGrade = charmGrade;
	}

	public String getUsercoding() {
		return usercoding;
	}

	public void setUsercoding(String usercoding) {
		this.usercoding = usercoding;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getSex() {
		return sex;
	}

	public void setSex(Integer sex) {
		this.sex = sex;
	}

	public String getUserAvaterUrl() {
		return userAvaterUrl;
	}

	public void setUserAvaterUrl(String userAvaterUrl) {
		this.userAvaterUrl = userAvaterUrl;
	}
	
	public Integer getVoiceTime() {
		return voiceTime;
	}

	public void setVoiceTime(Integer voiceTime) {
		this.voiceTime = voiceTime;
	}
	
	public Integer getIsAttention() {
		return isAttention;
	}

	public void setIsAttention(Integer isAttention) {
		this.isAttention = isAttention;
	}

}
