package com.stylefeng.guns.rest.modular.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import com.cloopen.rest.sdk.CCPRestSmsSDK;

/**
 * 短信发送接口
 */
public class SendSmsUtil {

	// 生成环境：https://app.cloopen.com:8883
	// 开发环境：https://sandboxapp.cloopen.com:8883

	private static String URL = "app.cloopen.com";
	private static String PORT = "8883";
	private static String ACCOUNTSID = "8a216da865ae11f80165b7f25fae041b";
	private static String AUTHTOKEN = "1c5eefcb21104fd0bae480ef14cc929d";
	private static String APPID = "8a216da865ae11f80165b7f2600a0422";
	
	/**
	 * 发送方法 其他方法同理 返回1都是提交成功 -1发送失败
	 */
	@SuppressWarnings("unchecked")
	public static Map<String, Object> sendSMS(String msg, int type, String... phone) {
		HashMap<String, Object> result = null;
		CCPRestSmsSDK restAPI = new CCPRestSmsSDK();
		restAPI.init(URL, PORT);// 初始化服务器地址和端口，格式如下，服务器地址不需要写https://
		restAPI.setAccount(ACCOUNTSID, AUTHTOKEN);// 初始化主帐号和主帐号TOKEN
		restAPI.setAppId(APPID);// 初始化应用ID
		StringBuffer telPhone = new StringBuffer();
		for (int i = 0; i < phone.length; i++) {
			telPhone.append(phone[i] + ",");
		}
		result = restAPI.sendTemplateSMS(telPhone.substring(0, telPhone.length() - 1).toString(), "398585",
				new String[] { msg, "10分钟" });
		if ("000000".equals(result.get("statusCode"))) {
			// 正常返回输出data包体信息（map）
			HashMap<String, Object> data = (HashMap<String, Object>) result.get("data");
			Set<String> keySet = data.keySet();
			for (String key : keySet) {
				Object object = data.get(key);
				System.out.println(key + " = " + object);
			}
		} else {
			// 异常返回输出错误码和错误信息
			System.out.println("错误码=" + result.get("statusCode") + " 错误信息= " + result.get("statusMsg"));
		}
		return result;
	}

	public static Map<String, Object> sendSMS1(String msg, String... phone) {
		HashMap<String, Object> result = null;
		CCPRestSmsSDK restAPI = new CCPRestSmsSDK();
		restAPI.init(URL, PORT);// 初始化服务器地址和端口，格式如下，服务器地址不需要写https://
		restAPI.setAccount(ACCOUNTSID, AUTHTOKEN);// 初始化主帐号和主帐号TOKEN
		restAPI.setAppId(APPID);// 初始化应用ID
		StringBuffer telPhone = new StringBuffer();
		for (int i = 0; i < phone.length; i++) {
			telPhone.append(phone[i] + ",");
		}
		result = restAPI.sendTemplateSMS(telPhone.substring(0, telPhone.length() - 1).toString(), "259215",
				new String[] { msg });
		if ("000000".equals(result.get("statusCode"))) {
			// 正常返回输出data包体信息（map）
			HashMap<String, Object> data = (HashMap<String, Object>) result.get("data");
			Set<String> keySet = data.keySet();
			for (String key : keySet) {
				Object object = data.get(key);
				System.out.println(key + " = " + object);
			}
		} else {
			// 异常返回输出错误码和错误信息
			System.out.println("错误码=" + result.get("statusCode") + " 错误信息= " + result.get("statusMsg"));
		}
		return result;
	}
}
