package com.stylefeng.guns.rest.modular.system.model;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

/**
 * @Description 收藏 Entity。
 * @author Grazer_Ma
 * @Date 2020-05-25 11:45:35
 */
@TableName("app_favorite")
public class Favorite extends Model<Favorite> {

	private static final long serialVersionUID = 1L;

	@TableId(value = "id", type = IdType.AUTO)
	private Integer id; // 收藏主键 ID。
	private Date createTime; // 收藏时间。

	private Integer userId; // 收藏者 ID。
	private Integer roomId; // 收藏房间 ID。

	@TableField(exist = false)
	private String img;
	@TableField(exist = false)
	private String roomName;
	@TableField(exist = false)
	private Integer num;
	@TableField(exist = false)
	private String roomLabel;
	@TableField(exist = false)
	private String usercoding;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getRoomId() {
		return roomId;
	}

	public void setRoomId(Integer roomId) {
		this.roomId = roomId;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public String getRoomName() {
		return roomName;
	}

	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}

	public Integer getNum() {
		return num;
	}

	public void setNum(Integer num) {
		this.num = num;
	}

	public String getRoomLabel() {
		return roomLabel;
	}

	public void setRoomLabel(String roomLabel) {
		this.roomLabel = roomLabel;
	}

	public String getUsercoding() {
		return usercoding;
	}

	public void setUsercoding(String usercoding) {
		this.usercoding = usercoding;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "Friend{" + "id=" + id + ", userId=" + userId + ", roomId=" + roomId + ", createTime=" + createTime
				+ ", img=" + img + ", roomName=" + roomName + ", num=" + num + ", roomLabel=" + roomLabel
				+ ", usercoding=" + usercoding + "}";
	}
}
