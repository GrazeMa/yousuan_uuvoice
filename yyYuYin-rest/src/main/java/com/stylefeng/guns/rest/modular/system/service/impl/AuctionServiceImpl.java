package com.stylefeng.guns.rest.modular.system.service.impl;

import com.stylefeng.guns.rest.modular.system.model.Auction;
import com.stylefeng.guns.rest.modular.system.dao.AuctionMapper;
import com.stylefeng.guns.rest.modular.system.service.IAuctionService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import java.util.List;

import org.springframework.stereotype.Service;

/**
 * <p>
 * 房间竞拍排名 服务实现类
 * </p>
 *
 * @author wumeng123
 * @since 2019-01-30
 */
@Service
public class AuctionServiceImpl extends ServiceImpl<AuctionMapper, Auction> implements IAuctionService {

	@Override
	public List<Auction> getList(Auction model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getList(model);
	}

	@Override
	public Auction getOne(Auction model) {
		// TODO Auto-generated method stub
		return this.baseMapper.getOne(model);
	}

}
